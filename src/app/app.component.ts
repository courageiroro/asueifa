import { Component, AfterViewInit } from '@angular/core';
import { FuseSplashScreenService } from './core/services/splash-screen.service';
/* import { AppsService } from './app.service'; */
import { FuseProjectComponent } from './main/content/apps/dashboards/project/project.component'
import { Router, RouterModule } from '@angular/router';
import { UserService } from './user.service';
import {PouchService} from './provider/pouch-service'
declare var jquery: any;
declare var $: any;


@Component({
    selector: 'fuse-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
    public rootPage: any;

    constructor(private fuseSplashScreen: FuseSplashScreenService, public db: PouchService, public router: Router, /* private db: AppsService */ splashScreen: FuseSplashScreenService, private user: UserService) {


        var localStorageitem = JSON.parse(localStorage.getItem('user'));
        console.log(localStorageitem)
        if (!localStorageitem) {
            var userType = { usertype: null }
            localStorage.setItem('user', JSON.stringify(userType));
        }

        

        let credentials = JSON.parse(localStorage.getItem('credentials'));
        console.log(credentials);
        this.db.initDB(credentials);
 
        /*    if (credentials) {
               credentials.online = true; //debug
               credentials.skip_setup = true; //debug
               this.db.initDB(credentials);
   
               this.rootPage = FuseProjectComponent;
           }
           else {
               this.rootPage = FuseLoginDBComponent;
           } */
        // Okay, so the platform is ready and our plugins are available.
        // Here you can do any higher level native things you might need.
        //statusBar.styleDefault();
        //splashScreen.hide();
        //statusBar.hide();

    }

    ngAfterViewInit() {
        this.db.getADStaffs().then(res => {
            console.log(res);
            var localStorageitem = JSON.parse(localStorage.getItem('user'));
            if (localStorageitem.usertype == null && res.length == 0) {
                this.router.navigate(['/apps/login']);
            }
            else if(localStorageitem.usertype == null && res.length > 0){
                this.router.navigate(['/apps/login']);
            }
            else if(localStorageitem.usertype == 'Admin' && res.length > 0){
                //this.router.navigate(['/apps/login']);
                this.router.navigate(['/apps/dashboards/project']);
                /* this.user.setUserLoggedIn();
                console.log(this.user.setUserLoggedIn()); */
            }
            else {
                this.router.navigate(['/apps/lab_dashboards']);
                /* this.user.setUserLoggedIn()
                console.log(this.user.setUserLoggedIn()); */
            }
    
        })
       
    }

}
