import { NgModule, enableProdMode } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, ActivatedRoute, ParamMap, Routes } from '@angular/router';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { Pipe, PipeTransform } from "@angular/core";
import { CurrencyPipe } from '@angular/common';
import { AngularWeatherWidgetModule, WeatherApiName } from 'angular-weather-widget';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { LoadingModule } from 'ngx-loading';
/* import { AppsService } from './app.service'; */
import { CreateDbModule } from './create-db/create-db.module';
import { Angular4PaystackModule } from 'angular4-paystack';
//import { Ng2AutoCompleteModule } from 'ng2-auto-complete';

import {
    MdAutocompleteModule,
    MdButtonModule,
    MdButtonToggleModule,
    MdCardModule,
    MdCheckboxModule,
    MdChipsModule,
    MdCoreModule,
    MdDatepickerModule,
    MdDialogModule,
    MdExpansionModule,
    MdGridListModule,
    MdIconModule,
    MdInputModule,
    MdListModule,
    MdMenuModule,
    MdNativeDateModule,
    MdPaginatorModule,
    MdProgressBarModule,
    MdProgressSpinnerModule,
    MdRadioModule,
    MdRippleModule,
    MdSelectModule,
    MdSidenavModule,
    MdSliderModule,
    MdSlideToggleModule,
    MdSnackBarModule,
    MdSortModule,
    MdTableModule,
    MdTabsModule,
    MdToolbarModule,
    MdTooltipModule,
} from '@angular/material';
import 'hammerjs';

import { SharedModule } from './core/modules/shared.module';
import { AppComponent } from './app.component';
//import { FuseCreateDbComponent } from './create-db/create-db.component';
//import { FuseLoginDBComponent } from './main/content/apps/logindb/logindb.component';
import { ProjectModule } from './main/content/apps/dashboards/project/project.module';
import { FuseDepartmentsModule } from './main/content/apps/departments/departments.module';
import { FuseStaffsModule } from './main/content/apps/staffs/staffs.module';
import { FuseDoctorsModule } from './main/content/apps/doctorviews/doctorviews.module';
import { FuseAccountantsModule } from './main/content/apps/accountants/accountants.module';
import { FuseAccountsModule } from './main/content/apps/accounts/accounts.module';
import { FuseAddappointmentsModule } from './main/content/apps/addappointments/addappointments.module';
import { FuseLab_dashboardsModule } from './main/content/apps/lab_dashboards/lab_dashboards.module';
import { FuseAdminsModule } from './main/content/apps/admins/admins.module';
import { FuseAdmit_historysModule } from './main/content/apps/admit_historys/admit_historys.module';
import { FusealertssModule } from './main/content/apps/alerts/alerts.module';
import { FuseConfigureanterecordsModule } from './main/content/apps/ante_records/ante_records.module';
import { FuseantenatalsModule } from './main/content/apps/antenatal/antenatals.module';
import { FusePatientsModule } from './main/content/apps/patients/patients.module';
import { FuseRecordsModule } from './main/content/apps/records/records.module';
import { FuseanterecordsModule } from './main/content/apps/anterecords/anterecords.module';
import { FuseAppointmentsModule } from './main/content/apps/appointments/appointments.module';
import { FuseBed_allotmentsModule } from './main/content/apps/bed_allotments/bed_allotments.module';
import { FuseBedsModule } from './main/content/apps/beds/beds.module';
import { FuseBlood_banksModule } from './main/content/apps/blood_banks/blood_banks.module';
import { FuseBlood_donorsModule } from './main/content/apps/blood_donors/blood_donors.module';
import { FuseBlood_pricesModule } from './main/content/apps/blood_prices/blood_prices.module';
import { FuseBlood_salesModule } from './main/content/apps/blood_sales/blood_sales.module';
import { FuseConfigurerecordsModule } from './main/content/apps/configure_records/configure_records.module';
import { FuseConsultancysModule } from './main/content/apps/consultancys/consultancys.module';
import { FuseCurrencysModule } from './main/content/apps/currencys/currencys.module';
import { FuseDiagnosis_reportsModule } from './main/content/apps/diagnosis_reports/diagnosis_reports.module';
import { FuseExpensesModule } from './main/content/apps/expenses/expenses.module';
import { FuseExtendlicensesModule } from './main/content/apps/extendlicenses/extendlicenses.module';
import { FuseInsuranceshemesModule } from './main/content/apps/insuranceshemes/insuranceshemes.module';
import { FuseInsurancetypesModule } from './main/content/apps/insurancetypes/insurancetypes.module';
import { FuseInvoicesModule } from './main/content/apps/invoices/invoices.module';
import { FuseLaboratoristsModule } from './main/content/apps/laboratorists/laboratorists.module';
import { FuselabrecordsModule } from './main/content/apps/labrecords/labrecords.module';
import { FuselabtestsModule } from './main/content/apps/labtest/labtest.module';
import { FuseManagepaymentsModule } from './main/content/apps/managepayments/managepayments.module';
import { FuseMedicine_categorysModule } from './main/content/apps/medicine_categorys/medicine_categorys.module';
import { FuseMedicine_salesModule } from './main/content/apps/medicine_sales/medicine_sales.module';
import { FuseMedicinesModule } from './main/content/apps/medicines/medicines.module';
import { FuseMessage_threadsModule } from './main/content/apps/message_threads/message_threads.module';
import { FuseMessagesModule } from './main/content/apps/messages/messages.module';
import { FuseNotesModule } from './main/content/apps/notes/notes.module';
import { FuseNoticesModule } from './main/content/apps/notices/notices.module';
import { FuseNursesModule } from './main/content/apps/nurses/nurses.module';
import { FuseOfflinesModule } from './main/content/apps/offlines/offlines.module';
import { FusePayrollsModule } from './main/content/apps/payrolls/payrolls.module';
import { FusePharmacistsModule } from './main/content/apps/pharmacists/pharmacists.module';
import { FusePrescriptionsModule } from './main/content/apps/prescriptions/prescriptions.module';
import { FuseReceiptsModule } from './main/content/apps/receipts/receipts.module';
import { FuseReceptionistsModule } from './main/content/apps/receptionists/receptionists.module';
import { FuseReport_historysModule } from './main/content/apps/report_historys/report_historys.module';
import { FuseReportsModule } from './main/content/apps/reports/reports.module';
import { FuseSettingsModule } from './main/content/apps/settings/settings.module';
/* import { FuseSmssModule } from './main/content/apps/smss/smss.module'; */
import { FuseSmssettingsModule } from './main/content/apps/smssettings/smssettings.module';
import { FuseDoctorviewpagesModule } from './main/content/apps/doctorviews/doctorviewpage/doctorviewpages.module';
import { FuseMailModule } from './main/content/apps/mail2/mail.module';
import { FusefollowupsModule } from './main/content/apps/followup/followups.module';
import { PerfectScrollbarConfigInterface, PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FuseFakeDbService } from './fuse-fake-db/fuse-fake-db.service';
import { FuseMainModule } from './main/main.module';
import { PagesModule } from './main/content/pages/pages.module';
import { UIModule } from './main/content/ui/ui.module';
import { ComponentsModule } from './main/content/components/components.module';
import { FuseSplashScreenService } from './core/services/splash-screen.service';
import { FuseConfigService } from './core/services/config.service';
import { StaffService } from './provider/staff-service';
import { HttpModule } from '@angular/http';
import { CdkTableModule } from '@angular/cdk';
import { AuthGuard } from './auth.guard';
//import { CreateDbModule } from './create-db/create-db.module';


const appRoutes: Routes = [

    {
        path: 'apps/mails',
        loadChildren: './main/content/apps/mail2/mail.module#FuseMailModule',
        canActivate: [AuthGuard]
    },
    {
        path: 'apps/mail',
        loadChildren: './main/content/apps/mail/mail.module#FuseMailModule',
        canActivate: [AuthGuard]
    },
    /*  {
        path: 'apps/mails/inbox',
        loadChildren: './main/content/apps/mail2/mail.module#FuseMailModule'
    },  */
    {
        path: 'apps/login',
        loadChildren: './main/content/apps/login/login.module#FuseLoginModule',
        //canActivate: [AuthGuard]
    },
      {
        path: 'apps/managepayment',
        loadChildren: './main/content/apps/managepayments/managepayments.module#FuseManagepaymentsModule',
        //canActivate: [AuthGuard]
    },
     {
        path: 'apps/offline',
        loadChildren: './main/content/apps/offlines/offlines.module#FuseOfflinesModule',
        //canActivate: [AuthGuard]
    },
      {
        path: 'apps/insurancetype',
        loadChildren: './main/content/apps/insurancetypes/insurancetypes.module#FuseInsurancetypesModule',
        //canActivate: [AuthGuard]
    },
     {
        path: 'apps/insurancescheme',
        loadChildren: './main/content/apps/insuranceshemes/insuranceshemes.module#FuseInsuranceshemesModule',
        //canActivate: [AuthGuard]
    },
     {
        path: 'apps/extendlicense',
        loadChildren: './main/content/apps/extendlicenses/extendlicenses.module#FuseExtendlicensesModule',
        //canActivate: [AuthGuard]
    },
      {
        path: 'apps/consultancy/:id',
        loadChildren: './main/content/apps/consultancys/consultancys.module#FuseConsultancysModule',
        //canActivate: [AuthGuard]
    },
     {
        path: 'apps/configurerecord',
        loadChildren: './main/content/apps/configure_records/configure_records.module#FuseConfigurerecordsModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/labtest',
        loadChildren: './main/content/apps/labtest/labtest.module#FuselabtestsModule',
    },
     {
        path: 'apps/record/:id',
        loadChildren: './main/content/apps/records/records.module#FuseRecordsModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/labrecord/:id',
        loadChildren: './main/content/apps/labrecords/labrecords.module#FuselabrecordsModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/create_account',
        loadChildren: './main/content/apps/create-account/create-account.module#CreateAccountModule',
        /*  canActivate: [AuthGuard] */
    },
    /* {
        path: 'apps/create_db',
        loadChildren: './create-db/create-db.module#CreateDbModule',
    }, */
     {
        path: 'apps/print',
        loadChildren: './print/print.module#PrintModule',
    },
    {
        path: 'apps/loader',
        loadChildren: './main/content/apps/loader/loaders.module#FuseLoadersModule',
        //canActivate: [AuthGuard]
    },

    {
        path: 'apps/chat',
        loadChildren: './main/content/apps/chat/chat.module#FuseChatModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/admithistory',
        loadChildren: './main/content/apps/admit_historys/admit_historys.module#FuseAdmit_historysModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/reporthistory',
        loadChildren: './main/content/apps/report_historys/report_historys.module#FuseReport_historysModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/expense',
        loadChildren: './main/content/apps/expenses/expenses.module#FuseExpensesModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/accountants',
        loadChildren: './main/content/apps/accountants/accountants.module#FuseAccountantsModule',
        //canActivate: [AuthGuard]
    },
    /*  {
         path: 'apps/admins',
         loadChildren: './main/content/apps/admins/admins.module#FuseAccountsModule'
     }, */
    {
        path: 'apps/appointments',
        loadChildren: './main/content/apps/appointments/appointments.module#FuseAppointmentsModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/beds',
        loadChildren: './main/content/apps/beds/beds.module#FuseBedsModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/bed_allotments',
        loadChildren: './main/content/apps/bed_allotments/bed_allotments.module#FuseBed_allotmentsModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/blood_banks',
        loadChildren: './main/content/apps/blood_banks/blood_banks.module#FuseBlood_banksModule',
        //canActivate: [AuthGuard]
    },
    {

        path: 'apps/blood_donors',
        loadChildren: './main/content/apps/blood_donors/blood_donors.module#FuseBlood_donorsModule',
        //canActivate: [AuthGuard]
    },
    /*  {
         path: 'apps/ci_sessions',
         loadChildren: './main/content/apps/ci_sessions/ci_sessions.module#FuseCi_sessionsModule'
     }, */
    {
        path: 'apps/currencys',
        loadChildren: './main/content/apps/currencys/currencys.module#FuseCurrencysModule',
        //canActivate: [AuthGuard]
    },
    {

        path: 'apps/departments',
        loadChildren: './main/content/apps/departments/departments.module#FuseDepartmentsModule',
        //canActivate: [AuthGuard]
    }, 
    {
        path: 'apps/diagnosis_reports',
        loadChildren: './main/content/apps/diagnosis_reports/diagnosis_reports.module#FuseDiagnosis_reportsModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/receipts',
        loadChildren: './main/content/apps/receipts/receipts.module#FuseReceiptsModule',
        //canActivate: [AuthGuard]
    },
   {
        path: 'apps/staffs',
        loadChildren: './main/content/apps/staffs/staffs.module#FuseStaffsModule',
        //canActivate: [AuthGuard]
    }, 
    {
        path: 'apps/email_templates',
        loadChildren: './main/content/apps/email_templates/email_templates.module#FuseEmail_templatesModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/calendar',
        loadChildren: './main/content/apps/calendar/calendar.module#FuseCalendarModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/form_elements',
        loadChildren: './main/content/apps/form_elements/form_elements.module#FuseForm_elementsModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/todo',
        loadChildren: './main/content/apps/todo/todo.module#FuseTodoModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/admins',
        loadChildren: './main/content/apps/admins/admins.module#FuseAdminsModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/file-manager',
        loadChildren: './main/content/apps/file-manager/file-manager.module#FuseFileManagerModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/laboratorists',
        loadChildren: './main/content/apps/laboratorists/laboratorists.module#FuseLaboratoristsModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/medicines',
        loadChildren: './main/content/apps/medicines/medicines.module#FuseMedicinesModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/medicine_categorys',
        loadChildren: './main/content/apps/medicine_categorys/medicine_categorys.module#FuseMedicine_categorysModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/medicine_sales',
        loadChildren: './main/content/apps/medicine_sales/medicine_sales.module#FuseMedicine_salesModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/messages',
        loadChildren: './main/content/apps/messages/messages.module#FuseMessagesModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/message_threads',
        loadChildren: './main/content/apps/message_threads/message_threads.module#FuseMessage_threadsModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/notes',
        loadChildren: './main/content/apps/notes/notes.module#FuseNotesModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/notices',
        loadChildren: './main/content/apps/notices/notices.module#FuseNoticesModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/invoicepages/:id',
        loadChildren: './main/content/apps/invoices/invoice-page.module#FuseInvoicePageModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/medicine_salepages/:id',
        loadChildren: './main/content/apps/medicine_sales/medicine_sale-page.module#FuseMedicine_salePageModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/payrollpages/:id',
        loadChildren: './main/content/apps/payrolls/payroll-page.module#FusePayrollPageModule',
        //canActivate: [AuthGuard]
    },

    {
        path: 'apps/diagnosis_reportpages/:id',
        loadChildren: './main/content/apps/diagnosis_reports/diagnosis_report-page.module#FuseDiagnosis_reportPageModule',
        //canActivate: [AuthGuard]
    },

    {
        path: 'apps/smssettings',
        loadChildren: './main/content/apps/smssettings/smssettings.module#FuseSmssettingsModule',
        //canActivate: [AuthGuard]
    },

     {
        path: 'apps/prints',
        loadChildren: './main/content/apps/smssettings/smssettings.module#FuseSmssettingsModule',
        //canActivate: [AuthGuard]
    },

    {
        path: 'apps/smss',
        loadChildren: './main/content/apps/smss/smss.module#FuseSmssModule',
        ///canActivate: [AuthGuard]
    },

    {
        path: 'apps/accounts',
        loadChildren: './main/content/apps/accounts/accounts.module#FuseAccountsModule',
        //canActivate: [AuthGuard]
    },

    {
        path: 'apps/invoices',
        loadChildren: './main/content/apps/invoices/invoices.module#FuseInvoicesModule',
        //canActivate: [AuthGuard]
    },
    /*   {
          path: 'apps/invoices2',
          loadChildren: './main/content/pages/invoices/compact2/compact.module#InvoiceCompactModule'
      }, */
    {
        path: 'apps/nurses',
        loadChildren: './main/content/apps/nurses/nurses.module#FuseNursesModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/patients',
        loadChildren: './main/content/apps/patients/patients.module#FusePatientsModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/antenatals',
        loadChildren: './main/content/apps/antenatal/antenatals.module#FuseantenatalsModule'
    },
    {
        path: 'apps/anterecords/:id',
        loadChildren: './main/content/apps/anterecords/anterecords.module#FuseanterecordsModule'
    },
    {
        path: 'apps/anteconfigures',
        loadChildren: './main/content/apps/ante_records/ante_records.module#FuseConfigureanterecordsModule'
    },
   {
        path: 'apps/doctorviews',
        loadChildren: './main/content/apps/doctorviews/doctorviews.module#FuseDoctorsModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/doctorpages/:id',
        loadChildren: './main/content/apps/doctorviews/doctorviewpage/doctorviewpages.module#FuseDoctorviewpagesModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/payrolls',
        loadChildren: './main/content/apps/payrolls/payrolls.module#FusePayrollsModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/pharmacists',
        loadChildren: './main/content/apps/pharmacists/pharmacists.module#FusePharmacistsModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/prescriptions/:id',
        loadChildren: './main/content/apps/prescriptions/prescriptions.module#FusePrescriptionsModule',
        //canActivate: [AuthGuard]
    },
   /*  {
        path: 'apps/prescriptions/:id',
        loadChildren: './main/content/apps/prescriptions/prescriptions.module#FusePrescriptionsModule',
    }, */
    {
        path: 'apps/receptionists',
        loadChildren: './main/content/apps/receptionists/receptionists.module#FuseReceptionistsModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/addappointments',
        loadChildren: './main/content/apps/addappointments/addappointments.module#FuseAddappointmentsModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/reports',
        loadChildren: './main/content/apps/reports/reports.module#FuseReportsModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/followups',
        loadChildren: './main/content/apps/followup/followups.module#FusefollowupsModule',
    },
    {
        path: 'apps/settings',
        loadChildren: './main/content/apps/settings/settings.module#FuseSettingsModule',
        //canActivate: [AuthGuard]
    },
    /*  {
       path: 'apps/seats/:id',
       loadChildren: './main/content/apps/schedules/seats.module#FuseSeatsModule'
   },  */
    {
        path: 'apps/languages',
        loadChildren: './main/content/apps/languages/languages.module#FuseLanguagesModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/lab_dashboards',
        loadChildren: './main/content/apps/lab_dashboards/lab_dashboards.module#FuseLab_dashboardsModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/blood_prices',
        loadChildren: './main/content/apps/blood_prices/blood_prices.module#FuseBlood_pricesModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/alerts',
        loadChildren: './main/content/apps/alerts/alerts.module#FusealertssModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'apps/blood_sales',
        loadChildren: './main/content/apps/blood_sales/blood_sales.module#FuseBlood_salesModule',
        //canActivate: [AuthGuard]
    },
   /*  {
        path: 'apps/logindb',
        loadChildren: './main/content/apps/logindb/logindb.module#LogindbModule',
        //canActivate: [AuthGuard]
    }, */
    {
        path: '**',
        redirectTo: 'apps/login'
    }
];

@NgModule({
    declarations: [
        AppComponent,
        /*  FuseCreateDbComponent,
         FuseLoginDBComponent */
    ],
    imports: [
        AngularWeatherWidgetModule.forRoot({
            key: 'ab7b8e124586c5c859e6e1d7d860001f',
            name: WeatherApiName.OPEN_WEATHER_MAP,
            baseUrl: 'http://api.openweathermap.org/data/2.5'
        }),
        BrowserModule,
        FroalaEditorModule.forRoot(),
        FroalaViewModule.forRoot(),
        Ng4LoadingSpinnerModule,
        HttpModule,
        HttpClientModule,
        CreateDbModule,
        //Ng2AutoCompleteModule,
        LoadingModule,
        Angular4PaystackModule,
        MdNativeDateModule,
        BrowserAnimationsModule,
        RouterModule.forRoot(appRoutes, { useHash: false }),
        SharedModule,
        CdkTableModule,
        MdAutocompleteModule,
        MdButtonModule,
        MdButtonToggleModule,
        MdCardModule,
        MdCheckboxModule,
        MdChipsModule,
        MdCoreModule,
        MdDatepickerModule,
        MdDialogModule,
        MdExpansionModule,
        MdGridListModule,
        MdIconModule,
        MdInputModule,
        MdListModule,
        MdMenuModule,
        MdPaginatorModule,
        MdProgressBarModule,
        MdProgressSpinnerModule,
        MdRadioModule,
        MdRippleModule,
        MdSelectModule,
        MdSidenavModule,
        MdSliderModule,
        MdSlideToggleModule,
        MdSnackBarModule,
        MdSortModule,
        MdTableModule,
        MdTabsModule,
        MdToolbarModule,
        MdTooltipModule,
        FormsModule,
        ReactiveFormsModule,

        InMemoryWebApiModule.forRoot(FuseFakeDbService, { delay: 125, passThruUnknownUrl: true }),

        PerfectScrollbarModule.forRoot(),

        FuseMainModule,
        //CreateDbModule,
        ProjectModule,
        FuseDepartmentsModule,
        FuseStaffsModule,
        FuseDoctorsModule,
        FuseAccountantsModule,
        FuseAccountsModule,
        FuseAddappointmentsModule,
        FuseLab_dashboardsModule,
        FuseAdminsModule,
        FuseAdmit_historysModule,
        FusealertssModule,
        FuseConfigureanterecordsModule,
        FuseantenatalsModule,
        FusePatientsModule,
        FuseRecordsModule,
        FuseanterecordsModule,
        FuseAppointmentsModule,
        FuseBed_allotmentsModule,
        FuseBedsModule,
        FuseBlood_banksModule,
        FuseBlood_donorsModule,
        FuseBlood_pricesModule,
        FuseBlood_salesModule,
        FuseConfigurerecordsModule,
        FuseConsultancysModule,
        FuseCurrencysModule,
        FuseDiagnosis_reportsModule,
        FuseExpensesModule,
        FuseExtendlicensesModule,
        FuseInsuranceshemesModule,
        FuseInsurancetypesModule,
        FuseInvoicesModule,
        FuseLaboratoristsModule,
        FuselabrecordsModule,
        FuselabtestsModule,
        FuseManagepaymentsModule,
        FuseMedicine_categorysModule,
        FuseMedicine_salesModule,
        FuseMedicinesModule,
        FuseMessage_threadsModule,
        FuseMessagesModule,
        FuseNotesModule,
        FuseNoticesModule,
        FuseNursesModule,
        FuseOfflinesModule,
        FusePayrollsModule,
        FusePharmacistsModule,
        FusePrescriptionsModule,
        FuseReceiptsModule,
        FuseReceptionistsModule,
        FuseReport_historysModule,
        FuseReportsModule,
        FuseSettingsModule,
        FuseDoctorviewpagesModule,
        FuseMailModule,
        FusefollowupsModule,
        /* FuseSmssModule, */
        FuseSmssettingsModule,
        PagesModule,
        UIModule,
        ComponentsModule
    ],
    providers: [
        FuseSplashScreenService,
        FuseConfigService,
        StaffService,
        AuthGuard,
        /* AppsService */
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {
}
