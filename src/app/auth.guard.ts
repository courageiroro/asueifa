import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UserService } from './user.service';
import { Router } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private user: UserService, private router: Router) {
    var localStorageItem = JSON.parse(localStorage.getItem('authguard'));
    console.log(localStorageItem)
    if (localStorageItem) {
      this.user.setUserLoggedIn();
    }
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.user.getUserLoggedIn()) {
      console.log(this.user.getUserLoggedIn());
      return this.user.getUserLoggedIn();
    }
  }
}
