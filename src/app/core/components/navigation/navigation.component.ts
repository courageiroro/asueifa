import { Component, ViewEncapsulation } from '@angular/core';
import { FuseNavigationService } from './navigation.service';
import { Router } from '@angular/router';

@Component({
    selector: 'fuse-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class FuseNavigationComponent {
    navigation: any[];
    empty;

    constructor(private navigationService: FuseNavigationService, private router: Router) {
        var localStorageItem = JSON.parse(localStorage.getItem('user'))

        if (!localStorageItem) {
            var userType = { usertype: null }
            localStorage.setItem('user', JSON.stringify(userType));
        }
        else { 

            this.navigation = navigationService.getNavigation();
            this.navigation = navigationService.getLNavigation();
            this.navigation = navigationService.getANavigation();
            this.navigation = navigationService.getRNavigation();
            this.navigation = navigationService.getPNavigation();
            this.navigation = navigationService.getNNavigation();
            this.navigation = navigationService.getDNavigation();
            this.navigation = navigationService.getPANavigation();
            this.navigation = navigationService.getLONavigation();


            if (localStorageItem.usertype == null) {
                this.navigation = navigationService.getLONavigation();
            }
            else if (localStorageItem.usertype == "Laboratorist") {
                this.navigation = navigationService.getLNavigation();

            } else if (localStorageItem.usertype == "Admin") {
                this.navigation = navigationService.getNavigation();
            }
            else if (localStorageItem.usertype == "Accountant") {
                this.navigation = navigationService.getANavigation();
            }
            else if (localStorageItem.usertype == "Receptionist") {
                this.navigation = navigationService.getRNavigation();
            }
            else if (localStorageItem.usertype == "Pharmacist") {
                this.navigation = navigationService.getPNavigation();
            }
            else if (localStorageItem.usertype == "Nurse") {
                this.navigation = navigationService.getNNavigation();
            }
            else if (localStorageItem.usertype == "Doctor") {
                this.navigation = navigationService.getDNavigation();
            }
            else if (localStorageItem.usertype == "Patient") {
                this.navigation = navigationService.getPANavigation();
            }
        }
    }

}
