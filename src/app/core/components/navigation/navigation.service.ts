import { EventEmitter, Injectable } from '@angular/core';
import { FuseNavigation } from '../../../navigation.model';

@Injectable()
export class FuseNavigationService {
    onNavCollapseToggled = new EventEmitter<any>();
    navigation: any[];
    navigation2: any[];
    flatNavigation: any[] = [];
    navArray: any

    constructor() {
        this.navigation = new FuseNavigation().items;
    }

    /**
     * Get navigation array
     * @returns {any[]}
     */
    getNavigation() {
        var localStorageItem = JSON.parse(localStorage.getItem('user'))
        if (localStorageItem.usertype == "Admin") {

            var navigation = new FuseNavigation().items;
            console.log(navigation)
            navigation[0].url = "apps/dashboards/project";
            navigation[2] = {};
            navigation[3].children[1] = {};
            navigation[3].children[2] = {};
            navigation[3].children[3] = {};
            navigation[9].children[1] = {};
            navigation[9].children[7].children[1] = {};
            navigation[9].children[7].children[2] = {};
            navigation[9].children[8] = {};
            navigation[9].children[9] = {};
            return navigation;
        }
    }

    getLNavigation() {
        var navigation = new FuseNavigation().items;
        navigation[0].url = "apps/lab_dashboards";
        navigation[1] = {};
        navigation[2] = {};
        navigation[3].children[0] = {};
        navigation[3].children[1] = {};
        navigation[3].children[3] = {};
        navigation[3].children[4] = {};
        /* navigation[4] = {}; */
        //navigation[6] = {};
        navigation[7].children[0] = {};
        navigation[7].children[1] = {};
        navigation[9].children[0] = {};
        navigation[9].children[1] = {};
        navigation[9].children[2] = {};
        navigation[9].children[7] = {};
        navigation[9].children[8] = {};
        navigation[9].children[9] = {};
        navigation[9].children[10] = {};
        navigation[10] = {};
        return navigation;
    }

    getANavigation() {
        var navigation = new FuseNavigation().items;
        navigation[0].url = "apps/lab_dashboards";
        navigation[1] = {};
        navigation[2] = {};
        navigation[3].children[0] = {};
        navigation[3].children[1] = {};
        navigation[3].children[2] = {};
        navigation[3].children[4] = {};
        navigation[4] = {};
        //navigation[6] = {};
        navigation[7].children[0] = {};
        navigation[9].children[0] = {};
        navigation[9].children[1] = {};
        navigation[9].children[2] = {};
        navigation[9].children[3] = {};
        navigation[9].children[4] = {};
        navigation[9].children[7] = {};
        navigation[9].children[8] = {};
        navigation[9].children[9] = {};
        navigation[9].children[10] = {};
        navigation[10] = {};
        return navigation;
    }

    getRNavigation() {
        var navigation = new FuseNavigation().items;
        navigation[0].url = "apps/lab_dashboards";
        navigation[1] = {};
        navigation[3] = {};
        //navigation[6] = {};
        navigation[7].children[0] = {};
        navigation[7].children[1] = {};
        navigation[9] = {};
        navigation[10] = {};
        return navigation;
    }

    getPNavigation() {
        var navigation = new FuseNavigation().items;
        navigation[0].url = "apps/lab_dashboards";
        navigation[1] = {};
        navigation[2] = {};
        navigation[3] = {};
        //navigation[6] = {};
        navigation[7].children[0] = {};
        navigation[7].children[1] = {};
        navigation[9].children[0] = {};
        navigation[9].children[1] = {};
        navigation[9].children[2] = {};
        navigation[9].children[3] = {};
        navigation[9].children[4] = {};
        navigation[9].children[5] = {};
        navigation[9].children[8] = {};
        navigation[9].children[9] = {};
        navigation[9].children[10] = {};
        navigation[10] = {};
        return navigation;
    }

    getNNavigation() {
        var localStorageItem = JSON.parse(localStorage.getItem('user'))
        if (localStorageItem.usertype == "Nurse") {
            var navigation = new FuseNavigation().items;
            navigation[0].url = "apps/lab_dashboards";
            navigation[1] = {};
            navigation[3] = {};
            //navigation[6] = {};
            navigation[7].children[0] = {};
            navigation[7].children[1] = {};
            navigation[9].children[0] = {};
            navigation[9].children[5] = {};
            navigation[9].children[6] = {};
            navigation[9].children[7] = {};
            navigation[9].children[8] = {};
            navigation[9].children[9] = {};
            navigation[10] = {};
            return navigation;
        }
    }

    getDNavigation() {
        var navigation = new FuseNavigation().items;
        navigation[0].url = "apps/addappointments";
        navigation[1] = {};
        navigation[3].children[0] = {};
        navigation[3].children[2] = {};
        navigation[3].children[3] = {};
        //navigation[6] = {};
        navigation[7].children[0] = {};
        navigation[7].children[1] = {};
        navigation[9].children[0] = {};
        navigation[9].children[1] = {};
        navigation[9].children[5] = {};
        navigation[9].children[6] = {};
        navigation[9].children[7] = {};
        navigation[9].children[8] = {};
        navigation[9].children[9] = {};
        navigation[10] = {};
        return navigation;
    }

    getPANavigation() {
        var navigation = new FuseNavigation().items;
        navigation[0].url = "apps/lab_dashboards";
        navigation[1] = {};
        navigation[2].children[0] = {};
        navigation[3].children[0] = {};
        navigation[3].children[1] = {};
        navigation[3].children[2] = {};
        navigation[3].children[3] = {};
        navigation[4] = {};
        //navigation[6] = {};
        navigation[7].children[0] = {};
        navigation[7].children[2] = {};
        navigation[9].children[0] = {};
        navigation[9].children[1] = {};
        navigation[9].children[2] = {};
        navigation[9].children[5] = {};
        navigation[9].children[6] = {};
        navigation[9].children[7] = {};
        navigation[9].children[8] = {};
        navigation[10] = {};
        return navigation;
    }

    getLONavigation() {
        var navigation;
        //navigation[0].url = "apps/login";
        return this.flatNavigation
    }


    /**
     * Get flattened navigation array
     * @param navigationItems
     * @returns {any[]}
     */
    getFlatNavigation(navigationItems?) {
        if (!navigationItems) {
            navigationItems = this.navigation;
        }

        for (const navItem of navigationItems) {
            if (navItem.type === 'subheader') {
                continue;
            }

            if (navItem.type === 'nav-item') {
                this.flatNavigation.push({
                    title: navItem.title,
                    type: navItem.type,
                    icon: navItem.icon || false,
                    url: navItem.url
                });

                continue;
            }

            if (navItem.type === 'nav-collapse') {
                this.getFlatNavigation(navItem.children);
            }
        }

        return this.flatNavigation;
    }
}
