import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg'
import { FuseConfigService } from './../core/services/config.service';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs/Rx';
/* import { AppsService } from '../app.service'; */
import { Http, Headers, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Subject } from "rxjs/Subject";
//import { SettingsService } from '../main/content/apps/settings/settings.service';
import { ngResource } from 'angular-resource'
//import 'rxjs/Rx';

@Component({
    selector: 'fuse-create-db',
    templateUrl: './create-db.component.html',
    styleUrls: ['./create-db.component.scss']
})
export class FuseCreateDbComponent implements OnInit {
    createDbForm: FormGroup;
    createDbFormErrors: any;
    userexist = false;
    userchecking = false;
    emailexist = false;
    phone: string;
    password: string;
    confirmPassword: string;
    country: string;
    databasename: string;
    name: string;
    email: string;
    public answer;
    public secretQuestion;
    public data;
    public departmentss;
    private baseUrl = 'https://sarutech.com/apirest.php';
    public error;
    submitAttempt = false;
    public userTypes;
    online = true;
    emailchecking = false;
    usernamechange: Subject<string> = new Subject<string>();
    emailchange: Subject<string> = new Subject<string>();
    last: any = '';
    headers: any;
    public loading = false;
    //username: string;

    constructor(
        public http: Http,
        @Inject(Http) public http2: HttpClient,
        private fuseConfig: FuseConfigService,
        private formBuilder: FormBuilder,
        private router: Router,
        /* private db: AppsService, */
        //private db2: SettingsService,

    ) {

        this.usernamechange.debounceTime(2000) // wait 1 sec after the last event before emitting last event
            .subscribe(model => {
                if (this.last != model) {
                    this.userexist = false;
                    this.userchecking = true;
                    this.http.get('https://sarutech.com/couchdblogin/auth/validate-username/' + this.databasename).map(res => res.json()).subscribe(data => {
                        this.userchecking = false;
                    },
                        (err) => {
                            if (model.length > 12) {
                                model = model.slice(0, 12);
                            }
                            this.last = this.databasename = model + Math.floor(Math.random() * 1000);
                            this.userchecking = false;
                        });
                }
            });

        this.emailchange.debounceTime(2000) // wait 1 sec after the last event before emitting last event
            .subscribe(model => {
                if (this.email != undefined && this.online) {
                    this.userexist = false;
                    this.emailchecking = true;
                    this.http.get('https://sarutech.com/couchdblogin/auth/validate-email/' + this.email).map(res => res.json()).subscribe(data => {
                        this.emailexist = false;
                        this.emailchecking = false;
                    },
                        (err) => {
                            this.emailexist = true;
                            this.emailchecking = false;
                            this.createDbForm.controls.email.setErrors({ 'incorrect': true });
                        });
                }

            });

        this.fuseConfig.setSettings({
            layout: {
                navigation: 'none',
                toolbar: 'none',
                footer: 'none'
            }
        });

        this.createDbFormErrors = {
            name: {},
            databasename: {},
            phone: {},
            country: {},
            email: {},
            password: {}
        };


    }

    onDatabasenameChange(query: string) {
  
        this.databasename = this.databasename.toLowerCase();
        if (query != undefined) {
            this.usernamechange.next(query);
        }
    }

    onEmailChange(query: string) {
        this.emailchange.next(query);
    }

    navLP() {
        this.router.navigate(['/apps/logindb']);
    }


    ngOnInit() {

        this.userTypes = ['Admin']

        this.createDbForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(6)]],
            phone: ['', Validators.required],
            country: ['', Validators.required],
            databasename: ['', Validators.required],
            name: ['', Validators.required]
        });

        this.createDbForm.valueChanges.subscribe(() => {
            this.onForgotPasswordFormValuesChanged();
        });

        this.onlineCheck();
    }

    onlineCheck() {
        this.http.get('https://sarutech.com/couchdblogin/auth/validate-username/xs2da2432252sda').map(res => res.json()).subscribe(data => {
            this.online = true;
           
        },
            (err) => {
                this.online = false;
               
            });
    }

    onForgotPasswordFormValuesChanged() {
        for (const field in this.createDbFormErrors) {
            if (!this.createDbFormErrors.hasOwnProperty(field)) {
                continue;
            }

            // Clear previous errors
            this.createDbFormErrors[field] = {};

            // Get the control
            const control = this.createDbForm.get(field);

            if (control && control.dirty && !control.valid) {
                this.createDbFormErrors[field] = control.errors;
            }
        }
    }

    register() {
        this.loading = true;

        this.databasename = this.databasename.toLowerCase();
        this.submitAttempt = true;
        if (this.createDbForm.valid) {
            //this.loading.present();
            let headers = new Headers();
            headers.append('Content-Type', 'application/json');
            headers.append('Access-Control-Allow-Origin', '*');
            let user = {
                name: this.name,
                username: this.databasename,
                email: this.email,
                password: this.password,
                confirmPassword: this.password
            };

            let newuser = {
                id: '',
                rev: '',
                username: this.databasename,
                fullname: this.name,
                password: this.password,
                email: this.email,
                phone: this.phone,
                country: this.country,
                online: this.online,
                type: 'register'
            }

            if (this.online) {
                this.http.post('https://sarutech.com/couchdblogin/auth/register', JSON.stringify(user), { headers: headers })
                    .subscribe(res => {
                        let details = res.json();
                        let data = {
                            id: details.user_id,
                            database: 'hospital'
                        }
                        headers.append("Authorization", `Bearer ${details.token}:${details.password}`);
                        this.http.post('https://sarutech.com/couchdblogin/user/add-db', JSON.stringify(data), { headers: headers })
                            .subscribe(res => {
                                this.http.post('https://sarutech.com/couchdblogin/auth/login', JSON.stringify(user), { headers: headers })
                                    .subscribe(res => {
                                        let credentials = res.json();
                                        credentials.online = this.online;
                                        credentials.skip_setup = false;
                                        credentials.password = this.password;
                                        this.http.post('https://sarutech.com/apirest.php', JSON.stringify(newuser))
                                            .subscribe(res => {
                                              
                                            });
                                        /* this.db.initDB(); */
                                        this.initSettings(credentials);
                                    }, (err) => {
                                        this.error = err.json().message;
                                        //this.loading.dismiss();
                                    });
                            });
                    }, (err) => {
                        //this.loading.dismiss();
                        console.log(err.json());
                        this.error = err.json().message;
                        //this.loading.dismiss();
                    });
            }

            else {
                let credentials = {
                    user_id: newuser.username,
                    password: newuser.password,
                    online: false,
                    skip_setup: false,
                }
                /* this.db.initDB(); */
                this.initSettings(credentials);

            }
        }

    }

    initSettings(credentials) {
        /* let records = this.getRecords();
        let promises = records.map(async (record) => {
            this.db.saveConfigurerecord(record).then(record => {
            });
        });
        Promise.all(promises).then(res => {
            this.db.getSettings().then(settings => {
                if (settings[0] == undefined) {
                    console.log('storing new settings');
                    var setting = {
                        id: credentials.user_id,
                        rev: '',
                        company: this.name,
                        city: '',
                        name: this.name,
                        databasename: credentials.user_id,
                        password: credentials.password,
                        address: '',
                        country: this.country,
                        phone: this.phone,
                        email: this.email,
                        receiptmessage: '',
                        startdate: new Date().toUTCString(),
                        expiredate: null,
                        plans: [],
                        email_templates: [],
                        online: credentials.online
                    }
                  
                    this.db.saveSetting(setting).then(isetting => {
                        this.db.extendLicense(new Date().setMonth(new Date().getMonth() + 1), 'free');
                        this.router.navigate(['pages/auth/create-account']);//go home page
                       
                        this.loading = false;
                    });
                }
                else {
                    console.log('found old settings');
                }
            });
        }); */
    }
    getRecords() {
        return [
            {
                id: '',
                rev: '',
                name: 'Date of Birth',
                category: 'General',
                type: 'date'
            },
            {
                id: '',
                rev: '',
                name: 'Marital Status',
                category: 'General',
                type: 'input'
            },
            {
                id: '',
                rev: '',
                name: 'Occupation',
                category: 'General',
                type: 'input'
            },
            {
                id: '',
                rev: '',
                name: 'BMI',
                category: 'General',
                type: 'input'
            },
            {
                id: '',
                rev: '',
                name: 'Doctor',
                category: 'General',
                type: 'input'
            },
            {
                id: '',
                rev: '',
                name: 'Doctors No:',
                category: 'General',
                type: 'input'
            },
            {
                id: '',
                rev: '',
                name: 'Hospital',
                category: 'General',
                type: 'input'
            },
            {
                id: '',
                rev: '',
                name: 'Relationship',
                category: 'General',
                type: 'input'
            },
            {
                id: '',
                rev: '',
                name: 'Religion',
                category: 'General',
                type: 'input'
            },
            {
                id: '',
                rev: '',
                name: 'Allergies',
                category: 'General',
                type: 'text'
            },
            {
                id: '',
                rev: '',
                name: 'Previous Surgeries',
                category: 'Medical History',
                type: 'text'
            },
            {
                id: '',
                rev: '',
                name: 'Previous Ailments',
                category: 'Medical History',
                type: 'text'
            },
            {
                id: '',
                rev: '',
                name: 'Family History',
                category: 'Family History',
                type: 'text'
            },
            {
                id: '',
                rev: '',
                name: 'Notes',
                category: 'Notes',
                type: 'text'
            },
            {
                id: '',
                rev: '',
                name: 'OTC',
                category: 'Current Medications',
                type: 'text'
            },
            {
                id: '',
                rev: '',
                name: 'Herbal',
                category: 'Current Medications',
                type: 'text'
            },
            {
                id: '',
                rev: '',
                name: 'Prescriptions',
                category: 'Current Medications',
                type: 'text'
            },
            {
                id: '',
                rev: '',
                name: 'Diabetes',
                category: 'Medical Conditions',
                type: 'checkbox'
            },
            {
                id: '',
                rev: '',
                name: 'Hypertension',
                category: 'Medical Conditions',
                type: 'checkbox'
            },
            {
                id: '',
                rev: '',
                name: 'Asthma',
                category: 'Medical Conditions',
                type: 'checkbox'
            },
            {
                id: '',
                rev: '',
                name: 'Genotype',
                category: 'Medical Conditions',
                type: 'checkbox'
            },
            {
                id: '',
                rev: '',
                name: 'AKI',
                category: 'Medical Conditions',
                type: 'checkbox'
            },
            {
                id: '',
                rev: '',
                name: 'CKD',
                category: 'Medical Conditions',
                type: 'checkbox'
            },
            {
                id: '',
                rev: '',
                name: 'Liver Disease',
                category: 'Medical Conditions',
                type: 'checkbox'
            },
            {
                id: '',
                rev: '',
                name: 'Arthritis',
                category: 'Medical Conditions',
                type: 'checkbox'
            },
            {
                id: '',
                rev: '',
                name: 'GERD',
                category: 'Medical Conditions',
                type: 'checkbox'
            },
            {
                id: '',
                rev: '',
                name: 'Cancer',
                category: 'Medical Conditions',
                type: 'checkbox'
            },
            {
                id: '',
                rev: '',
                name: 'Gout',
                category: 'Medical Conditions',
                type: 'checkbox'
            },
            {
                id: '',
                rev: '',
                name: 'Thyroid Disorder',
                category: 'Medical Conditions',
                type: 'checkbox'
            },
            {
                id: '',
                rev: '',
                name: 'Cigarette / Tobacco',
                category: 'Social History',
                type: 'text'
            },
            {
                id: '',
                rev: '',
                name: 'Alochol',
                category: 'Social History',
                type: 'text'
            },
            {
                id: '',
                rev: '',
                name: 'Exercise',
                category: 'Social History',
                type: 'text'
            },
            {
                id: '',
                rev: '',
                name: 'Coffee',
                category: 'Social History',
                type: 'text'
            }
        ];
    }

}
