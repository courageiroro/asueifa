import { NgModule } from '@angular/core';
import { SharedModule } from './../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseCreateDbComponent } from './create-db.component';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { HttpModule } from '@angular/http';
import {HttpClientModule} from '@angular/common/http';
import { LoadingModule } from 'ngx-loading';

const routes: Routes = [
    {
        path: 'apps/create_db',
        component: FuseCreateDbComponent,
        resolve: {
            
        }
    }
];

@NgModule({
    declarations: [
       FuseCreateDbComponent
    ],
    imports: [
        SharedModule,
        HttpModule,
        RouterModule.forChild(routes),
        FroalaEditorModule.forRoot(),
        FroalaViewModule.forRoot(),
        HttpClientModule,
        LoadingModule
    ],
    providers: [

    ]
})

export class CreateDbModule {

}
