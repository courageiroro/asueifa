import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import { BehaviorSubject } from 'rxjs/Rx';
import 'rxjs/Rx';
import { Accountant } from '../accountant.model';


@Component({
    selector: 'fuse-accountants-accountant-form-dialog',
    templateUrl: './accountant-form.component.html',
    styleUrls: ['./accountant-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseAccountantsAccountantFormDialogComponent implements OnInit {

    event: CalendarEvent;
    dialogTitle: string;
    accountantForm: FormGroup;
    action: string;
    accountant: Accountant;
    bindaccountant: string;

    constructor(
        public dialogRef: MdDialogRef<FuseAccountantsAccountantFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Accountant';
            this.accountant = data.accountant;
        }
        else {
            this.dialogTitle = 'New Accountant';
            this.accountant = {
                id: '',
                rev: '',
                name: '',
                email: '',
                password: '',
                address: '',
                phone: '',
                payrolls: []
            }
        }

        this.accountantForm = this.createAccountantForm();

    }

    ngOnInit() {
    }

    createAccountantForm() {
        return this.formBuilder.group({
            id: [this.accountant.id],
            rev: [this.accountant.rev],
            name: [this.accountant.name],
            email: [this.accountant.email],
            password: [this.accountant.password],
            address: [this.accountant.address],
            phone: [this.accountant.phone]
        });
    }
}
