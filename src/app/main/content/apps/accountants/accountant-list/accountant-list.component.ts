import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AccountantsService } from '../accountants.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { FuseAccountantsAccountantFormDialogComponent } from '../accountant-form/accountant-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Accountant } from '../accountant.model';
import { PouchService } from '../../../../../provider/pouch-service';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';

@Component({
    selector: 'fuse-accountants-accountant-list',
    templateUrl: './accountant-list.component.html',
    styleUrls: ['./accountant-list.component.scss']
})
export class FuseAccountantsAccountantListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public accountants: Array<Accountant> = [];
    accountants2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'name', 'password', 'address', 'phone', 'email', 'buttons'];
    selectedAccountants: any[];
    checkboxes: {};

    dialogRef: any;


    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;

    constructor(public dialog: MdDialog, public db: PouchService) { }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        this._loadAccountants();
    }

    private _loadAccountants(): void {
        this.db.onAccountantsChanged.subscribe(data => {
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        this.db.getAccountants()
            .then((accountants: Array<Accountant>) => {
                this.accountants = accountants;
                this.accountants2 = new BehaviorSubject<any>(accountants);


                this.checkboxes = {};
                accountants.map(accountant => {
                    this.checkboxes[accountant.id] = false;
                });
                this.db.onSelectedAccountantsChanged.subscribe(selectedAccountants => {
                    for (const id in this.checkboxes) {
                        this.checkboxes[id] = selectedAccountants.includes(id);
                    }

                    this.selectedAccountants = selectedAccountants;
                });
                this.db.onSearchTextChanged.subscribe(data => {
                    this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                })



            });

    }

    newAccountant() {
        this.dialogRef = this.dialog.open(FuseAccountantsAccountantFormDialogComponent, {
            panelClass: 'accountant-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }

                var res = response.getRawValue();
                this.db.saveAccountant(res);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

            });

    }

    editAccountant(accountant) {
        this.dialogRef = this.dialog.open(FuseAccountantsAccountantFormDialogComponent, {
            panelClass: 'accountant-form-dialog',
            data: {
                accountant: accountant,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                var form = formData.getRawValue();
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':

                        this.db.updateAccountant(form);
                        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);


                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteAccountant(accountant);

                        break;
                }
            });

    }



    /**
     * Delete accountant
     */
    deleteAccountant(accountant) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deleteAccountant(accountant);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(accountantId) {
        this.db.toggleSelectedAccountant(accountantId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    accountants2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            of(this.db.accountants).delay(5000),
            this.sort.mdSortChange,
            this._paginator.page,
        ];

        //this._paginator.length = this.db.accountants.length;
        var result = Observable.fromPromise(this.db.getAccountants());
        /*                //result.subscribe(x => console.log(x), e => console.error(e));
                result.subscribe(
                    x => console.log(),
                    e => console.error(e)
                );
                return result; */

        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.accountants]));
        });
    }

    disconnect() {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
            return data;
        }

        return data.sort((a, b) => {
            const isAsc = this.sort.direction === 'asc';
            switch (this.sort.active) {
                case 'name': return compare(a.name, b.name, isAsc);
                case 'amount': return compare(+a.amount, +b.amount, isAsc);
                case 'id': return compare(+a.id, +b.id, isAsc);
                default: return 0;
            }
        });
    }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}