export interface Accountant {
	id: string,
	rev: string,
	name: string,
	email: string,
	password: string,
	address: string,
	phone: string,
	payrolls: Array<string>
}

