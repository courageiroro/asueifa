import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AccountantsService } from './accountants.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-accountants',
    templateUrl  : './accountants.component.html',
    styleUrls    : ['./accountants.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FuseAccountantsComponent implements OnInit
{
    hasSelectedAccountants: boolean;
    searchInput: FormControl;

    constructor(private accountantsService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.accountantsService.onSelectedAccountantsChanged
            .subscribe(selectedAccountants => {
                this.hasSelectedAccountants = selectedAccountants.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.accountantsService.onSearchTextChanged.next(searchText);
            });
    }

}
