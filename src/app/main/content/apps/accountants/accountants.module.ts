import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseAccountantsComponent } from './accountants.component';
import { AccountantsService } from './accountants.service';
import { FuseAccountantsAccountantListComponent } from './accountant-list/accountant-list.component';
import { FuseAccountantsSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseAccountantsAccountantFormDialogComponent } from './accountant-form/accountant-form.component';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/accountants',
        component: FuseAccountantsComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            accountants: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseAccountantsComponent,
        FuseAccountantsAccountantListComponent,
        FuseAccountantsSelectedBarComponent ,
        FuseAccountantsAccountantFormDialogComponent
    ],
    providers      : [
        AccountantsService,
        PouchService,
        AuthGuard
    ],
    entryComponents: [FuseAccountantsAccountantFormDialogComponent]
})
export class FuseAccountantsModule
{
}
