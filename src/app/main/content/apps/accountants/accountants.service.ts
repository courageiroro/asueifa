/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Accountant } from './accountant.model';
import { Schema } from '../schema';

@Injectable()
export class AccountantsService {
    public name = "";
    public sCredentials;

    onAccountantsChanged: BehaviorSubject<any> = new BehaviorSubject({});


    onSelectedAccountantsChanged: BehaviorSubject<any> = new BehaviorSubject([]);


    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    accountants: Accountant[];
    user: any;
    selectedAccountants: string[] = [];

    checkboxes: {};

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The accountants App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getAccountants()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getAccountants();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getAccountants();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        credentials = JSON.parse(localStorage.getItem('credentials'))
        this.db = new PouchDB('abayelsa', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/abayelsa';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
       //h}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        if (credentials.userDBs != undefined) {
            
            this.remote = 'http://sarutech.com:5984/abayelsa';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        }

    }

    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }

    /***********
     * accountant
     **********/
    /**
     * Save a accountant
     * @param {accountant} accountant
     *
     * @return Promise<accountant>
     */
    saveAccountant(accountant: Accountant): Promise<Accountant> {
        accountant.id = Math.floor(Date.now()).toString();

        accountant.payrolls = [];
        return this.db.rel.save('accountant', accountant)
            .then((data: any) => {
              
                if (data && data.accountants && data.accountants
                [0]) {
                    return data.accountants[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a accountant
    * @param {accountant} accountant
    *
    * @return Promise<accountant>
    */
    updateAccountant(accountant: Accountant) {
        return this.db.rel.save('accountant', accountant)
            .then((data: any) => {
                if (data && data.accountants && data.accountants
                [0]) {
                    return data.accountants[0]
                }
                return null;
            }).catch((err: any) => {
                console.error(err);
            });

    }
    /**
     * Remove a accountant
     * @param {accountant} accountant
     *
     * @return Promise<boolean>
     */
    removeAccountant(accountant: Accountant): Promise<boolean> {
        return this.db.rel.del('accountant', accountant)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getAccountants(): Promise<Array<Accountant>> {
        return this.db.rel.find('accountant')
            .then((data: any) => {
                this.accountants = data.accountants;
                if (this.searchText && this.searchText !== '') {
                    this.accountants = FuseUtils.filterArrayByString(this.accountants, this.searchText);
                }
                //this.onaccountantsChanged.next(this.accountants);
                return Promise.resolve(this.accountants);
                //return data.accountants ? data.accountants : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }
    /**
     * Read a accountant
     * @param {accountant} accountant
     *
     * @return Promise<accountant>
     */
    getAccountant(accountant: Accountant): Promise<Accountant> {
        return this.db.rel.find('accountant', accountant.id)
            .then((data: any) => {
                return data && data.accountants ? data.accountants[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Toggle selected accountant by id
     * @param id
     */
    toggleSelectedAccountant(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedAccountants.length > 0) {
            const index = this.selectedAccountants.indexOf(id);

            if (index !== -1) {
                this.selectedAccountants.splice(index, 1);

                // Trigger the next event
                this.onSelectedAccountantsChanged.next(this.selectedAccountants);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedAccountants.push(id);

        // Trigger the next event
        this.onSelectedAccountantsChanged.next(this.selectedAccountants);
    }


    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedAccountants.length > 0) {
            this.deselectAccountants();
        }
        else {
            this.selectAccountants();
        }
    }

    selectAccountants(filterParameter?, filterValue?) {
        this.selectedAccountants = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedAccountants = [];
            this.accountants.map(accountant => {
                this.selectedAccountants.push(accountant.id);
            });
        }
        else {
            /* this.selectedaccountants.push(...
                 this.accountants.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedAccountantsChanged.next(this.selectedAccountants);
    }

    deselectAccountants() {
        //console.log(this.onSelectedaccountantsChanged);
        this.selectedAccountants = [];
        //return this.selectedaccountants;

        // Trigger the next event
        this.onSelectedAccountantsChanged.next(this.selectedAccountants);
        //console.log(this.onSelectedaccountantsChanged);
    }

    deleteAccountant(accountant) {
        this.db.rel.del('accountant', accountant)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const accountantIndex = this.accountants.indexOf(accountant);
                this.accountants.splice(accountantIndex, 1);
                this.onAccountantsChanged.next(this.accountants);
            }).catch((err: any) => {
                console.error(err);
            });
    }

    deleteSelectedAccountants() {
        for (const accountantId of this.selectedAccountants) {
            const accountant = this.accountants.find(_accountant => {
                return _accountant.id === accountantId;
            });

            this.db.rel.del('accountant', accountant)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const accountantIndex = this.accountants.indexOf(accountant);
                    this.accountants.splice(accountantIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onAccountantsChanged.next(this.accountants);
        this.deselectAccountants();
    }

}
