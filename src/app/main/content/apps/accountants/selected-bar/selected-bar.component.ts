import { Component, OnInit } from '@angular/core';
import { AccountantsService } from '../accountants.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseAccountantsSelectedBarComponent implements OnInit
{
    selectedAccountants: string[];
    hasSelectedAccountants: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private db: PouchService,
        public dialog: MdDialog
    )
    {
        this.db.onSelectedAccountantsChanged
            .subscribe(selectedAccountants => {
                this.selectedAccountants = selectedAccountants;
                setTimeout(() => {
                    this.hasSelectedAccountants = selectedAccountants.length > 0;
                    this.isIndeterminate = (selectedAccountants.length !== this.db.accountants.length && selectedAccountants.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.db.selectAccountants();
    }

    deselectAll()
    {
        this.db.deselectAccountants();
    }

    deleteSelectedAccountants()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected accountants?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.db.deleteSelectedAccountants();
            }
            this.confirmDialogRef = null;
        });
    }

}
