import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Account } from '../account.model';


@Component({
    selector: 'fuse-accounts-account-form-dialog',
    templateUrl: './account-form.component.html',
    styleUrls: ['./account-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseAccountsAccountFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    accountForm: FormGroup;
    action: string;
    account: Account;

    constructor(
        public dialogRef: MdDialogRef<FuseAccountsAccountFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit account';
            this.account = data.account;
        }
        else {
            this.dialogTitle = 'New account';
            this.account = {
                id: '',
                rev: '',
                name: '',
                email: '',
                password: '',
                admins: []
            }
        }

        this.accountForm = this.createaccountForm();
    }

    ngOnInit() {
    }

    createaccountForm() {
        return this.formBuilder.group({
            id: [this.account.id],
            rev: [this.account.rev],
            name: [this.account.name],
            email: [this.account.email],
            password: [this.account.password]
        });
    }
}
