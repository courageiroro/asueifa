import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AccountsService } from '../accounts.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { FuseAccountsAccountFormDialogComponent } from '../account-form/account-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Account } from '../account.model';
import { PouchService } from '../../../../../provider/pouch-service';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';

@Component({
    selector: 'fuse-accounts-account-list',
    templateUrl: './account-list.component.html',
    styleUrls: ['./account-list.component.scss']
})
export class FuseAccountsAccountListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public accounts: Array<Account> = [];
    accounts2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'name', 'email', 'password', 'buttons'];
    selectedAccounts: any[];
    checkboxes: {};

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;

    constructor(public dialog: MdDialog, public db: PouchService) { }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        this._loadAccounts();
    }

    private _loadAccounts(): void {
        this.db.onAccountsChanged.subscribe(data => {
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        this.db.getAccounts()
            .then((accounts: Array<Account>) => {
                this.accounts = accounts;
                this.accounts2 = new BehaviorSubject<any>(accounts);

                this.checkboxes = {};
                accounts.map(account => {
                    this.checkboxes[account.id] = false;
                });
                this.db.onSelectedAccountsChanged.subscribe(selectedAccounts => {
                    for (const id in this.checkboxes) {
                        this.checkboxes[id] = selectedAccounts.includes(id);
                    }

                    this.selectedAccounts = selectedAccounts;
                });
                this.db.onSearchTextChanged.subscribe(data => {
                    this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                })
            });

    }

    newAccount() {
        this.dialogRef = this.dialog.open(FuseAccountsAccountFormDialogComponent, {
            panelClass: 'account-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }

                this.db.saveAccount(response.getRawValue());
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

            });

    }

    editAccount(account) {
        this.dialogRef = this.dialog.open(FuseAccountsAccountFormDialogComponent, {
            panelClass: 'account-form-dialog',
            data: {
                account: account,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':

                        this.db.updateAccount(formData.getRawValue());
                        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteAccount(account);

                        break;
                }
            });
    }

    /**
     * Delete account
     */
    deleteAccount(account) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deleteAccount(account);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(accountId) {
        this.db.toggleSelectedAccount(accountId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    accounts2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            of(this.db.accounts).delay(5000),
            this.sort.mdSortChange,
            this._paginator.page,
        ];

        //this._paginator.length = this.db.accounts.length;
        var result = Observable.fromPromise(this.db.getAccounts());
        /*   result.subscribe(x => console.log(x), e => console.error(e));
         return result;   */

        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.accounts]));
        });
    }

    disconnect() {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
            return data;
        }

        return data.sort((a, b) => {
            const isAsc = this.sort.direction === 'asc';
            switch (this.sort.active) {
                case 'name': return compare(a.name, b.name, isAsc);
                case 'amount': return compare(+a.amount, +b.amount, isAsc);
                case 'id': return compare(+a.id, +b.id, isAsc);
                default: return 0;
            }
        });
    }

}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
