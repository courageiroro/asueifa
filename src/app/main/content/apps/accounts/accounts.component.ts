import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AccountsService } from './accounts.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-accounts',
    templateUrl  : './accounts.component.html',
    styleUrls    : ['./accounts.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FuseAccountsComponent implements OnInit
{
    hasSelectedAccounts: boolean;
    searchInput: FormControl;

    constructor(private accountsService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.accountsService.onSelectedAccountsChanged
            .subscribe(selectedAccounts => {
                this.hasSelectedAccounts = selectedAccounts.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.accountsService.onSearchTextChanged.next(searchText);
            });
    }

}
