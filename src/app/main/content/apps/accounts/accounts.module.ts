import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseAccountsComponent } from './accounts.component';
import { AccountsService } from './accounts.service';
import { FuseAccountsAccountListComponent } from './account-list/account-list.component';
import { FuseAccountsSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseAccountsAccountFormDialogComponent } from './account-form/account-form.component';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/accounts',
        component: FuseAccountsComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            accounts: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseAccountsComponent,
        FuseAccountsAccountListComponent,
        FuseAccountsSelectedBarComponent,
        FuseAccountsAccountFormDialogComponent
    ],
    providers      : [
        AccountsService,
        PouchService,
        AuthGuard
    ],
    entryComponents: [FuseAccountsAccountFormDialogComponent]
})
export class FuseAccountsModule
{
}
