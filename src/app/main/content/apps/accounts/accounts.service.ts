/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Account } from './account.model';
import { Schema } from '../schema';

@Injectable()
export class AccountsService {
    public name = "";
    public sCredentials;

    onAccountsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedAccountsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    accounts: Account[];
    user: any;
    selectedAccounts: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The accounts App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getAccounts()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getAccounts();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getAccounts();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('abayelsa', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/abayelsa';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/abayelsa';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }

    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }

    /***********
     * account
     **********/
    /**
     * Save a account
     * @param {account} account
     *
     * @return Promise<account>
     */
    saveAccount(account: Account): Promise<Account> {
        account.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('account', account)
            .then((data: any) => {

                if (data && data.accounts && data.accounts
                [0]) {
                    return data.accounts[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a account
    * @param {account} account
    *
    * @return Promise<account>
    */
    updateAccount(account: Account) {

        return this.db.rel.save('account', account)
            .then((data: any) => {
                if (data && data.accounts && data.accounts
                [0]) {
                    return data.accounts[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a account
     * @param {account} account
     *
     * @return Promise<boolean>
     */
    removeAccount(account: Account): Promise<boolean> {
        return this.db.rel.del('account', account)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the accounts
     *
     * @return Promise<Array<account>>
     */
    getAccounts(): Promise<Array<Account>> {
        return this.db.rel.find('account')
            .then((data: any) => {
                this.accounts = data.accounts;
                if (this.searchText && this.searchText !== '') {
                    this.accounts = FuseUtils.filterArrayByString(this.accounts, this.searchText);
                }
                //this.onaccountsChanged.next(this.accounts);
                return Promise.resolve(this.accounts);
                //return data.accounts ? data.accounts : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a account
     * @param {account} account
     *
     * @return Promise<account>
     */
    getAccount(account: Account): Promise<Account> {
        return this.db.rel.find('account', account.id)
            .then((data: any) => {
                return data && data.accounts ? data.accounts[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected account by id
     * @param id
     */
    toggleSelectedAccount(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedAccounts.length > 0) {
            const index = this.selectedAccounts.indexOf(id);

            if (index !== -1) {
                this.selectedAccounts.splice(index, 1);

                // Trigger the next event
                this.onSelectedAccountsChanged.next(this.selectedAccounts);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedAccounts.push(id);

        // Trigger the next event
        this.onSelectedAccountsChanged.next(this.selectedAccounts);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedAccounts.length > 0) {
            this.deselectAccounts();
        }
        else {
            this.selectAccounts();
        }
    }

    selectAccounts(filterParameter?, filterValue?) {
        this.selectedAccounts = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedAccounts = [];
            this.accounts.map(account => {
                this.selectedAccounts.push(account.id);
            });
        }
        else {
            /* this.selectedaccounts.push(...
                 this.accounts.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedAccountsChanged.next(this.selectedAccounts);
    }





    deselectAccounts() {
        this.selectedAccounts = [];

        // Trigger the next event
        this.onSelectedAccountsChanged.next(this.selectedAccounts);
    }

    deleteAccount(account) {
        this.db.rel.del('account', account)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const accountIndex = this.accounts.indexOf(account);
                this.accounts.splice(accountIndex, 1);
                this.onAccountsChanged.next(this.accounts);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedAccounts() {
        for (const accountId of this.selectedAccounts) {
            const account = this.accounts.find(_account => {
                return _account.id === accountId;
            });

            this.db.rel.del('account', account)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const accountIndex = this.accounts.indexOf(account);
                    this.accounts.splice(accountIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onAccountsChanged.next(this.accounts);
        this.deselectAccounts();
    }
}
