import { Component, OnInit } from '@angular/core';
import { AccountsService } from '../accounts.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseAccountsSelectedBarComponent implements OnInit
{
    selectedAccounts: string[];
    hasSelectedAccounts: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private accountsService: PouchService,
        public dialog: MdDialog
    )
    {
        this.accountsService.onSelectedAccountsChanged
            .subscribe(selectedAccounts => {
                this.selectedAccounts = selectedAccounts;
                setTimeout(() => {
                    this.hasSelectedAccounts = selectedAccounts.length > 0;
                    this.isIndeterminate = (selectedAccounts.length !== this.accountsService.accounts.length && selectedAccounts.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.accountsService.selectAccounts();
    }

    deselectAll()
    {
        this.accountsService.deselectAccounts();
    }

    deleteSelectedAccounts()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected accounts?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.accountsService.deleteSelectedAccounts();
            }
            this.confirmDialogRef = null;
        });
    }

}
