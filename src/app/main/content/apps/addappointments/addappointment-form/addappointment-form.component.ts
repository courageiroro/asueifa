import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent, CalendarEvent2 } from 'angular-calendar';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Appointment } from '../addappointment.model';
import { MatColors } from '../../../../../core/matColors';
import 'rxjs/Rx';
import { ColorPickerService } from 'angular4-color-picker';
import { StaffsService } from '../../staffs/staffs.service';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-addappointment-addappointment-form-dialog',
    templateUrl: './addappointment-form.component.html',
    styleUrls: ['./addappointment-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseAddappointmentsAddappointmentFormDialogComponent implements OnInit {
    //private color: string = "#127bdc";

    addappointments: CalendarEvent2;
    addappointment: Appointment;
    dialogTitle: string;
    addappointmentForm: FormGroup;
    action: string;
    presetColors = MatColors.presets;
    patientss: any;
    doctorss: any;
    statuss: any;
    localStorageItem: any;
    public nameId: string;
    public patientId: string;

    constructor(
        public dialogRef: MdDialogRef<FuseAddappointmentsAddappointmentFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        private cpService: ColorPickerService,
        private db: PouchService
    ) {
        this.localStorageItem = JSON.parse(localStorage.getItem('user'));
        this.addappointments = data.addappointments;
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Appointment';
            this.addappointments = data.addappointments;
        }
        else {
            this.dialogTitle = 'New Appointment';
            this.addappointments = new Appointment({
                id: data.id,
                rev: data.rev,
                start: data.date,
                end: data.date

            })
        }
        db.getPStaffs().then(res => {
            this.patientss = res;
        })
         if (this.localStorageItem.usertype === "Doctor") {
          this.doctorss = [{name:this.localStorageItem.name,id:this.localStorageItem.id}]
        }
        else{ 
          db.getDStaffs().then(res => {
                this.doctorss = res;
            })
        }

        this.addappointmentForm = this.createAddappointmentForm();
    }

    ngOnInit() {
        this.statuss = ['Available', 'Booked'];
    }

    getPId(patient) {
        
        this.addappointments.patientId = patient.id;
    }

    getDId(doctor) {
        
        this.addappointments.nameId = doctor.id;
    }


    createAddappointmentForm() {
        return new FormGroup({
            id: new FormControl(this.addappointments.id),
            rev: new FormControl(this.addappointments.rev),
            title: new FormControl(this.addappointments.title),
            start: new FormControl(this.addappointments.start),
            end: new FormControl(this.addappointments.end),
            doctor_id: new FormControl(this.addappointments.doctor_id),
            patient_id: new FormControl(this.addappointments.patient_id),
            startTime: new FormControl(this.addappointments.startTime),
            endTime: new FormControl(this.addappointments.endTime),
            status: new FormControl(this.addappointments.status),
            nameId: new FormControl(this.addappointments.nameId),
            patientId: new FormControl(this.addappointments.patientId),
            description: new FormControl(this.addappointments.description),
            color: this.formBuilder.group({
                primary: new FormControl(this.addappointments.color.primary),
                secondary: new FormControl(this.addappointments.color.secondary)
            }),
        });
    }
}
