import { startOfDay, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth, addHours } from 'date-fns';
import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseAddappointmentsAddappointmentFormDialogComponent } from './addappointment-form/addappointment-form.component';
import { FormGroup } from '@angular/forms';
import { Appointment } from './addappointment.model';
import { AddappointmentsService } from './addappointments.service';
import {
    CalendarEvent2,
    CalendarEvent,
    CalendarEventAction,
    CalendarEventTimesChangedEvent,
    CalendarMonthViewDay
} from 'angular-calendar';
import { FuseConfirmDialogComponent } from '../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector: 'fuse-addappointment',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './addappointment.component.html',
    styleUrls: ['./addappointment.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class FuseAddappointmentComponent implements OnInit {
    view: string;
    public localStorageItem: any;
    public localStorageType: any;
    viewDate: Date;
    getAddappointments: any;
    addappointment: Appointment;

    addappointmentss: CalendarEvent[];

    public actions: CalendarEventAction[];

    activeDayIsOpen: boolean;

    refresh: Subject<any> = new Subject();

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    selectedDay: any;

    constructor(
        public dialog: MdDialog,
        public addappointmentService: PouchService
    ) {
        this.localStorageItem = JSON.parse(localStorage.getItem('user'));
        this.localStorageType = this.localStorageItem.usertype;
        /*  this.noticeService.getNotices().then(res=>{
            this.getNotices = res;
        }) */

        this.view = 'month';
        this.viewDate = new Date();
        this.activeDayIsOpen = true;
        this.selectedDay = { date: startOfDay(new Date()) };

        this.actions = [
            {
                label: '<i class="material-icons s-16">edit</i>',
                onClick: ({ event2 }: { event2: CalendarEvent2 }): void => {
                    this.editAddappointment('edit', event2);
                }
            },
            {
                label: '<i class="material-icons s-16">delete</i>',
                onClick: ({ event2 }: { event2: CalendarEvent2 }): void => {
                    this.deleteAddappointment(event);
                }
            }
        ];

        /**
         * Get events from service/server
         */
        this.setAddappointments();
    }

    ngOnInit() {
        /**
         * Watch re-render-refresh for updating db
         */
        this.refresh.subscribe(updateDB => {
            //console.warn('REFRESH');
            if (updateDB) {
                //console.warn('UPDATE DB');
                this.addappointmentService.updateAddappointment(this.addappointment);
            }
        });

        this.addappointmentService.onEventsUpdated.subscribe(events => {
            this.setAddappointments();
            this.refresh.next();
        });

    }


    setAddappointments() {
        //console.log(this.getNotices);
        this.addappointmentss = this.addappointmentService.addappointments.map(item => {
            item.actions = this.actions;
            
            return new Appointment(item);
        });
    }

    /**
     * Before View Renderer
     * @param {any} header
     * @param {any} body
     */
    beforeMonthViewRender({ header, body }) {
        // console.info('beforeMonthViewRender');
        /**
         * Get the selected day
         */
        const _selectedDay = body.find((_day) => {
            return _day.date.getTime() === this.selectedDay.date.getTime();
        });

        if (_selectedDay) {
            /**
             * Set selectedday style
             * @type {string}
             */
            _selectedDay.cssClass = 'mat-elevation-z3';
        }

    }

    /**
     * Day clicked
     * @param {MonthViewDay} day
     */
    dayClicked(day: CalendarMonthViewDay): void {
        const date: Date = day.date;
        const events: CalendarEvent2[] = day.events2;

        if (isSameMonth(date, this.viewDate)) {
            if ((isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) || events.length === 0) {
                this.activeDayIsOpen = false;
            }
            else {
                this.activeDayIsOpen = true;
                this.viewDate = date;
            }
        }
        this.selectedDay = day;
        this.refresh.next();
    }

    /**
     * Event times changed
     * Event dropped or resized
     * @param {CalendarEvent} event
     * @param {Date} newStart
     * @param {Date} newEnd
     */
    eventTimesChanged({ event, newStart, newEnd }: CalendarEventTimesChangedEvent): void {
        event.start = newStart;
        event.end = newEnd;
        // console.warn('Dropped or resized', event);
        this.refresh.next(true);
    }

    /**
     * Delete Event
     * @param event
     */
    deleteAddappointment(addappointment) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.addappointmentService.deleteAddappointment(addappointment);
                const addappointmentIndex = this.addappointmentss.indexOf(addappointment);
                this.addappointmentss.splice(addappointmentIndex, 1);
                this.refresh.next(true);
            }
            this.confirmDialogRef = null;
        });

    }

    /**
     * Edit Event
     * @param {string} action
     * @param {CalendarEvent} event
     */
    editAddappointment(action: string, addappointments: CalendarEvent2) {
        const eventIndex = this.addappointmentss.indexOf(addappointments);
        
        this.dialogRef = this.dialog.open(FuseAddappointmentsAddappointmentFormDialogComponent, {
            panelClass: 'event-form-dialog',
            data: {
                addappointments: addappointments,
                action: action

            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':
                        this.addappointmentss[eventIndex] = Object.assign(this.addappointmentss[eventIndex], formData.getRawValue());
                        this.addappointmentService.updateAddappointments(formData.getRawValue());
                        this.refresh.next(true);
                     

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteAddappointment(event);

                        break;
                }
            });
    }

    /**
     * Add Event
     */
    addAddappointment(): void {
        this.dialogRef = this.dialog.open(FuseAddappointmentsAddappointmentFormDialogComponent, {
            panelClass: 'event-form-dialog',
            data: {
                action: 'new',
                date: this.selectedDay.date
            }
        });
        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
                const newAddappointment = response.getRawValue();
                /*  newNotice.actions = this.actions; */
                this.addappointmentss.push(newAddappointment);
                this.addappointmentService.saveAddappointment(newAddappointment);
                
                this.refresh.next(true);
            });
    }
}


