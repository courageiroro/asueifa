import {
    CalendarEventAction
} from 'angular-calendar';

import {
    startOfDay,
    endOfDay,
    subDays,
    addDays,
    endOfMonth,
    isSameDay,
    isSameMonth,
    addHours
} from 'date-fns';

export class Appointment {
    id: string;
    nameId: string;
    patientId: string;
    rev: string;
    title: string;
    description: string;
    start: Date;
    end: Date;
    startTime: string;
    endTime: string;
    color: {
        primary: string;
        secondary: string;
    };
    actions?: CalendarEventAction[];
    cssClass?: string;
    resizable?: {
        beforeStart?: boolean;
        afterEnd?: boolean;
    };
    draggable?: boolean;
    noticeboards: Array<string>;
    doctor_id: string;
    patient_id: string;
    status: string;

    constructor(data?) {
        data = data || {};
        this.id = data.id || '';
        this.nameId = data.nameId || '';
        this.patientId = data.patientId || '';
        this.rev = data.rev || '';
        this.start = new Date(data.start) || startOfDay(new Date());
        this.end = new Date(data.end) || endOfDay(new Date());
        this.title = data.title || '';
        this.status = data.status || '';
        this.startTime = data.startTime || '';
        this.endTime = data.endTime || '';
        this.description = data.description || '';
        this.noticeboards = data.noticeboards || [];
        this.doctor_id = data.doctor_id || [];
        this.patient_id = data.patient_id || [];
        this.actions = data.actions || [];
        this.cssClass = data.cssClass || '';
        this.resizable = {
            beforeStart: data.resizable && data.resizable.beforeStart || true,
            afterEnd: data.resizable && data.resizable.afterEnd || true
        };
        this.draggable = data.draggable || true;
        this.color = {
            primary: data.color && data.color.primary || '#1e90ff',
            secondary: data.color && data.color.secondary || '#D1E8FF'
        };
    }

}


