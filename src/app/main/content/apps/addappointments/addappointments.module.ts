import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseAddappointmentComponent } from './addappointment.component';
import { AddappointmentsService } from './addappointments.service';
import { FuseAddappointmentsAddappointmentFormDialogComponent } from './addappointment-form/addappointment-form.component';
//import { FuseNoticesNoticeListComponent } from './notice-list/notice-list.component';
import { FuseAddappointmentsSelectedBarComponent } from './selected-bar/selected-bar.component';
//import { FuseNoticesNoticeFormDialogComponent } from './notice-form/notice-form.component';
import { CalendarModule } from 'angular-calendar';
import {ColorPickerModule} from 'angular4-color-picker';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/addappointments',
        component: FuseAddappointmentComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            addappointments: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes),
        CalendarModule.forRoot(),
        ColorPickerModule
    ],
    declarations   : [
        FuseAddappointmentsAddappointmentFormDialogComponent,
        FuseAddappointmentComponent,
        FuseAddappointmentsSelectedBarComponent,
        FuseAddappointmentsAddappointmentFormDialogComponent
    ],
    providers      : [
        AddappointmentsService,
        PouchService,
        AuthGuard
    ],
    entryComponents: [FuseAddappointmentsAddappointmentFormDialogComponent]
})
export class FuseAddappointmentsModule
{
}
