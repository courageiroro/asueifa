/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Appointment } from './addappointment.model';
import { Schema } from '../schema';

@Injectable()
export class AddappointmentsService {
    public addappointmentname = "";
    addappointmentss: any;
    onAddappointmentsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedAddappointmentsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    addappointments: Appointment[];
    user: any;
    selectedAddappointments: string[] = [];
    onEventsUpdated = new Subject<any>();
    searchText: string;
    filterBy: string;
    public sCredentials;
    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The notices App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getAddappointments()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getAddappointments();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getAddappointments();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('abayelsa', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/abayelsa';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/abayelsa';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }


    /***********
     * addappointment
     **********/
    /**
     * Save a addappointment
     * @param {addappointment} addappointment
     *
     * @return Promise<addappointment>
     */
    saveAddappointment(appointment: Appointment): Promise<Appointment> {
        appointment.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('appointment', appointment)
            .then((data: any) => {

                if (data && data.appointments && data.appointments
                [0]) {

                    return data.appointments[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a appointment
    * @param {appointment} appointment
    *
    * @return Promise<appointment>
    */
    updateAddappointment(appointment: Appointment) {

        return this.db.rel.save('appointment', appointment)
            .then((data: any) => {
                if (data && data.appointments && data.appointments
                [0]) {

                    return data.appointments[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    updateAddappointments(appointment: Promise<Array<Appointment>>) {

        return this.db.rel.save('appointment', appointment)
            .then((data: any) => {
                if (data && data.appointments && data.appointments
                [0]) {
                    return data.appointments[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a addappointment
     * @param {addappointment} addappointment
     *
     * @return Promise<boolean>
     */
    removeAddappointment(appointment: Appointment): Promise<boolean> {
        return this.db.rel.del('appointment', appointment)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the addappointments
     *
     * @return Promise<Array<addappointment>>
     */
    getAddappointments(): Promise<Array<Appointment>> {
        var localStorageItem = JSON.parse(localStorage.getItem('user'));
        var userType = localStorageItem.usertype;

        return this.db.rel.find('appointment')
            .then((data: any) => {
                this.addappointments = data.appointments;

                this.onEventsUpdated.next(this.addappointments);
                if (this.searchText && this.searchText !== '') {
                    this.addappointments = FuseUtils.filterArrayByString(this.addappointments, this.searchText);
                }
                //this.onnoticesChanged.next(this.addappointments);
                if (userType === "Doctor") {
                    var userId = localStorageItem.id
                    this.addappointments = this.addappointments.filter(data => data.nameId == userId)

                }
                else if (userType === "Patient") {
                    var userId = localStorageItem.id
                    this.addappointments = this.addappointments.filter(data => data.patientId == userId)

                }

                return Promise.resolve(this.addappointments);

                //return data.addappointments ? data.addappointments : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Read a addappointment
     * @param {addappointment} addappointment
     *
     * @return Promise<addappointment>
     */
    getAddappointment(appointment: Appointment): Promise<Appointment> {
        return this.db.rel.find('appointment', appointment.id)
            .then((data: any) => {
                //this.onEventsUpdated.next(this.notices);
                return data && data.appointments ? data.appointments[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected addappointment by id
     * @param id
     */
    toggleSelectedAddappointment(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedAddappointments.length > 0) {
            const index = this.selectedAddappointments.indexOf(id);

            if (index !== -1) {
                this.selectedAddappointments.splice(index, 1);

                // Trigger the next event
                this.onSelectedAddappointmentsChanged.next(this.selectedAddappointments);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedAddappointments.push(id);

        // Trigger the next event
        this.onSelectedAddappointmentsChanged.next(this.selectedAddappointments);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedAddappointments.length > 0) {
            this.deselectAddappointments();
        }
        else {
            this.selectAddappointments();
        }
    }

    selectAddappointments(filterParameter?, filterValue?) {
        this.selectedAddappointments = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedAddappointments = [];
            this.addappointments.map(appointment => {
                this.selectedAddappointments.push(appointment.id);
            });
        }
        else {
            /* this.selectedaddappointments.push(...
                 this.addappointments.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedAddappointmentsChanged.next(this.selectedAddappointments);
    }





    deselectAddappointments() {
        this.selectedAddappointments = [];

        // Trigger the next event
        this.onSelectedAddappointmentsChanged.next(this.selectedAddappointments);
    }

    deleteAddappointment(appointment) {
        this.db.rel.del('appointment', appointment)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const addappointmentIndex = this.addappointments.indexOf(appointment);
                this.addappointments.splice(addappointmentIndex, 1);
                this.onAddappointmentsChanged.next(this.addappointments);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedAddappointments() {
        for (const addappointmentId of this.selectedAddappointments) {
            const addappointment = this.addappointments.find(_addappointment => {
                return _addappointment.id === addappointmentId;
            });

            this.db.rel.del('appointment', addappointment)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const addappointmentIndex = this.addappointments.indexOf(addappointment);
                    this.addappointments.splice(addappointmentIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onAddappointmentsChanged.next(this.addappointments);
        this.deselectAddappointments();
    }
}
