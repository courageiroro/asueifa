/* import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder,FormControl, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Notice } from '../notice.model';

@Component({
    selector: 'fuse-notices-notice-form-dialog',
    templateUrl: './notice-form.component.html',
    styleUrls: ['./notice-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseNoticesNoticeFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    noticeForm: FormGroup;
    action: string;
    notice: Notice;

    constructor(
        public dialogRef: MdDialogRef<FuseNoticesNoticeFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Bus Type';
            this.notice = data.notice;
        }
        else {
            this.dialogTitle = 'New Bus Type';
            this.notice = {
                id: '',
                rev: '',
                title: '',
                description: '',
                start_timestamp: new Date(),
                end_timestamp: new Date(),
                noticeboards: [],
                actions: [],
                color: { primary: '', secondary: '' }
            }
        }

        this.noticeForm = this.createNoticeForm();
    }

    ngOnInit() {
    }

    createNoticeForm() {
        return this.formBuilder.group({
            id: [this.notice.id],
            rev: [this.notice.rev],
            title: [this.notice.title],
            color: this.formBuilder.group({
                primary: new FormControl(this.event.color.primary),
                secondary: new FormControl(this.event.color.secondary)
            }),
            description: [this.notice.description],
            start_timestamp: [this.notice.start_timestamp],
            end_timestamp: [this.notice.end_timestamp],

        });
    }
}
 */