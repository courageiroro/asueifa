/* import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NoticesService } from './notices.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';

@Component({
    selector     : 'fuse-notices',
    templateUrl  : './notices.component.html',
    styleUrls    : ['./notices.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FuseNoticesComponent implements OnInit
{
    hasSelectedNotices: boolean;
    searchInput: FormControl;

    constructor(private noticesService: NoticesService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.noticesService.onSelectedNoticesChanged
            .subscribe(selectedNotices => {
                this.hasSelectedNotices = selectedNotices.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.noticesService.onSearchTextChanged.next(searchText);
            });
    }

}
 */