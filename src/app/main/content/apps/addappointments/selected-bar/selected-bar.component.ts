import { Component, OnInit } from '@angular/core';
import { AddappointmentsService } from '../addappointments.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseAddappointmentsSelectedBarComponent implements OnInit
{
    selectedAddappointments: string[];
    hasSelectedAddappointments: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private addappointmentsService: PouchService,
        public dialog: MdDialog
    )
    {
        this.addappointmentsService.onSelectedAddappointmentsChanged
            .subscribe(selectedAddappointments => {
                this.selectedAddappointments = selectedAddappointments;
                setTimeout(() => {
                    this.hasSelectedAddappointments = selectedAddappointments.length > 0;
                    this.isIndeterminate = (selectedAddappointments.length !== this.addappointmentsService.addappointments.length && selectedAddappointments.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.addappointmentsService.selectAddappointments();
    }

    deselectAll()
    {
        this.addappointmentsService.deselectAddappointments();
    }

    deleteSelectedAddappointments()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected noticees?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.addappointmentsService.deleteSelectedAddappointments();
            }
            this.confirmDialogRef = null;
        });
    }

}
