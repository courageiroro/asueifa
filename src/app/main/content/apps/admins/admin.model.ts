export interface Admin{
    id: string,
    rev: string,
    name: string,
    email: string,
    password: string,
    notes: Array<string>,
    payrolls: Array<string>,
}