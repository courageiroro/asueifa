import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AdminsService } from './admins.service';
import { AccountsService } from '../accounts/accounts.service';
import { Animations } from '../../../../core/animations';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FuseConfigService } from './../../../../core/services/config.service';
import { Account } from './../accounts/account.model';
import { BehaviorSubject } from 'rxjs/Rx';
import { StaffsService } from '../staffs/staffs.service';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector: 'fuse-admins',
    templateUrl: './admins.component.html',
    styleUrls: ['./admins.component.scss'],
    /* encapsulation: ViewEncapsulation.None,*/
    animations: [Animations.slideInTop]
})
export class FuseAdminsComponent implements OnInit {
    registerForm: FormGroup;
    registerFormErrors: any;
    localStorageItem: any;
    localStorageType: any;
    public accountss: Array<Account> = [];
    account: any;
    accounts2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    accounts: any;
    public accountId: string;
    username;
    name;
    email;
    address;
    phone;
    newPassword;
    attachment;
    confirmPassword;
    currentPassword;
    public error: any;
    image;

    constructor(private router: Router, private staffService: PouchService, private formBuilder: FormBuilder, private fuseConfig: FuseConfigService,
        public _DomSanitizer: DomSanitizer
    ) {

        /*    this.fuseConfig.setSettings({
              layout: {
                  navigation: 'none',
                  toolbar   : 'none',
                  footer    : 'none'
              }
          }); */

        document.addEventListener('click', function () {
            staffService.file = document.querySelector('#fileupload');
            var imagestring = document.querySelector('#imagestring');

        })

        this.staffService.getGStaffs().then(res => {

        })


        this.registerFormErrors = {
            name: {},
            email: {},
            address: {},
            phone: {},
            _attachments: {},
            currentpassword: {},
            password: {},
            passwordConfirm: {}
        };


    }

    ngOnInit() {
        this.localStorageItem = JSON.parse(localStorage.getItem('user'));

        this.staffService.getStaff(this.localStorageItem).then(res => {
            this.username = res._attachments;
            this.image = res.image;
            this.attachment = res._attachments;
        })


        this.registerForm = this.formBuilder.group({
            id: [this.localStorageItem.id],
            rev: [this.localStorageItem.rev],
            name: [this.localStorageItem.name, Validators.required],
            email: [this.localStorageItem.email, [Validators.required, Validators.email]],
            address: [this.localStorageItem.address, Validators.required],
            phone: [this.localStorageItem.phone, Validators.required],
            _attachments: [this.localStorageItem.attachments],
            currentpassword: [this.localStorageItem.password, Validators.required],
            password: [this.localStorageItem.password, Validators.required],
            passwordConfirm: [this.localStorageItem.password, Validators.required]
        });


        this.registerForm.valueChanges.subscribe(() => {
            this.onRegisterFormValuesChanged();
        });
    }


    update() {
        const formData: FormGroup = this.registerForm;

        this.staffService.getStaff(this.localStorageItem).then(res => {

            localStorage.setItem('user', JSON.stringify(res));
            this.localStorageItem = JSON.parse(localStorage.getItem('user'));
            res = formData.getRawValue()

            //res.password; 

            var reader = new FileReader();
            var file = this.staffService.file;
            var retrieve = (<HTMLInputElement>file).files[0];
            var file2;
            var myThis = this;

            if (retrieve == undefined) {
                res.image = this.localStorageItem.image;
                res.id = this.localStorageItem.id;
                res.rev = this.localStorageItem.rev;
                res.usertype = this.localStorageItem.usertype;
                res.department_id = this.localStorageItem.department_id;
                res.profile = this.localStorageItem.profile
                res._attachments = this.attachment;
                res.sex = this.localStorageItem.sex;
                res.birth_date = this.localStorageItem.birth_date;
                res.age = this.localStorageItem.age;
                res.blood_group = this.localStorageItem.blood_group;
                res.account_opening_timestamp = this.localStorageItem.account_opening_timestamp;

                myThis.staffService.updateStaff(res);
            }
            else {
                reader.readAsDataURL(retrieve);
                reader.onloadend = function () {
                    reader.result;

                    res.image = reader.result;
                    res.id = myThis.localStorageItem.id;
                    res.rev = myThis.localStorageItem.rev;
                    res.usertype = myThis.localStorageItem.usertype;
                    res.department_id = myThis.localStorageItem.department_id;
                    res.profile = myThis.localStorageItem.profile
                    res._attachments = myThis.attachment;
                    res.sex = myThis.localStorageItem.sex;
                    res.birth_date = myThis.localStorageItem.birth_date;
                    res.age = myThis.localStorageItem.age;
                    res.blood_group = myThis.localStorageItem.blood_group;
                    res.account_opening_timestamp = myThis.localStorageItem.account_opening_timestamp;

                    myThis.staffService.updateStaff(res);
                };
                reader.onerror = function (error) {
                    console.log('Error: ', error);
                };
            }
            /*    localStorage.clear();
               this.router.navigate(['/apps/login']); */


        })
    }

    updatePassword() {

        if (this.newPassword !== this.confirmPassword) {
            this.error = "Confirm Password does not match Password";
        }
        else if (this.localStorageItem.password !== this.currentPassword) {
            this.error = "Check Current Password";
        }
        else {
            this.staffService.updateStaff(this.localStorageItem).then(res => {

            })
        }
    }

    checkPassword() {
        if (this.localStorageItem.password !== this.currentPassword) {
            this.error = "Password entered is Incorrect";
        }
        else {
            this.error = " ";
        }
    }

    onRegisterFormValuesChanged() {
        for (const field in this.registerFormErrors) {
            if (!this.registerFormErrors.hasOwnProperty(field)) {
                continue;
            }

            // Clear previous errors
            this.registerFormErrors[field] = {};

            // Get the control
            const control = this.registerForm.get(field);

            if (control && control.dirty && !control.valid) {
                this.registerFormErrors[field] = control.errors;
            }
        }
    }

}
