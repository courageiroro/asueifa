import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseAdminsComponent } from './admins.component';
import { AdminsService } from './admins.service';
import { AccountsService } from '../accounts/accounts.service';
import { StaffsService } from '../staffs/staffs.service';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path: 'apps/admins',
        component: FuseAdminsComponent,
        canActivate: [AuthGuard],
        children: [],
        resolve: {
            admins: PouchService
        }
    }
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        FuseAdminsComponent,
    ],
    providers: [
        AdminsService,
        AccountsService,
        StaffsService,
        PouchService,
        AuthGuard
    ],
    //entryComponents: [FuseAdminsAdminFormDialogComponent]
})
export class FuseAdminsModule {
}
