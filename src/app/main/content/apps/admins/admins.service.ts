/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import { StaffsService } from '../staffs/staffs.service';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Admin } from './admin.model';
import { Schema } from '../schema';


@Injectable()
export class AdminsService {
    public admin = "";
    public sCredentials;

    onAdminsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedAdminsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    admins: Admin[];
    //salarys: Payment[];
    user: any;
    selectedAdmins: string[] = [];

    checkboxes: {};

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);

    }

    /**
     * The admins App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getAdmins()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getAdmins();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getAdmins();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('abayelsa', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/abayelsa';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/abayelsa';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }

    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }

    /***********
     * admin
     **********/
    /**
     * Save a admin
     * @param {admin} admin
     *
     * @return Promise<admin>
     */
    saveAdmin(admin: Admin): Promise<Admin> {
        admin.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('admin', admin)
            .then((data: any) => {
                if (data && data.admins && data.admins
                [0]) {

                    return data.admins[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });

    }

    /**
    * Update a admin
    * @param {admin} admin
    *
    * @return Promise<admin>
    */
    updateAdmin(admin: Admin) {

        return this.db.rel.save('admin', admin)
            .then((data: any) => {
                if (data && data.admins && data.admins
                [0]) {
                    return data.admins[0]
                }
                return null;
            }).catch((err: any) => {
                console.error(err);
            });

    }


    /**
     * Remove a admin
     * @param {admin} Admin
     *
     * @return Promise<boolean>
     */
    removeAdmin(admin: Admin): Promise<boolean> {
        return this.db.rel.del('admin', admin)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }



    getAdmins(): Promise<Array<Admin>> {
        return this.db.rel.find('admin')
            .then((data: any) => {
                this.admins = data.admins;
                if (this.searchText && this.searchText !== '') {
                    this.admins = FuseUtils.filterArrayByString(this.admins, this.searchText);
                }

                return Promise.resolve(this.admins);
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a admin
     * @param {admin} admin
     *
     * @return Promise<admin>
     */
    getAdmin(admin: Admin): Promise<Admin> {
        return this.db.rel.find('admin', admin.id)
            .then((data: any) => {
                return data && data.admins ? data.admins[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Toggle selected admin by id
     * @param id
     */
    toggleSelectedAdmin(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedAdmins.length > 0) {
            const index = this.selectedAdmins.indexOf(id);

            if (index !== -1) {
                this.selectedAdmins.splice(index, 1);

                // Trigger the next event
                this.onSelectedAdminsChanged.next(this.selectedAdmins);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedAdmins.push(id);

        // Trigger the next event
        this.onSelectedAdminsChanged.next(this.selectedAdmins);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedAdmins.length > 0) {
            this.deselectAdmins();
        }
        else {
            this.selectAdmins();
        }
    }


    selectAdmins(filterParameter?, filterValue?) {
        this.selectedAdmins = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedAdmins = [];
            this.admins.map(admin => {
                this.selectedAdmins.push(admin.id);
            });
        }


        // Trigger the next event
        this.onSelectedAdminsChanged.next(this.selectedAdmins);
    }

    deselectAdmins() {
        //console.log(this.onSelectedadmin2sChanged);
        this.selectedAdmins = [];
        //return this.selectedadmin2s;

        // Trigger the next event
        this.onSelectedAdminsChanged.next(this.selectedAdmins);
        //console.log(this.onSelectedadmin2sChanged);
    }

    deleteAdmin(admin) {
        this.db.rel.del('admin', admin)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const adminIndex = this.admins.indexOf(admin);
                this.admins.splice(adminIndex, 1);
                this.onAdminsChanged.next(this.admins);
            }).catch((err: any) => {
                console.error(err);
            });
    }

    deleteSelectedAdmins() {
        for (const adminId of this.selectedAdmins) {
            const admin = this.admins.find(_admin => {
                return _admin.id === adminId;
            });

            this.db.rel.del('admin', admin)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const adminIndex = this.admins.indexOf(admin);
                    this.admins.splice(adminIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onAdminsChanged.next(this.admins);
        this.deselectAdmins();
    }


}
