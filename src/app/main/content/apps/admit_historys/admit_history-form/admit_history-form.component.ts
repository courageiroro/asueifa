import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import { BehaviorSubject } from 'rxjs/Rx';
import 'rxjs/Rx';
import { Bed_allotment } from '../admit_history.model';
import { BedsService } from '../../beds/beds.service';
import { Bed } from '../../beds/bed.model';
import { StaffsService } from '../../staffs/staffs.service';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-admit_historys-admit_history-form-dialog',
    templateUrl: './admit_history-form.component.html',
    styleUrls: ['./admit_history-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseAdmit_historysAdmit_historyFormDialogComponent implements OnInit {
    public bedsss: Array<Bed> = [];
    bed: any;
    beds2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    event: CalendarEvent;
    dialogTitle: string;
    bed_allotmentForm: FormGroup;
    action: string;
    bed_allotment: Bed_allotment;
    patientss: any;
    bedss: any;
    bedNumber2: any;
    public bedNumber;
    public newArray;
    public newArray2;
    public bedId: string;
    public bedType: string;

    constructor(
        public dialogRef: MdDialogRef<FuseAdmit_historysAdmit_historyFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        public db: PouchService
    ) {

        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Admit History';
            this.bed_allotment = data.bed_allotment;
        }
        else {
            this.dialogTitle = 'New Admit History';
            this.bed_allotment = {
                id: '',
                rev: '',
                bed_id: '',
                bed_type: '',
                patient_id: '',
                allotment_timestamp: new Date,
                discharge_timestamp: new Date,
                bed_number: ''
            }
        }

        this.bed_allotmentForm = this.createBed_allotmentForm();

        db.getB_AllotBeds().then(res => {
            this.bedss = res;
        })

        db.getPStaffs().then(res => {
            this.patientss = res;
        })

    }

    getAId(bed) {
        this.db.bed = bed.id;
        this.bedId = bed.id
        this.bedType = bed.type
    }

    getArray(get) {
        var array = [get.bed_number].join()
        return array
    }

    click() {
        
        this.db.getBeds().then(res => {
            this.bedNumber2 = res;
            this.newArray = this.bedNumber2.map(this.getArray);
            
            for (var i = 0; i < this.newArray.length; i++) {
                this.newArray2 = this.newArray[i];
                
            }
        });
    }

    ngOnInit() {
        
    }

    createBed_allotmentForm() {
        return this.formBuilder.group({
            id: [this.bed_allotment.id],
            rev: [this.bed_allotment.rev],
            bed_id: ({
                value: this.bed_allotment.bed_id,
                var: [this.bedNumber = this.bed_allotment.bed_id]
            }),
            bed_type: ({
                value: this.bed_allotment.bed_type,
                var: [this.bedType = this.bed_allotment.bed_type]
            }),
            patient_id: [this.bed_allotment.patient_id],
            allotment_timestamp: new Date(this.bed_allotment.allotment_timestamp).toDateString(),
            discharge_timestamp: new Date(this.bed_allotment.discharge_timestamp).toDateString(),
            bed_number: [this.bed_allotment.bed_number],
        });
    }
}
