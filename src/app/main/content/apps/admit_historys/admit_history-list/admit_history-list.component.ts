import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Bed_allotmentsService } from '../../bed_allotments/bed_allotments.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { FuseAdmit_historysAdmit_historyFormDialogComponent } from '../admit_history-form/admit_history-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Bed_allotment } from '../../bed_allotments/bed_allotment.model';
import {PouchService} from '../../../../../provider/pouch-service';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';


@Component({
    selector: 'fuse-admit_historys-admit_history-list',
    templateUrl: './admit_history-list.component.html',
    styleUrls: ['./admit_history-list.component.scss']
})
export class FuseAdmit_historysAdmit_historyListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public bed_allotments: Array<Bed_allotment> = [];
    bed_allotments2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['number', 'type', 'allotment', 'discharge', 'buttons'];
    selectedBed_allotments: any[];
    checkboxes: {};
    public localStorageItem: any;
    public localStorageType: any;
    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;

    constructor(public dialog: MdDialog, public db: PouchService) {
        this.localStorageItem = JSON.parse(localStorage.getItem('user'));
        this.localStorageType = this.localStorageItem.usertype;
    }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        
        this._loadBed_allotments();

    }

    private _loadBed_allotments(): void {
        this.db.onBed_allotmentsChanged.subscribe(data => {
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        this.db.getBed_allotments()
            .then((bed_allotments: Array<Bed_allotment>) => {
                this.bed_allotments = bed_allotments;
                this.bed_allotments2 = new BehaviorSubject<any>(bed_allotments);
                

                this.checkboxes = {};
                bed_allotments.map(bed_allotment => {
                    this.checkboxes[bed_allotment.id] = false;
                });

                this.db.onSelectedBed_allotmentsChanged.subscribe(selectedBed_allotments => {
                    for (const id in this.checkboxes) {
                        this.checkboxes[id] = selectedBed_allotments.includes(id);
                    }

                    this.selectedBed_allotments = selectedBed_allotments;
                });
                this.db.onSearchTextChanged.subscribe(data => {
                    this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                })
            });

    }

    newBed_allotment() {
        this.dialogRef = this.dialog.open(FuseAdmit_historysAdmit_historyFormDialogComponent, {
            panelClass: 'bed_allotment-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }

                var res = response.getRawValue();
                res.allotment_timestamp = new Date(res.allotment_timestamp).toISOString().substring(0, 10);
                res.discharge_timestamp = new Date(res.discharge_timestamp).toISOString().substring(0, 10);
                this.db.saveBed_allotment(res);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

            });

    }

    editBed_allotment(bed_allotment) {
        this.dialogRef = this.dialog.open(FuseAdmit_historysAdmit_historyFormDialogComponent, {
            panelClass: 'bed_allotment-form-dialog',
            data: {
                bed_allotment: bed_allotment,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                var form = formData.getRawValue();
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':
                        form.allotment_timestamp = new Date(form.allotment_timestamp).toISOString().substring(0, 10);
                        form.discharge_timestamp = new Date(form.discharge_timestamp).toISOString().substring(0, 10);
                        this.db.updateBed_allotment(form);
                        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteBed_allotment(bed_allotment);

                        break;
                }
            });
    }

    /**
     * Delete bed_allotment
     */
    deleteBed_allotment(bed_allotment) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deleteBed_allotment(bed_allotment);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(bed_allotmentId) {
        this.db.toggleSelectedBed_allotment(bed_allotmentId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    bed_allotments2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            of(this.db.bed_allotments).delay(5000),
            this.sort.mdSortChange,
            this._paginator.page,
        ];

        //this._paginator.length = this.db.bed_allotments.length;
       var result = Observable.fromPromise(this.db.getBed_allotments());
       /*   result.subscribe(x => console.log(x), e => console.error(e));
        return result; */

        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.bed_allotments]));
        });
    }

    disconnect() {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
          return data;
        }
    
        return data.sort((a, b) => {
          const isAsc = this.sort.direction === 'asc';
          switch (this.sort.active) {
            case 'bed_id': return compare(a.bed_id, b.bed_id, isAsc);
            case 'amount': return compare(+a.amount, +b.amount, isAsc);
            case 'id': return compare(+a.id, +b.id, isAsc);
            default: return 0;
          }
        });
      }
    
}

 /** Simple sort comparator for example ID/Name columns (for client-side sorting). */
 function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }