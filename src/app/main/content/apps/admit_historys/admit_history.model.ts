export interface Bed_allotment
{
	id: string,
	rev: string,
	bed_id: string,
	bed_number: string,
	bed_type: string,
	patient_id: string,
	allotment_timestamp: Date,
	discharge_timestamp: Date,
}

