import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseAdmit_historysComponent } from './admit_historys.component';
import { Bed_allotmentsService } from './../bed_allotments/bed_allotments.service';
import { FuseAdmit_historysAdmit_historyListComponent } from './admit_history-list/admit_history-list.component';
import { FuseAdmit_historysSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseAdmit_historysAdmit_historyFormDialogComponent } from './admit_history-form/admit_history-form.component';
import { BedsService } from './../beds/beds.service';
import { PatientsService } from './../patients/patients.service';
import { StaffsService } from './../staffs/staffs.service';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path: 'apps/admithistory',
        component: FuseAdmit_historysComponent,
        canActivate: [AuthGuard],
        children: [],
        resolve: {
            bed_allotments: PouchService
        }
    }
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        FuseAdmit_historysComponent,
        FuseAdmit_historysAdmit_historyListComponent,
        FuseAdmit_historysSelectedBarComponent,
        FuseAdmit_historysAdmit_historyFormDialogComponent
    ],
    providers: [
        Bed_allotmentsService,
        BedsService,
        StaffsService,
        PouchService,
        AuthGuard
    ],
    entryComponents: [FuseAdmit_historysAdmit_historyFormDialogComponent]
})
export class FuseAdmit_historysModule {
}
