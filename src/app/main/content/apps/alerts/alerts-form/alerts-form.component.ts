import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { alerts } from '../alerts.model';
import { alertssService } from '../alerts.service';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-alertss-alerts-form-dialog',
    templateUrl: './alerts-form.component.html',
    styleUrls: ['./alerts-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FusealertssalertsFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    alertsForm: FormGroup;
    action: string;
    alerts: alerts;
    public categorys;
    public types;
    category

    constructor(
        public dialogRef: MdDialogRef<FusealertssalertsFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        private db: PouchService
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit alerts';
            this.alerts = data.alerts;
        }
        else {
            this.dialogTitle = 'New alerts';
            this.alerts = {
                id: '',
                rev: '',
                message: '',
                date: new Date()
            }
        }

        this.alertsForm = this.createalertsForm();
    }

    ngOnInit() {
        this.categorys = ['General', 'Medical History', 'Social History', 'Medical Conditions', 'Current Medications', 'Family History', 'Notes']
        this.types = ['input', 'text', 'checkbox']
    }

   

    createalertsForm() {
        return this.formBuilder.group({
            id: [this.alerts.id],
            rev: [this.alerts.rev],
            message: [this.alerts.message],
            date: [this.alerts.date],

        });
    }
}
