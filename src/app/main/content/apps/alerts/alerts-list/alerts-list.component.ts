import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { alertssService } from '../alerts.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { FusealertssalertsFormDialogComponent } from '../alerts-form/alerts-form.component';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { InvoicesService } from '../../invoices/invoices.service';
import { alerts } from '../alerts.model';
import { Subject } from 'rxjs/Subject';
import { Http, Headers, RequestOptions } from '@angular/http';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-alertss-alerts-list',
    templateUrl: './alerts-list.component.html',
    styleUrls: ['./alerts-list.component.scss']
})
export class FusealertssalertsListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public alertss: Array<alerts> = [];
    alertss2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    //dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'name', 'description', 'buttons'];
    selectedalertss: any[];
    online = true;
    checkboxes: {};
    general = [];
    refresh: Subject<any> = new Subject();
    medicalhistory = [];
    socialhistory = [];
    medicalconditions = [];
    currentmedications = [];
    familyhistory = [];
    notes = [];
    recordss: alerts[];
    alerts;
    invoices;

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(public dialog: MdDialog, public db: PouchService, public http: Http)
    { }

    ngOnInit() {
        //this.dataSource = new FilesDataSource(this.db);
        this._loadalertss();
    }

    private _loadalertss(): void {
        this.db.getInvoices().then(res => {
            this.alerts = res;
        })

    }


    onlineCheck() {
        this.online = true;
        this.http.get('https://sarutech.com/couchdblogin/auth/validate-username/xs2da2432252sda').subscribe(data => {
            this.online = true;
         
        },
            (err) => {
                this.online = false;
                
            });
    }

    click() {
        this.onlineCheck();
    }

    newalerts() {
        this.dialogRef = this.dialog.open(FusealertssalertsFormDialogComponent, {
            panelClass: 'alerts-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
              
                if (this.db.category == "General") {
                    this.general.push(response.getRawValue());
                }
                else if (this.db.category == "Medical History") {
                    this.medicalhistory.push(response.getRawValue());
                }
                else if (this.db.category == "Social History") {
                    this.socialhistory.push(response.getRawValue());
                }
                else if (this.db.category == "Medical Conditions") {
                    this.medicalconditions.push(response.getRawValue());
                }
                else if (this.db.category == "Current Medications") {
                    this.currentmedications.push(response.getRawValue());
                }
                else if (this.db.category == "Family History") {
                    this.familyhistory.push(response.getRawValue());
                }
                else if (this.db.category == "Notes") {
                    this.notes.push(response.getRawValue());
                }
                this.db.savealerts(response.getRawValue());
                this.refresh.next(true);
                //this.dataSource = new FilesDataSource(this.db);
                

            });

    }

    editalerts(alerts) {
        
        this.dialogRef = this.dialog.open(FusealertssalertsFormDialogComponent, {
            panelClass: 'alerts-form-dialog',
            data: {
                alerts: alerts,
                action: 'edit'

            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                //this.general.push(formData.getRawValue());
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':
                        if (alerts.category == "General") {
                            
                                alerts.id = '',
                                alerts.rev = '',
                                alerts.name = formData.getRawValue().name,
                                alerts.type = formData.getRawValue().type,
                                alerts.category = formData.getRawValue().category
                            
                        }
                        else if (alerts.category == "Medical History") {
                                alerts.id = '',
                                alerts.rev = '',
                                alerts.name = formData.getRawValue().name,
                                alerts.type = formData.getRawValue().type,
                                alerts.category = formData.getRawValue().category

                        }
                        else if (alerts.category == "Social History") {
                                alerts.id = '',
                                alerts.rev = '',
                                alerts.name = formData.getRawValue().name,
                                alerts.type = formData.getRawValue().type,
                                alerts.category = formData.getRawValue().category
                        }
                        else if (alerts.category == "Medical Conditions") {
                                alerts.id = '',
                                alerts.rev = '',
                                alerts.name = formData.getRawValue().name,
                                alerts.type = formData.getRawValue().type,
                                alerts.category = formData.getRawValue().category
                        }
                        else if (alerts.category == "Current Medications") {
                                alerts.id = '',
                                alerts.rev = '',
                                alerts.name = formData.getRawValue().name,
                                alerts.type = formData.getRawValue().type,
                                alerts.category = formData.getRawValue().category
                        }
                        else if (alerts.category == "Family History") {
                                alerts.id = '',
                                alerts.rev = '',
                                alerts.name = formData.getRawValue().name,
                                alerts.type = formData.getRawValue().type,
                                alerts.category = formData.getRawValue().category
                        }
                        else if (alerts.category == "Notes") {
                                alerts.id = '',
                                alerts.rev = '',
                                alerts.name = formData.getRawValue().name,
                                alerts.type = formData.getRawValue().type,
                                alerts.category = formData.getRawValue().category
                        }

                        this.db.updatealerts(formData.getRawValue());
                        this.refresh.next(true);
                        //this.dataSource = new FilesDataSource(this.db);
                        

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deletealerts(alerts);

                        break;
                }
            });
    }

    /**
     * Delete alerts
     */
    deletealerts(alerts) {
        
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                if (alerts.category == "General") {
                  var index =  this.general.indexOf(alerts);
                  this.general.splice(index,1);
                   
                }
                 else if (alerts.category == "Medical History") {
                              var index =  this.general.indexOf(alerts);
                              this.general.splice(index,1);
                              
                        }
                        else if (alerts.category == "Social History") {
                              var index =  this.general.indexOf(alerts);
                              this.socialhistory.splice(index,1);
                        }
                        else if (alerts.category == "Medical Conditions") {
                                var index =  this.general.indexOf(alerts);
                                this.medicalconditions.splice(index,1);
                        }
                        else if (alerts.category == "Current Medications") {
                                var index =  this.general.indexOf(alerts);
                                this.currentmedications.splice(index,1);
                        }
                        else if (alerts.category == "Family History") {
                                 var index =  this.general.indexOf(alerts);
                                 this.familyhistory.splice(index,1);
                        }
                        else if (alerts.category == "Notes") {
                                 var index =  this.general.indexOf(alerts);
                                 this.notes.splice(index,1);
                        }
                this.db.deletealerts(alerts);
                //this.dataSource = new FilesDataSource(this.db);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(alertsId) {
        this.db.toggleSelectedalerts(alertsId);
    }

}

/* export class FilesDataSource extends DataSource<any>
{
    alertss2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: alertssService) {
        super();
    }

     //Connect function called by the table to retrieve one stream containing the data to render.
    connect(): Observable<any[]> {
        var result = Observable.fromPromise(this.db.getalertss());
        result.subscribe(x => console.log(x), e => console.error(e));
        return result;
    }

    disconnect() {
    }
}
 */