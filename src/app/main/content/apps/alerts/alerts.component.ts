import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { alertssService } from './alerts.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector: 'fuse-alertss',
    templateUrl: './alerts.component.html',
    styleUrls: ['./alerts.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [Animations.slideInTop]
})
export class FusealertssComponent implements OnInit {
    hasSelectedalertss: boolean;
    searchInput: FormControl;

    constructor(private alertssService: PouchService) {
        this.searchInput = new FormControl('');
       
    }

    ngOnInit() {

        this.alertssService.onSelectedalertssChanged
            .subscribe(selectedalertss => {
                this.hasSelectedalertss = selectedalertss.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.alertssService.onSearchTextChanged.next(searchText);
            });
    }

}
