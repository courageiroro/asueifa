import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FusealertssComponent } from './alerts.component';
import { alertssService } from './alerts.service';
//import { UserService } from '../../../../user.service';
//import { alertssService2 } from './contacts.service';
import { FusealertssalertsListComponent } from './alerts-list/alerts-list.component';
import { FusealertssSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FusealertssalertsFormDialogComponent } from './alerts-form/alerts-form.component';
import { AuthGuard } from 'app/auth.guard';
import { InvoicesService } from './../invoices/invoices.service';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/alerts',
        component: FusealertssComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            alertss: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FusealertssComponent,
        FusealertssalertsListComponent,
        FusealertssSelectedBarComponent,
        FusealertssalertsFormDialogComponent
    ],
    providers      : [
        alertssService,
        AuthGuard,
        InvoicesService,
        PouchService
       //UserService
    ],
    entryComponents: [FusealertssalertsFormDialogComponent]
})
export class FusealertssModule
{
}
