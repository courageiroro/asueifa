/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { alerts } from './alerts.model';
import { Schema } from '../schema';

@Injectable()
export class alertssService {
    public alerts = "";
    public sCredentials;
    public category;
    onalertssChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedalertssChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    alertss: alerts[];
    user: any;
    selectedalertss: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The alertss App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getalertss()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getalertss();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getalertss();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('abayelsa', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/abayelsa';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/abayelsa';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }


    /***********
     * alerts
     **********/
    /**
     * Save a alerts
     * @param {alerts} alerts
     *
     * @return Promise<alerts>
     */
    savealerts(alerts: alerts): Promise<alerts> {
        //alerts.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('alerts', alerts)
            .then((data: any) => {

                if (data && data.alertss && data.alertss
                [0]) {
                    console.log('save');
                
                    return data.alertss[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a alerts
    * @param {alerts} alerts
    *
    * @return Promise<alerts>
    */
    updatealerts(alerts: alerts) {
        return this.db.rel.save('alerts', alerts)
            .then((data: any) => {
                if (data && data.alertss && data.alertss
                [0]) {
                    console.log('Update')
                    
                    return data.alertss[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a alerts
     * @param {alerts} alerts
     *
     * @return Promise<boolean>
     */
    removealerts(alerts: alerts): Promise<boolean> {
        return this.db.rel.del('alerts', alerts)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the alertss
     *
     * @return Promise<Array<alerts>>
     */
    getalertss(): Promise<Array<alerts>> {
        return this.db.rel.find('alerts')
            .then((data: any) => {
                this.alertss = data.alertss;
                if (this.searchText && this.searchText !== '') {
                    this.alertss = FuseUtils.filterArrayByString(this.alertss, this.searchText);
                }
                //this.onalertssChanged.next(this.alertss);
                return Promise.resolve(this.alertss);
                //return data.alertss ? data.alertss : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getDoctoralertss(): Promise<Array<alerts>> {
        return this.db.rel.find('alerts')
            .then((data: any) => {
                return data.alertss ? data.alertss : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a alerts
     * @param {alerts} alerts
     *
     * @return Promise<alerts>
     */
    getalerts(alerts: alerts): Promise<alerts> {
        return this.db.rel.find('alerts', alerts.id)
            .then((data: any) => {
                console.log("Get")
                
                return data && data.alertss ? data.alertss[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected alerts by id
     * @param id
     */
    toggleSelectedalerts(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedalertss.length > 0) {
            const index = this.selectedalertss.indexOf(id);

            if (index !== -1) {
                this.selectedalertss.splice(index, 1);

                // Trigger the next event
                this.onSelectedalertssChanged.next(this.selectedalertss);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedalertss.push(id);

        // Trigger the next event
        this.onSelectedalertssChanged.next(this.selectedalertss);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedalertss.length > 0) {
            this.deselectalertss();
        }
        else {
            this.selectalertss();
        }
    }

    selectalertss(filterParameter?, filterValue?) {
        this.selectedalertss = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedalertss = [];
            this.alertss.map(alerts => {
                this.selectedalertss.push(alerts.id);
            });
        }
        else {
            /* this.selectedalertss.push(...
                 this.alertss.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedalertssChanged.next(this.selectedalertss);
    }





    deselectalertss() {
        this.selectedalertss = [];

        // Trigger the next event
        this.onSelectedalertssChanged.next(this.selectedalertss);
    }

    deletealerts(alerts) {
        this.db.rel.del('alerts', alerts)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const alertsIndex = this.alertss.indexOf(alerts);
                this.alertss.splice(alertsIndex, 1);
                this.onalertssChanged.next(this.alertss);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedalertss() {
        for (const alertsId of this.selectedalertss) {
            const alerts = this.alertss.find(_alerts => {
                return _alerts.id === alertsId;
            });

            this.db.rel.del('alerts', alerts)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const alertsIndex = this.alertss.indexOf(alerts);
                    this.alertss.splice(alertsIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onalertssChanged.next(this.alertss);
        this.deselectalertss();
    }
}
