import { Component, OnInit } from '@angular/core';
import { alertssService } from '../alerts.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FusealertssSelectedBarComponent implements OnInit
{
    selectedalertss: string[];
    hasSelectedalertss: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private alertssService: PouchService,
        public dialog: MdDialog
    )
    {
        this.alertssService.onSelectedalertssChanged
            .subscribe(selectedalertss => {
                this.selectedalertss = selectedalertss;
                setTimeout(() => {
                    this.hasSelectedalertss = selectedalertss.length > 0;
                    this.isIndeterminate = (selectedalertss.length !== this.alertssService.alertss.length && selectedalertss.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.alertssService.selectalertss();
    }

    deselectAll()
    {
        this.alertssService.deselectalertss();
    }

    deleteSelectedalertss()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected alertss?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.alertssService.deleteSelectedalertss();
            }
            this.confirmDialogRef = null;
        });
    }

}
