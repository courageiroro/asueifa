import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Configureanterecord } from '../ante_record.model';
import { ConfigureanterecordsService } from '../ante_records.service';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-configureanterecords-configureanterecord-form-dialog',
    templateUrl: './ante_record-form.component.html',
    styleUrls: ['./ante_record-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseConfigureanterecordsConfigureanterecordFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    configureanterecordForm: FormGroup;
    action: string;
    configureanterecord: Configureanterecord;
    public categorys;
    public types;
    category

    constructor(
        public dialogRef: MdDialogRef<FuseConfigureanterecordsConfigureanterecordFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        private db: PouchService
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit configureanterecord';
            this.configureanterecord = data.configureanterecord;
        }
        else {
            this.dialogTitle = 'New configureanterecord';
            this.configureanterecord = {
                id: '',
                rev: '',
                name: '',
                category: '',
                type: ''
            }
        }

        this.configureanterecordForm = this.createconfigureanterecordForm();
    }

    ngOnInit() {
        this.categorys = ['Precious Medical History', 'Child Birth', 'Immunization', 'Primary Assessment']
        this.types = ['input', 'text', 'checkbox', 'date']
    }

    getCategory(){
       this.db.category = this.configureanterecord.category;
       
    }

    createconfigureanterecordForm() {
        return this.formBuilder.group({
            id: [this.configureanterecord.id],
            rev: [this.configureanterecord.rev],
            name: [this.configureanterecord.name],
            type: [this.configureanterecord.type],
            category: [this.configureanterecord.category],

        });
    }
}
