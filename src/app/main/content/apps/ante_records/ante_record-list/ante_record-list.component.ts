import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ConfigureanterecordsService } from '../ante_records.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { FuseConfigureanterecordsConfigureanterecordFormDialogComponent } from '../ante_record-form/ante_record-form.component';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Configureanterecord } from '../ante_record.model';
import { Subject } from 'rxjs/Subject';
import { Http, Headers, RequestOptions } from '@angular/http';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-configureanterecords-configureanterecord-list',
    templateUrl: './ante_record-list.component.html',
    styleUrls: ['./ante_record-list.component.scss']
})
export class FuseConfigureanterecordsConfigureanterecordListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public configureanterecords: Array<Configureanterecord> = [];
    configureanterecords2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    //dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'name', 'description', 'buttons'];
    selectedConfigureanterecords: any[];
    online = true;
    checkboxes: {};
    pmh = [];
    refresh: Subject<any> = new Subject();
    childBirths = [];
    immunization = [];
    primaryasses = [];
    currentmedications = [];
    familyhistory = [];
    notes = [];
    anterecordss: Configureanterecord[];

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(public dialog: MdDialog, public db: PouchService, public http: Http)
    { }

    ngOnInit() {
        //this.dataSource = new FilesDataSource(this.db);
        this._loadConfigureanterecords();
    }

    private _loadConfigureanterecords(): void {
        this.db.getConfigureanterecords()
            .then(configureanterecords => {
                this.configureanterecords = configureanterecords;
                //this.configureanterecords2 = new BehaviorSubject<any>(configureanterecords);
                
                this.configureanterecords = configureanterecords;
                //this.anterecords2 = new BehaviorSubject<any>(anterecords);
                this.pmh = this.configureanterecords.filter(data => data.category == 'Precious Medical History');
                this.refresh.next(this.pmh);
                this.childBirths = this.configureanterecords.filter(data => data.category == 'Child Birth');
                this.immunization = this.configureanterecords.filter(data => data.category == 'Immunization');
                this.primaryasses = this.configureanterecords.filter(data => data.category == 'Primary Assessment');
                //console.log(this.configureanterecords2);
            });

    }


    onlineCheck() {
        this.online = true;
        this.http.get('https://sarutech.com/couchdblogin/auth/validate-username/xs2da2432252sda').subscribe(data => {
            this.online = true;
         
        },
            (err) => {
                this.online = false;
                
            });
    }

    click() {
        this.onlineCheck();
    }

    newConfigureanterecord() {
        this.dialogRef = this.dialog.open(FuseConfigureanterecordsConfigureanterecordFormDialogComponent, {
            panelClass: 'configureanterecord-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
              
                if (this.db.category == "Precious Medical History") {
                    
                    this.pmh.push(response.getRawValue());
                }
                else if (this.db.category == "Child Birth") {
                    this.childBirths.push(response.getRawValue());
                }
                else if (this.db.category == "Immunization") {
                    this.immunization.push(response.getRawValue());
                }
                else if (this.db.category == "Primary Assessment") {
                    this.primaryasses.push(response.getRawValue());
                }
                this.db.saveConfigureanterecord(response.getRawValue());
                this.refresh.next(true);
                //this.dataSource = new FilesDataSource(this.db);
                

            });

    }

    editConfigureanterecord(configureanterecord) {
        
        this.dialogRef = this.dialog.open(FuseConfigureanterecordsConfigureanterecordFormDialogComponent, {
            panelClass: 'configureanterecord-form-dialog',
            data: {
                configureanterecord: configureanterecord,
                action: 'edit'

            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                //this.general.push(formData.getRawValue());
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':
                        if (configureanterecord.category == "Precious Medical History") {
                            
                                configureanterecord.id = '',
                                configureanterecord.rev = '',
                                configureanterecord.name = formData.getRawValue().name,
                                configureanterecord.type = formData.getRawValue().type,
                                configureanterecord.category = formData.getRawValue().category
                            
                        }
                        else if (configureanterecord.category == "Child Birth") {
                                configureanterecord.id = '',
                                configureanterecord.rev = '',
                                configureanterecord.name = formData.getRawValue().name,
                                configureanterecord.type = formData.getRawValue().type,
                                configureanterecord.category = formData.getRawValue().category

                        }
                        else if (configureanterecord.category == "Immunization") {
                                configureanterecord.id = '',
                                configureanterecord.rev = '',
                                configureanterecord.name = formData.getRawValue().name,
                                configureanterecord.type = formData.getRawValue().type,
                                configureanterecord.category = formData.getRawValue().category
                        }
                        else if (configureanterecord.category == "Primary Assessment") {
                                configureanterecord.id = '',
                                configureanterecord.rev = '',
                                configureanterecord.name = formData.getRawValue().name,
                                configureanterecord.type = formData.getRawValue().type,
                                configureanterecord.category = formData.getRawValue().category
                        }

                        this.db.updateConfigureanterecord(formData.getRawValue());
                        this.refresh.next(true);
                        //this.dataSource = new FilesDataSource(this.db);
                        

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteConfigureanterecord(configureanterecord);

                        break;
                }
            });
    }

    /**
     * Delete configureanterecord
     */
    deleteConfigureanterecord(configureanterecord) {
        
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                if (configureanterecord.category == "Precious Medical History") {
                  var index =  this.pmh.indexOf(configureanterecord);
                  this.pmh.splice(index,1);
                   
                }
                 else if (configureanterecord.category == "Child Birth") {
                              var index =  this.childBirths.indexOf(configureanterecord);
                              this.childBirths.splice(index,1);
                              
                        }
                        else if (configureanterecord.category == "Immunization") {
                              var index =  this.immunization.indexOf(configureanterecord);
                              this.immunization.splice(index,1);
                        }
                        else if (configureanterecord.category == "Primary Assessment") {
                                var index =  this.primaryasses.indexOf(configureanterecord);
                                this.primaryasses.splice(index,1);
                        }
                       
                this.db.deleteConfigureanterecord(configureanterecord);
                //this.dataSource = new FilesDataSource(this.db);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(configureanterecordId) {
        this.db.toggleSelectedConfigureanterecord(configureanterecordId);
    }

}

/* export class FilesDataSource extends DataSource<any>
{
    configureanterecords2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: ConfigureanterecordsService) {
        super();
    }

     //Connect function called by the table to retrieve one stream containing the data to render.
    connect(): Observable<any[]> {
        var result = Observable.fromPromise(this.db.getConfigureanterecords());
        result.subscribe(x => console.log(x), e => console.error(e));
        return result;
    }

    disconnect() {
    }
}
 */