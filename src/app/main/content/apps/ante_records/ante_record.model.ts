export interface Configureanterecord {
    id: string,
    rev: string,
    name: string,
    category: string,
    type: string
}

