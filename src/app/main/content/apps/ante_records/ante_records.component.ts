import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ConfigureanterecordsService } from './ante_records.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector: 'fuse-configureanterecords',
    templateUrl: './ante_records.component.html',
    styleUrls: ['./ante_records.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [Animations.slideInTop]
})
export class FuseConfigureanterecordsComponent implements OnInit {
    hasSelectedConfigureanterecords: boolean;
    searchInput: FormControl;

    constructor(private configureanterecordsService: PouchService) {
        this.searchInput = new FormControl('');
       
    }

    ngOnInit() {

        this.configureanterecordsService.onSelectedConfigureanterecordsChanged
            .subscribe(selectedConfigureanterecords => {
                this.hasSelectedConfigureanterecords = selectedConfigureanterecords.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.configureanterecordsService.onSearchTextChanged.next(searchText);
            });
    }

}
