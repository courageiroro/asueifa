import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseConfigureanterecordsComponent } from './ante_records.component';
import { ConfigureanterecordsService } from './ante_records.service';
//import { UserService } from '../../../../user.service';
//import { configureanterecordsService2 } from './contacts.service';
import { FuseConfigureanterecordsConfigureanterecordListComponent } from './ante_record-list/ante_record-list.component';
import { FuseConfigureanterecordsSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseConfigureanterecordsConfigureanterecordFormDialogComponent } from './ante_record-form/ante_record-form.component';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/anteconfigures',
        component: FuseConfigureanterecordsComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            configureanterecords: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseConfigureanterecordsComponent,
        FuseConfigureanterecordsConfigureanterecordListComponent,
        FuseConfigureanterecordsSelectedBarComponent,
        FuseConfigureanterecordsConfigureanterecordFormDialogComponent
    ],
    providers      : [
        ConfigureanterecordsService,
        AuthGuard,
        PouchService
       //UserService
    ],
    entryComponents: [FuseConfigureanterecordsConfigureanterecordFormDialogComponent]
})
export class FuseConfigureanterecordsModule
{
}
