/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Configureanterecord } from './ante_record.model';
import { Schema } from '../schema';

@Injectable()
export class ConfigureanterecordsService {
    public configureanterecord = "";
    public sCredentials;
    public category;
    onConfigureanterecordsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedConfigureanterecordsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    configureanterecords: Configureanterecord[];
    user: any;
    selectedConfigureanterecords: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The configureanterecords App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getConfigureanterecords()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getConfigureanterecords();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getConfigureanterecords();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('abayelsa', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/abayelsa';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/abayelsa';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }


    /***********
     * configureanterecord
     **********/
    /**
     * Save a configureanterecord
     * @param {configureanterecord} Configureanterecord
     *
     * @return Promise<configureanterecord>
     */
    saveConfigureanterecord(configureanterecord: Configureanterecord): Promise<Configureanterecord> {
        //configureanterecord.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('configureanterecord', configureanterecord)
            .then((data: any) => {

                if (data && data.configureanterecords && data.configureanterecords
                [0]) {
                    console.log('save');
                
                    return data.configureanterecords[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a configureanterecord
    * @param {configureanterecord} configureanterecord
    *
    * @return Promise<configureanterecord>
    */
    updateConfigureanterecord(configureanterecord: Configureanterecord) {
        return this.db.rel.save('configureanterecord', configureanterecord)
            .then((data: any) => {
                if (data && data.configureanterecords && data.configureanterecords
                [0]) {
                    console.log('Update')
                    
                    return data.configureanterecords[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a configureanterecord
     * @param {configureanterecord} configureanterecord
     *
     * @return Promise<boolean>
     */
    removeConfigureanterecord(configureanterecord: Configureanterecord): Promise<boolean> {
        return this.db.rel.del('configureanterecord', configureanterecord)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the configureanterecords
     *
     * @return Promise<Array<configureanterecord>>
     */
    getConfigureanterecords(): Promise<Array<Configureanterecord>> {
        return this.db.rel.find('configureanterecord')
            .then((data: any) => {
                this.configureanterecords = data.configureanterecords;
                if (this.searchText && this.searchText !== '') {
                    this.configureanterecords = FuseUtils.filterArrayByString(this.configureanterecords, this.searchText);
                }
                //this.onconfigureanterecordsChanged.next(this.configureanterecords);
                return Promise.resolve(this.configureanterecords);
                //return data.configureanterecords ? data.configureanterecords : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getDoctorConfigureanterecords(): Promise<Array<Configureanterecord>> {
        return this.db.rel.find('configureanterecord')
            .then((data: any) => {
                return data.configureanterecords ? data.configureanterecords : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a configureanterecord
     * @param {configureanterecord} configureanterecord
     *
     * @return Promise<configureanterecord>
     */
    getConfigureanterecord(configureanterecord: Configureanterecord): Promise<Configureanterecord> {
        return this.db.rel.find('configureanterecord', configureanterecord.id)
            .then((data: any) => {
                console.log("Get")
                
                return data && data.configureanterecords ? data.configureanterecords[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected configureanterecord by id
     * @param id
     */
    toggleSelectedConfigureanterecord(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedConfigureanterecords.length > 0) {
            const index = this.selectedConfigureanterecords.indexOf(id);

            if (index !== -1) {
                this.selectedConfigureanterecords.splice(index, 1);

                // Trigger the next event
                this.onSelectedConfigureanterecordsChanged.next(this.selectedConfigureanterecords);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedConfigureanterecords.push(id);

        // Trigger the next event
        this.onSelectedConfigureanterecordsChanged.next(this.selectedConfigureanterecords);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedConfigureanterecords.length > 0) {
            this.deselectConfigureanterecords();
        }
        else {
            this.selectConfigureanterecords();
        }
    }

    selectConfigureanterecords(filterParameter?, filterValue?) {
        this.selectedConfigureanterecords = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedConfigureanterecords = [];
            this.configureanterecords.map(configureanterecord => {
                this.selectedConfigureanterecords.push(configureanterecord.id);
            });
        }
        else {
            /* this.selectedconfigureanterecords.push(...
                 this.configureanterecords.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedConfigureanterecordsChanged.next(this.selectedConfigureanterecords);
    }





    deselectConfigureanterecords() {
        this.selectedConfigureanterecords = [];

        // Trigger the next event
        this.onSelectedConfigureanterecordsChanged.next(this.selectedConfigureanterecords);
    }

    deleteConfigureanterecord(configureanterecord) {
        this.db.rel.del('configureanterecord', configureanterecord)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const configureanterecordIndex = this.configureanterecords.indexOf(configureanterecord);
                this.configureanterecords.splice(configureanterecordIndex, 1);
                this.onConfigureanterecordsChanged.next(this.configureanterecords);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedConfigureanterecords() {
        for (const configureanterecordId of this.selectedConfigureanterecords) {
            const configureanterecord = this.configureanterecords.find(_configureanterecord => {
                return _configureanterecord.id === configureanterecordId;
            });

            this.db.rel.del('configureanterecord', configureanterecord)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const configureanterecordIndex = this.configureanterecords.indexOf(configureanterecord);
                    this.configureanterecords.splice(configureanterecordIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onConfigureanterecordsChanged.next(this.configureanterecords);
        this.deselectConfigureanterecords();
    }
}
