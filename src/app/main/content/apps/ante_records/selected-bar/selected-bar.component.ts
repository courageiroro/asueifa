import { Component, OnInit } from '@angular/core';
import { ConfigureanterecordsService } from '../ante_records.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseConfigureanterecordsSelectedBarComponent implements OnInit
{
    selectedConfigureanterecords: string[];
    hasSelectedConfigureanterecords: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private configureanterecordsService: PouchService,
        public dialog: MdDialog
    )
    {
        this.configureanterecordsService.onSelectedConfigureanterecordsChanged
            .subscribe(selectedConfigureanterecords => {
                this.selectedConfigureanterecords = selectedConfigureanterecords;
                setTimeout(() => {
                    this.hasSelectedConfigureanterecords = selectedConfigureanterecords.length > 0;
                    this.isIndeterminate = (selectedConfigureanterecords.length !== this.configureanterecordsService.configureanterecords.length && selectedConfigureanterecords.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.configureanterecordsService.selectConfigureanterecords();
    }

    deselectAll()
    {
        this.configureanterecordsService.deselectConfigureanterecords();
    }

    deleteSelectedConfigureanterecords()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected configureanterecords?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.configureanterecordsService.deleteSelectedConfigureanterecords();
            }
            this.confirmDialogRef = null;
        });
    }

}
