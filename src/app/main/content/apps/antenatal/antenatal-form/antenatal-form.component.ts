import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { antenatal } from '../../antenatal/antenatal.model';
import { Blood_banksService } from '../../blood_banks/blood_banks.service'
import { antenatalsService } from '../../antenatal/antenatals.service';
import { InsurancetypesService } from '../../insurancetypes/insurancetypes.service';
import { DomSanitizer } from '@angular/platform-browser';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-antenatals-antenatal-form-dialog',
    templateUrl: './antenatal-form.component.html',
    styleUrls: ['./antenatal-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseantenatalsantenatalFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    antenatalForm: FormGroup;
    action: string;
    antenatal: antenatal;
    sex: any;
    speakenglish = [];
    literate = [];
    public blood_bankss;
    insuranceTypes;
    image;

    constructor(
        public dialogRef: MdDialogRef<FuseantenatalsantenatalFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        public db: PouchService,
        public _DomSanitizer: DomSanitizer

    ) {

        this.db.getInsurancetypes().then(res => {
            this.insuranceTypes = res;
        })

        /* document.addEventListener('click', function () {
            db2.file = document.querySelector('#fileupload');
            var imagestring = document.querySelector('#imagestring');

            var retrieve;

        }) */

        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit antenatal';
            this.antenatal = data.antenatal;
            this.image = data.antenatal.image;
        }
        else {
            this.dialogTitle = 'New antenatal';
            this.antenatal = {
                id: '',
                rev: '',
                surname: '',
                firstname: '',
                xrayno: '',
                unitno: '',
                specialpoints: '',
                consultant: '',
                dateofbooking: new Date(),
                indicationbook: '',
                lmp: '',
                edd: '',
                egabooking: '',
                address: '',
                age: '',
                occupation: '',
                speakenglish: '',
                literate: '',
                husbandname: '',
                husbandoccupation: '',
                husbandemployer: '',
                anterecord: '',
                anterecords: '',
                followup: []
            }

        }
        this.db.getBlood_banks().then(res => {
            this.blood_bankss = res;

        })

        this.antenatalForm = this.createantenatalForm();
    }

    ngOnInit() {
        this.speakenglish = ['Yes', 'No'];
        this.literate = ['Yes', 'No'];
    }



    select(sex) {

    }

    createantenatalForm() {
        return this.formBuilder.group({
            id: [this.antenatal.id],
            rev: [this.antenatal.rev],
            surname: [this.antenatal.surname],
            firstname: [this.antenatal.firstname],
            xrayno: [this.antenatal.xrayno],
            unitno: [this.antenatal.unitno],
            specialpoints: [this.antenatal.specialpoints],
            consultant: [this.antenatal.consultant],
            dateofbooking: [this.antenatal.dateofbooking],
            indicationbook: [this.antenatal.indicationbook],
            lmp: [this.antenatal.lmp],
            edd: [this.antenatal.edd],
            egabooking: [this.antenatal.egabooking],
            address: [this.antenatal.address],
            age: [this.antenatal.age],
            occupation: [this.antenatal.occupation],
            speakenglish: [this.antenatal.speakenglish],
            literate: [this.antenatal.literate],
            husbandname: [this.antenatal.husbandname],
            husbandoccupation: [this.antenatal.husbandoccupation],
            husbandemployer: [this.antenatal.husbandemployer],
            anterecord: [this.antenatal.anterecord],
            anterecords: [this.antenatal.anterecords]
        });
    }
}
