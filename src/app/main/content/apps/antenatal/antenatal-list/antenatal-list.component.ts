import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { antenatalsService } from '../../antenatal/antenatals.service';
import { PrescriptionsService } from '../../prescriptions/prescriptions.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs/Rx';
import { FuseantenatalsantenatalFormDialogComponent } from '../antenatal-form/antenatal-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { antenatal } from '../../antenatal/antenatal.model';
import { PouchService } from '../../../../../provider/pouch-service';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';


@Component({
    selector: 'fuse-antenatals-antenatal-list',
    templateUrl: './antenatal-list.component.html',
    styleUrls: ['./antenatal-list.component.scss']
})
export class FuseantenatalsantenatalListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public localStorageItem: any;
    public localStorageType: any;
    public antenatals: Array<antenatal> = [];
    antenatals2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'surname', 'firstname', 'xrayno', 'unitno', 'specialpoints', 'consultant', 'dateofbooking', 'indicationbook', 'lmp', 'edd', 'egabooking', 'address', 'age', 'occupation', 'speakenglish', 'literate', 'husbandname', 'husbandoccupation', 'husbandemployer', 'buttons'];
    selectedantenatals: any[];
    checkboxes: {};

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;


    constructor(public dialog: MdDialog, public db: PouchService, public router: Router) {
        this.localStorageItem = JSON.parse(localStorage.getItem('user'));
        this.localStorageType = this.localStorageItem.usertype;
    }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        this._loadantenatals();
    }

    private _loadantenatals(): void {
        this.db.onantenatalsChanged.subscribe(data => {
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        this.db.getantenatals()
            .then((antenatals: Array<antenatal>) => {
                var userType = "antenatal"
                this.antenatals = antenatals;

                this.antenatals2 = new BehaviorSubject<any>(antenatals);

                this.checkboxes = {};
                antenatals.map(antenatal => {
                    this.checkboxes[antenatal.id] = false;
                });
                this.db.onSelectedantenatalsChanged.subscribe(selectedantenatals => {
                    for (const id in this.checkboxes) {
                        this.checkboxes[id] = selectedantenatals.includes(id);
                    }

                    this.selectedantenatals = selectedantenatals;
                });
                this.db.onSearchTextChanged.subscribe(data => {
                    this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                })
            });

    }

    get() {
        this.db.getantenatals().then(res => {

            /*   var url = URL.createObjectURL(res[3]._attachments);
  
              var img = document.createElement('img');
              img.src = url;
              document.getElementById('img2').appendChild(img) */
        })
    }

    newantenatal() {
        this.dialogRef = this.dialog.open(FuseantenatalsantenatalFormDialogComponent, {
            panelClass: 'antenatal-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
                var res = response.getRawValue();
                /* res.dateofbooking = new Date(res.birth_date).toISOString().substring(0, 10); */

                this.db.saveantenatal(res);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
            });


    }

    antenatalrecords(antenatal) {
        this.router.navigate(['apps/anterecords', antenatal.id]);
    }

    antenatallab(antenatal) {
        this.router.navigate(['apps/labrecord', antenatal.id]);
    }

    antenatalpresc(antenatal) {
        this.router.navigate(['apps/prescriptions', antenatal.id]);
        this.db.id = antenatal.id;
    }

    editantenatal(antenatal) {
        console.log(antenatal);
        this.dialogRef = this.dialog.open(FuseantenatalsantenatalFormDialogComponent, {
            panelClass: 'antenatal-form-dialog',
            data: {
                antenatal: antenatal,
                action: 'edit'
            }
        });



        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                var form = formData.getRawValue();
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':
                        /* form.dateofbooking = new Date(form.birth_date).toISOString().substring(0, 10); */

                        this.db.updateantenatal(form);

                        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);


                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteantenatal(antenatal);

                        break;
                }
            });

    }

    /**
     * Delete antenatal
     */
    deleteantenatal(antenatal) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deleteantenatal(antenatal);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(antenatalId) {
        this.db.toggleSelectedantenatal(antenatalId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    antenatals2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            of(this.db.antenatals).delay(5000),
            this.sort.mdSortChange,
            this._paginator.page,
        ];
        var result = Observable.fromPromise(this.db.getantenatals());
        /*   result.subscribe(x => console.log(x), e => console.error(e));
         return result; */

        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.antenatals]));
        });
    }

    disconnect() {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
            return data;
        }

        return data.sort((a, b) => {
            const isAsc = this.sort.direction === 'asc';
            switch (this.sort.active) {
                case 'surname': return compare(a.surname, b.surname, isAsc);
                case 'amount': return compare(+a.amount, +b.amount, isAsc);
                case 'id': return compare(+a.id, +b.id, isAsc);
                default: return 0;
            }
        });
    }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}