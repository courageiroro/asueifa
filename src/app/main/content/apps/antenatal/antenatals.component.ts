import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { antenatalsService } from '../antenatal/antenatals.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-antenatals',
    templateUrl  : './antenatals.component.html',
    styleUrls    : ['./antenatals.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FuseantenatalsComponent implements OnInit
{
    hasSelectedantenatals: boolean;
    searchInput: FormControl;

    constructor(private antenatalsService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.antenatalsService.onSelectedantenatalsChanged
            .subscribe(selectedantenatals => {
                this.hasSelectedantenatals = selectedantenatals.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.antenatalsService.onSearchTextChanged.next(searchText);
            });
    }

}
