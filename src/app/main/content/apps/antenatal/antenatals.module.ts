import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseantenatalsComponent } from './antenatals.component';
import { antenatalsService } from '../antenatal/antenatals.service';
import { FuseantenatalsantenatalListComponent } from './antenatal-list/antenatal-list.component';
import { FuseantenatalsSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseantenatalsantenatalFormDialogComponent } from './antenatal-form/antenatal-form.component';
import {Blood_banksService} from '../blood_banks/blood_banks.service'
import { RecordsService } from '../records/records.service';
import { PrescriptionsService } from '../prescriptions/prescriptions.service';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/antenatals',
        component: FuseantenatalsComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            antenatals: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseantenatalsComponent,
        FuseantenatalsantenatalListComponent,
        FuseantenatalsSelectedBarComponent,
        FuseantenatalsantenatalFormDialogComponent
    ],
    providers      : [
        antenatalsService,
        AuthGuard,
        Blood_banksService,
        RecordsService,
        PrescriptionsService,
        PouchService
    ],
    entryComponents: [FuseantenatalsantenatalFormDialogComponent]
})
export class FuseantenatalsModule
{
}
