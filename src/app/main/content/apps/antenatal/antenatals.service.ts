/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { antenatal } from './antenatal.model';
import { Schema } from '../schema';

@Injectable()
export class antenatalsService {
    public antenatalname = "";
    public antenatalnumber = "";
    public sCredentials;
    
    onantenatalsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedantenatalsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    antenatals: antenatal[];
    user: any;
    selectedantenatals: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The antenatals App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getantenatals()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getantenatals();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getantenatals();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('abayelsa', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/abayelsa';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/abayelsa';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }

    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }


    /***********
     * antenatal
     **********/
    /**
     * Save a antenatal
     * @param {antenatal} antenatal
     *
     * @return Promise<antenatal>
     */
    saveantenatal(antenatal: antenatal): Promise<antenatal> {
        antenatal.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('antenatal', antenatal)
            .then((data: any) => {

                if (data && data.antenatals && data.antenatals
                [0]) {
                    return data.antenatals[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a antenatal
    * @param {antenatal} antenatal
    *
    * @return Promise<antenatal>
    */
    updateantenatal(antenatal: antenatal) {

        return this.db.rel.save('antenatal', antenatal)
            .then((data: any) => {
                if (data && data.antenatals && data.antenatals
                [0]) {
                    
                    return data.antenatals[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a antenatal
     * @param {antenatal} antenatal
     *
     * @return Promise<boolean>
     */
    removeantenatal(antenatal: antenatal): Promise<boolean> {
        return this.db.rel.del('antenatal', antenatal)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the antenatals
     *
     * @return Promise<Array<antenatal>>
     */
    getantenatals(): Promise<Array<antenatal>> {
        return this.db.rel.find('antenatal')
            .then((data: any) => {
                this.antenatals = data.antenatals;
                if (this.searchText && this.searchText !== '') {
                    this.antenatals = FuseUtils.filterArrayByString(this.antenatals, this.searchText);
                }
                //this.onantenatalsChanged.next(this.antenatals);
                return Promise.resolve(this.antenatals);
                //return data.antenatals ? data.antenatals : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getAppointmentantenatals(): Promise<Array<antenatal>> {
        return this.db.rel.find('antenatal')
            .then((data: any) => {
                return data.antenatals ? data.antenatals : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a antenatal
     * @param {antenatal} antenatal
     *
     * @return Promise<antenatal>
     */
    getantenatal(antenatal: antenatal): Promise<antenatal> {
        return this.db.rel.find('antenatal', antenatal.id)
            .then((data: any) => {
                return data && data.antenatals ? data.antenatals[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected antenatal by id
     * @param id
     */
    toggleSelectedantenatal(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedantenatals.length > 0) {
            const index = this.selectedantenatals.indexOf(id);

            if (index !== -1) {
                this.selectedantenatals.splice(index, 1);

                // Trigger the next event
                this.onSelectedantenatalsChanged.next(this.selectedantenatals);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedantenatals.push(id);

        // Trigger the next event
        this.onSelectedantenatalsChanged.next(this.selectedantenatals);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedantenatals.length > 0) {
            this.deselectantenatals();
        }
        else {
            this.selectantenatals();
        }
    }

    selectantenatals(filterParameter?, filterValue?) {
        this.selectedantenatals = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedantenatals = [];
            this.antenatals.map(antenatal => {
                this.selectedantenatals.push(antenatal.id);
            });
        }
        else {
            /* this.selectedantenatals.push(...
                 this.antenatals.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedantenatalsChanged.next(this.selectedantenatals);
    }





    deselectantenatals() {
        this.selectedantenatals = [];

        // Trigger the next event
        this.onSelectedantenatalsChanged.next(this.selectedantenatals);
    }

    deleteantenatal(antenatal) {
        this.db.rel.del('antenatal', antenatal)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const antenatalIndex = this.antenatals.indexOf(antenatal);
                this.antenatals.splice(antenatalIndex, 1);
                this.onantenatalsChanged.next(this.antenatals);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedantenatals() {
        for (const antenatalId of this.selectedantenatals) {
            const antenatal = this.antenatals.find(_antenatal => {
                return _antenatal.id === antenatalId;
            });

            this.db.rel.del('antenatal', antenatal)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const antenatalIndex = this.antenatals.indexOf(antenatal);
                    this.antenatals.splice(antenatalIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onantenatalsChanged.next(this.antenatals);
        this.deselectantenatals();
    }
}
