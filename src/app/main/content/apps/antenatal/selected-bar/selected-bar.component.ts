import { Component, OnInit } from '@angular/core';
import { antenatalsService } from '../../antenatal/antenatals.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseantenatalsSelectedBarComponent implements OnInit
{
    selectedantenatals: string[];
    hasSelectedantenatals: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private antenatalsService: PouchService,
        public dialog: MdDialog
    )
    {
        this.antenatalsService.onSelectedantenatalsChanged
            .subscribe(selectedantenatals => {
                this.selectedantenatals = selectedantenatals;
                setTimeout(() => {
                    this.hasSelectedantenatals = selectedantenatals.length > 0;
                    this.isIndeterminate = (selectedantenatals.length !== this.antenatalsService.antenatals.length && selectedantenatals.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.antenatalsService.selectantenatals();
    }

    deselectAll()
    {
        this.antenatalsService.deselectantenatals();
    }

    deleteSelectedantenatals()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected antenatales?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.antenatalsService.deleteSelectedantenatals();
            }
            this.confirmDialogRef = null;
        });
    }

}
