import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { anterecord } from '../anterecord.model';
import { anterecordsService } from '../anterecords.service';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-anterecords-anterecord-form-dialog',
    templateUrl: './anterecord-form.component.html',
    styleUrls: ['./anterecord-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseanterecordsanterecordFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    anterecordForm: FormGroup;
    action: string;
    anterecord: anterecord;
    public categorys;
    public types;
    category

    constructor(
        public dialogRef: MdDialogRef<FuseanterecordsanterecordFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        private db: PouchService
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit anterecord';
            this.anterecord = data.anterecord;
        }
        else {
            this.dialogTitle = 'New anterecord';
            this.anterecord = {
                id: '',
                rev: '',
                customer: '',
            }
        }

        this.anterecordForm = this.createanterecordForm();
    }

    ngOnInit() {
        this.categorys = ['General', 'Medical History', 'Social History', 'Medical Conditions', 'Current Medications', 'Family History', 'Notes']
        this.types = ['input', 'text', 'checkbox']
    }

    getCategory(){
       this.db.category = this.category
       
    }

    createanterecordForm() {
        return this.formBuilder.group({
            id: [this.anterecord.id],
            rev: [this.anterecord.rev],
            customer: [this.anterecord.customer]
        });
    }
}
