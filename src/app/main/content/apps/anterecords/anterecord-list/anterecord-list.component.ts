import { Component, OnInit, TemplateRef, ViewChild, AfterViewInit } from '@angular/core';
import { anterecordsService } from '../anterecords.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { ConfigureanterecordsService } from '../../ante_records/ante_records.service';
import { FuseanterecordsanterecordFormDialogComponent } from '../anterecord-form/anterecord-form.component';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { anterecord } from '../anterecord.model';
import { Subject } from 'rxjs/Subject';
import { antenatal } from '../../antenatal/antenatal.model';
import { Configureanterecord } from '../../ante_records/ante_record.model';
import { Http, Headers, RequestOptions } from '@angular/http';
import { antenatalsService } from '../../antenatal/antenatals.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { FuseanterecordreceiptsanterecordreceiptFormDialogComponent } from '../anterecord-receipt/anterecord-receipt.component';
import { remote, ipcRenderer } from 'electron';
import {PouchService} from '../../../../../provider/pouch-service';
/* declare const window: any;
const { remote } = window.require('electron');
const { BrowserWindow, dialog, shell } = remote;
const fs = window.require('fs'); */

@Component({
    selector: 'fuse-anterecords-anterecord-list',
    templateUrl: './anterecord-list.component.html',
    styleUrls: ['./anterecord-list.component.scss']
})
export class FuseanterecordsanterecordListComponent implements OnInit, AfterViewInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    nativeWindowOpen: true
    public anterecords: Array<anterecord> = [];
    public configureanterecords: Array<Configureanterecord> = [];
    public antenatal: antenatal;
    patients;
    patientName;
    patientAddress;
    patientFirstname;
    patientDatebooking;
    window: Window;
    urlanterecord;
    image;
    checkboxstate;
    pid;
    anterecords2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    //dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'name', 'description', 'buttons'];
    selectedanterecords: any[];
    online = true;
    checkboxes: {};
    anterecord: any = {
        id: Math.round((new Date()).getTime()).toString(),
        rev: '',
        customer: ''
    };
    pmh = [];
    refresh: Subject<any> = new Subject();
    childBirths = [];
    immunization = [];
    primaryasses = [];
    currentmedications = [];
    familyhistory = [];
    notes = [];
    anterecordss: anterecord[];
    typeofprint;
    image2;

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(public dialog: MdDialog, public _DomSanitizer: DomSanitizer, public db: PouchService, public http: Http, public activateRoute: ActivatedRoute, public router: Router) {

        let id = this.activateRoute.snapshot.params['id'];
        this.pid = id;
        this.db.getantenatals().then(res => {
            this.click(event);
            this.checkboxstate = event.isTrusted
            
        })
        

    }

    ngOnInit() {
        //this.dataSource = new FilesDataSource(this.db);
        this._loadanterecords();
    }

    ngAfterViewInit() {
        console.log(this.pid)
        this.db.getantenatal(this.pid).then(res => {
            console.log(res);
            this.patients = res;
            
            this.patientName = res.surname;
            this.patientAddress = res.address; 
            this.patientFirstname = res.firstname;
            this.patientDatebooking = res.dateofbooking;
           
           /*  if (res.image) {
                this.image = true;
                this.image2 = res.image;
                
            }
            else {
                this.image = false
            } */
            
            if (this.patients.anterecord == "") {
                this.anterecord = {
                    id: Math.round((new Date()).getTime()).toString(),
                    rev: '',
                    customer: ''
                };
            }
            else {
                console.log(this.patients.anterecord)
                this.db.getanterecord(this.patients.anterecord).then(anterecord => {
                    this.anterecord = anterecord;
                    console.log(this.anterecord);
                })
            }
        })


    }

    private _loadanterecords(): void {
        this.db.getConfigureanterecords()
            .then(configureanterecords => {
                this.configureanterecords = configureanterecords;
                //this.anterecords2 = new BehaviorSubject<any>(anterecords);
                this.pmh = this.configureanterecords.filter(data => data.category == 'Precious Medical History');
                this.refresh.next(this.pmh);
                this.childBirths = this.configureanterecords.filter(data => data.category == 'Child Birth');
                this.immunization = this.configureanterecords.filter(data => data.category == 'Immunization');
                this.primaryasses = this.configureanterecords.filter(data => data.category == 'Primary Assessment');
                //console.log(this.anterecords2);
            });

    }


    /*    onlineCheck() {
           this.online = true;
           this.http.get('https://sarutech.com/couchdblogin/auth/validate-username/xs2da2432252sda').subscribe(data => {
               this.online = true;
               console.log(data)
               console.log(this.online);
           },
               (err) => {
                   this.online = false;
                   console.log(this.online);
               });
       }
   
       click() {
           this.onlineCheck();
       } */

    newanterecord() {
        this.dialogRef = this.dialog.open(FuseanterecordsanterecordFormDialogComponent, {
            panelClass: 'anterecord-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
              
                if (this.db.category == "Precious Medical History") {
                    
                    this.pmh.push(response.getRawValue());
                }
                else if (this.db.category == "Child Birth") {
                    this.childBirths.push(response.getRawValue());
                }
                else if (this.db.category == "Immunization") {
                    this.immunization.push(response.getRawValue());
                }
                else if (this.db.category == "Primary Assessment") {
                    this.primaryasses.push(response.getRawValue());
                }
                //this.db.saveanterecord(response.getRawValue());
                this.refresh.next(true);
                //this.dataSource = new FilesDataSource(this.db);
                

            });

    }

    editanterecord(anterecord) {
        
        this.dialogRef = this.dialog.open(FuseanterecordsanterecordFormDialogComponent, {
            panelClass: 'anterecord-form-dialog',
            data: {
                anterecord: anterecord,
                action: 'edit'

            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':

                        //this.db.updateanterecord(formData.getRawValue());
                        //this.dataSource = new FilesDataSource(this.db);
                        
                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteanterecord(anterecord);

                        break;
                }
            });
    }

    /**
     * Delete anterecord
     */
    deleteanterecord(anterecord) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deleteanterecord(anterecord);
                //this.dataSource = new FilesDataSource(this.db);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(anterecordId) {
        this.db.toggleSelectedanterecord(anterecordId);
    }
    navConfigure() {
        this.router.navigate(['apps/anteconfigures']);
    }
    click(event) {
        
        this.checkboxstate = event;
    }

    submit() {
      
        this.patients.anterecord = this.anterecord.id;
        //this.customer.anterecord = this.anterecord.id;
        this.db.saveanterecord(this.anterecord, this.patients).then();
    }

    print() {
        /*   this.db.content = document.getElementById("printable").innerHTML;
          console.log(this.db.content) */
        /* window.document.write(this.db.content);
        window.document.close();
        window.print();
 */

        this.router.navigate(['pages/print', this.pid]);
    }

    printanterecord() {

        /*  const mainWindow = new BrowserWindow({
                width: 800,
                height: 600,
                webPreferences: {
                    nativeWindowOpen: true
                }
            })
    
            mainWindow.loadURL('file://' + __dirname + 'dist/src/app/main/content/apps/anterecords/anterecord-receipt/anterecord-receipt.component.ts');
            console.log(__dirname)
            mainWindow.show();
            setTimeout(function () {
                mainWindow.webContents.print();
            }, 1000); */


        /*  mainWindow.webContents.on('new-window', (event, url, frameName, disposition, options, additionalFeatures) => {
             if (frameName === 'Hospital Manager') {
                 // open window as modal
                 event.preventDefault()
                 Object.assign(options, {
                     modal: true,
                     parent: mainWindow,
                     width: 100,
                     height: 100
                 })
                 event.newGuest = new BrowserWindow(options)
             }
         })
  */
        // renderer process (mainWindow)
        /*   let modal = window.open('', 'Hospital Manager')
          modal.document.write('<h1>Hello</h1>') */
        /* 
                this.dialogRef = this.dialog.open(FuseanterecordreceiptsanterecordreceiptFormDialogComponent, {
                    panelClass: 'anterecord-receipt-dialog',
                    data: {
                        pid: this.pid
                    }
                })
                this.dialogRef.afterClosed() */

        //this.db.hide = "hidden";
        var win: any;
        win = window;
        var printContent = document.getElementById('printArea').innerHTML;
        var printWindow = win.open('', '_blank', 'top=0,left=0,height=auto,width=auto');
        printWindow.document.open();
        printWindow.document.write(`<html><head><title>Patient anterecord `, `</title><style>
            .avatar {
                                 font-size: 12px;
                                 padding-bottom: 2px;
                                 border-radius: 50%;
                                 height: 50px;
                                 width: 50px
                              }
                              .username {
                                     padding-left: 6px;
                                 }
                                 .print {
                            display: none;
                         }
                         .print2 {
                            display: none;
                         }
                          .hide {
                                 display: none;
                                 }
                             .invoice-name {
                                     padding-left: 6px;
                                 }
                                 .margin {
                                 font-size: 12px;
                                 padding-bottom: 6px;
                                 margin-bottom: 12px;
                                 }
 
                                  .margin2 {
                                 font-size: 12px;
                                 padding-bottom: 6px;
                                 margin-bottom: 30px;
                                 margin-left: 30px;
                                 }
 
                              .date {
                                     padding-left: 6px;
                                 }
                                 .info {
                                 color: rgba(0, 0, 0, 0.54);
                                 line-height: 22px;
                                 font-size: 12px;
                                 padding-bottom: 12px;
                             }
                             .issuer {
                             margin-right: -58px;
                             padding-right: 66px;
                             }
                         #co-size {
                         visibility: hidden   
                                   }
                     .invoice-table {
                             margin-top: 64px;
                             font-size: 15px;
                             }
                         .text-right{
                             padding-left: 150px;
                         }
                         .invoice-table-footer {
                             margin: 32px 0 72px 0;
                             }
                         td.line{
                            border-bottom: 1px solid rgba(0, 0, 0, 0.12);
                         }
                          td.mytotal {
                                         padding: 24px 8px;
                                         font-size: 35px;
                                         font-weight: 300;
                                         color: rgba(0, 0, 0, 1);
                                     }
                                     .alignright{
                                         padding-left: 100px;
                                     }
                                     #alignright{
                                         padding-left: 100px;
                                     }
           </style></head><body></body></html>`);
        printWindow.document.write(printContent);
        printWindow.document.close();
        printWindow.print();
        // window.print();
    }

}

/* export class FilesDataSource extends DataSource<any>
{
    anterecords2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: anterecordsService) {
        super();
    }

     //Connect function called by the table to retrieve one stream containing the data to render.
    connect(): Observable<any[]> {
        var result = Observable.fromPromise(this.db.getanterecords());
        result.subscribe(x => console.log(x), e => console.error(e));
        return result;
    }

    disconnect() {
    }
}
 */