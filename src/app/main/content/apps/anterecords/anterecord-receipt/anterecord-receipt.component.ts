import { Component, Inject, OnInit, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { anterecordsService } from '../anterecords.service';
import { StaffsService } from '../../staffs/staffs.service';
import { SettingsService } from '../../settings/settings.service';
import { CurrencysService } from '../../currencys/currencys.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { ConfigureanterecordsService } from '../../ante_records/ante_records.service';
import { Subject } from 'rxjs/Subject';
import { anterecord } from '../anterecord.model';
import { Configureanterecord } from '../../ante_records/ante_record.model';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-anterecord-receipts-anterecord-receipt-form-dialog',
    templateUrl: './anterecord-receipt.component.html',
    styleUrls: ['./anterecord-receipt.component.scss'],
    encapsulation: ViewEncapsulation.None,
})

export class FuseanterecordreceiptsanterecordreceiptFormDialogComponent implements OnInit {

    event: CalendarEvent;
    dialogTitle: string;
    anterecordreceiptForm: FormGroup;
    action: string;
    public settings;
    public result;
    public final;
    public hospitalName;
    public result2;
    public final2;
    public hospitalAddress;
    public result3;
    public final3;
    public hospitalPhone;
    public result4;
    public final4;
    public hospitalEmail;
    public Pid;
    public anterecordDate: any;
    public anterecordMonth: any;
    public anterecordNumber: any;
    public anterecordDueDate: any;
    public staffName: any;
    public basicSalary: any;
    public userType: any;
    public allowancess;
    public deductionss;
    public staffId;
    public staffAddress;
    public staffNumber;
    public configureanterecords: Array<Configureanterecord> = [];
    public staffEmail;
    public subTotal;
    public vat;
    patients;
    patientName;
    patientAddress;
    patientPhone;
    patientEmail;
    public discount;
    public total;
    checkboxstate;
    public currencys;
    public result5;
    public final5;
    public displayCurrencys;
    urlanterecord;
    image;
    general = [];
    refresh: Subject<any> = new Subject();
    medicalhistory = [];
    socialhistory = [];
    medicalconditions = [];
    currentmedications = [];
    familyhistory = [];
    notes = [];
    anterecordss: anterecord[];
    anterecord: any = {
        id: Math.round((new Date()).getTime()).toString(),
        rev: '',
        customer: ''
    };


    constructor(
        public dialogRef: MdDialogRef<FuseanterecordreceiptsanterecordreceiptFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        public db: PouchService,
        public _DomSanitizer: DomSanitizer,
        public router: Router,
        public activateRoute: ActivatedRoute
    ) {
        this.Pid = data.pid
        this.dialogTitle = 'anterecord Receipt';
        /* 
                if (this.action === 'edit') {
                    this.dialogTitle = 'Edit anterecord-receipt';
                    this.anterecord-receipt = data.anterecord-receipt;
                }
                else {
                    this.dialogTitle = 'New anterecord-receipt';
                    this.anterecord-receipt = {
                        id: '',
                        rev: '',
                        name: '',
                        description: '',
                        doctors: []
                    } */


        //this.anterecord-receiptForm = this.createanterecord-receiptForm();
    }


    ngOnInit() {
        this.db.getPStaffs().then(res => {
            this.click(event);
            this.checkboxstate = event.isTrusted
            
        })
        this.db.getPStaff2(this.Pid).then(res => {
            this.patients = res;
            
            this.patientName = res.name;
            this.patientAddress = res.address;
            this.patientPhone = res.phone;
            this.patientEmail = res.email;
            if (res._attachments) {
                this.image = true;
                this.urlanterecord = URL.createObjectURL(res._attachments);
                
            }
            else {
                this.image = false
            }
            
            if (this.patients.anterecord == "") {
                this.anterecord = {
                    id: Math.round((new Date()).getTime()).toString(),
                    rev: '',
                    customer: ''
                };
            }
            else {
                this.db.getanterecord(this.patients.anterecord).then(anterecord => {
                    this.anterecord = anterecord;
                    
                })
            }
        })
        this._loadanterecords();

    }

    private _loadanterecords(): void {
        this.db.getConfigureanterecords()
            .then(configureanterecords => {
                this.configureanterecords = configureanterecords;
                //this.anterecords2 = new BehaviorSubject<any>(anterecords);
                this.general = this.configureanterecords.filter(data => data.category == 'General');
                this.refresh.next(this.general);
                this.medicalhistory = this.configureanterecords.filter(data => data.category == 'Medical History');
                this.socialhistory = this.configureanterecords.filter(data => data.category == 'Social History');
                this.medicalconditions = this.configureanterecords.filter(data => data.category == 'Medical Conditions');
                this.currentmedications = this.configureanterecords.filter(data => data.category == 'Current Medications');
                this.familyhistory = this.configureanterecords.filter(data => data.category == 'Family History');
                this.notes = this.configureanterecords.filter(data => data.category == 'Notes');

                //console.log(this.anterecords2);
            });

    }

    click(event) {
        
        this.checkboxstate = event;
    }

    print() {
      /*   this.db.content = document.getElementById("printable").innerHTML;
        console.log(this.db.content) */
        /* window.document.write(this.db.content);
        window.document.close();
        window.print();
 */
     this.router.navigate(['pages/print']);
    }

    createanterecordreceiptForm() {
        /*  return this.formBuilder.group({
             id: [this.anterecord-receipt.id],
             rev: [this.anterecord-receipt.rev],
             name: [this.anterecord-receipt.name],
             description: [this.anterecord-receipt.description],
 
         });*/
    }
}
