import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { anterecordsService } from './anterecords.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector: 'fuse-anterecords',
    templateUrl: './anterecords.component.html',
    styleUrls: ['./anterecords.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [Animations.slideInTop]
})
export class FuseanterecordsComponent implements OnInit {
    hasSelectedanterecords: boolean;
    searchInput: FormControl;

    constructor(private anterecordsService: PouchService) {
        this.searchInput = new FormControl('');
       
    }

    ngOnInit() {

        this.anterecordsService.onSelectedanterecordsChanged
            .subscribe(selectedanterecords => {
                this.hasSelectedanterecords = selectedanterecords.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.anterecordsService.onSearchTextChanged.next(searchText);
            });
    }

}
