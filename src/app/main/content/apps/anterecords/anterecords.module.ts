import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseanterecordsComponent } from './anterecords.component';
import { anterecordsService } from './anterecords.service';
//import { UserService } from '../../../../user.service';
import { ConfigureanterecordsService } from './../ante_records/ante_records.service';
import { FuseanterecordsanterecordListComponent } from './anterecord-list/anterecord-list.component';
import { FuseanterecordsSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseanterecordsanterecordFormDialogComponent } from './anterecord-form/anterecord-form.component';
import { antenatalsService } from './../antenatal/antenatals.service';
import {FuseanterecordreceiptsanterecordreceiptFormDialogComponent} from './anterecord-receipt/anterecord-receipt.component';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/anterecords/:id',
        component: FuseanterecordsComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            anterecords: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseanterecordsComponent,
        FuseanterecordsanterecordListComponent,
        FuseanterecordsSelectedBarComponent,
        FuseanterecordsanterecordFormDialogComponent,
        FuseanterecordreceiptsanterecordreceiptFormDialogComponent
    ],
    providers      : [
        anterecordsService,
        antenatalsService,
        AuthGuard,
        ConfigureanterecordsService,
        PouchService
       //UserService
    ],
    entryComponents: [FuseanterecordsanterecordFormDialogComponent,FuseanterecordreceiptsanterecordreceiptFormDialogComponent]
})
export class FuseanterecordsModule
{
}
