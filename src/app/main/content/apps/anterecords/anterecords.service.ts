/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import { Staff } from '../staffs/staff.model';
import { StaffsService } from '../staffs/staffs.service';
declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { anterecord } from './anterecord.model';
import { Schema } from '../schema';

@Injectable()
export class anterecordsService {
    public anterecord = "";
    public sCredentials;
    public category;
    public file;
    public retrieve;
    public content: any;
    onanterecordsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedanterecordsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    anterecords: anterecord[];
    staffs: Staff[];
    user: any;
    selectedanterecords: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    public db2: StaffsService
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http, ) {
        this.initDB(this.sCredentials);
    }

    /**
     * The anterecords App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getanterecords()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getanterecords();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getanterecords();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('abayelsa', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/abayelsa';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/abayelsa';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }

     /**
    * Update a staff
    * @param {staff} staff
    *
    * @return Promise<staff>
    */
    updateStaff(staff: Staff) {

            return this.db.rel.save('staff', staff)
                .then((data: any) => {
                    if (data && data.staffs && data.staffs
                    [0]) {
                        console.log(data.staffs[0])
                        return data.staffs[0]
                    }
                    return null;
                }).catch((err: any) => {
                    console.error(err);
                });

        
      
    }

    /***********
     * anterecord
     **********/
    /**
     * Save a anterecord
     * @param {anterecord} anterecord
     *
     * @return Promise<anterecord>
     */
    saveanterecord(anterecord: anterecord, staff: Staff): Promise<anterecord> {
        //anterecord.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('anterecord', anterecord)
            .then((data: any) => {
                console.log(staff);
                this.updateStaff(staff);
                /*   if (this.file) {
                      this.file;
                      
                      this.retrieve = (<HTMLInputElement>this.file).files[0];
  
                      if (this.retrieve = (<HTMLInputElement>this.file).files[0]) {
                          staff._attachments = this.retrieve
                      }
  
                      else {
                          staff._attachments;
                          //this.getStaff = staff._attachments;
                      }
  
                      return this.db.rel.save('staff', staff)
                          .then((data: any) => {
                              if (data && data.staffs && data.staffs
                              [0]) {
                                   this.db.rel.putAttachment('staff', { id: staff.id, rev: staff.rev }, 'file', staff._attachments, 'image/png').then(res => {
                                      var reader: FileReader = new FileReader();
  
                                      reader.onloadend = function (e) {
                                          var base64data = reader.result;
                                          staff.image = base64data
                                          //console.log(staff.image);
                                      }
                                      reader.readAsDataURL(this.retrieve);
                                      staff._attachments = res;
                                      staff.image = res
  
                                  }) 
                                  
                                  return data.staffs[0]
                              }
  
                              return null;
                          }).catch((err: any) => {
                              console.error(err);
                          });
  
                  }
                  else {
                      
  
                      return this.db.rel.save('staff', staff)
                          .then((data: any) => {
                              if (data && data.staffs && data.staffs
                              [0]) {
                                   this.db.rel.putAttachment('staff', { id: staff.id, rev: staff.rev }, 'file', staff._attachments, 'image/png').then(res => {
                                      staff._attachments = res
                                  }) 
                                  
                                  return data.staffs[0]
                              }
  
                              return null;
                          }).catch((err: any) => {
                              console.error(err);
                          });
                  } */
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a anterecord
    * @param {anterecord} anterecord
    *
    * @return Promise<anterecord>
    */
    updateanterecord(anterecord: anterecord) {
        return this.db.rel.save('anterecord', anterecord)
            .then((data: any) => {
                if (data && data.anterecords && data.anterecords
                [0]) {
                    console.log('Update')

                    return data.anterecords[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a anterecord
     * @param {anterecord} anterecord
     *
     * @return Promise<boolean>
     */
    removeanterecord(anterecord: anterecord): Promise<boolean> {
        return this.db.rel.del('anterecord', anterecord)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the anterecords
     *
     * @return Promise<Array<anterecord>>
     */
    getanterecords(): Promise<Array<anterecord>> {
        return this.db.rel.find('anterecord')
            .then((data: any) => {
                this.anterecords = data.anterecords;
                if (this.searchText && this.searchText !== '') {
                    this.anterecords = FuseUtils.filterArrayByString(this.anterecords, this.searchText);
                }
                //this.onanterecordsChanged.next(this.anterecords);
                return Promise.resolve(this.anterecords);
                //return data.anterecords ? data.anterecords : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getDoctoranterecords(): Promise<Array<anterecord>> {
        return this.db.rel.find('anterecord')
            .then((data: any) => {
                return data.anterecords ? data.anterecords : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a anterecord
     * @param {anterecord} anterecord
     *
     * @return Promise<anterecord>
     */
    getanterecord(id): Promise<anterecord> {
        return this.db.rel.find('anterecord', id)
            .then((data: any) => {
                console.log("Get")

                return data && data.anterecords ? data.anterecords[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected anterecord by id
     * @param id
     */
    toggleSelectedanterecord(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedanterecords.length > 0) {
            const index = this.selectedanterecords.indexOf(id);

            if (index !== -1) {
                this.selectedanterecords.splice(index, 1);

                // Trigger the next event
                this.onSelectedanterecordsChanged.next(this.selectedanterecords);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedanterecords.push(id);

        // Trigger the next event
        this.onSelectedanterecordsChanged.next(this.selectedanterecords);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedanterecords.length > 0) {
            this.deselectanterecords();
        }
        else {
            this.selectanterecords();
        }
    }

    selectanterecords(filterParameter?, filterValue?) {
        this.selectedanterecords = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedanterecords = [];
            this.anterecords.map(anterecord => {
                this.selectedanterecords.push(anterecord.id);
            });
        }
        else {
            /* this.selectedanterecords.push(...
                 this.anterecords.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedanterecordsChanged.next(this.selectedanterecords);
    }





    deselectanterecords() {
        this.selectedanterecords = [];

        // Trigger the next event
        this.onSelectedanterecordsChanged.next(this.selectedanterecords);
    }

    deleteanterecord(anterecord) {
        this.db.rel.del('anterecord', anterecord)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const anterecordIndex = this.anterecords.indexOf(anterecord);
                this.anterecords.splice(anterecordIndex, 1);
                this.onanterecordsChanged.next(this.anterecords);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedanterecords() {
        for (const anterecordId of this.selectedanterecords) {
            const anterecord = this.anterecords.find(_anterecord => {
                return _anterecord.id === anterecordId;
            });

            this.db.rel.del('anterecord', anterecord)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const anterecordIndex = this.anterecords.indexOf(anterecord);
                    this.anterecords.splice(anterecordIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onanterecordsChanged.next(this.anterecords);
        this.deselectanterecords();
    }
}
