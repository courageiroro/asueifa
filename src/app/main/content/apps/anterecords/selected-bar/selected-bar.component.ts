import { Component, OnInit } from '@angular/core';
import { anterecordsService } from '../anterecords.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseanterecordsSelectedBarComponent implements OnInit
{
    selectedanterecords: string[];
    hasSelectedanterecords: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private anterecordsService: PouchService,
        public dialog: MdDialog
    )
    {
        this.anterecordsService.onSelectedanterecordsChanged
            .subscribe(selectedanterecords => {
                this.selectedanterecords = selectedanterecords;
                setTimeout(() => {
                    this.hasSelectedanterecords = selectedanterecords.length > 0;
                    this.isIndeterminate = (selectedanterecords.length !== this.anterecordsService.anterecords.length && selectedanterecords.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.anterecordsService.selectanterecords();
    }

    deselectAll()
    {
        this.anterecordsService.deselectanterecords();
    }

    deleteSelectedanterecords()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected anterecords?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.anterecordsService.deleteSelectedanterecords();
            }
            this.confirmDialogRef = null;
        });
    }

}
