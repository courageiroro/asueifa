import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent,CalendarEvent2 } from 'angular-calendar';
import { FormBuilder,FormControl,FormGroup } from '@angular/forms';
import { BehaviorSubject } from 'rxjs/Rx';
import { MatColors } from '../../../../../core/matColors';
import 'rxjs/Rx';
import { ColorPickerService } from 'angular4-color-picker';
import { Appointment } from '../../addappointments/addappointment.model';;
import {StaffsService} from '../../staffs/staffs.service'
import {PouchService} from '../../../../../provider/pouch-service';
//declare var jquery: any;
declare var $: any;

@Component({
    selector: 'fuse-appointments-appointment-form-dialog',
    templateUrl: './appointment-form.component.html',
    styleUrls: ['./appointment-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseAppointmentsAppointmentFormDialogComponent implements OnInit {

    addappointments: CalendarEvent2;
    addappointment: Appointment;
    dialogTitle: string;
    addappointmentForm: FormGroup;
    action: string;
    presetColors = MatColors.presets;
    patientss: any;
    doctorss: any;
    statuss: any;
    appointment: Appointment


    /* event: CalendarEvent;
    dialogTitle: string;
    appointmentForm: FormGroup;
    action: string;
    staffs: any;
    patients: any;
    appointment: Appointment;
    statuss = []; */

    constructor(
        public dialogRef: MdDialogRef<FuseAppointmentsAppointmentFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        private cpService: ColorPickerService,
        private db:PouchService
    ) {
        //this.addappointments = data.addappointments;
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Appointment';
            this.appointment = data.appointment;
        }
        else {
            this.dialogTitle = 'New Appointment';
           /*  this.appointment = {
                id: '',
                rev: '',
                title:'',
                description:'',
                start: new Date,
                end: new Date,
                startTime:'',
                endTime:'',
                color:{primary:'',secondary:''},
                noticeboards:[],
                doctor_id:'',
                patient_id:'',
                status:''



            } */
        }

        this.addappointmentForm = this.createAddappointmentForm();
    }

    ngOnInit() {
        this.statuss = ['Available', 'Booked'];
        this.db.getDStaffs().then(res=>{
            this.doctorss = res
        })
        this.db.getPStaffs().then(res=>{
            this.patientss = res
        })
    }

   createAddappointmentForm() {
        return this.formBuilder.group({
            id: [this.appointment.id],
            rev: [this.appointment.rev],
            title: [this.appointment.title],
            start: [this.appointment.start],
            end: [this.appointment.end],
            doctor_id: [this.appointment.doctor_id],
            patient_id: [this.appointment.patient_id],
            startTime: [this.appointment.startTime],
            endTime: [this.appointment.endTime],
            status: [this.appointment.status],
            description: [this.appointment.description],
            color: this.formBuilder.group({
                primary: [this.appointment.color.primary],
                secondary: [this.appointment.color.secondary]
            }),
        });
    }
}
