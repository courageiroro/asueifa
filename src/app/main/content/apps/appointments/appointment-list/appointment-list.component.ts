import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AddappointmentsService } from '../../addappointments/addappointments.service';
import { AppointmentsService } from '../appointments.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { Router, RouterModule } from '@angular/router';
import { FuseAppointmentsAppointmentFormDialogComponent } from '../appointment-form/appointment-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import {
    CalendarEvent2,
    CalendarEvent,
    CalendarEventAction,
    CalendarEventTimesChangedEvent,
    CalendarMonthViewDay
} from 'angular-calendar';
import { Appointment } from '../../addappointments/addappointment.model';
import {PouchService} from '../../../../../provider/pouch-service';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';

@Component({
    selector: 'fuse-appointments-appointment-list',
    templateUrl: './appointment-list.component.html',
    styleUrls: ['./appointment-list.component.scss']
})
export class FuseAppointmentsAppointmentListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public localStorageItem: any;
    public localStorageType: any;
    public appointments: Array<Appointment> = [];
    appointments2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'title', 'doctor', 'patient', 'status', 'starttime', 'endtime', 'buttons'];
    selectedAppointments: any[];
    checkboxes: {};
    addappointmentss: CalendarEvent[];
    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;

    constructor(public dialog: MdDialog, public db: PouchService, private router: Router) {
        this.localStorageItem = JSON.parse(localStorage.getItem('user'));
        this.localStorageType = this.localStorageItem.usertype;
        
    }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        this._loadAppointments();
    }

    private _loadAppointments(): void {
        this.db.onAddappointmentsChanged.subscribe(data => {
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        this.db.getAddappointments()
            .then((appointments: Array<Appointment>) => {
                this.appointments = appointments;
                this.appointments2 = new BehaviorSubject<any>(appointments);
                //console.log(this.appointments2);

                this.checkboxes = {};
                appointments.map(appointment => {
                    this.checkboxes[appointment.id] = false;
                });
                this.db.onSelectedAddappointmentsChanged.subscribe(selectedAppointments => {
                    for (const id in this.checkboxes) {
                        this.checkboxes[id] = selectedAppointments.includes(id);
                    }

                    this.selectedAppointments = selectedAppointments;
                });
                this.db.onSearchTextChanged.subscribe(data => {
                    this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                })
            });

    }

    newAppointment() {

        this.router.navigate(['apps/addappointments']);
        /*  this.dialogRef = this.dialog.open(FuseAppointmentsAppointmentFormDialogComponent, {
             panelClass: 'appointment-form-dialog',
             data: {
                 action: 'new'
             }
         });
 
         this.dialogRef.afterClosed()
             .subscribe((response: FormGroup) => {
                 if (!response) {
                     return;
                 }
 
                 var res = response.getRawValue();
                 this.db.saveAddappointment(res);
                 this.dataSource = new FilesDataSource(this.db);
 
             }); */

    }

    editAppointment(appointment) {
        
        this.dialogRef = this.dialog.open(FuseAppointmentsAppointmentFormDialogComponent, {
            panelClass: 'appointment-form-dialog',
            data: {
                appointment: appointment,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                var form = formData.getRawValue();
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':
                        this.db.updateAddappointment(form);
                        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteAppointment(appointment);

                        break;
                }
            });
    }

    /**
     * Delete appointment
     */
    deleteAppointment(appointment) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deleteAddappointment(appointment);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(appointmentId) {
        this.db.toggleSelectedAddappointment(appointmentId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    appointments2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            of(this.db.appointments).delay(5000),
            this.sort.mdSortChange,
            this._paginator.page,
        ];

        //this._paginator.length = this.db.appointments.length;
        var result = Observable.fromPromise(this.db.getAddappointments());
        /* result.subscribe(x => console.log(x), e => console.error(e));
        return result; */

        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.appointments]));
        });
    }

    disconnect() {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
          return data;
        }
    
        return data.sort((a, b) => {
          const isAsc = this.sort.direction === 'asc';
          switch (this.sort.active) {
            case 'title': return compare(a.title, b.title, isAsc);
            case 'doctor': return compare(+a.doctor_id, +b.doctor_id, isAsc);
            case 'patient': return compare(+a.patient_id, +b.patient_id, isAsc);
            default: return 0;
          }
        });
      }
}

 /** Simple sort comparator for example ID/Name columns (for client-side sorting). */
 function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
