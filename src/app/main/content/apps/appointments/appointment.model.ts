export interface Appointment
{
	id: string,
	rev: string,
	timestamp: string,
	doctor_id: string,
	patient_id: string,
	status: string,
}


