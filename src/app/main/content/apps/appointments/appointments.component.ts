import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AppointmentsService } from './appointments.service';
import { AddappointmentsService } from './../addappointments/addappointments.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-appointments',
    templateUrl  : './appointments.component.html',
    styleUrls    : ['./appointments.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FuseAppointmentsComponent implements OnInit
{
    hasSelectedAppointments: boolean;
    searchInput: FormControl;

    constructor(private db: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        
        this.db.onSelectedAddappointmentsChanged
            .subscribe(selectedAppointments => {
                this.hasSelectedAppointments = selectedAppointments.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.db.onSearchTextChanged.next(searchText);
            });
            
    }

}
