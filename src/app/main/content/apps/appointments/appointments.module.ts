import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseAppointmentsComponent } from './appointments.component';
import { AddappointmentsService } from './../addappointments/addappointments.service';
import { AppointmentsService } from './../appointments/appointments.service';
import { FuseAppointmentsAppointmentListComponent } from './appointment-list/appointment-list.component';
import { FuseAppointmentsSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseAppointmentsAppointmentFormDialogComponent } from './appointment-form/appointment-form.component';
import {StaffsService} from './../staffs/staffs.service'
import { CalendarModule } from 'angular-calendar';
import {ColorPickerModule} from 'angular4-color-picker';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/appointments',
        component: FuseAppointmentsComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            appointments: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes),
        CalendarModule.forRoot(),
        ColorPickerModule
    ],
    declarations   : [
        FuseAppointmentsComponent,
        FuseAppointmentsAppointmentListComponent,
        FuseAppointmentsSelectedBarComponent,
        FuseAppointmentsAppointmentFormDialogComponent
    ],
    providers      : [
        AddappointmentsService,
        AppointmentsService,
        StaffsService,
        PouchService,
        AuthGuard
    ],
    entryComponents: [FuseAppointmentsAppointmentFormDialogComponent]
})
export class FuseAppointmentsModule
{
}
