/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Appointment } from './appointment.model';
import { Schema } from '../schema';

@Injectable()
export class AppointmentsService {
    public sCredentials;

    onAppointmentsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedAppointmentsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    public inccat = "";

    appointments: Appointment[];
    user: any;
    selectedAppointments: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The appointments App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getAppointments()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getAppointments();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getAppointments();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('abayelsa', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/abayelsa';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/abayelsa';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }

    /***********
     * appointment
     **********/
    /**
     * Save a appointment
     * @param {appointment} appointment
     *
     * @return Promise<appointment>
     */
    saveAppointment(appointment: Appointment): Promise<Appointment> {
        appointment.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('appointment', appointment)
            .then((data: any) => {
                if (data && data.appointments && data.appointments[0]) {
                    return data.appointments[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a appointment
    * @param {appointment} appointment
    *
    * @return Promise<appointment>
    */
    updateAppointment(appointment: Appointment) {
        return this.db.rel.save('appointment', appointment)
            .then((data: any) => {
                if (data && data.appointments && data.appointments
                [0]) {
                    return data.appointments[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a appointment
     * @param {appointment} Appointment
     *
     * @return Promise<boolean>
     */
    removeAppointment(appointment: Appointment): Promise<boolean> {
        return this.db.rel.del('appointment', appointment)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the appointments
     *
     * @return Promise<Array<appointment>>
     */
    getAppointments(): Promise<Array<Appointment>> {
        return this.db.rel.find('appointment')
            .then((data: any) => {
                this.appointments = data.appointments;
                if (this.searchText && this.searchText !== '') {
                    this.appointments = FuseUtils.filterArrayByString(this.appointments, this.searchText);
                }
                //this.onappointmentsChanged.next(this.appointments);
                return Promise.resolve(this.appointments);
                //return data.appointments ? data.appointments : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a appointment
     * @param {appointment} appointment
     *
     * @return Promise<appointment>
     */
    getAppointment(appointment: Appointment): Promise<Appointment> {
        return this.db.rel.find('appointment', appointment.id)
            .then((data: any) => {
                return data && data.appointments ? data.appointments[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected appointment by id
     * @param id
     */
    toggleSelectedAppointment(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedAppointments.length > 0) {
            const index = this.selectedAppointments.indexOf(id);

            if (index !== -1) {
                this.selectedAppointments.splice(index, 1);

                // Trigger the next event
                this.onSelectedAppointmentsChanged.next(this.selectedAppointments);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedAppointments.push(id);

        // Trigger the next event
        this.onSelectedAppointmentsChanged.next(this.selectedAppointments);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedAppointments.length > 0) {
            this.deselectAppointments();
        }
        else {
            this.selectAppointments();
        }
    }

    selectAppointments(filterParameter?, filterValue?) {
        this.selectedAppointments = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedAppointments = [];
            this.appointments.map(appointment => {
                this.selectedAppointments.push(appointment.id);
            });
        }
        else {
            /* this.selectedappointments.push(...
                 this.appointments.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedAppointmentsChanged.next(this.selectedAppointments);
    }





    deselectAppointments() {
        this.selectedAppointments = [];

        // Trigger the next event
        this.onSelectedAppointmentsChanged.next(this.selectedAppointments);
    }

    deleteAppointment(appointment) {
        this.db.rel.del('appointment', appointment)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const appointmentIndex = this.appointments.indexOf(appointment);
                this.appointments.splice(appointmentIndex, 1);
                this.onAppointmentsChanged.next(this.appointments);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedAppointments() {
        for (const appointmentId of this.selectedAppointments) {
            const appointment = this.appointments.find(_appointment => {
                return _appointment.id === appointmentId;
            });

            this.db.rel.del('appointment', appointment)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const appointmentIndex = this.appointments.indexOf(appointment);
                    this.appointments.splice(appointmentIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onAppointmentsChanged.next(this.appointments);
        this.deselectAppointments();
    }
}
