import { Component, OnInit } from '@angular/core';
import { AddappointmentsService } from '../../addappointments/addappointments.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseAppointmentsSelectedBarComponent implements OnInit
{
    selectedAppointments: string[];
    hasSelectedAppointments: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private appointmentsService: PouchService,
        public dialog: MdDialog
    )
    {
        this.appointmentsService.onSelectedAddappointmentsChanged
            .subscribe(selectedAppointments => {
                this.selectedAppointments = selectedAppointments;
                setTimeout(() => {
                    this.hasSelectedAppointments = selectedAppointments.length > 0;
                    this.isIndeterminate = (selectedAppointments.length !== this.appointmentsService.addappointments.length && selectedAppointments.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.appointmentsService.selectAddappointments();
    }

    deselectAll()
    {
        this.appointmentsService.deselectAddappointments();
    }

    deleteSelectedAppointments()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected appointments?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.appointmentsService.deleteSelectedAddappointments();
            }
            this.confirmDialogRef = null;
        });
    }

}
