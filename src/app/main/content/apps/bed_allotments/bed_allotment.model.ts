export interface Bed_allotment
{
	id: string,
	rev: string,
	bed_id: string,
	bed_number: string,
	bed_type: string,
	patient_id: string,
	pidentity: string,
	allotment_timestamp: any,
	discharge_timestamp: any,
	allotintimestamp:any,
	disintimestamp:any
}

