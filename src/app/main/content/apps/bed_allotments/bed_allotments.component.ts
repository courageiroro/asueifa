import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Animations } from '../../../../core/animations';
import { Bed_allotmentsService } from './bed_allotments.service';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-bed_allotments',
    templateUrl  : './bed_allotments.component.html',
    styleUrls    : ['./bed_allotments.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FuseBed_allotmentsComponent implements OnInit
{
    hasSelectedBed_allotments: boolean;
    searchInput: FormControl;

    constructor(private bed_allotmentsService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.bed_allotmentsService.onSelectedBed_allotmentsChanged
            .subscribe(selectedBed_allotments => {
                this.hasSelectedBed_allotments = selectedBed_allotments.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.bed_allotmentsService.onSearchTextChanged.next(searchText);
            });
    }

}
