import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseBed_allotmentsComponent } from './bed_allotments.component';
import { Bed_allotmentsService } from './bed_allotments.service';
import { FuseBed_allotmentsBed_allotmentListComponent } from './bed_allotment-list/bed_allotment-list.component';
import { FuseBed_allotmentsSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseBed_allotmentsBed_allotmentFormDialogComponent } from './bed_allotment-form/bed_allotment-form.component';
import { BedsService } from './../beds/beds.service';
import { PatientsService } from './../patients/patients.service';
import { StaffsService } from './../staffs/staffs.service';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/bed_allotments',
        component: FuseBed_allotmentsComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            bed_allotments: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseBed_allotmentsComponent,
        FuseBed_allotmentsBed_allotmentListComponent,
        FuseBed_allotmentsSelectedBarComponent,
        FuseBed_allotmentsBed_allotmentFormDialogComponent
    ],
    providers      : [
        Bed_allotmentsService,
        BedsService,
        StaffsService,
        PouchService,
        AuthGuard
    ],
    entryComponents: [FuseBed_allotmentsBed_allotmentFormDialogComponent]
})
export class FuseBed_allotmentsModule
{
}
