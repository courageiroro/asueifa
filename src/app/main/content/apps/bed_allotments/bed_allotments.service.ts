/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import { BedsService } from './../beds/beds.service';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Bed_allotment } from './bed_allotment.model';
import { Schema } from '../schema';

@Injectable()
export class Bed_allotmentsService {

    public allotmentName = "";
    public sCredentials;

    onBed_allotmentsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedBed_allotmentsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    bed_allotments: Bed_allotment[];
    user: any;
    selectedBed_allotments: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http, public db2: BedsService) {
        this.initDB(this.sCredentials);
    }

    /**
     * The bed_allotments App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getBed_allotments()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getBed_allotments();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getBed_allotments();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('abayelsa', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/abayelsa';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/abayelsa';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }


    /***********
     * bed_allotment
     **********/
    /**
     * Save a bed_allotment
     * @param {bed_allotment} Bed_allotment
     *
     * @return Promise<bed_allotment>
     */
    saveBed_allotment(bed_allotment: Bed_allotment): Promise<Bed_allotment> {
        bed_allotment.id = Math.floor(Date.now()).toString();
        return this.db.rel.save('bed_allotment', bed_allotment)
            .then((data: any) => {
                if (data && data.bed_allotments && data.bed_allotments
                [0]) {

                    return data.bed_allotments[0]

                };

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
    * Update a bed_allotment
    * @param {bed_allotment} Bed_allotment
    *
    * @return Promise<Bed_allotment>
    */
    updateBed_allotment(bed_allotment: Bed_allotment) {
        return this.db.rel.save('bed_allotment', bed_allotment)
            .then((data: any) => {
                if (data && data.bed_allotments && data.bed_allotments
                [0]) {
                    return data.bed_allotments[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a bed_allotment
     * @param {bed_allotment} Bed_allotment
     *
     * @return Promise<boolean>
     */
    removeBed_allotment(bed_allotment: Bed_allotment): Promise<boolean> {
        return this.db.rel.del('bed_allotment', bed_allotment)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the bed_allotments
     *
     * @return Promise<Array<bed_allotment>>
     */
    getBed_allotments(): Promise<Array<Bed_allotment>> {
        var localStorageItem = JSON.parse(localStorage.getItem('user'));
        return this.db.rel.find('bed_allotment')
            .then((data: any) => {
                this.bed_allotments = data.bed_allotments;
                if (this.searchText && this.searchText !== '') {
                    this.bed_allotments = FuseUtils.filterArrayByString(this.bed_allotments, this.searchText);
                }
                //this.onbed_allotmentsChanged.next(this.bed_allotments);
                var userId = localStorageItem.id
                if (localStorageItem.usertype == "Patient") {
                    this.bed_allotments = this.bed_allotments.filter(data => data.pidentity == userId)
                }
                return Promise.resolve(this.bed_allotments);
                //return data.bed_allotments ? data.bed_allotments : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a bed_allotment
     * @param {bed_allotment} bed_allotment
     *
     * @return Promise<bed_allotment>
     */
    getBed_allotment(bed_allotment: Bed_allotment): Promise<Bed_allotment> {
        return this.db.rel.find('bed_allotment', bed_allotment.id)
            .then((data: any) => {
                return data && data.bed_allotments ? data.bed_allotments[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected bed_allotment by id
     * @param id
     */
    toggleSelectedBed_allotment(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedBed_allotments.length > 0) {
            const index = this.selectedBed_allotments.indexOf(id);

            if (index !== -1) {
                this.selectedBed_allotments.splice(index, 1);

                // Trigger the next event
                this.onSelectedBed_allotmentsChanged.next(this.selectedBed_allotments);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedBed_allotments.push(id);

        // Trigger the next event
        this.onSelectedBed_allotmentsChanged.next(this.selectedBed_allotments);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedBed_allotments.length > 0) {
            this.deselectBed_allotments();
        }
        else {
            this.selectBed_allotments();
        }
    }

    selectBed_allotments(filterParameter?, filterValue?) {
        this.selectedBed_allotments = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedBed_allotments = [];
            this.bed_allotments.map(bed_allotment => {
                this.selectedBed_allotments.push(bed_allotment.id);
            });
        }
        else {
            /* this.selectedbed_allotments.push(...
                 this.bed_allotments.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedBed_allotmentsChanged.next(this.selectedBed_allotments);
    }





    deselectBed_allotments() {
        this.selectedBed_allotments = [];

        // Trigger the next event
        this.onSelectedBed_allotmentsChanged.next(this.selectedBed_allotments);
    }

    deleteBed_allotment(bed_allotment) {
        this.db.rel.del('bed_allotment', bed_allotment)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const bed_allotmentIndex = this.bed_allotments.indexOf(bed_allotment);
                this.bed_allotments.splice(bed_allotmentIndex, 1);
                this.onBed_allotmentsChanged.next(this.bed_allotments);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedBed_allotments() {
        for (const bed_allotmentId of this.selectedBed_allotments) {
            const bed_allotment = this.bed_allotments.find(_bed_allotment => {
                return _bed_allotment.id === bed_allotmentId;
            });

            this.db.rel.del('bed_allotment', bed_allotment)
                .then((data: any) => {
                    this.db2.getBed2(bed_allotment.bed_number).then(data=>{
                        data.status = "Available"
                        this.db2.updateBed(data);
                    })
                    //return data && data.deleted ? data.deleted: false;
                    const bed_allotmentIndex = this.bed_allotments.indexOf(bed_allotment);
                    this.bed_allotments.splice(bed_allotmentIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onBed_allotmentsChanged.next(this.bed_allotments);
        this.deselectBed_allotments();
    }
}
