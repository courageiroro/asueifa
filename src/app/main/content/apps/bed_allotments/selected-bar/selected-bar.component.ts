import { Component, OnInit } from '@angular/core';
import { Bed_allotmentsService } from '../bed_allotments.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseBed_allotmentsSelectedBarComponent implements OnInit
{
    selectedBed_allotments: string[];
    hasSelectedBed_allotments: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private bed_allotmentsService: PouchService,
        public dialog: MdDialog
    )
    {
        this.bed_allotmentsService.onSelectedBed_allotmentsChanged
            .subscribe(selectedBed_allotments => {
                this.selectedBed_allotments = selectedBed_allotments;
                setTimeout(() => {
                    this.hasSelectedBed_allotments = selectedBed_allotments.length > 0;
                    this.isIndeterminate = (selectedBed_allotments.length !== this.bed_allotmentsService.bed_allotments.length && selectedBed_allotments.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.bed_allotmentsService.selectBed_allotments();
    }

    deselectAll()
    {
        this.bed_allotmentsService.deselectBed_allotments();
    }

    deleteSelectedBed_allotments()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected bed_allotments?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.bed_allotmentsService.deleteSelectedBed_allotments();
            }
            this.confirmDialogRef = null;
        });
    }

}
