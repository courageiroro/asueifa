import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Bed } from '../bed.model';

@Component({
    selector: 'fuse-beds-bed-form-dialog',
    templateUrl: './bed-form.component.html',
    styleUrls: ['./bed-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseBedsBedFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    bedForm: FormGroup;
    action: string;
    bed: Bed;
    statuss: any;

    constructor(
        public dialogRef: MdDialogRef<FuseBedsBedFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Income Category';
            this.bed = data.bed;
        }
        else {
            this.dialogTitle = 'New Income Category';
            this.bed = {
                id: '',
                rev: '',
                bed_number: '',
                type: '',
                status: '',
                description: '',
                bed_allotments: []
            }
        }

        this.bedForm = this.createBedForm();
    }

    ngOnInit() {
         this.statuss = ['Available', 'Occupied']
    }

    createBedForm() {
        return this.formBuilder.group({
            id: [this.bed.id],
            rev: [this.bed.rev],
            bed_number: [this.bed.bed_number],
            type: [this.bed.type],
            status: [this.bed.status],
            description: [this.bed.description]

        });
    }
}
