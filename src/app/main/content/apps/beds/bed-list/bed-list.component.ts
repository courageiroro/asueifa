import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { BedsService } from '../beds.service';
import { Bed_allotmentsService } from '../../bed_allotments/bed_allotments.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { FuseBedsBedFormDialogComponent } from '../bed-form/bed-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Bed } from '../bed.model';
import { PouchService } from '../../../../../provider/pouch-service';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';

@Component({
    selector: 'fuse-beds-bed-list',
    templateUrl: './bed-list.component.html',
    styleUrls: ['./bed-list.component.scss']
})
export class FuseBedsBedListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public beds: Array<Bed> = [];
    beds2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'number', 'type', 'description', 'status', 'buttons'];
    selectedBeds: any[];
    checkboxes: {};

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;

    constructor(public dialog: MdDialog, public db: PouchService) {
        var result;
        var array = [];
        var todayDate = new Date();
        var unixTodayDate = new Date(todayDate).getTime();


        db.getBed_allotments().then(res => {
            res = res.filter(data => (data.disintimestamp) > unixTodayDate)
            for (var i = 0; i <= res.length - 1; i++) {

                var bedId = res[i].bed_number;
                this.db.getBed2(bedId).then(data => {
                    data.status = "Occupied";
                    this.db.updateBed(data).then(res => {
                    })
                })

            }
        })

        db.getBed_allotments().then(res => {
            res = res.filter(data => (data.disintimestamp) < unixTodayDate)
            for (var i = 0; i <= res.length - 1; i++) {
                var bedId = res[i].bed_number;
                this.db.getBed2(bedId).then(data => {
                    data.status = "Available";
                    this.db.updateBed(data).then(res => {
                    })
                })

            }
        })


    }

    getBednumber(gets) {
        var number = [gets.bed_number].join();
        return number;
    }


    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        this._loadBeds();
    }

    private _loadBeds(): void {
        this.db.onBedsChanged.subscribe(data => {
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        this.db.getBeds()
            .then((beds: Array<Bed>) => {
                this.beds = beds;
                this.beds2 = new BehaviorSubject<any>(beds);
                //console.log(this.beds2);

                this.checkboxes = {};
                beds.map(bed => {
                    this.checkboxes[bed.id] = false;
                });
                this.db.onSelectedBedsChanged.subscribe(selectedBeds => {
                    for (const id in this.checkboxes) {
                        this.checkboxes[id] = selectedBeds.includes(id);
                    }

                    this.selectedBeds = selectedBeds;
                });
                this.db.onSearchTextChanged.subscribe(data => {
                    this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                })
            });

    }

    newBed() {
        this.dialogRef = this.dialog.open(FuseBedsBedFormDialogComponent, {
            panelClass: 'bed-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }

                this.db.saveBed(response.getRawValue());
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

            });

    }

    editBed(bed) {
        this.dialogRef = this.dialog.open(FuseBedsBedFormDialogComponent, {
            panelClass: 'bed-form-dialog',
            data: {
                bed: bed,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':

                        this.db.updateBed(formData.getRawValue());
                        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteBed(bed);

                        break;
                }
            });
    }

    /**
     * Delete bed
     */
    deleteBed(bed) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deleteBed(bed);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(bedId) {
        this.db.toggleSelectedBed(bedId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    beds2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            of(this.db.beds).delay(5000),
            this.sort.mdSortChange,
            this._paginator.page,
        ];
        var result = Observable.fromPromise(this.db.getBeds());
        /*   result.subscribe(x => console.log(x), e => console.error(e));
         return result; */
        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.beds]));
        });
    }

    disconnect() {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
            return data;
        }

        return data.sort((a, b) => {
            const isAsc = this.sort.direction === 'asc';
            switch (this.sort.active) {
                case 'type': return compare(a.type, b.type, isAsc);
                case 'amount': return compare(+a.amount, +b.amount, isAsc);
                case 'id': return compare(+a.id, +b.id, isAsc);
                default: return 0;
            }
        });
    }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}