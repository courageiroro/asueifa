export interface Bed {
    id: string,
    rev: string,
    bed_number: string,
    type: string,
    status: string,
    description: string,
    bed_allotments: Array<string>
}

