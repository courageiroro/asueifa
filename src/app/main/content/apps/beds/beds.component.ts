import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { BedsService } from './beds.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-beds',
    templateUrl  : './beds.component.html',
    styleUrls    : ['./beds.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FuseBedsComponent implements OnInit
{
    hasSelectedBeds: boolean;
    searchInput: FormControl;

    constructor(private bedsService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.bedsService.onSelectedBedsChanged
            .subscribe(selectedBeds => {
                this.hasSelectedBeds = selectedBeds.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.bedsService.onSearchTextChanged.next(searchText);
            });
    }

}
