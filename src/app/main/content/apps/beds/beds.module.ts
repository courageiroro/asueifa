import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseBedsComponent } from './beds.component';
import { BedsService } from './beds.service';
import { FuseBedsBedListComponent } from './bed-list/bed-list.component';
import { FuseBedsSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseBedsBedFormDialogComponent } from './bed-form/bed-form.component';
import { Bed_allotmentsService } from './../bed_allotments/bed_allotments.service';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/beds',
        component: FuseBedsComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            beds: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseBedsComponent,
        FuseBedsBedListComponent,
        FuseBedsSelectedBarComponent,
        FuseBedsBedFormDialogComponent
    ],
    providers      : [
        BedsService,
        AuthGuard,
        PouchService,
        Bed_allotmentsService
    ],
    entryComponents: [FuseBedsBedFormDialogComponent]
})
export class FuseBedsModule
{
}
