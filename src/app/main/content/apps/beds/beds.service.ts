/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Bed } from './bed.model';
import { Schema } from '../schema';

@Injectable()
export class BedsService {
    public bed = "";
    public sCredentials;

    onBedsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedBedsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    beds: Bed[];
    user: any;
    selectedBeds: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The beds App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getBeds()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getBeds();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getBeds();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('abayelsa', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/abayelsa';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/abayelsa';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }


    /***********
     * bed
     **********/
    /**
     * Save a bed
     * @param {bed} Bed
     *
     * @return Promise<bed>
     */
    saveBed(bed: Bed): Promise<Bed> {
        bed.id = Math.floor(Date.now()).toString();
        bed.bed_allotments = [];
        //bed.driverslicenses = [];
        return this.db.rel.save('bed', bed)
            .then((data: any) => {
                //console.log(data);
                //console.log(bed);
                if (data && data.beds && data.beds
                [0]) {
                    return data.beds[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a bed
    * @param {bed} Bed
    *
    * @return Promise<bed>
    */
    updateBed(bed: Bed) {
        return this.db.rel.save('bed', bed)
            .then((data: any) => {
                if (data && data.beds && data.beds
                [0]) {
                    return data.beds[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a bed
     * @param {bed} bed
     *
     * @return Promise<boolean>
     */
    removeBed(bed: Bed): Promise<boolean> {
        return this.db.rel.del('bed', bed)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the beds
     *
     * @return Promise<Array<bed>>
     */
    getBeds(): Promise<Array<Bed>> {
        return this.db.rel.find('bed')
            .then((data: any) => {
                this.beds = data.beds;
                if (this.searchText && this.searchText !== '') {
                    this.beds = FuseUtils.filterArrayByString(this.beds, this.searchText);
                }
                //this.onbedsChanged.next(this.beds);
                return Promise.resolve(this.beds);
                //return data.beds ? data.beds : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getB_AllotBeds(): Promise<Array<Bed>> {
        return this.db.rel.find('bed')
            .then((data: any) => {
                return data.beds ? data.beds : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a bed
     * @param {bed} bed
     *
     * @return Promise<bed>
     */
    getBed(bed: Bed): Promise<Bed> {
        return this.db.rel.find('bed', bed.id)
            .then((data: any) => {
                return data && data.beds ? data.beds[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getBed2(id): Promise<Bed> {
        return this.db.rel.find('bed', id)
            .then((data: any) => {
                return data && data.beds ? data.beds[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected bed by id
     * @param id
     */
    toggleSelectedBed(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedBeds.length > 0) {
            const index = this.selectedBeds.indexOf(id);

            if (index !== -1) {
                this.selectedBeds.splice(index, 1);

                // Trigger the next event
                this.onSelectedBedsChanged.next(this.selectedBeds);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedBeds.push(id);

        // Trigger the next event
        this.onSelectedBedsChanged.next(this.selectedBeds);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedBeds.length > 0) {
            this.deselectBeds();
        }
        else {
            this.selectBeds();
        }
    }

    selectBeds(filterParameter?, filterValue?) {
        this.selectedBeds = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedBeds = [];
            this.beds.map(bed => {
                this.selectedBeds.push(bed.id);
            });
        }
        else {
            /* this.selectedbeds.push(...
                 this.beds.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedBedsChanged.next(this.selectedBeds);
    }





    deselectBeds() {
        this.selectedBeds = [];

        // Trigger the next event
        this.onSelectedBedsChanged.next(this.selectedBeds);
    }

    deleteBed(bed) {
        this.db.rel.del('bed', bed)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const bedIndex = this.beds.indexOf(bed);
                this.beds.splice(bedIndex, 1);
                this.onBedsChanged.next(this.beds);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedBeds() {
        for (const bedId of this.selectedBeds) {
            const bed = this.beds.find(_bed => {
                return _bed.id === bedId;
            });

            this.db.rel.del('bed', bed)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const bedIndex = this.beds.indexOf(bed);
                    this.beds.splice(bedIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onBedsChanged.next(this.beds);
        this.deselectBeds();
    }
}
