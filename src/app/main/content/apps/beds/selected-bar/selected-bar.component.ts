import { Component, OnInit } from '@angular/core';
import { BedsService } from '../beds.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseBedsSelectedBarComponent implements OnInit
{
    selectedBeds: string[];
    hasSelectedBeds: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private bedsService: PouchService,
        public dialog: MdDialog
    )
    {
        this.bedsService.onSelectedBedsChanged
            .subscribe(selectedBeds => {
                this.selectedBeds = selectedBeds;
                setTimeout(() => {
                    this.hasSelectedBeds = selectedBeds.length > 0;
                    this.isIndeterminate = (selectedBeds.length !== this.bedsService.beds.length && selectedBeds.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.bedsService.selectBeds();
    }

    deselectAll()
    {
        this.bedsService.deselectBeds();
    }

    deleteSelectedBeds()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected Income categories?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.bedsService.deleteSelectedBeds();
            }
            this.confirmDialogRef = null;
        });
    }

}
