import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Blood_bank } from '../blood_bank.model';

@Component({
    selector: 'fuse-blood_banks-blood_bank-form-dialog',
    templateUrl: './blood_bank-form.component.html',
    styleUrls: ['./blood_bank-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseBlood_banksBlood_bankFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    blood_bankForm: FormGroup;
    action: string;
    blood_bank: Blood_bank;

    constructor(
        public dialogRef: MdDialogRef<FuseBlood_banksBlood_bankFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Blood Bank';
            this.blood_bank = data.blood_bank;
        }
        else {
            this.dialogTitle = 'New Blood Bank';
            this.blood_bank = {
                id: '',
                rev: '',
                blood_group: '',
                status: null,
                patients: [],
                blood_donors: []
            }
        }

        this.blood_bankForm = this.createblood_bankForm();
    }

    ngOnInit() {
    }

    createblood_bankForm() {
        return this.formBuilder.group({
            id: [this.blood_bank.id],
            rev: [this.blood_bank.rev],
            blood_group: [this.blood_bank.blood_group],
            status: [this.blood_bank.status]
            
        });
    }
}
