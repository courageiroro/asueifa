import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Blood_banksService } from '../blood_banks.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { FuseBlood_banksBlood_bankFormDialogComponent } from '../blood_bank-form/blood_bank-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Blood_bank } from '../blood_bank.model';
import {PouchService} from '../../../../../provider/pouch-service';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';

@Component({
    selector: 'fuse-blood_banks-blood_bank-list',
    templateUrl: './blood_bank-list.component.html',
    styleUrls: ['./blood_bank-list.component.scss']
})
export class FuseBlood_banksBlood_bankListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public blood_banks: Array<Blood_bank> = [];
    blood_banks2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'group', 'status', 'buttons'];
    selectedBlood_banks: any[];
    checkboxes: {};
    public localStorageItem: any;
    public localStorageType: any;

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;

    constructor(public dialog: MdDialog, public db: PouchService)
    { }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        this._loadBlood_banks();

        this.localStorageItem = JSON.parse(localStorage.getItem('user'))
        this.localStorageType = this.localStorageItem.usertype
        
    }

    private _loadBlood_banks(): void {
        this.db.onBlood_banksChanged.subscribe(data => {
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        this.db.getBlood_banks()
            .then((blood_banks: Array<Blood_bank>) => {
                this.blood_banks = blood_banks;
                this.blood_banks2 = new BehaviorSubject<any>(blood_banks);
                //console.log(this.blood_banks2);

                this.checkboxes = {};
                blood_banks.map(blood_bank => {
                    this.checkboxes[blood_bank.id] = false;
                });
                this.db.onSelectedBlood_banksChanged.subscribe(selectedBlood_banks => {
                    for (const id in this.checkboxes) {
                        this.checkboxes[id] = selectedBlood_banks.includes(id);
                    }

                    this.selectedBlood_banks = selectedBlood_banks;
                });
                this.db.onSearchTextChanged.subscribe(data => {
                    this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                })
            });

    }

    newBlood_bank() {
        this.dialogRef = this.dialog.open(FuseBlood_banksBlood_bankFormDialogComponent, {
            panelClass: 'blood_bank-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }

                var res = response.getRawValue();
                res.status = res.status;
                this.db.saveBlood_bank(res);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

            });

    }

    editBlood_bank(blood_bank) {
        this.dialogRef = this.dialog.open(FuseBlood_banksBlood_bankFormDialogComponent, {
            panelClass: 'blood_bank-form-dialog',
            data: {
                blood_bank: blood_bank,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':

                        var res = formData.getRawValue();
                        res.status = res.status;
                        this.db.updateBlood_bank(res);
                        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteBlood_bank(blood_bank);

                        break;
                }
            });
    }

    /**
     * Delete blood_bank
     */
    deleteBlood_bank(blood_bank) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deleteBlood_bank(blood_bank);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(blood_bankId) {
        this.db.toggleSelectedBlood_bank(blood_bankId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    blood_banks2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            of(this.db.blood_banks).delay(5000),
            this.sort.mdSortChange,
            this._paginator.page,
        ];

          var result = Observable.fromPromise(this.db.getBlood_banks());
       /* result.subscribe(x => console.log(x), e => console.error(e));
        return result; */

        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.blood_banks]));
        });
    }

    disconnect() {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
          return data;
        }
    
        return data.sort((a, b) => {
          const isAsc = this.sort.direction === 'asc';
          switch (this.sort.active) {
            case 'group': return compare(a.blood_group, b.blood_group, isAsc);
            case 'amount': return compare(+a.amount, +b.amount, isAsc);
            case 'id': return compare(+a.id, +b.id, isAsc);
            default: return 0;
          }
        });
      }
}

 /** Simple sort comparator for example ID/Name columns (for client-side sorting). */
 function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }