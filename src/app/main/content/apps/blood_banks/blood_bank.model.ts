export interface Blood_bank {
    id: string,
    rev: string,
    blood_group: string,
    status: number,
    patients: Array<string>,
    blood_donors: Array<string>
}

