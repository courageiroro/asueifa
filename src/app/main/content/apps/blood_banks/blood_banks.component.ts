import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Blood_banksService } from './blood_banks.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-blood_banks',
    templateUrl  : './blood_banks.component.html',
    styleUrls    : ['./blood_banks.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FuseBlood_banksComponent implements OnInit
{
    hasSelectedBlood_banks: boolean;
    searchInput: FormControl;

    constructor(private blood_banksService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.blood_banksService.onSelectedBlood_banksChanged
            .subscribe(selectedBlood_banks => {
                this.hasSelectedBlood_banks = selectedBlood_banks.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.blood_banksService.onSearchTextChanged.next(searchText);
            });
    }

}
