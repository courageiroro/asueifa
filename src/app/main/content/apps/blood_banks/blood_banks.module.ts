import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseBlood_banksComponent } from './blood_banks.component';
import { Blood_banksService } from './blood_banks.service';
import { FuseBlood_banksBlood_bankListComponent } from './blood_bank-list/blood_bank-list.component';
import { FuseBlood_banksSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseBlood_banksBlood_bankFormDialogComponent } from './blood_bank-form/blood_bank-form.component';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path: 'apps/blood_banks',
        component: FuseBlood_banksComponent,
        canActivate: [AuthGuard],
        children: [],
        resolve: {
            blood_banks: PouchService
        }
    }
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        FuseBlood_banksComponent,
        FuseBlood_banksBlood_bankListComponent,
        FuseBlood_banksSelectedBarComponent,
        FuseBlood_banksBlood_bankFormDialogComponent
    ],
    providers: [
        Blood_banksService,
        PouchService,
        AuthGuard
    ],
    entryComponents: [FuseBlood_banksBlood_bankFormDialogComponent]
})
export class FuseBlood_banksModule {
}
