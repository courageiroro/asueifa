/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
PouchDB.plugin(require('pouchdb-find'));
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('relational-pouch'));

import { Blood_bank } from './blood_bank.model';
import { Schema } from '../schema';

@Injectable()
export class Blood_banksService {
    public blood_bank = "";
    public sCredentials;

    onBlood_banksChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedBlood_banksChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    blood_banks: Blood_bank[];
    user: any;
    selectedBlood_banks: string[] = [];
    public bloodBank = "";

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The blood_banks App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getBlood_banks()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getBlood_banks();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getBlood_banks();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('abayelsa', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/abayelsa';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/abayelsa';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }

    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }

    /***********
     * blood_bank
     **********/
    /**
     * Save a blood_bank
     * @param {blood_bank} blood_bank
     *
     * @return Promise<blood_bank>
     */
    saveBlood_bank(blood_bank: Blood_bank): Promise<Blood_bank> {
        blood_bank.id = Math.floor(Date.now()).toString();
        blood_bank.patients = [],
            blood_bank.blood_donors = []

        return this.db.rel.save('blood_bank', blood_bank)
            .then((data: any) => {
                if (data && data.blood_banks && data.blood_banks
                [0]) {
                    return data.blood_banks[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a blood_bank
    * @param {blood_bank} Blood_bank
    *
    * @return Promise<blood_bank>
    */
    updateBlood_bank(blood_bank: Blood_bank) {
        return this.db.rel.save('blood_bank', blood_bank)
            .then((data: any) => {
                if (data && data.blood_banks && data.blood_banks
                [0]) {
                    return data.blood_banks[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a blood_bank
     * @param {blood_bank} Blood_bank
     *
     * @return Promise<boolean>
     */
    removeBlood_bank(blood_bank: Blood_bank): Promise<boolean> {
        return this.db.rel.del('blood_bank', blood_bank)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the blood_banks
     *
     * @return Promise<Array<blood_bank>>
     */
    getBlood_banks(): Promise<Array<Blood_bank>> {
        return this.db.rel.find('blood_bank')
            .then((data: any) => {
                this.blood_banks = data.blood_banks;
                if (this.searchText && this.searchText !== '') {
                    this.blood_banks = FuseUtils.filterArrayByString(this.blood_banks, this.searchText);
                }
                //this.onblood_banksChanged.next(this.blood_banks);
                return Promise.resolve(this.blood_banks);
                //return data.blood_banks ? data.blood_banks : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getPatientBlood_banks(): Promise<Array<Blood_bank>> {
        return this.db.rel.find('blood_bank')
            .then((data: any) => {
                return data.blood_banks ? data.blood_banks : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a blood_bank
     * @param {blood_bank} blood_bank
     *
     * @return Promise<blood_bank>
     */
    getBlood_bank(blood_bank: Blood_bank): Promise<Blood_bank> {
        return this.db.rel.find('blood_bank', blood_bank.id)
            .then((data: any) => {
                return data && data.blood_banks ? data.blood_banks[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getBlood_bank2(id): Promise<Blood_bank> {
        return this.db.rel.find('blood_bank', id)
            .then((data: any) => {
                return data && data.blood_banks ? data.blood_banks[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected blood_bank by id
     * @param id
     */
    toggleSelectedBlood_bank(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedBlood_banks.length > 0) {
            const index = this.selectedBlood_banks.indexOf(id);

            if (index !== -1) {
                this.selectedBlood_banks.splice(index, 1);

                // Trigger the next event
                this.onSelectedBlood_banksChanged.next(this.selectedBlood_banks);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedBlood_banks.push(id);

        // Trigger the next event
        this.onSelectedBlood_banksChanged.next(this.selectedBlood_banks);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedBlood_banks.length > 0) {
            this.deselectBlood_banks();
        }
        else {
            this.selectBlood_banks();
        }
    }

    selectBlood_banks(filterParameter?, filterValue?) {
        this.selectedBlood_banks = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedBlood_banks = [];
            this.blood_banks.map(blood_bank => {
                this.selectedBlood_banks.push(blood_bank.id);
            });
        }
        else {
            /* this.selectedblood_banks.push(...
                 this.blood_banks.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedBlood_banksChanged.next(this.selectedBlood_banks);
    }





    deselectBlood_banks() {
        this.selectedBlood_banks = [];

        // Trigger the next event
        this.onSelectedBlood_banksChanged.next(this.selectedBlood_banks);
    }

    deleteBlood_bank(blood_bank) {
        this.db.rel.del('blood_bank', blood_bank)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const blood_bankIndex = this.blood_banks.indexOf(blood_bank);
                this.blood_banks.splice(blood_bankIndex, 1);
                this.onBlood_banksChanged.next(this.blood_banks);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedBlood_banks() {
        for (const blood_bankId of this.selectedBlood_banks) {
            const blood_bank = this.blood_banks.find(_blood_bank => {
                return _blood_bank.id === blood_bankId;
            });

            this.db.rel.del('blood_bank', blood_bank)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const blood_bankIndex = this.blood_banks.indexOf(blood_bank);
                    this.blood_banks.splice(blood_bankIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onBlood_banksChanged.next(this.blood_banks);
        this.deselectBlood_banks();
    }
}
