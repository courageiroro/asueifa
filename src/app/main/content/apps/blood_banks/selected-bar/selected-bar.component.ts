import { Component, OnInit } from '@angular/core';
import { Blood_banksService } from '../blood_banks.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseBlood_banksSelectedBarComponent implements OnInit
{
    selectedBlood_banks: string[];
    hasSelectedBlood_banks: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private blood_banksService: PouchService,
        public dialog: MdDialog
    )
    {
        this.blood_banksService.onSelectedBlood_banksChanged
            .subscribe(selectedBlood_banks => {
                this.selectedBlood_banks = selectedBlood_banks;
                setTimeout(() => {
                    this.hasSelectedBlood_banks = selectedBlood_banks.length > 0;
                    this.isIndeterminate = (selectedBlood_banks.length !== this.blood_banksService.blood_banks.length && selectedBlood_banks.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.blood_banksService.selectBlood_banks();
    }

    deselectAll()
    {
        this.blood_banksService.deselectBlood_banks();
    }

    deleteSelectedBlood_banks()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected blood_bank categorys?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.blood_banksService.deleteSelectedBlood_banks();
            }
            this.confirmDialogRef = null;
        });
    }

}
