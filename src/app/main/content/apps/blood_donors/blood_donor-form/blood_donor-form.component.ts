import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Blood_donor } from '../blood_donor.model';
import { Blood_bank } from '../../blood_banks/blood_bank.model';
import { BehaviorSubject } from 'rxjs/Rx';
import { Blood_banksService } from '../../blood_banks/blood_banks.service';
import { Blood_donorsService } from '../blood_donors.service';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-blood_donors-blood_donor-form-dialog',
    templateUrl: './blood_donor-form.component.html',
    styleUrls: ['./blood_donor-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseBlood_donorsBlood_donorFormDialogComponent implements OnInit {

    event: CalendarEvent;
    dialogTitle: string;
    blood_donorForm: FormGroup;
    action: string;
    blood_donor: Blood_donor;
    sexss: any;
    blood_bankss: any;
    public bloodId: string;


    constructor(
        public dialogRef: MdDialogRef<FuseBlood_donorsBlood_donorFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        public db: PouchService
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Blood Donor';
            this.blood_donor = data.blood_donor;
        }
        else {
            this.dialogTitle = 'New Blood Donor';
            this.blood_donor = {
                id: '',
                rev: '',
                name: '',
                blood_group: '',
                sex: '',
                age: '',
                phone: '',
                email: '',
                address: '',
                last_donation_timestamp: new Date,
                blood_donated: null,
                blood_bank: '',
            }
        }

        db.getBlood_banks().then(res => {
            this.blood_bankss = res
        })

        this.blood_donorForm = this.createBlood_donorForm();
    }

    ngOnInit() {
        this.db.bankId;
        
        this.sexss = ['Male', 'Female']
    }

    getBId(blood_bank) {
        
        this.db.bloodBank = blood_bank.id
        this.bloodId = blood_bank.id;

    }

    createBlood_donorForm() {
        //var expirydate = [this.blood_donor.expirydate];

        return this.formBuilder.group({
            id: [this.blood_donor.id],
            rev: [this.blood_donor.rev],
            name: [this.blood_donor.name],
            blood_group: [this.blood_donor.blood_group],
            sex: [this.blood_donor.sex],
            age: [this.blood_donor.age],
            phone: [this.blood_donor.phone],
            email: [this.blood_donor.email],
            address: [this.blood_donor.address],
            blood_donated: [this.blood_donor.blood_donated],
            blood_bank: [this.blood_donor.blood_bank],
            last_donation_timestamp: new Date(this.blood_donor.last_donation_timestamp).toDateString()

        });
    }
}
