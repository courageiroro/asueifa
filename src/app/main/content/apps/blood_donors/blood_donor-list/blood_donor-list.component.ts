import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Blood_donorsService } from '../blood_donors.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { FuseBlood_donorsBlood_donorFormDialogComponent } from '../blood_donor-form/blood_donor-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Blood_donor } from '../blood_donor.model';
import { Blood_banksService } from '../../blood_banks/blood_banks.service';
import {PouchService} from '../../../../../provider/pouch-service';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';

@Component({
    selector: 'fuse-blood_donors-blood_donor-list',
    templateUrl: './blood_donor-list.component.html',
    styleUrls: ['./blood_donor-list.component.scss']
})
export class FuseBlood_donorsBlood_donorListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public blood_donors: Array<Blood_donor> = [];
    blood_donors2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'name', 'age', 'sex', 'group', 'donation', 'address', 'email', 'phone', 'buttons'];
    selectedBlood_donors: any[];
    checkboxes: {};
    public localStorageItem: any;
    public localStorageType: any;

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;

    constructor(public dialog: MdDialog, public db: PouchService)
    { }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        this._loadBlood_donors();
        this.localStorageItem = JSON.parse(localStorage.getItem('user'));
        this.localStorageType = this.localStorageItem.usertype

        if (this.localStorageType === 'Laboratorist' || this.localStorageType === 'Nurse') {
            this.localStorageType = 'LabNurse'
        }

    }

    private _loadBlood_donors(): void {
        this.db.onBlood_donorsChanged.subscribe(data => {
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        this.db.getBlood_donors()
            .then((blood_donors: Array<Blood_donor>) => {
                this.blood_donors = blood_donors;
                this.blood_donors2 = new BehaviorSubject<any>(blood_donors);

                this.checkboxes = {};
                blood_donors.map(blood_donor => {
                    this.checkboxes[blood_donor.id] = false;
                });
                this.db.onSelectedBlood_donorsChanged.subscribe(selectedBlood_donors => {
                    for (const id in this.checkboxes) {
                        this.checkboxes[id] = selectedBlood_donors.includes(id);
                    }

                    this.selectedBlood_donors = selectedBlood_donors;
                });
                this.db.onSearchTextChanged.subscribe(data => {
                    this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                })
            });

    }

    newBlood_donor() {
        this.dialogRef = this.dialog.open(FuseBlood_donorsBlood_donorFormDialogComponent, {
            panelClass: 'blood_donor-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }

                var res = response.getRawValue();
                res.last_donation_timestamp = new Date(res.last_donation_timestamp).toISOString().substring(0,10);
                res.blood_donated = res.blood_donated;
                this.db.saveBlood_donor(res);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                this.db.getBlood_bank2(res.blood_bank).then(data => {
                    data.status = data.status - (-res.blood_donated);
                    this.db.updateBlood_bank(data);
                })

            });

    }

    editBlood_donor(blood_donor) {
        this.db.bankId = blood_donor.blood_bank
        var bloodDonated = blood_donor.blood_donated;
        this.dialogRef = this.dialog.open(FuseBlood_donorsBlood_donorFormDialogComponent, {
            panelClass: 'blood_donor-form-dialog',
            data: {
                blood_donor: blood_donor,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                var form = formData.getRawValue();
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':
                        form.blood_bank = blood_donor.blood_bank;
                        form.last_donation_timestamp = new Date(form.last_donation_timestamp).toISOString().substring(0,10);
                        this.db.updateBlood_donor(form);
                        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                        this.db.getBlood_bank2(blood_donor.blood_bank).then(data => {
                            data.status = data.status - (-form.blood_donated) - bloodDonated;
                            this.db.updateBlood_bank(data);
                        })

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteBlood_donor(blood_donor);

                        break;
                }
            });
    }

    /**
     * Delete blood_donor
     */
    deleteBlood_donor(blood_donor) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deleteBlood_donor(blood_donor);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                this.db.getBlood_bank2(blood_donor.blood_bank).then(data => {

                    data.status = data.status - blood_donor.blood_donated;
                    this.db.updateBlood_bank(data);
                })
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(blood_donorId) {
        this.db.toggleSelectedBlood_donor(blood_donorId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    blood_donors2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            of(this.db.blood_donors).delay(5000),
            this.sort.mdSortChange,
            this._paginator.page,
        ];


        var result = Observable.fromPromise(this.db.getBlood_donors());
       /*  result.subscribe(x => console.log(x), e => console.error(e));
        return result; */

        
        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.blood_donors]));
        });
    }

    disconnect() {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
          return data;
        }
    
        return data.sort((a, b) => {
          const isAsc = this.sort.direction === 'asc';
          switch (this.sort.active) {
            case 'name': return compare(a.name, b.name, isAsc);
            case 'amount': return compare(+a.amount, +b.amount, isAsc);
            case 'id': return compare(+a.id, +b.id, isAsc);
            default: return 0;
          }
        });
      }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }