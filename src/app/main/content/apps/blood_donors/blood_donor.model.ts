export interface Blood_donor {
    id: string,
    rev: string,
    name: string,
    blood_group: string,
    sex: string,
    age: string,
    phone: string,
    email: string,
    address: string,
    last_donation_timestamp: Date,
    blood_donated: number,
    blood_bank: string
}

