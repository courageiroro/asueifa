import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Blood_donorsService } from './blood_donors.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-driverlicenses',
    templateUrl  : './blood_donors.component.html',
    styleUrls    : ['./blood_donors.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FuseBlood_donorsComponent implements OnInit
{
    hasSelectedBlood_donors: boolean;
    searchInput: FormControl;

    constructor(private blood_donorsService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.blood_donorsService.onSelectedBlood_donorsChanged
            .subscribe(selectedBlood_donors => {
                this.hasSelectedBlood_donors = selectedBlood_donors.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.blood_donorsService.onSearchTextChanged.next(searchText);
            });
    }

}
