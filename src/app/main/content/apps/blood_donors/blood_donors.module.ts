import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseBlood_donorsComponent } from './blood_donors.component';
import { Blood_donorsService } from './blood_donors.service';
import { FuseBlood_donorsBlood_donorListComponent } from './blood_donor-list/blood_donor-list.component';
import { FuseBlood_donorsSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseBlood_donorsBlood_donorFormDialogComponent } from './blood_donor-form/blood_donor-form.component';
import { Blood_banksService } from './../blood_banks/blood_banks.service';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/blood_donors',
        component: FuseBlood_donorsComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            blood_donors: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseBlood_donorsComponent,
        FuseBlood_donorsBlood_donorListComponent,
        FuseBlood_donorsSelectedBarComponent,
        FuseBlood_donorsBlood_donorFormDialogComponent
    ],
    providers      : [
        Blood_donorsService,
        Blood_banksService,
        PouchService,
        AuthGuard
    ],
    entryComponents: [FuseBlood_donorsBlood_donorFormDialogComponent]
})
export class FuseBlood_donorsModule
{
}
