/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import { Blood_banksService } from '../blood_banks/blood_banks.service';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Blood_donor } from './blood_donor.model';
import { Schema } from '../schema';

@Injectable()
export class Blood_donorsService {
    public sCredentials;
    onBlood_donorsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedBlood_donorsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    public staff = "";
    public bankId = "";

    blood_donors: Blood_donor[];
    user: any;
    selectedBlood_donors: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http, public db2: Blood_banksService) {
        this.initDB(this.sCredentials);
    }

    /**
     * The blood_donors App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getBlood_donors()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getBlood_donors();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getBlood_donors();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('abayelsa', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/abayelsa';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/abayelsa';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }

    /***********
     *blood_donor
     **********/
    /**
     * Save a blood_donor
     * @param {blood_donor} Blood_donor
     *
     * @return Promise<blood_donor>
     */
    saveBlood_donor(blood_donor: Blood_donor): Promise<Blood_donor> {
        blood_donor.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('blood_donor', blood_donor)
            .then((data: any) => {

                if (data && data.blood_donors && data.blood_donors[0]) {

                    return data.blood_donors[0]

                }


                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
    * Update a blood_donor
    * @param {blood_donor} Blood_donor
    *
    * @return Promise<blood_donor>
    */
    updateBlood_donor(blood_donor: Blood_donor) {

        return this.db.rel.save('blood_donor', blood_donor)
            .then((data: any) => {
                if (data && data.blood_donors && data.blood_donors
                [0]) {
                    return data.blood_donors[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a blood_donor
     * @param {blood_donor} Blood_donor
     *
     * @return Promise<boolean>
     */
    removeBlood_donor(blood_donor: Blood_donor): Promise<boolean> {
        return this.db.rel.del('blood_donor', blood_donor)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the blood_donors
     *
     * @return Promise<Array<blood_donor>>
     */
    getBlood_donors(): Promise<Array<Blood_donor>> {
        return this.db.rel.find('blood_donor')
            .then((data: any) => {
                this.blood_donors = data.blood_donors;
                if (this.searchText && this.searchText !== '') {
                    this.blood_donors = FuseUtils.filterArrayByString(this.blood_donors, this.searchText);
                }
                return Promise.resolve(this.blood_donors);
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a blood_donor
     * @param {blood_donor} blood_donor
     *
     * @return Promise<blood_donor>
     */
    getBlood_donor(blood_donor: Blood_donor): Promise<Blood_donor> {
        return this.db.rel.find('blood_donor', blood_donor.id)
            .then((data: any) => {
                return data && data.blood_donors ? data.blood_donors[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getBlood_donor2(id): Promise<Blood_donor> {
        return this.db.rel.find('blood_donor', id)
            .then((data: any) => {
                return data && data.blood_donors ? data.blood_donors[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Toggle selected blood_donor by id
     * @param id
     */
    toggleSelectedBlood_donor(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedBlood_donors.length > 0) {
            const index = this.selectedBlood_donors.indexOf(id);

            if (index !== -1) {
                this.selectedBlood_donors.splice(index, 1);

                // Trigger the next event
                this.onSelectedBlood_donorsChanged.next(this.selectedBlood_donors);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedBlood_donors.push(id);

        // Trigger the next event
        this.onSelectedBlood_donorsChanged.next(this.selectedBlood_donors);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedBlood_donors.length > 0) {
            this.deselectBlood_donors();
        }
        else {
            this.selectBlood_donors();
        }
    }

    selectBlood_donors(filterParameter?, filterValue?) {
        this.selectedBlood_donors = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedBlood_donors = [];
            this.blood_donors.map(blood_donor => {
                this.selectedBlood_donors.push(blood_donor.id);
            });
        }
        else {
            /* this.selecteddriverlicenses.push(...
                 this.driverlicenses.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedBlood_donorsChanged.next(this.selectedBlood_donors);
    }





    deselectBlood_donors() {
        this.selectedBlood_donors = [];

        // Trigger the next event
        this.onSelectedBlood_donorsChanged.next(this.selectedBlood_donors);
    }

    deleteBlood_donor(blood_donor) {
        this.db.rel.del('blood_donor', blood_donor)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const blood_donorIndex = this.blood_donors.indexOf(blood_donor);
                this.blood_donors.splice(blood_donorIndex, 1);
                this.onBlood_donorsChanged.next(this.blood_donors);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedBlood_donors() {
        for (const blood_donorId of this.selectedBlood_donors) {
            const blood_donor = this.blood_donors.find(_blood_donor => {
                return _blood_donor.id === blood_donorId;
            });

            this.db.rel.del('blood_donor', blood_donor)
                .then((data: any) => {
                    this.db2.getBlood_bank2(blood_donor.blood_bank).then(data => {
                        data.status = data.status - blood_donor.blood_donated;
                        this.db2.updateBlood_bank(data);
                    })
                    //return data && data.deleted ? data.deleted: false;
                    const blood_donorIndex = this.blood_donors.indexOf(blood_donor);
                    this.blood_donors.splice(blood_donorIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onBlood_donorsChanged.next(this.blood_donors);
        this.deselectBlood_donors();
    }
}
