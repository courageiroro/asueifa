import { Component, OnInit } from '@angular/core';
import { Blood_donorsService } from '../blood_donors.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseBlood_donorsSelectedBarComponent implements OnInit
{
    selectedBlood_donors: string[];
    hasSelectedBlood_donors: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private blood_donorsService: PouchService,
        public dialog: MdDialog
    )
    {
        this.blood_donorsService.onSelectedBlood_donorsChanged
            .subscribe(selectedBlood_donors => {
                this.selectedBlood_donors = selectedBlood_donors;
                setTimeout(() => {
                    this.hasSelectedBlood_donors = selectedBlood_donors.length > 0;
                    this.isIndeterminate = (selectedBlood_donors.length !== this.blood_donorsService.blood_donors.length && selectedBlood_donors.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.blood_donorsService.selectBlood_donors();
    }

    deselectAll()
    {
        this.blood_donorsService.deselectBlood_donors();
    }

    deleteSelectedBlood_donors()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected Driver licenses?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.blood_donorsService.deleteSelectedBlood_donors();
            }
            this.confirmDialogRef = null;
        });
    }

}
