import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Blood_price } from '../blood_price.model';

@Component({
    selector: 'fuse-blood_prices-blood_price-form-dialog',
    templateUrl: './blood_price-form.component.html',
    styleUrls: ['./blood_price-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseBlood_pricesBlood_priceFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    blood_priceForm: FormGroup;
    action: string;
    blood_price: Blood_price;

    constructor(
        public dialogRef: MdDialogRef<FuseBlood_pricesBlood_priceFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Blood Price';
            this.blood_price = data.blood_price;
        }
        else {
            this.dialogTitle = 'New Blood Price';
            this.blood_price = {
                id: '',
                rev: '',
                amount: null,
                price: null,
                blood_sales: []
            }
        }

        this.blood_priceForm = this.createblood_priceForm();
    }

    ngOnInit() {
    }

    createblood_priceForm() {
        return this.formBuilder.group({
            id: [this.blood_price.id],
            rev: [this.blood_price.rev],
            amount: [this.blood_price.amount],
            price: [this.blood_price.price]
            
        });
    }
}
