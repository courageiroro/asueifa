import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Blood_pricesService } from '../blood_prices.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { FuseBlood_pricesBlood_priceFormDialogComponent } from '../blood_price-form/blood_price-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Blood_price } from '../blood_price.model';
import {PouchService} from '../../../../../provider/pouch-service';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';

@Component({
    selector: 'fuse-blood_prices-blood_price-list',
    templateUrl: './blood_price-list.component.html',
    styleUrls: ['./blood_price-list.component.scss']
})
export class FuseBlood_pricesBlood_priceListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public blood_prices: Array<Blood_price> = [];
    blood_prices2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'amount', 'price', 'buttons'];
    selectedblood_prices: any[];
    checkboxes: {};
    public localStorageItem: any;
    public localStorageType: any;

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;

    constructor(public dialog: MdDialog, public db: PouchService)
    { }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        this._loadBlood_prices();

        this.localStorageItem = JSON.parse(localStorage.getItem('user'))
        this.localStorageType = this.localStorageItem.usertype
        
    }

    private _loadBlood_prices(): void {
        this.db.onBlood_pricesChanged.subscribe(data => {
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        this.db.getBlood_prices()
            .then((blood_prices: Array<Blood_price>) => {
                this.blood_prices = blood_prices;
                this.blood_prices2 = new BehaviorSubject<any>(blood_prices);
                //console.log(this.blood_prices2);

                this.checkboxes = {};
                blood_prices.map(blood_price => {
                    this.checkboxes[blood_price.id] = false;
                });
                this.db.onSelectedBlood_pricesChanged.subscribe(selectedBlood_prices => {
                    for (const id in this.checkboxes) {
                        this.checkboxes[id] = selectedBlood_prices.includes(id);
                    }

                    this.selectedblood_prices = selectedBlood_prices;
                });
                this.db.onSearchTextChanged.subscribe(data => {
                    this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                })
            });

    }

    newBlood_price() {
        this.dialogRef = this.dialog.open(FuseBlood_pricesBlood_priceFormDialogComponent, {
            panelClass: 'blood_price-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }

                var res = response.getRawValue();
                res.status = res.status;
                this.db.saveBlood_price(res);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

            });

    }

    editBlood_price(blood_price) {
        this.dialogRef = this.dialog.open(FuseBlood_pricesBlood_priceFormDialogComponent, {
            panelClass: 'blood_price-form-dialog',
            data: {
                blood_price: blood_price,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':

                        var res = formData.getRawValue();
                        res.status = res.status;
                        this.db.updateBlood_price(res);
                        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteBlood_price(blood_price);

                        break;
                }
            });
    }

    /**
     * Delete blood_price
     */
    deleteBlood_price(blood_price) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deleteBlood_price(blood_price);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(blood_priceId) {
        this.db.toggleSelectedBlood_price(blood_priceId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    blood_prices2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            of(this.db.blood_prices).delay(5000),
            this.sort.mdSortChange,
            this._paginator.page,
        ];

        var result = Observable.fromPromise(this.db.getBlood_prices());
       /*  result.subscribe(x => console.log(x), e => console.error(e));
        return result; */

        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.blood_prices]));
        });
    }

    disconnect() {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
          return data;
        }
    
        return data.sort((a, b) => {
          const isAsc = this.sort.direction === 'asc';
          switch (this.sort.active) {
            case 'amount': return compare(a.amount, b.amount, isAsc);
            case 'amounts': return compare(+a.amount, +b.amount, isAsc);
            case 'id': return compare(+a.id, +b.id, isAsc);
            default: return 0;
          }
        });
      }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }