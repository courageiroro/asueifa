export interface Blood_price {
    id: string,
    rev: string,
    amount: number,
    price: number,
    blood_sales: Array<string>
}

