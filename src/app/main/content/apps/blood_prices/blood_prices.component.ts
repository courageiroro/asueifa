import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Blood_pricesService } from './blood_prices.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-blood_prices',
    templateUrl  : './blood_prices.component.html',
    styleUrls    : ['./blood_prices.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FuseBlood_pricesComponent implements OnInit
{
    hasSelectedBlood_prices: boolean;
    searchInput: FormControl;

    constructor(private blood_pricesService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.blood_pricesService.onSelectedBlood_pricesChanged
            .subscribe(selectedBlood_prices => {
                this.hasSelectedBlood_prices = selectedBlood_prices.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.blood_pricesService.onSearchTextChanged.next(searchText);
            });
    }

}
