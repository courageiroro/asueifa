import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseBlood_pricesComponent } from './blood_prices.component';
import { Blood_pricesService } from './blood_prices.service';
import { FuseBlood_pricesBlood_priceListComponent } from './blood_price-list/blood_price-list.component';
import { FuseBlood_pricesSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseBlood_pricesBlood_priceFormDialogComponent } from './blood_price-form/blood_price-form.component';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/blood_prices',
        component: FuseBlood_pricesComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            blood_prices: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseBlood_pricesComponent,
        FuseBlood_pricesBlood_priceListComponent,
        FuseBlood_pricesSelectedBarComponent,
        FuseBlood_pricesBlood_priceFormDialogComponent
    ],
    providers      : [
        Blood_pricesService,
        PouchService,
        AuthGuard
    ],
    entryComponents: [FuseBlood_pricesBlood_priceFormDialogComponent]
})
export class FuseBlood_pricesModule
{
}
