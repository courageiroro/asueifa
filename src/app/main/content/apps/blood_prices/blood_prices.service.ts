/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Blood_price } from './blood_price.model';
import { Schema } from '../schema';

@Injectable()
export class Blood_pricesService {
    public blood_price = "";
    public sCredentials;
    onBlood_pricesChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedBlood_pricesChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    blood_prices: Blood_price[];
    user: any;
    selectedBlood_prices: string[] = [];
    public bloodPrice = "";

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The blood_prices App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getBlood_prices()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getBlood_prices();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getBlood_prices();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('abayelsa', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/abayelsa';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/abayelsa';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }
    /***********
     * blood_price
     **********/
    /**
     * Save a blood_price
     * @param {blood_price} blood_price
     *
     * @return Promise<blood_price>
     */
    saveBlood_price(blood_price: Blood_price): Promise<Blood_price> {
        blood_price.id = Math.floor(Date.now()).toString();
        blood_price.blood_sales = []

        return this.db.rel.save('blood_price', blood_price)
            .then((data: any) => {
                if (data && data.blood_prices && data.blood_prices
                [0]) {
                    return data.blood_prices[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a blood_price
    * @param {blood_price} Blood_price
    *
    * @return Promise<blood_price>
    */
    updateBlood_price(blood_price: Blood_price) {
        return this.db.rel.save('blood_price', blood_price)
            .then((data: any) => {
                if (data && data.blood_prices && data.blood_prices
                [0]) {
                    return data.blood_prices[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a blood_price
     * @param {blood_price} Blood_price
     *
     * @return Promise<boolean>
     */
    removeBlood_price(blood_price: Blood_price): Promise<boolean> {
        return this.db.rel.del('blood_price', blood_price)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the blood_prices
     *
     * @return Promise<Array<blood_price>>
     */
    getBlood_prices(): Promise<Array<Blood_price>> {
        return this.db.rel.find('blood_price')
            .then((data: any) => {
                this.blood_prices = data.blood_prices;
                if (this.searchText && this.searchText !== '') {
                    this.blood_prices = FuseUtils.filterArrayByString(this.blood_prices, this.searchText);
                }
                //this.onblood_pricesChanged.next(this.blood_prices);
                return Promise.resolve(this.blood_prices);
                //return data.blood_prices ? data.blood_prices : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getPatientBlood_prices(): Promise<Array<Blood_price>> {
        return this.db.rel.find('blood_price')
            .then((data: any) => {
                return data.blood_prices ? data.blood_prices : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a blood_price
     * @param {blood_price} blood_price
     *
     * @return Promise<blood_price>
     */
    getBlood_price(blood_price: Blood_price): Promise<Blood_price> {
        return this.db.rel.find('blood_price', blood_price.id)
            .then((data: any) => {
                return data && data.blood_prices ? data.blood_prices[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getBlood_price2(id): Promise<Blood_price> {
        return this.db.rel.find('blood_price', id)
            .then((data: any) => {
                return data && data.blood_prices ? data.blood_prices[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected blood_price by id
     * @param id
     */
    toggleSelectedBlood_price(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedBlood_prices.length > 0) {
            const index = this.selectedBlood_prices.indexOf(id);

            if (index !== -1) {
                this.selectedBlood_prices.splice(index, 1);

                // Trigger the next event
                this.onSelectedBlood_pricesChanged.next(this.selectedBlood_prices);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedBlood_prices.push(id);

        // Trigger the next event
        this.onSelectedBlood_pricesChanged.next(this.selectedBlood_prices);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedBlood_prices.length > 0) {
            this.deselectBlood_prices();
        }
        else {
            this.selectBlood_prices();
        }
    }

    selectBlood_prices(filterParameter?, filterValue?) {
        this.selectedBlood_prices = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedBlood_prices = [];
            this.blood_prices.map(blood_price => {
                this.selectedBlood_prices.push(blood_price.id);
            });
        }
        else {
            /* this.selectedblood_prices.push(...
                 this.blood_prices.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedBlood_pricesChanged.next(this.selectedBlood_prices);
    }





    deselectBlood_prices() {
        this.selectedBlood_prices = [];

        // Trigger the next event
        this.onSelectedBlood_pricesChanged.next(this.selectedBlood_prices);
    }

    deleteBlood_price(blood_price) {
        this.db.rel.del('blood_price', blood_price)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const blood_priceIndex = this.blood_prices.indexOf(blood_price);
                this.blood_prices.splice(blood_priceIndex, 1);
                this.onBlood_pricesChanged.next(this.blood_prices);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedblood_prices() {
        for (const blood_priceId of this.selectedBlood_prices) {
            const blood_price = this.blood_prices.find(_blood_price => {
                return _blood_price.id === blood_priceId;
            });

            this.db.rel.del('blood_price', blood_price)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const blood_priceIndex = this.blood_prices.indexOf(blood_price);
                    this.blood_prices.splice(blood_priceIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onBlood_pricesChanged.next(this.blood_prices);
        this.deselectBlood_prices();
    }
}
