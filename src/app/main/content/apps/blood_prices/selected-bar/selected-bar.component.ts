import { Component, OnInit } from '@angular/core';
import { Blood_pricesService } from '../blood_prices.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseBlood_pricesSelectedBarComponent implements OnInit
{
    selectedBlood_prices: string[];
    hasSelectedBlood_prices: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private blood_pricesService: PouchService,
        public dialog: MdDialog
    )
    {
        this.blood_pricesService.onSelectedBlood_pricesChanged
            .subscribe(selectedBlood_prices => {
                this.selectedBlood_prices = selectedBlood_prices;
                setTimeout(() => {
                    this.hasSelectedBlood_prices = selectedBlood_prices.length > 0;
                    this.isIndeterminate = (selectedBlood_prices.length !== this.blood_pricesService.blood_prices.length && selectedBlood_prices.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.blood_pricesService.selectBlood_prices();
    }

    deselectAll()
    {
        this.blood_pricesService.deselectBlood_prices();
    }

    deleteSelectedBlood_prices()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected blood_price categorys?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.blood_pricesService.deleteSelectedblood_prices();
            }
            this.confirmDialogRef = null;
        });
    }

}
