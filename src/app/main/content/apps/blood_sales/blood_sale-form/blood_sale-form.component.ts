import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Blood_sale } from '../blood_sale.model';
import { Blood_bank } from '../../blood_banks/blood_bank.model';
import { BehaviorSubject } from 'rxjs/Rx';
import { Blood_banksService } from '../../blood_banks/blood_banks.service';
import { Blood_salesService } from '../blood_sales.service';
import { StaffsService } from '../../staffs/staffs.service';
import { Blood_pricesService } from '../../blood_prices/blood_prices.service';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-blood_sales-blood_sale-form-dialog',
    templateUrl: './blood_sale-form.component.html',
    styleUrls: ['./blood_sale-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseBlood_salesBlood_saleFormDialogComponent implements OnInit {

    event: CalendarEvent;
    dialogTitle: string;
    blood_saleForm: FormGroup;
    action: string;
    blood_sale: Blood_sale;
    sexss: any;
    blood_bankss: any;
    public bloodId: string;
    public patientBlood: string;
    public patientId: string;
    public patientss;
    public labss;
    public labName;
    public labId;
    public priceId;
    public bloodName;
    public patientSex: string;
    public pricess;
    public price;
    public priceName;
    public total = 0;
    public amountSold;


    constructor(
        public dialogRef: MdDialogRef<FuseBlood_salesBlood_saleFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        public db: PouchService
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Blood Sale';
            this.blood_sale = data.blood_sale;
        }
        else {
            this.dialogTitle = 'New Blood Sale';
            this.blood_sale = {
                id: '',
                rev: '',
                price: 0,
                blood_group: '',
                sex: '',
                patient: '',
                patient_bloodgroup: '',
                lab_name: '',
                amount_blood: 0,
                date: new Date,
                total: 0,
                priceId: '',
                patientId: '',
                blood_groupId: '',
                staffId: '',
                insurancename: '',
                insurancepercent: 0
            }
        }

        db.getPStaffs().then(res => {
            this.patientss = res;
        })
        db.getLStaffs().then(res => {
            this.labss = res;
        })

        db.getBlood_banks().then(res => {
            this.blood_bankss = res
        })
        db.getBlood_prices().then(res => {
            this.pricess = res;
        })
        //this.total = 0;
        this.blood_sale.amount_blood;
        //this.priceName = 0

        this.blood_saleForm = this.createblood_saleForm();
    }

    ngOnInit() {
        this.db.bankId;
        
        this.sexss = ['Male', 'Female']
    }

    getPId(patient) {
        
        this.patientBlood = patient.blood_group;
        this.patientSex = patient.sex;
        this.patientId = patient.id;
        this.blood_sale.insurancename = patient.insurancename;
        this.blood_sale.insurancepercent = patient.insurancepercent;
    }

    getPRId(price) {
        this.priceId = price.id;
        this.priceName = price.price;
    }

    getLId(lab) {
        
        this.labName = lab.name;
        this.labId = lab.id;
    }

    getBId(blood_bank) {
        
        this.bloodName = blood_bank.blood_group;
        this.bloodId = blood_bank.id;
    }

    sumTotal() {
        this.total = 0;
        this.blood_sale.total = this.blood_sale.price * this.blood_sale.amount_blood * (this.blood_sale.insurancepercent/100);
        this.blood_sale.total = this.blood_sale.price * this.blood_sale.amount_blood - this.blood_sale.total;
    
    }

    createblood_saleForm() {
        //var expirydate = [this.blood_sale.expirydate];

        return this.formBuilder.group({
            id: [this.blood_sale.id],
            rev: [this.blood_sale.rev],
            price: [this.blood_sale.price],
            blood_group: [this.blood_sale.blood_group],
            sex: [this.blood_sale.sex],
            patient: [this.blood_sale.patient],
            patient_bloodgroup: [this.blood_sale.patient_bloodgroup],
            lab_name: [this.blood_sale.lab_name],
            amount_blood: [this.blood_sale.amount_blood],
            total: [this.blood_sale.total],
            priceId: [this.blood_sale.priceId],
            patientId: [this.blood_sale.patientId],
            blood_groupId: [this.blood_sale.blood_groupId],
            staffId: [this.blood_sale.staffId],
            insurancename: [{value:this.blood_sale.insurancename,disabled:true}],
            insurancepercent: [{value:this.blood_sale.insurancepercent,disabled:true}],
            date: new Date(this.blood_sale.date).toDateString()

        });
    }
}
