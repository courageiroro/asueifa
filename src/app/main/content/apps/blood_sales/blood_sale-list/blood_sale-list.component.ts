import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Blood_salesService } from '../blood_sales.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { FuseBlood_salesBlood_saleFormDialogComponent } from '../blood_sale-form/blood_sale-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort  } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Blood_sale } from '../blood_sale.model';
import {Blood_banksService} from '../../blood_banks/blood_banks.service';
import {PouchService} from '../../../../../provider/pouch-service';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';

@Component({
    selector: 'fuse-blood_sales-blood_sale-list',
    templateUrl: './blood_sale-list.component.html',
    styleUrls: ['./blood_sale-list.component.scss']
})
export class FuseBlood_salesBlood_saleListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public blood_sales: Array<Blood_sale> = [];
    blood_sales2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'patient', 'price', 'amount', 'date', 'total', 'lab_name', 'sex', 'patient_bloodgroup', 'buttons'];
    selectedblood_sales: any[];
    checkboxes: {};
    public localStorageItem: any;
    public localStorageType: any;

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;

    constructor(public dialog: MdDialog, public db: PouchService)
    { }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        this._loadBlood_sales();
        this.localStorageItem = JSON.parse(localStorage.getItem('user'));
        this.localStorageType = this.localStorageItem.usertype

        if (this.localStorageType === 'Laboratorist' || this.localStorageType === 'Nurse') {
            this.localStorageType = 'LabNurse'
        }

    }

    private _loadBlood_sales(): void {
        this.db.onBlood_salesChanged.subscribe(data => {
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        this.db.getBlood_sales()
            .then((blood_sales: Array<Blood_sale>) => {
                this.blood_sales = blood_sales;
                this.blood_sales2 = new BehaviorSubject<any>(blood_sales);

                this.checkboxes = {};
                blood_sales.map(blood_sale => {
                    this.checkboxes[blood_sale.id] = false;
                });
                this.db.onSelectedBlood_salesChanged.subscribe(selectedBlood_sales => {
                    for (const id in this.checkboxes) {
                        this.checkboxes[id] = selectedBlood_sales.includes(id);
                    }

                    this.selectedblood_sales = selectedBlood_sales;
                });
                this.db.onSearchTextChanged.subscribe(data => {
                    this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                })
            });

    }

    newBlood_sale() {
        this.dialogRef = this.dialog.open(FuseBlood_salesBlood_saleFormDialogComponent, {
            panelClass: 'blood_sale-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }

                var res = response.getRawValue();
                res.date = new Date(res.date).toISOString().substring(0,10);
                res.blood_group = res.blood_group;
                
                this.db.saveBlood_sale(res);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                this.db.getBlood_bank2(res.blood_groupId).then(data => {
                    
                    data.status = data.status - res.amount_blood;
                    this.db.updateBlood_bank(data);
                })

            });

    }

    editBlood_sale(blood_sale) {
        console.log(blood_sale);
        var bloodAmount = blood_sale.amount_blood;

        this.db.bankId = blood_sale.blood_bank
        this.dialogRef = this.dialog.open(FuseBlood_salesBlood_saleFormDialogComponent, {
            panelClass: 'blood_sale-form-dialog',
            data: {
                blood_sale: blood_sale,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                var form = formData.getRawValue();
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':
                        form.priceId = blood_sale.priceId;
                        form.patientId = blood_sale.patientId;
                        form.staffId = blood_sale.staffId;
                        form.blood_groupId = blood_sale.blood_groupId;
                        form.date = new Date(form.date).toISOString().substring(0,10);
                        this.db.updateBlood_sale(form);
                        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                        this.db.getBlood_bank2(blood_sale.blood_groupId).then(data => {
                            data.status = data.status - (form.amount_blood - bloodAmount);

                            this.db.updateBlood_bank(data);
                        })

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteBlood_sale(blood_sale);

                        break;
                }
            });
    }

    /**
     * Delete blood_sale
     */
    deleteBlood_sale(blood_sale) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deleteBlood_sale(blood_sale);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                this.db.getBlood_bank2(blood_sale.blood_groupId).then(data => {

                    data.status = data.status - (-blood_sale.amount_blood);
                    this.db.updateBlood_bank(data);
                })
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(blood_saleId) {
        this.db.toggleSelectedBlood_sale(blood_saleId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    blood_sales2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            of(this.db.blood_sales).delay(5000),
            this.sort.mdSortChange,
            this._paginator.page,
        ];

        //this._paginator.length = this.db.blood_sales.length;

        var result = Observable.fromPromise(this.db.getBlood_sales());
        /* result.subscribe(x => console.log(x), e => console.error(e));
        return result; */
        
        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.blood_sales]));
        });

    }

    disconnect() {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
          return data;
        }
    
        return data.sort((a, b) => {
          const isAsc = this.sort.direction === 'asc';
          switch (this.sort.active) {
            case 'patient': return compare(a.patient, b.patient, isAsc);
            case 'amount': return compare(+a.amount, +b.amount, isAsc);
            case 'id': return compare(+a.id, +b.id, isAsc);
            default: return 0;
          }
        });
      }
}

 /** Simple sort comparator for example ID/Name columns (for client-side sorting). */
 function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }