export interface Blood_sale {
    id: string,
    rev: string,
    price: number,
    blood_group: string,
    patient: string,
    patient_bloodgroup: string,
    lab_name: string,
    amount_blood: number,
    date: Date,
    sex: string,
    total: number,
    priceId: string,
    patientId: string,
    staffId: string,
    blood_groupId: string,
    insurancename: string,
    insurancepercent: any
}

