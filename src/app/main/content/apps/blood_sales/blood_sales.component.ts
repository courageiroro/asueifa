import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Blood_salesService } from './blood_sales.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-driverlicenses',
    templateUrl  : './blood_sales.component.html',
    styleUrls    : ['./blood_sales.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FuseBlood_salesComponent implements OnInit
{
    hasSelectedBlood_sales: boolean;
    searchInput: FormControl;

    constructor(private blood_salesService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.blood_salesService.onSelectedBlood_salesChanged
            .subscribe(selectedBlood_sales => {
                this.hasSelectedBlood_sales = selectedBlood_sales.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.blood_salesService.onSearchTextChanged.next(searchText);
            });
    }

}
