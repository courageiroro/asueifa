import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseBlood_salesComponent } from './blood_sales.component';
import { Blood_salesService } from './blood_sales.service';
import { FuseBlood_salesBlood_saleListComponent } from './blood_sale-list/blood_sale-list.component';
import { FuseBlood_salesSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseBlood_salesBlood_saleFormDialogComponent } from './blood_sale-form/blood_sale-form.component';
import { Blood_banksService } from './../blood_banks/blood_banks.service';
import { Blood_pricesService } from './../blood_prices/blood_prices.service';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/blood_sales',
        component: FuseBlood_salesComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            blood_sales: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseBlood_salesComponent,
        FuseBlood_salesBlood_saleListComponent,
        FuseBlood_salesSelectedBarComponent,
        FuseBlood_salesBlood_saleFormDialogComponent
    ],
    providers      : [
        Blood_salesService,
        Blood_banksService,
        AuthGuard,
        PouchService,
        Blood_pricesService
    ],
    entryComponents: [FuseBlood_salesBlood_saleFormDialogComponent]
})
export class FuseBlood_salesModule
{
}
