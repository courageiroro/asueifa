/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import { Blood_banksService } from '../blood_banks/blood_banks.service';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Blood_sale } from './blood_sale.model';
import { Schema } from '../schema';

@Injectable()
export class Blood_salesService {

    onBlood_salesChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedBlood_salesChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    public staff = "";
    public bankId = "";
    public sCredentials;

    blood_sales: Blood_sale[];
    user: any;
    selectedBlood_sales: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http, public db2: Blood_banksService) {
        this.initDB(this.sCredentials);
    }

    /**
     * The blood_sales App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getBlood_sales()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getBlood_sales();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getBlood_sales();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('abayelsa', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/abayelsa';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/abayelsa';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


     validateUsername(username){
        return this.http.get('https://sarutech.com/couchdblogin/'+ 'auth/validate-username/' + username).map(res => res.json());
    }


    /***********
     *blood_sale
     **********/
    /**
     * Save a blood_sale
     * @param {blood_sale} blood_sale
     *
     * @return Promise<blood_sale>
     */
    saveBlood_sale(blood_sale: Blood_sale): Promise<Blood_sale> {
        blood_sale.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('blood_sale', blood_sale)
            .then((data: any) => {

                if (data && data.blood_sales && data.blood_sales[0]) {

                    return data.blood_sales[0]

                }


                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
    * Update a blood_sale
    * @param {blood_sale} blood_sale
    *
    * @return Promise<blood_sale>
    */
    updateBlood_sale(blood_sale: Blood_sale) {

        return this.db.rel.save('blood_sale', blood_sale)
            .then((data: any) => {
                if (data && data.blood_sales && data.blood_sales
                [0]) {
                    return data.blood_sales[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a blood_sale
     * @param {blood_sale} Blood_sale
     *
     * @return Promise<boolean>
     */
    removeBlood_sale(blood_sale: Blood_sale): Promise<boolean> {
        return this.db.rel.del('blood_sale', blood_sale)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the blood_sales
     *
     * @return Promise<Array<blood_sale>>
     */
    getBlood_sales(): Promise<Array<Blood_sale>> {
        return this.db.rel.find('blood_sale')
            .then((data: any) => {
                this.blood_sales = data.blood_sales;
                if (this.searchText && this.searchText !== '') {
                    this.blood_sales = FuseUtils.filterArrayByString(this.blood_sales, this.searchText);
                }
                return Promise.resolve(this.blood_sales);
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a blood_sale
     * @param {blood_sale} blood_sale
     *
     * @return Promise<blood_sale>
     */
    getBlood_sale(blood_sale: Blood_sale): Promise<Blood_sale> {
        return this.db.rel.find('blood_sale', blood_sale.id)
            .then((data: any) => {
                return data && data.blood_sales ? data.blood_sales[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getBlood_sale2(id): Promise<Blood_sale> {
        return this.db.rel.find('blood_sale', id)
            .then((data: any) => {
                return data && data.blood_sales ? data.blood_sales[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Toggle selected blood_sale by id
     * @param id
     */
    toggleSelectedBlood_sale(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedBlood_sales.length > 0) {
            const index = this.selectedBlood_sales.indexOf(id);

            if (index !== -1) {
                this.selectedBlood_sales.splice(index, 1);

                // Trigger the next event
                this.onSelectedBlood_salesChanged.next(this.selectedBlood_sales);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedBlood_sales.push(id);

        // Trigger the next event
        this.onSelectedBlood_salesChanged.next(this.selectedBlood_sales);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedBlood_sales.length > 0) {
            this.deselectBlood_sales();
        }
        else {
            this.selectBlood_sales();
        }
    }

    selectBlood_sales(filterParameter?, filterValue?) {
        this.selectedBlood_sales = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedBlood_sales = [];
            this.blood_sales.map(blood_sale => {
                this.selectedBlood_sales.push(blood_sale.id);
            });
        }
        else {
            /* this.selecteddriverlicenses.push(...
                 this.driverlicenses.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedBlood_salesChanged.next(this.selectedBlood_sales);
    }





    deselectBlood_sales() {
        this.selectedBlood_sales = [];

        // Trigger the next event
        this.onSelectedBlood_salesChanged.next(this.selectedBlood_sales);
    }

    deleteBlood_sale(blood_sale) {
        this.db.rel.del('blood_sale', blood_sale)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const blood_saleIndex = this.blood_sales.indexOf(blood_sale);
                this.blood_sales.splice(blood_saleIndex, 1);
                this.onBlood_salesChanged.next(this.blood_sales);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedBlood_sales() {
        for (const blood_saleId of this.selectedBlood_sales) {
            const blood_sale = this.blood_sales.find(_blood_sale => {
                return _blood_sale.id === blood_saleId;
            });

            this.db.rel.del('blood_sale', blood_sale)
                .then((data: any) => {
                    this.db2.getBlood_bank2(blood_sale.blood_groupId).then(data => {
                        data.status = data.status - (-blood_sale.amount_blood);
                        this.db2.updateBlood_bank(data);
                    })
                    //return data && data.deleted ? data.deleted: false;
                    const blood_saleIndex = this.blood_sales.indexOf(blood_sale);
                    this.blood_sales.splice(blood_saleIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onBlood_salesChanged.next(this.blood_sales);
        this.deselectBlood_sales();
    }
}
