import { Component, OnInit } from '@angular/core';
import { Blood_salesService } from '../blood_sales.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseBlood_salesSelectedBarComponent implements OnInit
{
    selectedBlood_sales: string[];
    hasSelectedBlood_sales: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private blood_salesService: PouchService,
        public dialog: MdDialog
    )
    {
        this.blood_salesService.onSelectedBlood_salesChanged
            .subscribe(selectedBlood_sales => {
                this.selectedBlood_sales = selectedBlood_sales;
                setTimeout(() => {
                    this.hasSelectedBlood_sales = selectedBlood_sales.length > 0;
                    this.isIndeterminate = (selectedBlood_sales.length !== this.blood_salesService.blood_sales.length && selectedBlood_sales.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.blood_salesService.selectBlood_sales();
    }

    deselectAll()
    {
        this.blood_salesService.deselectBlood_sales();
    }

    deleteSelectedBlood_sales()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected Driver licenses?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.blood_salesService.deleteSelectedBlood_sales();
            }
            this.confirmDialogRef = null;
        });
    }

}
