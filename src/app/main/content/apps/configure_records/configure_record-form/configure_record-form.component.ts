import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Configurerecord } from '../configure_record.model';
import { ConfigurerecordsService } from '../configure_records.service';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-configurerecords-configurerecord-form-dialog',
    templateUrl: './configure_record-form.component.html',
    styleUrls: ['./configure_record-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseConfigurerecordsConfigurerecordFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    configurerecordForm: FormGroup;
    action: string;
    configurerecord: Configurerecord;
    public categorys;
    public types;
    category

    constructor(
        public dialogRef: MdDialogRef<FuseConfigurerecordsConfigurerecordFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        private db: PouchService
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit configurerecord';
            this.configurerecord = data.configurerecord;
        }
        else {
            this.dialogTitle = 'New configurerecord';
            this.configurerecord = {
                id: '',
                rev: '',
                name: '',
                category: '',
                type: ''
            }
        }

        this.configurerecordForm = this.createconfigurerecordForm();
    }

    ngOnInit() {
        this.categorys = ['General', 'Medical History', 'Social History', 'Medical Conditions', 'Current Medications', 'Family History', 'Notes']
        this.types = ['input', 'text', 'checkbox','date']
    }

    getCategory(){
       this.db.category = this.configurerecord.category;
       
    }

    createconfigurerecordForm() {
        return this.formBuilder.group({
            id: [this.configurerecord.id],
            rev: [this.configurerecord.rev],
            name: [this.configurerecord.name],
            type: [this.configurerecord.type],
            category: [this.configurerecord.category],

        });
    }
}
