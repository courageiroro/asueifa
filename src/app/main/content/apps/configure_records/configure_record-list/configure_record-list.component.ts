import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ConfigurerecordsService } from '../configure_records.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { FuseConfigurerecordsConfigurerecordFormDialogComponent } from '../configure_record-form/configure_record-form.component';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Configurerecord } from '../configure_record.model';
import { Subject } from 'rxjs/Subject';
import { Http, Headers, RequestOptions } from '@angular/http';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-configurerecords-configurerecord-list',
    templateUrl: './configure_record-list.component.html',
    styleUrls: ['./configure_record-list.component.scss']
})
export class FuseConfigurerecordsConfigurerecordListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public configurerecords: Array<Configurerecord> = [];
    configurerecords2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    //dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'name', 'description', 'buttons'];
    selectedConfigurerecords: any[];
    online = true;
    checkboxes: {};
    general = [];
    refresh: Subject<any> = new Subject();
    medicalhistory = [];
    socialhistory = [];
    medicalconditions = [];
    currentmedications = [];
    familyhistory = [];
    notes = [];
    recordss: Configurerecord[];

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(public dialog: MdDialog, public db: PouchService, public http: Http)
    { }

    ngOnInit() {
        //this.dataSource = new FilesDataSource(this.db);
        this._loadConfigurerecords();
    }

    private _loadConfigurerecords(): void {
        this.db.getConfigurerecords()
            .then(configurerecords => {
                this.configurerecords = configurerecords;
                //this.configurerecords2 = new BehaviorSubject<any>(configurerecords);
                
                this.general = this.configurerecords.filter(data => data.category == 'General');
                this.refresh.next(this.general);
                this.medicalhistory = this.configurerecords.filter(data => data.category == 'Medical History');
                this.socialhistory = this.configurerecords.filter(data => data.category == 'Social History');
                this.medicalconditions = this.configurerecords.filter(data => data.category == 'Medical Conditions');
                this.currentmedications = this.configurerecords.filter(data => data.category == 'Current Medications');
                this.familyhistory = this.configurerecords.filter(data => data.category == 'Family History');
                this.notes = this.configurerecords.filter(data => data.category == 'Notes');

                //console.log(this.configurerecords2);
            });

    }


    onlineCheck() {
        this.online = true;
        this.http.get('https://sarutech.com/couchdblogin/auth/validate-username/xs2da2432252sda').subscribe(data => {
            this.online = true;
         
        },
            (err) => {
                this.online = false;
                
            });
    }

    click() {
        this.onlineCheck();
    }

    newConfigurerecord() {
        this.dialogRef = this.dialog.open(FuseConfigurerecordsConfigurerecordFormDialogComponent, {
            panelClass: 'configurerecord-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
              
                if (this.db.category == "General") {
                    this.general.push(response.getRawValue());
                }
                else if (this.db.category == "Medical History") {
                    this.medicalhistory.push(response.getRawValue());
                }
                else if (this.db.category == "Social History") {
                    this.socialhistory.push(response.getRawValue());
                }
                else if (this.db.category == "Medical Conditions") {
                    this.medicalconditions.push(response.getRawValue());
                }
                else if (this.db.category == "Current Medications") {
                    this.currentmedications.push(response.getRawValue());
                }
                else if (this.db.category == "Family History") {
                    this.familyhistory.push(response.getRawValue());
                }
                else if (this.db.category == "Notes") {
                    this.notes.push(response.getRawValue());
                }
                this.db.saveConfigurerecord(response.getRawValue());
                this.refresh.next(true);
                //this.dataSource = new FilesDataSource(this.db);
                

            });

    }

    editConfigurerecord(configurerecord) {
        
        this.dialogRef = this.dialog.open(FuseConfigurerecordsConfigurerecordFormDialogComponent, {
            panelClass: 'configurerecord-form-dialog',
            data: {
                configurerecord: configurerecord,
                action: 'edit'

            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                //this.general.push(formData.getRawValue());
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':
                        if (configurerecord.category == "General") {
                            
                                configurerecord.id = '',
                                configurerecord.rev = '',
                                configurerecord.name = formData.getRawValue().name,
                                configurerecord.type = formData.getRawValue().type,
                                configurerecord.category = formData.getRawValue().category
                            
                        }
                        else if (configurerecord.category == "Medical History") {
                                configurerecord.id = '',
                                configurerecord.rev = '',
                                configurerecord.name = formData.getRawValue().name,
                                configurerecord.type = formData.getRawValue().type,
                                configurerecord.category = formData.getRawValue().category

                        }
                        else if (configurerecord.category == "Social History") {
                                configurerecord.id = '',
                                configurerecord.rev = '',
                                configurerecord.name = formData.getRawValue().name,
                                configurerecord.type = formData.getRawValue().type,
                                configurerecord.category = formData.getRawValue().category
                        }
                        else if (configurerecord.category == "Medical Conditions") {
                                configurerecord.id = '',
                                configurerecord.rev = '',
                                configurerecord.name = formData.getRawValue().name,
                                configurerecord.type = formData.getRawValue().type,
                                configurerecord.category = formData.getRawValue().category
                        }
                        else if (configurerecord.category == "Current Medications") {
                                configurerecord.id = '',
                                configurerecord.rev = '',
                                configurerecord.name = formData.getRawValue().name,
                                configurerecord.type = formData.getRawValue().type,
                                configurerecord.category = formData.getRawValue().category
                        }
                        else if (configurerecord.category == "Family History") {
                                configurerecord.id = '',
                                configurerecord.rev = '',
                                configurerecord.name = formData.getRawValue().name,
                                configurerecord.type = formData.getRawValue().type,
                                configurerecord.category = formData.getRawValue().category
                        }
                        else if (configurerecord.category == "Notes") {
                                configurerecord.id = '',
                                configurerecord.rev = '',
                                configurerecord.name = formData.getRawValue().name,
                                configurerecord.type = formData.getRawValue().type,
                                configurerecord.category = formData.getRawValue().category
                        }

                        this.db.updateConfigurerecord(formData.getRawValue());
                        this.refresh.next(true);
                        //this.dataSource = new FilesDataSource(this.db);
                        

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteConfigurerecord(configurerecord);

                        break;
                }
            });
    }

    /**
     * Delete configurerecord
     */
    deleteConfigurerecord(configurerecord) {
        
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                if (configurerecord.category == "General") {
                  var index =  this.general.indexOf(configurerecord);
                  this.general.splice(index,1);
                   
                }
                 else if (configurerecord.category == "Medical History") {
                              var index =  this.general.indexOf(configurerecord);
                              this.general.splice(index,1);
                              
                        }
                        else if (configurerecord.category == "Social History") {
                              var index =  this.general.indexOf(configurerecord);
                              this.socialhistory.splice(index,1);
                        }
                        else if (configurerecord.category == "Medical Conditions") {
                                var index =  this.general.indexOf(configurerecord);
                                this.medicalconditions.splice(index,1);
                        }
                        else if (configurerecord.category == "Current Medications") {
                                var index =  this.general.indexOf(configurerecord);
                                this.currentmedications.splice(index,1);
                        }
                        else if (configurerecord.category == "Family History") {
                                 var index =  this.general.indexOf(configurerecord);
                                 this.familyhistory.splice(index,1);
                        }
                        else if (configurerecord.category == "Notes") {
                                 var index =  this.general.indexOf(configurerecord);
                                 this.notes.splice(index,1);
                        }
                this.db.deleteConfigurerecord(configurerecord);
                //this.dataSource = new FilesDataSource(this.db);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(configurerecordId) {
        this.db.toggleSelectedConfigurerecord(configurerecordId);
    }

}

/* export class FilesDataSource extends DataSource<any>
{
    configurerecords2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: ConfigurerecordsService) {
        super();
    }

     //Connect function called by the table to retrieve one stream containing the data to render.
    connect(): Observable<any[]> {
        var result = Observable.fromPromise(this.db.getConfigurerecords());
        result.subscribe(x => console.log(x), e => console.error(e));
        return result;
    }

    disconnect() {
    }
}
 */