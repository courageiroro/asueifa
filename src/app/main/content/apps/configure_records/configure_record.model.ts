export interface Configurerecord {
    id: string,
    rev: string,
    name: string,
    category: string,
    type: string
}

