import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ConfigurerecordsService } from './configure_records.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector: 'fuse-configurerecords',
    templateUrl: './configure_records.component.html',
    styleUrls: ['./configure_records.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [Animations.slideInTop]
})
export class FuseConfigurerecordsComponent implements OnInit {
    hasSelectedConfigurerecords: boolean;
    searchInput: FormControl;

    constructor(private configurerecordsService: PouchService) {
        this.searchInput = new FormControl('');
       
    }

    ngOnInit() {

        this.configurerecordsService.onSelectedConfigurerecordsChanged
            .subscribe(selectedConfigurerecords => {
                this.hasSelectedConfigurerecords = selectedConfigurerecords.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.configurerecordsService.onSearchTextChanged.next(searchText);
            });
    }

}
