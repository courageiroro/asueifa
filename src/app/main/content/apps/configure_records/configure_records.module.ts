import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseConfigurerecordsComponent } from './configure_records.component';
import { ConfigurerecordsService } from './configure_records.service';
//import { UserService } from '../../../../user.service';
//import { configurerecordsService2 } from './contacts.service';
import { FuseConfigurerecordsConfigurerecordListComponent } from './configure_record-list/configure_record-list.component';
import { FuseConfigurerecordsSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseConfigurerecordsConfigurerecordFormDialogComponent } from './configure_record-form/configure_record-form.component';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/configurerecord',
        component: FuseConfigurerecordsComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            configurerecords: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseConfigurerecordsComponent,
        FuseConfigurerecordsConfigurerecordListComponent,
        FuseConfigurerecordsSelectedBarComponent,
        FuseConfigurerecordsConfigurerecordFormDialogComponent
    ],
    providers      : [
        ConfigurerecordsService,
        PouchService,
        AuthGuard,
       //UserService
    ],
    entryComponents: [FuseConfigurerecordsConfigurerecordFormDialogComponent]
})
export class FuseConfigurerecordsModule
{
}
