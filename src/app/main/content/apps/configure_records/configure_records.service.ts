/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
PouchDB.plugin(require('pouchdb-find'));
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('relational-pouch'));

import { Configurerecord } from './configure_record.model';
import { Schema } from '../schema';

@Injectable()
export class ConfigurerecordsService {
    public configurerecord = "";
    public sCredentials;
    public category;
    onConfigurerecordsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedConfigurerecordsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    configurerecords: Configurerecord[];
    user: any;
    selectedConfigurerecords: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The configurerecords App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getConfigurerecords()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getConfigurerecords();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getConfigurerecords();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('abayelsa', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/abayelsa';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/abayelsa';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }


    /***********
     * configurerecord
     **********/
    /**
     * Save a configurerecord
     * @param {configurerecord} Configurerecord
     *
     * @return Promise<configurerecord>
     */
    saveConfigurerecord(configurerecord: Configurerecord): Promise<Configurerecord> {
        //configurerecord.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('configurerecord', configurerecord)
            .then((data: any) => {

                if (data && data.configurerecords && data.configurerecords
                [0]) {
                    console.log('save');
                
                    return data.configurerecords[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a configurerecord
    * @param {configurerecord} configurerecord
    *
    * @return Promise<configurerecord>
    */
    updateConfigurerecord(configurerecord: Configurerecord) {
        return this.db.rel.save('configurerecord', configurerecord)
            .then((data: any) => {
                if (data && data.configurerecords && data.configurerecords
                [0]) {
                    console.log('Update')
                    
                    return data.configurerecords[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a configurerecord
     * @param {configurerecord} configurerecord
     *
     * @return Promise<boolean>
     */
    removeConfigurerecord(configurerecord: Configurerecord): Promise<boolean> {
        return this.db.rel.del('configurerecord', configurerecord)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the configurerecords
     *
     * @return Promise<Array<configurerecord>>
     */
    getConfigurerecords(): Promise<Array<Configurerecord>> {
        return this.db.rel.find('configurerecord')
            .then((data: any) => {
                this.configurerecords = data.configurerecords;
                if (this.searchText && this.searchText !== '') {
                    this.configurerecords = FuseUtils.filterArrayByString(this.configurerecords, this.searchText);
                }
                //this.onconfigurerecordsChanged.next(this.configurerecords);
                return Promise.resolve(this.configurerecords);
                //return data.configurerecords ? data.configurerecords : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getDoctorConfigurerecords(): Promise<Array<Configurerecord>> {
        return this.db.rel.find('configurerecord')
            .then((data: any) => {
                return data.configurerecords ? data.configurerecords : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a configurerecord
     * @param {configurerecord} configurerecord
     *
     * @return Promise<configurerecord>
     */
    getConfigurerecord(configurerecord: Configurerecord): Promise<Configurerecord> {
        return this.db.rel.find('configurerecord', configurerecord.id)
            .then((data: any) => {
                console.log("Get")
                
                return data && data.configurerecords ? data.configurerecords[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected configurerecord by id
     * @param id
     */
    toggleSelectedConfigurerecord(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedConfigurerecords.length > 0) {
            const index = this.selectedConfigurerecords.indexOf(id);

            if (index !== -1) {
                this.selectedConfigurerecords.splice(index, 1);

                // Trigger the next event
                this.onSelectedConfigurerecordsChanged.next(this.selectedConfigurerecords);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedConfigurerecords.push(id);

        // Trigger the next event
        this.onSelectedConfigurerecordsChanged.next(this.selectedConfigurerecords);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedConfigurerecords.length > 0) {
            this.deselectConfigurerecords();
        }
        else {
            this.selectConfigurerecords();
        }
    }

    selectConfigurerecords(filterParameter?, filterValue?) {
        this.selectedConfigurerecords = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedConfigurerecords = [];
            this.configurerecords.map(configurerecord => {
                this.selectedConfigurerecords.push(configurerecord.id);
            });
        }
        else {
            /* this.selectedconfigurerecords.push(...
                 this.configurerecords.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedConfigurerecordsChanged.next(this.selectedConfigurerecords);
    }





    deselectConfigurerecords() {
        this.selectedConfigurerecords = [];

        // Trigger the next event
        this.onSelectedConfigurerecordsChanged.next(this.selectedConfigurerecords);
    }

    deleteConfigurerecord(configurerecord) {
        this.db.rel.del('configurerecord', configurerecord)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const configurerecordIndex = this.configurerecords.indexOf(configurerecord);
                this.configurerecords.splice(configurerecordIndex, 1);
                this.onConfigurerecordsChanged.next(this.configurerecords);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedConfigurerecords() {
        for (const configurerecordId of this.selectedConfigurerecords) {
            const configurerecord = this.configurerecords.find(_configurerecord => {
                return _configurerecord.id === configurerecordId;
            });

            this.db.rel.del('configurerecord', configurerecord)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const configurerecordIndex = this.configurerecords.indexOf(configurerecord);
                    this.configurerecords.splice(configurerecordIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onConfigurerecordsChanged.next(this.configurerecords);
        this.deselectConfigurerecords();
    }
}
