import { Component, OnInit } from '@angular/core';
import { ConfigurerecordsService } from '../configure_records.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseConfigurerecordsSelectedBarComponent implements OnInit
{
    selectedConfigurerecords: string[];
    hasSelectedConfigurerecords: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private configurerecordsService: PouchService,
        public dialog: MdDialog
    )
    {
        this.configurerecordsService.onSelectedConfigurerecordsChanged
            .subscribe(selectedConfigurerecords => {
                this.selectedConfigurerecords = selectedConfigurerecords;
                setTimeout(() => {
                    this.hasSelectedConfigurerecords = selectedConfigurerecords.length > 0;
                    this.isIndeterminate = (selectedConfigurerecords.length !== this.configurerecordsService.configurerecords.length && selectedConfigurerecords.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.configurerecordsService.selectConfigurerecords();
    }

    deselectAll()
    {
        this.configurerecordsService.deselectConfigurerecords();
    }

    deleteSelectedConfigurerecords()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected configurerecords?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.configurerecordsService.deleteSelectedConfigurerecords();
            }
            this.confirmDialogRef = null;
        });
    }

}
