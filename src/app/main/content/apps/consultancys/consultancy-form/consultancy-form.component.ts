import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Consultancy } from '../consultancy.model';

@Component({
    selector: 'fuse-consultancys-consultancy-form-dialog',
    templateUrl: './consultancy-form.component.html',
    styleUrls: ['./consultancy-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseConsultancysConsultancyFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    consultancyForm: FormGroup;
    action: string;
    consultancy: Consultancy;

    constructor(
        public dialogRef: MdDialogRef<FuseConsultancysConsultancyFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Consultancy';
            this.consultancy = data.consultancy;
        }
        else {
            this.dialogTitle = 'New Consultancy';
            this.consultancy = {
                id: '',
                rev: '',
                observations: '',
                date: new Date,
                plans: '',
                treatments:'',
                patientId:''
            }
        }

        this.consultancyForm = this.createConsultancyForm();
    }

    ngOnInit() {
    }

    createConsultancyForm() {
        return this.formBuilder.group({
            id: [this.consultancy.id],
            rev: [this.consultancy.rev],
            observations: [this.consultancy.observations],
            date: [this.consultancy.date],
            plans: [this.consultancy.plans],
            treatments: [this.consultancy.treatments],
            patientId: [this.consultancy.patientId]
        });
    }
}
