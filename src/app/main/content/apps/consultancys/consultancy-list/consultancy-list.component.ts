import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ConsultancysService } from '../consultancys.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { FuseConsultancysConsultancyFormDialogComponent } from '../consultancy-form/consultancy-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Consultancy } from '../consultancy.model';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Http, Headers, RequestOptions } from '@angular/http';
import {PouchService} from '../../../../../provider/pouch-service';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';

@Component({
    selector: 'fuse-consultancys-consultancy-list',
    templateUrl: './consultancy-list.component.html',
    styleUrls: ['./consultancy-list.component.scss']
})
export class FuseConsultancysConsultancyListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public consultancys: Array<Consultancy> = [];
    consultancys2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'treatment', 'complaint', 'plan', 'date', 'buttons'];
    selectedconsultancys: any[];
    online = true;
    checkboxes: {};
    patientId;

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;

    constructor(public dialog: MdDialog, public db: PouchService, public http: Http, public activateroute: ActivatedRoute) {
        let id = this.activateroute.snapshot.params['id']
        this.db.patientId = id;
        
    }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        this._loadConsultancys();
    }

    private _loadConsultancys(): void {
        this.db.onConsultancysChanged.subscribe(data => {
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        this.db.getConsultancys2()
            .then((consultancys: Array<Consultancy>) => {
                this.consultancys = consultancys;
                this.consultancys2 = new BehaviorSubject<any>(consultancys);
                //console.log(this.consultancys2);

                this.checkboxes = {};
                consultancys.map(consultancy => {
                    this.checkboxes[consultancy.id] = false;
                });
                this.db.onSelectedConsultancysChanged.subscribe(selectedConsultancys => {
                    for (const id in this.checkboxes) {
                        this.checkboxes[id] = selectedConsultancys.includes(id);
                    }

                    this.selectedconsultancys = selectedConsultancys;
                });
                this.db.onSearchTextChanged.subscribe(data => {
                    this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                })
            });

    }


    onlineCheck() {
        this.online = true;
        this.http.get('https://sarutech.com/couchdblogin/auth/validate-username/xs2da2432252sda').subscribe(data => {
            this.online = true;
        
        },
            (err) => {
                this.online = false;
                
            });
    }

    click() {
        this.onlineCheck();
    }

    newConsultancy() {
        
        this.dialogRef = this.dialog.open(FuseConsultancysConsultancyFormDialogComponent, {
            panelClass: 'consultancy-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
                var res = response.getRawValue();
                res.patientId = this.db.patientId
                res.date = new Date(res.date).toISOString();
                this.db.saveConsultancy(res);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                

            });

    }

    editConsultancy(consultancy) {
        
        this.dialogRef = this.dialog.open(FuseConsultancysConsultancyFormDialogComponent, {
            panelClass: 'consultancy-form-dialog',
            data: {
                consultancy: consultancy,
                action: 'edit'

            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                var res = formData.getRawValue();
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':
                        res.patientId = this.db.patientId
                        res.date = new Date(res.date).toISOString();
                        this.db.updateConsultancy(res);
                        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                        
                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteConsultancy(consultancy);

                        break;
                }
            });
    }

    /**
     * Delete consultancy
     */
    deleteConsultancy(consultancy) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deleteConsultancy(consultancy);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(consultancyId) {
        this.db.toggleSelectedConsultancy(consultancyId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    consultancys2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            of(this.db.consultancys).delay(5000),
            this.sort.mdSortChange,
            this._paginator.page,
        ];

        var result = Observable.fromPromise(this.db.getConsultancys2());
       /*  result.subscribe(x => console.log(x), e => console.error(e));
        return result; */

        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.consultancys]));
        });
    }

    disconnect() {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
          return data;
        }
    
        return data.sort((a, b) => {
          const isAsc = this.sort.direction === 'asc';
          switch (this.sort.active) {
            case 'complaint': return compare(a.observations, b.observations, isAsc);
            case 'amount': return compare(+a.amount, +b.amount, isAsc);
            case 'id': return compare(+a.id, +b.id, isAsc);
            default: return 0;
          }
        });
      }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }