export interface Consultancy {
    id: string,
    rev: string,
    observations: string,
    treatments: string,
    plans: string,
    date: Date,
    patientId: string
}

