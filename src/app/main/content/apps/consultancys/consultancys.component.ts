import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ConsultancysService } from './consultancys.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector: 'fuse-consultancys',
    templateUrl: './consultancys.component.html',
    styleUrls: ['./consultancys.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [Animations.slideInTop]
})
export class FuseConsultancysComponent implements OnInit {
    hasSelectedConsultancys: boolean;
    searchInput: FormControl;

    constructor(private consultancysService: PouchService) {
        this.searchInput = new FormControl('');
       
    }

    ngOnInit() {

        this.consultancysService.onSelectedConsultancysChanged
            .subscribe(selectedConsultancys => {
                this.hasSelectedConsultancys = selectedConsultancys.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.consultancysService.onSearchTextChanged.next(searchText);
            });
    }

}
