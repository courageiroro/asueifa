import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseConsultancysComponent } from './consultancys.component';
import { ConsultancysService } from './consultancys.service';
//import { UserService } from '../../../../user.service';
//import { consultancysService2 } from './contacts.service';
import { FuseConsultancysConsultancyListComponent } from './consultancy-list/consultancy-list.component';
import { FuseConsultancysSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseConsultancysConsultancyFormDialogComponent } from './consultancy-form/consultancy-form.component';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/consultancy/:id',
        component: FuseConsultancysComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            consultancys: ConsultancysService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseConsultancysComponent,
        FuseConsultancysConsultancyListComponent,
        FuseConsultancysSelectedBarComponent,
        FuseConsultancysConsultancyFormDialogComponent
    ],
    providers      : [
        ConsultancysService,
        PouchService,
        AuthGuard,
       //UserService
    ],
    entryComponents: [FuseConsultancysConsultancyFormDialogComponent]
})
export class FuseConsultancysModule
{
}
