/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
PouchDB.plugin(require('pouchdb-find'));
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('relational-pouch'));

import { Consultancy } from './consultancy.model';
import { Schema } from '../schema';

@Injectable()
export class ConsultancysService {
    public consultancy = "";
    public sCredentials;
    patientId;
    onConsultancysChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedConsultancysChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    consultancys: Consultancy[];
    user: any;
    selectedConsultancys: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The consultancys App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getConsultancys()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getConsultancys();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getConsultancys();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('abayelsa', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/abayelsa';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/abayelsa';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }

    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }


    /***********
     * consultancy
     **********/
    /**
     * Save a consultancy
     * @param {consultancy} consultancy
     *
     * @return Promise<consultancy>
     */
    saveConsultancy(consultancy: Consultancy): Promise<Consultancy> {
        consultancy.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('consultancy', consultancy)
            .then((data: any) => {

                if (data && data.consultancys && data.consultancys
                [0]) {
                    console.log('save');
                    
                    return data.consultancys[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a consultancy
    * @param {consultancy} Consultancy
    *
    * @return Promise<consultancy>
    */
    updateConsultancy(consultancy: Consultancy) {
        return this.db.rel.save('consultancy', consultancy)
            .then((data: any) => {
                if (data && data.consultancys && data.consultancys
                [0]) {
                    console.log('Update')
                    
                    return data.consultancys[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a consultancy
     * @param {consultancy} consultancy
     *
     * @return Promise<boolean>
     */
    removeConsultancy(consultancy: Consultancy): Promise<boolean> {
        return this.db.rel.del('consultancy', consultancy)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the consultancys
     *
     * @return Promise<Array<consultancy>>
     */
    getConsultancys(): Promise<Array<Consultancy>> {
        return this.db.rel.find('consultancy')
            .then((data: any) => {
                this.consultancys = data.consultancys;
                if (this.searchText && this.searchText !== '') {
                    this.consultancys = FuseUtils.filterArrayByString(this.consultancys, this.searchText);
                }
                //this.onconsultancysChanged.next(this.consultancys);
                return Promise.resolve(this.consultancys);
                //return data.consultancys ? data.consultancys : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

     getConsultancys2(): Promise<Array<Consultancy>> {
        return this.db.rel.find('consultancy')
            .then((data: any) => {
                this.consultancys = data.consultancys;
                if (this.searchText && this.searchText !== '') {
                    this.consultancys = FuseUtils.filterArrayByString(this.consultancys, this.searchText);
                }
                //this.onconsultancysChanged.next(this.consultancys);
                this.consultancys = this.consultancys.filter(data=>data.patientId == this.patientId)
                return Promise.resolve(this.consultancys);
                //return data.consultancys ? data.consultancys : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getDoctorConsultancys(): Promise<Array<Consultancy>> {
        return this.db.rel.find('consultancy')
            .then((data: any) => {
                return data.consultancys ? data.consultancys : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a consultancy
     * @param {consultancy} consultancy
     *
     * @return Promise<consultancy>
     */
    getConsultancy(consultancy: Consultancy): Promise<Consultancy> {
        return this.db.rel.find('consultancy', consultancy.id)
            .then((data: any) => {
                console.log("Get")
                
                return data && data.consultancys ? data.consultancys[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected consultancy by id
     * @param id
     */
    toggleSelectedConsultancy(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedConsultancys.length > 0) {
            const index = this.selectedConsultancys.indexOf(id);

            if (index !== -1) {
                this.selectedConsultancys.splice(index, 1);

                // Trigger the next event
                this.onSelectedConsultancysChanged.next(this.selectedConsultancys);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedConsultancys.push(id);

        // Trigger the next event
        this.onSelectedConsultancysChanged.next(this.selectedConsultancys);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedConsultancys.length > 0) {
            this.deselectConsultancys();
        }
        else {
            this.selectConsultancys();
        }
    }

    selectConsultancys(filterParameter?, filterValue?) {
        this.selectedConsultancys = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedConsultancys = [];
            this.consultancys.map(consultancy => {
                this.selectedConsultancys.push(consultancy.id);
            });
        }
        else {
            /* this.selectedconsultancys.push(...
                 this.consultancys.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedConsultancysChanged.next(this.selectedConsultancys);
    }





    deselectConsultancys() {
        this.selectedConsultancys = [];

        // Trigger the next event
        this.onSelectedConsultancysChanged.next(this.selectedConsultancys);
    }

    deleteConsultancy(consultancy) {
        this.db.rel.del('consultancy', consultancy)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const consultancyIndex = this.consultancys.indexOf(consultancy);
                this.consultancys.splice(consultancyIndex, 1);
                this.onConsultancysChanged.next(this.consultancys);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedConsultancys() {
        for (const consultancyId of this.selectedConsultancys) {
            const consultancy = this.consultancys.find(_consultancy => {
                return _consultancy.id === consultancyId;
            });

            this.db.rel.del('consultancy', consultancy)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const consultancyIndex = this.consultancys.indexOf(consultancy);
                    this.consultancys.splice(consultancyIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onConsultancysChanged.next(this.consultancys);
        this.deselectConsultancys();
    }
}
