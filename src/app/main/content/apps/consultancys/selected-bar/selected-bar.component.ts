import { Component, OnInit } from '@angular/core';
import { ConsultancysService } from '../consultancys.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseConsultancysSelectedBarComponent implements OnInit
{
    selectedConsultancys: string[];
    hasSelectedConsultancys: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private consultancysService: PouchService,
        public dialog: MdDialog
    )
    {
        this.consultancysService.onSelectedConsultancysChanged
            .subscribe(selectedConsultancys => {
                this.selectedConsultancys = selectedConsultancys;
                setTimeout(() => {
                    this.hasSelectedConsultancys = selectedConsultancys.length > 0;
                    this.isIndeterminate = (selectedConsultancys.length !== this.consultancysService.consultancys.length && selectedConsultancys.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.consultancysService.selectConsultancys();
    }

    deselectAll()
    {
        this.consultancysService.deselectConsultancys();
    }

    deleteSelectedConsultancys()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected consultancies?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.consultancysService.deleteSelectedConsultancys();
            }
            this.confirmDialogRef = null;
        });
    }

}
