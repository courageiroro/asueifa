import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg'
import { FuseConfigService } from './../../../../core/services/config.service';
import { StaffsService } from './../../apps/staffs/staffs.service';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs/Rx';
import 'rxjs/Rx';
import { Staff } from './../staffs/staff.model';
import { DepartmentsService } from './../departments/departments.service';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector: 'fuse-create-account',
    templateUrl: './create-account.component.html',
    styleUrls: ['./create-account.component.scss']
})
export class FuseCreateAccountComponent implements OnInit {
    createAccountForm: FormGroup;
    createAccountFormErrors: any;
    public email;
    public answer;
    public secretQuestion;
    public data;
    public departmentss;
    public password;
    public error;
    public userTypes;
    

    constructor(
        private fuseConfig: FuseConfigService,
        private formBuilder: FormBuilder,
        private staffservice: PouchService,
        private router: Router,
    ) {

        /*  var userType = { usertype: "null" }
         localStorage.setItem('user', JSON.stringify(userType)); */

        this.fuseConfig.setSettings({
            layout: {
                navigation: 'none',
                toolbar: 'none',
                footer: 'none'
            }
        });

        this.createAccountFormErrors = {
            email: {},
            answer: {}
        };

        staffservice.getDepartments().then(res => {
            this.departmentss = res;
        })
    }


    emailChange() {
        this.staffservice.getADStaffs().then(res => {
            res = res.filter(data => data.email == this.email.trim());
            this.data = res;
            this.secretQuestion = res[0].secretquestion;
            
        })
    }

    revealPassword() {

        if (this.answer.trim() == this.data[0].answer) {
            this.password = this.data[0].password;
            this.error = "";
        }
        else if (this.answer.trim() != this.data[0].answer) {
            this.error = "Wrong answer, please check your answer";
        }
    }

    navLP() {
        this.router.navigate(['/apps/login']);
    }


    ngOnInit() {

        this.userTypes = ['Admin']
        
        this.createAccountForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            answer: ['',]
        });

        this.createAccountForm.valueChanges.subscribe(() => {
            this.onForgotPasswordFormValuesChanged();
        });
    }

    onForgotPasswordFormValuesChanged() {
           for ( const field in this.createAccountFormErrors )
          {
              if ( !this.createAccountFormErrors.hasOwnProperty(field) )
              {
                  continue;
              }
     
              // Clear previous errors
              this.createAccountFormErrors[field] = {};
     
              // Get the control
              const control = this.createAccountFormErrors.get(field);
     
              if ( control && control.dirty && !control.valid )
              {
                  this.createAccountFormErrors[field] = control.errors;
              }
          } 
    }
}
