import { NgModule } from '@angular/core';
import { SharedModule } from './../../../../core/modules/shared.module';
import { RouterModule, Routes} from '@angular/router';
import { StaffsService } from './../../apps/staffs/staffs.service';
import { DepartmentsService } from './../departments/departments.service';
import { RecordsService } from './../records/records.service';
import { FuseCreateAccountComponent } from './create-account.component';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/create_account',
        component: FuseCreateAccountComponent,
        resolve: {
            staffs: PouchService
        }
    }
];

@NgModule({
    declarations: [
        FuseCreateAccountComponent
    ],
    imports     : [
        SharedModule,
        RouterModule.forChild(routes),
        FroalaEditorModule.forRoot(),
        FroalaViewModule.forRoot()
    ],
    providers: [
        StaffsService,
        DepartmentsService,
        PouchService,
        RecordsService
    ]
})

export class CreateAccountModule
{

}
