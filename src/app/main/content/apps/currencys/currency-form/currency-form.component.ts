import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Currency } from '../currency.model';

@Component({
    selector: 'fuse-currencys-currency-form-dialog',
    templateUrl: './currency-form.component.html',
    styleUrls: ['./currency-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseCurrencysCurrencyFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    currencyForm: FormGroup;
    action: string;
    currency: Currency;

    constructor(
        public dialogRef: MdDialogRef<FuseCurrencysCurrencyFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Currency';
            this.currency = data.currency;
        }
        else {
            this.dialogTitle = 'New Staff Type';
            this.currency = {
                id: '',
                rev: '',
                currency_code: '',
                currency_symbol: '',
                currency_name: ''
            }
        }

        this.currencyForm = this.createcurrencyForm();
    }

    ngOnInit() {
    }

    createcurrencyForm() {
        return this.formBuilder.group({
            id: [this.currency.id],
            rev: [this.currency.rev],
            currency_code: [(this.currency.currency_code).toUpperCase()],
            currency_symbol: [this.currency.currency_symbol],
            currency_name: [this.currency.currency_name]
        });
    }
}
