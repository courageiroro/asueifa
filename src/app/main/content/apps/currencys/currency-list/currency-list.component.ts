import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { CurrencysService } from '../currencys.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject} from 'rxjs/Rx';
import { FuseCurrencysCurrencyFormDialogComponent } from '../currency-form/currency-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Currency } from '../currency.model';
import {PouchService} from '../../../../../provider/pouch-service';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';

@Component({
    selector   : 'fuse-currencys-currency-list',
    templateUrl: './currency-list.component.html',
    styleUrls  : ['./currency-list.component.scss']
})
export class FuseCurrencysCurrencyListComponent implements OnInit
{
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public currencys: Array<Currency> = [];
    currencys2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox','name','code','symbol','buttons'];
    selectedCurrencys: any[];
    checkboxes: {};

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;

    constructor(public dialog: MdDialog, public db:PouchService)
    { }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        this._loadCurrencys();
    }

    private _loadCurrencys(): void {
        this.db.onCurrencysChanged.subscribe(data =>{
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        this.db.getCurrencys()
        .then((currencys: Array<Currency>) => {
            this.currencys = currencys;
            this.currencys2= new BehaviorSubject<any>(currencys);
            //console.log(this.currencys2);

            this.checkboxes = {};
            currencys.map(currency => {
                this.checkboxes[currency.id] = false;
            });

               this.db.onSelectedCurrencysChanged.subscribe(selectedCurrencys => {
                for ( const id in this.checkboxes )
                {
                    this.checkboxes[id] = selectedCurrencys.includes(id);
                }

                this.selectedCurrencys = selectedCurrencys;
            });
        this.db.onSearchTextChanged.subscribe(data =>{
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })

        });
        
    }

    newCurrency()
    {
        this.dialogRef = this.dialog.open(FuseCurrencysCurrencyFormDialogComponent, {
            panelClass: 'currency-form-dialog',
            data      : {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if ( !response )
                {
                    return;
                }

                this.db.saveCurrency(response.getRawValue());
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

            });

    }

    editCurrency(currency)
    {
        this.dialogRef = this.dialog.open(FuseCurrencysCurrencyFormDialogComponent, {
            panelClass: 'currency-form-dialog',
            data      : {
                currency: currency,
                action : 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch ( actionType )
                {
                    /**
                     * Save
                     */
                    case 'save':

                        this.db.updateCurrency(formData.getRawValue());
                        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteCurrency(currency);

                        break;
                }
            });
    }

    /**
     * Delete currency
     */
    deleteCurrency(currency)
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.db.deleteCurrency(currency);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(currencyId)
    {
        this.db.toggleSelectedCurrency(currencyId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    currencys2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort)
    {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]>
    {
        const displayDataChanges = [
            of(this.db.currencys).delay(5000),
            this.sort.mdSortChange,
            this._paginator.page,
        ];

        var result = Observable.fromPromise(this.db.getCurrencys());
        /* result.subscribe(x => console.log(x), e => console.error(e));
        return result;    */ 

        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.currencys]));
        });
    }

    disconnect()
    {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
          return data;
        }
    
        return data.sort((a, b) => {
          const isAsc = this.sort.direction === 'asc';
          switch (this.sort.active) {
            case 'name': return compare(a.currency_name, b.currency_name, isAsc);
            case 'amount': return compare(+a.amount, +b.amount, isAsc);
            case 'id': return compare(+a.id, +b.id, isAsc);
            default: return 0;
          }
        });
      }
}

 /** Simple sort comparator for example ID/Name columns (for client-side sorting). */
 function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }