export interface Currency {
    id: string,
    rev: string,
    currency_code: string,
    currency_symbol: string,
    currency_name: string
}