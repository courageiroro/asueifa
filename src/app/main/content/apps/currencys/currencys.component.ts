import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { CurrencysService } from './currencys.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-currencys',
    templateUrl  : './currencys.component.html',
    styleUrls    : ['./currencys.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FuseCurrencysComponent implements OnInit
{
    hasSelectedCurrencys: boolean;
    searchInput: FormControl;

    constructor(private currencysService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.currencysService.onSelectedCurrencysChanged
            .subscribe(selectedCurrencys => {
                this.hasSelectedCurrencys = selectedCurrencys.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.currencysService.onSearchTextChanged.next(searchText);
            });
    }

}
