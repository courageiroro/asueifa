import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseCurrencysComponent } from './currencys.component';
import { CurrencysService } from './currencys.service';
//import { StaffsService2 } from './contacts.service';
import { FuseCurrencysCurrencyListComponent } from './currency-list/currency-list.component';
import { FuseCurrencysSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseCurrencysCurrencyFormDialogComponent } from './currency-form/currency-form.component';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/currencys',
        component: FuseCurrencysComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            currencys: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseCurrencysComponent,
        FuseCurrencysCurrencyListComponent,
        FuseCurrencysSelectedBarComponent,
        FuseCurrencysCurrencyFormDialogComponent
    ],
    providers      : [
        CurrencysService,
        PouchService,
        AuthGuard
    ],
    entryComponents: [FuseCurrencysCurrencyFormDialogComponent]
})
export class FuseCurrencysModule
{
}
