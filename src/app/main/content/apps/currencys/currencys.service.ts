/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
PouchDB.plugin(require('pouchdb-find'));
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('relational-pouch'));

import { Currency } from './currency.model';
import { Schema } from '../schema';

@Injectable()
export class CurrencysService {

    onCurrencysChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedCurrencysChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    currencys: Currency[];
    user: any;
    selectedCurrencys: string[] = [];

    searchText: string;
    filterBy: string;
    public sCredentials;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The routes App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getCurrencys()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getCurrencys();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getCurrencys();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('abayelsa', { adaptater: 'websql' });
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
        this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
        this.remote = 'http://sarutech.com:5984/abayelsa';
        this.db.sync(this.remote, options).on('change', function (change) {
            console.log('changed');
        }).on('complete', function (complete) {
            console.log('complete');
        }).on('error', function (err) {
            console.log('offline');
        });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

        this.remote = 'http://sarutech.com:5984/abayelsa';
        return this.db.sync(this.remote, options).on('change', function (change) {

            //return true;
        }).on('complete', function (complete) {
            return true;
        }).on('error', function (err) {
            console.log('offline');
        });
        //}

    }

    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }


    /***********
     * currency
     **********/
    /**
     * Save a currency
     * @param {currency} Currency
     *
     * @return Promise<currency>
     */
    saveCurrency(currency: Currency): Promise<Currency> {
        currency.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('currency', currency)
            .then((data: any) => {

                if (data && data.currencys && data.currencys
                [0]) {
                    return data.currencys[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a currency
    * @param {currency} Currency
    *
    * @return Promise<Currency>
    */
    updateCurrency(currency: Currency) {

        return this.db.rel.save('currency', currency)
            .then((data: any) => {
                if (data && data.currencys && data.currencys
                [0]) {
                    return data.routes[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a currency
     * @param {currency} Currency
     *
     * @return Promise<boolean>
     */
    removeCurrency(currency: Currency): Promise<boolean> {
        return this.db.rel.del('currency', currency)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the currencys
     *
     * @return Promise<Array<currency>>
     */
    getCurrencys(): Promise<Array<Currency>> {
        return this.db.rel.find('currency')
            .then((data: any) => {
                this.currencys = data.currencys;
                if (this.searchText && this.searchText !== '') {

                    this.currencys = FuseUtils.filterArrayByString(this.currencys, this.searchText);
                    //this.oncurrencysChanged.next(this.currencys);
                }
                return Promise.resolve(this.currencys);
                //return data.currencys ? data.currencys : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a currency
     * @param {currency} currency
     *
     * @return Promise<currency>
     */
    getCurrency(currency: Currency): Promise<Currency> {
        return this.db.rel.find('currency', currency.id)
            .then((data: any) => {
                return data && data.currencys ? data.currencys[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected route by id
     * @param id
     */
    toggleSelectedCurrency(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedCurrencys.length > 0) {
            const index = this.selectedCurrencys.indexOf(id);

            if (index !== -1) {
                this.selectedCurrencys.splice(index, 1);

                // Trigger the next event
                this.onSelectedCurrencysChanged.next(this.selectedCurrencys);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedCurrencys.push(id);

        // Trigger the next event
        this.onSelectedCurrencysChanged.next(this.selectedCurrencys);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedCurrencys.length > 0) {
            this.deselectCurrencys();
        }
        else {
            this.selectCurrencys();
        }
    }

    selectCurrencys(filterParameter?, filterValue?) {
        this.selectedCurrencys = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedCurrencys = [];
            this.currencys.map(currency => {
                this.selectedCurrencys.push(currency.id);
            });
        }
        else {
            /* this.selectedroutes.push(...
                 this.routes.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedCurrencysChanged.next(this.selectedCurrencys);
    }





    deselectCurrencys() {
        this.selectedCurrencys = [];

        // Trigger the next event
        this.onSelectedCurrencysChanged.next(this.selectedCurrencys);
    }

    deleteCurrency(currency) {
        this.db.rel.del('currency', currency)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const currencyIndex = this.currencys.indexOf(currency);
                this.currencys.splice(currencyIndex, 1);
                this.onCurrencysChanged.next(this.currencys);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedCurrencys() {
        for (const currencyId of this.selectedCurrencys) {
            const currency = this.currencys.find(_currency => {
                return _currency.id === currencyId;
            });

            this.db.rel.del('currency', currency)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const currencyIndex = this.currencys.indexOf(currency);
                    this.currencys.splice(currencyIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onCurrencysChanged.next(this.currencys);
        this.deselectCurrencys();
    }
}
