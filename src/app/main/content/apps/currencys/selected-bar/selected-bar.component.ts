import { Component, OnInit } from '@angular/core';
import { CurrencysService } from '../currencys.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseCurrencysSelectedBarComponent implements OnInit
{
    selectedCurrencys: string[];
    hasSelectedCurrencys: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private currencysService: PouchService,
        public dialog: MdDialog
    )
    {
        this.currencysService.onSelectedCurrencysChanged
            .subscribe(selectedCurrencys => {
                this.selectedCurrencys = selectedCurrencys;
                setTimeout(() => {
                    this.hasSelectedCurrencys = selectedCurrencys.length > 0;
                    this.isIndeterminate = (selectedCurrencys.length !== this.currencysService.currencys.length && selectedCurrencys.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.currencysService.selectCurrencys();
    }

    deselectAll()
    {
        this.currencysService.deselectCurrencys();
    }

    deleteSelectedCurrencys()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected Designations?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.currencysService.deleteSelectedCurrencys();
            }
            this.confirmDialogRef = null;
        });
    }

}
