import { Inject, Injectable } from '@angular/core';
import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ProjectsDashboardService } from './projects.service';
import * as shape from 'd3-shape';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { DataSource } from '@angular/cdk';
import { StaffsService } from '../../staffs/staffs.service';
import { InvoicesService } from '../../invoices/invoices.service';
import { MedicinesService } from '../../medicines/medicines.service';
import { ReportsService } from '../../reports/reports.service';
import { ExpensesService } from '../../expenses/expenses.service';
import { PayrollsService } from '../../payrolls/payrolls.service';
import { SettingsService } from '../../settings/settings.service';
import { Medicine_salesService } from '../../medicine_sales/medicine_sales.service';
import { Expense } from '../../expenses/expense.model';
import { Blood_salesService } from '../../blood_sales/blood_sales.service';
import { AppComponent } from 'app/app.component'
import { DOCUMENT } from '@angular/common';
//import { FuseSplashScreenService } from '../../../../../core/services/splash-screen.service';
import { animate, AnimationBuilder, AnimationPlayer, style } from '@angular/animations';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { WeatherSettings, TemperatureScale, ForecastMode, WeatherLayout } from 'angular-weather-widget';
import * as $ from "jquery";
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-project',
    templateUrl: './project.component.html',
    styleUrls: ['./project.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class FuseProjectComponent implements OnInit, OnDestroy {
   
    projects: any[];
    selectedProject: any;
    doctorNumber: any;
    patientNumber: any;
    nurseNumber: any;
    pharmacistNumber: any;
    labNumber: any;
    accountantNumber: any;
    invoiceNumber: any;
    bloodSaleNumber: any;
    medicineNumber: any;
    operationNumber: any;
    birthNumber: any;
    lossNumber: any;
    settingNumber: any;
    public cityName: string;
    public cityName2: string;
    jData;
    tempKelvin: any;
    tempCelsius: any;
    mintempKelvin: any;
    mintempCelsius: any;
    maxtempKelvin: any;
    maxtempCelsius: any;
    wind: any;
    setting: any;
    windDegree: any;
    humidity: any;
    visibility: any;
    cloudiness: any;
    localStorageItem: any;
    weatherImage: any;
    imageExt: string;
    minweatherImage: any;
    minimageExt: any;
    maxweatherImage: any;
    maximageExt: any;
    expenses: any;
    expenses2: any;
    expenses3: any;
    expenses4: any;
    expenses5: any;
    expenses6: any;
    expenses7: any;
    expenses8: any;
    expenses9: any;
    expenses10: any;
    expenses11: any;
    expenses12: any;
    payrolls: any;
    payrolls2: any;
    payrolls3: any;
    payrolls4: any;
    payrolls5: any;
    payrolls6: any;
    payrolls7: any;
    payrolls8: any;
    payrolls9: any;
    payrolls10: any;
    payrolls11: any;
    payrolls12: any;
    eDate: any;
    public totalExpense: any;
    eDate2: any;
    totalExpense2: any;
    eDate3: any;
    totalExpense3: any;
    eDate4: any;
    totalExpense4: any;
    eDate5: any;
    totalExpense5: any;
    eDate6: any;
    totalExpense6: any;
    eDate7: any;
    totalExpense7: any;
    eDate8: any;
    totalExpense8: any;
    eDate9: any;
    totalExpense9: any;
    eDate10: any;
    totalExpense10: any;
    eDate11: any;
    totalExpense11: any;
    eDate12: any;
    totalExpense12: any;
    pDate: any;
    public totalPayroll: any;
    pDate2: any;
    totalPayroll2: any;
    pDate3: any;
    totalPayroll3: any;
    pDate4: any;
    totalPayroll4: any;
    pDate5: any;
    totalPayroll5: any;
    pDate6: any;
    totalPayroll6: any;
    pDate7: any;
    totalPayroll7: any;
    pDate8: any;
    totalPayroll8: any;
    pDate9: any;
    totalPayroll9: any;
    pDate10: any;
    totalPayroll10: any;
    pDate11: any;
    totalPayroll11: any;
    pDate12: any;
    totalPayroll12: any;
    invoices: any;
    invoices2: any;
    invoices3: any;
    invoices4: any;
    invoices5: any;
    invoices6: any;
    invoices7: any;
    invoices8: any;
    invoices9: any;
    invoices10: any;
    invoices11: any;
    invoices12: any;
    bloodSales: any;
    bloodSales2: any;
    bloodSales3: any;
    bloodSales4: any;
    bloodSales5: any;
    bloodSales6: any;
    bloodSales7: any;
    bloodSales8: any;
    bloodSales9: any;
    bloodSales10: any;
    bloodSales11: any;
    bloodSales12: any;
    bsDate: any;
    totalBloodSale: any;
    bsDate2: any;
    totalBloodSale2: any;
    bsDate3: any;
    totalBloodSale3: any;
    bsDate4: any;
    totalBloodSale4: any;
    bsDate5: any;
    totalBloodSale5: any;
    bsDate6: any;
    totalBloodSale6: any;
    bsDate7: any;
    totalBloodSale7: any;
    bsDate8: any;
    totalBloodSale8: any;
    bsDate9: any;
    totalBloodSale9: any;
    bsDate10: any;
    totalBloodSale10: any;
    bsDate11: any;
    totalBloodSale11: any;
    bsDate12: any;
    totalBloodSale12: any;
    iDate: any;
    totalInvoice: any;
    iDate2: any;
    totalInvoice2: any;
    iDate3: any;
    totalInvoice3: any;
    iDate4: any;
    totalInvoice4: any;
    iDate5: any;
    totalInvoice5: any;
    iDate6: any;
    totalInvoice6: any;
    iDate7: any;
    totalInvoice7: any;
    iDate8: any;
    totalInvoice8: any;
    iDate9: any;
    totalInvoice9: any;
    iDate10: any;
    totalInvoice10: any;
    iDate11: any;
    totalInvoice11: any;
    iDate12: any;
    totalInvoice12: any;
    medicinesales: any;
    medicinesales2: any;
    medicinesales3: any;
    medicinesales4: any;
    medicinesales5: any;
    medicinesales6: any;
    medicinesales7: any;
    medicinesales8: any;
    medicinesales9: any;
    medicinesales10: any;
    medicinesales11: any;
    medicinesales12: any;
    mDate: any;
    totalMedicine: any;
    mDate2: any;
    totalMedicine2: any;
    mDate3: any;
    totalMedicine3: any;
    mDate4: any;
    totalMedicine4: any;
    mDate5: any;
    totalMedicine5: any;
    mDate6: any;
    totalMedicine6: any;
    mDate7: any;
    totalMedicine7: any;
    mDate8: any;
    totalMedicine8: any;
    mDate9: any;
    totalMedicine9: any;
    mDate10: any;
    totalMedicine10: any;
    mDate11: any;
    totalMedicine11: any;
    mDate12: any;
    totalMedicine12: any;
    selectedYear: any;
    years: any;
    yearss= [];
    yearNow: any;
    yearArray: any;
    demo = false;
    widgets: any;
    widgets2: any;
    mywidget5: any = {};
    widget6: any = {};
    widget7: any = {};
    widget8: any = {};
    widget9: any = {};
    widget11: any = {};
    expiredate = null;
    error = "";
    isextend = false;

    dateNow = Date.now();

    constructor(private projectsDashboardService: ProjectsDashboardService, private db: PouchService,
        private animationBuilder: AnimationBuilder,
        @Inject(DOCUMENT) private document: any,
        private router: Router) {

        this.projectsDashboardService.februaryIncome

        this.yearArray = [];
        this.yearNow = new Date().getFullYear();
        
        for (var i = 1980; i <= this.yearNow; i++) {

            this.years = i
            this.yearArray.push(this.years);
            
            /*  if (i = 1980) {
                 break;
             } */

        }

        this.localStorageItem = JSON.parse(localStorage.getItem('user'));
        this.localStorageItem = this.localStorageItem.name

        this.db.getDStaffs().then(res => {
            this.doctorNumber = res.length;
        })

        this.db.getPStaffs().then(res => {
            this.patientNumber = res.length

        })

        this.db.getNStaffs().then(res => {
            this.nurseNumber = res.length;

        })

        this.db.getPHStaffs().then(res => {
            this.pharmacistNumber = res.length;

        })

        this.db.getLStaffs().then(res => {
            this.labNumber = res.length

        })

        this.db.getAStaffs().then(res => {
            this.accountantNumber = res.length;

        })

        this.db.getInvoices().then(res => {
            this.invoiceNumber = res.length;

        })
        this.db.getBlood_sales().then(res => {
            this.bloodSaleNumber = res
        })

        this.db.getMedicines().then(res => {
            this.medicineNumber = res.length;

        })

        this.db.getReports().then(res => {
            this.operationNumber = res;
            this.operationNumber = this.operationNumber.filter(data => data.type === 'Operation')
            this.operationNumber = this.operationNumber.length;

            this.birthNumber = res;
            this.birthNumber = this.birthNumber.filter(data => data.type === 'Birth');
            this.birthNumber = this.birthNumber.length;
            

            this.lossNumber = res;
            this.lossNumber = this.lossNumber.filter(data => data.type == 'Death');
            this.lossNumber = this.lossNumber.length;
        })

        this.db.getSettings().then(res => {
            this.settingNumber = res.length;

        })

        this.projects = this.projectsDashboardService.projects;

        this.selectedProject = this.projects[0];

        this.widgets = this.projectsDashboardService.widgets;

        this.projectsDashboardService.getWidgets2().then(res => {
            this.widgets2 = res;
            
            this.mywidget5 = {
                currentRange: 'TW',
                xAxis: true,
                yAxis: true,
                gradient: false,
                legend: true,
                showXAxisLabel: false,
                xAxisLabel: 'Days',
                showYAxisLabel: false,
                yAxisLabel: 'Isues',
                scheme: {
                    domain: ['#2eb82e', '#85e085', '#C7B42C', '#AAAAAA']
                },
                onSelect: (ev) => {
                    
                },
                supporting: {
                    currentRange: '',
                    xAxis: false,
                    yAxis: false,
                    gradient: false,
                    legend: false,
                    showXAxisLabel: false,
                    xAxisLabel: 'Days',
                    showYAxisLabel: false,
                    yAxisLabel: 'Isues',
                    scheme: {
                        domain: ['#2eb82e', '#85e085', '#C7B42C', '#AAAAAA']
                    },
                    curve: shape.curveBasis
                }
            };
          
        })


        /**
         * Widget 5
         */


        /**
         * Widget 6
         */
        this.widget6 = {
            currentRange: 'TW',
            legend: false,
            explodeSlices: false,
            labels: true,
            doughnut: true,
            gradient: false,
            scheme: {
                domain: ['#2eb82e', '#85e085', '#03a9f4', '#e91e63']
            },
            onSelect: (ev) => {
                
            }
        };

        /**
         * Widget 7
         */
        this.widget7 = {
            currentRange: 'T'
        };

        /**
         * Widget 8
         */
        this.widget8 = {
            legend: false,
            explodeSlices: false,
            labels: true,
            doughnut: false,
            gradient: false,
            scheme: {
                domain: ['#2eb82e', '#85e085', '#03a9f4', '#e91e63', '#ffc107']
            },
            onSelect: (ev) => {
                
            }
        };

        /**
         * Widget 9
         */
        this.widget9 = {
            currentRange: 'TW',
            xAxis: false,
            yAxis: false,
            gradient: false,
            legend: false,
            showXAxisLabel: false,
            xAxisLabel: 'Days',
            showYAxisLabel: false,
            yAxisLabel: 'Isues',
            scheme: {
                domain: ['#2eb82e', '#85e085', '#C7B42C', '#AAAAAA']
            },
            curve: shape.curveBasis
        };

        setInterval(() => {
            this.dateNow = Date.now();
        }, 1000);

        /*   this.splashScreenEl = this.document.body.querySelector('#fuse-splash-screen');

        const hideOnLoad = this.router.events.subscribe((event) => {
            if (event instanceof NavigationEnd) {
                setTimeout(() => {
                    this.hide();
                    hideOnLoad.unsubscribe();
                }, 0);
            }
        }
        ); */


    }

    /*   show()
   {
       this.player =
           this.animationBuilder
               .build([
                   style({
                       opacity: '0',
                       zIndex : '99999'
                   }),
                   animate('400ms ease', style({opacity: '1'}))
               ]).create(this.splashScreenEl);

       setTimeout(() => {
           this.player.play();
       }, 0);
   }

      hide()
   {
       this.player =
           this.animationBuilder
               .build([
                   style({opacity: '1'}),
                   animate('400ms ease', style({
                       opacity: '0',
                       zIndex : '-10'
                   }))
               ]).create(this.splashScreenEl);

       setTimeout(() => {
           this.player.play();
       }, 0);
   } 
*/

    getArray(gets) {

        var mine = Number([gets.amount].join())
        return mine
    }

    getPayrollArray(gets) {

        var mine = Number([gets.joining_salary].join())
        return mine
    }

    getInvoiceArray(gets) {

        var mine = Number([gets.grandtotal].join())
        return mine
    }

    getBlood_SaleArray(gets) {

        var mine = Number([gets.total].join())
        return mine
    }

    getSaleArray(gets) {

        var mine = Number([gets.total_amount].join())
        return mine
    }

    getSum(total, num) {
        return total + num;
    }

    getYear(year) {

        this.widgets2 = this.projectsDashboardService.widgets2;

        this.selectedYear = year

        this.db.getExpenses().then(res => {
            this.expenses = res;
            this.expenses2 = res;
            this.expenses3 = res;
            this.expenses4 = res;
            this.expenses5 = res;
            this.expenses6 = res;
            this.expenses7 = res;
            this.expenses8 = res;
            this.expenses9 = res;
            this.expenses10 = res;
            this.expenses11 = res;
            this.expenses12 = res;

            this.years = res
            var currentYear = this.selectedYear.toString();
            var currentMonth = '01';

            this.expenses = this.expenses.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
            
            if (this.expenses.length != 0) {
                
                this.eDate = this.expenses.map(this.getArray);
                
            }
            else {
                this.eDate = [0, 0]
            }
            this.totalExpense = this.eDate.reduce(this.getSum)

            
            var currentYear = this.selectedYear.toString();
            var currentMonth = '02';
            this.expenses2 = this.expenses2.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
            if (this.expenses2.length != 0) {
                
                this.eDate2 = this.expenses2.map(this.getArray);
                
            }
            else {
                this.eDate2 = [0, 0]
            }
            this.totalExpense2 = this.eDate2.reduce(this.getSum)

            var currentYear = this.selectedYear.toString();
            var currentMonth = '03';
            this.expenses3 = this.expenses3.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
            if (this.expenses3.length != 0) {
                
                this.eDate3 = this.expenses3.map(this.getArray);
            }
            else {
                this.eDate3 = [0, 0]
            }
            this.totalExpense3 = this.eDate3.reduce(this.getSum)

            var currentYear = this.selectedYear.toString();
            var currentMonth = '04';
            this.expenses4 = this.expenses4.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
            if (this.expenses4.length != 0) {
                
                this.eDate4 = this.expenses4.map(this.getArray);
            }
            else {
                this.eDate4 = [0, 0]
            }
            this.totalExpense4 = this.eDate4.reduce(this.getSum)

            var currentYear = this.selectedYear.toString();
            var currentMonth = '05';
            this.expenses5 = this.expenses5.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
            if (this.expenses5.length != 0) {
                
                this.eDate5 = this.expenses5.map(this.getArray);
            }
            else {
                this.eDate5 = [0, 0]
            }
            this.totalExpense5 = this.eDate5.reduce(this.getSum)

            var currentYear = this.selectedYear.toString();
            var currentMonth = '06';
            this.expenses6 = this.expenses6.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
            if (this.expenses6.length != 0) {
                
                this.eDate6 = this.expenses6.map(this.getArray);
            }
            else {
                this.eDate6 = [0, 0]
            }
            this.totalExpense6 = this.eDate6.reduce(this.getSum)

            var currentYear = this.selectedYear.toString();
            var currentMonth = '07';
            this.expenses7 = this.expenses7.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
            if (this.expenses7.length != 0) {
                
                this.eDate7 = this.expenses7.map(this.getArray);
            }
            else {
                this.eDate7 = [0, 0]
            }
            this.totalExpense7 = this.eDate7.reduce(this.getSum)

            var currentYear = this.selectedYear.toString();
            var currentMonth = '08';
            this.expenses8 = this.expenses8.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
            if (this.expenses8.length != 0) {
                
                this.eDate8 = this.expenses8.map(this.getArray);
            }
            else {
                this.eDate8 = [0, 0]
            }
            this.totalExpense8 = this.eDate8.reduce(this.getSum)

            var currentYear = this.selectedYear.toString();
            var currentMonth = '09';
            this.expenses9 = this.expenses9.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
            if (this.expenses9.length != 0) {
                
                this.eDate9 = this.expenses9.map(this.getArray);
            }
            else {
                this.eDate9 = [0, 0]
            }
            this.totalExpense9 = this.eDate9.reduce(this.getSum)

            var currentYear = this.selectedYear.toString();
            var currentMonth = '10';
            this.expenses10 = this.expenses10.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
            if (this.expenses10.length != 0) {
                
                this.eDate10 = this.expenses10.map(this.getArray);
            }
            else {
                this.eDate10 = [0, 0]
            }
            this.totalExpense10 = this.eDate10.reduce(this.getSum)

            var currentYear = this.selectedYear.toString();
            var currentMonth = '11';
            this.expenses11 = this.expenses11.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
            if (this.expenses11.length != 0) {
                
                this.eDate11 = this.expenses11.map(this.getArray);
            }
            else {
                this.eDate11 = [0, 0]
            }
            this.totalExpense11 = this.eDate11.reduce(this.getSum)

            var currentYear = this.selectedYear.toString();
            var currentMonth = '12';
            this.expenses12 = this.expenses12.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
            if (this.expenses12.length != 0) {
                
                this.eDate12 = this.expenses12.map(this.getArray);
            }
            else {
                this.eDate12 = [0, 0]
            }
            this.totalExpense12 = this.eDate12.reduce(this.getSum)


        }).then(res => {

            this.db.getPayrolls().then(res => {
                this.payrolls = res;
                this.payrolls2 = res;
                this.payrolls3 = res;
                this.payrolls4 = res;
                this.payrolls5 = res;
                this.payrolls6 = res;
                this.payrolls7 = res;
                this.payrolls8 = res;
                this.payrolls9 = res;
                this.payrolls10 = res;
                this.payrolls11 = res;
                this.payrolls12 = res;

                this.years = res
                var currentYear = this.selectedYear.toString();
                var currentMonth = '01';

                this.payrolls = this.payrolls.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                
                if (this.payrolls.length != 0) {
                    
                    this.pDate = this.payrolls.map(this.getPayrollArray);
                    
                }
                else {
                    this.pDate = [0, 0]
                }
                this.totalPayroll = this.pDate.reduce(this.getSum)

                
                var currentYear = this.selectedYear.toString();
                var currentMonth = '02';
                this.payrolls2 = this.payrolls2.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                if (this.payrolls2.length != 0) {
                    
                    this.pDate2 = this.payrolls2.map(this.getPayrollArray);
                    
                }
                else {
                    this.pDate2 = [0, 0]
                }
                this.totalPayroll2 = this.pDate2.reduce(this.getSum)

                var currentYear = this.selectedYear.toString();
                var currentMonth = '03';
                this.payrolls3 = this.payrolls3.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                if (this.payrolls3.length != 0) {
                    
                    this.pDate3 = this.payrolls3.map(this.getPayrollArray);
                }
                else {
                    this.pDate3 = [0, 0]
                }
                this.totalPayroll3 = this.pDate3.reduce(this.getSum)

                var currentYear = this.selectedYear.toString();
                var currentMonth = '04';
                this.payrolls4 = this.payrolls4.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                if (this.payrolls4.length != 0) {
                    
                    this.pDate4 = this.payrolls4.map(this.getPayrollArray);
                }
                else {
                    this.pDate4 = [0, 0]
                }
                this.totalPayroll4 = this.pDate4.reduce(this.getSum)

                var currentYear = this.selectedYear.toString();
                var currentMonth = '05';
                this.payrolls5 = this.payrolls5.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                if (this.payrolls5.length != 0) {
                    
                    this.pDate5 = this.payrolls5.map(this.getPayrollArray);
                }
                else {
                    this.pDate5 = [0, 0]
                }
                this.totalPayroll5 = this.pDate5.reduce(this.getSum)

                var currentYear = this.selectedYear.toString();
                var currentMonth = '06';
                this.payrolls6 = this.payrolls6.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                if (this.payrolls6.length != 0) {
                    
                    this.pDate6 = this.payrolls6.map(this.getPayrollArray);
                }
                else {
                    this.pDate6 = [0, 0]
                }
                this.totalPayroll6 = this.pDate6.reduce(this.getSum)

                var currentYear = this.selectedYear.toString();
                var currentMonth = '07';
                this.payrolls7 = this.payrolls7.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                if (this.payrolls7.length != 0) {
                    
                    this.pDate7 = this.payrolls7.map(this.getPayrollArray);
                }
                else {
                    this.pDate7 = [0, 0]
                }
                this.totalPayroll7 = this.pDate7.reduce(this.getSum)

                var currentYear = this.selectedYear.toString();
                var currentMonth = '08';
                this.payrolls8 = this.payrolls8.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                if (this.payrolls8.length != 0) {
                    
                    this.pDate8 = this.payrolls8.map(this.getPayrollArray);
                }
                else {
                    this.pDate8 = [0, 0]
                }
                this.totalPayroll8 = this.pDate8.reduce(this.getSum)

                var currentYear = this.selectedYear.toString();
                var currentMonth = '09';
                this.payrolls9 = this.payrolls9.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                if (this.payrolls9.length != 0) {
                    
                    this.pDate9 = this.payrolls9.map(this.getPayrollArray);
                }
                else {
                    this.pDate9 = [0, 0]
                }
                this.totalPayroll9 = this.pDate9.reduce(this.getSum)

                var currentYear = this.selectedYear.toString();
                var currentMonth = '10';
                this.payrolls10 = this.payrolls10.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                if (this.payrolls10.length != 0) {
                    
                    this.pDate10 = this.payrolls10.map(this.getPayrollArray);
                }
                else {
                    this.pDate10 = [0, 0]
                }
                this.totalPayroll10 = this.pDate10.reduce(this.getSum)

                var currentYear = this.selectedYear.toString();
                var currentMonth = '11';
                this.payrolls11 = this.payrolls11.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                if (this.payrolls11.length != 0) {
                    
                    this.pDate11 = this.payrolls11.map(this.getPayrollArray);
                }
                else {
                    this.pDate11 = [0, 0]
                }
                this.totalPayroll11 = this.pDate11.reduce(this.getSum)

                var currentYear = this.selectedYear.toString();
                var currentMonth = '12';
                this.payrolls12 = this.payrolls12.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                if (this.payrolls12.length != 0) {
                    
                    this.pDate12 = this.payrolls12.map(this.getPayrollArray);
                }
                else {
                    this.pDate12 = [0, 0]
                }
                this.totalPayroll12 = this.pDate12.reduce(this.getSum)

            }).then(res => {

                this.db.getInvoices().then(res => {
                    this.invoices = res;
                    this.invoices2 = res;
                    this.invoices3 = res;
                    this.invoices4 = res;
                    this.invoices5 = res;
                    this.invoices6 = res;
                    this.invoices7 = res;
                    this.invoices8 = res;
                    this.invoices9 = res;
                    this.invoices10 = res;
                    this.invoices11 = res;
                    this.invoices12 = res;
                    
                    this.years = res
                    var currentYear = this.selectedYear.toString();
                    var currentMonth = '01';

                    this.invoices = this.invoices.filter(data => data.creation_timestamp.substring(0, 4) === currentYear && data.creation_timestamp.substring(5, 7) === currentMonth)
                    
                    if (this.invoices.length != 0) {
                        
                        this.iDate = this.invoices.map(this.getInvoiceArray);
                        
                    }
                    else {
                        this.iDate = [0, 0]
                    }
                    this.totalInvoice = this.iDate.reduce(this.getSum)
                    //
                    
                    var currentYear = this.selectedYear.toString();
                    var currentMonth = '02';
                    this.invoices2 = this.invoices2.filter(data => data.creation_timestamp.substring(0, 4) === currentYear && data.creation_timestamp.substring(5, 7) === currentMonth)
                    if (this.invoices2.length != 0) {
                        
                        this.iDate2 = this.invoices2.map(this.getInvoiceArray);
                        
                    }
                    else {
                        this.iDate2 = [0, 0]
                    }
                    this.totalInvoice2 = this.iDate2.reduce(this.getSum)
                    //
                    var currentYear = this.selectedYear.toString();
                    var currentMonth = '03';
                    this.invoices3 = this.invoices3.filter(data => data.creation_timestamp.substring(0, 4) === currentYear && data.creation_timestamp.substring(5, 7) === currentMonth)
                    if (this.invoices3.length != 0) {
                        
                        this.iDate3 = this.invoices3.map(this.getInvoiceArray);
                    }
                    else {
                        this.iDate3 = [0, 0]
                    }
                    this.totalInvoice3 = this.iDate3.reduce(this.getSum)

                    var currentYear = this.selectedYear.toString();
                    var currentMonth = '04';
                    this.invoices4 = this.invoices4.filter(data => data.creation_timestamp.substring(0, 4) === currentYear && data.creation_timestamp.substring(5, 7) === currentMonth)
                    if (this.invoices4.length != 0) {
                        
                        this.iDate4 = this.invoices4.map(this.getInvoiceArray);
                    }
                    else {
                        this.iDate4 = [0, 0]
                    }
                    this.totalInvoice4 = this.iDate4.reduce(this.getSum)
                    //
                    var currentYear = this.selectedYear.toString();
                    var currentMonth = '05';
                    this.invoices5 = this.invoices5.filter(data => data.creation_timestamp.substring(0, 4) === currentYear && data.creation_timestamp.substring(5, 7) === currentMonth)
                    if (this.invoices5.length != 0) {
                        
                        this.iDate5 = this.invoices5.map(this.getInvoiceArray);
                    }
                    else {
                        this.iDate5 = [0, 0]
                    }
                    this.totalInvoice5 = this.iDate5.reduce(this.getSum)

                    var currentYear = this.selectedYear.toString();
                    var currentMonth = '06';
                    this.invoices6 = this.invoices6.filter(data => data.creation_timestamp.substring(0, 4) === currentYear && data.creation_timestamp.substring(5, 7) === currentMonth)
                    if (this.invoices6.length != 0) {
                        
                        this.iDate6 = this.invoices6.map(this.getInvoiceArray);
                    }
                    else {
                        this.iDate6 = [0, 0]
                    }
                    this.totalInvoice6 = this.iDate6.reduce(this.getSum)

                    var currentYear = this.selectedYear.toString();
                    var currentMonth = '07';
                    this.invoices7 = this.invoices7.filter(data => data.creation_timestamp.substring(0, 4) === currentYear && data.creation_timestamp.substring(5, 7) === currentMonth)
                    if (this.invoices7.length != 0) {
                        
                        this.iDate7 = this.invoices7.map(this.getInvoiceArray);
                    }
                    else {
                        this.iDate7 = [0, 0]
                    }
                    this.totalInvoice7 = this.iDate7.reduce(this.getSum)

                    var currentYear = this.selectedYear.toString();
                    var currentMonth = '08';
                    this.invoices8 = this.invoices8.filter(data => data.creation_timestamp.substring(0, 4) === currentYear && data.creation_timestamp.substring(5, 7) === currentMonth)
                    if (this.invoices8.length != 0) {
                        
                        this.iDate8 = this.invoices8.map(this.getInvoiceArray);
                    }
                    else {
                        this.iDate8 = [0, 0]
                    }
                    this.totalInvoice8 = this.iDate8.reduce(this.getSum)
                    //
                    var currentYear = this.selectedYear.toString();
                    var currentMonth = '09';
                    this.invoices9 = this.invoices9.filter(data => data.creation_timestamp.substring(0, 4) === currentYear && data.creation_timestamp.substring(5, 7) === currentMonth)
                    if (this.invoices9.length != 0) {
                        
                        this.iDate9 = this.invoices9.map(this.getInvoiceArray);
                    }
                    else {
                        this.iDate9 = [0, 0]
                    }
                    this.totalInvoice9 = this.iDate9.reduce(this.getSum)

                    var currentYear = this.selectedYear.toString();
                    var currentMonth = '10';
                    this.invoices10 = this.invoices10.filter(data => data.creation_timestamp.substring(0, 4) === currentYear && data.creation_timestamp.substring(5, 7) === currentMonth)
                    if (this.invoices10.length != 0) {
                        
                        this.iDate10 = this.invoices10.map(this.getInvoiceArray);
                    }
                    else {
                        this.iDate10 = [0, 0]
                    }
                    this.totalInvoice10 = this.iDate10.reduce(this.getSum)

                    var currentYear = this.selectedYear.toString();
                    var currentMonth = '11';
                    this.invoices11 = this.invoices11.filter(data => data.creation_timestamp.substring(0, 4) === currentYear && data.creation_timestamp.substring(5, 7) === currentMonth)
                    if (this.invoices11.length != 0) {
                        
                        this.iDate11 = this.invoices11.map(this.getInvoiceArray);
                    }
                    else {
                        this.iDate11 = [0, 0]
                    }
                    this.totalInvoice11 = this.iDate11.reduce(this.getSum)

                    var currentYear = this.selectedYear.toString();
                    var currentMonth = '12';
                    this.invoices12 = this.invoices12.filter(data => data.creation_timestamp.substring(0, 4) === currentYear && data.creation_timestamp.substring(5, 7) === currentMonth)
                    if (this.invoices12.length != 0) {
                        
                        this.iDate12 = this.invoices12.map(this.getInvoiceArray);
                    }
                    else {
                        this.iDate12 = [0, 0]
                    }
                    this.totalInvoice12 = this.iDate12.reduce(this.getSum)
                    //END
                }).then(res => {
                    this.db.getBlood_sales().then(res => {
                        this.bloodSales = res;
                        this.bloodSales2 = res;
                        this.bloodSales3 = res;
                        this.bloodSales4 = res;
                        this.bloodSales5 = res;
                        this.bloodSales6 = res;
                        this.bloodSales7 = res;
                        this.bloodSales8 = res;
                        this.bloodSales9 = res;
                        this.bloodSales10 = res;
                        this.bloodSales11 = res;
                        this.bloodSales12 = res;

                       
                        this.years = res
                        var currentYear = this.selectedYear.toString();
                        var currentMonth = '01';

                        this.bloodSales = this.bloodSales.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                        
                        if (this.bloodSales.length != 0) {
                            
                            this.bsDate = this.bloodSales.map(this.getBlood_SaleArray);
                            
                        }
                        else {
                            this.bsDate = [0, 0]
                        }
                        this.totalBloodSale = this.bsDate.reduce(this.getSum);

                        
                        var currentYear = this.selectedYear.toString();
                        var currentMonth = '02';
                        this.bloodSales2 = this.bloodSales2.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                        if (this.bloodSales2.length != 0) {
                            
                            this.bsDate2 = this.bloodSales2.map(this.getBlood_SaleArray);
                            
                        }
                        else {
                            this.bsDate2 = [0, 0]
                        }
                        this.totalBloodSale2 = this.bsDate2.reduce(this.getSum)

                        var currentYear = this.selectedYear.toString();
                        var currentMonth = '03';
                        this.bloodSales3 = this.bloodSales3.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                        if (this.bloodSales3.length != 0) {
                            
                            this.bsDate3 = this.bloodSales3.map(this.getBlood_SaleArray);
                        }
                        else {
                            this.bsDate3 = [0, 0]
                        }
                        this.totalBloodSale3 = this.bsDate3.reduce(this.getSum)

                        var currentYear = this.selectedYear.toString();
                        var currentMonth = '04';
                        this.bloodSales4 = this.bloodSales4.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                        if (this.bloodSales4.length != 0) {
                            
                            this.bsDate4 = this.bloodSales4.map(this.getBlood_SaleArray);
                        }
                        else {
                            this.bsDate4 = [0, 0]
                        }
                        this.totalBloodSale4 = this.bsDate4.reduce(this.getSum)

                        var currentYear = this.selectedYear.toString();
                        var currentMonth = '05';
                        this.bloodSales5 = this.bloodSales5.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                        if (this.bloodSales5.length != 0) {
                            
                            this.bsDate5 = this.bloodSales5.map(this.getBlood_SaleArray);
                        }
                        else {
                            this.bsDate5 = [0, 0]
                        }
                        this.totalBloodSale5 = this.bsDate5.reduce(this.getSum)

                        var currentYear = this.selectedYear.toString();
                        var currentMonth = '06';
                        this.bloodSales6 = this.bloodSales6.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                        if (this.bloodSales6.length != 0) {
                            
                            this.bsDate6 = this.bloodSales6.map(this.getBlood_SaleArray);
                        }
                        else {
                            this.bsDate6 = [0, 0]
                        }
                        this.totalBloodSale6 = this.bsDate6.reduce(this.getSum)

                        var currentYear = this.selectedYear.toString();
                        var currentMonth = '07';
                        this.bloodSales7 = this.bloodSales7.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                        if (this.bloodSales7.length != 0) {
                            
                            this.bsDate7 = this.bloodSales7.map(this.getBlood_SaleArray);
                        }
                        else {
                            this.bsDate7 = [0, 0]
                        }
                        this.totalBloodSale7 = this.bsDate7.reduce(this.getSum)

                        var currentYear = this.selectedYear.toString();
                        var currentMonth = '08';
                        this.bloodSales8 = this.bloodSales8.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                        if (this.bloodSales8.length != 0) {
                            
                            this.bsDate8 = this.bloodSales8.map(this.getBlood_SaleArray);
                        }
                        else {
                            this.bsDate8 = [0, 0]
                        }
                        this.totalBloodSale8 = this.bsDate8.reduce(this.getSum)

                        var currentYear = this.selectedYear.toString();
                        var currentMonth = '09';
                        this.bloodSales9 = this.bloodSales9.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                        if (this.bloodSales9.length != 0) {
                            
                            this.bsDate9 = this.bloodSales9.map(this.getBlood_SaleArray);
                        }
                        else {
                            this.bsDate9 = [0, 0]
                        }
                        this.totalBloodSale9 = this.bsDate9.reduce(this.getSum)

                        var currentYear = this.selectedYear.toString();
                        var currentMonth = '10';
                        this.bloodSales10 = this.bloodSales10.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                        if (this.bloodSales10.length != 0) {
                            
                            this.bsDate10 = this.bloodSales10.map(this.getBlood_SaleArray);
                        }
                        else {
                            this.bsDate10 = [0, 0]
                        }
                        this.totalBloodSale10 = this.bsDate10.reduce(this.getSum)

                        var currentYear = this.selectedYear.toString();
                        var currentMonth = '11';
                        this.bloodSales11 = this.bloodSales11.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                        if (this.bloodSales11.length != 0) {
                            
                            this.bsDate11 = this.bloodSales11.map(this.getBlood_SaleArray);
                        }
                        else {
                            this.bsDate11 = [0, 0]
                        }
                        this.totalBloodSale11 = this.bsDate11.reduce(this.getSum)

                        var currentYear = this.selectedYear.toString();
                        var currentMonth = '12';
                        this.bloodSales12 = this.bloodSales12.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                        if (this.bloodSales12.length != 0) {
                            
                            this.bsDate12 = this.bloodSales12.map(this.getBlood_SaleArray);
                        }
                        else {
                            this.bsDate12 = [0, 0]
                        }
                        this.totalBloodSale12 = this.bsDate12.reduce(this.getSum)


                    }).then(res => {
                        this.db.getMedicine_sales().then(res => {
                            this.medicinesales = res;
                            this.medicinesales2 = res;
                            this.medicinesales3 = res;
                            this.medicinesales4 = res;
                            this.medicinesales5 = res;
                            this.medicinesales6 = res;
                            this.medicinesales7 = res;
                            this.medicinesales8 = res;
                            this.medicinesales9 = res;
                            this.medicinesales10 = res;
                            this.medicinesales11 = res;
                            this.medicinesales12 = res;

                            this.years = res
                            var currentYear = this.selectedYear.toString();
                            var currentMonth = '01';

                            this.medicinesales = this.medicinesales.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                            
                            if (this.medicinesales.length != 0) {
                                
                                this.mDate = this.medicinesales.map(this.getSaleArray);
                                
                            }
                            else {
                                this.mDate = [0, 0]
                            }
                            this.totalMedicine = this.mDate.reduce(this.getSum)

                            
                            var currentYear = this.selectedYear.toString();
                            var currentMonth = '02';
                            this.medicinesales2 = this.medicinesales2.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                            if (this.medicinesales2.length != 0) {
                                
                                this.mDate2 = this.medicinesales2.map(this.getSaleArray);
                                
                            }
                            else {
                                this.mDate2 = [0, 0]
                            }
                            this.totalMedicine2 = this.mDate2.reduce(this.getSum)

                            var currentYear = this.selectedYear.toString();
                            var currentMonth = '03';
                            this.medicinesales3 = this.medicinesales3.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                            if (this.medicinesales3.length != 0) {
                                
                                this.mDate3 = this.medicinesales3.map(this.getSaleArray);
                            }
                            else {
                                this.mDate3 = [0, 0]
                            }
                            this.totalMedicine3 = this.mDate3.reduce(this.getSum)

                            var currentYear = this.selectedYear.toString();
                            var currentMonth = '04';
                            this.medicinesales4 = this.medicinesales4.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                            if (this.medicinesales4.length != 0) {
                                
                                this.mDate4 = this.medicinesales4.map(this.getSaleArray);
                            }
                            else {
                                this.mDate4 = [0, 0]
                            }
                            this.totalMedicine4 = this.mDate4.reduce(this.getSum)

                            var currentYear = this.selectedYear.toString();
                            var currentMonth = '05';
                            this.medicinesales5 = this.medicinesales5.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                            if (this.medicinesales5.length != 0) {
                                
                                this.mDate5 = this.medicinesales5.map(this.getSaleArray);
                            }
                            else {
                                this.mDate5 = [0, 0]
                            }
                            this.totalMedicine5 = this.mDate5.reduce(this.getSum)

                            var currentYear = this.selectedYear.toString();
                            var currentMonth = '06';
                            this.medicinesales6 = this.medicinesales6.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                            if (this.medicinesales6.length != 0) {
                                
                                this.mDate6 = this.medicinesales6.map(this.getSaleArray);
                            }
                            else {
                                this.mDate6 = [0, 0]
                            }
                            this.totalMedicine6 = this.mDate6.reduce(this.getSum)

                            var currentYear = this.selectedYear.toString();
                            var currentMonth = '07';
                            this.medicinesales7 = this.medicinesales7.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                            if (this.medicinesales7.length != 0) {
                                
                                this.mDate7 = this.medicinesales7.map(this.getSaleArray);
                            }
                            else {
                                this.mDate7 = [0, 0]
                            }
                            this.totalMedicine7 = this.mDate7.reduce(this.getSum)

                            var currentYear = this.selectedYear.toString();
                            var currentMonth = '08';
                            this.medicinesales8 = this.medicinesales8.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                            if (this.medicinesales8.length != 0) {
                                
                                this.mDate8 = this.medicinesales8.map(this.getSaleArray);
                            }
                            else {
                                this.mDate8 = [0, 0]
                            }
                            this.totalMedicine8 = this.mDate8.reduce(this.getSum)

                            var currentYear = this.selectedYear.toString();
                            var currentMonth = '09';
                            this.medicinesales9 = this.medicinesales9.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                            if (this.medicinesales9.length != 0) {
                                
                                this.mDate9 = this.medicinesales9.map(this.getSaleArray);
                            }
                            else {
                                this.mDate9 = [0, 0]
                            }
                            this.totalMedicine9 = this.mDate9.reduce(this.getSum)

                            var currentYear = this.selectedYear.toString();
                            var currentMonth = '10';
                            this.medicinesales10 = this.medicinesales10.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                            if (this.medicinesales10.length != 0) {
                                
                                this.mDate10 = this.medicinesales10.map(this.getSaleArray);
                            }
                            else {
                                this.mDate10 = [0, 0]
                            }
                            this.totalMedicine10 = this.mDate10.reduce(this.getSum)

                            var currentYear = this.selectedYear.toString();
                            var currentMonth = '11';
                            this.medicinesales11 = this.medicinesales11.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                            if (this.medicinesales11.length != 0) {
                                
                                this.mDate11 = this.medicinesales11.map(this.getSaleArray);
                            }
                            else {
                                this.mDate11 = [0, 0]
                            }
                            this.totalMedicine11 = this.mDate11.reduce(this.getSum)

                            var currentYear = this.selectedYear.toString();
                            var currentMonth = '12';
                            this.medicinesales12 = this.medicinesales12.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                            if (this.medicinesales12.length != 0) {
                                
                                this.mDate12 = this.medicinesales12.map(this.getSaleArray);
                            }
                            else {
                                this.mDate12 = [0, 0]
                            }
                            this.totalMedicine12 = this.mDate12.reduce(this.getSum)
                        }).then(res => {


                            
                            this.projectsDashboardService.getWidgets2().then(res => {
                                this.widgets2 = res
                                

                                this.mywidget5 = {
                                    currentRange: 'TW',
                                    xAxis: true,
                                    yAxis: true,
                                    gradient: false,
                                    legend: true,
                                    showXAxisLabel: false,
                                    xAxisLabel: 'Days',
                                    showYAxisLabel: false,
                                    yAxisLabel: 'Isues',
                                    scheme: {
                                        domain: ['#2eb82e', '#85e085', '#C7B42C', '#AAAAAA']
                                    },
                                    onSelect: (ev) => {
                                        
                                    },
                                    supporting: {
                                        currentRange: '',
                                        xAxis: false,
                                        yAxis: false,
                                        gradient: false,
                                        legend: false,
                                        showXAxisLabel: false,
                                        xAxisLabel: 'Days',
                                        showYAxisLabel: false,
                                        yAxisLabel: 'Isues',
                                        scheme: {
                                            domain: ['#2eb82e', '#85e085', '#C7B42C', '#AAAAAA']
                                        },
                                        curve: shape.curveBasis
                                    }
                                };
                                //Incomes
                                this.projectsDashboardService.januaryIncome = this.totalInvoice + this.totalMedicine + this.totalBloodSale;
                                this.projectsDashboardService.februaryIncome = this.totalInvoice2 + this.totalMedicine2 + this.totalBloodSale2;
                                this.projectsDashboardService.marchIncome = this.totalInvoice3 + this.totalMedicine3 + this.totalBloodSale3;
                                this.projectsDashboardService.aprilIncome = this.totalInvoice4 + this.totalMedicine4 + this.totalBloodSale4;
                                this.projectsDashboardService.mayIncome = this.totalInvoice5 + this.totalMedicine5 + this.totalBloodSale5;
                                this.projectsDashboardService.juneIncome = this.totalInvoice6 + this.totalMedicine6 + this.totalBloodSale6;
                                this.projectsDashboardService.julyIncome = this.totalInvoice7 + this.totalMedicine7 + this.totalBloodSale7;
                                this.projectsDashboardService.augustIncome = this.totalInvoice8 + this.totalMedicine8 + this.totalBloodSale8;
                                this.projectsDashboardService.septemberIncome = this.totalInvoice9 + this.totalMedicine9 + this.totalBloodSale9;
                                this.projectsDashboardService.octoberIncome = this.totalInvoice10 + this.totalMedicine10 + this.totalBloodSale10;
                                this.projectsDashboardService.novemberIncome = this.totalInvoice11 + this.totalMedicine11 + this.totalBloodSale11;
                                this.projectsDashboardService.decemberIncome = this.totalInvoice12 + this.totalMedicine12 + this.totalBloodSale12;

                                //Expenses
                                this.projectsDashboardService.januaryExpense = this.totalExpense + this.totalPayroll;
                                this.projectsDashboardService.februaryExpense = this.totalExpense2 + this.totalPayroll2;
                                this.projectsDashboardService.marchExpense = this.totalExpense3 + this.totalPayroll3;
                                this.projectsDashboardService.aprilExpense = this.totalExpense4 + this.totalPayroll4;
                                this.projectsDashboardService.mayExpense = this.totalExpense5 + this.totalPayroll5;
                                this.projectsDashboardService.juneExpense = this.totalExpense6 + this.totalPayroll6;
                                this.projectsDashboardService.julyExpense = this.totalExpense7 + this.totalPayroll7;
                                this.projectsDashboardService.augustExpense = this.totalExpense8 + this.totalPayroll8;
                                this.projectsDashboardService.septemberExpense = this.totalExpense9 + this.totalPayroll9;
                                this.projectsDashboardService.octoberExpense = this.totalExpense10 + this.totalPayroll10;
                                this.projectsDashboardService.novemberExpense = this.totalExpense11 + this.totalPayroll11;
                                this.projectsDashboardService.decemberExpense = this.totalExpense12 + this.totalPayroll12;

                                res.mywidget5.mainChart.TW[0].series[0].value = this.projectsDashboardService.januaryIncome;
                                res.mywidget5.mainChart.TW[0].series[1].value = this.projectsDashboardService.januaryExpense;
                                res.mywidget5.mainChart.TW[1].series[0].value = this.projectsDashboardService.februaryIncome;
                                res.mywidget5.mainChart.TW[1].series[1].value = this.projectsDashboardService.februaryExpense;
                                res.mywidget5.mainChart.TW[2].series[0].value = this.projectsDashboardService.marchIncome;
                                res.mywidget5.mainChart.TW[2].series[1].value = this.projectsDashboardService.marchExpense;
                                res.mywidget5.mainChart.TW[3].series[0].value = this.projectsDashboardService.aprilIncome;
                                res.mywidget5.mainChart.TW[3].series[1].value = this.projectsDashboardService.aprilExpense;
                                res.mywidget5.mainChart.TW[4].series[0].value = this.projectsDashboardService.mayIncome;
                                res.mywidget5.mainChart.TW[4].series[1].value = this.projectsDashboardService.mayExpense;
                                res.mywidget5.mainChart.TW[5].series[0].value = this.projectsDashboardService.juneIncome;
                                res.mywidget5.mainChart.TW[5].series[1].value = this.projectsDashboardService.juneExpense;
                                res.mywidget5.mainChart.TW[6].series[0].value = this.projectsDashboardService.julyIncome;
                                res.mywidget5.mainChart.TW[6].series[1].value = this.projectsDashboardService.julyExpense;
                                res.mywidget5.mainChart.TW[7].series[0].value = this.projectsDashboardService.augustIncome;
                                res.mywidget5.mainChart.TW[7].series[1].value = this.projectsDashboardService.augustExpense;
                                res.mywidget5.mainChart.TW[8].series[0].value = this.projectsDashboardService.septemberIncome;
                                res.mywidget5.mainChart.TW[8].series[1].value = this.projectsDashboardService.septemberExpense;
                                res.mywidget5.mainChart.TW[9].series[0].value = this.projectsDashboardService.octoberIncome;
                                res.mywidget5.mainChart.TW[9].series[1].value = this.projectsDashboardService.octoberExpense;
                                res.mywidget5.mainChart.TW[10].series[0].value = this.projectsDashboardService.novemberIncome;
                                res.mywidget5.mainChart.TW[10].series[1].value = this.projectsDashboardService.novemberExpense;
                                res.mywidget5.mainChart.TW[11].series[0].value = this.projectsDashboardService.decemberIncome;
                                res.mywidget5.mainChart.TW[11].series[1].value = this.projectsDashboardService.decemberExpense;


                            })
                        })

                    })
                })


            })
        })


    }

    ngOnInit() {

        this.db.getSettings().then(settings => {
            if (settings[0] != undefined) {
                
                this.setting = settings[0];
                this.expiredate = new Date(this.setting.expiredate).toDateString();
                if (new Date(this.setting.expiredate).getTime() < new Date().getTime()) {
                    this.error = 'License has Expired. Please Click the button below to renew license';
                    //this.ios = false;
                }
                if (new Date(this.setting.expiredate).getTime() < (new Date().getTime() + (30 * 24 * 60 * 60 * 1000))) {
                    this.isextend = true;
                }

                let start = new Date(this.setting.startdate).getFullYear();
                let end = new Date().getFullYear();
                for (var i = end; i > start - 1; i--) {
                    this.yearss.push(i);
                }
                
            }
        });

        this.selectedYear = new Date().toISOString().substring(0, 4)
        

        this.db.getExpenses().then(res => {
            this.expenses = res;
            this.expenses2 = res;
            this.expenses3 = res;
            this.expenses4 = res;
            this.expenses5 = res;
            this.expenses6 = res;
            this.expenses7 = res;
            this.expenses8 = res;
            this.expenses9 = res;
            this.expenses10 = res;
            this.expenses11 = res;
            this.expenses12 = res;

            this.years = res
            var currentYear = this.selectedYear.toString();
            var currentMonth = '01';

            this.expenses = this.expenses.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
            
            if (this.expenses.length != 0) {
                
                this.eDate = this.expenses.map(this.getArray);
                
            }
            else {
                this.eDate = [0, 0]
            }
            this.totalExpense = this.eDate.reduce(this.getSum)

            
            var currentYear = this.selectedYear.toString();
            var currentMonth = '02';
            this.expenses2 = this.expenses2.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
            if (this.expenses2.length != 0) {
                
                this.eDate2 = this.expenses2.map(this.getArray);
                
            }
            else {
                this.eDate2 = [0, 0]
            }
            this.totalExpense2 = this.eDate2.reduce(this.getSum)

            var currentYear = this.selectedYear.toString();
            var currentMonth = '03';
            this.expenses3 = this.expenses3.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
            if (this.expenses3.length != 0) {
                
                this.eDate3 = this.expenses3.map(this.getArray);
            }
            else {
                this.eDate3 = [0, 0]
            }
            this.totalExpense3 = this.eDate3.reduce(this.getSum)

            var currentYear = this.selectedYear.toString();
            var currentMonth = '04';
            this.expenses4 = this.expenses4.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
            if (this.expenses4.length != 0) {
                
                this.eDate4 = this.expenses4.map(this.getArray);
            }
            else {
                this.eDate4 = [0, 0]
            }
            this.totalExpense4 = this.eDate4.reduce(this.getSum)

            var currentYear = this.selectedYear.toString();
            var currentMonth = '05';
            this.expenses5 = this.expenses5.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
            if (this.expenses5.length != 0) {
                
                this.eDate5 = this.expenses5.map(this.getArray);
            }
            else {
                this.eDate5 = [0, 0]
            }
            this.totalExpense5 = this.eDate5.reduce(this.getSum)

            var currentYear = this.selectedYear.toString();
            var currentMonth = '06';
            this.expenses6 = this.expenses6.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
            if (this.expenses6.length != 0) {
                
                this.eDate6 = this.expenses6.map(this.getArray);
            }
            else {
                this.eDate6 = [0, 0]
            }
            this.totalExpense6 = this.eDate6.reduce(this.getSum)

            var currentYear = this.selectedYear.toString();
            var currentMonth = '07';
            this.expenses7 = this.expenses7.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
            if (this.expenses7.length != 0) {
                
                this.eDate7 = this.expenses7.map(this.getArray);
            }
            else {
                this.eDate7 = [0, 0]
            }
            this.totalExpense7 = this.eDate7.reduce(this.getSum)

            var currentYear = this.selectedYear.toString();
            var currentMonth = '08';
            this.expenses8 = this.expenses8.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
            if (this.expenses8.length != 0) {
                
                this.eDate8 = this.expenses8.map(this.getArray);
            }
            else {
                this.eDate8 = [0, 0]
            }
            this.totalExpense8 = this.eDate8.reduce(this.getSum)

            var currentYear = this.selectedYear.toString();
            var currentMonth = '09';
            this.expenses9 = this.expenses9.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
            if (this.expenses9.length != 0) {
                
                this.eDate9 = this.expenses9.map(this.getArray);
            }
            else {
                this.eDate9 = [0, 0]
            }
            this.totalExpense9 = this.eDate9.reduce(this.getSum)

            var currentYear = this.selectedYear.toString();
            var currentMonth = '10';
            this.expenses10 = this.expenses10.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
            if (this.expenses10.length != 0) {
                
                this.eDate10 = this.expenses10.map(this.getArray);
            }
            else {
                this.eDate10 = [0, 0]
            }
            this.totalExpense10 = this.eDate10.reduce(this.getSum)

            var currentYear = this.selectedYear.toString();
            var currentMonth = '11';
            this.expenses11 = this.expenses11.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
            if (this.expenses11.length != 0) {
                
                this.eDate11 = this.expenses11.map(this.getArray);
            }
            else {
                this.eDate11 = [0, 0]
            }
            this.totalExpense11 = this.eDate11.reduce(this.getSum)

            var currentYear = this.selectedYear.toString();
            var currentMonth = '12';
            this.expenses12 = this.expenses12.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
            if (this.expenses12.length != 0) {
                
                this.eDate12 = this.expenses12.map(this.getArray);
            }
            else {
                this.eDate12 = [0, 0]
            }
            this.totalExpense12 = this.eDate12.reduce(this.getSum)
        }).then(res => {

            this.db.getPayrolls().then(res => {
                this.payrolls = res;
                this.payrolls2 = res;
                this.payrolls3 = res;
                this.payrolls4 = res;
                this.payrolls5 = res;
                this.payrolls6 = res;
                this.payrolls7 = res;
                this.payrolls8 = res;
                this.payrolls9 = res;
                this.payrolls10 = res;
                this.payrolls11 = res;
                this.payrolls12 = res;

                this.years = res
                var currentYear = this.selectedYear.toString();
                var currentMonth = '01';

                this.payrolls = this.payrolls.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                
                if (this.payrolls.length != 0) {
                    
                    this.pDate = this.payrolls.map(this.getPayrollArray);
                    
                }
                else {
                    this.pDate = [0, 0]
                }
                this.totalPayroll = this.pDate.reduce(this.getSum)

                
                var currentYear = this.selectedYear.toString();
                var currentMonth = '02';
                this.payrolls2 = this.payrolls2.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                if (this.payrolls2.length != 0) {
                    
                    this.pDate2 = this.payrolls2.map(this.getPayrollArray);
                    
                }
                else {
                    this.pDate2 = [0, 0]
                }
                this.totalPayroll2 = this.pDate2.reduce(this.getSum)

                var currentYear = this.selectedYear.toString();
                var currentMonth = '03';
                this.payrolls3 = this.payrolls3.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                if (this.payrolls3.length != 0) {
                    
                    this.pDate3 = this.payrolls3.map(this.getPayrollArray);
                }
                else {
                    this.pDate3 = [0, 0]
                }
                this.totalPayroll3 = this.pDate3.reduce(this.getSum)

                var currentYear = this.selectedYear.toString();
                var currentMonth = '04';
                this.payrolls4 = this.payrolls4.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                if (this.payrolls4.length != 0) {
                    
                    this.pDate4 = this.payrolls4.map(this.getPayrollArray);
                }
                else {
                    this.pDate4 = [0, 0]
                }
                this.totalPayroll4 = this.pDate4.reduce(this.getSum)

                var currentYear = this.selectedYear.toString();
                var currentMonth = '05';
                this.payrolls5 = this.payrolls5.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                if (this.payrolls5.length != 0) {
                    
                    this.pDate5 = this.payrolls5.map(this.getPayrollArray);
                }
                else {
                    this.pDate5 = [0, 0]
                }
                this.totalPayroll5 = this.pDate5.reduce(this.getSum)

                var currentYear = this.selectedYear.toString();
                var currentMonth = '06';
                this.payrolls6 = this.payrolls6.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                if (this.payrolls6.length != 0) {
                    
                    this.pDate6 = this.payrolls6.map(this.getPayrollArray);
                }
                else {
                    this.pDate6 = [0, 0]
                }
                this.totalPayroll6 = this.pDate6.reduce(this.getSum)

                var currentYear = this.selectedYear.toString();
                var currentMonth = '07';
                this.payrolls7 = this.payrolls7.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                if (this.payrolls7.length != 0) {
                    
                    this.pDate7 = this.payrolls7.map(this.getPayrollArray);
                }
                else {
                    this.pDate7 = [0, 0]
                }
                this.totalPayroll7 = this.pDate7.reduce(this.getSum)

                var currentYear = this.selectedYear.toString();
                var currentMonth = '08';
                this.payrolls8 = this.payrolls8.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                if (this.payrolls8.length != 0) {
                    
                    this.pDate8 = this.payrolls8.map(this.getPayrollArray);
                }
                else {
                    this.pDate8 = [0, 0]
                }
                this.totalPayroll8 = this.pDate8.reduce(this.getSum)

                var currentYear = this.selectedYear.toString();
                var currentMonth = '09';
                this.payrolls9 = this.payrolls9.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                if (this.payrolls9.length != 0) {
                    
                    this.pDate9 = this.payrolls9.map(this.getPayrollArray);
                }
                else {
                    this.pDate9 = [0, 0]
                }
                this.totalPayroll9 = this.pDate9.reduce(this.getSum)

                var currentYear = this.selectedYear.toString();
                var currentMonth = '10';
                this.payrolls10 = this.payrolls10.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                if (this.payrolls10.length != 0) {
                    
                    this.pDate10 = this.payrolls10.map(this.getPayrollArray);
                }
                else {
                    this.pDate10 = [0, 0]
                }
                this.totalPayroll10 = this.pDate10.reduce(this.getSum)

                var currentYear = this.selectedYear.toString();
                var currentMonth = '11';
                this.payrolls11 = this.payrolls11.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                if (this.payrolls11.length != 0) {
                    
                    this.pDate11 = this.payrolls11.map(this.getPayrollArray);
                }
                else {
                    this.pDate11 = [0, 0]
                }
                this.totalPayroll11 = this.pDate11.reduce(this.getSum)

                var currentYear = this.selectedYear.toString();
                var currentMonth = '12';
                this.payrolls12 = this.payrolls12.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                if (this.payrolls12.length != 0) {
                    
                    this.pDate12 = this.payrolls12.map(this.getPayrollArray);
                }
                else {
                    this.pDate12 = [0, 0]
                }
                this.totalPayroll12 = this.pDate12.reduce(this.getSum)

            }).then(res => {

                this.db.getInvoices().then(res => {
                    this.invoices = res;
                    this.invoices2 = res;
                    this.invoices3 = res;
                    this.invoices4 = res;
                    this.invoices5 = res;
                    this.invoices6 = res;
                    this.invoices7 = res;
                    this.invoices8 = res;
                    this.invoices9 = res;
                    this.invoices10 = res;
                    this.invoices11 = res;
                    this.invoices12 = res;
                    
                    this.years = res
                    var currentYear = this.selectedYear.toString();
                    var currentMonth = '01';

                    this.invoices = this.invoices.filter(data => data.creation_timestamp.substring(0, 4) === currentYear && data.creation_timestamp.substring(5, 7) === currentMonth)
                    
                    if (this.invoices.length != 0) {
                        
                        this.iDate = this.invoices.map(this.getInvoiceArray);
                        
                    }
                    else {
                        this.iDate = [0, 0]
                    }
                    this.totalInvoice = this.iDate.reduce(this.getSum)

                    
                    var currentYear = this.selectedYear.toString();
                    var currentMonth = '02';
                    this.invoices2 = this.invoices2.filter(data => data.creation_timestamp.substring(0, 4) === currentYear && data.creation_timestamp.substring(5, 7) === currentMonth)
                    if (this.invoices2.length != 0) {
                        
                        this.iDate2 = this.invoices2.map(this.getInvoiceArray);
                        
                    }
                    else {
                        this.iDate2 = [0, 0]
                    }
                    this.totalInvoice2 = this.iDate2.reduce(this.getSum)

                    var currentYear = this.selectedYear.toString();
                    var currentMonth = '03';
                    this.invoices3 = this.invoices3.filter(data => data.creation_timestamp.substring(0, 4) === currentYear && data.creation_timestamp.substring(5, 7) === currentMonth)
                    if (this.invoices3.length != 0) {
                        
                        this.iDate3 = this.invoices3.map(this.getInvoiceArray);
                    }
                    else {
                        this.iDate3 = [0, 0]
                    }
                    this.totalInvoice3 = this.iDate3.reduce(this.getSum)

                    var currentYear = this.selectedYear.toString();
                    var currentMonth = '04';
                    this.invoices4 = this.invoices4.filter(data => data.creation_timestamp.substring(0, 4) === currentYear && data.creation_timestamp.substring(5, 7) === currentMonth)
                    if (this.invoices4.length != 0) {
                        
                        this.iDate4 = this.invoices4.map(this.getInvoiceArray);
                    }
                    else {
                        this.iDate4 = [0, 0]
                    }
                    this.totalInvoice4 = this.iDate4.reduce(this.getSum)

                    var currentYear = this.selectedYear.toString();
                    var currentMonth = '05';
                    this.invoices5 = this.invoices5.filter(data => data.creation_timestamp.substring(0, 4) === currentYear && data.creation_timestamp.substring(5, 7) === currentMonth)
                    if (this.invoices5.length != 0) {
                        
                        this.iDate5 = this.invoices5.map(this.getInvoiceArray);
                    }
                    else {
                        this.iDate5 = [0, 0]
                    }
                    this.totalInvoice5 = this.iDate5.reduce(this.getSum)

                    var currentYear = this.selectedYear.toString();
                    var currentMonth = '06';
                    this.invoices6 = this.invoices6.filter(data => data.creation_timestamp.substring(0, 4) === currentYear && data.creation_timestamp.substring(5, 7) === currentMonth)
                    if (this.invoices6.length != 0) {
                        
                        this.iDate6 = this.invoices6.map(this.getInvoiceArray);
                    }
                    else {
                        this.iDate6 = [0, 0]
                    }
                    this.totalInvoice6 = this.iDate6.reduce(this.getSum)

                    var currentYear = this.selectedYear.toString();
                    var currentMonth = '07';
                    this.invoices7 = this.invoices7.filter(data => data.creation_timestamp.substring(0, 4) === currentYear && data.creation_timestamp.substring(5, 7) === currentMonth)
                    if (this.invoices7.length != 0) {
                        
                        this.iDate7 = this.invoices7.map(this.getInvoiceArray);
                    }
                    else {
                        this.iDate7 = [0, 0]
                    }
                    this.totalInvoice7 = this.iDate7.reduce(this.getSum)

                    var currentYear = this.selectedYear.toString();
                    var currentMonth = '08';
                    this.invoices8 = this.invoices8.filter(data => data.creation_timestamp.substring(0, 4) === currentYear && data.creation_timestamp.substring(5, 7) === currentMonth)
                    if (this.invoices8.length != 0) {
                        
                        this.iDate8 = this.invoices8.map(this.getInvoiceArray);
                    }
                    else {
                        this.iDate8 = [0, 0]
                    }
                    this.totalInvoice8 = this.iDate8.reduce(this.getSum)

                    var currentYear = this.selectedYear.toString();
                    var currentMonth = '09';
                    this.invoices9 = this.invoices9.filter(data => data.creation_timestamp.substring(0, 4) === currentYear && data.creation_timestamp.substring(5, 7) === currentMonth)
                    if (this.invoices9.length != 0) {
                        
                        this.iDate9 = this.invoices9.map(this.getInvoiceArray);
                    }
                    else {
                        this.iDate9 = [0, 0]
                    }
                    this.totalInvoice9 = this.iDate9.reduce(this.getSum)

                    var currentYear = this.selectedYear.toString();
                    var currentMonth = '10';
                    this.invoices10 = this.invoices10.filter(data => data.creation_timestamp.substring(0, 4) === currentYear && data.creation_timestamp.substring(5, 7) === currentMonth)
                    if (this.invoices10.length != 0) {
                        
                        this.iDate10 = this.invoices10.map(this.getInvoiceArray);
                    }
                    else {
                        this.iDate10 = [0, 0]
                    }
                    this.totalInvoice10 = this.iDate10.reduce(this.getSum)

                    var currentYear = this.selectedYear.toString();
                    var currentMonth = '11';
                    this.invoices11 = this.invoices11.filter(data => data.creation_timestamp.substring(0, 4) === currentYear && data.creation_timestamp.substring(5, 7) === currentMonth)
                    if (this.invoices11.length != 0) {
                        
                        this.iDate11 = this.invoices11.map(this.getInvoiceArray);
                    }
                    else {
                        this.iDate11 = [0, 0]
                    }
                    this.totalInvoice11 = this.iDate11.reduce(this.getSum)

                    var currentYear = this.selectedYear.toString();
                    var currentMonth = '12';
                    this.invoices12 = this.invoices12.filter(data => data.creation_timestamp.substring(0, 4) === currentYear && data.creation_timestamp.substring(5, 7) === currentMonth)
                    if (this.invoices12.length != 0) {
                        
                        this.iDate12 = this.invoices12.map(this.getInvoiceArray);
                    }
                    else {
                        this.iDate12 = [0, 0]
                    }
                    this.totalInvoice12 = this.iDate12.reduce(this.getSum)
                }).then(res => {
                    this.db.getBlood_sales().then(res => {
                        this.bloodSales = res;
                        this.bloodSales2 = res;
                        this.bloodSales3 = res;
                        this.bloodSales4 = res;
                        this.bloodSales5 = res;
                        this.bloodSales6 = res;
                        this.bloodSales7 = res;
                        this.bloodSales8 = res;
                        this.bloodSales9 = res;
                        this.bloodSales10 = res;
                        this.bloodSales11 = res;
                        this.bloodSales12 = res;

                        
                        this.years = res
                        var currentYear = this.selectedYear.toString();
                        var currentMonth = '01';

                        this.bloodSales = this.bloodSales.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                        
                        if (this.bloodSales.length != 0) {
                            
                            this.bsDate = this.bloodSales.map(this.getBlood_SaleArray);
                            
                        }
                        else {
                            this.bsDate = [0, 0]
                        }
                        this.totalBloodSale = this.bsDate.reduce(this.getSum);

                        
                        var currentYear = this.selectedYear.toString();
                        var currentMonth = '02';
                        this.bloodSales2 = this.bloodSales2.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                        if (this.bloodSales2.length != 0) {
                            
                            this.bsDate2 = this.bloodSales2.map(this.getBlood_SaleArray);
                            
                        }
                        else {
                            this.bsDate2 = [0, 0]
                        }
                        this.totalBloodSale2 = this.bsDate2.reduce(this.getSum)

                        var currentYear = this.selectedYear.toString();
                        var currentMonth = '03';
                        this.bloodSales3 = this.bloodSales3.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                        if (this.bloodSales3.length != 0) {
                            
                            this.bsDate3 = this.bloodSales3.map(this.getBlood_SaleArray);
                        }
                        else {
                            this.bsDate3 = [0, 0]
                        }
                        this.totalBloodSale3 = this.bsDate3.reduce(this.getSum)

                        var currentYear = this.selectedYear.toString();
                        var currentMonth = '04';
                        this.bloodSales4 = this.bloodSales4.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                        if (this.bloodSales4.length != 0) {
                            
                            this.bsDate4 = this.bloodSales4.map(this.getBlood_SaleArray);
                        }
                        else {
                            this.bsDate4 = [0, 0]
                        }
                        this.totalBloodSale4 = this.bsDate4.reduce(this.getSum)

                        var currentYear = this.selectedYear.toString();
                        var currentMonth = '05';
                        this.bloodSales5 = this.bloodSales5.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                        if (this.bloodSales5.length != 0) {
                            
                            this.bsDate5 = this.bloodSales5.map(this.getBlood_SaleArray);
                        }
                        else {
                            this.bsDate5 = [0, 0]
                        }
                        this.totalBloodSale5 = this.bsDate5.reduce(this.getSum)

                        var currentYear = this.selectedYear.toString();
                        var currentMonth = '06';
                        this.bloodSales6 = this.bloodSales6.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                        if (this.bloodSales6.length != 0) {
                            
                            this.bsDate6 = this.bloodSales6.map(this.getBlood_SaleArray);
                        }
                        else {
                            this.bsDate6 = [0, 0]
                        }
                        this.totalBloodSale6 = this.bsDate6.reduce(this.getSum)

                        var currentYear = this.selectedYear.toString();
                        var currentMonth = '07';
                        this.bloodSales7 = this.bloodSales7.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                        if (this.bloodSales7.length != 0) {
                            
                            this.bsDate7 = this.bloodSales7.map(this.getBlood_SaleArray);
                        }
                        else {
                            this.bsDate7 = [0, 0]
                        }
                        this.totalBloodSale7 = this.bsDate7.reduce(this.getSum)

                        var currentYear = this.selectedYear.toString();
                        var currentMonth = '08';
                        this.bloodSales8 = this.bloodSales8.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                        if (this.bloodSales8.length != 0) {
                            
                            this.bsDate8 = this.bloodSales8.map(this.getBlood_SaleArray);
                        }
                        else {
                            this.bsDate8 = [0, 0]
                        }
                        this.totalBloodSale8 = this.bsDate8.reduce(this.getSum)

                        var currentYear = this.selectedYear.toString();
                        var currentMonth = '09';
                        this.bloodSales9 = this.bloodSales9.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                        if (this.bloodSales9.length != 0) {
                            
                            this.bsDate9 = this.bloodSales9.map(this.getBlood_SaleArray);
                        }
                        else {
                            this.bsDate9 = [0, 0]
                        }
                        this.totalBloodSale9 = this.bsDate9.reduce(this.getSum)

                        var currentYear = this.selectedYear.toString();
                        var currentMonth = '10';
                        this.bloodSales10 = this.bloodSales10.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                        if (this.bloodSales10.length != 0) {
                            
                            this.bsDate10 = this.bloodSales10.map(this.getBlood_SaleArray);
                        }
                        else {
                            this.bsDate10 = [0, 0]
                        }
                        this.totalBloodSale10 = this.bsDate10.reduce(this.getSum)

                        var currentYear = this.selectedYear.toString();
                        var currentMonth = '11';
                        this.bloodSales11 = this.bloodSales11.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                        if (this.bloodSales11.length != 0) {
                            
                            this.bsDate11 = this.bloodSales11.map(this.getBlood_SaleArray);
                        }
                        else {
                            this.bsDate11 = [0, 0]
                        }
                        this.totalBloodSale11 = this.bsDate11.reduce(this.getSum)

                        var currentYear = this.selectedYear.toString();
                        var currentMonth = '12';
                        this.bloodSales12 = this.bloodSales12.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                        if (this.bloodSales12.length != 0) {
                            
                            this.bsDate12 = this.bloodSales12.map(this.getBlood_SaleArray);
                        }
                        else {
                            this.bsDate12 = [0, 0]
                        }
                        this.totalBloodSale12 = this.bsDate12.reduce(this.getSum)


                    }).then(res => {
                        this.db.getMedicine_sales().then(res => {
                            this.medicinesales = res;
                            this.medicinesales2 = res;
                            this.medicinesales3 = res;
                            this.medicinesales4 = res;
                            this.medicinesales5 = res;
                            this.medicinesales6 = res;
                            this.medicinesales7 = res;
                            this.medicinesales8 = res;
                            this.medicinesales9 = res;
                            this.medicinesales10 = res;
                            this.medicinesales11 = res;
                            this.medicinesales12 = res;

                            this.years = res
                            var currentYear = this.selectedYear.toString();
                            var currentMonth = '01';

                            this.medicinesales = this.medicinesales.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                            
                            if (this.medicinesales.length != 0) {
                                
                                this.mDate = this.medicinesales.map(this.getSaleArray);
                                
                            }
                            else {
                                this.mDate = [0, 0]
                            }
                            this.totalMedicine = this.mDate.reduce(this.getSum)

                            
                            var currentYear = this.selectedYear.toString();
                            var currentMonth = '02';
                            this.medicinesales2 = this.medicinesales2.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                            if (this.medicinesales2.length != 0) {
                                
                                this.mDate2 = this.medicinesales2.map(this.getSaleArray);
                                
                            }
                            else {
                                this.mDate2 = [0, 0]
                            }
                            this.totalMedicine2 = this.mDate2.reduce(this.getSum)

                            var currentYear = this.selectedYear.toString();
                            var currentMonth = '03';
                            this.medicinesales3 = this.medicinesales3.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                            if (this.medicinesales3.length != 0) {
                                
                                this.mDate3 = this.medicinesales3.map(this.getSaleArray);
                            }
                            else {
                                this.mDate3 = [0, 0]
                            }
                            this.totalMedicine3 = this.mDate3.reduce(this.getSum)

                            var currentYear = this.selectedYear.toString();
                            var currentMonth = '04';
                            this.medicinesales4 = this.medicinesales4.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                            if (this.medicinesales4.length != 0) {
                                
                                this.mDate4 = this.medicinesales4.map(this.getSaleArray);
                            }
                            else {
                                this.mDate4 = [0, 0]
                            }
                            this.totalMedicine4 = this.mDate4.reduce(this.getSum)

                            var currentYear = this.selectedYear.toString();
                            var currentMonth = '05';
                            this.medicinesales5 = this.medicinesales5.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                            if (this.medicinesales5.length != 0) {
                                
                                this.mDate5 = this.medicinesales5.map(this.getSaleArray);
                            }
                            else {
                                this.mDate5 = [0, 0]
                            }
                            this.totalMedicine5 = this.mDate5.reduce(this.getSum)

                            var currentYear = this.selectedYear.toString();
                            var currentMonth = '06';
                            this.medicinesales6 = this.medicinesales6.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                            if (this.medicinesales6.length != 0) {
                                
                                this.mDate6 = this.medicinesales6.map(this.getSaleArray);
                            }
                            else {
                                this.mDate6 = [0, 0]
                            }
                            this.totalMedicine6 = this.mDate6.reduce(this.getSum)

                            var currentYear = this.selectedYear.toString();
                            var currentMonth = '07';
                            this.medicinesales7 = this.medicinesales7.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                            if (this.medicinesales7.length != 0) {
                                
                                this.mDate7 = this.medicinesales7.map(this.getSaleArray);
                            }
                            else {
                                this.mDate7 = [0, 0]
                            }
                            this.totalMedicine7 = this.mDate7.reduce(this.getSum)

                            var currentYear = this.selectedYear.toString();
                            var currentMonth = '08';
                            this.medicinesales8 = this.medicinesales8.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                            if (this.medicinesales8.length != 0) {
                                
                                this.mDate8 = this.medicinesales8.map(this.getSaleArray);
                            }
                            else {
                                this.mDate8 = [0, 0]
                            }
                            this.totalMedicine8 = this.mDate8.reduce(this.getSum)

                            var currentYear = this.selectedYear.toString();
                            var currentMonth = '09';
                            this.medicinesales9 = this.medicinesales9.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                            if (this.medicinesales9.length != 0) {
                                
                                this.mDate9 = this.medicinesales9.map(this.getSaleArray);
                            }
                            else {
                                this.mDate9 = [0, 0]
                            }
                            this.totalMedicine9 = this.mDate9.reduce(this.getSum)

                            var currentYear = this.selectedYear.toString();
                            var currentMonth = '10';
                            this.medicinesales10 = this.medicinesales10.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                            if (this.medicinesales10.length != 0) {
                                
                                this.mDate10 = this.medicinesales10.map(this.getSaleArray);
                            }
                            else {
                                this.mDate10 = [0, 0]
                            }
                            this.totalMedicine10 = this.mDate10.reduce(this.getSum)

                            var currentYear = this.selectedYear.toString();
                            var currentMonth = '11';
                            this.medicinesales11 = this.medicinesales11.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                            if (this.medicinesales11.length != 0) {
                                
                                this.mDate11 = this.medicinesales11.map(this.getSaleArray);
                            }
                            else {
                                this.mDate11 = [0, 0]
                            }
                            this.totalMedicine11 = this.mDate11.reduce(this.getSum)

                            var currentYear = this.selectedYear.toString();
                            var currentMonth = '12';
                            this.medicinesales12 = this.medicinesales12.filter(data => data.date.substring(0, 4) === currentYear && data.date.substring(5, 7) === currentMonth)
                            if (this.medicinesales12.length != 0) {
                                
                                this.mDate12 = this.medicinesales12.map(this.getSaleArray);
                            }
                            else {
                                this.mDate12 = [0, 0]
                            }
                            this.totalMedicine12 = this.mDate12.reduce(this.getSum)

                        }).then(res => {


                            
                            this.projectsDashboardService.getWidgets2().then(res => {
                                this.widgets2 = res
                                

                                this.mywidget5 = {
                                    currentRange: 'TW',
                                    xAxis: true,
                                    yAxis: true,
                                    gradient: false,
                                    legend: true,
                                    showXAxisLabel: false,
                                    xAxisLabel: 'Days',
                                    showYAxisLabel: false,
                                    yAxisLabel: 'Isues',
                                    scheme: {
                                        domain: ['#2eb82e', '#85e085', '#C7B42C', '#AAAAAA']
                                    },
                                    onSelect: (ev) => {
                                        
                                    },
                                    supporting: {
                                        currentRange: '',
                                        xAxis: false,
                                        yAxis: false,
                                        gradient: false,
                                        legend: false,
                                        showXAxisLabel: false,
                                        xAxisLabel: 'Days',
                                        showYAxisLabel: false,
                                        yAxisLabel: 'Isues',
                                        scheme: {
                                            domain: ['#2eb82e', '#85e085', '#C7B42C', '#AAAAAA']
                                        },
                                        curve: shape.curveBasis
                                    }
                                };
                                //Incomes
                                this.projectsDashboardService.januaryIncome = this.totalInvoice + this.totalMedicine + this.totalBloodSale;
                                this.projectsDashboardService.februaryIncome = this.totalInvoice2 + this.totalMedicine2 + this.totalBloodSale2;
                                this.projectsDashboardService.marchIncome = this.totalInvoice3 + this.totalMedicine3 + this.totalBloodSale3;
                                this.projectsDashboardService.aprilIncome = this.totalInvoice4 + this.totalMedicine4 + this.totalBloodSale4;
                                this.projectsDashboardService.mayIncome = this.totalInvoice5 + this.totalMedicine5 + this.totalBloodSale5;
                                this.projectsDashboardService.juneIncome = this.totalInvoice6 + this.totalMedicine6 + this.totalBloodSale6;
                                this.projectsDashboardService.julyIncome = this.totalInvoice7 + this.totalMedicine7 + this.totalBloodSale7;
                                this.projectsDashboardService.augustIncome = this.totalInvoice8 + this.totalMedicine8 + this.totalBloodSale8;
                                this.projectsDashboardService.septemberIncome = this.totalInvoice9 + this.totalMedicine9 + this.totalBloodSale9;
                                this.projectsDashboardService.octoberIncome = this.totalInvoice10 + this.totalMedicine10 + this.totalBloodSale10;
                                this.projectsDashboardService.novemberIncome = this.totalInvoice11 + this.totalMedicine11 + this.totalBloodSale11;
                                this.projectsDashboardService.decemberIncome = this.totalInvoice12 + this.totalMedicine12 + this.totalBloodSale12;

                                //Expenses
                                this.projectsDashboardService.januaryExpense = this.totalExpense + this.totalPayroll;
                                this.projectsDashboardService.februaryExpense = this.totalExpense2 + this.totalPayroll2;
                                this.projectsDashboardService.marchExpense = this.totalExpense3 + this.totalPayroll3;
                                this.projectsDashboardService.aprilExpense = this.totalExpense4 + this.totalPayroll4;
                                this.projectsDashboardService.mayExpense = this.totalExpense5 + this.totalPayroll5;
                                this.projectsDashboardService.juneExpense = this.totalExpense6 + this.totalPayroll6;
                                this.projectsDashboardService.julyExpense = this.totalExpense7 + this.totalPayroll7;
                                this.projectsDashboardService.augustExpense = this.totalExpense8 + this.totalPayroll8;
                                this.projectsDashboardService.septemberExpense = this.totalExpense9 + this.totalPayroll9;
                                this.projectsDashboardService.octoberExpense = this.totalExpense10 + this.totalPayroll10;
                                this.projectsDashboardService.novemberExpense = this.totalExpense11 + this.totalPayroll11;
                                this.projectsDashboardService.decemberExpense = this.totalExpense12 + this.totalPayroll12;

                                res.mywidget5.mainChart.TW[0].series[0].value = this.projectsDashboardService.januaryIncome;
                                res.mywidget5.mainChart.TW[0].series[1].value = this.projectsDashboardService.januaryExpense;
                                res.mywidget5.mainChart.TW[1].series[0].value = this.projectsDashboardService.februaryIncome;
                                res.mywidget5.mainChart.TW[1].series[1].value = this.projectsDashboardService.februaryExpense;
                                res.mywidget5.mainChart.TW[2].series[0].value = this.projectsDashboardService.marchIncome;
                                res.mywidget5.mainChart.TW[2].series[1].value = this.projectsDashboardService.marchExpense;
                                res.mywidget5.mainChart.TW[3].series[0].value = this.projectsDashboardService.aprilIncome;
                                res.mywidget5.mainChart.TW[3].series[1].value = this.projectsDashboardService.aprilExpense;
                                res.mywidget5.mainChart.TW[4].series[0].value = this.projectsDashboardService.mayIncome;
                                res.mywidget5.mainChart.TW[4].series[1].value = this.projectsDashboardService.mayExpense;
                                res.mywidget5.mainChart.TW[5].series[0].value = this.projectsDashboardService.juneIncome;
                                res.mywidget5.mainChart.TW[5].series[1].value = this.projectsDashboardService.juneExpense;
                                res.mywidget5.mainChart.TW[6].series[0].value = this.projectsDashboardService.julyIncome;
                                res.mywidget5.mainChart.TW[6].series[1].value = this.projectsDashboardService.julyExpense;
                                res.mywidget5.mainChart.TW[7].series[0].value = this.projectsDashboardService.augustIncome;
                                res.mywidget5.mainChart.TW[7].series[1].value = this.projectsDashboardService.augustExpense;
                                res.mywidget5.mainChart.TW[8].series[0].value = this.projectsDashboardService.septemberIncome;
                                res.mywidget5.mainChart.TW[8].series[1].value = this.projectsDashboardService.septemberExpense;
                                res.mywidget5.mainChart.TW[9].series[0].value = this.projectsDashboardService.octoberIncome;
                                res.mywidget5.mainChart.TW[9].series[1].value = this.projectsDashboardService.octoberExpense;
                                res.mywidget5.mainChart.TW[10].series[0].value = this.projectsDashboardService.novemberIncome;
                                res.mywidget5.mainChart.TW[10].series[1].value = this.projectsDashboardService.novemberExpense;
                                res.mywidget5.mainChart.TW[11].series[0].value = this.projectsDashboardService.decemberIncome;
                                res.mywidget5.mainChart.TW[11].series[1].value = this.projectsDashboardService.decemberExpense;

                        


                            })
                        })
                    })
                })
            })
        })



        //
        this.db.getPayrolls().then(res => {
            this.payrolls = res;
            
        })
        /**
         * Widget 11
         */
        this.widget11.onContactsChanged = new BehaviorSubject({});
        this.widget11.onContactsChanged.next(this.widgets.widget11.table.rows);
        this.widget11.dataSource = new FilesDataSource(this.widget11);

        this.db.getSettings().then(res => {
            var newArray = res.map(data => data.city)
            var accessValue = newArray.length - 1;
            this.cityName = newArray[accessValue]

            $.getJSON("http://api.openweathermap.org/data/2.5/weather?q=" + this.cityName + "&APPID=e50e2e77b139b08b8d56ebcd18cbaf7c", function (json) {

                
                this.jData = JSON.stringify(json)

            }).then(res => {
                
                this.wind = res.wind.speed;
                this.tempKelvin = res.main.temp;
                this.tempCelsius = Math.round(this.tempKelvin - 273.15);
                this.windDegree = res.wind.deg;
                this.humidity = res.main.humidity;
                this.mintempKelvin = res.main.temp_min;
                this.mintempCelsius = Math.round(this.mintempKelvin - 273.15);
                this.maxtempKelvin = res.main.temp_max;
                this.maxtempCelsius = Math.round(this.maxtempKelvin - 273.15);
                this.cloudiness = res.weather[0].description;
                this.visibility = res.visibility;
                this.imageExt = res.weather[0].icon + ".png";
                this.weatherImage = "http://openweathermap.org/img/w/" + this.imageExt + "";
                this.maximageExt = res.weather[1].icon + ".png";
                this.maxweatherImage = "http://openweathermap.org/img/w/" + this.maximageExt + "";
                this.minimageExt = res.weather[2].icon + ".png";
                this.minweatherImage = "http://openweathermap.org/img/w/" + this.minimageExt + "";
                
            })

        })



        
        
    }

    dbLogout() {
        localStorage.removeItem('credentials');
        localStorage.removeItem('user'); //logout user also
        this.router.navigate(['apps/logindb'])
    }

    ngOnDestroy() {
    }


        extend(): void {
            this.router.navigate(['apps/extendlicense']);
        }


}

export class FilesDataSource extends DataSource<any>
{
    constructor(private widget11) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        return this.widget11.onContactsChanged;
    }

    disconnect() {
    }
}

