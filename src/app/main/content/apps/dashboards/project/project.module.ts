import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FuseProjectComponent } from './project.component';
import { SharedModule } from '../../../../../core/modules/shared.module';
import { ProjectsDashboardService } from './projects.service';
import { FuseWidgetModule } from '../../../../../core/components/widget/widget.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { InvoicesService } from '../../invoices/invoices.service';
import { StaffsService } from '../../staffs/staffs.service';
import { MedicinesService } from '../../medicines/medicines.service';
import { ReportsService } from '../../reports/reports.service';
import { SettingsService } from '../../settings/settings.service';
import { ExpensesService } from '../../expenses/expenses.service';
import { PayrollsService } from '../../payrolls/payrolls.service';
//import { FuseSplashScreenService } from '../../../../../core/services/splash-screen.service';
import { Medicine_salesService } from '../../medicine_sales/medicine_sales.service';
import { Blood_salesService } from '../../blood_sales/blood_sales.service';
import { Blood_banksService } from '../../blood_banks/blood_banks.service';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from '../../../../../provider/pouch-service';

const routes: Routes = [
    {
        path: 'apps/dashboards/project',
        component: FuseProjectComponent,
        canActivate: [AuthGuard],
        resolve: {
            data: ProjectsDashboardService
        }
    }
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes),
        FuseWidgetModule,
        NgxChartsModule
    ],
    declarations: [
        FuseProjectComponent,
    ],
    providers: [
        //FuseSplashScreenService,
        AuthGuard,
        ProjectsDashboardService,
        StaffsService,
        InvoicesService,
        MedicinesService,
        ReportsService,
        SettingsService,
        ExpensesService,
        PayrollsService,
        Medicine_salesService,
        Blood_salesService,
        Blood_banksService,
        PouchService
    ]
})
export class ProjectModule {
}

