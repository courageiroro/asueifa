import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { PayrollsService } from '../../payrolls/payrolls.service';
import { ExpensesService } from '../../expenses/expenses.service';
import { InvoicesService } from '../../invoices/invoices.service';
import { Medicine_salesService } from '../../medicine_sales/medicine_sales.service';

@Injectable()
export class ProjectsDashboardService implements Resolve<any>
{
    januaryExpense: any;
    public februaryExpense: any;
    marchExpense: any;
    aprilExpense: any;
    mayExpense: any;
    juneExpense: any;
    julyExpense: any;
    augustExpense: any;
    septemberExpense: any;
    octoberExpense: any;
    novemberExpense: any;
    decemberExpense: any;

    januaryIncome: any;
    public februaryIncome: any;
    marchIncome: any;
    aprilIncome: any;
    mayIncome: any;
    juneIncome: any;
    julyIncome: any;
    augustIncome: any;
    septemberIncome: any;
    octoberIncome: any;
    novemberIncome: any;
    decemberIncome: any;
    projects: any[];
    widgets: any[];
    widgets2: {};

    constructor(
        private http: Http
    ) {

        //this.februaryIncome = 10;
        


        /*  */
    }

    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

        return new Promise((resolve, reject) => {
            
            Promise.all([
                this.getProjects(),
                this.getWidgets(),
                this.getWidgets2(),
                
            ]).then(
                () => {
                    resolve();
                },
                reject
                );
        });

    }

    getProjects(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get('api/projects-dashboard-projects')
                .subscribe(response => {
                    this.projects = response.json().data;
                    resolve(response.json().data);
                }, reject);
        });
    }

    getWidgets(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get('api/projects-dashboard-widgets')
                .subscribe(response => {
                    this.widgets = response.json().data;
                    
                    resolve(response.json().data);
                }, reject);
        });
    }



    getWidgets2(): Promise<any> {
        this.januaryExpense = 5;
        this.januaryIncome = 25;
        this.februaryIncome = 10;
        this.februaryExpense = 15;
        this.marchIncome = 15;
        this.marchExpense = 19;
        this.aprilIncome = 28;
        this.aprilExpense = 16;
        this.mayIncome = 40;
        this.mayExpense = 48;
        this.juneIncome = 34;
        this.juneExpense = 4;
        this.julyIncome = 16;
        this.julyExpense = 5;
        this.augustIncome = 28;
        this.augustExpense = 12;
        this.septemberIncome = 10;
        this.septemberExpense = 15;
        this.octoberIncome = 56;
        this.octoberExpense = 34;
        this.novemberIncome = 10;
        this.novemberExpense = 15;
        this.decemberIncome = 35;
        this.decemberExpense = 12;

        
        this.widgets2 = {

            'mywidget5': {
                'title': 'Yearly Revenue',
                'ranges': {
                    'TW': 'This Week',
                    'LW': 'Last Week',
                    '2W': '2 Weeks Ago'
                },
                'mainChart': {
                    '2W': [
                        {
                            'name': 'Mon',
                            'series': [
                                {
                                    'name': 'issues',
                                    'value': 37
                                },
                                {
                                    'name': 'closed issues',
                                    'value': 9
                                }
                            ]
                        },
                        {
                            'name': 'Tue',
                            'series': [
                                {
                                    'name': 'issues',
                                    'value': 32
                                },
                                {
                                    'name': 'closed issues',
                                    'value': 12
                                }
                            ]
                        },
                        {
                            'name': 'Wed',
                            'series': [
                                {
                                    'name': 'issues',
                                    'value': 39
                                },
                                {
                                    'name': 'closed issues',
                                    'value': 9
                                }
                            ]
                        },
                        {
                            'name': 'Thu',
                            'series': [
                                {
                                    'name': 'issues',
                                    'value': 27
                                },
                                {
                                    'name': 'closed issues',
                                    'value': 12
                                }
                            ]
                        },
                        {
                            'name': 'Fri',
                            'series': [
                                {
                                    'name': 'issues',
                                    'value': 18
                                },
                                {
                                    'name': 'closed issues',
                                    'value': 7
                                }
                            ]
                        },
                        {
                            'name': 'Sat',
                            'series': [
                                {
                                    'name': 'issues',
                                    'value': 24
                                },
                                {
                                    'name': 'closed issues',
                                    'value': 8
                                }
                            ]
                        },
                        {
                            'name': 'Sun',
                            'series': [
                                {
                                    'name': 'issues',
                                    'value': 20
                                },
                                {
                                    'name': 'closed issues',
                                    'value': 16
                                }
                            ]
                        }
                    ],
                    'LW': [
                        {
                            'name': 'Mon',
                            'series': [
                                {
                                    'name': 'issues',
                                    'value': 37
                                },
                                {
                                    'name': 'closed issues',
                                    'value': 12
                                }
                            ]
                        },
                        {
                            'name': 'Tue',
                            'series': [
                                {
                                    'name': 'issues',
                                    'value': 24
                                },
                                {
                                    'name': 'closed issues',
                                    'value': 8
                                }
                            ]
                        },
                        {
                            'name': 'Wed',
                            'series': [
                                {
                                    'name': 'issues',
                                    'value': 51
                                },
                                {
                                    'name': 'closed issues',
                                    'value': 7
                                }
                            ]
                        },
                        {
                            'name': 'Thu',
                            'series': [
                                {
                                    'name': 'issues',
                                    'value': 31
                                },
                                {
                                    'name': 'closed issues',
                                    'value': 13
                                }
                            ]
                        },
                        {
                            'name': 'Fri',
                            'series': [
                                {
                                    'name': 'issues',
                                    'value': 29
                                },
                                {
                                    'name': 'closed issues',
                                    'value': 7
                                }
                            ]
                        },
                        {
                            'name': 'Sat',
                            'series': [
                                {
                                    'name': 'issues',
                                    'value': 17
                                },
                                {
                                    'name': 'closed issues',
                                    'value': 6
                                }
                            ]
                        },
                        {
                            'name': 'Sun',
                            'series': [
                                {
                                    'name': 'issues',
                                    'value': 31
                                },
                                {
                                    'name': 'closed issues',
                                    'value': 10
                                }
                            ]
                        }
                    ],
                    'TW': [
                        {
                            'name': 'January',
                            'series': [
                                {
                                    'name': 'Incomes',
                                    'value': this.januaryIncome
                                },
                                {
                                    'name': 'Expenses',
                                    'value': this.januaryExpense
                                }
                            ]
                        },
                        {
                            'name': 'February',
                            'series': [
                                {
                                    'name': 'Incomes',
                                    'value': this.februaryIncome
                                },
                                {
                                    'name': 'Expenses',
                                    'value': this.februaryExpense
                                }
                            ]
                        },
                        {
                            'name': 'March',
                            'series': [
                                {
                                    'name': 'Incomes',
                                    'value': this.marchIncome
                                },
                                {
                                    'name': 'Expenses',
                                    'value': this.marchExpense
                                }
                            ]
                        },
                        {
                            'name': 'April',
                            'series': [
                                {
                                    'name': 'Incomes',
                                    'value': this.aprilIncome
                                },
                                {
                                    'name': 'Expenses',
                                    'value': this.aprilExpense
                                }
                            ]
                        },
                        {
                            'name': 'May',
                            'series': [
                                {
                                    'name': 'Incomes',
                                    'value': this.mayIncome
                                },
                                {
                                    'name': 'Expenses',
                                    'value': this.mayExpense
                                }
                            ]
                        },
                        {
                            'name': 'June',
                            'series': [
                                {
                                    'name': 'Incomes',
                                    'value': this.juneIncome
                                },
                                {
                                    'name': 'Expenses',
                                    'value': this.juneExpense
                                }
                            ]
                        },
                        {
                            'name': 'July',
                            'series': [
                                {
                                    'name': 'Incomes',
                                    'value': this.julyIncome
                                },
                                {
                                    'name': 'Expenses',
                                    'value': this.julyExpense
                                }
                            ]
                        },
                        {
                            'name': 'August',
                            'series': [
                                {
                                    'name': 'Incomes',
                                    'value': this.augustIncome
                                },
                                {
                                    'name': 'Expenses',
                                    'value': this.augustExpense
                                }
                            ]
                        },
                        {
                            'name': 'September',
                            'series': [
                                {
                                    'name': 'Incomes',
                                    'value': this.septemberIncome
                                },
                                {
                                    'name': 'Expenses',
                                    'value': this.septemberExpense
                                }
                            ]
                        },
                        {
                            'name': 'October',
                            'series': [
                                {
                                    'name': 'Incomes',
                                    'value': this.octoberIncome
                                },
                                {
                                    'name': 'Expenses',
                                    'value': this.octoberExpense
                                }
                            ]
                        },
                        {
                            'name': 'November',
                            'series': [
                                {
                                    'name': 'Incomes',
                                    'value': this.novemberIncome
                                },
                                {
                                    'name': 'Expenses',
                                    'value': this.novemberExpense
                                }
                            ]
                        },
                        {
                            'name': 'December',
                            'series': [
                                {
                                    'name': 'Incomes',
                                    'value': this.decemberIncome
                                },
                                {
                                    'name': 'Expenses',
                                    'value': this.decemberExpense
                                }
                            ]
                        }
                    ]
                },
                'supporting': {
                    'created': {
                        'label': 'CREATED',
                        'count': {
                            '2W': 48,
                            'LW': 46,
                            'TW': 54
                        },
                        'chart': {
                            '2W': [
                                {
                                    'name': 'CREATED',
                                    'series': [
                                        {
                                            'name': 'Mon',
                                            'value': 5
                                        },
                                        {
                                            'name': 'Tue',
                                            'value': 8
                                        },
                                        {
                                            'name': 'Wed',
                                            'value': 5
                                        },
                                        {
                                            'name': 'Thu',
                                            'value': 6
                                        },
                                        {
                                            'name': 'Fri',
                                            'value': 7
                                        },
                                        {
                                            'name': 'Sat',
                                            'value': 8
                                        },
                                        {
                                            'name': 'Sun',
                                            'value': 7
                                        }
                                    ]
                                }
                            ],
                            'LW': [
                                {
                                    'name': 'Created',
                                    'series': [
                                        {
                                            'name': 'Mon',
                                            'value': 6
                                        },
                                        {
                                            'name': 'Tue',
                                            'value': 3
                                        },
                                        {
                                            'name': 'Wed',
                                            'value': 7
                                        },
                                        {
                                            'name': 'Thu',
                                            'value': 5
                                        },
                                        {
                                            'name': 'Fri',
                                            'value': 5
                                        },
                                        {
                                            'name': 'Sat',
                                            'value': 4
                                        },
                                        {
                                            'name': 'Sun',
                                            'value': 7
                                        }
                                    ]
                                }
                            ],
                            'TW': [
                                {
                                    'name': 'Created',
                                    'series': [
                                        {
                                            'name': 'Mon',
                                            'value': 3
                                        },
                                        {
                                            'name': 'Tue',
                                            'value': 2
                                        },
                                        {
                                            'name': 'Wed',
                                            'value': 1
                                        },
                                        {
                                            'name': 'Thu',
                                            'value': 4
                                        },
                                        {
                                            'name': 'Fri',
                                            'value': 8
                                        },
                                        {
                                            'name': 'Sat',
                                            'value': 8
                                        },
                                        {
                                            'name': 'Sun',
                                            'value': 4
                                        }
                                    ]
                                }
                            ]
                        }
                    }


                }
            },

        };
        //
        return new Promise((resolve, reject) => {
            this.widgets2
            
            this.februaryIncome;
            
            resolve(this.widgets2);
            //return this.widgets2

        });
    }
}


