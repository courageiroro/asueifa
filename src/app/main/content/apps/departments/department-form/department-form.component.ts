import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Department } from '../department.model';
import { DepartmentsService } from '../departments.service';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-departments-department-form-dialog',
    templateUrl: './department-form.component.html',
    styleUrls: ['./department-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseDepartmentsDepartmentFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    departmentForm: FormGroup;
    action: string;
    department: Department;

    constructor(
        public dialogRef: MdDialogRef<FuseDepartmentsDepartmentFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        public db: PouchService,
    ) {

        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Department';
            this.department = data.department;
        }
        else {
            this.dialogTitle = 'New Department';
            this.department = {
                id: '',
                rev: '',
                name: '',
                description: '',
                doctors: []
            }
        }

        this.departmentForm = this.createdepartmentForm();
    }

    ngOnInit() {
    }

    createdepartmentForm() {
        return this.formBuilder.group({
            id: [this.department.id],
            rev: [this.department.rev],
            name: [this.department.name],
            description: [this.department.description],

        });
    }
}
