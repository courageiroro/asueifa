import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { DepartmentsService } from '../departments.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { FuseDepartmentsDepartmentFormDialogComponent } from '../department-form/department-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Department } from '../department.model';
import { Http, Headers, RequestOptions } from '@angular/http';
import { PouchService } from '../../../../../provider/pouch-service';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';

@Component({
    selector: 'fuse-departments-department-list',
    templateUrl: './department-list.component.html',
    styleUrls: ['./department-list.component.scss']
})
export class FuseDepartmentsDepartmentListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public departments: Array<Department> = [];
    departments2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'name', 'description', 'buttons'];
    selectedDepartments: any[];
    online = true;
    checkboxes: {};

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;

    constructor(public dialog: MdDialog, public db: PouchService, public http: Http) {

    }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        this._loadDepartments();
    }

    private _loadDepartments(): void {
        this.db.onDepartmentsChanged.subscribe(data => {
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        this.db.getDepartments()
            .then((departments: Array<Department>) => {
                this.departments = departments;
                this.departments2 = new BehaviorSubject<any>(departments);
                //console.log(this.departments2);

                this.checkboxes = {};
                departments.map(department => {
                    this.checkboxes[department.id] = false;
                    console.log(this.checkboxes);
                    console.log(departments);
                });
                this.db.onSelectedDepartmentsChanged.subscribe(selectedDepartments => {
                    for (const id in this.checkboxes) {
                        this.checkboxes[id] = selectedDepartments.includes(id);
                    }

                    this.selectedDepartments = selectedDepartments;
                });
                this.db.onSearchTextChanged.subscribe(data => {
                    this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                })
            });

    }


    onlineCheck() {
        this.online = true;
        this.http.get('https://sarutech.com/couchdblogin/auth/validate-username/xs2da2432252sda').subscribe(data => {
            this.online = true;

        },
            (err) => {
                this.online = false;

            });
    }

    click() {
        this.onlineCheck();
    }

    newDepartment() {
        this.dialogRef = this.dialog.open(FuseDepartmentsDepartmentFormDialogComponent, {
            panelClass: 'department-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }

                this.db.saveDepartment(response.getRawValue());
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);


            });

    }

    editDepartment(department) {

        this.dialogRef = this.dialog.open(FuseDepartmentsDepartmentFormDialogComponent, {
            panelClass: 'department-form-dialog',
            data: {
                department: department,
                action: 'edit'

            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':

                        this.db.updateDepartment(formData.getRawValue());
                        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteDepartment(department);

                        break;
                }
            });
    }

    /**
     * Delete department
     */
    deleteDepartment(department) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deleteDepartment(department);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(departmentId) {
        this.db.toggleSelectedDepartment(departmentId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    departments2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            of(this.db.departments).delay(5000),
            this.sort.mdSortChange,
            this._paginator.page,
        ];

        this._paginator.length = this.db.departments.length;

        var result = Observable.fromPromise(this.db.getDepartments());
        /*   result.subscribe(x => console.log(x), e => console.error(e));
         return result; */

        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.departments]));
        });
    }

    disconnect() {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
            return data;
        }

        return data.sort((a, b) => {
            const isAsc = this.sort.direction === 'asc';
            switch (this.sort.active) {
                case 'name': return compare(a.name, b.name, isAsc);
                case 'amount': return compare(+a.amount, +b.amount, isAsc);
                case 'id': return compare(+a.id, +b.id, isAsc);
                default: return 0;
            }
        });
    }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
