export interface Department {
    id: string,
    rev: string,
    name: string,
    description: string,
    doctors: Array<string>
}

