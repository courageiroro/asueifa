import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { DepartmentsService } from './departments.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import { UserService } from '../../../../user.service';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector: 'fuse-departments',
    templateUrl: './departments.component.html',
    styleUrls: ['./departments.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [Animations.slideInTop]
})
export class FuseDepartmentsComponent implements OnInit {
    hasSelectedDepartments: boolean;
    searchInput: FormControl;

    constructor(private departmentsService: PouchService, public user:UserService) {
        this.searchInput = new FormControl('');
        var localStorageItem = JSON.parse(localStorage.getItem('user'));
        
        if(localStorageItem){
         this.user.setUserLoggedIn();
        }
       
    }

    ngOnInit() {

        this.departmentsService.onSelectedDepartmentsChanged
            .subscribe(selectedDepartments => {
                this.hasSelectedDepartments = selectedDepartments.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.departmentsService.onSearchTextChanged.next(searchText);
            });
    }

}
