import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseDepartmentsComponent } from './departments.component';
import { DepartmentsService } from './departments.service';
//import { departmentsService2 } from './contacts.service';
import { FuseDepartmentsDepartmentListComponent } from './department-list/department-list.component';
import { FuseDepartmentsSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseDepartmentsDepartmentFormDialogComponent } from './department-form/department-form.component';
import { UserService } from '../../../../user.service';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/departments',
        component: FuseDepartmentsComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            departments: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseDepartmentsComponent,
        FuseDepartmentsDepartmentListComponent,
        FuseDepartmentsSelectedBarComponent,
        FuseDepartmentsDepartmentFormDialogComponent
    ],
    providers      : [
        DepartmentsService,
        PouchService,
        AuthGuard,
        /* UserService */
    ],
    entryComponents: [FuseDepartmentsDepartmentFormDialogComponent]
})
export class FuseDepartmentsModule
{
}
