/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Department } from './department.model';
import { Schema } from '../schema';

@Injectable()
export class DepartmentsService {
    public department = "";
    public sCredentials;
    onDepartmentsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedDepartmentsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    departments: Department[];
    user: any;
    selectedDepartments: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The departments App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getDepartments()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getDepartments();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getDepartments();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('abayelsa', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/abayelsa';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/abayelsa';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }


    /***********
     * department
     **********/
    /**
     * Save a department
     * @param {department} Department
     *
     * @return Promise<department>
     */
    saveDepartment(department: Department): Promise<Department> {
        department.id = Math.floor(Date.now()).toString();

        department.doctors = [];
        return this.db.rel.save('department', department)
            .then((data: any) => {

                if (data && data.departments && data.departments
                [0]) {
                    console.log('save');
                    
                    return data.departments[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a department
    * @param {department} Department
    *
    * @return Promise<department>
    */
    updateDepartment(department: Department) {
        return this.db.rel.save('department', department)
            .then((data: any) => {
                if (data && data.departments && data.departments
                [0]) {
                    console.log('Update')
                    
                    return data.departments[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a department
     * @param {department} department
     *
     * @return Promise<boolean>
     */
    removedepartment(department: Department): Promise<boolean> {
        return this.db.rel.del('department', department)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the departments
     *
     * @return Promise<Array<department>>
     */
    getDepartments(): Promise<Array<Department>> {
        return this.db.rel.find('department')
            .then((data: any) => {
                this.departments = data.departments;
                if (this.searchText && this.searchText !== '') {
                    this.departments = FuseUtils.filterArrayByString(this.departments, this.searchText);
                }
                //this.ondepartmentsChanged.next(this.departments);
                return Promise.resolve(this.departments);
                //return data.departments ? data.departments : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getDoctorDepartments(): Promise<Array<Department>> {
        return this.db.rel.find('department')
            .then((data: any) => {
                return data.departments ? data.departments : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a department
     * @param {department} department
     *
     * @return Promise<department>
     */
    getDepartment(department: Department): Promise<Department> {
        return this.db.rel.find('department', department.id)
            .then((data: any) => {
                console.log("Get")
                
                return data && data.departments ? data.departments[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected department by id
     * @param id
     */
    toggleSelectedDepartment(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedDepartments.length > 0) {
            const index = this.selectedDepartments.indexOf(id);

            if (index !== -1) {
                this.selectedDepartments.splice(index, 1);

                // Trigger the next event
                this.onSelectedDepartmentsChanged.next(this.selectedDepartments);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedDepartments.push(id);

        // Trigger the next event
        this.onSelectedDepartmentsChanged.next(this.selectedDepartments);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedDepartments.length > 0) {
            this.deselectDepartments();
        }
        else {
            this.selectDepartments();
        }
    }

    selectDepartments(filterParameter?, filterValue?) {
        this.selectedDepartments = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedDepartments = [];
            this.departments.map(department => {
                this.selectedDepartments.push(department.id);
            });
        }
        else {
            /* this.selecteddepartments.push(...
                 this.departments.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedDepartmentsChanged.next(this.selectedDepartments);
    }





    deselectDepartments() {
        this.selectedDepartments = [];

        // Trigger the next event
        this.onSelectedDepartmentsChanged.next(this.selectedDepartments);
    }

    deleteDepartment(department) {
        this.db.rel.del('department', department)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const departmentIndex = this.departments.indexOf(department);
                this.departments.splice(departmentIndex, 1);
                this.onDepartmentsChanged.next(this.departments);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedDepartments() {
        for (const departmentId of this.selectedDepartments) {
            const department = this.departments.find(_department => {
                return _department.id === departmentId;
            });

            this.db.rel.del('department', department)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const departmentIndex = this.departments.indexOf(department);
                    this.departments.splice(departmentIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onDepartmentsChanged.next(this.departments);
        this.deselectDepartments();
    }
}
