import { Component, OnInit } from '@angular/core';
import { DepartmentsService } from '../departments.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseDepartmentsSelectedBarComponent implements OnInit
{
    selectedDepartments: string[];
    hasSelectedDepartments: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private departmentsService: PouchService,
        public dialog: MdDialog
    )
    {
        this.departmentsService.onSelectedDepartmentsChanged
            .subscribe(selectedDepartments => {
                this.selectedDepartments = selectedDepartments;
                setTimeout(() => {
                    this.hasSelectedDepartments = selectedDepartments.length > 0;
                    this.isIndeterminate = (selectedDepartments.length !== this.departmentsService.departments.length && selectedDepartments.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.departmentsService.selectDepartments();
    }

    deselectAll()
    {
        this.departmentsService.deselectDepartments();
    }

    deleteSelectedDepartments()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected departments?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.departmentsService.deleteSelectedDepartments();
            }
            this.confirmDialogRef = null;
        });
    }

}
