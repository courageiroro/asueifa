import { FuseUtils } from '../../../../core/fuseUtils';

export class Staff
{
    id: string;
    rev: string;		
    stafftype: string;		
    fullname: string;		
    address: string;		
    mobile: string;		
    salary: string;
    email: string;
    password: string;
    bus: string;
    driverslicenses: Array<string>;

    constructor(staff)
    {
        {
            this.id = staff.id || FuseUtils.generateGUID();
            this.rev = staff.name || '';
            this.stafftype = staff.lastName || '';
            this.fullname = staff.nickname || '';
            this.address = staff.company || '';
            this.mobile = staff.jobTitle || '';
            this.email = staff.email || '';
            this.salary = staff.phone || '';
            this.address = staff.address || '';
            this.password = staff.birthday || '';
            this.bus = staff.notes || '';
            this.driverslicenses = [];
        }
    }
}
