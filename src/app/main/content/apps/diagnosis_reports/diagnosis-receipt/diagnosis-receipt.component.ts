import { Component, Inject, OnInit, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Diagnosis_reportsService } from '../diagnosis_reports.service';
import { StaffsService } from '../../staffs/staffs.service';
import { SettingsService } from '../../settings/settings.service';
import { CurrencysService } from '../../currencys/currencys.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-diagnosis_report-receipts-diagnosis_report-receipt-form-dialog',
    templateUrl: './diagnosis-receipt.component.html',
    styleUrls: ['./diagnosis\-receipt.component.scss'],
    encapsulation: ViewEncapsulation.None,
    outputs: ['childEvent']
})

export class FuseDiagnosis_reportreceiptsDiagnosis_reportreceiptFormDialogComponent implements OnInit {

    event: CalendarEvent;
    dialogTitle: string;
    diagnosis_reportreceiptForm: FormGroup;
    childEvent = new EventEmitter<string>();
    action: string;
    public settings;
    public result;
    public final;
    public hospitalName;
    public result2;
    public final2;
    public hospitalAddress;
    public result3;
    public final3;
    public hospitalPhone;
    public result4;
    public final4;
    public hospitalEmail;
    public Pid;
    public diagnosis_reportDate: any;
    public diagnosis_reportMonth: any;
    public diagnosis_reportNumber: any;
    public diagnosis_reportDueDate: any;
    public staffName: any;
    public basicSalary: any;
    public userType: any;
    public allowancess;
    public deductionss;
    public staffId;
    public staffAddress;
    public staffNumber;
    public staffEmail;
    public subTotal;
    public vat;
    public discount;
    public total;
    public currencys;
    public result5;
    public final5;
    public displayCurrencys;
    public documentName: any;
    public prescriptionName: any;
    public fileName;
    public description;
    public laboratoristName;


    constructor(
        public dialogRef: MdDialogRef<FuseDiagnosis_reportreceiptsDiagnosis_reportreceiptFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        public db: PouchService,
        public router: Router,
        public activateRoute: ActivatedRoute
    ) {
        this.Pid = data.pid
        this.dialogTitle = 'diagnosis_report Receipt';
        /* 
                if (this.action === 'edit') {
                    this.dialogTitle = 'Edit diagnosis_report-receipt';
                    this.diagnosis_report-receipt = data.diagnosis_report-receipt;
                }
                else {
                    this.dialogTitle = 'New diagnosis_report-receipt';
                    this.diagnosis_report-receipt = {
                        id: '',
                        rev: '',
                        name: '',
                        description: '',
                        doctors: []
                    } */


        //this.diagnosis_report-receiptForm = this.creatediagnosis_report-receiptForm();
    }

    getArray(gets) {
        var mine = [gets.name].join()
        return mine

    }
    getAddress(gets) {
        var mine = [gets.address].join()
        return mine

    }
    getPhone(gets) {
        var mine = [gets.phone].join()
        return mine

    }
    getEmail(gets) {
        var mine = [gets.email].join()
        return mine

    }

    getCurrency(gets) {
        var mine = [gets.currency_symbol].join()
        return mine

    }

    getAmount(gets) {
        var mine = [gets.amount].join()
        return mine

    }

    ngOnInit() {
        this.db.getDiagnosis_report2(this.Pid).then(res => {
            
            //this.diagnosis_reportDate = new Date(res.creation_timestamp).toDateString()
            this.diagnosis_reportNumber = res.report_type;
            this.diagnosis_reportDueDate = new Date(res.timestamp).toDateString();
            this.documentName = res.document_type;
            this.fileName = res.file_name;
            this.prescriptionName = res.prescription_id;
            this.description = res.description;
            this.description = this.description.replace(/<\/?[^>]+>/ig, "  ");
            this.laboratoristName = res.laboratorist_id;
            /*  this.patientEmail = res.pemail;
             this.unitss = res.amountss;
             this.subTotal = res.subtotal;
             this.vat = res.vat_percentage;
             this.discount = res.discount_amount;
             this.total = res.grandtotal; */
            //console.log(this.arrayAmount)

            this.db.getCurrencys().then(res => {
                this.currencys = res;
                this.result5 = this.currencys.map(this.getCurrency)
                this.final5 = this.result5.length - 1;
                this.displayCurrencys = this.result5[this.final5]
                
                /*  this.arrayAmount = this.unitss.map(this.getAmount)
                 console.log(this.arrayAmount.join())
                 for (this.i = 0; this.i <= this.arrayAmount.length-1; this.i++) {
                     this.formatted = "\n" + (new CurrencyPipe('en-US')).transform(this.arrayAmount[this.i], this.displayCurrencys.trim(), true)+ "<br>";
                     console.log(this.formatted)
                     this.newArray = [];
                     this.newArray.push(this.formatted.toString());
                     console.log(this.newArray);
                 } */
            })


        })


        this.db.getSettings().then(res => {
            this.settings = res;
            this.result = this.settings.map(this.getArray)
            this.final = this.result.length - 1;
            this.hospitalName = this.result[this.final]

            this.result2 = this.settings.map(this.getAddress)
            this.final2 = this.result2.length - 1;
            this.hospitalAddress = this.result2[this.final2]

            this.result3 = this.settings.map(this.getPhone)
            this.final3 = this.result3.length - 1;
            this.hospitalPhone = this.result3[this.final3]

            this.result4 = this.settings.map(this.getEmail)
            this.final4 = this.result4.length - 1;
            this.hospitalEmail = this.result4[this.final4]
        })


        //DATE PIPE
        //this.birthday = new Date;
        /* this.datePipe.transform(this.birthday, 'yyyy-MM-dd');
        console.log(this.datePipe.transform(this.birthday, 'yyyy-MM-dd')); */
        //END
    }

    print() {

        window.print();

    }

    creatediagnosis_reportreceiptForm() {
        /*  return this.formBuilder.group({
             id: [this.diagnosis_report-receipt.id],
             rev: [this.diagnosis_report-receipt.rev],
             name: [this.diagnosis_report-receipt.name],
             description: [this.diagnosis_report-receipt.description],
 
         });*/
    }
}
