import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { DataSource } from '@angular/cdk';
import { FormBuilder, FormGroup } from '@angular/forms';
import { BehaviorSubject } from 'rxjs/Rx';
import 'rxjs/Rx';
import { Diagnosis_report } from '../diagnosis_report.model';
import { StaffsService } from '../../staffs/staffs.service';
import { PrescriptionsService } from '../../prescriptions/prescriptions.service';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-diagnosis_reports-diagnosis_report-form-dialog',
    templateUrl: './diagnosis_report-form.component.html',
    styleUrls: ['./diagnosis_report-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseDiagnosis_reportsDiagnosis_reportFormDialogComponent implements OnInit {

    event: CalendarEvent;
    dialogTitle: string;
    diagnosis_reportForm: FormGroup;
    action: string;
    diagnosis_report: Diagnosis_report;
    diagnosis_reporttype: any;
    laboratorists: any;
    patients: any;
    prescriptions: any;
    public nameId: string;
    public prescriptionId: string;
    public patientId: string

    constructor(
        public dialogRef: MdDialogRef<FuseDiagnosis_reportsDiagnosis_reportFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        private staffService: PouchService,
        
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit diagnosis report';
            this.diagnosis_report = data.diagnosis_report;
        }
        else {
            this.dialogTitle = 'New diagnosis report';
            this.diagnosis_report = {
                id: '',
                rev: '',
                report_type: '',
                document_type: '',
                file_name: '',
                prescription_id: '',
                description: '',
                timestamp: new Date,
                laboratorist_id: '',
                name_id: '',
                patient_id: '',
                patient_name: '',
                prescription_name: ''
            }
        }

        this.diagnosis_reportForm = this.createDiagnosis_reportForm();
    }

    ngOnInit() {
        this.staffService.getLStaffs().then(res => {
            this.laboratorists = res
            
        })
        this.staffService.getPrescriptions().then(res => {
            this.prescriptions = res
        })
        this.staffService.getPStaffs().then(res => {
            this.patients = res
        })
    }

    getLId(laboratorist) {
        //this.db.patientname = patient.id;
        this.nameId = laboratorist.id;
        
    }

    getPId(prescription) {
        //this.db.patientname = patient.id;
        this.prescriptionId = prescription.id;
        
    }

    getPAId(patient) {
        this.patientId = patient.id;
        
    }

    createDiagnosis_reportForm() {
        return this.formBuilder.group({
            id: [this.diagnosis_report.id],
            rev: [this.diagnosis_report.rev],
            report_type: [this.diagnosis_report.report_type],
            document_type: [this.diagnosis_report.document_type],
            file_name: [this.diagnosis_report.file_name],
            prescription_id: [this.diagnosis_report.prescription_id],
            description: [this.diagnosis_report.description],
            timestamp: [this.diagnosis_report.timestamp],
            name_id: [this.diagnosis_report.name_id],
            laboratorist_id: [this.diagnosis_report.laboratorist_id],
            patient_id: [this.diagnosis_report.patient_id],
            patient_name: [this.diagnosis_report.patient_name],
            prescription_name: [this.diagnosis_report.prescription_name]
        });
    }
}
