import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Diagnosis_reportsService } from '../diagnosis_reports.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { FuseDiagnosis_reportsDiagnosis_reportFormDialogComponent } from '../diagnosis_report-form/diagnosis_report-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Diagnosis_report } from '../diagnosis_report.model';
import {PouchService} from '../../../../../provider/pouch-service';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';

@Component({
    selector: 'fuse-diagnosis_reports-diagnosis_report-list',
    templateUrl: './diagnosis_report-list.component.html',
    styleUrls: ['./diagnosis_report-list.component.scss']
})
export class FuseDiagnosis_reportsDiagnosis_reportListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public diagnosis_reports: Array<Diagnosis_report> = [];
    diagnosis_reports2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'laboratorist','patient', 'prescription', 'report', 'document', 'file', 'date', 'buttons'];
    selectedDiagnosis_reports: any[];
    checkboxes: {};

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;


    constructor(public dialog: MdDialog, public db: PouchService, public router: Router)
    { }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        this._loadDiagnosis_reports();

    }

    private _loadDiagnosis_reports(): void {
        this.db.onDiagnosis_reportsChanged.subscribe(data => {
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        this.db.getDiagnosis_reports()
            .then((diagnosis_reports: Array<Diagnosis_report>) => {
                this.diagnosis_reports = diagnosis_reports;
                this.diagnosis_reports2 = new BehaviorSubject<any>(diagnosis_reports);
                //console.log(this.diagnosis_reports2);

                this.checkboxes = {};
                diagnosis_reports.map(diagnosis_report => {
                    this.checkboxes[diagnosis_report.id] = false;
                });

                this.db.onSelectedDiagnosis_reportsChanged.subscribe(selectedDiagnosis_reports => {
                    for (const id in this.checkboxes) {
                        this.checkboxes[id] = selectedDiagnosis_reports.includes(id);
                    }

                    this.selectedDiagnosis_reports = selectedDiagnosis_reports;
                });
                this.db.onSearchTextChanged.subscribe(data => {
                    this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                })
            });

    }

    newDiagnosis_report() {
        this.dialogRef = this.dialog.open(FuseDiagnosis_reportsDiagnosis_reportFormDialogComponent, {
            panelClass: 'diagnosis_report-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
                //console.log(response.getRawValue());
                var res = response.getRawValue();
                res.timestamp = new Date(res.timestamp).toISOString().substring(0, 10);
                this.db.saveDiagnosis_report(res);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

            });

    }

    editDiagnosis_report(diagnosis_report) {
        this.dialogRef = this.dialog.open(FuseDiagnosis_reportsDiagnosis_reportFormDialogComponent, {
            panelClass: 'diagnosis_report-form-dialog',
            data: {
                diagnosis_report: diagnosis_report,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                var form = formData.getRawValue();
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':
                        form.timestamp = new Date(form.timestamp).toISOString().substring(0, 10);
                        this.db.updateDiagnosis_report(form);
                        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteDiagnosis_report(diagnosis_report);

                        break;
                }
            });
    }

    /**
     * Delete diagnosis_report
     */
    deleteDiagnosis_report(diagnosis_report) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deleteDiagnosis_report(diagnosis_report);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
            }
            this.confirmDialogRef = null;
        });

    }

    viewDiagnosis_report(diagnosis_report) {
        this.router.navigate(['/apps/diagnosis_reportpages/', diagnosis_report.id]);
    }

    onSelectedChange(diagnosis_reportId) {
        this.db.toggleSelectedDiagnosis_report(diagnosis_reportId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    diagnosis_reports2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            of(this.db.diagnosis_reports).delay(5000),
            this.sort.mdSortChange,
            this._paginator.page,
        ];

        var result = Observable.fromPromise(this.db.getDiagnosis_reports());
       /*  result.subscribe(x => console.log(x), e => console.error(e));
        return result; */

        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.diagnosis_reports]));
        });
    }

    disconnect() {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
          return data;
        }
    
        return data.sort((a, b) => {
          const isAsc = this.sort.direction === 'asc';
          switch (this.sort.active) {
            case 'patient': return compare(a.patient_name, b.patient_name, isAsc);
            case 'amount': return compare(+a.amount, +b.amount, isAsc);
            case 'id': return compare(+a.id, +b.id, isAsc);
            default: return 0;
          }
        });
      }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

