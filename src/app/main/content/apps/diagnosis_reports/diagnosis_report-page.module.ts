import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseDiagnosis_reportsComponent } from './diagnosis_reports.component';
import { Diagnosis_reportsService } from './diagnosis_reports.service';
import { FuseDiagnosis_reportsDiagnosis_reportListComponent } from './diagnosis_report-list/diagnosis_report-list.component';
import { FuseDiagnosis_reportsSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseDiagnosis_reportsDiagnosis_reportFormDialogComponent } from './diagnosis_report-form/diagnosis_report-form.component';
//import {AmountComponent} from './invoice-form/addtext.component'
import { FuseDiagnosis_reportsDiagnosis_reportPageComponent } from './diagnosis_report-page/diagnosis_report-page.component'
import { StaffsService } from './../staffs/staffs.service';
import { Staff } from './../staffs/staff.model';
import { SettingsService } from './../settings/settings.service';
import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from "@angular/core";
import { CurrencyPipe } from '@angular/common';
import { CurrencysService } from './../currencys/currencys.service';
import { FuseDiagnosis_reportreceiptsDiagnosis_reportreceiptFormDialogComponent } from './diagnosis-receipt/diagnosis-receipt.component';

const routes: Routes = [
    {
        path: '**',
        component: FuseDiagnosis_reportsDiagnosis_reportPageComponent,
        children: [],
        resolve: {
            diagnosis_reports: Diagnosis_reportsService
        }
    }
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        FuseDiagnosis_reportsDiagnosis_reportPageComponent,
        FuseDiagnosis_reportreceiptsDiagnosis_reportreceiptFormDialogComponent
        //AmountComponent
    ],
    providers: [
        Diagnosis_reportsService,
        StaffsService,
        SettingsService,
        CurrencyPipe,
        CurrencysService,
        DatePipe
    ],
    entryComponents: [FuseDiagnosis_reportsDiagnosis_reportPageComponent, FuseDiagnosis_reportreceiptsDiagnosis_reportreceiptFormDialogComponent]
})
export class FuseDiagnosis_reportPageModule {
}
