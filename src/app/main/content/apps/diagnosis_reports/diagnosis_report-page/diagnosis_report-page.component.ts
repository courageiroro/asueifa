import { Component, Inject, OnInit, ViewEncapsulation, ViewChild, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef, MdDialog } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Diagnosis_report } from '../diagnosis_report.model';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { Diagnosis_reportsService } from '../diagnosis_reports.service';
import { StaffsService } from '../../staffs/staffs.service';
import { SettingsService } from '../../settings/settings.service';
import { DataSource } from '@angular/cdk';
import { BehaviorSubject } from 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { CurrencysService } from '../../currencys/currencys.service';
import { Pipe, PipeTransform } from "@angular/core";
import { DatePipe } from '@angular/common';
import { CurrencyPipe } from '@angular/common';
import { FuseDiagnosis_reportreceiptsDiagnosis_reportreceiptFormDialogComponent } from '../diagnosis-receipt/diagnosis-receipt.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-diagnosis_reports-diagnosis_report-page',
    templateUrl: './diagnosis_report-page.component.html',
    styleUrls: ['./diagnosis_report-page.component.scss'],

})

export class FuseDiagnosis_reportsDiagnosis_reportPageComponent implements OnInit {

    event: CalendarEvent;
    dialogTitle: string;
    dialogRef: any;
    action: string;
    pid;
    dataSource: FilesDataSource | null;
    diagnosis_reports2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    diagnosis_report: Diagnosis_report;
    public diagnosis_reports: Array<Diagnosis_report> = [];
    checkboxes: {};
    selectedDiagnosis_reports: any[];
    statuss: any;
    public diagnosis_reportDate: any;
    public diagnosis_reportNumber: any;
    public diagnosis_reportDueDate: any;
    public documentName: any;
    public prescriptionName: any;
    public unitss;
    public fileName;
    public description;
    public laboratoristName;
    public patientEmail;
    public subTotal;
    public vat;
    public discount;
    public total;
    public settings;
    public result;
    public final;
    public hospitalName;
    public result2;
    public final2;
    public hospitalAddress;
    public result3;
    public final3;
    public hospitalPhone;
    public result4;
    public final4;
    public hospitalEmail;
    public id;
    public currencys;
    public result5;
    public final5;
    public displayCurrencys;
    public arrayAmount;
    public finalarrayAmount;
    public i;
    public formatted;
    public newArray;
    public hide;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        public dialog: MdDialog,
        public activateRoute: ActivatedRoute,
        public db: PouchService,
        private datePipe: DatePipe,
        private currencyPipe: CurrencyPipe
    ) {


    }

    getArray(gets) {
        var mine = [gets.name].join()
        return mine

    }
    getAddress(gets) {
        var mine = [gets.address].join()
        return mine

    }
    getPhone(gets) {
        var mine = [gets.phone].join()
        return mine

    }
    getEmail(gets) {
        var mine = [gets.email].join()
        return mine

    }

    getCurrency(gets) {
        var mine = [gets.currency_symbol].join()
        return mine

    }

    getAmount(gets) {
        var mine = [gets.amount].join()
        return mine

    }

    ngOnInit() {

        //this.pipe = currency
        //this.ok = 15;
        this.dataSource = new FilesDataSource(this.db);
        this.db.initDB(this.db.sCredentials);

        let id = this.activateRoute.snapshot.params['id'];
        this.pid = id
        this.db.getDiagnosis_report2(id).then(res => {
            
            //this.diagnosis_reportDate = new Date(res.creation_timestamp).toDateString()
            this.diagnosis_reportNumber = res.report_type;
            this.diagnosis_reportDueDate = new Date(res.timestamp).toDateString();
            this.documentName = res.document_type;
            this.fileName = res.file_name;
            this.prescriptionName = res.prescription_id;
            this.description = res.description;
            this.description = this.description.replace(/<\/?[^>]+>/ig, "  ");
            this.laboratoristName = res.laboratorist_id;
            /*  this.patientEmail = res.pemail;
             this.unitss = res.amountss;
             this.subTotal = res.subtotal;
             this.vat = res.vat_percentage;
             this.discount = res.discount_amount;
             this.total = res.grandtotal; */
            //console.log(this.arrayAmount)

            this.db.getCurrencys().then(res => {
                this.currencys = res;
                this.result5 = this.currencys.map(this.getCurrency)
                this.final5 = this.result5.length - 1;
                this.displayCurrencys = this.result5[this.final5]
                
                /*  this.arrayAmount = this.unitss.map(this.getAmount)
                 console.log(this.arrayAmount.join())
                 for (this.i = 0; this.i <= this.arrayAmount.length-1; this.i++) {
                     this.formatted = "\n" + (new CurrencyPipe('en-US')).transform(this.arrayAmount[this.i], this.displayCurrencys.trim(), true)+ "<br>";
                     console.log(this.formatted)
                     this.newArray = [];
                     this.newArray.push(this.formatted.toString());
                     console.log(this.newArray);
                 } */
            })


        })


        this.db.getSettings().then(res => {
            this.settings = res;
            this.result = this.settings.map(this.getArray)
            this.final = this.result.length - 1;
            this.hospitalName = this.result[this.final]

            this.result2 = this.settings.map(this.getAddress)
            this.final2 = this.result2.length - 1;
            this.hospitalAddress = this.result2[this.final2]

            this.result3 = this.settings.map(this.getPhone)
            this.final3 = this.result3.length - 1;
            this.hospitalPhone = this.result3[this.final3]

            this.result4 = this.settings.map(this.getEmail)
            this.final4 = this.result4.length - 1;
            this.hospitalEmail = this.result4[this.final4]
        })


        //DATE PIPE
        //this.birthday = new Date;
        /* this.datePipe.transform(this.birthday, 'yyyy-MM-dd');
        console.log(this.datePipe.transform(this.birthday, 'yyyy-MM-dd')); */
        //END


    }

    private _loadDiagnosis_reports(): void {
        this.db.onDiagnosis_reportsChanged.subscribe(data => {
            this.dataSource = new FilesDataSource(this.db);
        })
        this.db.getDiagnosis_reports()
            .then((diagnosis_reports: Array<Diagnosis_report>) => {
                this.diagnosis_reports = diagnosis_reports;
                this.diagnosis_reports2 = new BehaviorSubject<any>(diagnosis_reports);
                //console.log(this.invoices2);

                this.checkboxes = {};
                diagnosis_reports.map(diagnosis_report => {
                    this.checkboxes[diagnosis_report.id] = false;
                });
                this.db.onSelectedDiagnosis_reportsChanged.subscribe(selectedDiagnosis_reports => {
                    for (const id in this.checkboxes) {
                        this.checkboxes[id] = selectedDiagnosis_reports.includes(id);
                    }

                    this.selectedDiagnosis_reports = selectedDiagnosis_reports;
                });
                this.db.onSearchTextChanged.subscribe(data => {
                    this.dataSource = new FilesDataSource(this.db);
                })
            });

    }

    print() {
        this.dialogRef = this.dialog.open(FuseDiagnosis_reportreceiptsDiagnosis_reportreceiptFormDialogComponent, {
            panelClass: 'payroll-receipt-dialog',
            data: {
                pid: this.pid
            }
        })
        //window.print();
        this.dialogRef.afterClosed()
    }


}

export class FilesDataSource extends DataSource<any>
{
    invoices2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        var result = Observable.fromPromise(this.db.getDiagnosis_reports());
        result.subscribe(x => console.log(x), e => console.error(e));
        return result;
    }

    disconnect() {
    }
}