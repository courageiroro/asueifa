export interface Diagnosis_report {
    id: string,
    rev:string,
    report_type: string,
    document_type: string,
    file_name: string,
    prescription_id: string,
    description: string,
    timestamp: Date,
    laboratorist_id: string,
    name_id:string,
    patient_id:string,
    patient_name:string,
    prescription_name:string
}

