import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Diagnosis_reportsService } from './diagnosis_reports.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-diagnosis_reports',
    templateUrl  : './diagnosis_reports.component.html',
    styleUrls    : ['./diagnosis_reports.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FuseDiagnosis_reportsComponent implements OnInit
{
    hasSelectedDiagnosis_reports: boolean;
    searchInput: FormControl;

    constructor(private diagnosis_reportsService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {
       
        this.diagnosis_reportsService.onSelectedDiagnosis_reportsChanged
            .subscribe(selectedDiagnosis_reports => {
                this.hasSelectedDiagnosis_reports = selectedDiagnosis_reports.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.diagnosis_reportsService.onSearchTextChanged.next(searchText);
            });
            
    }

}
