import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseDiagnosis_reportsComponent } from './diagnosis_reports.component';
import { Diagnosis_reportsService } from './diagnosis_reports.service';
import { FuseDiagnosis_reportsDiagnosis_reportListComponent } from './diagnosis_report-list/diagnosis_report-list.component';
import { FuseDiagnosis_reportsSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseDiagnosis_reportsDiagnosis_reportFormDialogComponent } from './diagnosis_report-form/diagnosis_report-form.component';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import {StaffsService} from './../staffs/staffs.service';
import {PrescriptionsService} from './../prescriptions/prescriptions.service';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/diagnosis_reports',
        component: FuseDiagnosis_reportsComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            diagnosis_reports: Diagnosis_reportsService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes),
        FroalaEditorModule.forRoot(),
         FroalaViewModule.forRoot()
    ],
    declarations   : [
        FuseDiagnosis_reportsComponent,
        FuseDiagnosis_reportsDiagnosis_reportListComponent,
        FuseDiagnosis_reportsSelectedBarComponent,
        FuseDiagnosis_reportsDiagnosis_reportFormDialogComponent
    ],
    providers      : [
        Diagnosis_reportsService,
        StaffsService,
        PouchService,
        AuthGuard,
        PrescriptionsService
    ],
    entryComponents: [FuseDiagnosis_reportsDiagnosis_reportFormDialogComponent]
})
export class FuseDiagnosis_reportsModule
{
}
