/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Diagnosis_report } from './diagnosis_report.model';
import { Schema } from '../schema';

@Injectable()
export class Diagnosis_reportsService {
    public diagnosis_report = "";
    public hide = "";
    public sCredentials;
    onDiagnosis_reportsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedDiagnosis_reportsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    diagnosis_reports: Diagnosis_report[];
    user: any;
    selectedDiagnosis_reports: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The diagnosis_reports App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getDiagnosis_reports()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getDiagnosis_reports();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getDiagnosis_reports();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('abayelsa', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/abayelsa';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/abayelsa';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }
    /***********
     * diagnosis_report
     **********/
    /**
     * Save a diagnosis_report
     * @param {diagnosis_report} diagnosis_report
     *
     * @return Promise<diagnosis_report>
     */
    saveDiagnosis_report(diagnosis_report: Diagnosis_report): Promise<Diagnosis_report> {
        diagnosis_report.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('diagnosis_report', diagnosis_report)
            .then((data: any) => {

                if (data && data.diagnosis_reports && data.diagnosis_reports[0]) {

                    return data.diagnosis_reports[0];
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a diagnosis_report
    * @param {diagnosis_report} diagnosis_report
    *
    * @return Promise<diagnosis_report>
    */
    updateDiagnosis_report(diagnosis_report: Diagnosis_report) {

        return this.db.rel.save('diagnosis_report', diagnosis_report)
            .then((data: any) => {
                if (data && data.diagnosis_reports && data.diagnosis_reports
                [0]) {
                    return data.diagnosis_reports[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a diagnosis_report
     * @param {diagnosis_report} diagnosis_report
     *
     * @return Promise<boolean>
     */
    removedDiagnosis_report(diagnosis_report: Diagnosis_report): Promise<boolean> {
        return this.db.rel.del('diagnosis_report', diagnosis_report)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the diagnosis_reports
     *
     * @return Promise<Array<diagnosis_report>>
     */
    getDiagnosis_reports(): Promise<Array<Diagnosis_report>> {
        var localStorageItem = JSON.parse(localStorage.getItem('user'));
        return this.db.rel.find('diagnosis_report')
            .then((data: any) => {
                this.diagnosis_reports = data.diagnosis_reports;
                if (this.searchText && this.searchText !== '') {
                    this.diagnosis_reports = FuseUtils.filterArrayByString(this.diagnosis_reports, this.searchText);
                }
                //this.ondiagnosis_reportsChanged.next(this.diagnosis_reports);
                var userId = localStorageItem.id
                this.diagnosis_reports = this.diagnosis_reports.filter(data => data.name_id == userId)
                
                return Promise.resolve(this.diagnosis_reports);
                //return data.diagnosis_reports ? data.diagnosis_reports : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getLaboratoristdiagnosis_reports(): Promise<Array<Diagnosis_report>> {
        return this.db.rel.find('diagnosis_report')
            .then((data: any) => {
                return data.diagnosis_reports ? data.diagnosis_reports : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a diagnosis_report
     * @param {diagnosis_report} diagnosis_report
     *
     * @return Promise<diagnosis_report>
     */
    getDiagnosis_report(diagnosis_report: Diagnosis_report): Promise<Diagnosis_report> {
        return this.db.rel.find('diagnosis_report', diagnosis_report.id)
            .then((data: any) => {
                
                return data && data.diagnosis_reports ? data.diagnosis_reports[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

     getDiagnosis_report2(id): Promise<Diagnosis_report> {
        return this.db.rel.find('diagnosis_report', id)
            .then((data: any) => {
                return data && data.diagnosis_reports ? data.diagnosis_reports[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected diagnosis_report by id
     * @param id
     */
    toggleSelectedDiagnosis_report(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedDiagnosis_reports.length > 0) {
            const index = this.selectedDiagnosis_reports.indexOf(id);

            if (index !== -1) {
                this.selectedDiagnosis_reports.splice(index, 1);

                // Trigger the next event
                this.onSelectedDiagnosis_reportsChanged.next(this.selectedDiagnosis_reports);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedDiagnosis_reports.push(id);

        // Trigger the next event
        this.onSelectedDiagnosis_reportsChanged.next(this.selectedDiagnosis_reports);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedDiagnosis_reports.length > 0) {
            this.deselectDiagnosis_reports();
        }
        else {
            this.selectDiagnosis_reports();
        }
    }

    selectDiagnosis_reports(filterParameter?, filterValue?) {
        this.selectedDiagnosis_reports = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedDiagnosis_reports = [];
            this.diagnosis_reports.map(diagnosis_report => {
                this.selectedDiagnosis_reports.push(diagnosis_report.id);
            });
        }
        else {
            /* this.selecteddiagnosis_reports.push(...
                 this.diagnosis_reports.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedDiagnosis_reportsChanged.next(this.selectedDiagnosis_reports);
    }





    deselectDiagnosis_reports() {
        this.selectedDiagnosis_reports = [];

        // Trigger the next event
        this.onSelectedDiagnosis_reportsChanged.next(this.selectedDiagnosis_reports);
    }

    deleteDiagnosis_report(diagnosis_report) {
        this.db.rel.del('diagnosis_report', diagnosis_report)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const diagnosis_reportIndex = this.diagnosis_reports.indexOf(diagnosis_report);
                this.diagnosis_reports.splice(diagnosis_reportIndex, 1);
                this.onDiagnosis_reportsChanged.next(this.diagnosis_reports);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedDiagnosis_reports() {
        for (const diagnosis_reportId of this.selectedDiagnosis_reports) {
            const diagnosis_report = this.diagnosis_reports.find(_diagnosis_report => {
                return _diagnosis_report.id === diagnosis_reportId;
            });

            this.db.rel.del('diagnosis_report', diagnosis_report)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const diagnosis_reportIndex = this.diagnosis_reports.indexOf(diagnosis_report);
                    this.diagnosis_reports.splice(diagnosis_reportIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onDiagnosis_reportsChanged.next(this.diagnosis_reports);
        this.deselectDiagnosis_reports();
    }
}
