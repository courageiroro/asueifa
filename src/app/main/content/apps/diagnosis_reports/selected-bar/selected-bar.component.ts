import { Component, OnInit } from '@angular/core';
import { Diagnosis_reportsService } from '../diagnosis_reports.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseDiagnosis_reportsSelectedBarComponent implements OnInit
{
    selectedDiagnosis_reports: string[];
    hasSelectedDiagnosis_reports: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private diagnosis_reportsService: PouchService,
        public dialog: MdDialog
    )
    {
        this.diagnosis_reportsService.onSelectedDiagnosis_reportsChanged
            .subscribe(selectedDiagnosis_reports => {
                this.selectedDiagnosis_reports = selectedDiagnosis_reports;
                setTimeout(() => {
                    this.hasSelectedDiagnosis_reports = selectedDiagnosis_reports.length > 0;
                    this.isIndeterminate = (selectedDiagnosis_reports.length !== this.diagnosis_reportsService.diagnosis_reports.length && selectedDiagnosis_reports.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.diagnosis_reportsService.selectDiagnosis_reports();
    }

    deselectAll()
    {
        this.diagnosis_reportsService.deselectDiagnosis_reports();
    }

    deleteSelectedDiagnosis_reports()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected diagnosis reports?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.diagnosis_reportsService.deleteSelectedDiagnosis_reports();
            }
            this.confirmDialogRef = null;
        });
    }

}
