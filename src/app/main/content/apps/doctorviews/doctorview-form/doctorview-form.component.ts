import { Component, Inject, OnInit, ViewEncapsulation,TemplateRef, ViewChild } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Payment } from '../doctorview.model';
import { PaymentsService } from '../doctorviews.service';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-doctorviews-doctorview-form-dialog',
    templateUrl: './doctorview-form.component.html',
    styleUrls: ['./doctorview-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseDoctorviewsDoctorviewFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    paymentForm: FormGroup;
    action: string;
    payment: Payment;
    randomNumber:any

    constructor(
        public dialogRef: MdDialogRef<FuseDoctorviewsDoctorviewFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        public db:PouchService
    ) {
        this.randomNumber = this.db.getNumber();
        
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Bus Type';
            this.payment = data.payment;
        }
        else {
            this.dialogTitle = 'New Bus Type';
            this.payment = {
                id: '',
                rev: '',
                type: '',
                amount: '',
                title: '',
                description: '',
                payment_method: '',
                invoice_number: Math.floor((Math.random() * 90000) + 10000),
                timestamp: '',
            }
        }

        this.paymentForm = this.createPaymentForm();
    }

    ngOnInit() {
    }

    createPaymentForm() {
        return this.formBuilder.group({
            id: [this.payment.id],
            rev: [this.payment.rev],
            type: [this.payment.type],
            amount: [this.payment.amount],
            title: [this.payment.title],
            description: [this.payment.description],
            payment_method: [this.payment.payment_method],
            invoice_number: ({
                value: this.payment.invoice_number,
                disabled: true,

            }),
            timestamp: [this.payment.timestamp],

        });
    }
}
