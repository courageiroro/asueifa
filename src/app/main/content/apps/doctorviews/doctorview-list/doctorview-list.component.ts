import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { StaffsService } from '../../staffs/staffs.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { FuseDoctorviewsDoctorviewFormDialogComponent } from '../doctorview-form/doctorview-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Staff } from '../../staffs/staff.model';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {PouchService} from '../../../../../provider/pouch-service';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';


@Component({
    selector: 'fuse-doctorviews-doctorview-list',
    templateUrl: './doctorview-list.component.html',
    styleUrls: ['./doctorview-list.component.scss']
})
export class FuseDoctorviewsDoctorviewListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public staffs: Array<Staff> = [];
    staffs2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['name', 'department', 'buttons'];
    selectedStaffs: any[];
    checkboxes: {};
    randomnumber:any

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;


    constructor(public dialog: MdDialog, public db: PouchService, public router:Router)
    { }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        this._loadPayments();
    }

    private _loadPayments(): void {
        this.db.onStaffsChanged.subscribe(data => {
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        this.db.getDStaffs()
            .then((staffs: Array<Staff>) => {
                this.staffs = staffs;
                this.staffs2 = new BehaviorSubject<any>(staffs);

                this.checkboxes = {};
                staffs.map(staff => {
                    this.checkboxes[staff.id] = false;
                });
                this.db.onSelectedStaffsChanged.subscribe(selectedStaffs => {
                    for (const id in this.checkboxes) {
                        this.checkboxes[id] = selectedStaffs.includes(id);
                    }

                    this.selectedStaffs = selectedStaffs;
                });
                this.db.onSearchTextChanged.subscribe(data => {
                    this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                })
            });

    }

   
    newPayment() {
        this.dialogRef = this.dialog.open(FuseDoctorviewsDoctorviewFormDialogComponent, {
            panelClass: 'payment-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }

                this.db.saveStaff(response.getRawValue());
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

            });
    }

    editPayment(payment) {
        this.dialogRef = this.dialog.open(FuseDoctorviewsDoctorviewFormDialogComponent, {
            panelClass: 'payment-form-dialog',
            data: {
                payment: payment,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':

                        this.db.updateStaff(formData.getRawValue());
                        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deletePayment(payment);

                        break;
                }
            });
    }

    /**
     * Delete payment
     */
    deletePayment(payment) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deleteStaff(payment);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(paymentId) {
        this.db.toggleSelectedStaff(paymentId);
    }

     viewDoctor(staff){
        
        this.router.navigate(['/apps/doctorpages', staff.id]);
    }


}

export class FilesDataSource extends DataSource<any>
{
    payments2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            of(this.db.staffs).delay(5000),
            this.sort.mdSortChange,
            this._paginator.page,
        ];

        var result = Observable.fromPromise(this.db.getDStaffs());
      /*   result.subscribe(x => console.log(x), e => console.error(e));
        return result; */

        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.staffs]));
        });
    }

    disconnect() {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
          return data;
        }
    
        return data.sort((a, b) => {
          const isAsc = this.sort.direction === 'asc';
          switch (this.sort.active) {
            case 'name': return compare(a.name, b.name, isAsc);
            case 'amount': return compare(+a.amount, +b.amount, isAsc);
            case 'id': return compare(+a.id, +b.id, isAsc);
            default: return 0;
          }
        });
      }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
