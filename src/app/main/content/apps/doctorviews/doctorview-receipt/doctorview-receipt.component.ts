import { Component, Inject, OnInit, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { StaffsService } from '../../staffs/staffs.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-doctorview-receipts-doctorview-receipt-form-dialog',
    templateUrl: './doctorview-receipt.component.html',
    styleUrls: ['./doctorview-receipt.component.scss'],
    encapsulation: ViewEncapsulation.None,
    outputs: ['childEvent']
})

export class FuseDoctorviewreceiptsDoctorviewreceiptFormDialogComponent implements OnInit {

    event: CalendarEvent;
    dialogTitle: string;
    doctorviewreceiptForm: FormGroup;
    childEvent = new EventEmitter<string>();
    action: string;
    doctorName: any;
    doctorAddress: any;
    doctorDepartment: any;
    doctorPhone: any;
    doctorProfile: any;
    doctorProfileSplit: any;
    Pid;

    constructor(
        public dialogRef: MdDialogRef<FuseDoctorviewreceiptsDoctorviewreceiptFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        public db2: PouchService,
        public router: Router,
        public activateRoute: ActivatedRoute
    ) {
        this.Pid = data.pid
        this.dialogTitle = 'Doctors Profile';
        /* 
                if (this.action === 'edit') {
                    this.dialogTitle = 'Edit doctorview-receipt';
                    this.doctorview-receipt = data.doctorview-receipt;
                }
                else {
                    this.dialogTitle = 'New doctorview-receipt';
                    this.doctorview-receipt = {
                        id: '',
                        rev: '',
                        name: '',
                        description: '',
                        doctors: []
                    } */


        //this.doctorview-receiptForm = this.createdoctorview-receiptForm();
    }


    ngOnInit() {

        this.db2.getStaff2(this.Pid).then(res => {
            this.doctorName = res.name;
            this.doctorAddress = res.address;
            this.doctorDepartment = res.department_id;
            this.doctorPhone = res.phone;
            this.doctorProfile = res.profile;
            
            /*  this.doctorProfileSplit = this.doctorProfile.split("<p> "); */
            var url = URL.createObjectURL(res._attachments);
            
            var img = document.createElement('img');
            img.className = "image";
            img.src = url;
            
            document.getElementById('img4').appendChild(img).style.width = '100px';
            document.getElementById('img4').appendChild(img).style.height = '50px';
            document.getElementById('img4').appendChild(img).style.borderRadius = '50%';
        })
    }

    print() {

        window.print();

    }

    createdoctorviewreceiptForm() {
        /*  return this.formBuilder.group({
             id: [this.doctorview-receipt.id],
             rev: [this.doctorview-receipt.rev],
             name: [this.doctorview-receipt.name],
             description: [this.doctorview-receipt.description],
 
         });*/
    }
}
