export interface Payment {
    id: string,
    rev: string,
    type: string,
    amount: string,
    title: string,
    description: string,
    payment_method: string,
    invoice_number: number,
    timestamp: string,
}

