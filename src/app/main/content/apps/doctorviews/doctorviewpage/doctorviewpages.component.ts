import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { StaffsService } from '../../staffs/staffs.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { MD_DIALOG_DATA, MdDialogRef, MdDialog } from '@angular/material';
import { FuseDoctorviewreceiptsDoctorviewreceiptFormDialogComponent } from '../doctorview-receipt/doctorview-receipt.component';
import { DomSanitizer } from '@angular/platform-browser';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-forms',
    templateUrl: './doctorviewpages.component.html',
    styleUrls: ['./doctorviewpages.component.scss']
})
export class FuseDoctorviewpagesComponent implements OnInit {
    form: FormGroup;
    formErrors: any;
    doctorName: any;
    doctorAddress: any;
    doctorDepartment: any;
    doctorPhone: any;
    doctorProfile: any;
    doctorProfileSplit: any;
    dialogRef;
    pid;
    image;

    constructor(private formBuilder: FormBuilder, public dialog: MdDialog, private db: PouchService, private router: Router, public activateRoute: ActivatedRoute,
                public _DomSanitizer: DomSanitizer
    ) {
        this.formErrors = {
            company: {},
            firstName: {},
            lastName: {},
            address: {},
            address2: {},
            city: {},
            state: {},
            postalCode: {}
        };
    }

    ngOnInit() {
        let id = this.activateRoute.snapshot.params['id'];
        this.pid = id;
        this.db.getStaff2(id).then(res => {
            this.doctorName = res.name;
            this.doctorAddress = res.address;
            this.doctorDepartment = res.department_id;
            this.doctorPhone = res.phone;
            this.doctorProfile = res.profile;

            this.image = res.image;
        
        })

        this.form = this.formBuilder.group({
            /*  company: [
                 {
                     value: 'Google',
                     disabled: true
                 }, Validators.required
             ], */
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            address: ['', Validators.required],
            address2: ['', Validators.required],
            city: ['', Validators.required],
            state: ['', Validators.required],
            postalCode: ['', [Validators.required, Validators.maxLength(5)]]
        });

        this.form.valueChanges.subscribe(() => {
            this.onFormValuesChanged();
        });
    }

    print() {
        this.dialogRef = this.dialog.open(FuseDoctorviewreceiptsDoctorviewreceiptFormDialogComponent, {
            panelClass: 'doctorview-receipt-dialog',
            data: {
                pid: this.pid
            }
        })
        //window.print();
        this.dialogRef.afterClosed()
    }

    onFormValuesChanged() {
        for (const field in this.formErrors) {
            if (!this.formErrors.hasOwnProperty(field)) {
                continue;
            }

            // Clear previous errors
            this.formErrors[field] = {};

            // Get the control
            const control = this.form.get(field);

            if (control && control.dirty && !control.valid) {
                this.formErrors[field] = control.errors;
            }
        }
    }
}
