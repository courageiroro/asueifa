import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { StaffsService } from '../../staffs/staffs.service';
import {FuseDoctorviewreceiptsDoctorviewreceiptFormDialogComponent} from '../doctorview-receipt/doctorview-receipt.component';
import {PouchService} from '../../../../../provider/pouch-service';
import { FuseDoctorviewpagesComponent } from './doctorviewpages.component';

const routes: Routes = [
    {
        path     : 'apps/doctorpages/:id',
        component: FuseDoctorviewpagesComponent,
         children : [],
        resolve  : {
            staffs:  PouchService
        }
    }
];

@NgModule({
    imports     : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        FuseDoctorviewpagesComponent,
        FuseDoctorviewreceiptsDoctorviewreceiptFormDialogComponent
    ],
    providers      : [
        StaffsService,
        PouchService
    ],
    entryComponents: [FuseDoctorviewpagesComponent, FuseDoctorviewreceiptsDoctorviewreceiptFormDialogComponent]
})
export class FuseDoctorviewpagesModule
{
}
