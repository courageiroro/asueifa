import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { StaffsService } from './../staffs/staffs.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-doctorviews',
    templateUrl  : './doctorviews.component.html',
    styleUrls    : ['./doctorviews.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FuseDoctorviewsComponent implements OnInit
{
    hasSelectedStaffs: boolean;
    searchInput: FormControl;

    constructor(private staffsService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.staffsService.onSelectedStaffsChanged
            .subscribe(selectedStaffs => {
                this.hasSelectedStaffs = selectedStaffs.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.staffsService.onSearchTextChanged.next(searchText);
            });
    }

}
