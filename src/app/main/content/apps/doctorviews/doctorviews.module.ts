import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseDoctorviewsComponent } from './doctorviews.component';
import { StaffsService } from './../staffs/staffs.service';
import {FuseDoctorviewsDoctorviewListComponent} from './doctorview-list/doctorview-list.component';
import { FuseDoctorviewsSelectedBarComponent } from './selected-bar/selected-bar.component';
import {FuseDoctorviewsDoctorviewFormDialogComponent } from './doctorview-form/doctorview-form.component';
import { PaymentsService } from './doctorviews.service';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/doctorviews',
        component: FuseDoctorviewsComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            staffs:  PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseDoctorviewsComponent,
        FuseDoctorviewsDoctorviewListComponent,
        FuseDoctorviewsSelectedBarComponent ,
        FuseDoctorviewsDoctorviewFormDialogComponent
    ],
    providers      : [
        StaffsService,
        AuthGuard,
        PouchService,
        PaymentsService
    ],
    entryComponents: [FuseDoctorviewsDoctorviewFormDialogComponent]
})
export class FuseDoctorsModule
{
}
