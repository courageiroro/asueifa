/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Component, Injectable, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Payment } from './doctorview.model';
import { Schema } from '../schema';

export interface sendNumber {
    number: string;
}

@Injectable()
export class PaymentsService {

    transferredData: sendNumber = { number: "number" };
    saveNumber(num) {
        this.transferredData.number = num;
        
    }

    getNumber() {
        
        return this.transferredData.number;
    }

    public paymentname = "";
    public sCredentials;
    onPaymentsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedPaymentsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    payments: Payment[];
    user: any;
    selectedPayments: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The payments App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getPayments()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getPayments();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getPayments();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('abayelsa', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/abayelsa';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/abayelsa';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }

    /***********
     * payment
     **********/
    /**
     * Save a payment
     * @param {payment} payment
     *
     * @return Promise<payment>
     */
    savePayment(payment: Payment): Promise<Payment> {
        payment.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('payment', payment)
            .then((data: any) => {

                if (data && data.payments && data.payments
                [0]) {
                    return data.payments[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a payment
    * @param {payment} payment
    *
    * @return Promise<payment>
    */
    updatePayment(payment: Payment) {

        return this.db.rel.save('payment', payment)
            .then((data: any) => {
                if (data && data.payments && data.payments
                [0]) {
                    return data.payments[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a payment
     * @param {payment} payment
     *
     * @return Promise<boolean>
     */
    removePayment(payment: Payment): Promise<boolean> {
        return this.db.rel.del('payment', payment)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the payments
     *
     * @return Promise<Array<payment>>
     */
    getPayments(): Promise<Array<Payment>> {
        return this.db.rel.find('payment')
            .then((data: any) => {
                this.payments = data.payments;
                if (this.searchText && this.searchText !== '') {
                    this.payments = FuseUtils.filterArrayByString(this.payments, this.searchText);
                }
                //this.onpaymentsChanged.next(this.payments);
                return Promise.resolve(this.payments);
                //return data.payments ? data.payments : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a payment
     * @param {payment} payment
     *
     * @return Promise<payment>
     */
    getPayment(payment: Payment): Promise<Payment> {
        return this.db.rel.find('payment', payment.id)
            .then((data: any) => {
                return data && data.payments ? data.payments[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected payment by id
     * @param id
     */
    toggleSelectedPayment(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedPayments.length > 0) {
            const index = this.selectedPayments.indexOf(id);

            if (index !== -1) {
                this.selectedPayments.splice(index, 1);

                // Trigger the next event
                this.onSelectedPaymentsChanged.next(this.selectedPayments);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedPayments.push(id);

        // Trigger the next event
        this.onSelectedPaymentsChanged.next(this.selectedPayments);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedPayments.length > 0) {
            this.deselectPayments();
        }
        else {
            this.selectPayments();
        }
    }

    selectPayments(filterParameter?, filterValue?) {
        this.selectedPayments = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedPayments = [];
            this.payments.map(payment => {
                this.selectedPayments.push(payment.id);
            });
        }
        else {
            /* this.selectedpayments.push(...
                 this.payments.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedPaymentsChanged.next(this.selectedPayments);
    }





    deselectPayments() {
        this.selectedPayments = [];

        // Trigger the next event
        this.onSelectedPaymentsChanged.next(this.selectedPayments);
    }

    deletePayment(payment) {
        this.db.rel.del('payment', payment)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const paymentIndex = this.payments.indexOf(payment);
                this.payments.splice(paymentIndex, 1);
                this.onPaymentsChanged.next(this.payments);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedPayments() {
        for (const paymentId of this.selectedPayments) {
            const payment = this.payments.find(_payment => {
                return _payment.id === paymentId;
            });

            this.db.rel.del('payment', payment)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const paymentIndex = this.payments.indexOf(payment);
                    this.payments.splice(paymentIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onPaymentsChanged.next(this.payments);
        this.deselectPayments();
    }
}
