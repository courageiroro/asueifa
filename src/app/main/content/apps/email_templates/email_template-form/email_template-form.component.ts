import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Email_template } from '../email_template.model';
import { PatientsService } from '../../patients/patients.service';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-email_templates-email_template-form-dialog',
    templateUrl: './email_template-form.component.html',
    styleUrls: ['./email_template-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseEmail_templatesEmail_templateFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    email_templateForm: FormGroup;
    action: string;
    email_template: Email_template;
    public patientss;

    constructor(
        public dialogRef: MdDialogRef<FuseEmail_templatesEmail_templateFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        public db: PouchService 
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Email Template';
            this.email_template = data.email_template;
        }
        else {
            this.dialogTitle = 'New Email Template';
            this.email_template = {
                id:'',
                rev: '',
                patient_email:'',
                subject:'',
                body:'',
                sender_email:''
            }
        }

        db.getPatients().then(res=>{
            this.patientss = res;
        })

        this.email_templateForm = this.createEmail_templateForm();
    }

    ngOnInit() {
    }

    createEmail_templateForm() {
        return this.formBuilder.group({
            id: [this.email_template.id],
            rev: [this.email_template.rev],
            subject: [this.email_template.subject],
            body: [this.email_template.body],
            patient_email: [this.email_template.patient_email],
            sender_email: [this.email_template.sender_email],

        });
    }
}
