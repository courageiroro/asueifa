import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Email_templatesService } from '../email_templates.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject} from 'rxjs/Rx';
import { FuseEmail_templatesEmail_templateFormDialogComponent } from '../email_template-form/email_template-form.component';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Email_template } from '../email_template.model';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-email_templates-email_template-list',
    templateUrl: './email_template-list.component.html',
    styleUrls  : ['./email_template-list.component.scss']
})
export class FuseEmail_templatesEmail_templateListComponent implements OnInit
{
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public email_templates: Array<Email_template> = [];
    email_templates2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'patient','subject','body','buttons'];
    selectedEmail_templates: any[];
    checkboxes: {};

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(public dialog: MdDialog, public db:PouchService)
    { }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db);
        this._loadEmail_templates();
    }

    private _loadEmail_templates(): void {
         this.db.onEmail_templatesChanged.subscribe(data =>{
            this.dataSource = new FilesDataSource(this.db);
        })
        this.db.getEmail_templates()
        .then((email_templates: Array<Email_template>) => {
            this.email_templates = email_templates;
            this.email_templates2= new BehaviorSubject<any>(email_templates);
            //console.log(this.email_templates2);

            this.checkboxes = {};
            email_templates.map(email_template => {
                this.checkboxes[email_template.id] = false;
            });
             this.db.onSelectedEmail_templatesChanged.subscribe(selectedEmail_templates => {
                for ( const id in this.checkboxes )
                {
                    this.checkboxes[id] = selectedEmail_templates.includes(id);
                }

                this.selectedEmail_templates = selectedEmail_templates;
            });
        this.db.onSearchTextChanged.subscribe(data =>{
            this.dataSource = new FilesDataSource(this.db);
        })
        });
        
    }

    newEmail_template()
    {
        this.dialogRef = this.dialog.open(FuseEmail_templatesEmail_templateFormDialogComponent, {
            panelClass: 'email_template-form-dialog',
            data      : {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if ( !response )
                {
                    return;
                }

                this.db.saveEmail_template(response.getRawValue());
                this.dataSource = new FilesDataSource(this.db);

            });

    }

    editEmail_template(email_template)
    {
        this.dialogRef = this.dialog.open(FuseEmail_templatesEmail_templateFormDialogComponent, {
            panelClass: 'email_template-form-dialog',
            data      : {
                email_template: email_template,
                action : 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch ( actionType )
                {
                    /**
                     * Save
                     */
                    case 'save':

                        this.db.updateEmail_template(formData.getRawValue());
                        this.dataSource = new FilesDataSource(this.db);

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteEmail_template(email_template);

                        break;
                }
            });
    }

    /**
     * Delete email_template
     */
    deleteEmail_template(email_template)
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.db.deleteEmail_template(email_template);
                this.dataSource = new FilesDataSource(this.db);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(email_templateId)
    {
        this.db.toggleSelectedEmail_template(email_templateId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    email_templates2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService)
    {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]>
    {
        var result = Observable.fromPromise(this.db.getEmail_templates());
        result.subscribe(x => console.log(x), e => console.error(e));
        return result;    
    }

    disconnect()
    {
    }
}
