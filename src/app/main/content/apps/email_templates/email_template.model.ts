export interface Email_template {
    id: string,
    rev: string,
    patient_email: string,
    subject: string,
    body: string,
    sender_email: string
}

