import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Email_templatesService } from './email_templates.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-email_templates',
    templateUrl  : './email_templates.component.html',
    styleUrls    : ['./email_templates.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FuseEmail_templatesComponent implements OnInit
{
    hasSelectedEmail_templates: boolean;
    searchInput: FormControl;

    constructor(private email_templatesService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.email_templatesService.onSelectedEmail_templatesChanged
            .subscribe(selectedEmail_templates => {
                this.hasSelectedEmail_templates = selectedEmail_templates.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.email_templatesService.onSearchTextChanged.next(searchText);
            });
    }

}
