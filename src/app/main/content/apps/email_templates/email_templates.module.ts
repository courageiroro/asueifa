import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseEmail_templatesComponent } from './email_templates.component';
import { Email_templatesService } from './email_templates.service';
import { FuseEmail_templatesEmail_templateListComponent } from './email_template-list/email_template-list.component';
import { FuseEmail_templatesSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseEmail_templatesEmail_templateFormDialogComponent } from './email_template-form/email_template-form.component';
import { PatientsService } from './../patients/patients.service';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : '**',
        component: FuseEmail_templatesComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            email_templates: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseEmail_templatesComponent,
        FuseEmail_templatesEmail_templateListComponent,
        FuseEmail_templatesSelectedBarComponent,
        FuseEmail_templatesEmail_templateFormDialogComponent
    ],
    providers      : [
        Email_templatesService,
        PouchService,
        AuthGuard,
        PatientsService
    ],
    entryComponents: [FuseEmail_templatesEmail_templateFormDialogComponent]
})
export class FuseEmail_templatesModule
{
}
