/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Email_template } from './email_template.model';
import { Schema } from '../schema';

@Injectable()
export class Email_templatesService {
    public email_templatename = "";
    public sCredentials;
    onEmail_templatesChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedEmail_templatesChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    email_templates: Email_template[];
    user: any;
    selectedEmail_templates: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The email_templates App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getEmail_templates()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getEmail_templates();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getEmail_templates();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('abayelsa', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/abayelsa';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/abayelsa';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }

    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }

    /***********
     * email_template
     **********/
    /**
     * Save a email_template
     * @param {email_template} Email_template
     *
     * @return Promise<email_template>
     */
    saveEmail_template(email_template: Email_template): Promise<Email_template> {
        email_template.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('email_template', email_template)
            .then((data: any) => {
                //console.log(data);
                //console.log(email_template);
                if (data && data.email_templates && data.email_templates
                [0]) {
                    return data.email_templates[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a email_template
    * @param {email_template} Email_template
    *
    * @return Promise<Email_template>
    */
    updateEmail_template(email_template: Email_template) {

        return this.db.rel.save('email_template', email_template)
            .then((data: any) => {
                if (data && data.email_templates && data.email_templates
                [0]) {
                    return data.email_templates[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a email_template
     * @param {email_template} Email_template
     *
     * @return Promise<boolean>
     */
    removeEmail_template(email_template: Email_template): Promise<boolean> {
        return this.db.rel.del('email_template', email_template)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the email_templates
     *
     * @return Promise<Array<email_template>>
     */
    getEmail_templates(): Promise<Array<Email_template>> {
        return this.db.rel.find('email_template')
            .then((data: any) => {
                this.email_templates = data.email_templates;
                if (this.searchText && this.searchText !== '') {
                    this.email_templates = FuseUtils.filterArrayByString(this.email_templates, this.searchText);
                }
                //this.onemail_templatesChanged.next(this.email_templates);
                return Promise.resolve(this.email_templates);
                //return data.email_templates ? data.email_templates : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a email_template
     * @param {email_template} Email_template
     *
     * @return Promise<email_template>
     */
    getEmail_template(email_template: Email_template): Promise<Email_template> {
        return this.db.rel.find('email_template', email_template.id)
            .then((data: any) => {
                return data && data.email_templates ? data.email_templates[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected email_template by id
     * @param id
     */
    toggleSelectedEmail_template(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedEmail_templates.length > 0) {
            const index = this.selectedEmail_templates.indexOf(id);

            if (index !== -1) {
                this.selectedEmail_templates.splice(index, 1);

                // Trigger the next event
                this.onSelectedEmail_templatesChanged.next(this.selectedEmail_templates);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedEmail_templates.push(id);

        // Trigger the next event
        this.onSelectedEmail_templatesChanged.next(this.selectedEmail_templates);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedEmail_templates.length > 0) {
            this.deselectEmail_templates();
        }
        else {
            this.selectEmail_templates();
        }
    }

    selectEmail_templates(filterParameter?, filterValue?) {
        this.selectedEmail_templates = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedEmail_templates = [];
            this.email_templates.map(email_template => {
                this.selectedEmail_templates.push(email_template.id);
            });
        }
        else {
            /* this.selectedemail_templates.push(...
                 this.email_templates.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedEmail_templatesChanged.next(this.selectedEmail_templates);
    }





    deselectEmail_templates() {
        this.selectedEmail_templates = [];

        // Trigger the next event
        this.onSelectedEmail_templatesChanged.next(this.selectedEmail_templates);
    }

    deleteEmail_template(email_template) {
        this.db.rel.del('email_template', email_template)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const email_templateIndex = this.email_templates.indexOf(email_template);
                this.email_templates.splice(email_templateIndex, 1);
                this.onEmail_templatesChanged.next(this.email_templates);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedEmail_templates() {
        for (const email_templateId of this.selectedEmail_templates) {
            const email_template = this.email_templates.find(_email_template => {
                return _email_template.id === email_templateId;
            });

            this.db.rel.del('email_template', email_template)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const email_templateIndex = this.email_templates.indexOf(email_template);
                    this.email_templates.splice(email_templateIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onEmail_templatesChanged.next(this.email_templates);
        this.deselectEmail_templates();
    }
}
