import { Component, OnInit } from '@angular/core';
import { Email_templatesService } from '../email_templates.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseEmail_templatesSelectedBarComponent implements OnInit
{
    selectedEmail_templates: string[];
    hasSelectedEmail_templates: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private email_templatesService: PouchService,
        public dialog: MdDialog
    )
    {
        this.email_templatesService.onSelectedEmail_templatesChanged
            .subscribe(selectedEmail_templates => {
                this.selectedEmail_templates = selectedEmail_templates;
                setTimeout(() => {
                    this.hasSelectedEmail_templates = selectedEmail_templates.length > 0;
                    this.isIndeterminate = (selectedEmail_templates.length !== this.email_templatesService.email_templates.length && selectedEmail_templates.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.email_templatesService.selectEmail_templates();
    }

    deselectAll()
    {
        this.email_templatesService.deselectEmail_templates();
    }

    deleteSelectedEmail_templates()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected email_templatees?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.email_templatesService.deleteSelectedEmail_templates();
            }
            this.confirmDialogRef = null;
        });
    }

}
