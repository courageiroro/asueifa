import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Expense } from '../expense.model';

@Component({
    selector: 'fuse-expenses-expense-form-dialog',
    templateUrl: './expense-form.component.html',
    styleUrls: ['./expense-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseExpensesExpenseFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    expenseForm: FormGroup;
    action: string;
    expense: Expense;
    statuss: any;

    constructor(
        public dialogRef: MdDialogRef<FuseExpensesExpenseFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Expense';
            this.expense = data.expense;
        }
        else {
            this.dialogTitle = 'New Expense';
            this.expense = {
                id: '',
                rev: '',
                title: '',
                description: '',
                amount: '',
                date: new Date().toJSON().slice(0,10).replace(/-/g,'-'),
            }
        }

        this.expenseForm = this.createexpenseForm();
    }

    ngOnInit() {
         this.statuss = ['Available', 'Occupied']
    }

    createexpenseForm() {
        return this.formBuilder.group({
            id: [this.expense.id],
            rev: [this.expense.rev],
            title: [this.expense.title],
            description: [this.expense.description],
            amount: [this.expense.amount],
            date: new Date(this.expense.date).toJSON().slice(0,10).replace(/-/g,'-')

        });
    }
}
