import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ExpensesService } from '../expenses.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { FuseExpensesExpenseFormDialogComponent } from '../expense-form/expense-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Expense } from '../expense.model';
import {PouchService} from '../../../../../provider/pouch-service';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';

@Component({
    selector: 'fuse-expenses-expense-list',
    templateUrl: './expense-list.component.html',
    styleUrls: ['./expense-list.component.scss']
})
export class FuseExpensesExpenseListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public expenses: Array<Expense> = [];
    expenses2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'title', 'description', 'amount', 'date', 'buttons'];
    selectedExpenses: any[];
    checkboxes: {};

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;

    constructor(public dialog: MdDialog, public db: PouchService)
    { }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        this._loadexpenses();
    }

    private _loadexpenses(): void {
        this.db.onExpensesChanged.subscribe(data => {
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        this.db.getExpenses()
            .then((expenses: Array<Expense>) => {
                this.expenses = expenses;
                this.expenses2 = new BehaviorSubject<any>(expenses);
                //console.log(this.expenses2);

                this.checkboxes = {};
                expenses.map(expense => {
                    this.checkboxes[expense.id] = false;
                });
                this.db.onSelectedExpensesChanged.subscribe(selectedExpenses => {
                    for (const id in this.checkboxes) {
                        this.checkboxes[id] = selectedExpenses.includes(id);
                    }

                    this.selectedExpenses = selectedExpenses;
                });
                this.db.onSearchTextChanged.subscribe(data => {
                    this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                })
            });

    }

    newExpense() {
        this.dialogRef = this.dialog.open(FuseExpensesExpenseFormDialogComponent, {
            panelClass: 'expense-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
                var res = response.getRawValue();
                res.date = new Date(res.date).toISOString().substring(0, 10);
                this.db.saveExpense(res);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

            });

    }

    editExpense(expense) {
        this.dialogRef = this.dialog.open(FuseExpensesExpenseFormDialogComponent, {
            panelClass: 'expense-form-dialog',
            data: {
                expense: expense,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                var form = formData.getRawValue();
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':
                        form.date = new Date(form.date).toISOString().substring(0, 10);
                        this.db.updateExpense(form);
                        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteExpense(expense);

                        break;
                }
            });
    }

    /**
     * Delete expense
     */
    deleteExpense(expense) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deleteExpense(expense);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(expenseId) {
        this.db.toggleSelectedExpense(expenseId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    expenses2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            of(this.db.expenses).delay(5000),
            this.sort.mdSortChange,
            this._paginator.page,
        ];

        var result = Observable.fromPromise(this.db.getExpenses());
       /*  result.subscribe(x => console.log(x), e => console.error(e));
        return result; */

        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.expenses]));
        });
    }

    disconnect() {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
          return data;
        }
    
        return data.sort((a, b) => {
          const isAsc = this.sort.direction === 'asc';
          switch (this.sort.active) {
            case 'title': return compare(a.title, b.title, isAsc);
            case 'amount': return compare(+a.amount, +b.amount, isAsc);
            case 'id': return compare(+a.id, +b.id, isAsc);
            default: return 0;
          }
        });
      }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }