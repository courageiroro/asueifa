export interface Expense {
    id: string,
    rev: string,
    title: string,
    description: string,
    amount: string,
    date: any,
}

