import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ExpensesService } from './expenses.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-expenses',
    templateUrl  : './expenses.component.html',
    styleUrls    : ['./expenses.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FuseExpensesComponent implements OnInit
{
    hasSelectedExpenses: boolean;
    searchInput: FormControl;

    constructor(private expensesService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.expensesService.onSelectedExpensesChanged
            .subscribe(selectedExpenses => {
                this.hasSelectedExpenses = selectedExpenses.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.expensesService.onSearchTextChanged.next(searchText);
            });
    }

}
