import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseExpensesComponent } from './expenses.component';
import { ExpensesService } from './expenses.service';
import { FuseExpensesExpenseListComponent } from './expense-list/expense-list.component';
import { FuseExpensesSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseExpensesExpenseFormDialogComponent } from './expense-form/expense-form.component';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/expense',
        component: FuseExpensesComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            expenses: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseExpensesComponent,
        FuseExpensesExpenseListComponent,
        FuseExpensesSelectedBarComponent,
        FuseExpensesExpenseFormDialogComponent
    ],
    providers      : [
        ExpensesService,
        PouchService,
        AuthGuard
    ],
    entryComponents: [FuseExpensesExpenseFormDialogComponent]
})
export class FuseExpensesModule
{
}
