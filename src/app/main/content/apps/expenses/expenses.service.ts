/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Expense } from './expense.model';
import { Schema } from '../schema';

@Injectable()
export class ExpensesService {
    public expense = "";
    public sCredentials;
    onExpensesChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedExpensesChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    expenses: Expense[];
    user: any;
    selectedExpenses: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The expenses App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getExpenses()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getExpenses();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getExpenses();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('abayelsa', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/abayelsa';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/abayelsa';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }


    /***********
     * expense
     **********/
    /**
     * Save a expense
     * @param {expense} expense
     *
     * @return Promise<expense>
     */
    saveExpense(expense: Expense): Promise<Expense> {
        expense.id = Math.floor(Date.now()).toString();
        //expense.driverslicenses = [];
        return this.db.rel.save('expense', expense)
            .then((data: any) => {
                //console.log(data);
                //console.log(expense);
                if (data && data.expenses && data.expenses
                [0]) {
                    return data.expenses[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a expense
    * @param {expense} Expense
    *
    * @return Promise<expense>
    */
    updateExpense(expense: Expense) {
        return this.db.rel.save('expense', expense)
            .then((data: any) => {
                if (data && data.expenses && data.expenses
                [0]) {
                    return data.expenses[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a expense
     * @param {expense} Expense
     *
     * @return Promise<boolean>
     */
    removeExpense(expense: Expense): Promise<boolean> {
        return this.db.rel.del('expense', expense)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the expenses
     *
     * @return Promise<Array<expense>>
     */
    getExpenses(): Promise<Array<Expense>> {
        return this.db.rel.find('expense')
            .then((data: any) => {
                this.expenses = data.expenses;
                if (this.searchText && this.searchText !== '') {
                    this.expenses = FuseUtils.filterArrayByString(this.expenses, this.searchText);
                }
                //this.onexpensesChanged.next(this.expenses);
                return Promise.resolve(this.expenses);
                //return data.expenses ? data.expenses : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a expense
     * @param {expense} expense
     *
     * @return Promise<expense>
     */
    getexpense(expense: Expense): Promise<Expense> {
        return this.db.rel.find('expense', expense.id)
            .then((data: any) => {
                return data && data.expenses ? data.expenses[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected expense by id
     * @param id
     */
    toggleSelectedExpense(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedExpenses.length > 0) {
            const index = this.selectedExpenses.indexOf(id);

            if (index !== -1) {
                this.selectedExpenses.splice(index, 1);

                // Trigger the next event
                this.onSelectedExpensesChanged.next(this.selectedExpenses);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedExpenses.push(id);

        // Trigger the next event
        this.onSelectedExpensesChanged.next(this.selectedExpenses);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedExpenses.length > 0) {
            this.deselectExpenses();
        }
        else {
            this.selectExpenses();
        }
    }

    selectExpenses(filterParameter?, filterValue?) {
        this.selectedExpenses = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedExpenses = [];
            this.expenses.map(expense => {
                this.selectedExpenses.push(expense.id);
            });
        }
        else {
            /* this.selectedexpenses.push(...
                 this.expenses.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedExpensesChanged.next(this.selectedExpenses);
    }





    deselectExpenses() {
        this.selectedExpenses = [];

        // Trigger the next event
        this.onSelectedExpensesChanged.next(this.selectedExpenses);
    }

    deleteExpense(expense) {
        this.db.rel.del('expense', expense)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const expenseIndex = this.expenses.indexOf(expense);
                this.expenses.splice(expenseIndex, 1);
                this.onExpensesChanged.next(this.expenses);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedExpenses() {
        for (const expenseId of this.selectedExpenses) {
            const expense = this.expenses.find(_expense => {
                return _expense.id === expenseId;
            });

            this.db.rel.del('expense', expense)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const expenseIndex = this.expenses.indexOf(expense);
                    this.expenses.splice(expenseIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onExpensesChanged.next(this.expenses);
        this.deselectExpenses();
    }
}
