import { Component, OnInit } from '@angular/core';
import { ExpensesService } from '../expenses.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseExpensesSelectedBarComponent implements OnInit
{
    selectedExpenses: string[];
    hasSelectedExpenses: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private expensesService: PouchService,
        public dialog: MdDialog
    )
    {
        this.expensesService.onSelectedExpensesChanged
            .subscribe(selectedExpenses => {
                this.selectedExpenses = selectedExpenses;
                setTimeout(() => {
                    this.hasSelectedExpenses = selectedExpenses.length > 0;
                    this.isIndeterminate = (selectedExpenses.length !== this.expensesService.expenses.length && selectedExpenses.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.expensesService.selectExpenses();
    }

    deselectAll()
    {
        this.expensesService.deselectExpenses();
    }

    deleteSelectedExpenses()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected Expenses?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.expensesService.deleteSelectedExpenses();
            }
            this.confirmDialogRef = null;
        });
    }

}
