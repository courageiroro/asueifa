import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Extendlicense } from '../extendlicense.model';
import { ExtendlicensesService } from '../extendlicenses.service';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-extendlicenses-extendlicense-form-dialog',
    templateUrl: './extendlicense-form.component.html',
    styleUrls: ['./extendlicense-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseExtendlicensesExtendlicenseFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    extendlicenseForm: FormGroup;
    action: string;
    extendlicense: Extendlicense;
    public categorys;
    public types;
    category;
   

    constructor(
        public dialogRef: MdDialogRef<FuseExtendlicensesExtendlicenseFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        private db: PouchService,
        
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit extendlicense';
            this.extendlicense = data.extendlicense;
        }
        else {
            this.dialogTitle = 'New extendlicense';
            this.extendlicense = {
                id: '',
                rev: '',               
                amount: null,
                product:'',
                type:'',
                method:'',
                date:new Date,
                expire: null,
                reference:'',
                status: false
            }
        }


        this.extendlicenseForm = this.createextendlicenseForm();
    }

    ngOnInit() {
        this.categorys = ['General', 'Medical History', 'Social History', 'Medical Conditions', 'Current Medications', 'Family History', 'Notes']
        this.types = ['input', 'text', 'checkbox']
    }


    createextendlicenseForm() {
        return this.formBuilder.group({
            id: [this.extendlicense.id],
            rev: [this.extendlicense.rev],
            amount: [this.extendlicense.amount],
            product: [this.extendlicense.product],
            type: [this.extendlicense.type],
            method: [this.extendlicense.method],
            date: [this.extendlicense.date],
            expire: [this.extendlicense.expire],
            reference: [this.extendlicense.reference],
            status: [this.extendlicense.status]
        });
    }
}
