import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ExtendlicensesService } from '../extendlicenses.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
//import { ConfigureextendlicensesService } from '../../configure_extendlicenses/configure_extendlicenses.service';
import { FuseExtendlicensesExtendlicenseFormDialogComponent } from '../extendlicense-form/extendlicense-form.component';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Extendlicense } from '../extendlicense.model';
import { Subject } from 'rxjs/Subject';
import { Staff } from '../../staffs/staff.model';
//import { Configureextendlicense } from '../../configure_extendlicenses/configure_extendlicense.model';
import { Http, Headers, RequestOptions } from '@angular/http';
import { StaffsService } from '../../staffs/staffs.service';
import { DomSanitizer } from '@angular/platform-browser';
//import { Angular4paystack } from './angular4-paystack';
import { SettingsService } from '../../settings/settings.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-extendlicenses-extendlicense-list',
    templateUrl: './extendlicense-list.component.html',
    styleUrls: ['./extendlicense-list.component.scss']
})
export class FuseExtendlicensesExtendlicenseListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public extendlicenses: Array<Extendlicense> = [];
    //public configureextendlicenses: Array<Configureextendlicense> = [];
    public staff: Staff;
    patients;
    patientName;
    patientAddress;
    patientPhone;
    patientEmail;
    settings;
    isstripe=false;
    cardinfo: any = {
    number: '',
    expMonth: '',
    expYear: '',
    cvc: ''
  }

    settingsEmail;
    error = "";
    urlextendlicense;
    public extendlicense: Extendlicense;
    image;
    checkboxstate;
    extendlicenses2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'name', 'description', 'buttons'];
    selectedextendlicenses: any[];
    online = true;
    checkboxes: {};
    general = [];
    refresh: Subject<any> = new Subject();
    medicalhistory = [];
    socialhistory = [];
    medicalconditions = [];
    currentmedications = [];
    familyhistory = [];
    notes = [];
    extendlicensess: Extendlicense[];

    product = {
        id: 'com.sarutech.pharmacy.sixmonth',
        name: '6 Months',
        amount: 5000,
        expire: new Date().setMonth(new Date().getMonth() + 6)
    };

    products = [
        {
            id: 'com.sarutech.pharmacy.sixmonth',
            name: '6 Months',
            amount: 10000,
            expire: new Date().setMonth(new Date().getMonth() + 6)
        },
        {
            id: 'com.sarutech.pharmacy.oneyear',
            name: '1 Year',
            amount: 20000,
            expire: new Date(new Date().getFullYear() + 1, new Date().getMonth(), new Date().getDate())
        },
        {
            id: 'com.sarutech.pharmacy.twoyears',
            name: '2 Years',
            amount: 30000,
            expire: new Date(new Date().getFullYear() + 2, new Date().getMonth(), new Date().getDate())
        },
        {
            id: 'com.sarutech.pharmacy.tenyears',
            name: '10 Years',
            amount: 50000,
            expire: new Date(new Date().getFullYear() + 10, new Date().getMonth(), new Date().getDate())
        },
        {
            id: 'com.sarutech.pharmacy.fiftyyears',
            name: '50 Years',
            amount: 100000,
            expire: new Date(new Date().getFullYear() + 50, new Date().getMonth(), new Date().getDate())
        }
    ];


    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(public dialog: MdDialog, public _DomSanitizer: DomSanitizer, public db: PouchService, public http: Http, public activateRoute: ActivatedRoute, public router: Router) {
        let id = this.activateRoute.snapshot.params['id'];
        console.log(id);




        this.db.getSettings().then(settings => {
            console.log(settings)
            this.settings = settings[0];
            this.settingsEmail = settings[0].email;
            if (this.settingsEmail == "") {
                this.error = "PayStack needs an Email Address. Setup your Email Address under settings and return here. Thanks";
                console.log(this.error);
            }
        });
        this.extendlicense = {
            id: 'hospital-' + Math.round((new Date()).getTime()).toString(),
            rev: '',
            amount: 5000,
            product: 'com.sarutech.hospital.sixmonth',
            method: 'paystack',
            date: new Date().getTime(),
            expire: null,
            reference: '',
            type: 'subscription',
            status: false
        }
       
    }

      loadExternalScript(scriptUrl: string) {
        return new Promise((resolve, reject) => {
          const scriptElement = document.createElement('script')
          scriptElement.src = scriptUrl
          scriptElement.onload = resolve
          document.body.appendChild(scriptElement)
      })
    }


    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db);
        this._loadextendlicenses();

         let newwindow: any;
            let mythis = this;
            this.loadExternalScript("https://www.paypalobjects.com/api/checkout.js").then((res) => {
            newwindow = window;    
                
                newwindow.paypal.Button.render({
                env: 'production',
                client: {
                    sandbox:    'AQLJkIT6ytAX3Vwz-AQqGb_Yin2wa8ClwnQ5gHv1Svd0APYj1VNxghT9M7ZfHuFYrfRTE17r_XyaSx7z',
                    production: 'AWmkmBfF3q_Eu5aqZyZ5cX3FW0F8y3SbkMdlN1xMSILyO6q4BWaj8peKkCFur3BTuKFapI0oWpwkljnS'
                },
                commit: true,
                payment: function (data, actions) {
                    return actions.payment.create({
                    payment: {
                        transactions: [
                        {
                            amount: { total: mythis.product.amount/100, currency: 'USD' }
                        }
                        ]
                    }
                    })
                },
                onAuthorize: function(data, actions) {
                    return actions.payment.execute().then(function(res) {
                        mythis.processPayment(mythis,res.id,'PAYPAL');
                    })
                }
                }, '#paypal-button-container');
            });
    }

    private _loadextendlicenses(): void {

    }


    /*    onlineCheck() {
           this.online = true;
           this.http.get('https://sarutech.com/couchdblogin/auth/validate-username/xs2da2432252sda').subscribe(data => {
               this.online = true;
               console.log(data)
               console.log(this.online);
           },
               (err) => {
                   this.online = false;
                   console.log(this.online);
               });
       }
   
       click() {
           this.onlineCheck();
       } */

    newExtendlicense() {
        this.dialogRef = this.dialog.open(FuseExtendlicensesExtendlicenseFormDialogComponent, {
            panelClass: 'extendlicense-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
                

                this.db.saveExtendlicense(response.getRawValue());
                this.refresh.next(true);
                this.dataSource = new FilesDataSource(this.db);
                

            });

    }

    editExtendlicense(extendlicense) {
        
        this.dialogRef = this.dialog.open(FuseExtendlicensesExtendlicenseFormDialogComponent, {
            panelClass: 'extendlicense-form-dialog',
            data: {
                extendlicense: extendlicense,
                action: 'edit'

            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':

                        this.db.updateExtendlicense(formData.getRawValue());
                        this.dataSource = new FilesDataSource(this.db);
                        
                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteExtendlicense(extendlicense);

                        break;
                }
            });
    }

    /**
     * Delete extendlicense
     */
    deleteExtendlicense(extendlicense) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deleteExtendlicense(extendlicense);
                this.dataSource = new FilesDataSource(this.db);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(extendlicenseId) {
        this.db.toggleSelectedExtendlicense(extendlicenseId);
    }
    navConsultancy() {
        
        this.router.navigate(['apps/consultancy', this.patients.id]);
    }
    click(event) {
        
        this.checkboxstate = event;
    }

       payStack($event){
        
        this.processPayment(this,$event.trxref,'PAYSTACK');
    }

    processPayment(mythis,reference, method){
        //ready to alert api
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        let response:any = {};
        response.reference = reference;
        this.extendlicense.reference = reference; //also set this here
        response.description = mythis.product.id;
        response.product = 'tailor';
        response.customer = mythis.settings.username;
        response.amount = mythis.product.amount*3; //in naira
        response.payment_method = method;
        response.type = 'payment';
        this.http.post('https://sarutech.com/apirest.php', JSON.stringify(response))
        .subscribe(res => {
            let result = res.json();
            
            //save payment
            
            this.extendlicense.amount =  this.product.amount*3; //in naira
            this.extendlicense.expire =  this.product.expire;
            this.extendlicense.product = this.product.id;
            this.extendlicense.method = method;
            this.extendlicense.status = true;
            this.db.saveExtendlicense(this.extendlicense).then(res =>{
                this.db.extendLicense(this.extendlicense.expire,this.product.id);
                //this.navCtrl.pop();
                this.router.navigate(['apps/managepayment'])
            });
        }, (err) => {
            console.log(err.json());
            //alert('The payment was not successful, please try again or use another payment method');
        });
    }


    enableStripe(){
        this.isstripe = true;
    }

      payStripe() {
   /*  this.stripe.setPublishableKey('pk_test_UGjs8wsGFq9nRgXHSHV1zsmz');
    this.stripe.createCardToken(this.cardinfo).then((token) => {
        this.processPayment(this,token,'STRIPE');
    }) */
    }

    paymentCancel() {
        console.log('Payment was Cancelled');
    }

     processOffline(){
        this.extendlicense.amount =  this.product.amount*3; //in naira
        this.extendlicense.expire =  this.product.expire;
        this.extendlicense.product = this.product.id;
        this.extendlicense.method = 'OFFLINE';
        this.extendlicense.status = false;
        this.db.saveExtendlicense(this.extendlicense).then(res =>{
          this.router.navigate(['apps/offline']);
        });
    }

    submit() {

    }

}

export class FilesDataSource extends DataSource<any>
{
    extendlicenses2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService) {
        super();
    }

    //Connect function called by the table to retrieve one stream containing the data to render.
    connect(): Observable<any[]> {
        var result = Observable.fromPromise(this.db.getExtendlicenses());
        result.subscribe(x => console.log(x), e => console.error(e));
        return result;
    }

    disconnect() {
    }
}
