import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ExtendlicensesService } from './extendlicenses.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';


@Component({
    selector: 'fuse-extendlicenses',
    templateUrl: './extendlicenses.component.html',
    styleUrls: ['./extendlicenses.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [Animations.slideInTop]
})
export class FuseExtendlicensesComponent implements OnInit {
    hasSelectedExtendlicenses: boolean;
    searchInput: FormControl;

    constructor(private extendlicensesService: PouchService) {
        this.searchInput = new FormControl('');
       
    }

    ngOnInit() {

        this.extendlicensesService.onSelectedExtendlicensesChanged
            .subscribe(selectedExtendlicenses => {
                this.hasSelectedExtendlicenses = selectedExtendlicenses.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.extendlicensesService.onSearchTextChanged.next(searchText);
            });
    }

}
