import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { Angular4PaystackModule } from 'angular4-paystack';
import { FuseExtendlicensesComponent } from './extendlicenses.component';
import { ExtendlicensesService } from './extendlicenses.service';
//import { UserService } from '../../../../user.service';
//import { ConfigureExtendlicensesService } from './../configure_extendlicenses/configure_extendlicenses.service';
import { FuseExtendlicensesExtendlicenseListComponent } from './extendlicense-list/extendlicense-list.component';
import { FuseExtendlicensesSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseExtendlicensesExtendlicenseFormDialogComponent } from './extendlicense-form/extendlicense-form.component';
import { StaffsService } from './../staffs/staffs.service';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/extendlicense',
        component: FuseExtendlicensesComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            extendlicenses: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes),
        Angular4PaystackModule
    ],
    declarations   : [
        FuseExtendlicensesComponent,
        FuseExtendlicensesExtendlicenseListComponent,
        FuseExtendlicensesSelectedBarComponent,
        FuseExtendlicensesExtendlicenseFormDialogComponent
    ],
    providers      : [
        ExtendlicensesService,
        StaffsService,
        PouchService,
        AuthGuard
        //ConfigureextendlicensesService
       //UserService
    ],
    entryComponents: [FuseExtendlicensesExtendlicenseFormDialogComponent]
})
export class FuseExtendlicensesModule
{
}
