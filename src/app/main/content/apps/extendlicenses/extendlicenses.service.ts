/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import { Setting } from 'app/main/content/apps/settings/setting.model';
declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Extendlicense } from './extendlicense.model';
import { Schema } from '../schema';

@Injectable()
export class ExtendlicensesService {
    public extendlicense = "";
    public sCredentials;
    onExtendlicensesChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedExtendlicensesChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    extendlicenses: Extendlicense[];
    user: any;
    selectedExtendlicenses: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;
    settings: Setting[];
    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The extendlicenses App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getExtendlicenses()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getExtendlicenses();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getExtendlicenses();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('abayelsa', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/abayelsa';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/abayelsa';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }


    /***********
     * extendlicense
     **********/
    /**
     * Save a extendlicense
     * @param {extendlicense} extendlicense
     *
     * @return Promise<extendlicense>
     */
    saveExtendlicense(extendlicense: Extendlicense): Promise<Extendlicense> {
        extendlicense.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('extendlicense', extendlicense)
            .then((data: any) => {

                if (data && data.extendlicenses && data.extendlicenses
                [0]) {
                    console.log('save');
                    
                    return data.extendlicenses[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    extendLicense(date,plan){
        
        this.getSettings().then(settings=>{
            settings[0].expiredate = new Date(date).toUTCString();
            settings[0].plans.push(plan);
            
            this.updateSetting(settings[0]);
        });
    }

      /**
     * Return all the settings
     *
     * @return Promise<Array<setting>>
     */
    getSettings(): Promise<Array<Setting>> {
        return this.db.rel.find('settings')
            .then((data: any) => {
                this.settings = data.settings;
                if (this.searchText && this.searchText !== '') {
                    this.settings = FuseUtils.filterArrayByString(this.settings, this.searchText);
                }
                //this.onSettingsChanged.next(this.settings);
                return Promise.resolve(this.settings);
                //return data.settings ? data.settings : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

      /**
    * Update a setting
    * @param {setting} Setting
    *
    * @return Promise<setting>
    */
    updateSetting(setting: Setting) {

        return this.db.rel.save('settings', setting)
            .then((data: any) => {
                if (data && data.settings && data.settings
                [0]) {
                    return data.settings[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a extendlicense
    * @param {extendlicense} Extendlicense
    *
    * @return Promise<extendlicense>
    */
    updateExtendlicense(extendlicense: Extendlicense) {
        return this.db.rel.save('extendlicense', extendlicense)
            .then((data: any) => {
                if (data && data.extendlicenses && data.extendlicenses
                [0]) {
                    console.log('Update')
                    
                    return data.extendlicenses[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a extendlicense
     * @param {extendlicense} extendlicense
     *
     * @return Promise<boolean>
     */
    removeExtendlicense(extendlicense: Extendlicense): Promise<boolean> {
        return this.db.rel.del('extendlicense', extendlicense)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the extendlicenses
     *
     * @return Promise<Array<extendlicense>>
     */
    getExtendlicenses(): Promise<Array<Extendlicense>> {
        return this.db.rel.find('extendlicense')
            .then((data: any) => {
                this.extendlicenses = data.extendlicenses;
                if (this.searchText && this.searchText !== '') {
                    this.extendlicenses = FuseUtils.filterArrayByString(this.extendlicenses, this.searchText);
                }
                //this.onextendlicensesChanged.next(this.extendlicenses);
                return Promise.resolve(this.extendlicenses);
                //return data.extendlicenses ? data.extendlicenses : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getDoctorExtendlicenses(): Promise<Array<Extendlicense>> {
        return this.db.rel.find('extendlicense')
            .then((data: any) => {
                return data.extendlicenses ? data.extendlicenses : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a extendlicense
     * @param {extendlicense} extendlicense
     *
     * @return Promise<extendlicense>
     */
    getExtendlicense(extendlicense: Extendlicense): Promise<Extendlicense> {
        return this.db.rel.find('extendlicense', extendlicense.id)
            .then((data: any) => {
                console.log("Get")
                
                return data && data.extendlicenses ? data.extendlicenses[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected extendlicense by id
     * @param id
     */
    toggleSelectedExtendlicense(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedExtendlicenses.length > 0) {
            const index = this.selectedExtendlicenses.indexOf(id);

            if (index !== -1) {
                this.selectedExtendlicenses.splice(index, 1);

                // Trigger the next event
                this.onSelectedExtendlicensesChanged.next(this.selectedExtendlicenses);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedExtendlicenses.push(id);

        // Trigger the next event
        this.onSelectedExtendlicensesChanged.next(this.selectedExtendlicenses);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedExtendlicenses.length > 0) {
            this.deselectExtendlicenses();
        }
        else {
            this.selectExtendlicenses();
        }
    }

    selectExtendlicenses(filterParameter?, filterValue?) {
        this.selectedExtendlicenses = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedExtendlicenses = [];
            this.extendlicenses.map(extendlicense => {
                this.selectedExtendlicenses.push(extendlicense.id);
            });
        }
        else {
            /* this.selectedextendlicenses.push(...
                 this.extendlicenses.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedExtendlicensesChanged.next(this.selectedExtendlicenses);
    }





    deselectExtendlicenses() {
        this.selectedExtendlicenses = [];

        // Trigger the next event
        this.onSelectedExtendlicensesChanged.next(this.selectedExtendlicenses);
    }

    deleteExtendlicense(extendlicense) {
        this.db.rel.del('extendlicense', extendlicense)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const extendlicenseIndex = this.extendlicenses.indexOf(extendlicense);
                this.extendlicenses.splice(extendlicenseIndex, 1);
                this.onExtendlicensesChanged.next(this.extendlicenses);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedExtendlicenses() {
        for (const extendlicenseId of this.selectedExtendlicenses) {
            const extendlicense = this.extendlicenses.find(_extendlicense => {
                return _extendlicense.id === extendlicenseId;
            });

            this.db.rel.del('extendlicense', extendlicense)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const extendlicenseIndex = this.extendlicenses.indexOf(extendlicense);
                    this.extendlicenses.splice(extendlicenseIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onExtendlicensesChanged.next(this.extendlicenses);
        this.deselectExtendlicenses();
    }
}
