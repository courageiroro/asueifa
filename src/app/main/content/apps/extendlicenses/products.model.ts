export interface Product {
    id: string,
    rev: string,
    name: string,
    description: string,
    amount: any,
    cost: any,
    quantity: number,
    quantity_alert: number,
    shelf: any,
    category: any,
    image: any,
    _attachments: any,
    purchases: Array<string>,
    sales: Array<string>,
    expire: any,
    supplier: string,
    expire_qty:number
}
