import { Component, OnInit } from '@angular/core';
import { ExtendlicensesService } from '../extendlicenses.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseExtendlicensesSelectedBarComponent implements OnInit
{
    selectedExtendlicenses: string[];
    hasSelectedExtendlicenses: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private extendlicensesService: PouchService,
        public dialog: MdDialog
    )
    {
        this.extendlicensesService.onSelectedExtendlicensesChanged
            .subscribe(selectedExtendlicenses => {
                this.selectedExtendlicenses = selectedExtendlicenses;
                setTimeout(() => {
                    this.hasSelectedExtendlicenses = selectedExtendlicenses.length > 0;
                    this.isIndeterminate = (selectedExtendlicenses.length !== this.extendlicensesService.extendlicenses.length && selectedExtendlicenses.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.extendlicensesService.selectExtendlicenses();
    }

    deselectAll()
    {
        this.extendlicensesService.deselectExtendlicenses();
    }

    deleteSelectedextendlicenses()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected extendlicenses?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.extendlicensesService.deleteSelectedExtendlicenses();
            }
            this.confirmDialogRef = null;
        });
    }

}
