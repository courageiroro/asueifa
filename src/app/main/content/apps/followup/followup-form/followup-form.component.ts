import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { followup } from '../followup.model';
import { antenatalsService } from '../../antenatal/antenatals.service';
import { antenatal} from '../../antenatal/antenatal.model';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-followups-followup-form-dialog',
    templateUrl: './followup-form.component.html',
    styleUrls: ['./followup-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FusefollowupsfollowupFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    followupForm: FormGroup;
    action: string;
    followup: followup;
    antenatalArray;
    statuss: any;
    antenatalObject;
    filteredAntenetals: Array<antenatal> = [];
    public antenetals: antenatal;

    constructor(
        public dialogRef: MdDialogRef<FusefollowupsfollowupFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        private db: PouchService
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Income Category';
            this.followup = data.followup;
        }
        else {
            this.dialogTitle = 'New Income Category';
            this.followup = {
                id: '',
                rev: '',
                antenatalid: '',
                date: new Date(),
                heightoffunds: '',
                pp: '',
                urine: '',
                bp: '',
                weight: '',
                hb: '',
                oedema: '',
                remarks: '',
                fheart: '',
                rpb: '',
                ga: '',
                nextvisit: new Date(),
                initialexaminer: ''
            }
        }

        this.followupForm = this.createfollowupForm();
    }

    ngOnInit() {
        this.db.getantenatals().then(res => {
          this.antenatalArray = [];
          this.antenatalArray = res;
        })
        this.statuss = ['Available', 'Occupied']
    }

    selectAntenatals(newVal) {
        console.log(newVal);
        this.antenatalObject = newVal;
        if (this.action == 'edit') {
          this.followup.antenatalid = newVal;
        }
        else {
            this.followup.antenatalid = newVal.id;
        }
      }
    

    createfollowupForm() {
        return this.formBuilder.group({
            id: [this.followup.id],
            rev: [this.followup.rev],
            antenatalid: [this.followup.antenatalid],
            date: [this.followup.date],
            heightoffunds: [this.followup.heightoffunds],
            pp: [this.followup.pp],
            urine: [this.followup.urine],
            bp: [this.followup.bp],
            weight: [this.followup.weight],
            hb: [this.followup.hb],
            oedema: [this.followup.oedema],
            remarks: [this.followup.remarks],
            fheart: [this.followup.fheart],
            rpb: [this.followup.rpb],
            ga: [this.followup.ga],
            nextvisit: [this.followup.nextvisit],
            initialexaminer: [this.followup.initialexaminer]
        });
    }
}
