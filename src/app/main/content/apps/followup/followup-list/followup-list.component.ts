import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { followupsService } from '../followups.service';
import { antenatalsService } from '../../antenatal/antenatals.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { FusefollowupsfollowupFormDialogComponent } from '../followup-form/followup-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { followup } from '../followup.model';
import {PouchService} from '../../../../../provider/pouch-service';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';

@Component({
    selector: 'fuse-followups-followup-list',
    templateUrl: './followup-list.component.html',
    styleUrls: ['./followup-list.component.scss']
})
export class FusefollowupsfollowupListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public followups: Array<followup> = [];
    followups2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'antenatalid', 'date', 'heightoffunds','pp', 'urine', 'bp', 'weight','hb', 'oedema', 'fheart', 'rbp','ga', 'nextvisit', 'initialexaminer', 'buttons'];
    selectedfollowups: any[];
    checkboxes: {};

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;

    constructor(public dialog: MdDialog, public db: PouchService) {

    }

    getfollowupnumber(gets) {
        var number = [gets.followup_number].join();
        return number;
    }


    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        this._loadfollowups();
    }

    private _loadfollowups(): void {
        this.db.onfollowupsChanged.subscribe(data => {
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        this.db.getfollowups()
            .then((followups: Array<followup>) => {
                this.followups = followups;
                this.followups2 = new BehaviorSubject<any>(followups);
                //console.log(this.followups2);

                this.checkboxes = {};
                followups.map(followup => {
                    this.checkboxes[followup.id] = false;
                });
                this.db.onSelectedfollowupsChanged.subscribe(selectedfollowups => {
                    for (const id in this.checkboxes) {
                        this.checkboxes[id] = selectedfollowups.includes(id);
                    }

                    this.selectedfollowups = selectedfollowups;
                });
                this.db.onSearchTextChanged.subscribe(data => {
                    this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                })
            });

    }

    newfollowup() {
        this.dialogRef = this.dialog.open(FusefollowupsfollowupFormDialogComponent, {
            panelClass: 'followup-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
                var res = response.getRawValue();
                console.log(res);
                res.antenatalid = res.antenatalid.firstname;
                this.db.savefollowup(res);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

            });

    }

    editfollowup(followup) {
        console.log(followup);
        this.dialogRef = this.dialog.open(FusefollowupsfollowupFormDialogComponent, {
            panelClass: 'followup-form-dialog',
            data: {
                followup: followup,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':
                        var form = formData.getRawValue();
                        form.antenatalid = form.antenatalid.firstname;
                        this.db.updatefollowup(form);
                        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deletefollowup(followup);

                        break;
                }
            });
    }

    /**
     * Delete followup
     */
    deletefollowup(followup) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deletefollowup(followup);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(followupId) {
        this.db.toggleSelectedfollowup(followupId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    followups2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            of(this.db.followups).delay(5000),
            this.sort.mdSortChange,
            this._paginator.page,
        ];

        var result = Observable.fromPromise(this.db.getfollowups());
        /* result.subscribe(x => console.log(x), e => console.error(e));
        return result; */

        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.followups]));
        });
    }

    disconnect() {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
          return data;
        }
    
        return data.sort((a, b) => {
          const isAsc = this.sort.direction === 'asc';
          switch (this.sort.active) {
            case 'antenatalid': return compare(a.antenatalid, b.antenatalid, isAsc);
            case 'amount': return compare(+a.amount, +b.amount, isAsc);
            case 'id': return compare(+a.id, +b.id, isAsc);
            default: return 0;
          }
        });
      }
}

 /** Simple sort comparator for example ID/Name columns (for client-side sorting). */
 function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }