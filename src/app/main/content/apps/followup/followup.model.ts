export interface followup {
    id: string,
    rev: string,
    antenatalid: string,
    date: any,
    heightoffunds: string,
    pp: string,
    urine: string,
    bp: string,
    weight: string,
    hb: string,
    oedema: string,
    remarks: string,
    fheart: string,
    rpb: string,
    ga: string,
    nextvisit: any,
    initialexaminer: string
}

