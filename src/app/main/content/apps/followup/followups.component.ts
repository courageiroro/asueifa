import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { followupsService } from './followups.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-followups',
    templateUrl  : './followups.component.html',
    styleUrls    : ['./followups.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FusefollowupsComponent implements OnInit
{
    hasSelectedfollowups: boolean;
    searchInput: FormControl;

    constructor(private followupsService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.followupsService.onSelectedfollowupsChanged
            .subscribe(selectedfollowups => {
                this.hasSelectedfollowups = selectedfollowups.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.followupsService.onSearchTextChanged.next(searchText);
            });
    }

}
