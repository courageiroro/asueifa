import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FusefollowupsComponent } from './followups.component';
import { followupsService } from './followups.service';
import { FusefollowupsfollowupListComponent } from './followup-list/followup-list.component';
import { FusefollowupsSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FusefollowupsfollowupFormDialogComponent } from './followup-form/followup-form.component';
import { AuthGuard } from 'app/auth.guard';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';
import { antenatalsService } from './../antenatal/antenatals.service';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/followups',
        component: FusefollowupsComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            followups: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes),
        Ng2AutoCompleteModule
    ],
    declarations   : [
        FusefollowupsComponent,
        FusefollowupsfollowupListComponent,
        FusefollowupsSelectedBarComponent,
        FusefollowupsfollowupFormDialogComponent
    ],
    providers      : [
        followupsService,
        AuthGuard,
        PouchService,
        antenatalsService
    ],
    entryComponents: [FusefollowupsfollowupFormDialogComponent]
})
export class FusefollowupsModule
{
}
