import { Component, OnInit } from '@angular/core';
import { followupsService } from '../followups.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FusefollowupsSelectedBarComponent implements OnInit
{
    selectedfollowups: string[];
    hasSelectedfollowups: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private followupsService: PouchService,
        public dialog: MdDialog
    )
    {
        this.followupsService.onSelectedfollowupsChanged
            .subscribe(selectedfollowups => {
                this.selectedfollowups = selectedfollowups;
                setTimeout(() => {
                    this.hasSelectedfollowups = selectedfollowups.length > 0;
                    this.isIndeterminate = (selectedfollowups.length !== this.followupsService.followups.length && selectedfollowups.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.followupsService.selectfollowups();
    }

    deselectAll()
    {
        this.followupsService.deselectfollowups();
    }

    deleteSelectedfollowups()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected Income categories?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.followupsService.deleteSelectedfollowups();
            }
            this.confirmDialogRef = null;
        });
    }

}
