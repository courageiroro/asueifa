import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Form_element } from '../form_element.model';

@Component({
    selector: 'fuse-form_elements-form_element-form-dialog',
    templateUrl: './form_element-form.component.html',
    styleUrls: ['./form_element-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseForm_elementsForm_elementFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    form_elementForm: FormGroup;
    action: string;
    form_element: Form_element;

    constructor(
        public dialogRef: MdDialogRef<FuseForm_elementsForm_elementFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Form Element';
            this.form_element = data.form_element;
        }
        else {
            this.dialogTitle = 'New Form Element';
            this.form_element = {
                id: '',
                rev: '',
                type: '',
                html: '',
            }
        }

        this.form_elementForm = this.createForm_elementForm();
    }

    ngOnInit() {
    }

    createForm_elementForm() {
        return this.formBuilder.group({
            id: [this.form_element.id],
            rev: [this.form_element.rev],
            type: [this.form_element.type],
            html: [this.form_element.html],

        });
    }
}
