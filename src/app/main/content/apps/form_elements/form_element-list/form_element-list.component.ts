import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Form_elementsService } from '../form_elements.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject} from 'rxjs/Rx';
import { FuseForm_elementsForm_elementFormDialogComponent } from '../form_element-form/form_element-form.component';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Form_element } from '../form_element.model';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-form_elements-form_element-list',
    templateUrl: './form_element-list.component.html',
    styleUrls  : ['./form_element-list.component.scss']
})
export class FuseForm_elementsForm_elementListComponent implements OnInit
{
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public form_elements: Array<Form_element> = [];
    form_elements2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'form_element', 'image', 'buttons'];
    selectedForm_elements: any[];
    checkboxes: {};

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(public dialog: MdDialog, public db: PouchService)
    { }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db);
        this._loadForm_elements();
    }

    private _loadForm_elements(): void {
          this.db.onForm_elementsChanged.subscribe(data =>{
            this.dataSource = new FilesDataSource(this.db);
        })
        this.db.getForm_elements()
        .then((form_elements: Array<Form_element>) => {
            this.form_elements = form_elements;
            this.form_elements2= new BehaviorSubject<any>(form_elements);

            this.checkboxes = {};
            form_elements.map(form_element => {
                this.checkboxes[form_element.id] = false;
            });
             this.db.onSelectedForm_elementsChanged.subscribe(selectedForm_elements => {
                for ( const id in this.checkboxes )
                {
                    this.checkboxes[id] = selectedForm_elements.includes(id);
                }

                this.selectedForm_elements = selectedForm_elements;
            });
        this.db.onSearchTextChanged.subscribe(data =>{
            this.dataSource = new FilesDataSource(this.db);
        })
        });
        
    }

    newForm_element()
    {
        this.dialogRef = this.dialog.open(FuseForm_elementsForm_elementFormDialogComponent, {
            panelClass: 'form_element-form-dialog',
            data      : {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if ( !response )
                {
                    return;
                }

                this.db.saveForm_element(response.getRawValue());
                this.dataSource = new FilesDataSource(this.db);

            });

    }

    editForm_element(form_element)
    {
        this.dialogRef = this.dialog.open(FuseForm_elementsForm_elementFormDialogComponent, {
            panelClass: 'form_element-form-dialog',
            data      : {
                form_element: form_element,
                action : 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch ( actionType )
                {
                    /**
                     * Save
                     */
                    case 'save':

                        this.db.updateForm_element(formData.getRawValue());
                        this.dataSource = new FilesDataSource(this.db);

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteForm_element(form_element);

                        break;
                }
            });
    }

    /**
     * Delete form_element
     */
    deleteForm_element(form_element)
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.db.deleteForm_element(form_element);
                this.dataSource = new FilesDataSource(this.db);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(form_elementId)
    {
        this.db.toggleSelectedForm_element(form_elementId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    form_elements2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService)
    {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]>
    {
        var result = Observable.fromPromise(this.db.getForm_elements());
        result.subscribe(x => console.log(x), e => console.error(e));
        return result;    
    }

    disconnect()
    {
    }
}
