import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Form_elementsService } from './form_elements.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-form_elements',
    templateUrl  : './form_elements.component.html',
    styleUrls    : ['./form_elements.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FuseForm_elementsComponent implements OnInit
{
    hasSelectedForm_elements: boolean;
    searchInput: FormControl;

    constructor(private form_elementsService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.form_elementsService.onSelectedForm_elementsChanged
            .subscribe(selectedForm_elements => {
                this.hasSelectedForm_elements = selectedForm_elements.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.form_elementsService.onSearchTextChanged.next(searchText);
            });
    }

}
