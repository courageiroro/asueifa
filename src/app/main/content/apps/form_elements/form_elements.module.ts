import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseForm_elementsComponent } from './form_elements.component';
import { Form_elementsService } from './form_elements.service';
import { FuseForm_elementsForm_elementListComponent } from './form_element-list/form_element-list.component';
import { FuseForm_elementsSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseForm_elementsForm_elementFormDialogComponent } from './form_element-form/form_element-form.component';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : '**',
        component: FuseForm_elementsComponent,
        children : [],
        resolve  : {
            form_elements: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseForm_elementsComponent,
        FuseForm_elementsForm_elementListComponent,
        FuseForm_elementsSelectedBarComponent,
        FuseForm_elementsForm_elementFormDialogComponent
    ],
    providers      : [
        Form_elementsService,
        PouchService
    ],
    entryComponents: [FuseForm_elementsForm_elementFormDialogComponent]
})
export class FuseForm_elementsModule
{
}
