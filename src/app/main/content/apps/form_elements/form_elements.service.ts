/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Form_element } from './form_element.model';
import { Schema } from '../schema';

@Injectable()
export class Form_elementsService {

    onForm_elementsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedForm_elementsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    form_elements: Form_element[];
    user: any;
    selectedForm_elements: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB();
    }

    /**
     * The form_elements App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getForm_elements()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getForm_elements();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getForm_elements();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB() {
        this.db = new PouchDB('hospital_demo', { adaptater: 'websql' });
        this.db.setSchema(Schema);
        this.remote = 'http://sarutech.com:5984/hospital_demo';

        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };

        this.db.sync(this.remote, options);

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }


    /***********
     * form_element
     **********/
    /**
     * Save a form_element
     * @param {form_element} Form_element
     *
     * @return Promise<form_element>
     */
    saveForm_element(form_element: Form_element): Promise<Form_element> {
        form_element.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('form_element', form_element)
            .then((data: any) => {
                
                if (data && data.form_elements && data.form_elements
                [0]) {
                    return data.form_elements[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a form_element
    * @param {form_element} Form_element
    *
    * @return Promise<form_element>
    */
    updateForm_element(form_element: Form_element) {
       
        return this.db.rel.save('form_element', form_element)
            .then((data: any) => {
                if (data && data.form_elements && data.form_elements
                [0]) {
                    return data.form_elements[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a form_element
     * @param {form_element} form_element
     *
     * @return Promise<boolean>
     */
    removeForm_element(form_element: Form_element): Promise<boolean> {
        return this.db.rel.del('form_element', form_element)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the form_elements
     *
     * @return Promise<Array<form_element>>
     */
    getForm_elements(): Promise<Array<Form_element>> {
        return this.db.rel.find('form_element')
            .then((data: any) => {
                this.form_elements = data.form_elements;
                  if ( this.searchText && this.searchText !== '' )
                {
                    this.form_elements = FuseUtils.filterArrayByString(this.form_elements, this.searchText);
                }
                //this.onform_elementsChanged.next(this.form_elements);
                return Promise.resolve(this.form_elements);
                //return data.form_elements ? data.form_elements : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a form_element
     * @param {form_element} form_element
     *
     * @return Promise<form_element>
     */
    getForm_element(form_element: Form_element): Promise<Form_element> {
        return this.db.rel.find('form_element', form_element.id)
            .then((data: any) => {
                return data && data.form_elements ? data.form_elements[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected form_element by id
     * @param id
     */
    toggleSelectedForm_element(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedForm_elements.length > 0) {
            const index = this.selectedForm_elements.indexOf(id);

            if (index !== -1) {
                this.selectedForm_elements.splice(index, 1);

                // Trigger the next event
                this.onSelectedForm_elementsChanged.next(this.selectedForm_elements);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedForm_elements.push(id);

        // Trigger the next event
        this.onSelectedForm_elementsChanged.next(this.selectedForm_elements);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedForm_elements.length > 0) {
            this.deselectForm_elements();
        }
        else {
            this.selectForm_elements();
        }
    }

    selectForm_elements(filterParameter?, filterValue?) {
        this.selectedForm_elements = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedForm_elements = [];
            this.form_elements.map(form_element => {
                this.selectedForm_elements.push(form_element.id);
            });
        }
        else {
            /* this.selectedform_elements.push(...
                 this.form_elements.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedForm_elementsChanged.next(this.selectedForm_elements);
    }





    deselectForm_elements() {
        this.selectedForm_elements = [];

        // Trigger the next event
        this.onSelectedForm_elementsChanged.next(this.selectedForm_elements);
    }

    deleteForm_element(form_element) {
        this.db.rel.del('form_element', form_element)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const form_elementIndex = this.form_elements.indexOf(form_element);
                this.form_elements.splice(form_elementIndex, 1);
                this.onForm_elementsChanged.next(this.form_elements);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedForm_elements() {
        for (const form_elementId of this.selectedForm_elements) {
            const form_element = this.form_elements.find(_form_element => {
                return _form_element.id === form_elementId;
            });

            this.db.rel.del('form_element', form_element)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const form_elementIndex = this.form_elements.indexOf(form_element);
                    this.form_elements.splice(form_elementIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onForm_elementsChanged.next(this.form_elements);
        this.deselectForm_elements();
    }
}
