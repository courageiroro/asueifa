import { Component, OnInit } from '@angular/core';
import { Form_elementsService } from '../form_elements.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseForm_elementsSelectedBarComponent implements OnInit
{
    selectedForm_elements: string[];
    hasSelectedForm_elements: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private form_elementsService: PouchService,
        public dialog: MdDialog
    )
    {
        this.form_elementsService.onSelectedForm_elementsChanged
            .subscribe(selectedForm_elements => {
                this.selectedForm_elements = selectedForm_elements;
                setTimeout(() => {
                    this.hasSelectedForm_elements = selectedForm_elements.length > 0;
                    this.isIndeterminate = (selectedForm_elements.length !== this.form_elementsService.form_elements.length && selectedForm_elements.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.form_elementsService.selectForm_elements();
    }

    deselectAll()
    {
        this.form_elementsService.deselectForm_elements();
    }

    deleteSelectedForm_elements()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected form elements?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.form_elementsService.deleteSelectedForm_elements();
            }
            this.confirmDialogRef = null;
        });
    }

}
