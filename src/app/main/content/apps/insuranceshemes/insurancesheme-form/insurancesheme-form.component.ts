import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Insurancesheme } from '../insurancesheme.model';


@Component({
    selector: 'fuse-insuranceshemes-insurancesheme-form-dialog',
    templateUrl: './insurancesheme-form.component.html',
    styleUrls: ['./insurancesheme-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseInsuranceshemesInsuranceshemeFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    insuranceshemeForm: FormGroup;
    action: string;
    insurancesheme: Insurancesheme;

    constructor(
        public dialogRef: MdDialogRef<FuseInsuranceshemesInsuranceshemeFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit insurancesheme';
            this.insurancesheme = data.insurancesheme;
        }
        else {
            this.dialogTitle = 'New insurancesheme';
            this.insurancesheme = {
                id: '',
                rev: '',
                name: '',
                description: '',
                doctors: []
            }
        }

        this.insuranceshemeForm = this.createinsuranceshemeForm();
    }

    ngOnInit() {
    }

    createinsuranceshemeForm() {
        return this.formBuilder.group({
            id: [this.insurancesheme.id],
            rev: [this.insurancesheme.rev],
            name: [this.insurancesheme.name],
            description: [this.insurancesheme.description],

        });
    }
}
