import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { InsuranceshemesService } from '../insuranceshemes.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { FuseInsuranceshemesInsuranceshemeFormDialogComponent } from '../insurancesheme-form/insurancesheme-form.component';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Insurancesheme } from '../insurancesheme.model';
import { Http, Headers, RequestOptions } from '@angular/http';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-insuranceshemes-insurancesheme-list',
    templateUrl: './insurancesheme-list.component.html',
    styleUrls: ['./insurancesheme-list.component.scss']
})
export class FuseInsuranceshemesInsuranceshemeListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public insuranceshemes: Array<Insurancesheme> = [];
    insuranceshemes2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'name', 'description', 'buttons'];
    selectedinsuranceshemes: any[];
    online = true;
    checkboxes: {};

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(public dialog: MdDialog, public db: PouchService, public http: Http)
    { 
    }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db);
        this._loadInsuranceshemes();
    }

    private _loadInsuranceshemes(): void {
        this.db.onInsuranceshemesChanged.subscribe(data => {
            this.dataSource = new FilesDataSource(this.db);
        })
        this.db.getInsuranceshemes()
            .then((insuranceshemes: Array<Insurancesheme>) => {
                this.insuranceshemes = insuranceshemes;
                this.insuranceshemes2 = new BehaviorSubject<any>(insuranceshemes);
                //console.log(this.insuranceshemes2);

                this.checkboxes = {};
                insuranceshemes.map(insurancesheme => {
                    this.checkboxes[insurancesheme.id] = false;
                });
                this.db.onSelectedInsuranceshemesChanged.subscribe(selectedinsuranceshemes => {
                    for (const id in this.checkboxes) {
                        this.checkboxes[id] = selectedinsuranceshemes.includes(id);
                    }

                    this.selectedinsuranceshemes = selectedinsuranceshemes;
                });
                this.db.onSearchTextChanged.subscribe(data => {
                    this.dataSource = new FilesDataSource(this.db);
                })
            });

    }


    onlineCheck() {
        this.online = true;
        this.http.get('https://sarutech.com/couchdblogin/auth/validate-username/xs2da2432252sda').subscribe(data => {
            this.online = true;
           
        },
            (err) => {
                this.online = false;
                
            });
    }

    click() {
        this.onlineCheck();
    }

    newInsurancesheme() {
        this.dialogRef = this.dialog.open(FuseInsuranceshemesInsuranceshemeFormDialogComponent, {
            panelClass: 'insurancesheme-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }

                this.db.saveInsurancesheme(response.getRawValue());
                this.dataSource = new FilesDataSource(this.db);
                

            });

    }

    editInsurancesheme(insurancesheme) {
        
        this.dialogRef = this.dialog.open(FuseInsuranceshemesInsuranceshemeFormDialogComponent, {
            panelClass: 'insurancesheme-form-dialog',
            data: {
                insurancesheme: insurancesheme,
                action: 'edit'

            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':

                        this.db.updateInsurancesheme(formData.getRawValue());
                        this.dataSource = new FilesDataSource(this.db);
                        
                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteInsurancesheme(insurancesheme);

                        break;
                }
            });
    }

    /**
     * Delete insurancesheme
     */
    deleteInsurancesheme(insurancesheme) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deleteInsurancesheme(insurancesheme);
                this.dataSource = new FilesDataSource(this.db);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(insuranceshemeId) {
        this.db.toggleSelectedInsurancesheme(insuranceshemeId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    insuranceshemes2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        var result = Observable.fromPromise(this.db.getInsuranceshemes());
        result.subscribe(x => console.log(x), e => console.error(e));
        return result;
    }

    disconnect() {
    }
}
