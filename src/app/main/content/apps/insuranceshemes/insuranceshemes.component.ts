import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { InsuranceshemesService } from './insuranceshemes.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector: 'fuse-insuranceshemes',
    templateUrl: './insuranceshemes.component.html',
    styleUrls: ['./insuranceshemes.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [Animations.slideInTop]
})
export class FuseInsuranceshemesComponent implements OnInit {
    hasSelectedInsuranceshemes: boolean;
    searchInput: FormControl;

    constructor(private insuranceshemesService: PouchService) {
        this.searchInput = new FormControl('');
       
    }

    ngOnInit() {

        this.insuranceshemesService.onSelectedInsuranceshemesChanged
            .subscribe(selectedInsuranceshemes => {
                this.hasSelectedInsuranceshemes = selectedInsuranceshemes.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.insuranceshemesService.onSearchTextChanged.next(searchText);
            });
    }

}
