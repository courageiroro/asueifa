import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseInsuranceshemesComponent } from './insuranceshemes.component';
import { InsuranceshemesService } from './insuranceshemes.service';
//import { UserService } from '../../../../user.service';
//import { insuranceshemesService2 } from './contacts.service';
import { FuseInsuranceshemesInsuranceshemeListComponent } from './insurancesheme-list/insurancesheme-list.component';
import { FuseInsuranceshemesSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseInsuranceshemesInsuranceshemeFormDialogComponent } from './insurancesheme-form/insurancesheme-form.component';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/insurancescheme',
        component: FuseInsuranceshemesComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            insuranceshemes: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseInsuranceshemesComponent,
        FuseInsuranceshemesInsuranceshemeListComponent,
        FuseInsuranceshemesSelectedBarComponent,
        FuseInsuranceshemesInsuranceshemeFormDialogComponent
    ],
    providers      : [
        InsuranceshemesService,
        PouchService,
        AuthGuard
       //UserService
    ],
    entryComponents: [FuseInsuranceshemesInsuranceshemeFormDialogComponent]
})
export class FuseInsuranceshemesModule
{
}
