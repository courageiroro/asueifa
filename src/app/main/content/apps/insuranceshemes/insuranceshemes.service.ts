/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Insurancesheme } from './insurancesheme.model';
import { Schema } from '../schema';

@Injectable()
export class InsuranceshemesService {
    public insurancesheme = "";
    public sCredentials;
    onInsuranceshemesChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedInsuranceshemesChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    insuranceshemes: Insurancesheme[];
    user: any;
    selectedInsuranceshemes: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The insuranceshemes App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getInsuranceshemes()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getInsuranceshemes();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getInsuranceshemes();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('abayelsa', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/abayelsa';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/abayelsa';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }

    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }


    /***********
     * insurancesheme
     **********/
    /**
     * Save a insurancesheme
     * @param {insurancesheme} insurancesheme
     *
     * @return Promise<insurancesheme>
     */
    saveInsurancesheme(insurancesheme: Insurancesheme): Promise<Insurancesheme> {
        insurancesheme.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('insurancesheme', insurancesheme)
            .then((data: any) => {

                if (data && data.insuranceshemes && data.insuranceshemes
                [0]) {
                    console.log('save');
                    
                    return data.insuranceshemes[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a insurancesheme
    * @param {insurancesheme} insurancesheme
    *
    * @return Promise<insurancesheme>
    */
    updateInsurancesheme(insurancesheme: Insurancesheme) {
        return this.db.rel.save('insurancesheme', insurancesheme)
            .then((data: any) => {
                if (data && data.insuranceshemes && data.insuranceshemes
                [0]) {
                    console.log('Update')
                    
                    return data.insuranceshemes[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a insurancesheme
     * @param {insurancesheme} insurancesheme
     *
     * @return Promise<boolean>
     */
    removeInsurancesheme(insurancesheme: Insurancesheme): Promise<boolean> {
        return this.db.rel.del('insurancesheme', insurancesheme)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the insuranceshemes
     *
     * @return Promise<Array<insurancesheme>>
     */
    getInsuranceshemes(): Promise<Array<Insurancesheme>> {
        return this.db.rel.find('insurancesheme')
            .then((data: any) => {
                this.insuranceshemes = data.insuranceshemes;
                if (this.searchText && this.searchText !== '') {
                    this.insuranceshemes = FuseUtils.filterArrayByString(this.insuranceshemes, this.searchText);
                }
                //this.oninsuranceshemesChanged.next(this.insuranceshemes);
                return Promise.resolve(this.insuranceshemes);
                //return data.insuranceshemes ? data.insuranceshemes : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getDoctorInsuranceshemes(): Promise<Array<Insurancesheme>> {
        return this.db.rel.find('insurancesheme')
            .then((data: any) => {
                return data.insuranceshemes ? data.insuranceshemes : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a insurancesheme
     * @param {insurancesheme} insurancesheme
     *
     * @return Promise<insurancesheme>
     */
    getInsurancesheme(insurancesheme: Insurancesheme): Promise<Insurancesheme> {
        return this.db.rel.find('insurancesheme', insurancesheme.id)
            .then((data: any) => {
                console.log("Get")
                
                return data && data.insuranceshemes ? data.insuranceshemes[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected insurancesheme by id
     * @param id
     */
    toggleSelectedInsurancesheme(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedInsuranceshemes.length > 0) {
            const index = this.selectedInsuranceshemes.indexOf(id);

            if (index !== -1) {
                this.selectedInsuranceshemes.splice(index, 1);

                // Trigger the next event
                this.onSelectedInsuranceshemesChanged.next(this.selectedInsuranceshemes);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedInsuranceshemes.push(id);

        // Trigger the next event
        this.onSelectedInsuranceshemesChanged.next(this.selectedInsuranceshemes);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedInsuranceshemes.length > 0) {
            this.deselectInsuranceshemes();
        }
        else {
            this.selectInsuranceshemes();
        }
    }

    selectInsuranceshemes(filterParameter?, filterValue?) {
        this.selectedInsuranceshemes = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedInsuranceshemes = [];
            this.insuranceshemes.map(insurancesheme => {
                this.selectedInsuranceshemes.push(insurancesheme.id);
            });
        }
        else {
            /* this.selectedinsuranceshemes.push(...
                 this.insuranceshemes.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedInsuranceshemesChanged.next(this.selectedInsuranceshemes);
    }





    deselectInsuranceshemes() {
        this.selectedInsuranceshemes = [];

        // Trigger the next event
        this.onSelectedInsuranceshemesChanged.next(this.selectedInsuranceshemes);
    }

    deleteInsurancesheme(insurancesheme) {
        this.db.rel.del('insurancesheme', insurancesheme)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const insuranceshemeIndex = this.insuranceshemes.indexOf(insurancesheme);
                this.insuranceshemes.splice(insuranceshemeIndex, 1);
                this.onInsuranceshemesChanged.next(this.insuranceshemes);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedInsuranceshemes() {
        for (const insuranceshemeId of this.selectedInsuranceshemes) {
            const insurancesheme = this.insuranceshemes.find(_insurancesheme => {
                return _insurancesheme.id === insuranceshemeId;
            });

            this.db.rel.del('insurancesheme', insurancesheme)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const insuranceshemeIndex = this.insuranceshemes.indexOf(insurancesheme);
                    this.insuranceshemes.splice(insuranceshemeIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onInsuranceshemesChanged.next(this.insuranceshemes);
        this.deselectInsuranceshemes();
    }
}
