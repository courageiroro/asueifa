import { Component, OnInit } from '@angular/core';
import { InsuranceshemesService } from '../insuranceshemes.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseInsuranceshemesSelectedBarComponent implements OnInit
{
    selectedInsuranceshemes: string[];
    hasSelectedInsuranceshemes: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private insuranceshemesService: PouchService,
        public dialog: MdDialog
    )
    {
        this.insuranceshemesService.onSelectedInsuranceshemesChanged
            .subscribe(selectedInsuranceshemes => {
                this.selectedInsuranceshemes = selectedInsuranceshemes;
                setTimeout(() => {
                    this.hasSelectedInsuranceshemes = selectedInsuranceshemes.length > 0;
                    this.isIndeterminate = (selectedInsuranceshemes.length !== this.insuranceshemesService.insuranceshemes.length && selectedInsuranceshemes.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.insuranceshemesService.selectInsuranceshemes();
    }

    deselectAll()
    {
        this.insuranceshemesService.deselectInsuranceshemes();
    }

    deleteSelectedinsuranceshemes()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected Insurance Schemes?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.insuranceshemesService.deleteSelectedInsuranceshemes();
            }
            this.confirmDialogRef = null;
        });
    }

}
