import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Insurancetype } from '../insurancetype.model';

@Component({
    selector: 'fuse-insurancetypes-insurancetype-form-dialog',
    templateUrl: './insurancetype-form.component.html',
    styleUrls: ['./insurancetype-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseInsurancetypesInsurancetypeFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    insurancetypeForm: FormGroup;
    action: string;
    insurancetype: Insurancetype;

    constructor(
        public dialogRef: MdDialogRef<FuseInsurancetypesInsurancetypeFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Insurance Type';
            this.insurancetype = data.insurancetype;
        }
        else {
            this.dialogTitle = 'New Insurance Type';
            this.insurancetype = {
                id: '',
                rev: '',
                name: '',
                description: '',
                percentage: null,
                patient:''
            }
        }

        this.insurancetypeForm = this.createInsurancetypeForm();
    }

    ngOnInit() {
    }

    createInsurancetypeForm() {
        return this.formBuilder.group({
            id: [this.insurancetype.id],
            rev: [this.insurancetype.rev],
            name: [this.insurancetype.name],
            description: [this.insurancetype.description],
            percentage: [this.insurancetype.percentage],
        });
    }
}
