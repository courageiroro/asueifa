import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { InsurancetypesService } from '../insurancetypes.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { FuseInsurancetypesInsurancetypeFormDialogComponent } from '../insurancetype-form/insurancetype-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Insurancetype } from '../insurancetype.model';
import { Http, Headers, RequestOptions } from '@angular/http';
import {PouchService} from '../../../../../provider/pouch-service';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';

@Component({
    selector: 'fuse-insurancetypes-insurancetype-list',
    templateUrl: './insurancetype-list.component.html',
    styleUrls: ['./insurancetype-list.component.scss']
})
export class FuseInsurancetypesInsurancetypeListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public insurancetypes: Array<Insurancetype> = [];
    insurancetypes2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'name','percentage','description', 'buttons'];
    selectedInsurancetypes: any[];
    online = true;
    checkboxes: {};

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;

    constructor(public dialog: MdDialog, public db: PouchService, public http: Http)
    { 
    }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        this._loadInsurancetypes();
    }

    private _loadInsurancetypes(): void {
        this.db.onInsurancetypesChanged.subscribe(data => {
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        this.db.getInsurancetypes()
            .then((insurancetypes: Array<Insurancetype>) => {
                this.insurancetypes = insurancetypes;
                this.insurancetypes2 = new BehaviorSubject<any>(insurancetypes);
                //console.log(this.insurancetypes2);

                this.checkboxes = {};
                insurancetypes.map(insurancetype => {
                    this.checkboxes[insurancetype.id] = false;
                });
                this.db.onSelectedInsurancetypesChanged.subscribe(selectedInsurancetypes => {
                    for (const id in this.checkboxes) {
                        this.checkboxes[id] = selectedInsurancetypes.includes(id);
                    }

                    this.selectedInsurancetypes = selectedInsurancetypes;
                });
                this.db.onSearchTextChanged.subscribe(data => {
                    this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                })
            });

    }


    onlineCheck() {
        this.online = true;
        this.http.get('https://sarutech.com/couchdblogin/auth/validate-username/xs2da2432252sda').subscribe(data => {
            this.online = true;
            
        },
            (err) => {
                this.online = false;
                
            });
    }

    click() {
        this.onlineCheck();
    }

    newInsurancetype() {
        this.dialogRef = this.dialog.open(FuseInsurancetypesInsurancetypeFormDialogComponent, {
            panelClass: 'insurancetype-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }

                this.db.saveInsurancetype(response.getRawValue());
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                

            });

    }

    editInsurancetype(insurancetype) {
        
        this.dialogRef = this.dialog.open(FuseInsurancetypesInsurancetypeFormDialogComponent, {
            panelClass: 'insurancetype-form-dialog',
            data: {
                insurancetype: insurancetype,
                action: 'edit'

            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':

                        this.db.updateInsurancetype(formData.getRawValue());
                        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                        
                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteInsurancetype(insurancetype);

                        break;
                }
            });
    }

    /**
     * Delete insurancetype
     */
    deleteInsurancetype(insurancetype) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deleteInsurancetype(insurancetype);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(insurancetypeId) {
        this.db.toggleSelectedInsurancetype(insurancetypeId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    insurancetypes2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            of(this.db.insurancetypes).delay(5000),
            this.sort.mdSortChange,
            this._paginator.page,
        ];

        var result = Observable.fromPromise(this.db.getInsurancetypes());
       /*  result.subscribe(x => console.log(x), e => console.error(e));
        return result; */

        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.insurancetypes]));
        });
    }

    disconnect() {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
          return data;
        }
    
        return data.sort((a, b) => {
          const isAsc = this.sort.direction === 'asc';
          switch (this.sort.active) {
            case 'name': return compare(a.name, b.name, isAsc);
            case 'amount': return compare(+a.amount, +b.amount, isAsc);
            case 'id': return compare(+a.id, +b.id, isAsc);
            default: return 0;
          }
        });
      }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
