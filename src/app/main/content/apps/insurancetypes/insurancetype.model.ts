export interface Insurancetype {
    id: string,
    rev: string,
    name: string,
    percentage: any,
    description: string,
    patient: string
}

