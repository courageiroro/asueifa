import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { InsurancetypesService } from './insurancetypes.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector: 'fuse-insurancetypes',
    templateUrl: './insurancetypes.component.html',
    styleUrls: ['./insurancetypes.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [Animations.slideInTop]
})
export class FuseInsurancetypesComponent implements OnInit {
    hasSelectedInsurancetypes: boolean;
    searchInput: FormControl;

    constructor(private insurancetypesService: PouchService) {
        this.searchInput = new FormControl('');
       
    }

    ngOnInit() {

        this.insurancetypesService.onSelectedInsurancetypesChanged
            .subscribe(selectedInsurancetypes => {
                this.hasSelectedInsurancetypes = selectedInsurancetypes.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.insurancetypesService.onSearchTextChanged.next(searchText);
            });
    }

}
