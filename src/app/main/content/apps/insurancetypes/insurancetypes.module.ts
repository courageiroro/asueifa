import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseInsurancetypesComponent } from './insurancetypes.component';
import { InsurancetypesService } from './insurancetypes.service';
//import { UserService } from '../../../../user.service';
//import { insurancetypesService2 } from './contacts.service';
import { FuseInsurancetypesInsurancetypeListComponent } from './insurancetype-list/insurancetype-list.component';
import { FuseInsurancetypesSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseInsurancetypesInsurancetypeFormDialogComponent } from './insurancetype-form/insurancetype-form.component';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/insurancetype',
        component: FuseInsurancetypesComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            insurancetypes: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseInsurancetypesComponent,
        FuseInsurancetypesInsurancetypeListComponent,
        FuseInsurancetypesSelectedBarComponent,
        FuseInsurancetypesInsurancetypeFormDialogComponent
    ],
    providers      : [
        InsurancetypesService,
        PouchService,
        AuthGuard,
       //UserService
    ],
    entryComponents: [FuseInsurancetypesInsurancetypeFormDialogComponent]
})
export class FuseInsurancetypesModule
{
}
