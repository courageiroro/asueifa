/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Insurancetype } from './insurancetype.model';
import { Schema } from '../schema';

@Injectable()
export class InsurancetypesService {
    public insurancetype = "";
    public sCredentials;
    onInsurancetypesChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedInsurancetypesChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    insurancetypes: Insurancetype[];
    user: any;
    selectedInsurancetypes: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The insurancetypes App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getInsurancetypes()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getInsurancetypes();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getInsurancetypes();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('abayelsa', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/abayelsa';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/abayelsa';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }


    /***********
     * insurancetype
     **********/
    /**
     * Save a insurancetype
     * @param {insurancetype} insurancetype
     *
     * @return Promise<insurancetype>
     */
    saveInsurancetype(insurancetype: Insurancetype): Promise<Insurancetype> {
        insurancetype.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('insurancetype', insurancetype)
            .then((data: any) => {

                if (data && data.insurancetypes && data.insurancetypes
                [0]) {
                    console.log('save');
                    
                    return data.insurancetypes[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a insurancetype
    * @param {insurancetype} insurancetype
    *
    * @return Promise<insurancetype>
    */
    updateInsurancetype(insurancetype: Insurancetype) {
        return this.db.rel.save('insurancetype', insurancetype)
            .then((data: any) => {
                if (data && data.insurancetypes && data.insurancetypes
                [0]) {
                    console.log('Update')
                    
                    return data.insurancetypes[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a insurancetype
     * @param {insurancetype} insurancetype
     *
     * @return Promise<boolean>
     */
    removeInsurancetype(insurancetype: Insurancetype): Promise<boolean> {
        return this.db.rel.del('insurancetype', insurancetype)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the insurancetypes
     *
     * @return Promise<Array<insurancetype>>
     */
    getInsurancetypes(): Promise<Array<Insurancetype>> {
        return this.db.rel.find('insurancetype')
            .then((data: any) => {
                this.insurancetypes = data.insurancetypes;
                if (this.searchText && this.searchText !== '') {
                    this.insurancetypes = FuseUtils.filterArrayByString(this.insurancetypes, this.searchText);
                }
                //this.oninsurancetypesChanged.next(this.insurancetypes);
                return Promise.resolve(this.insurancetypes);
                //return data.insurancetypes ? data.insurancetypes : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getDoctorInsurancetypes(): Promise<Array<Insurancetype>> {
        return this.db.rel.find('insurancetype')
            .then((data: any) => {
                return data.insurancetypes ? data.insurancetypes : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a insurancetype
     * @param {insurancetype} insurancetype
     *
     * @return Promise<insurancetype>
     */
    getInsurancetype(insurancetype: Insurancetype): Promise<Insurancetype> {
        return this.db.rel.find('insurancetype', insurancetype.id)
            .then((data: any) => {
                console.log("Get")
                
                return data && data.insurancetypes ? data.insurancetypes[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected insurancetype by id
     * @param id
     */
    toggleSelectedInsurancetype(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedInsurancetypes.length > 0) {
            const index = this.selectedInsurancetypes.indexOf(id);

            if (index !== -1) {
                this.selectedInsurancetypes.splice(index, 1);

                // Trigger the next event
                this.onSelectedInsurancetypesChanged.next(this.selectedInsurancetypes);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedInsurancetypes.push(id);

        // Trigger the next event
        this.onSelectedInsurancetypesChanged.next(this.selectedInsurancetypes);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedInsurancetypes.length > 0) {
            this.deselectInsurancetypes();
        }
        else {
            this.selectInsurancetypes();
        }
    }

    selectInsurancetypes(filterParameter?, filterValue?) {
        this.selectedInsurancetypes = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedInsurancetypes = [];
            this.insurancetypes.map(insurancetype => {
                this.selectedInsurancetypes.push(insurancetype.id);
            });
        }
        else {
            /* this.selectedinsurancetypes.push(...
                 this.insurancetypes.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedInsurancetypesChanged.next(this.selectedInsurancetypes);
    }





    deselectInsurancetypes() {
        this.selectedInsurancetypes = [];

        // Trigger the next event
        this.onSelectedInsurancetypesChanged.next(this.selectedInsurancetypes);
    }

    deleteInsurancetype(insurancetype) {
        this.db.rel.del('insurancetype', insurancetype)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const insurancetypeIndex = this.insurancetypes.indexOf(insurancetype);
                this.insurancetypes.splice(insurancetypeIndex, 1);
                this.onInsurancetypesChanged.next(this.insurancetypes);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedInsurancetypes() {
        for (const insurancetypeId of this.selectedInsurancetypes) {
            const insurancetype = this.insurancetypes.find(_insurancetype => {
                return _insurancetype.id === insurancetypeId;
            });

            this.db.rel.del('insurancetype', insurancetype)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const insurancetypeIndex = this.insurancetypes.indexOf(insurancetype);
                    this.insurancetypes.splice(insurancetypeIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onInsurancetypesChanged.next(this.insurancetypes);
        this.deselectInsurancetypes();
    }
}
