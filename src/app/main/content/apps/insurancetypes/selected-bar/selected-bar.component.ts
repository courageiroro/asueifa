import { Component, OnInit } from '@angular/core';
import { InsurancetypesService } from '../insurancetypes.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseInsurancetypesSelectedBarComponent implements OnInit
{
    selectedInsurancetypes: string[];
    hasSelectedInsurancetypes: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private insurancetypesService: PouchService,
        public dialog: MdDialog
    )
    {
        this.insurancetypesService.onSelectedInsurancetypesChanged
            .subscribe(selectedInsurancetypes => {
                this.selectedInsurancetypes = selectedInsurancetypes;
                setTimeout(() => {
                    this.hasSelectedInsurancetypes = selectedInsurancetypes.length > 0;
                    this.isIndeterminate = (selectedInsurancetypes.length !== this.insurancetypesService.insurancetypes.length && selectedInsurancetypes.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.insurancetypesService.selectInsurancetypes();
    }

    deselectAll()
    {
        this.insurancetypesService.deselectInsurancetypes();
    }

    deleteSelectedInsurancetypes()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected Insurance Types?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.insurancetypesService.deleteSelectedInsurancetypes();
            }
            this.confirmDialogRef = null;
        });
    }

}
