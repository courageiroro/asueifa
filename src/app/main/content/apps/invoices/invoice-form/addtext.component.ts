import { Component, Inject,OnInit, Input, EventEmitter } from '@angular/core';
import { Validators, FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { FuseInvoicesInvoiceFormDialogComponent } from './invoice-form.component'
import { Invoice } from '../invoice.model';
import { Amount } from './addtext.model';

@Component({
  //moduleId: module.id,
  selector: 'amount',
  templateUrl: 'addtext.component.html',
  outputs: ['passInput'],
})
export class AmountComponent implements OnInit {
  public sum: number;
  action: string;
  invoice: Invoice;
  amount: Amount;
  invoiceForm: FormGroup;
  dialogTitle: string;
  passInput = new EventEmitter<number>();
  // we will pass in address from App component
  @Input('group')
  public amountForm: FormGroup;

  constructor(public dialogRef: MdDialogRef<FuseInvoicesInvoiceFormDialogComponent>, @Inject(MD_DIALOG_DATA) private data: any,
    private formBuilder: FormBuilder,
  ) {
    this.action = data.action;

    if (this.action === 'edit') {
      this.dialogTitle = 'Edit Invoice';
      this.invoice = data.invoice;
    }
    else {
      this.dialogTitle = 'New Invoice';
      this.amount = {
        id: '',
        rev: '',
        amount: null,
        entry: ''
      }
    }

    //this.invoiceForm = this.createInvoiceForm();


  }

   ngOnInit() {
        
    }

  onChange(value: number) {
    this.passInput.emit(value);
  }
 /*  createInvoiceForm() {
    return this.formBuilder.group({
      id: [this.invoice.id],
      rev: [this.invoice.rev],
      amount: ['1000'],
        entry: ['hdjf']
    })
  } */

}