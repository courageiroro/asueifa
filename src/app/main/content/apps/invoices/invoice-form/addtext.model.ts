export interface Amount {
    id: string,
    rev: string,
    amount: number,
    entry: string
}
