import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
    //moduleId: module.id,
    selector: 'entry',
    templateUrl: 'entry.component.html'
})
export class EntryComponent {
    // we will pass in address from App component
    @Input('group2')
    public entryForm: FormGroup;
}