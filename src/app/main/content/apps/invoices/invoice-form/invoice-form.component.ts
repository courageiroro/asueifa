import { Component, Inject, OnInit, ViewEncapsulation, ViewChild, ComponentFactoryResolver, ElementRef, ViewContainerRef } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { Validators, FormGroup, FormArray, FormBuilder, FormControl } from '@angular/forms';
import { BehaviorSubject } from 'rxjs/Rx';
import 'rxjs/Rx';
import { Invoice } from '../invoice.model';
import { Staff } from '../../staffs/staff.model';
import { AmountComponent } from './addtext.component'
import { StaffsService } from '../../staffs/staffs.service';
import { Amount } from './addtext.model';
import { InvoicesService } from '../invoices.service';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-invoices-invoice-form-dialog',
    templateUrl: './invoice-form.component.html',
    styleUrls: ['./invoice-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseInvoicesInvoiceFormDialogComponent implements OnInit {

    @ViewChild('parent', { read: ViewContainerRef }) container: ViewContainerRef;
    @ViewChild('who') input: ElementRef;

    public patientsss: Array<Staff> = [];
    patient: any;
    patients2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    event: CalendarEvent;
    dialogTitle: string;
    invoiceForm: FormGroup;
    action: string;
    invoice: Invoice;
    amount: Amount;
    statuss: any;
    patientss: any;
    idss: any;
    public textData: number;
    public sum;
    public gets;
    public i;
    public loop;
    public result;
    public result2;
    public control;
    public final;
    private arrayNumber;
    public trigger: string;
    public assign: string;
    public grandTotal: number;
    public vat: number;
    public dis: number;
    public dis2: number;
    public discount: number;
    public addTax: number;
    public object;
    public patientId: string;
    public patientEmail: string;
    public patientAddress: string;
    public patientNumber: string;
    public amountArray;

    constructor(
        public dialogRef: MdDialogRef<FuseInvoicesInvoiceFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        private factoryResolve: ComponentFactoryResolver,
        public db: PouchService,
        
    ) {

        this.arrayNumber;
        this.patient = this.db.patientname;
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Invoice';
            this.invoice = data.invoice;
        }
        else {
            this.dialogTitle = 'New Invoice';
            this.invoice = {
                id: '',
                rev: '',
                invoice_number: Math.floor((Math.random() * 90000) + 10000),
                patient_id: '',
                patient_name: '',
                title: '',
                invoice_entries: [],
                creation_timestamp: '',
                due_timestamp: '',
                status: '',
                paddress: '',
                pnumber: '',
                pemail: '',
                vat_percentage: null,
                discount_amount: null,
                subtotal: null,
                grandtotal: null,
                amountss: [],
                insurancename: '',
                notstatus: false
            }
        }

        this.invoiceForm = this.createInvoiceForm();

        db.getPStaffs().then(res => {
            this.patientss = res;
            
        })

    }

    ngOnInit() {
        this.statuss = ['Paid', 'Unpaid']
            
    }

    ngAfterViewInit() {
        //console.log(this.input.nativeElement.value);
    }

    getData(event) {
        this.invoice.vat_percentage = event;

    }

    getPId(patient) {
        this.invoice.discount_amount = patient.insurancepercent;
        this.invoice.insurancename = patient.insurancename;
        this.db.patientname = patient.id;
        this.invoice.patient_id = patient.id;
        this.invoice.paddress = patient.address;
        this.invoice.pemail = patient.email;
        this.invoice.pnumber = patient.phone
        

    }

    getDiscountData(event) {
        this.invoice.discount_amount = event;

    }

    getArray(gets) {
        var mine = Number([gets.amount].join())
        
        return mine
    }



    initAmount() {
        // initialize our amount
        this.result2 = this.invoice.amountss.map(this.getArray)
     
        for (this.i = 0; this.i <= this.result2.length - 1; this.i++) {
            this.amountArray = this.result2[this.i];
        }
        return this.formBuilder.group({
            amount: [''],
            entry: [''],
        });

    }



    getSum(total, num) {
        return total + num;
    }

    addAmount() {
        // add amount and entry to the list
        this.db.hide = "hidden";
        this.control = <FormArray>this.invoiceForm.controls['amountss'];
        this.control.push(this.initAmount());
        this.trigger = "show";
    }

    calculate() {
        this.trigger = "show";
        this.control = <FormArray>this.invoiceForm.controls['amountss'];
        var array = this.control.value;
        this.result = array.map(this.getArray);
        this.invoice.subtotal = this.result.reduce(this.getSum);
        this.discount = this.invoice.subtotal * this.invoice.vat_percentage / 100;
        this.addTax = this.invoice.subtotal - (-this.discount);
        this.invoice.grandtotal = this.addTax * (this.invoice.discount_amount / 100);
        this.invoice.grandtotal = this.addTax - this.invoice.grandtotal;
        
    }


    calculate2() {
        this.invoice.subtotal = this.textData;
        this.discount = this.invoice.subtotal * this.invoice.vat_percentage / 100;
        this.addTax = this.invoice.subtotal - (-this.discount);
        this.invoice.grandtotal = this.addTax * (this.invoice.discount_amount / 100);
        this.invoice.grandtotal = this.addTax - this.invoice.grandtotal;
      
    }


    removeAmount(i: number) {
        // remove amount and entry from the list
        const control = <FormArray>this.invoiceForm.controls['amountss'];
        control.removeAt(i);
    }

    createInvoiceForm() {
        return this.formBuilder.group({
            id: [this.invoice.id],
            rev: [this.invoice.rev],
            vat_percentage: ({
                value: this.invoice.vat_percentage,
                var: [this.vat = this.invoice.vat_percentage]
            }),
            invoice_number: ({
                value: this.invoice.invoice_number,
                disabled: true
            }),
            patient_id: [this.invoice.patient_id],
            notstatus: [this.invoice.notstatus],
            patient_name: [this.invoice.patient_name],
            title: [this.invoice.title],
            paddress: [this.invoice.paddress],
            pnumber: [this.invoice.pnumber],
            pemail: [this.invoice.pemail],
            invoice_entries: this.formBuilder.array([
                this.initAmount(),
            ]),
            creation_timestamp: [this.invoice.creation_timestamp],
            due_timestamp: [this.invoice.due_timestamp],
            status: [this.invoice.status],
            discount_amount: ({
                value: this.invoice.discount_amount,
                disabled: true,
                //var: [this.dis = this.invoice.discount_amount]
            }),
            insurancename: ({
                value: this.invoice.insurancename,
                disabled: true
            }),
            subtotal: ({
                disabled: true,
                value: this.invoice.subtotal,
                //var: [this.final = this.invoice.subtotal],

            }),
            grandtotal: ({
                value: this.invoice.grandtotal,
                disabled: true
            }),
            amountss: this.formBuilder.array([
                this.initAmount()
            ])
        });

    }

}
