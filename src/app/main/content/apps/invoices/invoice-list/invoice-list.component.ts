import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { InvoicesService } from '../invoices.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { FuseInvoicesInvoiceFormDialogComponent } from '../invoice-form/invoice-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Invoice } from '../invoice.model';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { AmountComponent } from '../invoice-form/addtext.component';
import {PouchService} from '../../../../../provider/pouch-service';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';

@Component({
    selector: 'fuse-invoices-invoice-list',
    templateUrl: './invoice-list.component.html',
    styleUrls: ['./invoice-list.component.scss']
})
export class FuseInvoicesInvoiceListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public invoices: Array<Invoice> = [];
    public localStorageItem: any;
    public localStorageType: any;
    invoices2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'invoice', 'title', 'patient', 'entries', 'create', 'due', 'status', 'vat_percentage', 'discount', 'subtotal', 'buttons'];
    selectedInvoices: any[];
    //router:any;
    checkboxes: {};
    public myName;

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;

    constructor(public dialog: MdDialog, public db: PouchService, public router: Router) {
        this.myName = db.getData();
        
        this.localStorageItem = JSON.parse(localStorage.getItem('user'));
        this.localStorageType = this.localStorageItem.usertype;
        
    }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        this._loadInvoices();
        this.db.getInvoices().then(res => {
            
        })

        if(this.localStorageType === "Admin" || this.localStorageType ==="Patient"){
            this.localStorageType = "Admin"
            
        }
    }

    private _loadInvoices(): void {
        this.db.onInvoicesChanged.subscribe(data => {
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        this.db.getInvoices()
            .then((invoices: Array<Invoice>) => {
                this.invoices = invoices;
                this.invoices2 = new BehaviorSubject<any>(invoices);
                //console.log(this.invoices2);

                this.checkboxes = {};
                invoices.map(invoice => {
                    this.checkboxes[invoice.id] = false;
                });
                this.db.onSelectedInvoicesChanged.subscribe(selectedInvoices => {
                    for (const id in this.checkboxes) {
                        this.checkboxes[id] = selectedInvoices.includes(id);
                    }

                    this.selectedInvoices = selectedInvoices;
                });
                this.db.onSearchTextChanged.subscribe(data => {
                    this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                })
            });

    }

    newInvoice() {
        this.dialogRef = this.dialog.open(FuseInvoicesInvoiceFormDialogComponent, {
            panelClass: 'invoice-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup, ) => {
                if (!response) {
                    return;
                }
                var res = response.getRawValue()
                
                res.invoicename = this.db.invoicename;
                this.db.saveInvoice(res);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

            });

    }

    editInvoice(invoice) {
        
        this.dialogRef = this.dialog.open(FuseInvoicesInvoiceFormDialogComponent, {
            panelClass: 'invoice-form-dialog',
            data: {
                invoice: invoice,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':

                        this.db.updateInvoice(formData.getRawValue());
                        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteInvoice(invoice);

                        break;
                }
            });
    }

    /**
     * Delete invoice
     */
    deleteInvoice(invoice) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deleteInvoice(invoice);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(invoiceId) {
        this.db.toggleSelectedInvoice(invoiceId);
    }

    viewInvoice(invoice) {
        this.router.navigate(['/apps/invoicepages', invoice.id]);
    }

}

export class FilesDataSource extends DataSource<any>
{
    invoices2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            of(this.db.invoices).delay(5000),
            this.sort.mdSortChange,
            this._paginator.page,
        ];

        var result = Observable.fromPromise(this.db.getInvoices());
        /* result.subscribe(x => console.log(x), e => console.error(e));
        return result; */

        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.invoices]));
        });
    }

    disconnect() {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
          return data;
        }
    
        return data.sort((a, b) => {
          const isAsc = this.sort.direction === 'asc';
          switch (this.sort.active) {
            case 'patient': return compare(a.patient_name, b.patient_name, isAsc);
            case 'amount': return compare(+a.amount, +b.amount, isAsc);
            case 'id': return compare(+a.id, +b.id, isAsc);
            default: return 0;
          }
        });
      }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

