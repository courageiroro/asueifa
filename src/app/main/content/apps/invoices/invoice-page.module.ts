import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseInvoicesComponent } from './invoices.component';
import { InvoicesService } from './invoices.service';
import { FuseInvoicesInvoiceListComponent } from './invoice-list/invoice-list.component';
import { FuseInvoicesSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseInvoicesInvoiceFormDialogComponent } from './invoice-form/invoice-form.component';
import { AmountComponent } from './invoice-form/addtext.component';
import { FuseInvoicesInvoicePageComponent } from './invoice-page/invoice-page.component';
import { StaffsService } from './../staffs/staffs.service';
import { SettingsService } from './../settings/settings.service';
import { CurrencysService } from './../currencys/currencys.service';
import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from "@angular/core";
import { CurrencyPipe } from '@angular/common';
import { FuseInvoicereceiptsInvoicereceiptFormDialogComponent } from './invoice-receipt/invoice-receipt.component';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path: '**',
        component: FuseInvoicesInvoicePageComponent,
        children: [],
        resolve: {
            invoices: PouchService
        }
    }
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        FuseInvoicesInvoicePageComponent,
        FuseInvoicereceiptsInvoicereceiptFormDialogComponent
    ],
    providers: [
        InvoicesService,
        StaffsService,
        SettingsService,
        CurrencysService,
        PouchService,
        DatePipe,
        CurrencyPipe

    ],
    entryComponents: [FuseInvoicesInvoicePageComponent, FuseInvoicereceiptsInvoicereceiptFormDialogComponent]
})
export class FuseInvoicePageModule {
}
