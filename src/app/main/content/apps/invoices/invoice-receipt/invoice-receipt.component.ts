import { Component, Inject, OnInit, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { InvoicesService } from '../invoices.service';
import { StaffsService } from '../../staffs/staffs.service';
import { SettingsService } from '../../settings/settings.service';
import { CurrencysService } from '../../currencys/currencys.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-invoice-receipts-invoice-receipt-form-dialog',
    templateUrl: './invoice-receipt.component.html',
    styleUrls: ['./invoice-receipt.component.scss'],
    encapsulation: ViewEncapsulation.None,
    outputs: ['childEvent']
})

export class FuseInvoicereceiptsInvoicereceiptFormDialogComponent implements OnInit {

    event: CalendarEvent;
    dialogTitle: string;
    invoicereceiptForm: FormGroup;
    childEvent = new EventEmitter<string>();
    action: string;
    public settings;
    public result;
    public final;
    public hospitalName;
    public result2;
    public final2;
    public hospitalAddress;
    public result3;
    public final3;
    public hospitalPhone;
    public result4;
    public final4;
    public hospitalEmail;
    public Pid;
    public invoiceDate: any;
    public invoiceMonth: any;
    public invoiceNumber: any;
    public invoiceDueDate: any;
    public staffName: any;
    public basicSalary: any;
    public userType: any;
    public allowancess;
    public deductionss;
    public staffId;
    public staffAddress;
    public staffNumber;
    public staffEmail;
    public subTotal;
    public vat;
    public discount;
    public total;
    public currencys;
    public result5;
    public final5;
    public displayCurrencys;
    public unitss;
    public patientId;
    public patientAddress;
    public patientNumber;
    public patientEmail;
    public patientName: any;
    public productName: any;


    constructor(
        public dialogRef: MdDialogRef<FuseInvoicereceiptsInvoicereceiptFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        public db: PouchService,
        public router: Router,
        public activateRoute: ActivatedRoute
    ) {
        this.Pid = data.pid
        this.dialogTitle = 'invoice Receipt';
        /* 
                if (this.action === 'edit') {
                    this.dialogTitle = 'Edit invoice-receipt';
                    this.invoice-receipt = data.invoice-receipt;
                }
                else {
                    this.dialogTitle = 'New invoice-receipt';
                    this.invoice-receipt = {
                        id: '',
                        rev: '',
                        name: '',
                        description: '',
                        doctors: []
                    } */


        //this.invoice-receiptForm = this.createinvoice-receiptForm();
    }

    getArray(gets) {
        var mine = [gets.name].join()
        return mine

    }
    getAddress(gets) {
        var mine = [gets.address].join()
        return mine

    }
    getPhone(gets) {
        var mine = [gets.phone].join()
        return mine

    }
    getEmail(gets) {
        var mine = [gets.email].join()
        return mine

    }

    getCurrency(gets) {
        var mine = [gets.currency_symbol].join()
        return mine

    }

    getAmount(gets) {
        var mine = [gets.amount].join()
        return mine

    }

    ngOnInit() {

        this.db.getInvoice(this.Pid).then(res => {
           
            this.invoiceDate = new Date(res.creation_timestamp).toDateString()
            this.invoiceNumber = res.invoice_number;
            this.invoiceDueDate = new Date(res.due_timestamp).toDateString();
            this.patientName = res.patient_name;
            this.patientId = res.patient_id;
            this.productName = res.title;
            this.patientAddress = res.paddress;
            this.patientNumber = res.pnumber;
            this.patientEmail = res.pemail;
            this.unitss = res.amountss;
            this.subTotal = res.subtotal;
            this.vat = res.vat_percentage;
            this.discount = res.discount_amount;
            this.total = res.grandtotal;
            //console.log(this.arrayAmount)

            this.db.getCurrencys().then(res => {
                this.currencys = res;
                this.result5 = this.currencys.map(this.getCurrency)
                this.final5 = this.result5.length - 1;
                this.displayCurrencys = this.result5[this.final5]
                
                /*  this.arrayAmount = this.unitss.map(this.getAmount)
                 console.log(this.arrayAmount.join())
                 for (this.i = 0; this.i <= this.arrayAmount.length-1; this.i++) {
                     this.formatted = "\n" + (new CurrencyPipe('en-US')).transform(this.arrayAmount[this.i], this.displayCurrencys.trim(), true)+ "<br>";
                     console.log(this.formatted)
                     this.newArray = [];
                     this.newArray.push(this.formatted.toString());
                     console.log(this.newArray);
                 } */
            })


        })


        this.db.getSettings().then(res => {
            this.settings = res;
            this.result = this.settings.map(this.getArray)
            this.final = this.result.length - 1;
            this.hospitalName = this.result[this.final]

            this.result2 = this.settings.map(this.getAddress)
            this.final2 = this.result2.length - 1;
            this.hospitalAddress = this.result2[this.final2]

            this.result3 = this.settings.map(this.getPhone)
            this.final3 = this.result3.length - 1;
            this.hospitalPhone = this.result3[this.final3]

            this.result4 = this.settings.map(this.getEmail)
            this.final4 = this.result4.length - 1;
            this.hospitalEmail = this.result4[this.final4]
        })


        //DATE PIPE
        //this.birthday = new Date;
        /* this.datePipe.transform(this.birthday, 'yyyy-MM-dd');
        console.log(this.datePipe.transform(this.birthday, 'yyyy-MM-dd')); */
        //END
    }

    print() {

        window.print();

    }

    createinvoicereceiptForm() {
        /*  return this.formBuilder.group({
             id: [this.invoice-receipt.id],
             rev: [this.invoice-receipt.rev],
             name: [this.invoice-receipt.name],
             description: [this.invoice-receipt.description],
 
         });*/
    }
}
