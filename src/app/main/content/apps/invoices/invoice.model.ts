import { Amount } from './invoice-form/addtext.model';
export interface Invoice {
    id: string,
    rev: string,
    invoice_number: number,
    patient_name: string,
    patient_id: string,
    title: string,
    paddress: string,
    pnumber: string,
    pemail: string,
    invoice_entries: Amount[],
    creation_timestamp: string,
    due_timestamp: string,
    status: string,
    vat_percentage: number,
    discount_amount: number,
    subtotal: number,
    grandtotal: number,
    amountss: Amount[],
    insurancename:'',
    notstatus: any
}

