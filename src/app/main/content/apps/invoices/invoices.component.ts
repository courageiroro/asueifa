import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { InvoicesService } from './invoices.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector: 'fuse-invoices',
    templateUrl: './invoices.component.html',
    styleUrls: ['./invoices.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [Animations.slideInTop]
})
export class FuseInvoicesComponent implements OnInit {
    hasSelectedInvoices: boolean;
    searchInput: FormControl;
    public myName;

    constructor(private invoicesService: PouchService) {
      
        this.searchInput = new FormControl('');
    }

    ngOnInit() {

        this.invoicesService.onSelectedInvoicesChanged
            .subscribe(selectedInvoices => {
                this.hasSelectedInvoices = selectedInvoices.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.invoicesService.onSearchTextChanged.next(searchText);
            });
    }

}
