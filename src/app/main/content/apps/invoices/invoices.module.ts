import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseInvoicesComponent } from './invoices.component';
import { InvoicesService } from './invoices.service';
import { FuseInvoicesInvoiceListComponent } from './invoice-list/invoice-list.component';
import { FuseInvoicesSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseInvoicesInvoiceFormDialogComponent } from './invoice-form/invoice-form.component';
import {AmountComponent} from './invoice-form/addtext.component'
import {FuseInvoicesInvoicePageComponent} from './invoice-page/invoice-page.component'
import { StaffsService } from './../staffs/staffs.service';
import { Staff } from './../staffs/staff.model';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/invoices',
        component: FuseInvoicesComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            invoices: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseInvoicesComponent,
        FuseInvoicesInvoiceListComponent,
        FuseInvoicesSelectedBarComponent,
        FuseInvoicesInvoiceFormDialogComponent,
        AmountComponent
    ],
    providers      : [
        InvoicesService,
        PouchService,
        StaffsService,
        AuthGuard
    ],
    entryComponents: [AmountComponent,FuseInvoicesInvoiceFormDialogComponent]
})
export class FuseInvoicesModule
{
}
