/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Component, Injectable, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import { DatePipe } from '@angular/common';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Invoice } from './invoice.model';
import { Schema } from '../schema';

export interface myData {
    name: string;
}

@Injectable()
export class InvoicesService {

    sharingData: myData = { name: "nyks" };
    saveData(name) {
        
        this.sharingData.name = name;

    }
    getData() {
      
        return this.sharingData.name;

    }

    public invoicename = "";
    public hide = "";
    public sCredentials;

    onInvoicesChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedInvoicesChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    invoices: Invoice[];
    user: any;
    selectedInvoices: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The invoices App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getInvoices()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getInvoices();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getInvoices();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('asueifai', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/asueifai';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/asueifai';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }

    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }

    /***********
     * invoice
     **********/
    /**
     * Save a invoice
     * @param {invoice} invoice
     *
     * @return Promise<invoice>
     */
    saveInvoice(invoice: Invoice): Promise<Invoice> {
        
        invoice.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('invoice', invoice)
            .then((data: any) => {

                if (data && data.invoices && data.invoices
                [0]) {
                    
                    return data.invoices[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a invoice
    * @param {invoice} invoice
    *
    * @return Promise<invoice>
    */
    updateInvoice(invoice: Invoice) {

        return this.db.rel.save('invoice', invoice)
            .then((data: any) => {
                if (data && data.invoices && data.invoices
                [0]) {
                    return data.invoices[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a invoice
     * @param {invoice} invoice
     *
     * @return Promise<boolean>
     */
    removeInvoice(invoice: Invoice): Promise<boolean> {
        return this.db.rel.del('invoice', invoice)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the invoices
     *
     * @return Promise<Array<invoice>>
     */
    getInvoices(): Promise<Array<Invoice>> {
        var localStorageItem = JSON.parse(localStorage.getItem('user'));
        return this.db.rel.find('invoice')
            .then((data: any) => {
                this.invoices = data.invoices;
                if (this.searchText && this.searchText !== '') {
                    this.invoices = FuseUtils.filterArrayByString(this.invoices, this.searchText);
                }
                //this.oninvoicesChanged.next(this.invoices);
                var userId = localStorageItem.id
                if (localStorageItem.usertype == "Patient") {
                    this.invoices = this.invoices.filter(data => data.patient_id == userId)
                }
                return Promise.resolve(this.invoices);
                //return data.invoices ? data.invoices : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a invoice
     * @param {invoice} invoice
     *
     * @return Promise<invoice>
     */
    getInvoice(id): Promise<Invoice> {
        return this.db.rel.find('invoice', id)
            .then((data: any) => {
                return data && data.invoices ? data.invoices[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected invoice by id
     * @param id
     */
    toggleSelectedInvoice(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedInvoices.length > 0) {
            const index = this.selectedInvoices.indexOf(id);

            if (index !== -1) {
                this.selectedInvoices.splice(index, 1);

                // Trigger the next event
                this.onSelectedInvoicesChanged.next(this.selectedInvoices);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedInvoices.push(id);

        // Trigger the next event
        this.onSelectedInvoicesChanged.next(this.selectedInvoices);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedInvoices.length > 0) {
            this.deselectInvoices();
        }
        else {
            this.selectInvoices();
        }
    }

    selectInvoices(filterParameter?, filterValue?) {
        this.selectedInvoices = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedInvoices = [];
            this.invoices.map(invoice => {
                this.selectedInvoices.push(invoice.id);
            });
        }
        else {
            /* this.selectedinvoices.push(...
                 this.invoices.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedInvoicesChanged.next(this.selectedInvoices);
    }





    deselectInvoices() {
        this.selectedInvoices = [];

        // Trigger the next event
        this.onSelectedInvoicesChanged.next(this.selectedInvoices);
    }

    deleteInvoice(invoice) {
        this.db.rel.del('invoice', invoice)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const invoiceIndex = this.invoices.indexOf(invoice);
                this.invoices.splice(invoiceIndex, 1);
                this.onInvoicesChanged.next(this.invoices);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedInvoices() {
        for (const invoiceId of this.selectedInvoices) {
            const invoice = this.invoices.find(_invoice => {
                return _invoice.id === invoiceId;
            });

            this.db.rel.del('invoice', invoice)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const invoiceIndex = this.invoices.indexOf(invoice);
                    this.invoices.splice(invoiceIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onInvoicesChanged.next(this.invoices);
        this.deselectInvoices();
    }
}
