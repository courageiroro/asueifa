import { Component, OnInit } from '@angular/core';
import { InvoicesService } from '../invoices.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseInvoicesSelectedBarComponent implements OnInit
{
    selectedInvoices: string[];
    hasSelectedInvoices: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private invoicesService: PouchService,
        public dialog: MdDialog
    )
    {
        this.invoicesService.onSelectedInvoicesChanged
            .subscribe(selectedInvoices => {
                this.selectedInvoices = selectedInvoices;
                setTimeout(() => {
                    this.hasSelectedInvoices = selectedInvoices.length > 0;
                    this.isIndeterminate = (selectedInvoices.length !== this.invoicesService.invoices.length && selectedInvoices.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.invoicesService.selectInvoices();
    }

    deselectAll()
    {
        this.invoicesService.deselectInvoices();
    }

    deleteSelectedInvoices()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected invoicees?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.invoicesService.deleteSelectedInvoices();
            }
            this.confirmDialogRef = null;
        });
    }

}
