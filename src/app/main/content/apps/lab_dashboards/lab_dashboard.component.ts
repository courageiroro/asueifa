import { startOfDay, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth, addHours } from 'date-fns';
import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { MdDialog, MdDialogRef } from '@angular/material';
//import { FuseNoticesNoticeFormDialogComponent } from './notice-form/notice-form.component';
import { FormGroup } from '@angular/forms';
import { Calendar } from './lab_dashboard.model';
import { Lab_dashboardsService } from './lab_dashboards.service';
import {
    CalendarEvent,
    CalendarEventAction,
    CalendarEventTimesChangedEvent,
    CalendarMonthViewDay
} from 'angular-calendar';
import { FuseConfirmDialogComponent } from '../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector: 'fuse-lab_dashboard',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './lab_dashboard.component.html',
    styleUrls: ['./lab_dashboard.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class FuseLab_dashboardComponent implements OnInit {
    view: string;
    localStorageItem;
    localStorageName;

    viewDate: Date;
    getLab_dashboards: any;
    lab_dashboard: Calendar;

    lab_dashboardss: CalendarEvent[];

    public actions: CalendarEventAction[];

    activeDayIsOpen: boolean;

    refresh: Subject<any> = new Subject();

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    selectedDay: any;

    constructor(
        public dialog: MdDialog,
        public lab_dashboardService: PouchService
    ) {
        /*  this.noticeService.getNotices().then(res=>{
            this.getNotices = res;
        }) */

        this.view = 'month';
        this.viewDate = new Date();
        this.activeDayIsOpen = true;
        this.selectedDay = { date: startOfDay(new Date()) };

        this.actions = [
            {
                label: '<i class="material-icons s-16">edit</i>',
                onClick: ({ event }: { event: CalendarEvent }): void => {
                    this.editLab_dashboard('edit', event);
                }
            },
            {
                label: '<i class="material-icons s-16">delete</i>',
                onClick: ({ event }: { event: CalendarEvent }): void => {
                    this.deleteLab_dashboard(event);
                }
            }
        ];

        /**
         * Get events from service/server
         */
        this.setLab_dashboards();
    }

    ngOnInit() {
        /**
         * Watch re-render-refresh for updating db
         */
        this.refresh.subscribe(updateDB => {
            //console.warn('REFRESH');
            if (updateDB) {
                //console.warn('UPDATE DB');
                this.lab_dashboardService.updateLab_dashboard(this.lab_dashboard);
            }
        });

        this.lab_dashboardService.onEventsUpdated.subscribe(events => {
            this.setLab_dashboards();
            this.refresh.next();
        });

        this.localStorageItem = JSON.parse(localStorage.getItem('user'));
        this.localStorageName = this.localStorageItem.usertype;
    }


    setLab_dashboards() {
        
        this.lab_dashboardss = this.lab_dashboardService.lab_dashboards.map(item => {
            item.actions = this.actions;
            
            return new Calendar(item);
        });
    }

    /**
     * Before View Renderer
     * @param {any} header
     * @param {any} body
     */
    beforeMonthViewRender({ header, body }) {
        // console.info('beforeMonthViewRender');
        /**
         * Get the selected day
         */
        const _selectedDay = body.find((_day) => {
            return _day.date.getTime() === this.selectedDay.date.getTime();
        });

        if (_selectedDay) {
            /**
             * Set selectedday style
             * @type {string}
             */
            _selectedDay.cssClass = 'mat-elevation-z3';
        }

    }

    /**
     * Day clicked
     * @param {MonthViewDay} day
     */
    dayClicked(day: CalendarMonthViewDay): void {
        const date: Date = day.date;
        const events: CalendarEvent[] = day.events;

        if (isSameMonth(date, this.viewDate)) {
            if ((isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) || events.length === 0) {
                this.activeDayIsOpen = false;
            }
            else {
                this.activeDayIsOpen = true;
                this.viewDate = date;
            }
        }
        this.selectedDay = day;
        this.refresh.next();
    }

    /**
     * Event times changed
     * Event dropped or resized
     * @param {CalendarEvent} event
     * @param {Date} newStart
     * @param {Date} newEnd
     */
    eventTimesChanged({ event, newStart, newEnd }: CalendarEventTimesChangedEvent): void {
        event.start = newStart;
        event.end = newEnd;
        // console.warn('Dropped or resized', event);
        this.refresh.next(true);
    }

    /**
     * Delete Event
     * @param event
     */
    deleteLab_dashboard(lab_dashboard) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.lab_dashboardService.deleteLab_dashboard(lab_dashboard);
                const lab_dashboardIndex = this.lab_dashboardss.indexOf(lab_dashboard);
                this.lab_dashboardss.splice(lab_dashboardIndex, 1);
                this.refresh.next(true);
            }
            this.confirmDialogRef = null;
        });

    }

    /**
     * Edit Event
     * @param {string} action
     * @param {CalendarEvent} event
     */
    editLab_dashboard(action: string, lab_dashboards: CalendarEvent) {
        const eventIndex = this.lab_dashboardss.indexOf(lab_dashboards);
        
        /*   this.dialogRef = this.dialog.open(FuseNoticesNoticeFormDialogComponent, {
              panelClass: 'event-form-dialog',
              data: {
                  lab_dashboards: lab_dashboards,
                  action: action
  
              }
          }); */

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':
                        this.lab_dashboardss[eventIndex] = Object.assign(this.lab_dashboardss[eventIndex], formData.getRawValue());
                        this.lab_dashboardService.updateLab_dashboards(formData.getRawValue());
                        this.refresh.next(true);
                      

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteLab_dashboard(event);

                        break;
                }
            });
    }

    /**
     * Add Event
     */
    addNotice(): void {
        /* this.dialogRef = this.dialog.open(FuseNoticesNoticeFormDialogComponent, {
            panelClass: 'event-form-dialog',
            data: {
                action: 'new',
                date: this.selectedDay.date
            }
        }); */
        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
                const newLab_dashboard = response.getRawValue();
                /*  newNotice.actions = this.actions; */
                this.lab_dashboardss.push(newLab_dashboard);
                this.lab_dashboardService.saveLab_dashboard(newLab_dashboard);
                
                this.refresh.next(true);
            });
    }
}


