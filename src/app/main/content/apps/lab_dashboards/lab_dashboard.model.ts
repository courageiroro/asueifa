import {
    CalendarEventAction
} from 'angular-calendar';

import {
    startOfDay,
    endOfDay,
    subDays,
    addDays,
    endOfMonth,
    isSameDay,
    isSameMonth,
    addHours
} from 'date-fns';

export class Calendar {
    id: string;
    rev: string;
    title: string;
    description: string;
    start: Date;
    end: Date;
    color: {
        primary: string;
        secondary: string;
    };
    actions?: CalendarEventAction[];
    cssClass?: string;
    resizable?: {
        beforeStart?: boolean;
        afterEnd?: boolean;
    };
    draggable?: boolean;
    noticeboards: Array<string>

    constructor(data?) {
        data = data || {};
        this.id = data.id || '';
        this.rev = data.rev || '';
        this.start = new Date(data.start) || startOfDay(new Date());
        this.end = new Date(data.end) || endOfDay(new Date());
        this.title = data.title || '';
        this.description = data.description || '';
        this.noticeboards = data.noticeboards || [];
        this.actions = data.actions || [];
        this.cssClass = data.cssClass || '';
        this.resizable = {
            beforeStart: data.resizable && data.resizable.beforeStart || true,
            afterEnd: data.resizable && data.resizable.afterEnd || true
        };
        this.draggable = data.draggable || true;
        this.color = {
            primary: data.color && data.color.primary || '#1e90ff',
            secondary: data.color && data.color.secondary || '#D1E8FF'
        };
    }

}


