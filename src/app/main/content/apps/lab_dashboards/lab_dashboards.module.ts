import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseLab_dashboardComponent } from './lab_dashboard.component';
import { Lab_dashboardsService } from './lab_dashboards.service';
//import { FuseNoticesNoticeFormDialogComponent } from './notice-form/notice-form.component';
//import { FuseNoticesNoticeListComponent } from './notice-list/notice-list.component';
import { FuseLab_dashboardsSelectedBarComponent } from './selected-bar/selected-bar.component';
//import { FuseNoticesNoticeFormDialogComponent } from './notice-form/notice-form.component';
import { CalendarModule } from 'angular-calendar';
import {ColorPickerModule} from 'angular4-color-picker';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/lab_dashboards',
        component: FuseLab_dashboardComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            lab_dashboards: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes),
        CalendarModule.forRoot(),
        ColorPickerModule
    ],
    declarations   : [
        //FuseNoticesNoticeFormDialogComponent,
        FuseLab_dashboardComponent,
        FuseLab_dashboardsSelectedBarComponent,
        //FuseNoticesNoticeFormDialogComponent
    ],
    providers      : [
    Lab_dashboardsService,
    PouchService,
    AuthGuard
    ],
    entryComponents: []
})
export class FuseLab_dashboardsModule
{
}
