/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Calendar } from './lab_dashboard.model';
import { Schema } from '../schema';

@Injectable()
export class Lab_dashboardsService {
    public lab_dashboardname = "";
    lab_dashboardss: any;
    onLab_dashboardsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedLab_dashboardsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    lab_dashboards: Calendar[];
    user: any;
    selectedLab_dashboards: string[] = [];
    onEventsUpdated = new Subject<any>();
    searchText: string;
    filterBy: string;
    public sCredentials;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The notices App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getLab_dashboards()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getLab_dashboards();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getLab_dashboards();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('asueifai', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/asueifai';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/asueifai';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }

    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }


    /***********
     * lab_dashboard
     **********/
    /**
     * Save a lab_dashboard
     * @param {lab_dashboard} lab_dashboard
     *
     * @return Promise<lab_dashboard>
     */
    saveLab_dashboard(lab_dashboard: Calendar): Promise<Calendar> {
        lab_dashboard.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('lab_dashboard', lab_dashboard)
            .then((data: any) => {

                if (data && data.lab_dashboards && data.lab_dashboards
                [0]) {
                    
                    return data.lab_dashboards[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a lab_dashboard
    * @param {lab_dashboard} lab_dashboard
    *
    * @return Promise<lab_dashboard>
    */
    updateLab_dashboard(lab_dashboard: Calendar) {

        return this.db.rel.save('lab_dashboard', lab_dashboard)
            .then((data: any) => {
                if (data && data.lab_dashboards && data.lab_dashboards
                [0]) {
                    
                    return data.lab_dashboards[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    updateLab_dashboards(lab_dashboard: Promise<Array<Calendar>>) {

        return this.db.rel.save('lab_dashboard', lab_dashboard)
            .then((data: any) => {
                if (data && data.lab_dashboards && data.lab_dashboards
                [0]) {
                    return data.lab_dashboards[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a lab_dashboard
     * @param {lab_dashboard} lab_dashboard
     *
     * @return Promise<boolean>
     */
    removeLab_dashboard(lab_dashboard: Calendar): Promise<boolean> {
        return this.db.rel.del('lab_dashboard', lab_dashboard)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the lab_dashboards
     *
     * @return Promise<Array<lab_dashboard>>
     */
    getLab_dashboards(): Promise<Array<Calendar>> {
        return this.db.rel.find('lab_dashboard')
            .then((data: any) => {
                this.lab_dashboards = data.lab_dashboards;
                
                this.onEventsUpdated.next(this.lab_dashboards);
                if (this.searchText && this.searchText !== '') {
                    this.lab_dashboards = FuseUtils.filterArrayByString(this.lab_dashboards, this.searchText);
                }
                //this.onnoticesChanged.next(this.lab_dashboards);
                return Promise.resolve(this.lab_dashboards);
                //return data.lab_dashboards ? data.lab_dashboards : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Read a lab_dashboard
     * @param {lab_dashboard} lab_dashboard
     *
     * @return Promise<lab_dashboard>
     */
    getLab_dashboard(lab_dashboard: Calendar): Promise<Calendar> {
        return this.db.rel.find('lab_dashboard', lab_dashboard.id)
            .then((data: any) => {
                //this.onEventsUpdated.next(this.notices);
                return data && data.notices ? data.notices[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected notice by id
     * @param id
     */
    toggleSelectedLab_dashboard(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedLab_dashboards.length > 0) {
            const index = this.selectedLab_dashboards.indexOf(id);

            if (index !== -1) {
                this.selectedLab_dashboards.splice(index, 1);

                // Trigger the next event
                this.onSelectedLab_dashboardsChanged.next(this.selectedLab_dashboards);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedLab_dashboards.push(id);

        // Trigger the next event
        this.onSelectedLab_dashboardsChanged.next(this.selectedLab_dashboards);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedLab_dashboards.length > 0) {
            this.deselectLab_dashboards();
        }
        else {
            this.selectLab_dashboards();
        }
    }

    selectLab_dashboards(filterParameter?, filterValue?) {
        this.selectedLab_dashboards = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedLab_dashboards = [];
            this.lab_dashboards.map(notice => {
                this.selectedLab_dashboards.push(notice.id);
            });
        }
        else {
            /* this.selectednotices.push(...
                 this.notices.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedLab_dashboardsChanged.next(this.selectedLab_dashboards);
    }





    deselectLab_dashboards() {
        this.selectedLab_dashboards = [];

        // Trigger the next event
        this.onSelectedLab_dashboardsChanged.next(this.selectedLab_dashboards);
    }

    deleteLab_dashboard(lab_dashboard) {
        this.db.rel.del('lab_dashboard', lab_dashboard)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const lab_dashboardIndex = this.lab_dashboards.indexOf(lab_dashboard);
                this.lab_dashboards.splice(lab_dashboardIndex, 1);
                this.onLab_dashboardsChanged.next(this.lab_dashboards);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedLab_dashboards() {
        for (const lab_dashboardId of this.selectedLab_dashboards) {
            const lab_dashboard = this.lab_dashboards.find(_lab_dashboard => {
                return _lab_dashboard.id === lab_dashboardId;
            });

            this.db.rel.del('lab_dashboard', lab_dashboard)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const lab_dashboardIndex = this.lab_dashboards.indexOf(lab_dashboard);
                    this.lab_dashboards.splice(lab_dashboardIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onLab_dashboardsChanged.next(this.lab_dashboards);
        this.deselectLab_dashboards();
    }
}
