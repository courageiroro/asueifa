/* import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Notice } from '../notice.model';
import { MatColors } from '../../../../../core/matColors';
import 'rxjs/Rx';
import { ColorPickerService } from 'angular4-color-picker';

@Component({
    selector: 'fuse-notice-notice-form-dialog',
    templateUrl: './notice-form.component.html',
    styleUrls: ['./notice-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseNoticesNoticeFormDialogComponent implements OnInit {
    //private color: string = "#127bdc";

    notices: CalendarEvent;
    notice: Notice;
    dialogTitle: string;
    noticeForm: FormGroup;
    action: string;
    presetColors = MatColors.presets;

    constructor(
        public dialogRef: MdDialogRef<FuseNoticesNoticeFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        private cpService: ColorPickerService
    ) {
        this.notices = data.notices;
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Notice';
            this.notices = data.notices;
        }
        else {
            this.dialogTitle = 'New Notice';
            this.notices = new Notice({
                id:data.id,
                rev:data.rev,
                start: data.date,
                end: data.date

            })
        }

        this.noticeForm = this.createNoticeForm();
    }

    ngOnInit() {
    }


    createNoticeForm() {
        return new FormGroup({
            id: new FormControl(this.notices.id),
            rev: new FormControl(this.notices.rev),
            title: new FormControl(this.notices.title),
            start: new FormControl(this.notices.start),
            end: new FormControl(this.notices.end),
            description: new FormControl(this.notices.description),
            color: this.formBuilder.group({
                primary: new FormControl(this.notices.color.primary),
                secondary: new FormControl(this.notices.color.secondary)
            }),
        });
    }
}
 */