import { Component, OnInit } from '@angular/core';
import { Lab_dashboardsService } from '../lab_dashboards.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseLab_dashboardsSelectedBarComponent implements OnInit
{
    selectedLab_dashboards: string[];
    hasSelectedLab_dashboards: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private lab_dashboardsService: PouchService,
        public dialog: MdDialog
    )
    {
        this.lab_dashboardsService.onSelectedLab_dashboardsChanged
            .subscribe(selectedLab_dashboards => {
                this.selectedLab_dashboards = selectedLab_dashboards;
                setTimeout(() => {
                    this.hasSelectedLab_dashboards = selectedLab_dashboards.length > 0;
                    this.isIndeterminate = (selectedLab_dashboards.length !== this.lab_dashboardsService.lab_dashboards.length && selectedLab_dashboards.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.lab_dashboardsService.selectLab_dashboards();
    }

    deselectAll()
    {
        this.lab_dashboardsService.deselectLab_dashboards();
    }

    deleteSelectedLab_dashboards()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected noticees?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.lab_dashboardsService.deleteSelectedLab_dashboards();
            }
            this.confirmDialogRef = null;
        });
    }

}
