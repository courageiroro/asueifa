import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Laboratorist } from '../laboratorist.model';

@Component({
    selector: 'fuse-laboratorists-laboratorist-form-dialog',
    templateUrl: './laboratorist-form.component.html',
    styleUrls: ['./laboratorist-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseLaboratoristsLaboratoristFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    laboratoristForm: FormGroup;
    action: string;
    laboratorist: Laboratorist;

    constructor(
        public dialogRef: MdDialogRef<FuseLaboratoristsLaboratoristFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Laboratorist';
            this.laboratorist = data.laboratorist;
        }
        else {
            this.dialogTitle = 'New Laboratorist';
            this.laboratorist = {
                id: '',
                rev: '',
                name: '',
                email: '',
                password: '',
                address: '',
                phone: '',
                diagnosis_reports: []
            }
        }

        this.laboratoristForm = this.createLaboratoristForm();
    }

    ngOnInit() {
    }

    createLaboratoristForm() {
        return this.formBuilder.group({
            id: [this.laboratorist.id],
            rev: [this.laboratorist.rev],
            name: [this.laboratorist.name],
            email: [this.laboratorist.email],
            password: [this.laboratorist.password],
            address: [this.laboratorist.address],
            phone: [this.laboratorist.phone]

        });
    }
}
