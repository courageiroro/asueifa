import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { LaboratoristsService } from '../laboratorists.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject} from 'rxjs/Rx';
import { FuseLaboratoristsLaboratoristFormDialogComponent } from '../laboratorist-form/laboratorist-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Laboratorist } from '../laboratorist.model';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {PouchService} from '../../../../../provider/pouch-service';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';

@Component({
    selector   : 'fuse-laboratorists-laboratorist-list',
    templateUrl: './laboratorist-list.component.html',
    styleUrls  : ['./laboratorist-list.component.scss']
})
export class FuseLaboratoristsLaboratoristListComponent implements OnInit
{
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public laboratorists: Array<Laboratorist> = [];
    laboratorists2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'name','address','phone','email', 'buttons'];
    selectedLaboratorists: any[];
    checkboxes: {};

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;

    constructor(public dialog: MdDialog, public db:PouchService,public router: Router)
    { }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        this._loadLaboratorists();
    }

    private _loadLaboratorists(): void {
         this.db.onLaboratoristsChanged.subscribe(data =>{
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        this.db.getLaboratorists()
        .then((laboratorists: Array<Laboratorist>) => {
            this.laboratorists = laboratorists;
            this.laboratorists2= new BehaviorSubject<any>(laboratorists);

            this.checkboxes = {};
            laboratorists.map(laboratorist => {
                this.checkboxes[laboratorist.id] = false;
            });
             this.db.onSelectedLaboratoristsChanged.subscribe(selectedLaboratorists => {
                for ( const id in this.checkboxes )
                {
                    this.checkboxes[id] = selectedLaboratorists.includes(id);
                }

                this.selectedLaboratorists = selectedLaboratorists;
            });
        this.db.onSearchTextChanged.subscribe(data =>{
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        });
        
    }

   /*  viewLabReport(report){
        this.router.navigate(['/apps/laboratoristpages',report.id]);
    } */

    newLaboratorist()
    {
        this.dialogRef = this.dialog.open(FuseLaboratoristsLaboratoristFormDialogComponent, {
            panelClass: 'laboratorist-form-dialog',
            data      : {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if ( !response )
                {
                    return;
                }

                this.db.saveLaboratorist(response.getRawValue());
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

            });

    }

    editLaboratorist(laboratorist)
    {
        this.dialogRef = this.dialog.open(FuseLaboratoristsLaboratoristFormDialogComponent, {
            panelClass: 'laboratorist-form-dialog',
            data      : {
                laboratorist: laboratorist,
                action : 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch ( actionType )
                {
                    /**
                     * Save
                     */
                    case 'save':

                        this.db.updateLaboratorist(formData.getRawValue());
                        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteLaboratorist(laboratorist);

                        break;
                }
            });
    }

    /**
     * Delete laboratorist
     */
    deleteLaboratorist(laboratorist)
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.db.deleteLaboratorist(laboratorist);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(laboratoristId)
    {
        this.db.toggleSelectedLaboratorist(laboratoristId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    laboratorists2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort)
    {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]>
    {
        const displayDataChanges = [
            of(this.db.laboratorists).delay(5000),
            this.sort.mdSortChange,
            this._paginator.page,
        ];


        var result = Observable.fromPromise(this.db.getLaboratorists());
       /*  result.subscribe(x => console.log(x), e => console.error(e));
        return result;     */

        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.laboratorists]));
        });
    }

    disconnect()
    {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
          return data;
        }
    
        return data.sort((a, b) => {
          const isAsc = this.sort.direction === 'asc';
          switch (this.sort.active) {
            case 'name': return compare(a.name, b.name, isAsc);
            case 'amount': return compare(+a.amount, +b.amount, isAsc);
            case 'id': return compare(+a.id, +b.id, isAsc);
            default: return 0;
          }
        });
      }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
