export interface Laboratorist {
    id: string,
    rev: string,
    name: string,
    email: string,
    password: string,
    address: string,
    phone: string,
    diagnosis_reports: Array<string>
}

