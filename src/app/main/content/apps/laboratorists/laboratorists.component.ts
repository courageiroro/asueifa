import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { LaboratoristsService } from './laboratorists.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-laboratorists',
    templateUrl  : './laboratorists.component.html',
    styleUrls    : ['./laboratorists.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FuseLaboratoristsComponent implements OnInit
{
    hasSelectedLaboratorists: boolean;
    searchInput: FormControl;

    constructor(private laboratoristsService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.laboratoristsService.onSelectedLaboratoristsChanged
            .subscribe(selectedLaboratorists => {
                this.hasSelectedLaboratorists = selectedLaboratorists.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.laboratoristsService.onSearchTextChanged.next(searchText);
            });
    }

}
