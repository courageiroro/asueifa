import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseLaboratoristsComponent } from './laboratorists.component';
import { LaboratoristsService } from './laboratorists.service';
import { FuseLaboratoristsLaboratoristListComponent } from './laboratorist-list/laboratorist-list.component';
import { FuseLaboratoristsSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseLaboratoristsLaboratoristFormDialogComponent } from './laboratorist-form/laboratorist-form.component';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/laboratorists',
        component: FuseLaboratoristsComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            laboratorists: LaboratoristsService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseLaboratoristsComponent,
        FuseLaboratoristsLaboratoristListComponent,
        FuseLaboratoristsSelectedBarComponent,
        FuseLaboratoristsLaboratoristFormDialogComponent
    ],
    providers      : [
        LaboratoristsService,
        PouchService,
        AuthGuard
    ],
    entryComponents: [FuseLaboratoristsLaboratoristFormDialogComponent]
})
export class FuseLaboratoristsModule
{
}
