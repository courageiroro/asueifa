/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Laboratorist } from './laboratorist.model';
import { Schema } from '../schema';

@Injectable()
export class LaboratoristsService {
    public name = "";

    onLaboratoristsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedLaboratoristsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    laboratorists: Laboratorist[];
    user: any;
    selectedLaboratorists: string[] = [];

    searchText: string;
    filterBy: string;
    public sCredentials;
    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The laboratorists App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getLaboratorists()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getLaboratorists();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getLaboratorists();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('asueifai', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/asueifai';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/asueifai';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }


    /***********
     * laboratorist
     **********/
    /**
     * Save a laboratorist
     * @param {laboratorist} laboratorist
     *
     * @return Promise<laboratorist>
     */
    saveLaboratorist(laboratorist: Laboratorist): Promise<Laboratorist> {
        laboratorist.id = Math.floor(Date.now()).toString();
        laboratorist.diagnosis_reports = [];

        return this.db.rel.save('laboratorist', laboratorist)
            .then((data: any) => {
                //console.log(data);
                //console.log(laboratorist);
                if (data && data.laboratorists && data.laboratorists
                [0]) {
                    return data.laboratorists[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a laboratorist
    * @param {laboratorist} Laboratorist
    *
    * @return Promise<Laboratorist>
    */
    updateLaboratorist(laboratorist: Laboratorist) {

        return this.db.rel.save('laboratorist', laboratorist)
            .then((data: any) => {
                if (data && data.laboratorists && data.laboratorists
                [0]) {
                    return data.laboratorists[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a laboratorist
     * @param {laboratorist} laboratorist
     *
     * @return Promise<boolean>
     */
    removeLaboratorist(laboratorist: Laboratorist): Promise<boolean> {
        return this.db.rel.del('laboratorist', laboratorist)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the laboratorists
     *
     * @return Promise<Array<laboratorist>>
     */
    getLaboratorists(): Promise<Array<Laboratorist>> {
        return this.db.rel.find('laboratorist')
            .then((data: any) => {
                this.laboratorists = data.laboratorists;
                if (this.searchText && this.searchText !== '') {
                    this.laboratorists = FuseUtils.filterArrayByString(this.laboratorists, this.searchText);
                }
                //this.onlaboratoristsChanged.next(this.laboratorists);
                return Promise.resolve(this.laboratorists);
                //return data.laboratorists ? data.laboratorists : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getReportLaboratorists(): Promise<Array<Laboratorist>> {
        return this.db.rel.find('laboratorist')
            .then((data: any) => {
                return data.laboratorists ? data.laboratorists : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a laboratorist
     * @param {laboratorist} Laboratorist
     *
     * @return Promise<Laboratorist>
     */
    getLaboratorist(id): Promise<Laboratorist> {
        return this.db.rel.find('laboratorist', id)
            .then((data: any) => {
                return data && data.laboratorists ? data.laboratorists[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected laboratorist by id
     * @param id
     */
    toggleSelectedLaboratorist(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedLaboratorists.length > 0) {
            const index = this.selectedLaboratorists.indexOf(id);

            if (index !== -1) {
                this.selectedLaboratorists.splice(index, 1);

                // Trigger the next event
                this.onSelectedLaboratoristsChanged.next(this.selectedLaboratorists);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedLaboratorists.push(id);

        // Trigger the next event
        this.onSelectedLaboratoristsChanged.next(this.selectedLaboratorists);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedLaboratorists.length > 0) {
            this.deselectLaboratorists();
        }
        else {
            this.selectLaboratorists();
        }
    }

    selectLaboratorists(filterParameter?, filterValue?) {
        this.selectedLaboratorists = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedLaboratorists = [];
            this.laboratorists.map(laboratorist => {
                this.selectedLaboratorists.push(laboratorist.id);
            });
        }
        else {
            /* this.selectedlaboratorists.push(...
                 this.laboratorists.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedLaboratoristsChanged.next(this.selectedLaboratorists);
    }





    deselectLaboratorists() {
        this.selectedLaboratorists = [];

        // Trigger the next event
        this.onSelectedLaboratoristsChanged.next(this.selectedLaboratorists);
    }

    deleteLaboratorist(laboratorist) {
        this.db.rel.del('laboratorist', laboratorist)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const laboratoristIndex = this.laboratorists.indexOf(laboratorist);
                this.laboratorists.splice(laboratoristIndex, 1);
                this.onLaboratoristsChanged.next(this.laboratorists);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedLaboratorists() {
        for (const laboratoristId of this.selectedLaboratorists) {
            const laboratorist = this.laboratorists.find(_laboratorist => {
                return _laboratorist.id === laboratoristId;
            });

            this.db.rel.del('laboratorist', laboratorist)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const laboratoristIndex = this.laboratorists.indexOf(laboratorist);
                    this.laboratorists.splice(laboratoristIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onLaboratoristsChanged.next(this.laboratorists);
        this.deselectLaboratorists();
    }
}
