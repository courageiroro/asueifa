import { Component, OnInit } from '@angular/core';
import { LaboratoristsService } from '../laboratorists.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseLaboratoristsSelectedBarComponent implements OnInit
{
    selectedLaboratorists: string[];
    hasSelectedLaboratorists: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private laboratoristsService: PouchService,
        public dialog: MdDialog
    )
    {
        this.laboratoristsService.onSelectedLaboratoristsChanged
            .subscribe(selectedLaboratorists => {
                this.selectedLaboratorists = selectedLaboratorists;
                setTimeout(() => {
                    this.hasSelectedLaboratorists = selectedLaboratorists.length > 0;
                    this.isIndeterminate = (selectedLaboratorists.length !== this.laboratoristsService.laboratorists.length && selectedLaboratorists.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.laboratoristsService.selectLaboratorists();
    }

    deselectAll()
    {
        this.laboratoristsService.deselectLaboratorists();
    }

    deleteSelectedLaboratorists()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected laboratoristes?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.laboratoristsService.deleteSelectedLaboratorists();
            }
            this.confirmDialogRef = null;
        });
    }

}
