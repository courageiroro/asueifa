import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { labrecord } from '../labrecord.model';
import { labrecordsService } from '../labrecords.service';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-labrecords-labrecord-form-dialog',
    templateUrl: './labrecord-form.component.html',
    styleUrls: ['./labrecord-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuselabrecordslabrecordFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    labrecordForm: FormGroup;
    action: string;
    labrecord: labrecord;
    public categorys;
    public types;
    category

    constructor(
        public dialogRef: MdDialogRef<FuselabrecordslabrecordFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        private db: PouchService
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit labrecord';
            this.labrecord = data.labrecord;
        }
        else {
            this.dialogTitle = 'New labrecord';
            this.labrecord = {
                id: '',
                rev: '',
                customer: '',
            }
        }

        this.labrecordForm = this.createlabrecordForm();
    }

    ngOnInit() {
        this.categorys = ['HAEMATOLOGY AND BLOOD BANKING', 'WBC DIFFERENTIAL', 'BLOOD CROSS MATCHING', 'SEROLOGY TEST', 'RETROVIRAL SCREENING', 'VDRL', 'HBSAg','HCV',
                          'MALARIA PARASITE','ABO BLOOD GROUP','HAEMOGLOGIN GENOTYPE','SICKLING TEST','BLOOD MICROFILARIA','PREGNANCY TEST(Urine/Serum)','MICROBIOLOGY TEST',
                           'URINALYSIS','MICROSCOPY','ANTIBIOTIC SEMSITIVITY','SEMICAL FLUID ANALYSIS','CHEMICAL PATHOLOGY GENERAL'
                              ]
        this.types = ['input', 'text', 'checkbox', 'date']
    }

    getCategory(){
       this.db.category = this.category
       
    }

    createlabrecordForm() {
        return this.formBuilder.group({
            id: [this.labrecord.id],
            rev: [this.labrecord.rev],
            customer: [this.labrecord.customer]
        });
    }
}
