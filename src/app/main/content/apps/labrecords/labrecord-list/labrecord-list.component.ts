import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { labrecordsService } from '../labrecords.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { labtestsService } from '../../labtest/labtest.service';
import { FuselabrecordslabrecordFormDialogComponent } from '../labrecord-form/labrecord-form.component';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { labrecord } from '../labrecord.model';
import { Subject } from 'rxjs/Subject';
import { Staff } from '../../staffs/staff.model';
import { labtest } from '../../labtest/labtest.model';
import { Http, Headers, RequestOptions } from '@angular/http';
import { StaffsService } from '../../staffs/staffs.service';
import { DomSanitizer } from '@angular/platform-browser';
import {PouchService} from '../../../../../provider/pouch-service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { FuselabrecordreceiptslabrecordreceiptFormDialogComponent } from '../labrecord-receipt/labrecord-receipt.component';
import { remote, ipcRenderer } from 'electron';
/* declare const window: any;
const { remote } = window.require('electron');
const { BrowserWindow, dialog, shell } = remote;
const fs = window.require('fs'); */

@Component({
    selector: 'fuse-labrecords-labrecord-list',
    templateUrl: './labrecord-list.component.html',
    styleUrls: ['./labrecord-list.component.scss']
})
export class FuselabrecordslabrecordListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    nativeWindowOpen: true
    public labrecords: Array<labrecord> = [];
    public configurelabrecords: Array<labtest> = [];
    public staff: Staff;
    patients;
    patientName;
    patientAddress;
    patientPhone;
    patientEmail;
    window: Window;
    urllabrecord;
    image;
    checkboxstate;
    pid;
    labrecords2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    //dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'name', 'description', 'buttons'];
    selectedlabrecords: any[];
    online = true;
    checkboxes: {};
    labrecord: any = {
        id: Math.round((new Date()).getTime()).toString(),
        rev: '',
        customer: ''
    };
    general = [];
    refresh: Subject<any> = new Subject();
    haematology = [];
    wbc = [];
    bloodCross = [];
    serology = [];
    retroviral = [];
    vdrl = [];
    hbsag = [];
    malaria = [];
    abo = [];
    hcv = [];
    genotype = [];
    sickling = [];
    bloodm = [];
    pTest = [];
    microTest = [];
    urinalysis = [];
    microscopy = [];
    antibiotic = [];
    semical = [];
    pathology = [];
    labrecordss: labrecord[];
    typeofprint;
    image2;

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(public dialog: MdDialog, public _DomSanitizer: DomSanitizer, public db: PouchService, public http: Http, public activateRoute: ActivatedRoute, public router: Router) {

        let id = this.activateRoute.snapshot.params['id'];
        this.pid = id;
        this.db.getPStaffs().then(res => {
            this.click(event);
            this.checkboxstate = event.isTrusted
            
        })
        this.db.getPStaff2(id).then(res => {
            console.log(res);
            this.patients = res;
            
            this.patientName = res.name;
            this.patientAddress = res.address; 
            this.patientPhone = res.phone;
            this.patientEmail = res.email;
           
            if (res.image) {
                this.image = true;
                this.image2 = res.image;
                
            }
            else {
                this.image = false
            }
            console.log(this.patients.record);
            if (this.patients.labrecord == "") {
                this.labrecord = {
                    id: Math.round((new Date()).getTime()).toString(),
                    rev: '',
                    customer: ''
                };
            }
            else {
                console.log(this.patients.labrecord);
                this.db.getlabrecord(this.patients.labrecord).then(labrecord => {
                    this.labrecord = labrecord;
                    console.log(this.labrecord);
                    
                })
            }
        })



    }

    ngOnInit() {
        //this.dataSource = new FilesDataSource(this.db);
        this._loadlabrecords();
    }

    private _loadlabrecords(): void {
        this.db.getlabtests()
            .then(configurelabrecords => {
                console.log(configurelabrecords)
                this.configurelabrecords = configurelabrecords;
                //this.labrecords2 = new BehaviorSubject<any>(labrecords);
                this.haematology = this.configurelabrecords.filter(data => data.category == 'HAEMATOLOGY AND BLOOD BANKING');
                console.log(this.haematology);
                this.refresh.next(this.haematology);
                this.wbc = this.configurelabrecords.filter(data => data.category == 'WBC DIFFERENTIAL');
                this.bloodCross = this.configurelabrecords.filter(data => data.category == 'BLOOD CROSS MATCHING');
                this.serology = this.configurelabrecords.filter(data => data.category == 'SEROLOGY TEST');
                this.retroviral = this.configurelabrecords.filter(data => data.category == 'RETROVIRAL SCREENING');
                this.vdrl = this.configurelabrecords.filter(data => data.category == 'VDRL');
                this.hbsag = this.configurelabrecords.filter(data => data.category == 'HBSAg');

                this.malaria = this.configurelabrecords.filter(data => data.category == 'MALARIA PARASITE');
                this.abo = this.configurelabrecords.filter(data => data.category == 'ABO BLOOD GROUP');
                this.hcv = this.configurelabrecords.filter(data => data.category == 'HCV');
                this.genotype = this.configurelabrecords.filter(data => data.category == 'HAEMOGLOGIN GENOTYPE');
                this.sickling = this.configurelabrecords.filter(data => data.category == 'SICKLING TEST');
                this.bloodm = this.configurelabrecords.filter(data => data.category == 'BLOOD MICROFILARIA');
                this.pTest = this.configurelabrecords.filter(data => data.category == 'PREGNANCY TEST(Urine/Serum)');
                this.microTest = this.configurelabrecords.filter(data => data.category == 'MICROBIOLOGY TEST');
                this.urinalysis = this.configurelabrecords.filter(data => data.category == 'URINALYSIS');
                this.microscopy = this.configurelabrecords.filter(data => data.category == 'MICROSCOPY');
                this.antibiotic = this.configurelabrecords.filter(data => data.category == 'ANTIBIOTIC SEMSITIVITY');
                this.semical = this.configurelabrecords.filter(data => data.category == 'SEMICAL FLUID ANALYSIS');
                this.pathology = this.configurelabrecords.filter(data => data.category == 'CHEMICAL PATHOLOGY GENERAL');


                //console.log(this.labrecords2);
            });

    }


    /*    onlineCheck() {
           this.online = true;
           this.http.get('https://sarutech.com/couchdblogin/auth/validate-username/xs2da2432252sda').subscribe(data => {
               this.online = true;
               console.log(data)
               console.log(this.online);
           },
               (err) => {
                   this.online = false;
                   console.log(this.online);
               });
       }
   
       click() {
           this.onlineCheck();
       } */

    newlabrecord() {
        this.dialogRef = this.dialog.open(FuselabrecordslabrecordFormDialogComponent, {
            panelClass: 'labrecord-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
              
                if (this.db.category == "HAEMATOLOGY AND BLOOD BANKING") {
                    this.haematology.push(response.getRawValue());
                }
                else if (this.db.category == "WBC DIFFERENTIAL") {
                    this.wbc.push(response.getRawValue());
                }
                else if (this.db.category == "BLOOD CROSS MATCHING") {
                    this.bloodCross.push(response.getRawValue());
                }
                else if (this.db.category == "SEROLOGY TEST") {
                    this.serology.push(response.getRawValue());
                }
                else if (this.db.category == "RETROVIRAL SCREENING") {
                    this.retroviral.push(response.getRawValue());
                }
                else if (this.db.category == "VDRL") {
                    this.vdrl.push(response.getRawValue());
                }
                else if (this.db.category == "HBSAg") {
                    this.hbsag.push(response.getRawValue());
                }
                else if (this.db.category == "MALARIA PARASITE") {
                    this.malaria.push(response.getRawValue());
                }
                else if (this.db.category == "ABO BLOOD GROUP") {
                    this.abo.push(response.getRawValue());
                }
                else if (this.db.category == "HCV") {
                    this.hcv.push(response.getRawValue());
                }
                else if (this.db.category == "HAEMOGLOGIN GENOTYPE") {
                    this.genotype.push(response.getRawValue());
                }
                else if (this.db.category == "SICKLING TEST") {
                    this.sickling.push(response.getRawValue());
                }
                else if (this.db.category == "BLOOD MICROFILARIA") {
                    this.bloodm.push(response.getRawValue());
                }
                else if (this.db.category == "PREGNANCY TEST(Urine/Serum)") {
                    this.pTest.push(response.getRawValue());
                }
                else if (this.db.category == "MICROBIOLOGY TEST") {
                    this.microTest.push(response.getRawValue());
                }
                else if (this.db.category == "URINALYSIS") {
                    this.urinalysis.push(response.getRawValue());
                }
                else if (this.db.category == "MICROSCOPY") {
                    this.microscopy.push(response.getRawValue());
                }
                else if (this.db.category == "ANTIBIOTIC SEMSITIVITY") {
                    this.antibiotic.push(response.getRawValue());
                }
                else if (this.db.category == "SEMICAL FLUID ANALYSIS") {
                    this.semical.push(response.getRawValue());
                }
                else if (this.db.category == "CHEMICAL PATHOLOGY GENERAL") {
                    this.pathology.push(response.getRawValue());
                }
                
                //this.db.savelabrecord(response.getRawValue());
                this.refresh.next(true);
                //this.dataSource = new FilesDataSource(this.db);
                

            });

    }

    editlabrecord(labrecord) {
        
        this.dialogRef = this.dialog.open(FuselabrecordslabrecordFormDialogComponent, {
            panelClass: 'labrecord-form-dialog',
            data: {
                labrecord: labrecord,
                action: 'edit'

            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':

                        //this.db.updatelabrecord(formData.getRawValue());
                        //this.dataSource = new FilesDataSource(this.db);
                        
                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deletelabrecord(labrecord);

                        break;
                }
            });
    }

    /**
     * Delete labrecord
     */
    deletelabrecord(labrecord) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deletelabrecord(labrecord);
                //this.dataSource = new FilesDataSource(this.db);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(labrecordId) {
        this.db.toggleSelectedlabrecord(labrecordId);
    }
    navConsultancy() {
        
        this.router.navigate(['apps/consultancy', this.patients.id]);
    }
    click(event) {
        
        this.checkboxstate = event;
    }

    submit() {
      
        this.patients.labrecord = this.labrecord.id;
        //this.customer.labrecord = this.labrecord.id;
        this.db.savelabrecord(this.labrecord, this.patients).then();
    }

    print() {
        /*   this.db.content = document.getElementById("printable").innerHTML;
          console.log(this.db.content) */
        /* window.document.write(this.db.content);
        window.document.close();
        window.print();
 */

        this.router.navigate(['pages/print', this.pid]);
    }

    printlabrecord() {

        /*  const mainWindow = new BrowserWindow({
                width: 800,
                height: 600,
                webPreferences: {
                    nativeWindowOpen: true
                }
            })
    
            mainWindow.loadURL('file://' + __dirname + 'dist/src/app/main/content/apps/labrecords/labrecord-receipt/labrecord-receipt.component.ts');
            console.log(__dirname)
            mainWindow.show();
            setTimeout(function () {
                mainWindow.webContents.print();
            }, 1000); */


        /*  mainWindow.webContents.on('new-window', (event, url, frameName, disposition, options, additionalFeatures) => {
             if (frameName === 'Hospital Manager') {
                 // open window as modal
                 event.preventDefault()
                 Object.assign(options, {
                     modal: true,
                     parent: mainWindow,
                     width: 100,
                     height: 100
                 })
                 event.newGuest = new BrowserWindow(options)
             }
         })
  */
        // renderer process (mainWindow)
        /*   let modal = window.open('', 'Hospital Manager')
          modal.document.write('<h1>Hello</h1>') */
        /* 
                this.dialogRef = this.dialog.open(FuselabrecordreceiptslabrecordreceiptFormDialogComponent, {
                    panelClass: 'labrecord-receipt-dialog',
                    data: {
                        pid: this.pid
                    }
                })
                this.dialogRef.afterClosed() */

        //this.db.hide = "hidden";
        var win: any;
        win = window;
        var printContent = document.getElementById('printArea').innerHTML;
        var printWindow = win.open('', '_blank', 'top=0,left=0,height=auto,width=auto');
        printWindow.document.open();
        printWindow.document.write(`<html><head><title>Patient labrecord `, `</title><style>
            .avatar {
                                 font-size: 12px;
                                 padding-bottom: 2px;
                                 border-radius: 50%;
                                 height: 50px;
                                 width: 50px
                              }
                              .username {
                                     padding-left: 6px;
                                 }
                                 .print {
                            display: none;
                         }
                         .print2 {
                            display: none;
                         }
                          .hide {
                                 display: none;
                                 }
                             .invoice-name {
                                     padding-left: 6px;
                                 }
                                 .margin {
                                 font-size: 12px;
                                 padding-bottom: 6px;
                                 margin-bottom: 12px;
                                 }
 
                                  .margin2 {
                                 font-size: 12px;
                                 padding-bottom: 6px;
                                 margin-bottom: 30px;
                                 margin-left: 30px;
                                 }
 
                              .date {
                                     padding-left: 6px;
                                 }
                                 .info {
                                 color: rgba(0, 0, 0, 0.54);
                                 line-height: 22px;
                                 font-size: 12px;
                                 padding-bottom: 12px;
                             }
                             .issuer {
                             margin-right: -58px;
                             padding-right: 66px;
                             }
                         #co-size {
                         visibility: hidden   
                                   }
                     .invoice-table {
                             margin-top: 64px;
                             font-size: 15px;
                             }
                         .text-right{
                             padding-left: 150px;
                         }
                         .invoice-table-footer {
                             margin: 32px 0 72px 0;
                             }
                         td.line{
                            border-bottom: 1px solid rgba(0, 0, 0, 0.12);
                         }
                          td.mytotal {
                                         padding: 24px 8px;
                                         font-size: 35px;
                                         font-weight: 300;
                                         color: rgba(0, 0, 0, 1);
                                     }
                                     .alignright{
                                         padding-left: 100px;
                                     }
                                     #alignright{
                                         padding-left: 100px;
                                     }
           </style></head><body></body></html>`);
        printWindow.document.write(printContent);
        printWindow.document.close();
        printWindow.print();
        // window.print();
    }

}

/* export class FilesDataSource extends DataSource<any>
{
    labrecords2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: labrecordsService) {
        super();
    }

     //Connect function called by the table to retrieve one stream containing the data to render.
    connect(): Observable<any[]> {
        var result = Observable.fromPromise(this.db.getlabrecords());
        result.subscribe(x => console.log(x), e => console.error(e));
        return result;
    }

    disconnect() {
    }
}
 */