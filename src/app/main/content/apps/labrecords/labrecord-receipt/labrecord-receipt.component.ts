import { Component, Inject, OnInit, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { labrecordsService } from '../labrecords.service';
import { StaffsService } from '../../staffs/staffs.service';
import { SettingsService } from '../../settings/settings.service';
import { CurrencysService } from '../../currencys/currencys.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { labtestsService } from '../../labtest/labtest.service';
import { Subject } from 'rxjs/Subject';
import { labrecord } from '../labrecord.model';
import { labtest } from '../../labtest/labtest.model';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-labrecord-receipts-labrecord-receipt-form-dialog',
    templateUrl: './labrecord-receipt.component.html',
    styleUrls: ['./labrecord-receipt.component.scss'],
    encapsulation: ViewEncapsulation.None,
})

export class FuselabrecordreceiptslabrecordreceiptFormDialogComponent implements OnInit {

    event: CalendarEvent;
    dialogTitle: string;
    labrecordreceiptForm: FormGroup;
    action: string;
    public settings;
    public result;
    public final;
    public hospitalName;
    public result2;
    public final2;
    public hospitalAddress;
    public result3;
    public final3;
    public hospitalPhone;
    public result4;
    public final4;
    public hospitalEmail;
    public Pid;
    public labrecordDate: any;
    public labrecordMonth: any;
    public labrecordNumber: any;
    public labrecordDueDate: any;
    public staffName: any;
    public basicSalary: any;
    public userType: any;
    public allowancess;
    public deductionss;
    public staffId;
    public staffAddress;
    public staffNumber;
    public configurelabrecords: Array<labtest> = [];
    public staffEmail;
    public subTotal;
    public vat;
    patients;
    patientName;
    patientAddress;
    patientPhone;
    patientEmail;
    public discount;
    public total;
    checkboxstate;
    public currencys;
    public result5;
    public final5;
    public displayCurrencys;
    urllabrecord;
    image;
    general = [];
    refresh: Subject<any> = new Subject();
    medicalhistory = [];
    socialhistory = [];
    medicalconditions = [];
    currentmedications = [];
    familyhistory = [];
    notes = [];
    labrecordss: labrecord[];
    labrecord: any = {
        id: Math.round((new Date()).getTime()).toString(),
        rev: '',
        customer: ''
    };


    constructor(
        public dialogRef: MdDialogRef<FuselabrecordreceiptslabrecordreceiptFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        public db: PouchService,
        public _DomSanitizer: DomSanitizer,
        public router: Router,
        public activateRoute: ActivatedRoute
    ) {
        this.Pid = data.pid
        this.dialogTitle = 'Laboratory Receipt';
        /* 
                if (this.action === 'edit') {
                    this.dialogTitle = 'Edit labrecord-receipt';
                    this.labrecord-receipt = data.labrecord-receipt;
                }
                else {
                    this.dialogTitle = 'New labrecord-receipt';
                    this.labrecord-receipt = {
                        id: '',
                        rev: '',
                        name: '',
                        description: '',
                        doctors: []
                    } */


        //this.labrecord-receiptForm = this.createlabrecord-receiptForm();
    }


    ngOnInit() {
        this.db.getPStaffs().then(res => {
            this.click(event);
            this.checkboxstate = event.isTrusted
            
        })
        this.db.getPStaff2(this.Pid).then(res => {
            this.patients = res;
            
            this.patientName = res.name;
            this.patientAddress = res.address;
            this.patientPhone = res.phone;
            this.patientEmail = res.email;
            if (res._attachments) {
                this.image = true;
                this.urllabrecord = URL.createObjectURL(res._attachments);
                
            }
            else {
                this.image = false
            }
            
            if (this.patients.labrecord == "") {
                this.labrecord = {
                    id: Math.round((new Date()).getTime()).toString(),
                    rev: '',
                    customer: ''
                };
            }
            else {
                this.db.getlabrecord(this.patients.labrecord).then(labrecord => {
                    this.labrecord = labrecord;
                    
                })
            }
        })
        this._loadlabrecords();

    }

    private _loadlabrecords(): void {
        this.db.getlabtests()
            .then(configurelabrecords => {
                this.configurelabrecords = configurelabrecords;
                //this.labrecords2 = new BehaviorSubject<any>(labrecords);
                this.general = this.configurelabrecords.filter(data => data.category == 'General');
                this.refresh.next(this.general);
                this.medicalhistory = this.configurelabrecords.filter(data => data.category == 'Medical History');
                this.socialhistory = this.configurelabrecords.filter(data => data.category == 'Social History');
                this.medicalconditions = this.configurelabrecords.filter(data => data.category == 'Medical Conditions');
                this.currentmedications = this.configurelabrecords.filter(data => data.category == 'Current Medications');
                this.familyhistory = this.configurelabrecords.filter(data => data.category == 'Family History');
                this.notes = this.configurelabrecords.filter(data => data.category == 'Notes');

                //console.log(this.labrecords2);
            });

    }

    click(event) {
        
        this.checkboxstate = event;
    }

    print() {
      /*   this.db.content = document.getElementById("printable").innerHTML;
        console.log(this.db.content) */
        /* window.document.write(this.db.content);
        window.document.close();
        window.print();
 */
     this.router.navigate(['pages/print']);
    }

    createlabrecordreceiptForm() {
        /*  return this.formBuilder.group({
             id: [this.labrecord-receipt.id],
             rev: [this.labrecord-receipt.rev],
             name: [this.labrecord-receipt.name],
             description: [this.labrecord-receipt.description],
 
         });*/
    }
}
