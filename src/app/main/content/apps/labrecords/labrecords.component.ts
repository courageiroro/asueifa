import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { labrecordsService } from './labrecords.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector: 'fuse-labrecords',
    templateUrl: './labrecords.component.html',
    styleUrls: ['./labrecords.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [Animations.slideInTop]
})
export class FuselabrecordsComponent implements OnInit {
    hasSelectedlabrecords: boolean;
    searchInput: FormControl;

    constructor(private labrecordsService: PouchService) {
        this.searchInput = new FormControl('');
       
    }

    ngOnInit() {

        this.labrecordsService.onSelectedlabrecordsChanged
            .subscribe(selectedlabrecords => {
                this.hasSelectedlabrecords = selectedlabrecords.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.labrecordsService.onSearchTextChanged.next(searchText);
            });
    }

}
