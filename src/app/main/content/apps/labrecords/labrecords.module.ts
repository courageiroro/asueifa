import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuselabrecordsComponent } from './labrecords.component';
import { labrecordsService } from './labrecords.service';
//import { UserService } from '../../../../user.service';
import { labtestsService } from '../labtest/labtest.service';
import { FuselabrecordslabrecordListComponent } from './labrecord-list/labrecord-list.component';
import { FuselabrecordsSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuselabrecordslabrecordFormDialogComponent } from './labrecord-form/labrecord-form.component';
import { StaffsService } from '../staffs/staffs.service';
import {FuselabrecordreceiptslabrecordreceiptFormDialogComponent} from './labrecord-receipt/labrecord-receipt.component';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/labrecord/:id',
        component: FuselabrecordsComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            labrecords: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuselabrecordsComponent,
        FuselabrecordslabrecordListComponent,
        FuselabrecordsSelectedBarComponent,
        FuselabrecordslabrecordFormDialogComponent,
        FuselabrecordreceiptslabrecordreceiptFormDialogComponent
    ],
    providers      : [
        labrecordsService,
        StaffsService,
        PouchService,
        AuthGuard,
        labtestsService
       //UserService
    ],
    entryComponents: [FuselabrecordslabrecordFormDialogComponent,FuselabrecordreceiptslabrecordreceiptFormDialogComponent]
})
export class FuselabrecordsModule
{
}
