/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import { Staff } from '../staffs/staff.model';
import { StaffsService } from '../staffs/staffs.service';
declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { labrecord } from './labrecord.model';
import { Schema } from '../schema';

@Injectable()
export class labrecordsService {
    public labrecord = "";
    public sCredentials;
    public category;
    public file;
    public retrieve;
    public content: any;
    onlabrecordsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedlabrecordsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    labrecords: labrecord[];
    staffs: Staff[];
    user: any;
    selectedlabrecords: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    public db2: StaffsService
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http, ) {
        this.initDB(this.sCredentials);
    }

    /**
     * The labrecords App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getlabrecords()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getlabrecords();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getlabrecords();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('asueifai', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/asueifai';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/asueifai';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }

     /**
    * Update a staff
    * @param {staff} staff
    *
    * @return Promise<staff>
    */
    updateStaff(staff: Staff) {

            return this.db.rel.save('staff', staff)
                .then((data: any) => {
                    if (data && data.staffs && data.staffs
                    [0]) {
                        console.log(data.staffs[0])
                        return data.staffs[0]
                    }
                    return null;
                }).catch((err: any) => {
                    console.error(err);
                });

        
      
    }

    /***********
     * labrecord
     **********/
    /**
     * Save a labrecord
     * @param {labrecord} labrecord
     *
     * @return Promise<labrecord>
     */
    savelabrecord(labrecord: labrecord, staff: Staff): Promise<labrecord> {
        //labrecord.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('labrecord', labrecord)
            .then((data: any) => {
                console.log(staff);
                this.updateStaff(staff);
                /*   if (this.file) {
                      this.file;
                      
                      this.retrieve = (<HTMLInputElement>this.file).files[0];
  
                      if (this.retrieve = (<HTMLInputElement>this.file).files[0]) {
                          staff._attachments = this.retrieve
                      }
  
                      else {
                          staff._attachments;
                          //this.getStaff = staff._attachments;
                      }
  
                      return this.db.rel.save('staff', staff)
                          .then((data: any) => {
                              if (data && data.staffs && data.staffs
                              [0]) {
                                   this.db.rel.putAttachment('staff', { id: staff.id, rev: staff.rev }, 'file', staff._attachments, 'image/png').then(res => {
                                      var reader: FileReader = new FileReader();
  
                                      reader.onloadend = function (e) {
                                          var base64data = reader.result;
                                          staff.image = base64data
                                          //console.log(staff.image);
                                      }
                                      reader.readAsDataURL(this.retrieve);
                                      staff._attachments = res;
                                      staff.image = res
  
                                  }) 
                                  
                                  return data.staffs[0]
                              }
  
                              return null;
                          }).catch((err: any) => {
                              console.error(err);
                          });
  
                  }
                  else {
                      
  
                      return this.db.rel.save('staff', staff)
                          .then((data: any) => {
                              if (data && data.staffs && data.staffs
                              [0]) {
                                   this.db.rel.putAttachment('staff', { id: staff.id, rev: staff.rev }, 'file', staff._attachments, 'image/png').then(res => {
                                      staff._attachments = res
                                  }) 
                                  
                                  return data.staffs[0]
                              }
  
                              return null;
                          }).catch((err: any) => {
                              console.error(err);
                          });
                  } */
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a labrecord
    * @param {labrecord} labrecord
    *
    * @return Promise<labrecord>
    */
    updatelabrecord(labrecord: labrecord) {
        return this.db.rel.save('labrecord', labrecord)
            .then((data: any) => {
                if (data && data.labrecords && data.labrecords
                [0]) {
                    console.log('Update')

                    return data.labrecords[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a labrecord
     * @param {labrecord} labrecord
     *
     * @return Promise<boolean>
     */
    removelabrecord(labrecord: labrecord): Promise<boolean> {
        return this.db.rel.del('labrecord', labrecord)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the labrecords
     *
     * @return Promise<Array<labrecord>>
     */
    getlabrecords(): Promise<Array<labrecord>> {
        return this.db.rel.find('labrecord')
            .then((data: any) => {
                this.labrecords = data.labrecords;
                if (this.searchText && this.searchText !== '') {
                    this.labrecords = FuseUtils.filterArrayByString(this.labrecords, this.searchText);
                }
                //this.onlabrecordsChanged.next(this.labrecords);
                return Promise.resolve(this.labrecords);
                //return data.labrecords ? data.labrecords : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getDoctorlabrecords(): Promise<Array<labrecord>> {
        return this.db.rel.find('labrecord')
            .then((data: any) => {
                return data.labrecords ? data.labrecords : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a labrecord
     * @param {labrecord} labrecord
     *
     * @return Promise<labrecord>
     */
    getlabrecord(id): Promise<labrecord> {
        return this.db.rel.find('labrecord', id)
            .then((data: any) => {
                console.log("Get")

                return data && data.labrecords ? data.labrecords[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected labrecord by id
     * @param id
     */
    toggleSelectedlabrecord(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedlabrecords.length > 0) {
            const index = this.selectedlabrecords.indexOf(id);

            if (index !== -1) {
                this.selectedlabrecords.splice(index, 1);

                // Trigger the next event
                this.onSelectedlabrecordsChanged.next(this.selectedlabrecords);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedlabrecords.push(id);

        // Trigger the next event
        this.onSelectedlabrecordsChanged.next(this.selectedlabrecords);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedlabrecords.length > 0) {
            this.deselectlabrecords();
        }
        else {
            this.selectlabrecords();
        }
    }

    selectlabrecords(filterParameter?, filterValue?) {
        this.selectedlabrecords = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedlabrecords = [];
            this.labrecords.map(labrecord => {
                this.selectedlabrecords.push(labrecord.id);
            });
        }
        else {
            /* this.selectedlabrecords.push(...
                 this.labrecords.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedlabrecordsChanged.next(this.selectedlabrecords);
    }





    deselectlabrecords() {
        this.selectedlabrecords = [];

        // Trigger the next event
        this.onSelectedlabrecordsChanged.next(this.selectedlabrecords);
    }

    deletelabrecord(labrecord) {
        this.db.rel.del('labrecord', labrecord)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const labrecordIndex = this.labrecords.indexOf(labrecord);
                this.labrecords.splice(labrecordIndex, 1);
                this.onlabrecordsChanged.next(this.labrecords);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedlabrecords() {
        for (const labrecordId of this.selectedlabrecords) {
            const labrecord = this.labrecords.find(_labrecord => {
                return _labrecord.id === labrecordId;
            });

            this.db.rel.del('labrecord', labrecord)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const labrecordIndex = this.labrecords.indexOf(labrecord);
                    this.labrecords.splice(labrecordIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onlabrecordsChanged.next(this.labrecords);
        this.deselectlabrecords();
    }
}
