import { Component, OnInit } from '@angular/core';
import { labrecordsService } from '../labrecords.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuselabrecordsSelectedBarComponent implements OnInit
{
    selectedlabrecords: string[];
    hasSelectedlabrecords: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private labrecordsService: PouchService,
        public dialog: MdDialog
    )
    {
        this.labrecordsService.onSelectedlabrecordsChanged
            .subscribe(selectedlabrecords => {
                this.selectedlabrecords = selectedlabrecords;
                setTimeout(() => {
                    this.hasSelectedlabrecords = selectedlabrecords.length > 0;
                    this.isIndeterminate = (selectedlabrecords.length !== this.labrecordsService.labrecords.length && selectedlabrecords.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.labrecordsService.selectlabrecords();
    }

    deselectAll()
    {
        this.labrecordsService.deselectlabrecords();
    }

    deleteSelectedlabrecords()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected labrecords?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.labrecordsService.deleteSelectedlabrecords();
            }
            this.confirmDialogRef = null;
        });
    }

}
