import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { labtest } from '../labtest.model';
import { labtestsService } from '../labtest.service';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-labtests-labtest-form-dialog',
    templateUrl: './labtest-form.component.html',
    styleUrls: ['./labtest-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuselabtestslabtestFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    labtestForm: FormGroup;
    action: string;
    labtest: labtest;
    public categorys;
    public types;
    category

    constructor(
        public dialogRef: MdDialogRef<FuselabtestslabtestFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        private db: PouchService
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit labtest';
            this.labtest = data.labtest;
        }
        else {
            this.dialogTitle = 'New labtest';
            this.labtest = {
                id: '',
                rev: '',
                name: '',
                category: '',
                type: '',
                normalvalue: ''
            }
        }

        this.labtestForm = this.createlabtestForm();
    }

    ngOnInit() {
        this.categorys = ['HAEMATOLOGY AND BLOOD BANKING',
            'WBC DIFFERENTIAL',
            'BLOOD CROSS MATCHING',
            'SEROLOGY TEST',
            'RETROVIRAL SCREENING',
            'VDRL',
            'HBSAg',
            'HCV',
            'MALARIA PARASITE',
            'ABO BLOOD GROUP',
            'HAEMOGLOGIN GENOTYPE',
            'SICKLING TEST',
            'BLOOD MICROFILARIA',
            'PREGNANCY TEST(Urine/Serum)',
            'MICROBIOLOGY TEST',
            'URINALYSIS',
            'MICROSCOPY',
            'ANTIBIOTIC SEMSITIVITY',
            'SEMICAL FLUID ANALYSIS',
            'CHEMICAL PATHOLOGY GENERAL'
            ]
        this.types = ['input', 'text', 'checkbox', 'date']
    }

    getCategory(){
       this.db.category = this.labtest.category;
       
    }

    createlabtestForm() {
        return this.formBuilder.group({
            id: [this.labtest.id],
            rev: [this.labtest.rev],
            name: [this.labtest.name],
            type: [this.labtest.type],
            category: [this.labtest.category],
            normalvalue: [this.labtest.normalvalue],
        });
    }
}
