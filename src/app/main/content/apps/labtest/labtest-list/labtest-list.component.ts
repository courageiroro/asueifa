import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { labtestsService } from '../labtest.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { FuselabtestslabtestFormDialogComponent } from '../labtest-form/labtest-form.component';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { labtest } from '../labtest.model';
import { Subject } from 'rxjs/Subject';
import { Http, Headers, RequestOptions } from '@angular/http';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-labtest-labtest-list',
    templateUrl: './labtest-list.component.html',
    styleUrls: ['./labtest-list.component.scss']
})
export class FuselabtestlabtestListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public labtest: Array<labtest> = [];
    labtest2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    //dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'name', 'description', 'buttons'];
    selectedlabtest: any[];
    online = true;
    checkboxes: {};
    haematology = [];
    refresh: Subject<any> = new Subject();
    wbc = [];
    bloodCross = [];
    serology = [];
    retroviral = [];
    vdrl = [];
    hbsag = [];
    malaria = [];
    abo = [];
    hcv = [];
    genotype = [];
    sickling = [];
    bloodm = [];
    pTest = [];
    microTest = [];
    urinalysis = [];
    microscopy = [];
    antibiotic = [];
    semical = [];
    pathology = [];

    recordss: labtest[];

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(public dialog: MdDialog, public db: PouchService, public http: Http) { }

    ngOnInit() {
        //this.dataSource = new FilesDataSource(this.db);
        this._loadlabtest();
    }

    private _loadlabtest(): void {
        this.db.getlabtests()
            .then(labtest => {
                this.labtest = labtest;
                //this.labtest2 = new BehaviorSubject<any>(labtest);

                this.haematology = this.labtest.filter(data => data.category == 'HAEMATOLOGY AND BLOOD BANKING');
                this.refresh.next(this.haematology);
                this.wbc = this.labtest.filter(data => data.category == 'WBC DIFFERENTIAL');
                this.bloodCross = this.labtest.filter(data => data.category == 'BLOOD CROSS MATCHING');
                this.serology = this.labtest.filter(data => data.category == 'SEROLOGY TEST');
                this.retroviral = this.labtest.filter(data => data.category == 'RETROVIRAL SCREENING');
                this.vdrl = this.labtest.filter(data => data.category == 'VDRL');
                this.hbsag = this.labtest.filter(data => data.category == 'HBSAg');

                this.malaria = this.labtest.filter(data => data.category == 'MALARIA PARASITE');
                this.abo = this.labtest.filter(data => data.category == 'ABO BLOOD GROUP');
                this.hcv = this.labtest.filter(data => data.category == 'HCV');
                this.genotype = this.labtest.filter(data => data.category == 'HAEMOGLOGIN GENOTYPE');
                this.sickling = this.labtest.filter(data => data.category == 'SICKLING TEST');
                this.bloodm = this.labtest.filter(data => data.category == 'BLOOD MICROFILARIA');
                this.pTest = this.labtest.filter(data => data.category == 'PREGNANCY TEST(Urine/Serum)');
                this.microTest = this.labtest.filter(data => data.category == 'MICROBIOLOGY TEST');
                this.urinalysis = this.labtest.filter(data => data.category == 'URINALYSIS');
                this.microscopy = this.labtest.filter(data => data.category == 'MICROSCOPY');
                this.antibiotic = this.labtest.filter(data => data.category == 'ANTIBIOTIC SEMSITIVITY');
                this.semical = this.labtest.filter(data => data.category == 'SEMICAL FLUID ANALYSIS');
                this.pathology = this.labtest.filter(data => data.category == 'CHEMICAL PATHOLOGY GENERAL');

                //console.log(this.labtest2);
            });

    }


    onlineCheck() {
        this.online = true;
        this.http.get('https://sarutech.com/couchdblogin/auth/validate-username/xs2da2432252sda').subscribe(data => {
            this.online = true;

        },
            (err) => {
                this.online = false;

            });
    }

    click() {
        this.onlineCheck();
    }

    newlabtest() {
        this.dialogRef = this.dialog.open(FuselabtestslabtestFormDialogComponent, {
            panelClass: 'labtest-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }

                if (this.db.category == "HAEMATOLOGY AND BLOOD BANKING") {
                    this.haematology.push(response.getRawValue());
                }
                else if (this.db.category == "WBC DIFFERENTIAL") {
                    this.wbc.push(response.getRawValue());
                }
                else if (this.db.category == "BLOOD CROSS MATCHING") {
                    this.bloodCross.push(response.getRawValue());
                }
                else if (this.db.category == "SEROLOGY TEST") {
                    this.serology.push(response.getRawValue());
                }
                else if (this.db.category == "RETROVIRAL SCREENING") {
                    this.retroviral.push(response.getRawValue());
                }
                else if (this.db.category == "VDRL") {
                    this.vdrl.push(response.getRawValue());
                }
                else if (this.db.category == "HBSAg") {
                    this.hbsag.push(response.getRawValue());
                }
                else if (this.db.category == "MALARIA PARASITE") {
                    this.malaria.push(response.getRawValue());
                }
                else if (this.db.category == "ABO BLOOD GROUP") {
                    this.abo.push(response.getRawValue());
                }
                else if (this.db.category == "HCV") {
                    this.hcv.push(response.getRawValue());
                }
                else if (this.db.category == "HAEMOGLOGIN GENOTYPE") {
                    this.genotype.push(response.getRawValue());
                }
                else if (this.db.category == "SICKLING TEST") {
                    this.sickling.push(response.getRawValue());
                }
                else if (this.db.category == "BLOOD MICROFILARIA") {
                    this.bloodm.push(response.getRawValue());
                }/* else if (this.db.category == "MALARIA PARASITE") {
                    this.malaria.push(response.getRawValue());
                }
                else if (this.db.category == "ABO BLOOD GROUP") {
                    this.abo.push(response.getRawValue());
                } */
                /* else if (this.db.category == "HCV") {
                    this.hcv.push(response.getRawValue());
                } */
                /*  else if (this.db.category == "HAEMOGLOGIN GENOTYPE") {
                     this.genotype.push(response.getRawValue());
                 }
                 else if (this.db.category == "SICKLING TEST") {
                     this.sickling.push(response.getRawValue());
                 }
                 else if (this.db.category == "BLOOD MICROFILARIA") {
                     this.bloodm.push(response.getRawValue());
                 } */
                else if (this.db.category == "PREGNANCY TEST(Urine/Serum)") {
                    this.pTest.push(response.getRawValue());
                }
                else if (this.db.category == "MICROBIOLOGY TEST") {
                    this.microTest.push(response.getRawValue());
                }
                else if (this.db.category == "URINALYSIS") {
                    this.urinalysis.push(response.getRawValue());
                }
                else if (this.db.category == "MICROSCOPY") {
                    this.microscopy.push(response.getRawValue());
                }
                else if (this.db.category == "ANTIBIOTIC SEMSITIVITY") {
                    this.antibiotic.push(response.getRawValue());
                }
                else if (this.db.category == "SEMICAL FLUID ANALYSIS") {
                    this.semical.push(response.getRawValue());
                }
                else if (this.db.category == "CHEMICAL PATHOLOGY GENERAL") {
                    this.pathology.push(response.getRawValue());
                }
                this.db.savelabtest(response.getRawValue());
                this.refresh.next(true);
                //this.dataSource = new FilesDataSource(this.db);

            });

    }

    editlabtest(labtest) {

        this.dialogRef = this.dialog.open(FuselabtestslabtestFormDialogComponent, {
            panelClass: 'labtest-form-dialog',
            data: {
                labtest: labtest,
                action: 'edit'

            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                //this.general.push(formData.getRawValue());
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':
                        if (labtest.category == "HAEMATOLOGY AND BLOOD BANKING") {

                            labtest.id = '',
                                labtest.rev = '',
                                labtest.name = formData.getRawValue().name,
                                labtest.type = formData.getRawValue().type,
                                labtest.category = formData.getRawValue().category,
                                labtest.normalvalue = formData.getRawValue().normalvalue

                        }
                        else if (labtest.category == "WBC DIFFERENTIAL") {
                            labtest.id = '',
                                labtest.rev = '',
                                labtest.name = formData.getRawValue().name,
                                labtest.type = formData.getRawValue().type,
                                labtest.category = formData.getRawValue().category,
                                labtest.normalvalue = formData.getRawValue().normalvalue

                        }
                        else if (labtest.category == "BLOOD CROSS MATCHING") {
                            labtest.id = '',
                                labtest.rev = '',
                                labtest.name = formData.getRawValue().name,
                                labtest.type = formData.getRawValue().type,
                                labtest.category = formData.getRawValue().category,
                                labtest.normalvalue = formData.getRawValue().normalvalue
                        }
                        else if (labtest.category == "SEROLOGY TEST") {
                            labtest.id = '',
                                labtest.rev = '',
                                labtest.name = formData.getRawValue().name,
                                labtest.type = formData.getRawValue().type,
                                labtest.category = formData.getRawValue().category,
                                labtest.normalvalue = formData.getRawValue().normalvalue
                        }
                        else if (labtest.category == "RETROVIRAL SCREENING") {
                            labtest.id = '',
                                labtest.rev = '',
                                labtest.name = formData.getRawValue().name,
                                labtest.type = formData.getRawValue().type,
                                labtest.category = formData.getRawValue().category,
                                labtest.normalvalue = formData.getRawValue().normalvalue
                        }
                        else if (labtest.category == "VDRL") {
                            labtest.id = '',
                                labtest.rev = '',
                                labtest.name = formData.getRawValue().name,
                                labtest.type = formData.getRawValue().type,
                                labtest.category = formData.getRawValue().category,
                                labtest.normalvalue = formData.getRawValue().normalvalue
                        }
                        else if (labtest.category == "HBSAg") {
                            labtest.id = '',
                                labtest.rev = '',
                                labtest.name = formData.getRawValue().name,
                                labtest.type = formData.getRawValue().type,
                                labtest.category = formData.getRawValue().category,
                                labtest.normalvalue = formData.getRawValue().normalvalue
                        }

                        else if (labtest.category == "MALARIA PARASITE") {
                            labtest.id = '',
                                labtest.rev = '',
                                labtest.name = formData.getRawValue().name,
                                labtest.type = formData.getRawValue().type,
                                labtest.category = formData.getRawValue().category,
                                labtest.normalvalue = formData.getRawValue().normalvalue

                        }
                        else if (labtest.category == "ABO BLOOD GROUP") {
                            labtest.id = '',
                                labtest.rev = '',
                                labtest.name = formData.getRawValue().name,
                                labtest.type = formData.getRawValue().type,
                                labtest.category = formData.getRawValue().category,
                                labtest.normalvalue = formData.getRawValue().normalvalue
                        }
                        else if (labtest.category == "HCV") {
                            labtest.id = '',
                                labtest.rev = '',
                                labtest.name = formData.getRawValue().name,
                                labtest.type = formData.getRawValue().type,
                                labtest.category = formData.getRawValue().category,
                                labtest.normalvalue = formData.getRawValue().normalvalue
                        }
                        else if (labtest.category == "HAEMOGLOGIN GENOTYPE") {
                            labtest.id = '',
                                labtest.rev = '',
                                labtest.name = formData.getRawValue().name,
                                labtest.type = formData.getRawValue().type,
                                labtest.category = formData.getRawValue().category,
                                labtest.normalvalue = formData.getRawValue().normalvalue
                        }
                        else if (labtest.category == "SICKLING TEST") {
                            labtest.id = '',
                                labtest.rev = '',
                                labtest.name = formData.getRawValue().name,
                                labtest.type = formData.getRawValue().type,
                                labtest.category = formData.getRawValue().category,
                                labtest.normalvalue = formData.getRawValue().normalvalue
                        }
                        else if (labtest.category == "BLOOD MICROFILARIA") {
                            labtest.id = '',
                                labtest.rev = '',
                                labtest.name = formData.getRawValue().name,
                                labtest.type = formData.getRawValue().type,
                                labtest.category = formData.getRawValue().category,
                                labtest.normalvalue = formData.getRawValue().normalvalue
                        }
                        
                        else if (labtest.category == "PREGNANCY TEST(Urine/Serum)") {
                            labtest.id = '',
                                labtest.rev = '',
                                labtest.name = formData.getRawValue().name,
                                labtest.type = formData.getRawValue().type,
                                labtest.category = formData.getRawValue().category,
                                labtest.normalvalue = formData.getRawValue().normalvalue

                        }
                        else if (labtest.category == "MICROBIOLOGY TEST") {
                            labtest.id = '',
                                labtest.rev = '',
                                labtest.name = formData.getRawValue().name,
                                labtest.type = formData.getRawValue().type,
                                labtest.category = formData.getRawValue().category,
                                labtest.normalvalue = formData.getRawValue().normalvalue
                        }
                        else if (labtest.category == "URINALYSIS") {
                            labtest.id = '',
                                labtest.rev = '',
                                labtest.name = formData.getRawValue().name,
                                labtest.type = formData.getRawValue().type,
                                labtest.category = formData.getRawValue().category,
                                labtest.normalvalue = formData.getRawValue().normalvalue
                        }
                        else if (labtest.category == "MICROSCOPY") {
                            labtest.id = '',
                                labtest.rev = '',
                                labtest.name = formData.getRawValue().name,
                                labtest.type = formData.getRawValue().type,
                                labtest.category = formData.getRawValue().category,
                                labtest.normalvalue = formData.getRawValue().normalvalue
                        }
                        else if (labtest.category == "ANTIBIOTIC SEMSITIVITY") {
                            labtest.id = '',
                                labtest.rev = '',
                                labtest.name = formData.getRawValue().name,
                                labtest.type = formData.getRawValue().type,
                                labtest.category = formData.getRawValue().category,
                                labtest.normalvalue = formData.getRawValue().normalvalue
                        }
                        else if (labtest.category == "SEMICAL FLUID ANALYSIS") {
                            labtest.id = '',
                                labtest.rev = '',
                                labtest.name = formData.getRawValue().name,
                                labtest.type = formData.getRawValue().type,
                                labtest.category = formData.getRawValue().category,
                                labtest.normalvalue = formData.getRawValue().normalvalue
                        }
                        else if (labtest.category == "CHEMICAL PATHOLOGY GENERAL") {
                            labtest.id = '',
                                labtest.rev = '',
                                labtest.name = formData.getRawValue().name,
                                labtest.type = formData.getRawValue().type,
                                labtest.category = formData.getRawValue().category,
                                labtest.normalvalue = formData.getRawValue().normalvalue
                        }

                        this.db.updatelabtest(formData.getRawValue());
                        this.refresh.next(true);
                        //this.dataSource = new FilesDataSource(this.db);


                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deletelabtest(labtest);

                        break;
                }
            });
    }

    /**
     * Delete labtest
     */
    deletelabtest(labtest) {

        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                if (labtest.category == "HAEMATOLOGY AND BLOOD BANKING") {
                    var index = this.haematology.indexOf(labtest);
                    this.haematology.splice(index, 1);

                }
                else if (labtest.category == "WBC DIFFERENTIAL") {
                    var index = this.wbc.indexOf(labtest);
                    this.wbc.splice(index, 1);

                }
                else if (labtest.category == "BLOOD CROSS MATCHING") {
                    var index = this.bloodCross.indexOf(labtest);
                    this.bloodCross.splice(index, 1);
                }
                else if (labtest.category == "SEROLOGY TEST") {
                    var index = this.serology.indexOf(labtest);
                    this.serology.splice(index, 1);
                }
                else if (labtest.category == "RETROVIRAL SCREENING") {
                    var index = this.retroviral.indexOf(labtest);
                    this.retroviral.splice(index, 1);
                }
                else if (labtest.category == "VDRL") {
                    var index = this.vdrl.indexOf(labtest);
                    this.vdrl.splice(index, 1);
                }
                else if (labtest.category == "HBSAg") {
                    var index = this.hbsag.indexOf(labtest);
                    this.hbsag.splice(index, 1);
                }
                
                else if (labtest.category == "MALARIA PARASITE") {
                    var index = this.malaria.indexOf(labtest);
                    this.malaria.splice(index, 1);

                }
                else if (labtest.category == "ABO BLOOD GROUP") {
                    var index = this.abo.indexOf(labtest);
                    this.abo.splice(index, 1);
                }
                else if (labtest.category == "HCV") {
                    var index = this.hcv.indexOf(labtest);
                    this.hcv.splice(index, 1);
                }
                else if (labtest.category == "HAEMOGLOGIN GENOTYPE") {
                    var index = this.genotype.indexOf(labtest);
                    this.genotype.splice(index, 1);
                }
                else if (labtest.category == "SICKLING TEST") {
                    var index = this.sickling.indexOf(labtest);
                    this.sickling.splice(index, 1);
                }
                else if (labtest.category == "BLOOD MICROFILARIA") {
                    var index = this.bloodm.indexOf(labtest);
                    this.bloodm.splice(index, 1);
                }
                
                else if (labtest.category == "PREGNANCY TEST(Urine/Serum)") {
                    var index = this.pTest.indexOf(labtest);
                    this.pTest.splice(index, 1);

                }
                else if (labtest.category == "MICROBIOLOGY TEST") {
                    var index = this.microTest.indexOf(labtest);
                    this.microTest.splice(index, 1);
                }
                else if (labtest.category == "URINALYSIS") {
                    var index = this.urinalysis.indexOf(labtest);
                    this.urinalysis.splice(index, 1);
                }
                else if (labtest.category == "MICROSCOPY") {
                    var index = this.microscopy.indexOf(labtest);
                    this.microscopy.splice(index, 1);
                }
                else if (labtest.category == "ANTIBIOTIC SEMSITIVITY") {
                    var index = this.antibiotic.indexOf(labtest);
                    this.antibiotic.splice(index, 1);
                }
                else if (labtest.category == "SEMICAL FLUID ANALYSIS") {
                    var index = this.semical.indexOf(labtest);
                    this.semical.splice(index, 1);
                }
                else if (labtest.category == "CHEMICAL PATHOLOGY GENERAL") {
                    var index = this.pathology.indexOf(labtest);
                    this.pathology.splice(index, 1);
                }
                this.db.deletelabtest(labtest);
                //this.dataSource = new FilesDataSource(this.db);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(labtestId) {
        this.db.toggleSelectedlabtest(labtestId);
    }

}

/* export class FilesDataSource extends DataSource<any>
{
    labtest2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: labtestService) {
        super();
    }

     //Connect function called by the table to retrieve one stream containing the data to render.
    connect(): Observable<any[]> {
        var result = Observable.fromPromise(this.db.getlabtest());
        result.subscribe(x => console.log(x), e => console.error(e));
        return result;
    }

    disconnect() {
    }
}
 */