import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { labtestsService } from './labtest.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector: 'fuse-labtests',
    templateUrl: './labtest.component.html',
    styleUrls: ['./labtest.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [Animations.slideInTop]
})
export class FuselabtestsComponent implements OnInit {
    hasSelectedlabtests: boolean;
    searchInput: FormControl;

    constructor(private labtestsService: PouchService) {
        this.searchInput = new FormControl('');
       
    }

    ngOnInit() {

        this.labtestsService.onSelectedlabtestsChanged
            .subscribe(selectedlabtests => {
                this.hasSelectedlabtests = selectedlabtests.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.labtestsService.onSearchTextChanged.next(searchText);
            });
    }

}
