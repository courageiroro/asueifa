export interface labtest {
    id: string,
    rev: string,
    name: string,
    category: string,
    type: string,
    normalvalue: string
}

