import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuselabtestsComponent } from './labtest.component';
import { labtestsService } from './labtest.service';
//import { UserService } from '../../../../user.service';
//import { labtestsService2 } from './contacts.service';
import { FuselabtestlabtestListComponent } from './labtest-list/labtest-list.component';
import { FuselabtestSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuselabtestslabtestFormDialogComponent } from './labtest-form/labtest-form.component';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/labtest',
        component: FuselabtestsComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            labtests: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuselabtestsComponent,
        FuselabtestlabtestListComponent,
        FuselabtestSelectedBarComponent,
        FuselabtestslabtestFormDialogComponent
    ],
    providers      : [
        labtestsService,
        PouchService,
        AuthGuard,
       //UserService
    ],
    entryComponents: [FuselabtestslabtestFormDialogComponent]
})
export class FuselabtestsModule
{
}
