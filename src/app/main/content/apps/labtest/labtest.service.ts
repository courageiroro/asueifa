/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
///require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { labtest } from './labtest.model';
import { Schema } from '../schema';

@Injectable()
export class labtestsService {
    public labtest = "";
    public sCredentials;
    public category;
    onlabtestsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedlabtestsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    labtests: labtest[];
    user: any;
    selectedlabtests: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The labtests App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getlabtests()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getlabtests();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getlabtests();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('asueifai', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/asueifai';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/asueifai';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }


    /***********
     * labtest
     **********/
    /**
     * Save a labtest
     * @param {labtest} labtest
     *
     * @return Promise<labtest>
     */
    savelabtest(labtest: labtest): Promise<labtest> {
        //labtest.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('labtest', labtest)
            .then((data: any) => {

                if (data && data.labtests && data.labtests
                [0]) {
                    console.log('save');
                
                    return data.labtests[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a labtest
    * @param {labtest} labtest
    *
    * @return Promise<labtest>
    */
    updatelabtest(labtest: labtest) {
        return this.db.rel.save('labtest', labtest)
            .then((data: any) => {
                if (data && data.labtests && data.labtests
                [0]) {
                    console.log('Update')
                    
                    return data.labtests[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a labtest
     * @param {labtest} labtest
     *
     * @return Promise<boolean>
     */
    removelabtest(labtest: labtest): Promise<boolean> {
        return this.db.rel.del('labtest', labtest)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the labtests
     *
     * @return Promise<Array<labtest>>
     */
    getlabtests(): Promise<Array<labtest>> {
        return this.db.rel.find('labtest')
            .then((data: any) => {
                this.labtests = data.labtests;
                if (this.searchText && this.searchText !== '') {
                    this.labtests = FuseUtils.filterArrayByString(this.labtests, this.searchText);
                }
                //this.onlabtestsChanged.next(this.labtests);
                return Promise.resolve(this.labtests);
                //return data.labtests ? data.labtests : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getDoctorlabtests(): Promise<Array<labtest>> {
        return this.db.rel.find('labtest')
            .then((data: any) => {
                return data.labtests ? data.labtests : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a labtest
     * @param {labtest} labtest
     *
     * @return Promise<labtest>
     */
    getlabtest(labtest: labtest): Promise<labtest> {
        return this.db.rel.find('labtest', labtest.id)
            .then((data: any) => {
                console.log("Get")
                
                return data && data.labtests ? data.labtests[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected labtest by id
     * @param id
     */
    toggleSelectedlabtest(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedlabtests.length > 0) {
            const index = this.selectedlabtests.indexOf(id);

            if (index !== -1) {
                this.selectedlabtests.splice(index, 1);

                // Trigger the next event
                this.onSelectedlabtestsChanged.next(this.selectedlabtests);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedlabtests.push(id);

        // Trigger the next event
        this.onSelectedlabtestsChanged.next(this.selectedlabtests);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedlabtests.length > 0) {
            this.deselectlabtests();
        }
        else {
            this.selectlabtests();
        }
    }

    selectlabtests(filterParameter?, filterValue?) {
        this.selectedlabtests = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedlabtests = [];
            this.labtests.map(labtest => {
                this.selectedlabtests.push(labtest.id);
            });
        }
        else {
            /* this.selectedlabtests.push(...
                 this.labtests.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedlabtestsChanged.next(this.selectedlabtests);
    }





    deselectlabtests() {
        this.selectedlabtests = [];

        // Trigger the next event
        this.onSelectedlabtestsChanged.next(this.selectedlabtests);
    }

    deletelabtest(labtest) {
        this.db.rel.del('labtest', labtest)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const labtestIndex = this.labtests.indexOf(labtest);
                this.labtests.splice(labtestIndex, 1);
                this.onlabtestsChanged.next(this.labtests);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedlabtests() {
        for (const labtestId of this.selectedlabtests) {
            const labtest = this.labtests.find(_labtest => {
                return _labtest.id === labtestId;
            });

            this.db.rel.del('labtest', labtest)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const labtestIndex = this.labtests.indexOf(labtest);
                    this.labtests.splice(labtestIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onlabtestsChanged.next(this.labtests);
        this.deselectlabtests();
    }
}
