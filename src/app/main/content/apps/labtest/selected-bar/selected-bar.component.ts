import { Component, OnInit } from '@angular/core';
import { labtestsService } from '../labtest.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuselabtestSelectedBarComponent implements OnInit
{
    selectedlabtest: string[];
    hasSelectedlabtest: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private labtestService: PouchService,
        public dialog: MdDialog
    )
    {
        this.labtestService.onSelectedlabtestsChanged
            .subscribe(selectedlabtest => {
                this.selectedlabtest = selectedlabtest;
                setTimeout(() => {
                    this.hasSelectedlabtest = selectedlabtest.length > 0;
                    this.isIndeterminate = (selectedlabtest.length !== this.labtestService.labtest.length && selectedlabtest.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.labtestService.selectlabtests();
    }

    deselectAll()
    {
        this.labtestService.deselectlabtests();
    }

    deleteSelectedlabtest()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected labtest?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.labtestService.deleteSelectedlabtests();
            }
            this.confirmDialogRef = null;
        });
    }

}
