import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Language } from '../language.model';

@Component({
    selector: 'fuse-languages-language-form-dialog',
    templateUrl: './language-form.component.html',
    styleUrls: ['./language-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseLanguagesLanguageFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    languageForm: FormGroup;
    action: string;
    language: Language;

    constructor(
        public dialogRef: MdDialogRef<FuseLanguagesLanguageFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Bus Type';
            this.language = data.language;
        }
        else {
            this.dialogTitle = 'New Bus Type';
            this.language = {
                id: '',
                rev: '',
                phrase: '',
                english: '',
                bengali: '',
                spanish: '',
                arabic: '',
                dutch: '',
                polish: '',
                german: '',
                french: '',
                italian: '',
                russian: '',
                portugese: ''
            }
        }

        this.languageForm = this.createLanguageForm();
    }

    ngOnInit() {
    }

    createLanguageForm() {
        return this.formBuilder.group({
            id: [this.language.id],
            rev: [this.language.rev],
            phrase: [this.language.phrase],
            english: [this.language.english],
            bengali: [this.language.bengali],
            spanish: [this.language.spanish],
            arabic: [this.language.arabic],
            dutch: [this.language.dutch],
            polish: [this.language.polish],
            german: [this.language.german],
            french: [this.language.french],
            italian: [this.language.italian],
            russian: [this.language.russian],
            portugese: [this.language.portugese]

        });
    }
}
