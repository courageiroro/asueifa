import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { LanguagesService } from '../languages.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject} from 'rxjs/Rx';
import { FuseLanguagesLanguageFormDialogComponent } from '../language-form/language-form.component';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Language } from '../language.model';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-languages-language-list',
    templateUrl: './language-list.component.html',
    styleUrls  : ['./language-list.component.scss']
})
export class FuseLanguagesLanguageListComponent implements OnInit
{
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public languages: Array<Language> = [];
    languages2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'language', 'buttons'];
    selectedLanguages: any[];
    checkboxes: {};

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(public dialog: MdDialog, public db:PouchService)
    { }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db);
        this._loadlanguages();
    }

    private _loadlanguages(): void {
         this.db.onLanguagesChanged.subscribe(data =>{
            this.dataSource = new FilesDataSource(this.db);
        })
        this.db.getLanguages()
        .then((languages: Array<Language>) => {
            this.languages = languages;
            this.languages2= new BehaviorSubject<any>(languages);
            //console.log(this.languages2);

            this.checkboxes = {};
            languages.map(language => {
                this.checkboxes[language.id] = false;
            });
             this.db.onSelectedLanguagesChanged.subscribe(selectedLanguages => {
                for ( const id in this.checkboxes )
                {
                    this.checkboxes[id] = selectedLanguages.includes(id);
                }

                this.selectedLanguages = selectedLanguages;
            });
        this.db.onSearchTextChanged.subscribe(data =>{
            this.dataSource = new FilesDataSource(this.db);
        })
        });
        
    }

    newLanguage()
    {
        this.dialogRef = this.dialog.open(FuseLanguagesLanguageFormDialogComponent, {
            panelClass: 'language-form-dialog',
            data      : {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if ( !response )
                {
                    return;
                }

                this.db.saveLanguage(response.getRawValue());
                this.dataSource = new FilesDataSource(this.db);

            });

    }

    editLanguage(language)
    {
        this.dialogRef = this.dialog.open(FuseLanguagesLanguageFormDialogComponent, {
            panelClass: 'language-form-dialog',
            data      : {
                language: language,
                action : 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch ( actionType )
                {
                    /**
                     * Save
                     */
                    case 'save':

                        this.db.updateLanguage(formData.getRawValue());
                        this.dataSource = new FilesDataSource(this.db);

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteLanguage(language);

                        break;
                }
            });
    }

    /**
     * Delete language
     */
    deleteLanguage(language)
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.db.deleteLanguage(language);
                this.dataSource = new FilesDataSource(this.db);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(languageId)
    {
        this.db.toggleSelectedLanguage(languageId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    languages2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService)
    {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]>
    {
        var result = Observable.fromPromise(this.db.getLanguages());
        result.subscribe(x => console.log(x), e => console.error(e));
        return result;    
    }

    disconnect()
    {
    }
}
