export interface Language
{
	id: string,
	rev: string,
	phrase: string,
	english: string,
	bengali: string,
	spanish: string,
	arabic: string,
	dutch: string,
	polish: string,
	german: string,
	french: string,
	italian: string,
	russian: string,
	portugese: string,
}

