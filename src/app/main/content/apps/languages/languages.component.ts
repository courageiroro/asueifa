import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { LanguagesService } from './languages.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-languages',
    templateUrl  : './languages.component.html',
    styleUrls    : ['./languages.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FuseLanguagesComponent implements OnInit
{
    hasSelectedLanguages: boolean;
    searchInput: FormControl;

    constructor(private languagesService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.languagesService.onSelectedLanguagesChanged
            .subscribe(selectedLanguages => {
                this.hasSelectedLanguages = selectedLanguages.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.languagesService.onSearchTextChanged.next(searchText);
            });
    }

}
