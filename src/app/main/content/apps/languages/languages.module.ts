import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseLanguagesComponent } from './languages.component';
import { LanguagesService } from './languages.service';
import { FuseLanguagesLanguageListComponent } from './language-list/language-list.component';
import { FuseLanguagesSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseLanguagesLanguageFormDialogComponent } from './language-form/language-form.component';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : '**',
        component: FuseLanguagesComponent,
        children : [],
        resolve  : {
            languages: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseLanguagesComponent,
        FuseLanguagesLanguageListComponent,
        FuseLanguagesSelectedBarComponent,
        FuseLanguagesLanguageFormDialogComponent
    ],
    providers      : [
        LanguagesService,
        PouchService
    ],
    entryComponents: [FuseLanguagesLanguageFormDialogComponent]
})
export class FuseLanguagesModule
{
}
