/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Language } from './language.model';
import { Schema } from '../schema';

@Injectable()
export class LanguagesService {
    public languagename = "";

    onLanguagesChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedLanguagesChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    languages: Language[];
    user: any;
    selectedLanguages: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB();
    }

    /**
     * The languages App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getLanguages()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getLanguages();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getLanguages();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB() {
        this.db = new PouchDB('hospital_demo', { adaptater: 'websql' });
        this.db.setSchema(Schema);
        this.remote = 'http://sarutech.com:5984/hospital_demo';

        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };

        this.db.sync(this.remote, options);

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }


    /***********
     * language
     **********/
    /**
     * Save a language
     * @param {language} language
     *
     * @return Promise<language>
     */
    saveLanguage(language: Language): Promise<Language> {
        language.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('language', language)
            .then((data: any) => {
                if (data && data.languages && data.languages
                [0]) {
                    return data.languages[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a language
    * @param {language} language
    *
    * @return Promise<language>
    */
    updateLanguage(language: Language) {
    
        return this.db.rel.save('language', language)
            .then((data: any) => {
                if (data && data.languages && data.languages
                [0]) {
                    return data.languages[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a language
     * @param {language} language
     *
     * @return Promise<boolean>
     */
    removeLanguage(language: Language): Promise<boolean> {
        return this.db.rel.del('language', language)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the languages
     *
     * @return Promise<Array<language>>
     */
    getLanguages(): Promise<Array<Language>> {
        return this.db.rel.find('language')
            .then((data: any) => {
                this.languages = data.languages;
                 if ( this.searchText && this.searchText !== '' )
                {
                    this.languages = FuseUtils.filterArrayByString(this.languages, this.searchText);
                }
                //this.onlanguagesChanged.next(this.languages);
                return Promise.resolve(this.languages);
                //return data.languages ? data.languages : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }
    /**
     * Read a language
     * @param {language} language
     *
     * @return Promise<language>
     */
    getLanguage(language: Language): Promise<Language> {
        return this.db.rel.find('language', language.id)
            .then((data: any) => {
                return data && data.languages ? data.languages[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected language by id
     * @param id
     */
    toggleSelectedLanguage(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedLanguages.length > 0) {
            const index = this.selectedLanguages.indexOf(id);

            if (index !== -1) {
                this.selectedLanguages.splice(index, 1);

                // Trigger the next event
                this.onSelectedLanguagesChanged.next(this.selectedLanguages);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedLanguages.push(id);

        // Trigger the next event
        this.onSelectedLanguagesChanged.next(this.selectedLanguages);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedLanguages.length > 0) {
            this.deselectLanguages();
        }
        else {
            this.selectLanguages();
        }
    }

    selectLanguages(filterParameter?, filterValue?) {
        this.selectedLanguages = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedLanguages = [];
            this.languages.map(language => {
                this.selectedLanguages.push(language.id);
            });
        }
        else {
            /* this.selectedlanguages.push(...
                 this.languages.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedLanguagesChanged.next(this.selectedLanguages);
    }





    deselectLanguages() {
        this.selectedLanguages = [];

        // Trigger the next event
        this.onSelectedLanguagesChanged.next(this.selectedLanguages);
    }

    deleteLanguage(language) {
        this.db.rel.del('language', language)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const languageIndex = this.languages.indexOf(language);
                this.languages.splice(languageIndex, 1);
                this.onLanguagesChanged.next(this.languages);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedLanguages() {
        for (const languageId of this.selectedLanguages) {
            const language = this.languages.find(_language => {
                return _language.id === languageId;
            });

            this.db.rel.del('language', language)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const languageIndex = this.languages.indexOf(language);
                    this.languages.splice(languageIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onLanguagesChanged.next(this.languages);
        this.deselectLanguages();
    }
}
