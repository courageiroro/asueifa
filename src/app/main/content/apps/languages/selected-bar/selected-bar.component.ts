import { Component, OnInit } from '@angular/core';
import { LanguagesService } from '../languages.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseLanguagesSelectedBarComponent implements OnInit
{
    selectedLanguages: string[];
    hasSelectedLanguages: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private languagesService: PouchService,
        public dialog: MdDialog
    )
    {
        this.languagesService.onSelectedLanguagesChanged
            .subscribe(selectedLanguages => {
                this.selectedLanguages = selectedLanguages;
                setTimeout(() => {
                    this.hasSelectedLanguages = selectedLanguages.length > 0;
                    this.isIndeterminate = (selectedLanguages.length !== this.languagesService.languages.length && selectedLanguages.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.languagesService.selectLanguages();
    }

    deselectAll()
    {
        this.languagesService.deselectLanguages();
    }

    deleteSelectedLanguages()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected languagees?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.languagesService.deleteSelectedLanguages();
            }
            this.confirmDialogRef = null;
        });
    }

}
