import { Inject, Injectable } from '@angular/core';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AccountsService } from '../accounts/accounts.service';
import { Animations } from '../../../../core/animations';
import { DOCUMENT } from '@angular/common';
import { animate, AnimationBuilder, AnimationPlayer, style } from '@angular/animations';
import { BehaviorSubject } from 'rxjs/Rx';
import { FuseSplashScreenService } from './loaders.service';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';

@Component({
    selector: 'fuse-loaders',
    templateUrl: './loaders.component.html',
    styleUrls: ['./loaders.component.scss'],
    /* encapsulation: ViewEncapsulation.None,*/
    animations: [Animations.slideInTop]
})
export class FuseLoadersComponent implements OnInit {
    splashScreenEl;
    public player: AnimationPlayer;

     constructor(
        private animationBuilder: AnimationBuilder,
        @Inject(DOCUMENT) private document: any,
        private router: Router
    ) {

     this.splashScreenEl = this.document.body.querySelector('#fuse-splash-screen');

        const hideOnLoad = this.router.events.subscribe((event) => {
                if ( event instanceof NavigationEnd )
                {
                    setTimeout(() => {
                        this.hide();
                        hideOnLoad.unsubscribe();
                    }, 0);
                }
            }
        );;
 
    }

      show()
    {
        this.player =
            this.animationBuilder
                .build([
                    style({
                        opacity: '0',
                        zIndex : '99999'
                    }),
                    animate('400ms ease', style({opacity: '1'}))
                ]).create(this.splashScreenEl);

        setTimeout(() => {
            this.player.play();
        }, 0);
    }

       hide()
    {
        this.player =
            this.animationBuilder
                .build([
                    style({opacity: '1'}),
                    animate('400ms ease', style({
                        opacity: '0',
                        zIndex : '-10'
                    }))
                ]).create(this.splashScreenEl);

        setTimeout(() => {
            this.player.play();
        }, 0);
    }

    ngOnInit() {

    }

  
}