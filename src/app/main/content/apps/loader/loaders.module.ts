import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import {FuseLoadersComponent} from './loaders.component';
import {FuseSplashScreenService} from './loaders.service';
//import { AdminsService } from './admins.service';
import { AccountsService } from '../accounts/accounts.service';
import { StaffsService } from '../staffs/staffs.service';

const routes: Routes = [
    {
        path: 'apps/loader',
        component: FuseLoadersComponent,
        children: [],
        resolve: {
            //admins: AdminsService
        }
    }
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        FuseLoadersComponent,
    ],
    providers: [
        //AdminsService,
        AccountsService,
        StaffsService,
        FuseSplashScreenService
    ],
    //entryComponents: [FuseAdminsAdminFormDialogComponent]
})
export class FuseLoadersModule {
}
