import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg'
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import { BehaviorSubject } from 'rxjs/Rx';
import 'rxjs/Rx';
import { Staff } from '../../staffs/staff.model';
declare var jquery: any;
declare var $: any;
import { DepartmentsService } from '../../departments/departments.service';
import { StaffsService } from '../../staffs/staffs.service';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-logins-login-form-dialog',
    templateUrl: './login-form.component.html',
    styleUrls: ['./login-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseLoginsLoginFormDialogComponent implements OnInit {

    event: CalendarEvent;
    dialogTitle: string;
    staffForm: FormGroup;
    action: string;
    staff: Staff;
    public departmentss;
    public imagestring;
    public retrieve;
    public file;
    public userTypes;
    defaultImage;


    constructor(
        public dialogRef: MdDialogRef<FuseLoginsLoginFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        public db: PouchService,
        
    ) {

        /*  var userType = { "usertype": "null" }
         localStorage.setItem('user', JSON.stringify(userType)); */

        document.addEventListener('click', function () {
            db.file = document.querySelector('#fileupload')
            var imagestring = document.querySelector('#imagestring');
            console.log(imagestring)
            var retrieve;
        })

        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Admin';
            this.staff = data.staff;
        }
        else {
            this.dialogTitle = 'New Admin';
            this.staff = {
                id: '',
                rev: '',
                name: '',
                image: '',
                email: '',
                password: '',
                address: '',
                phone: '',
                department_id: '',
                profile: '',
                _attachments: '',
                secretquestion: '',
                answer: '',
                usertype: '',
                sex: '',
                patientno:'',
                birth_date: new Date(),
                age: '',
                blood_group: '',
                record: '',
                records: '',
                labrecord: '',
                labrecords: '',
                insurancename: '',
                insuranceid: '',
                insurancepercent: null,
                account_opening_timestamp: '',
                imageurl: '',
                appointments: [],
                bed_allotments: [],
                invoices: [],
                medicine_sales: [],
                prescriptions: [],
                reports: [],
                email_templates: [],
                smss: [],
                appointment: [],
                diagnosis_reports: [],
                blood_sales: []
            }
        }

        db.getDepartments().then(res => {
            this.departmentss = res;
        })

        this.staffForm = this.createstaffForm();
    }

    ngOnInit() {
        this.userTypes = ['Admin']
    }


    createstaffForm() {
        return this.formBuilder.group({
            id: [this.staff.id],
            rev: [this.staff.rev],
            name: [this.staff.name],
            image: [this.staff.image],
            usertype: [this.staff.usertype],
            email: [this.staff.email],
            password: [this.staff.password],
            address: [this.staff.address],
            secretquestion: [this.staff.secretquestion],
            answer: [this.staff.answer],
            _attachments: [this.staff._attachments],
            phone: [this.staff.phone],
            department_id: [this.staff.department_id],
            profile: [this.staff.profile],
            patientno: [this.staff.patientno],
            sex: [this.staff.sex],
            birth_date: [this.staff.birth_date],
            imageurl: [this.staff.imageurl],
            age: [this.staff.age],
            blood_group: [this.staff.blood_group],
            account_opening_timestamp: [this.staff.account_opening_timestamp],
            record: [this.staff.record],
            records: [this.staff.records],
            labrecord: [this.staff.record],
            labrecords: [this.staff.records],
            insurancename: [this.staff.insurancename],
            insuranceid: [this.staff.insuranceid],
            insurancepercent: [this.staff.insurancepercent]
        });
    }
}
