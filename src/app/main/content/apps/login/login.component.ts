import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../../user.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FuseConfigService } from './../../../../core/services/config.service';
import { Router } from '@angular/router';
import { FuseConfirmDialogComponent } from './../../../../core/components/confirm-dialog/confirm-dialog.component';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseLoginsLoginFormDialogComponent } from './login-form/login-form.component';
import {PouchService} from './../../../../provider/pouch-service';
/* import { AppsService } from './../../../../app.service'; */

@Component({
    selector: 'fuse-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class FuseLoginComponent implements OnInit {
    loginForm: FormGroup;
    loginFormErrors: any;
    public username: any;
    public password: any;
    public error: any;
    public inputType;
    dialogRef: any;
    show;
    disable = false;
    public loading = false;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private fuseConfig: FuseConfigService,
        private formBuilder: FormBuilder,
        private staffsservice: PouchService,
        private router: Router,
        private user: UserService,
        public dialog: MdDialog,
        /* private appService: AppsService */
    ) {

        /* var userType = { usertype: null }
        localStorage.setItem('user', JSON.stringify(userType)); */
        localStorage.removeItem('user');
        localStorage.removeItem('authguard'); 
        
       /*  this.staffsservice.getGStaffs().then(res => {
            
        }) */

        this.show = "false";

        this.staffsservice.getADStaffs().then(res => {
            if (res.length > 0) {
                this.show = "true";
            }
            
        })

        this.fuseConfig.setSettings({
            layout: {
                navigation: 'none',
                toolbar: 'none',
                footer: 'none'
            }
        });

        this.loginFormErrors = {
            email: {},
            password: {}
        };

       /*  this.staffsservice.getGStaffs().then(res => {
            
        }) */
    }

    newStaff() {

        this.router.navigate(['/pages/auth/create-account'])

        /*    this.dialogRef = this.dialog.open(FuseLoginsLoginFormDialogComponent, {
               panelClass: 'staff-form-dialog',
               data: {
                   action: 'new'
               }
           });
   
           this.dialogRef.afterClosed()
               .subscribe((response: FormGroup) => {
                   if (!response) {
                       return;
                   }
   
                   var res = response.getRawValue();
   
                   res.profile
                   console.log(res.profile)
                   this.staffsservice.saveStaff(res);
                   console.log(res);
                   this.dataSource = new FilesDataSource(this.db);
   
               });
    */
    }

    ngOnInit() {

        this.staffsservice.getADStaffs().then(res => {
           if(res.length > 0){
               this.disable = true;
           }
        });

        this.loginForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });

        this.loginForm.valueChanges.subscribe(() => {
            this.onLoginFormValuesChanged();
        });
    }

    login() {
        /* this.appService.initDB(); */
        this.staffsservice.getGStaffs().then(res => {
            
            for (var i = 0; i < res.length; i++) {
                if (res[i].email == this.username && res[i].password == this.password) {
                    this.user.setUserLoggedIn();
                    

                    localStorage.setItem('user', JSON.stringify(res[i]));
                    var authguard = {status: this.user.isUserLoggedIn}
                    localStorage.setItem('authguard',JSON.stringify(authguard));

                    if (res[i].usertype == 'Admin') {
                        
                        this.loading = true;
                        this.router.navigate(['apps/dashboards/project']).then(res => {
                            this.loading = false;
                        }, err => {
                            this.loading = false;
                        });
                    }
                    else if (res[i].usertype == 'Laboratorist') {
                        this.loading = true;
                        this.router.navigate(['apps/lab_dashboards']).then(res => {
                            this.loading = false;
                        }, err => {
                            this.loading = false;
                        });
                    }
                    else if (res[i].usertype == 'Accountant') {
                        this.loading = true;
                        this.router.navigate(['apps/lab_dashboards']).then(res => {
                            this.loading = false;
                        }, err => {
                            this.loading = false;
                        });
                    }
                    else if (res[i].usertype == 'Receptionist') {
                        this.loading = true;
                        this.router.navigate(['apps/lab_dashboards']).then(res => {
                            this.loading = false;
                        }, err => {
                            this.loading = false;
                        });
                    }
                    else if (res[i].usertype == 'Pharmacist') {
                        this.loading = true;
                        this.router.navigate(['apps/lab_dashboards']).then(res => {
                            this.loading = false;
                        }, err => {
                            this.loading = false;
                        });
                    }
                    else if (res[i].usertype == 'Nurse') {
                        this.loading = true;
                        this.router.navigate(['apps/lab_dashboards']).then(res => {
                            this.loading = false;
                        }, err => {
                            this.loading = false;
                        });
                    }
                    else if (res[i].usertype == 'Doctor') {
                        this.loading = true;
                        this.router.navigate(['apps/addappointments']).then(res => {
                            this.loading = false;
                        }, err => {
                            this.loading = false;
                        });
                    }
                    else if (res[i].usertype == 'Patient') {
                        this.loading = true;
                        this.router.navigate(['apps/lab_dashboards']).then(res => {
                            this.loading = false;
                        }, err => {
                            this.loading = false;
                        });
                    }

                    break;
                }
            }
            if (this.user.isUserLoggedIn == false) {
                
                this.error = "Username or Password is not correct: Try Again or Ask Admin for Help";
                
            }
        })
    }

    navFP() {
        this.router.navigate(['/pages/auth/forgot-password'])
    }

    isActiveToggleTextPassword: Boolean = true;
    public toggleTextPassword(): void {
        this.isActiveToggleTextPassword = (this.isActiveToggleTextPassword == true) ? false : true;
    }

    public getType() {
        return this.isActiveToggleTextPassword ? 'password' : 'text';
    }

    onLoginFormValuesChanged() {
        for (const field in this.loginFormErrors) {
            if (!this.loginFormErrors.hasOwnProperty(field)) {
                continue;
            }

            // Clear previous errors
            this.loginFormErrors[field] = {};

            // Get the control
            const control = this.loginForm.get(field);

            if (control && control.dirty && !control.valid) {
                this.loginFormErrors[field] = control.errors;
            }
        }
    }
}
