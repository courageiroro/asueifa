import { NgModule } from '@angular/core';
import { SharedModule } from './../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { UserService } from '../../../../user.service';
import { StaffsService} from '../staffs/staffs.service'
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { FuseLoginComponent } from './login.component';
import {FuseLoginsLoginFormDialogComponent} from './login-form/login-form.component';
import { DepartmentsService } from './../departments/departments.service';
import { LoadingModule } from 'ngx-loading';
import { RecordsService } from './../records/records.service';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path: 'apps/login',
        component: FuseLoginComponent,
        children: [],
        resolve: {
            staffs: PouchService
        }
    }
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes),
        FroalaEditorModule.forRoot(),
        FroalaViewModule.forRoot(),
        LoadingModule
    ],
    declarations: [
        FuseLoginComponent,
        FuseLoginsLoginFormDialogComponent
    ],
    providers: [
        StaffsService,
        PouchService,
        UserService,
        DepartmentsService,
        RecordsService
    ],
    entryComponents: [FuseLoginsLoginFormDialogComponent]
})

export class FuseLoginModule {

}
