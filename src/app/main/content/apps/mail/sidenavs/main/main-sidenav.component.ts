import { Component, OnDestroy, OnInit } from '@angular/core';
import { MailService } from '../../mail.service';
import { Subscription } from 'rxjs/Subscription';
import { FuseMailComposeDialogComponent } from '../../dialogs/compose/compose.component';
import { MdDialog } from '@angular/material';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
    selector: 'fuse-mail-main-sidenav',
    templateUrl: './main-sidenav.component.html',
    styleUrls: ['./main-sidenav.component.scss']
})
export class FuseMailMainSidenavComponent implements OnInit, OnDestroy {
    folders: any[];
    filters: any[];
    labels: any[];
    accounts: object;
    selectedAccount: string;
    dialogRef: any;
    array2: any[];
    finalArray: any;
    folderHandle;
    onCurrentMailChanged;
    currentMail;
    onFoldersChanged: Subscription;
    onFiltersChanged: Subscription;
    onLabelsChanged: Subscription;
    numberId;
    routerLink;
    routerLink2;
    refresh: Subject<any> = new Subject();

    constructor(
        private mailService: MailService,
        public dialog: MdDialog,
        private route: ActivatedRoute,
        private location: Location,
        private router: Router
    ) {
        // Data
        this.accounts = {
            'creapond': 'johndoe@creapond.com',
            'withinpixels': 'johndoe@withinpixels.com'
        };

        this.selectedAccount = 'creapond';
    }

    ngOnInit() {
        this.onFoldersChanged =
            this.mailService.onFoldersChanged
                .subscribe(folders => {
                    this.folders = folders;
                    

                });

        this.onFiltersChanged =
            this.mailService.onFiltersChanged
                .subscribe(filters => {
                    this.filters = filters;
                });

        this.onLabelsChanged =
            this.mailService.onLabelsChanged
                .subscribe(labels => {
                    this.labels = labels;
                });
    }
    /*  getHandle(get) {
         var array = get.handle.join();
         //return array;
     } */


    composeDialog() {
        this.dialogRef = this.dialog.open(FuseMailComposeDialogComponent, {
            panelClass: 'mail-compose-dialog'
        });
        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Send
                     */
                    case 'send':
                        console.log('new Mail', formData.getRawValue());
                        break;
                    /**
                     * Delete
                     */
                    case 'delete':
                        console.log('delete Mail');
                        break;
                }
            });
    }

    ngOnDestroy() {
        this.onFoldersChanged.unsubscribe();
        this.onFiltersChanged.unsubscribe();
        this.onLabelsChanged.unsubscribe();
    }

    send() {
     /*    var time = setTimeout(() => {
            alert("Waited");
        }, 5000)
 */
        /*  this.router.navigate(['/apps/mail/inbox']).then(res => {
             alert(this.mailService.folderId);
         })
         console.log(this.folderHandle)
         this.refresh.next(true); */
        /*  this.mailService.getFolder(id).then(res=>{
             console.log(res);
         }) */
        /*  var folderHandle2 = this.route.snapshot.params.folderHandle;
         console.log(folderHandle2); */
        /*  var array2 = [];
         var array = this.folders.map(function (obj) {
             console.log(obj.id);
             array2.push(obj.id);
             console.log(array2)
             console.log(array2.length)
 
             return obj.id
 
         });   */
        //this.mailService.folderId2 = 
    }
}
