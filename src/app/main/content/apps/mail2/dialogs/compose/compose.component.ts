import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MailService } from '../../mail.service';
import { Message } from '../../mail.model';

@Component({
    selector: 'fuse-mail-compose',
    templateUrl: './compose.component.html',
    styleUrls: ['./compose.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class FuseMailComposeDialogComponent implements OnInit {
    composeForm: FormGroup;
    public localStorageItem: any;
    public localStorageType: any;
    public localStorageName: any;
    public localStorageEmail: any;
    emailTo;
    action: string;
    message: Message;
    dialogTitle: string;
    fileName;

    constructor(
        private db: MailService,
        public dialogRef: MdDialogRef<FuseMailComposeDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder
    ) {

        document.addEventListener('click', function () {
            db.file = document.querySelector('#fileupload')
            var imagestring = document.querySelector('#imagestring');
            
            var retrieve;
        })


        this.localStorageItem = JSON.parse(localStorage.getItem('user'));
        this.localStorageType = this.localStorageItem.usertype;
        this.localStorageName = this.localStorageItem.name;
        this.localStorageEmail = this.localStorageItem.email;
        this.emailTo;

        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Forward Message';
            this.message = data.message;
        }
        else {
            //this.dialogTitle = 'New Message';
            this.message = {
                id: '',
                from: this.localStorageEmail,
                to: '',
                subject: '',
                message: '',
                document_name:'',
                image:'',
                staff_id:'',
                time: new Date(),
                read: false,
                starred: false,
                important: false,
                _attachments:[''],
                hasAttachments: false,
                attachments: [{ type: '', fileName: '', preview: '', url: '', size: '' }],
                labels: [''],
                folder: null,
                toggleStar() {
                    this.starred = !this.starred;
                },

                toggleImportant() {
                    this.important = !this.important;
                }

            }
            /*  id: '';
             from: string;
             to: string;
             subject: string;
             message: string;
             time: string;
             read: boolean;
             starred: boolean;
             important: boolean;
             hasAttachments: boolean;
             attachments: {
                 type: string,
                     fileName: string,
                         preview: string,
                             url: string,
                                 size: string
             } [];
             labels: string[];
             folder: number; */
        }

        this.composeForm = this.createComposeForm();
    }
    attachFile(){

    }

    ngOnInit() {
    }

    createComposeForm() {
        return this.formBuilder.group({
            id: [this.message.id],
            //from:new FormControl([this.message.from]),
            from: new FormControl({
                value: [this.message.from],
                disabled: true
            }),
            to: [this.message.to],
            _attachments: [this.message._attachments],
            document_name: [this.message.document_name],
            staff_id: [this.message.staff_id],
            cc: new FormControl(''),
            bcc: new FormControl(''),
            subject: [this.message.subject],
            message: [this.message.message],
            time: new Date(this.message.time).toDateString(),
        });
    }

}
