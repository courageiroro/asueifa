import { Component, OnDestroy, OnInit } from '@angular/core';
import { MailService } from '../mail.service';
import { Message } from '../mail.model';
import { Subscription } from 'rxjs/Subscription';
import { MdDialog } from '@angular/material';
import { FuseMailComposeDialogComponent } from './../dialogs/compose/compose.component';
import { FormGroup } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { Subject } from 'rxjs/Subject';
import { StaffsService } from '../../staffs/staffs.service';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-mail-details',
    templateUrl: './mail-details.component.html',
    styleUrls: ['./mail-details.component.scss']
})
export class FuseMailDetailsComponent implements OnInit, OnDestroy {
    message: any;
    message2: any;
    labels: any[];
    showDetails = false;
    dialogRef: any;
    url;
    urlStaff;
    url2;
    fileType;
    attach;
    docName;
    public localStorageItem: any;
    public localStorageType: any;
    public localStorageName: any;
    public localStorageEmail: string;
    refresh: Subject<any> = new Subject();

    onCurrentMailChanged: Subscription;
    onLabelsChanged: Subscription;

    constructor(
        private mailService: MailService,
        public dialog: MdDialog,
        public _DomSanitizer: DomSanitizer,
        private staffService: PouchService
    ) {
        this.localStorageItem = JSON.parse(localStorage.getItem('user'));
        this.localStorageType = this.localStorageItem.usertype;
        this.localStorageName = this.localStorageItem.name;
        this.localStorageEmail = this.localStorageItem.email;
    }

    ngOnInit() {
        // Subscribe to update the current mail
        this.onCurrentMailChanged =
            this.mailService.onCurrentMailChanged
                .subscribe(currentMail => {
                    this.message = currentMail;
                    

                    this.mailService.getMail(currentMail).then(res => {
                        console.log(currentMail.image);
                        this.mailService.imageId = currentMail.id
                        this.docName = currentMail.document_name
                        console.log(this.docName);
                        
                        //this.message = res;
                        this.fileType = currentMail._attachments.type;
                        /* var URLObj = window.URL; */
                        this.url = this.message.image;
                        console.log(this.url);

                        this.staffService.getStaff2(currentMail.staff_id).then(res => {
                            this.attach = res.image
                            if (this.attach) {
                                this.attach = "true";
                                this.urlStaff = res.image;
                            }
                            else {
                                this.attach = "false";
                            }
                        })

                    })


                });
        this.message = "null"
        // Subscribe to update on label change
        this.onLabelsChanged =
            this.mailService.onLabelsChanged
                .subscribe(labels => {
                    this.labels = labels;
                });


    }

    private loadImage(message) {
        
    }

    ngOnDestroy() {
        this.onCurrentMailChanged.unsubscribe();
    }

    toggleStar(event) {
        event.stopPropagation();

        this.message.toggleStar();

        this.mailService.updateMail(this.message);
    }

    toggleImportant(event) {
        event.stopPropagation();

        this.message.toggleImportant();

        this.mailService.updateMail(this.message);
    }

    print() {
        this.mailService.hide = "hidden";
        var printContent = document.getElementById('printArea').innerHTML;
        var printWindow = window.open('', '_blank', 'top=0,left=0,height=auto,width=auto');
        printWindow.document.open();
        printWindow.document.write(`<html><head><title> `, `</title><style>
                    .subject {
                                font-size: 30px;
                                font-weight: 500;
                                position:absolute;
                                left:40%;
                                top:10%
                             }
                            .vetfloat{
                                position:absolute;
                                left:100%;
                            }
                             .floatdetail{
                                     position:absolute;
                                     left:100%;
                                }
                                .invoice-date {
                            font-size: 10px;
                            color: rgba(0, 0, 0, 0.54);
                            margin-bottom: 32px;
                        }
                         .messagefloat {
                                font-size: 18px;
                                padding-bottom: 2px;
                                position:absolute;
                                left:2%;
                                top:15%
                                }
                            .avatar {
                                    position:absolute;
                                    left:100%;
                                }
                                .due-date {
                                font-size: 12px;
                                padding-bottom: 6px;
                                }
                             .date {
                                    padding-left: 6px;
                                }
                                .info {
                                color: rgba(0, 0, 0, 0.54);
                                line-height: 22px;
                                font-size: 12px;
                                padding-bottom: 12px;
                            }
                            .issuer {
                            margin-right: -58px;
                            padding-right: 66px;
                            }
                        #co-size {
                        visibility: hidden   
                                  }
                    .invoice-table {
                            margin-top: 64px;
                            font-size: 15px;
                            }
                        .text-right{
                            padding-left: 150px;
                        }
                        .invoice-table-footer {
                            margin: 32px 0 72px 0;
                            }
                        td.line{
                           border-bottom: 1px solid rgba(0, 0, 0, 0.12);
                        }
                         td.mytotal {
                                        padding: 24px 8px;
                                        font-size: 35px;
                                        font-weight: 300;
                                        color: rgba(0, 0, 0, 1);
                                    }
                                    .alignright{
                                        padding-left: 100px;
                                    }
                                    #alignright{
                                        padding-left: 100px;
                                    }
          </style></head><body></body></html>`);
        printWindow.document.write(printContent);
        printWindow.document.close();
        printWindow.print();
        //window.print();
    }

    composeDialog(message) {
        message.from = message.to
        
        this.dialogRef = this.dialog.open(FuseMailComposeDialogComponent, {
            panelClass: 'mail-compose-dialog',
            data: {
                message: message,
                action: 'edit'
            }
        });
        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {

                    /*  Send */

                    case 'send':
                        this.mailService.emailTo = formData.getRawValue().to
                    
                        const messageData = formData.getRawValue();

                        this.mailService.saveMessage(messageData).then(res => {
                            if (res.from != this.localStorageEmail) {
                                this.mailService.mails.push(messageData);
                                this.mailService.onMailsChanged.next(this.mailService.mails);
                            }

                        })
                        this.refresh.next(true);
                        
                        break;

                    /*  Delete */

                    case 'delete':
                        console.log('delete Mail');
                        break;

                }
            });
    }

}
