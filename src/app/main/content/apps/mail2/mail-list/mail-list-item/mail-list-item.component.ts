import { Component, HostBinding, Input, OnDestroy, OnInit, EventEmitter } from '@angular/core';
import { Message } from '../../mail.model';
import { MailService } from '../../mail.service';
import { Subscription } from 'rxjs/Subscription';
import { DomSanitizer } from '@angular/platform-browser';
import { Staff } from '../../../staffs/staff.model';
import { StaffsService } from '../../../staffs/staffs.service';
import {PouchService} from '../../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-mail-list-item',
    templateUrl: './mail-list-item.component.html',
    styleUrls: ['./mail-list-item.component.scss'],
    outputs: ['childEvents']
})
export class FuseMailListItemComponent implements OnInit, OnDestroy {
    childEvent = new EventEmitter<string>();
    img;
    @Input() message: Message;
    staff
    messages;
    url;
    labels: any[];
    @HostBinding('class.selected') selected: boolean;

    routeSnap;
    onSelectedMailsChanged: Subscription;
    onLabelsChanged: Subscription;

    constructor(
        private mailService: MailService,
        public _DomSanitizer: DomSanitizer,
        private staffService: PouchService
    ) {
    }

    ngOnInit() {
        //this.mailService.routerSnapShot = "inbox";
        
        //this.img = 'assets/EP2.png';
        // Set the initial values
        if (this.message.from != "johndoe@creapond.com") {
            this.message = new Message(this.message);
        }

        this.staff = Number(this.message.staff_id)
        
        this.staffService.getStaff2(this.message.staff_id).then(res => {            
             this.url = res.image;
        })
        /*  this.mailService.getMailsByFolder().then(res=>{
             this.messages = res;
             console.log(this.messages);
         }) */

        // Subscribe to update on selected mail change
        this.onSelectedMailsChanged =
            this.mailService.onSelectedMailsChanged
                .subscribe(selectedMails => {
                    this.selected = false;

                    if (selectedMails.length > 0) {
                        for (const mail of selectedMails) {
                            if (mail.id === this.message.id) {
                                this.selected = true;
                                break;
                            }
                        }
                    }
                });

        // Subscribe to update on label change
        this.onLabelsChanged =
            this.mailService.onLabelsChanged
                .subscribe(labels => {
                    this.labels = labels;
                });
    }

    ngOnDestroy() {
        this.onSelectedMailsChanged.unsubscribe();
    }

    onSelectedChange() {
        this.mailService.toggleSelectedMail(this.message.id);
        this.mailService.message = this.message;
        
    }

    /**
     * Toggle star
     * @param event
     */
    toggleStar(event) {
        event.stopPropagation();

        this.message.toggleStar();

        this.mailService.updateMail(this.message);
    }

    /**
     * Toggle Important
     * @param event
     */
    toggleImportant(event) {
        event.stopPropagation();

        this.message.toggleImportant();

        this.mailService.updateMail(this.message);
    }
}
