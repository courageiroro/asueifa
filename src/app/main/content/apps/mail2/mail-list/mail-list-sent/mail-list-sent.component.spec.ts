import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MailListSentComponent } from './mail-list-sent.component';

describe('MailListSentComponent', () => {
  let component: MailListSentComponent;
  let fixture: ComponentFixture<MailListSentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MailListSentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MailListSentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
