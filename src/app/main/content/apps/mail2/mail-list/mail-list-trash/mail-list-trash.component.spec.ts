import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MailListTrashComponent } from './mail-list-trash.component';

describe('MailListTrashComponent', () => {
  let component: MailListTrashComponent;
  let fixture: ComponentFixture<MailListTrashComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MailListTrashComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MailListTrashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
