import { Component, OnInit, HostBinding, Input, OnDestroy } from '@angular/core';
import { Message } from '../../mail.model';
import { MailService } from '../../mail.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-mail-list-trash',
  templateUrl: './mail-list-trash.component.html',
  styleUrls: ['./mail-list-trash.component.scss']
})
export class MailListTrashComponent implements OnInit {

   img;
  @Input() message: Message;
  labels: any[];
  @HostBinding('class.selected') selected: boolean;

  routeSnap;
  onSelectedMailsChanged: Subscription;
  onLabelsChanged: Subscription;

  constructor(private mailService: MailService) { }

  ngOnInit() {

    //this.mailService.routerSnapShot = "trash";
 
        
        this.img ='assets/EP2.png';
        // Set the initial values
        this.message = new Message(this.message);

        // Subscribe to update on selected mail change
          this.onSelectedMailsChanged =
            this.mailService.onSelectedMailsChanged
                .subscribe(selectedMails => {
                    this.selected = false;

                    if ( selectedMails.length > 0 )
                    {
                        for ( const mail of selectedMails )
                        {
                            if ( mail.id === this.message.id )
                            {
                                this.selected = true;
                                break;
                            }
                        }
                    }
                });  

        // Subscribe to update on label change
        this.onLabelsChanged =
            this.mailService.onLabelsChanged
                .subscribe(labels => {
                    this.labels = labels;
                });

  }

   ngOnDestroy()
    {
        this.onSelectedMailsChanged.unsubscribe();
    }

     onSelectedChange()
    {
        this.mailService.toggleSelectedMail(this.message.id);
    }

    /**
     * Toggle star
     * @param event
     */
    toggleStar(event)
    {
        event.stopPropagation();

        this.message.toggleStar();

        this.mailService.updateMail(this.message);
    }

    /**
     * Toggle Important
     * @param event
     */
    toggleImportant(event)
    {
        event.stopPropagation();

        this.message.toggleImportant();

        this.mailService.updateMail(this.message);
    }

}
