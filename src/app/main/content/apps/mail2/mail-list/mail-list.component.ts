import { Component, OnDestroy, OnInit } from '@angular/core';
import { Message } from '../mail.model';
import { ActivatedRoute } from '@angular/router';
import { MailService } from '../mail.service';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs/Subscription';


@Component({
    selector: 'fuse-mail-list',
    templateUrl: './mail-list.component.html',
    styleUrls: ['./mail-list.component.scss']
})
export class FuseMailListComponent implements OnInit, OnDestroy {
    messages: Message[];
    currentMail: Message;
    onMailsChanged: Subscription;
    onCurrentMailChanged: Subscription;
    routeSnap;
    public show: string;

    constructor(
        private route: ActivatedRoute,
        private mailService: MailService,
        private location: Location
    ) {
    }

    ngOnInit() {
        // Subscribe to update mails on changes       
        this.show = this.mailService.routerSnapShot;
        
        this.onMailsChanged =
            this.mailService.onMailsChanged
                .subscribe(mails => {
                    
                    this.routeSnap = this.mailService.routerSnapShot
                    this.messages = mails;
                   

                });

        // Subscribe to update current mail on changes
        this.onCurrentMailChanged =
            this.mailService.onCurrentMailChanged
                .subscribe(currentMail => {
                    if (!currentMail) {
                        // Set the current mail id to null to deselect the current mail
                        this.currentMail = null;

                        // Handle the location changes
                        const labelHandle = this.route.snapshot.params.labelHandle,
                            filterHandle = this.route.snapshot.params.filterHandle,
                            folderHandle = this.route.snapshot.params.folderHandle
                        
                        if (labelHandle) {
                            this.location.go('apps/mails/label/' + labelHandle);
                        }
                        else if (filterHandle) {
                            this.location.go('apps/mails/filter/' + filterHandle);
                        }
                        else {
                            this.location.go('apps/mails/inbox');
                        }
                    }
                    else {
                        this.currentMail = currentMail;
                    }
                });
        //folderHandle = this.route.snapshot.params.folderHandle;

    }

    ngOnDestroy() {
        this.onMailsChanged.unsubscribe();
        this.onCurrentMailChanged.unsubscribe();
    }

    click() {
        var id = this.route.snapshot.params.folderHandle;
        
    }

    /**
     * Read mail
     * @param mailId
     */
    readMail(mailId) {
        
        const labelHandle = this.route.snapshot.params.labelHandle,
            filterHandle = this.route.snapshot.params.filterHandle,
            folderHandle = this.route.snapshot.params.folderHandle;

        if (labelHandle) {
            this.location.go('apps/mails/label/' + labelHandle + '/' + mailId);
        }
        else if (filterHandle) {
            this.location.go('apps/mails/filter/' + filterHandle + '/' + mailId);
        }
        else {
            this.location.go('apps/mails/inbox' + '/' + mailId);
        }
        // Set current mail
        this.mailService.setCurrentMail(mailId);
    }

    readMail2(mailId) {
        
        const labelHandle = this.route.snapshot.params.labelHandle,
            filterHandle = this.route.snapshot.params.filterHandle,
            folderHandle = this.route.snapshot.params.folderHandle;

        if (labelHandle) {
            this.location.go('apps/mails/label/' + labelHandle + '/' + mailId);
        }
        else if (filterHandle) {
            this.location.go('apps/mails/filter/' + filterHandle + '/' + mailId);
        }
        else {
            this.location.go('apps/mails/sent' + '/' + mailId);
        }

        // Set current mail
        this.mailService.setCurrentMail(mailId);
    }

}
