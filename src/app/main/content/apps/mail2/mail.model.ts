export class Message {
    id: string;
    /* from: {
        name: string,
        avatar: string,
        email: string
    }; */
    from: string;
    /*  to: {
         name: string,
         email: string
     }[]; */
    to: string;
    subject: string;
    message: string;
    time: any;
    image: any;
    read: boolean;
    starred: boolean;
    important: boolean;
    document_name:string;
    staff_id:string;
    hasAttachments: boolean;
    _attachments: any;
    attachments: {
        type: string,
        fileName: string,
        preview: string,
        url: string,
        size: string
    }[];
    labels: string[];
    folder: number;

    constructor(mail) {
        this.id = mail.id;
        this.from = mail.from;
        this.to = mail.to;
        this. _attachments = mail._attachments;
        this.subject = mail.subject;
        this.image = mail.image;
        this.document_name = mail.document_name;
        this.staff_id = mail.staff_id;
        this.message = mail.message;
        this.time = mail.time;
        this.read = mail.read;
        this.starred = mail.starred;
        this.important = mail.important;
        this.hasAttachments = mail.hasAttachments;
        this.attachments = mail.attachments;
        this.labels = mail.labels;
        this.folder = mail.folder;
    }

    toggleStar() {
        this.starred = !this.starred;
    }

    toggleImportant() {
        this.important = !this.important;
    }
}
