import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseMailComponent } from './mail.component';
import { FuseMailMainSidenavComponent } from './sidenavs/main/main-sidenav.component';
import { FuseMailListItemComponent } from './mail-list/mail-list-item/mail-list-item.component';
import { FuseMailListComponent } from './mail-list/mail-list.component';
import { FuseMailDetailsComponent } from './mail-details/mail-details.component';
import { MailService } from './mail.service';
import { FuseMailComposeDialogComponent } from './dialogs/compose/compose.component';
import { MailListSentComponent } from './mail-list/mail-list-sent/mail-list-sent.component';
import { MailListTrashComponent } from './mail-list/mail-list-trash/mail-list-trash.component';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/mails/label/:labelHandle',
        component: FuseMailComponent,
        resolve  : {
            mail: MailService
        }
    },
       {
        path     : 'apps/mails/inbox',
        component: FuseMailComponent,
        resolve  : {
            mail: MailService
        }
    },   
     {
        path     : 'apps/mails/sent',
        component: FuseMailComponent,
        resolve  : {
            mail: MailService
        }
    },   
     {
        path     : 'apps/mails/trash',
        component: FuseMailComponent,
        resolve  : {
            mail: MailService
        }
    },   
    {
        path     : 'apps/mails/label/:labelHandle/:mailId',
        component: FuseMailComponent,
        resolve  : {
            mail: MailService
        } 
    },
    {
        path     : 'apps/mails/filter/:filterHandle',
        component: FuseMailComponent,
        resolve  : {
            mail: MailService
        }
    },
    {
        path     : 'apps/mails/filter/:filterHandle/:mailId',
        component: FuseMailComponent,
        resolve  : {
            mail: MailService
        }
    },
     {
        path     : 'apps/mails/:folderHandle',
        component: FuseMailComponent,
        resolve  : {
            mail: MailService
        }
    }, 
    {
        path     : 'apps/mails/:folderHandle/:mailId',
        component: FuseMailComponent,
        resolve  : {
            mail: MailService
        }
    },
    /*  {
        path      : '**',
        redirectTo: 'apps/mails/inbox'
    } */
];

@NgModule({
    declarations   : [
        FuseMailComponent,
        FuseMailListComponent,
        FuseMailListItemComponent,
        FuseMailDetailsComponent,
        FuseMailMainSidenavComponent,
        FuseMailComposeDialogComponent,
        MailListSentComponent,
        MailListTrashComponent
    ],
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    providers      : [
        MailService,
        PouchService
    ],
    entryComponents: [FuseMailComposeDialogComponent]
})
export class FuseMailModule
{
}
