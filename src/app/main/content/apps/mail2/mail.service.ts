/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { Message } from './mail.model';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from 'app/core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Schema } from '../schema';

@Injectable()
export class MailService implements Resolve<any>
{
    public file;
    public sCredentials;
    public retrieve;
    public fileName;
    public hide = "";
    mails: Message[];
    selectedMails: Message[];
    selectedMails2: string[] = [];
    currentMail: Message;
    newMail: Message[];
    searchText = '';
    public showImage = 0;
    public imageId: any;
    folderId;
    folderId2: any;
    folders: any[];
    filters: any[];
    labels: any[];
    public routerSnapShot: string;
    public message: any;
    routeParams: any;
    filterBy: string;
    onMailsChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    onSelectedMailsChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    onCurrentMailChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    onFilterChanged: Subject<any> = new Subject();
    onFoldersChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    onFiltersChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    onLabelsChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    onEventsUpdated = new Subject<any>();
    public localStorageEmail: string;
    public localStorageItem: any;
    public localStorageType: any;
    public localStorageName: any;
    public localStorageId: any;
    emailTo;

    //onSearchTextChanged: BehaviorSubject<any> = new BehaviorSubject('');
    onSearchTextChanged: Subject<any> = new Subject();

    private db;
    remote: any;

    constructor(private http: Http) {
        this.selectedMails = [];
        this.initDB(this.sCredentials);
        this.routerSnapShot = "inbox";

    }

    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        this.routeParams = route.params;

        return new Promise((resolve, reject) => {
            Promise.all([
                /* this.getFolders(), */
                this.getFilters(),
                this.getLabels(),
                this.getMailsByFolder(),
                this.getMails()
            ]).then(
                () => {
                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getMails();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getMails();
                    });

                    if (this.routeParams.mailId) {
                        this.setCurrentMail(this.routeParams.mailId);
                    }
                    else {
                        this.setCurrentMail(null);
                    }

                    this.onSearchTextChanged.subscribe(searchText => {
                        if (searchText !== '') {
                            this.searchText = searchText;
                            this.getMails();
                        }
                        else {
                            this.searchText = searchText;
                            this.getMails();
                        }
                    });

                    resolve();
                },
                reject
                );
        });
    }

    /**
   * Initialize PouchDB database
   */
  initDB(credentials) {
    console.log(credentials);
    credentials = JSON.parse(localStorage.getItem('credentials'))

    this.sCredentials = credentials;
    this.db = new PouchDB('asueifaibayelsa', { adaptater: 'websql'});
    localStorage.setItem('credentials', JSON.stringify(credentials));
    this.db.setSchema(Schema);

    //if (credentials.online) {
        this.enableSyncing(credentials);
    //}

    this.db.createIndex({
        index: {
            fields: ['_id']
        }
    });
}

databaseExist(db): Promise<boolean> {
    return PouchDB.allDbs().then(dbs => {
        return dbs.indexOf(db) != -1;
    });
}

enableSyncing(credentials) {
    let options = {
        Auth: {
            username: 'admin',
            password: 'realpeople'
        },
        live: true,
        retry: true,
        continuous: true
    };
    //if (credentials.userDBs != undefined) {
        this.remote = 'http://157.230.219.86:5984/asueifaibayelsa';
        this.db.sync(this.remote, options).on('change', function (change) {
            console.log('changed');
        }).on('complete', function (complete) {
            console.log('complete');
        }).on('error', function (err) {
            console.log('offline');
        });;
    //}

}

loadRemoteDB(credentials) {

    //this.remote = 'http://sarutech.com:5984/hospital_demo';
    let options = {
        Auth: {
            username: 'admin',
            password: 'realpeople'
        },
        /*    live: true,
           retry: true,
           continuous: true */
    };
    //if (credentials.userDBs != undefined) {

        this.remote = 'http://157.230.219.86:5984/asueifaibayelsa';
        return this.db.sync(this.remote, options).on('change', function (change) {

            //return true;
        }).on('complete', function (complete) {
            return true;
        }).on('error', function (err) {
            console.log('offline');
        });
    //}

}


    validateUsername(username) {
        return this.http.get('http://157.230.219.86:5984/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }

    /**
     * Get all folders
     * @returns {Promise<any>}
     */
    /*   getFolders(): Promise<any> {
          return new Promise((resolve, reject) => {
              this.http.get('api/mail-folders')
                  .subscribe(response => {
                      this.folders = response.json().data;
                      this.onFoldersChanged.next(this.folders);
                      resolve(this.folders);
                  }, reject);
          });
      } */

    /**
     * Get all filters
     * @returns {Promise<any>}
     */
    getFilters(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get('api/mail-filters')
                .subscribe(response => {
                    this.filters = response.json().data;
                    
                    this.onFiltersChanged.next(this.filters);
                    resolve(this.filters);
                }, reject);
        });
    }

    /**
     * Get all labels
     * @returns {Promise<any>}
     */
    getLabels(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get('api/mail-labels')
                .subscribe(response => {
                    this.labels = response.json().data;
                    
                    this.onLabelsChanged.next(this.labels);
                    resolve(this.labels);
                }, reject);
        });
    }

    /**
     * Get all mails
     * @returns {Promise<Mail[]>}
     */
    getMails(): Promise<Message[]> {
        if (this.routeParams.labelHandle) {
            return this.getMailsByLabel(this.routeParams.labelHandle);
        }

        if (this.routeParams.filterHandle) {
            return this.getMailsByFilter(this.routeParams.filterHandle);
        }
        
        return this.getMailsByFolder();

    }

    /***********
    * message
    **********/
    /**
     * Save a message
     * @param {mail} mail
     *
     * @return Promise<message>
     */
    saveMessage(message: Message): Promise<Message> {
        message.id = Math.floor(Date.now()).toString();

        var filess;
        filess

        this.file;
        
        this.retrieve = (<HTMLInputElement>this.file).files[0];
        

        message._attachments = this.retrieve;
        if (message._attachments) {
            message.document_name = message._attachments.name;
        }
        message.staff_id = this.localStorageItem.id;
        

        return this.db.rel.save('message', message)
            .then((data: any) => {

                if (data && data.messages && data.messages
                [0]) {
                    this.db.rel.putAttachment('message', { id: message.id, }, 'file', message._attachments, 'image/png').then(res => {
                        message._attachments = res
                    })
                    
                    return data.messages[0]
                }
                //this.onMessagesChanged.next(this.messages);
                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Get mails by folder
     * @param handle
     * @returns {Promise<Message[]>}
     */
    getMailsByFolder(): Promise<Array<Message>> {
        this.localStorageItem = JSON.parse(localStorage.getItem('user'));
        this.localStorageType = this.localStorageItem.usertype;
        this.localStorageName = this.localStorageItem.name;
        this.localStorageEmail = this.localStorageItem.email;
      
        //return new Promise((reject,resolve) => {
        return this.db.rel.find('message').then((data: any) => {
            this.mails = data.messages;

            this.onEventsUpdated.next(this.mails)

            this.mails = FuseUtils.filterArrayByString(this.mails, this.searchText)

            /*   this.http.get('api/mail-folders?handle=' + handle)
                 .subscribe(folders => {
                     console.log(handle);
                     this.folderId = folders.json().data[0].id;
                     console.log(this.folderId)
                     this.http.get('api/mail-mails?folder=' + this.folderId)
                         .subscribe(mails => {
 
                             this.mails = mails.json().data.map(mail => {
                                 console.log(mails.json().data)
                                 return new Message(mail);
 
                             });
 
                             this.mails = FuseUtils.filterArrayByString(this.mails, this.searchText);
 
                             this.onMailsChanged.next(this.mails);
 
                            resolve(this.mails);
                             //console.log(this.mails)
                         });  
            //});*/
            
            if (this.routerSnapShot == "inbox") {
                var email = this.localStorageEmail;

                this.mails = this.mails.filter(data => data.to == email);
               
                this.onMailsChanged.next(this.mails);
                
                return Promise.resolve(this.mails);
            }
            else if (this.routerSnapShot == "sent") {
                var email = this.localStorageEmail;
                this.mails = this.mails.filter(data => data.from == email);
                this.onMailsChanged.next(this.mails);
                return Promise.resolve(this.mails);
            }

        }).catch((err: any) => {
            console.error(err);
        })
        // });
    }

    /**
     * Get mails by filter
     * @param handle
     * @returns {Promise<Mail[]>}
     */
    getMailsByFilter(handle): Promise<Message[]> {
        return new Promise((resolve, reject) => {

            this.http.get('api/mail-mails?' + handle + '=true')
                .subscribe(mails => {

                    this.mails = mails.json().data.map(mail => {
                        return new Message(mail);
                    });

                    this.mails = FuseUtils.filterArrayByString(this.mails, this.searchText);

                    this.onMailsChanged.next(this.mails);
                    
                    resolve(this.mails);

                }, reject);
        });
    }

    /**
     * Get mails by label
     * @param handle
     * @returns {Promise<Mail[]>}
     */
    getMailsByLabel(handle): Promise<Message[]> {
        return new Promise((resolve, reject) => {
            this.http.get('api/mail-labels?handle=' + handle)
                .subscribe(labels => {

                    const labelId = labels.json().data[0].id;

                    this.http.get('api/mail-mails?labels=' + labelId)
                        .subscribe(mails => {

                            this.mails = mails.json().data.map(mail => {
                                return new Message(mail);
                            });

                            this.mails = FuseUtils.filterArrayByString(this.mails, this.searchText);

                            this.onMailsChanged.next(this.mails);

                            resolve(this.mails);

                        }, reject);
                });
        });
    }

    /**
     * Toggle selected mail by id
     * @param id
     */
    toggleSelectedMail(id) {
        // First, check if we already have that mail as selected...
        if (this.selectedMails2.length > 0) {
            for (const mail of this.selectedMails) {
                // ...delete the selected mail
                if (mail.id === id) {
                    const index = this.selectedMails2.indexOf(id);

                    if (index !== -1) {
                        this.selectedMails2.splice(index, 1);

                        // Trigger the next event
                        this.onSelectedMailsChanged.next(this.selectedMails2);

                        // Return
                        return;
                    }
                }
            }
        }

        // If we don't have it, push as selected
        this.selectedMails2.push(id);
        /* this.mails.find(mail => {
            return mail.id === id;
        }) */

        
        // Trigger the next event
        this.onSelectedMailsChanged.next(this.selectedMails2);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedMails2.length > 0) {
            this.deselectMails();
        }
        else {
            this.selectMails();
        }

    }


    selectMails(filterParameter?, filterValue?) {
        this.selectedMails = [];

        // If there is no filter, select all mails
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedMails = this.mails;
        }
        else {
            this.selectedMails.push(...
                this.mails.filter(mail => {
                    return mail[filterParameter] === filterValue;
                })
            );
        }

        // Trigger the next event
        this.onSelectedMailsChanged.next(this.selectedMails);
    }

    deselectMails() {
        this.selectedMails2 = [];

        // Trigger the next event
        this.onSelectedMailsChanged.next(this.selectedMails2);
    }

    /**
     * Set current mail by id
     * @param id
     */
    setCurrentMail(id) {
        
        this.currentMail = this.mails.find(mail => {
            
            return mail.id === id;
        });

        this.onCurrentMailChanged.next(this.currentMail);
        
    }

    /**
     * Toggle label on selected mails
     * @param labelId
     */
    toggleLabelOnSelectedMails(labelId) {
        this.selectedMails.map(mail => {

            const index = mail.labels.indexOf(labelId);

            if (index !== -1) {
                mail.labels.splice(index, 1);
            }
            else {
                mail.labels.push(labelId);
            }

            this.updateMail(mail);
        });
    }

    /**
     * Set folder on selected mails
     * @param folderId
     */
    setFolderOnSelectedMails(folderId) {
        this.selectedMails.map(mail => {
            mail.folder = folderId;

            this.updateMail(mail);
        });

        this.deselectMails();
    }

    /**
     * Update the mail
     * @param mail
     * @returns {Promise<any>}
     */
    updateMail(message: Message) {
        return this.db.rel.save('message', message)
            .then((data: any) => {
                if (data && data.messages && data.messages
                [0]) {
                    
                    return data.messages[0]
                }
                //this.onMessagesChanged.next(this.messages);
                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getMail(id): Promise<Message> {
        return this.db.rel.find('message', id)
            .then((data: any) => {
                return data && data.messages ? data.messages[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedMails() {
        for (const mailId of this.selectedMails2) {
            const message = this.mails.find(_message => {
                
                return _message.id === mailId;
            });

            this.db.rel.del('message', message)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const mailIndex = this.mails.indexOf(message);
                    this.mails.splice(mailIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onMailsChanged.next(this.mails);
        this.deselectMails();
    }

    updateSelectedMails() {
        for (const mailId of this.selectedMails2) {
            const message = this.mails.find(_message => {
                
                return _message.id === mailId;
            });

            this.db.rel.save('message', message)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const mailIndex = this.mails.indexOf(message);
                    this.mails.splice(mailIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onMailsChanged.next(this.mails);
        this.deselectMails();
    }


}
