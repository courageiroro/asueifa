import { Component, OnDestroy, OnInit } from '@angular/core';
import { MailService } from '../../mail.service';
import { Subscription } from 'rxjs/Subscription';
import { FuseMailComposeDialogComponent } from '../../dialogs/compose/compose.component';
import { MdDialog } from '@angular/material';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
    selector: 'fuse-mail-main-sidenav',
    templateUrl: './main-sidenav.component.html',
    styleUrls: ['./main-sidenav.component.scss']
})
export class FuseMailMainSidenavComponent implements OnInit, OnDestroy {
    folders: any[];
    filters: any[];
    labels: any[];
    accounts: object;
    selectedAccount: string;
    dialogRef: any;
    array2: any[];
    finalArray: any;
    folderHandle;
    onCurrentMailChanged;
    currentMail;
    onFoldersChanged: Subscription;
    onFiltersChanged: Subscription;
    onLabelsChanged: Subscription;
    numberId;
    routerLink;
    routerLink2;
    //emailTo;
    public localStorageItem: any;
    public localStorageType: any;
    public localStorageName: any;
    public localStorageEmail: string;
    refresh: Subject<any> = new Subject();

    constructor(
        private mailService: MailService,
        public dialog: MdDialog,
        private route: ActivatedRoute,
        private location: Location,
        private router: Router
    ) {
        this.localStorageItem = JSON.parse(localStorage.getItem('user'));
        this.localStorageType = this.localStorageItem.usertype;
        this.localStorageName = this.localStorageItem.name;
        this.localStorageEmail = this.localStorageItem.email;
        // Data
        this.accounts = {
            'creapond': 'johndoe@creapond.com',
            'withinpixels': 'johndoe@withinpixels.com'
        };

        this.selectedAccount = 'creapond';
    }

    ngOnInit() {
        this.onFoldersChanged =
            this.mailService.onFoldersChanged
                .subscribe(folders => {
                    this.folders = folders;


                });

        this.onFiltersChanged =
            this.mailService.onFiltersChanged
                .subscribe(filters => {
                    this.filters = filters;
                });

        this.onLabelsChanged =
            this.mailService.onLabelsChanged
                .subscribe(labels => {
                    this.labels = labels;
                });
    }
    /*  getHandle(get) {
         var array = get.handle.join();
         //return array;
     } */


    composeDialog() {
        this.dialogRef = this.dialog.open(FuseMailComposeDialogComponent, {
            panelClass: 'mail-compose-dialog',
            data: {
                action: 'new'
            }
        });
        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Send
                     */
                    case 'send':
                        this.mailService.emailTo = formData.getRawValue().to

                        const messageData = formData.getRawValue();
                        var reader = new FileReader();
                        var file = this.mailService.file;
                        var retrieve = (<HTMLInputElement>file).files[0];
                        var file2;
                        var myThis = this;

                        if (retrieve == undefined) {

                            this.mailService.saveMessage(messageData).then(res => {
                                if (res.from != this.localStorageEmail) {
                                    this.mailService.mails.push(messageData);
                                    this.mailService.onMailsChanged.next(this.mailService.mails);
                                }

                            })
                            this.refresh.next(true);
                        }
                        else {
                            reader.readAsDataURL(retrieve);
                            reader.onloadend = function () {
                                reader.result;

                                messageData.image = reader.result;

                                myThis.mailService.saveMessage(messageData).then(res => {
                                    if (res.from != myThis.localStorageEmail) {
                                        myThis.mailService.mails.push(messageData);
                                        myThis.mailService.onMailsChanged.next(myThis.mailService.mails);
                                    }

                                })
                                myThis.refresh.next(true);
                            }
                        };
                        reader.onerror = function (error) {
                            console.log('Error: ', error);
                        };



                        break;
                    /**
                     * Delete
                     */
                    case 'delete':
                        console.log('delete Mail');
                        break;

                }
            });
    }

    ngOnDestroy() {
        this.onFoldersChanged.unsubscribe();
        this.onFiltersChanged.unsubscribe();
        this.onLabelsChanged.unsubscribe();
    }

    inbox() {
        this.mailService.routerSnapShot = "inbox";
        this.router.navigate(['/apps/mails/inbox'])

        /*  this.mailService.getFolder(id).then(res=>{
             console.log(res);
         }) */
        /*  var folderHandle2 = this.route.snapshot.params.folderHandle;
         console.log(folderHandle2); */
        /*  var array2 = [];
         var array = this.folders.map(function (obj) {
             console.log(obj.id);
             array2.push(obj.id);
             console.log(array2)
             console.log(array2.length)
 
             return obj.id
 
         });   */
        //this.mailService.folderId2 = 
    }

    sent() {

        this.mailService.routerSnapShot = "sent";
        this.router.navigate(['/apps/mails/sent']);
    }

}
