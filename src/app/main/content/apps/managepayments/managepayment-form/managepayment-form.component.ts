import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Extendlicense } from '../../extendlicenses/extendlicense.model';

@Component({
    selector: 'fuse-managepayments-managepayment-form-dialog',
    templateUrl: './managepayment-form.component.html',
    styleUrls: ['./managepayment-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseManagepaymentsManagepaymentFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    managepaymentForm: FormGroup;
    action: string;
    managepayment: Extendlicense;

    constructor(
        public dialogRef: MdDialogRef<FuseManagepaymentsManagepaymentFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Manage Payment';
            this.managepayment = data.managepayment;
        }
        else {
            this.dialogTitle = 'New Manage Payment';
            this.managepayment = {
                id: '',
                rev: '',
                amount: null,
                product: '',
                type: '',
                method: '',
                date: new Date,
                expire: new Date,
                reference: '',
                status: false
            }
        }

        this.managepaymentForm = this.createManagepaymentForm();
    }

    ngOnInit() {
    }

    createManagepaymentForm() {
        return this.formBuilder.group({
            id: [this.managepayment.id],
            rev: [this.managepayment.rev],
            amount: [{value:this.managepayment.amount,disabled:true}],
            product: [{value:this.managepayment.product,disabled:true}],
            type: [{value:this.managepayment.type,disabled:true}],
            method: [{value:this.managepayment.method,disabled:true}],
            date: [{value:this.managepayment.date,disabled:true}],
            expire: [{value:this.managepayment.expire,disabled:true}],
            reference: [{value:this.managepayment.reference,disabled:true}],
            status: [{value:this.managepayment.status,disabled:true}],
        });
    }
}
