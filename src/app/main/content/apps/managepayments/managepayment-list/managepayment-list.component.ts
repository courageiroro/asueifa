import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ManagepaymentsService } from '../managepayments.service';
import { ExtendlicensesService } from '../../extendlicenses/extendlicenses.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { FuseManagepaymentsManagepaymentFormDialogComponent } from '../managepayment-form/managepayment-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Managepayment } from '../managepayment.model';
import { Extendlicense } from '../../extendlicenses/extendlicense.model';
import { Http, Headers, RequestOptions } from '@angular/http';
import {PouchService} from '../../../../../provider/pouch-service';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';

@Component({
    selector: 'fuse-managepayments-managepayment-list',
    templateUrl: './managepayment-list.component.html',
    styleUrls: ['./managepayment-list.component.scss']
})
export class FuseManagepaymentsManagepaymentListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public managepayments: Array<Extendlicense> = [];
    managepayments2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'product', 'reference','amount','expire','method','buttons'];
    selectedManagepayments: any[];
    online = true;
    checkboxes: {};

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;


    constructor(public dialog: MdDialog, public db: PouchService, public http: Http)
    { }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        this._loadManagepayments();
    }

    private _loadManagepayments(): void {
        this.db.onExtendlicensesChanged.subscribe(data => {
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        this.db.getExtendlicenses()
            .then((managepayments: Array<Extendlicense>) => {
                
                this.managepayments = managepayments;
                this.managepayments2 = new BehaviorSubject<any>(managepayments);
                //console.log(this.managepayments2);

                this.checkboxes = {};
                managepayments.map(managepayment => {
                    this.checkboxes[managepayment.id] = false;
                });
                this.db.onSelectedExtendlicensesChanged.subscribe(selectedManagepayments => {
                    for (const id in this.checkboxes) {
                        this.checkboxes[id] = selectedManagepayments.includes(id);
                    }

                    this.selectedManagepayments = selectedManagepayments;
                });
                this.db.onSearchTextChanged.subscribe(data => {
                    this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                })
            });

    }


    onlineCheck() {
        this.online = true;
        this.http.get('https://sarutech.com/couchdblogin/auth/validate-username/xs2da2432252sda').subscribe(data => {
            this.online = true;
         
        },
            (err) => {
                this.online = false;
                
            });
    }

    click() {
        this.onlineCheck();
    }

    newManagepayment() {
        this.dialogRef = this.dialog.open(FuseManagepaymentsManagepaymentFormDialogComponent, {
            panelClass: 'managepayment-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }

                this.db.saveExtendlicense(response.getRawValue());
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                

            });

    }

    editManagepayment(managepayment) {
        
        this.dialogRef = this.dialog.open(FuseManagepaymentsManagepaymentFormDialogComponent, {
            panelClass: 'managepayment-form-dialog',
            data: {
                managepayment: managepayment,
                action: 'edit'

            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':

                        this.db.updateExtendlicense(formData.getRawValue());
                        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                        
                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteManagepayment(managepayment);

                        break;
                }
            });
    }

    /**
     * Delete managepayment
     */
    deleteManagepayment(managepayment) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deleteExtendlicense(managepayment);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(managepaymentId) {
        this.db.toggleSelectedExtendlicense(managepaymentId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    managepayments2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            of(this.db.managepayments).delay(5000),
            this.sort.mdSortChange,
            this._paginator.page,
        ];

        var result = Observable.fromPromise(this.db.getExtendlicenses());
       /*  result.subscribe(x => console.log(x), e => console.error(e));
        return result; */

        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.managepayments]));
        });
    }

    disconnect() {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
          return data;
        }
    
        return data.sort((a, b) => {
          const isAsc = this.sort.direction === 'asc';
          switch (this.sort.active) {
            case 'product': return compare(a.product, b.product, isAsc);
            case 'amount': return compare(+a.amount, +b.amount, isAsc);
            case 'id': return compare(+a.id, +b.id, isAsc);
            default: return 0;
          }
        });
      }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

