export interface Managepayment {
    id: string,
    rev: string,
    name: string,
    description: string,
    doctors: Array<string>
}

