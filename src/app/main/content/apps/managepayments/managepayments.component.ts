import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ManagepaymentsService } from './managepayments.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';


@Component({
    selector: 'fuse-managepayments',
    templateUrl: './managepayments.component.html',
    styleUrls: ['./managepayments.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [Animations.slideInTop]
})
export class FuseManagepaymentsComponent implements OnInit {
    hasSelectedManagepayments: boolean;
    searchInput: FormControl;

    constructor(private managepaymentsService: PouchService) {
        this.searchInput = new FormControl('');
       
    }

    ngOnInit() {

        this.managepaymentsService.onSelectedManagepaymentsChanged
            .subscribe(selectedManagepayments => {
                this.hasSelectedManagepayments = selectedManagepayments.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.managepaymentsService.onSearchTextChanged.next(searchText);
            });
    }

}
