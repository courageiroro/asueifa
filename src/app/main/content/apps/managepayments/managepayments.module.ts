import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseManagepaymentsComponent } from './managepayments.component';
import { ManagepaymentsService } from './managepayments.service';
//import { UserService } from '../../../../user.service';
//import { managepaymentsService2 } from './contacts.service';
import { FuseManagepaymentsManagepaymentListComponent } from './managepayment-list/managepayment-list.component';
import { FuseManagepaymentsSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseManagepaymentsManagepaymentFormDialogComponent } from './managepayment-form/managepayment-form.component';
import { ExtendlicensesService } from './../extendlicenses/extendlicenses.service';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/managepayment',
        component: FuseManagepaymentsComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            managepayments: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseManagepaymentsComponent,
        FuseManagepaymentsManagepaymentListComponent,
        FuseManagepaymentsSelectedBarComponent,
        FuseManagepaymentsManagepaymentFormDialogComponent
    ],
    providers      : [
        ManagepaymentsService,
        AuthGuard,
        PouchService,
        ExtendlicensesService
       //UserService
    ],
    entryComponents: [FuseManagepaymentsManagepaymentFormDialogComponent]
})
export class FuseManagepaymentsModule
{
}
