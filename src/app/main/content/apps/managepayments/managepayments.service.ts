/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Managepayment } from './managepayment.model';
import { Schema } from '../schema';

@Injectable()
export class ManagepaymentsService {
    public managepayment = "";
    public sCredentials;
    onManagepaymentsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedManagepaymentsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    managepayments: Managepayment[];
    user: any;
    selectedManagepayments: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The managepayments App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getManagepayments()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getManagepayments();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getManagepayments();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('asueifai', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/asueifai';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/asueifai';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }


    /***********
     * managepayment
     **********/
    /**
     * Save a managepayment
     * @param {managepayment} managepayment
     *
     * @return Promise<managepayment>
     */
    saveManagepayment(managepayment: Managepayment): Promise<Managepayment> {
        managepayment.id = Math.floor(Date.now()).toString();

        //managepayment.doctors = [];
        return this.db.rel.save('managepayment', managepayment)
            .then((data: any) => {

                if (data && data.managepayments && data.managepayments
                [0]) {
                    console.log('save');
                    
                    return data.managepayments[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a managepayment
    * @param {managepayment} managepayment
    *
    * @return Promise<managepayment>
    */
    updateManagepayment(managepayment: Managepayment) {
        return this.db.rel.save('managepayment', managepayment)
            .then((data: any) => {
                if (data && data.managepayments && data.managepayments
                [0]) {
                    console.log('Update')
                    
                    return data.managepayments[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a managepayment
     * @param {managepayment} managepayment
     *
     * @return Promise<boolean>
     */
    removeManagepayment(managepayment: Managepayment): Promise<boolean> {
        return this.db.rel.del('managepayment', managepayment)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the managepayments
     *
     * @return Promise<Array<managepayment>>
     */
    getManagepayments(): Promise<Array<Managepayment>> {
        return this.db.rel.find('managepayment')
            .then((data: any) => {
                this.managepayments = data.managepayments;
                if (this.searchText && this.searchText !== '') {
                    this.managepayments = FuseUtils.filterArrayByString(this.managepayments, this.searchText);
                }
                //this.onmanagepaymentsChanged.next(this.managepayments);
                return Promise.resolve(this.managepayments);
                //return data.managepayments ? data.managepayments : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getDoctorManagepayments(): Promise<Array<Managepayment>> {
        return this.db.rel.find('managepayment')
            .then((data: any) => {
                return data.managepayments ? data.managepayments : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a managepayment
     * @param {managepayment} managepayment
     *
     * @return Promise<managepayment>
     */
    getManagepayment(managepayment: Managepayment): Promise<Managepayment> {
        return this.db.rel.find('managepayment', managepayment.id)
            .then((data: any) => {
                console.log("Get")
                
                return data && data.managepayments ? data.managepayments[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected managepayment by id
     * @param id
     */
    toggleSelectedManagepayment(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedManagepayments.length > 0) {
            const index = this.selectedManagepayments.indexOf(id);

            if (index !== -1) {
                this.selectedManagepayments.splice(index, 1);

                // Trigger the next event
                this.onSelectedManagepaymentsChanged.next(this.selectedManagepayments);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedManagepayments.push(id);

        // Trigger the next event
        this.onSelectedManagepaymentsChanged.next(this.selectedManagepayments);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedManagepayments.length > 0) {
            this.deselectManagepayments();
        }
        else {
            this.selectManagepayments();
        }
    }

    selectManagepayments(filterParameter?, filterValue?) {
        this.selectedManagepayments = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedManagepayments = [];
            this.managepayments.map(managepayment => {
                this.selectedManagepayments.push(managepayment.id);
            });
        }
        else {
            /* this.selectedmanagepayments.push(...
                 this.managepayments.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedManagepaymentsChanged.next(this.selectedManagepayments);
    }





    deselectManagepayments() {
        this.selectedManagepayments = [];

        // Trigger the next event
        this.onSelectedManagepaymentsChanged.next(this.selectedManagepayments);
    }

    deleteManagepayment(managepayment) {
        this.db.rel.del('managepayment', managepayment)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const managepaymentIndex = this.managepayments.indexOf(managepayment);
                this.managepayments.splice(managepaymentIndex, 1);
                this.onManagepaymentsChanged.next(this.managepayments);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedManagepayments() {
        for (const managepaymentId of this.selectedManagepayments) {
            const managepayment = this.managepayments.find(_managepayment => {
                return _managepayment.id === managepaymentId;
            });

            this.db.rel.del('managepayment', managepayment)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const managepaymentIndex = this.managepayments.indexOf(managepayment);
                    this.managepayments.splice(managepaymentIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onManagepaymentsChanged.next(this.managepayments);
        this.deselectManagepayments();
    }
}
