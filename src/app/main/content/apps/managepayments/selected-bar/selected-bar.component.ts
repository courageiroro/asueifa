import { Component, OnInit } from '@angular/core';
import { ManagepaymentsService } from '../managepayments.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseManagepaymentsSelectedBarComponent implements OnInit
{
    selectedManagepayments: string[];
    hasSelectedManagepayments: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private managepaymentsService: PouchService,
        public dialog: MdDialog
    )
    {
        this.managepaymentsService.onSelectedManagepaymentsChanged
            .subscribe(selectedManagepayments => {
                this.selectedManagepayments = selectedManagepayments;
                setTimeout(() => {
                    this.hasSelectedManagepayments = selectedManagepayments.length > 0;
                    this.isIndeterminate = (selectedManagepayments.length !== this.managepaymentsService.managepayments.length && selectedManagepayments.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.managepaymentsService.selectManagepayments();
    }

    deselectAll()
    {
        this.managepaymentsService.deselectManagepayments();
    }

    deleteSelectedManagepayments()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected managepayments?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.managepaymentsService.deleteSelectedManagepayments();
            }
            this.confirmDialogRef = null;
        });
    }

}
