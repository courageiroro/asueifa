import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Medicine_category } from '../medicine_category.model';

@Component({
    selector: 'fuse-medicine_categorys-medicine_category-form-dialog',
    templateUrl: './medicine_category-form.component.html',
    styleUrls: ['./medicine_category-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseMedicine_categorysMedicine_categoryFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    medicine_categoryForm: FormGroup;
    action: string;
    medicine_category: Medicine_category;

    constructor(
        public dialogRef: MdDialogRef<FuseMedicine_categorysMedicine_categoryFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Medicine Category';
            this.medicine_category = data.medicine_category;
        }
        else {
            this.dialogTitle = 'New Medicine Category';
            this.medicine_category = {
                id: '',
                rev: '',
                name: '',
                description: '',
                medicines: []
            }
        }

        this.medicine_categoryForm = this.createMedicine_categoryForm();
    }

    ngOnInit() {
    }

    createMedicine_categoryForm() {
        return this.formBuilder.group({
            id: [this.medicine_category.id],
            rev: [this.medicine_category.rev],
            name: [this.medicine_category.name],
            description: [this.medicine_category.description],

        });
    }
}
