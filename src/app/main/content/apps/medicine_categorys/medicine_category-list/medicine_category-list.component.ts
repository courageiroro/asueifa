import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Medicine_categorysService } from '../medicine_categorys.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject} from 'rxjs/Rx';
import { FuseMedicine_categorysMedicine_categoryFormDialogComponent } from '../medicine_category-form/medicine_category-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Medicine_category } from '../medicine_category.model';
import {PouchService} from '../../../../../provider/pouch-service';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';

@Component({
    selector   : 'fuse-medicine_categorys-medicine_category-list',
    templateUrl: './medicine_category-list.component.html',
    styleUrls  : ['./medicine_category-list.component.scss']
})
export class FuseMedicine_categorysMedicine_categoryListComponent implements OnInit
{
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public medicine_categorys: Array<Medicine_category> = [];
    medicine_categorys2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'name','description','buttons'];
    selectedMedicine_categorys: any[];
    checkboxes: {};

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;

    constructor(public dialog: MdDialog, public db:PouchService)
    { }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        this._loadMedicine_categorys();
    }

    private _loadMedicine_categorys(): void {
         this.db.onMedicine_categorysChanged.subscribe(data =>{
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        this.db.getMedicine_categorys()
        .then((medicine_categorys: Array<Medicine_category>) => {
            this.medicine_categorys = medicine_categorys;
            this.medicine_categorys2= new BehaviorSubject<any>(medicine_categorys);
            //console.log(this.medicine_categorys2);

            this.checkboxes = {};
            medicine_categorys.map(medicine_category => {
                this.checkboxes[medicine_category.id] = false;
            });
             this.db.onSelectedMedicine_categorysChanged.subscribe(selectedMedicine_categorys => {
                for ( const id in this.checkboxes )
                {
                    this.checkboxes[id] = selectedMedicine_categorys.includes(id);
                }

                this.selectedMedicine_categorys = selectedMedicine_categorys;
            });
        this.db.onSearchTextChanged.subscribe(data =>{
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        });
        
    }

    newMedicine_category()
    {
        this.dialogRef = this.dialog.open(FuseMedicine_categorysMedicine_categoryFormDialogComponent, {
            panelClass: 'medicine_category-form-dialog',
            data      : {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if ( !response )
                {
                    return;
                }

                this.db.saveMedicine_category(response.getRawValue());
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

            });

    }

    editMedicine_category(medicine_category)
    {
        this.dialogRef = this.dialog.open(FuseMedicine_categorysMedicine_categoryFormDialogComponent, {
            panelClass: 'medicine_category-form-dialog',
            data      : {
                medicine_category: medicine_category,
                action : 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch ( actionType )
                {
                    /**
                     * Save
                     */
                    case 'save':

                        this.db.updateMedicine_category(formData.getRawValue());
                        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteMedicine_category(medicine_category);

                        break;
                }
            });
    }

    /**
     * Delete medicine_category
     */
    deleteMedicine_category(medicine_category)
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.db.deleteMedicine_category(medicine_category);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(medicine_categoryId)
    {
        this.db.toggleSelectedMedicine_category(medicine_categoryId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    medicine_categorys2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort)
    {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]>
    {
        const displayDataChanges = [
            of(this.db.medicine_categorys).delay(5000),
            this.sort.mdSortChange,
            this._paginator.page,
        ];

        var result = Observable.fromPromise(this.db.getMedicine_categorys());
        /* result.subscribe(x => console.log(x), e => console.error(e));
        return result;   */  

        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.medicine_categorys]));
        });
    }

    disconnect()
    {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
          return data;
        }
    
        return data.sort((a, b) => {
          const isAsc = this.sort.direction === 'asc';
          switch (this.sort.active) {
            case 'name': return compare(a.name, b.name, isAsc);
            case 'amount': return compare(+a.amount, +b.amount, isAsc);
            case 'id': return compare(+a.id, +b.id, isAsc);
            default: return 0;
          }
        });
      }
      
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
