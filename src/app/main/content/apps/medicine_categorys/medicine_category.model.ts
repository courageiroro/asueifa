export interface Medicine_category {
    id: string,
    rev: string,
    name: string,
    description: string,
    medicines: Array<string>
}

