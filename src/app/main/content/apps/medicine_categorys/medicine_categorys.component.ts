import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Medicine_categorysService } from './medicine_categorys.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-medicine_categorys',
    templateUrl  : './medicine_categorys.component.html',
    styleUrls    : ['./medicine_categorys.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FuseMedicine_categorysComponent implements OnInit
{
    hasSelectedMedicine_categorys: boolean;
    searchInput: FormControl;

    constructor(private medicine_categorysService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.medicine_categorysService.onSelectedMedicine_categorysChanged
            .subscribe(selectedMedicine_categorys => {
                this.hasSelectedMedicine_categorys = selectedMedicine_categorys.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.medicine_categorysService.onSearchTextChanged.next(searchText);
            });
    }

}
