import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseMedicine_categorysComponent } from './medicine_categorys.component';
import { Medicine_categorysService } from './medicine_categorys.service';
import { FuseMedicine_categorysMedicine_categoryListComponent } from './medicine_category-list/medicine_category-list.component';
import { FuseMedicine_categorysSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseMedicine_categorysMedicine_categoryFormDialogComponent } from './medicine_category-form/medicine_category-form.component';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/medicine_categorys',
        component: FuseMedicine_categorysComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            medicine_categorys: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseMedicine_categorysComponent,
        FuseMedicine_categorysMedicine_categoryListComponent,
        FuseMedicine_categorysSelectedBarComponent,
        FuseMedicine_categorysMedicine_categoryFormDialogComponent
    ],
    providers      : [
        Medicine_categorysService,
        PouchService,
        AuthGuard
    ],
    entryComponents: [FuseMedicine_categorysMedicine_categoryFormDialogComponent]
})
export class FuseMedicine_categorysModule
{
}
