/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Medicine_category } from './medicine_category.model';
import { Schema } from '../schema';

@Injectable()
export class Medicine_categorysService {
    public medicine_categoryname = "";
    public sCredentials;
    onMedicine_categorysChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedMedicine_categorysChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    medicine_categorys: Medicine_category[];
    user: any;
    selectedMedicine_categorys: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The medicine_categorys App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getMedicine_categorys()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getMedicine_categorys();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getMedicine_categorys();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('asueifai', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/asueifai';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/asueifai';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }


    /***********
     * medicine_category
     **********/
    /**
     * Save a medicine_category
     * @param {medicine_category} medicine_category
     *
     * @return Promise<medicine_category>
     */
    saveMedicine_category(medicine_category: Medicine_category): Promise<Medicine_category> {
        medicine_category.id = Math.floor(Date.now()).toString();

        medicine_category.medicines = [];
        return this.db.rel.save('medicine_category', medicine_category)
            .then((data: any) => {

                if (data && data.medicine_categorys && data.medicine_categorys
                [0]) {
                    return data.medicine_categorys[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a medicine_category
    * @param {medicine_category} medicine_category
    *
    * @return Promise<medicine_category>
    */
    updateMedicine_category(medicine_category: Medicine_category) {

        return this.db.rel.save('medicine_category', medicine_category)
            .then((data: any) => {
                if (data && data.medicine_categorys && data.medicine_categorys
                [0]) {
                    return data.medicine_categorys[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a medicine_category
     * @param {medicine_category} medicine_category
     *
     * @return Promise<boolean>
     */
    removeMedicine_category(medicine_category: Medicine_category): Promise<boolean> {
        return this.db.rel.del('medicine_category', medicine_category)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the medicine_categorys
     *
     * @return Promise<Array<medicine_category>>
     */
    getMedicine_categorys(): Promise<Array<Medicine_category>> {
        return this.db.rel.find('medicine_category')
            .then((data: any) => {
                this.medicine_categorys = data.medicine_categorys;
                if (this.searchText && this.searchText !== '') {
                    this.medicine_categorys = FuseUtils.filterArrayByString(this.medicine_categorys, this.searchText);
                }
                //this.onmedicine_categorysChanged.next(this.medicine_categorys);
                return Promise.resolve(this.medicine_categorys);
                //return data.medicine_categorys ? data.medicine_categorys : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getMedMedicine_categorys(): Promise<Array<Medicine_category>> {
        return this.db.rel.find('medicine_category')
            .then((data: any) => {
                return data.medicine_categorys ? data.medicine_categorys : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a medicine_category
     * @param {medicine_category} medicine_category
     *
     * @return Promise<medicine_category>
     */
    getMedicine_category(medicine_category: Medicine_category): Promise<Medicine_category> {
        return this.db.rel.find('medicine_category', medicine_category.id)
            .then((data: any) => {
                return data && data.medicine_categorys ? data.medicine_categorys[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected medicine_category by id
     * @param id
     */
    toggleSelectedMedicine_category(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedMedicine_categorys.length > 0) {
            const index = this.selectedMedicine_categorys.indexOf(id);

            if (index !== -1) {
                this.selectedMedicine_categorys.splice(index, 1);

                // Trigger the next event
                this.onSelectedMedicine_categorysChanged.next(this.selectedMedicine_categorys);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedMedicine_categorys.push(id);

        // Trigger the next event
        this.onSelectedMedicine_categorysChanged.next(this.selectedMedicine_categorys);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedMedicine_categorys.length > 0) {
            this.deselectMedicine_categorys();
        }
        else {
            this.selectMedicine_categorys();
        }
    }

    selectMedicine_categorys(filterParameter?, filterValue?) {
        this.selectedMedicine_categorys = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedMedicine_categorys = [];
            this.medicine_categorys.map(medicine_category => {
                this.selectedMedicine_categorys.push(medicine_category.id);
            });
        }
        else {
            /* this.selectedmedicine_categorys.push(...
                 this.medicine_categorys.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedMedicine_categorysChanged.next(this.selectedMedicine_categorys);
    }





    deselectMedicine_categorys() {
        this.selectedMedicine_categorys = [];

        // Trigger the next event
        this.onSelectedMedicine_categorysChanged.next(this.selectedMedicine_categorys);
    }

    deleteMedicine_category(medicine_category) {
        this.db.rel.del('medicine_category', medicine_category)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const medicine_categoryIndex = this.medicine_categorys.indexOf(medicine_category);
                this.medicine_categorys.splice(medicine_categoryIndex, 1);
                this.onMedicine_categorysChanged.next(this.medicine_categorys);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedMedicine_categorys() {
        for (const medicine_categoryId of this.selectedMedicine_categorys) {
            const medicine_category = this.medicine_categorys.find(_medicine_category => {
                return _medicine_category.id === medicine_categoryId;
            });

            this.db.rel.del('medicine_category', medicine_category)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const medicine_categoryIndex = this.medicine_categorys.indexOf(medicine_category);
                    this.medicine_categorys.splice(medicine_categoryIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onMedicine_categorysChanged.next(this.medicine_categorys);
        this.deselectMedicine_categorys();
    }
}
