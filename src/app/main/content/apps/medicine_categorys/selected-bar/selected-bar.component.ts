import { Component, OnInit } from '@angular/core';
import { Medicine_categorysService } from '../medicine_categorys.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseMedicine_categorysSelectedBarComponent implements OnInit
{
    selectedMedicine_categorys: string[];
    hasSelectedMedicine_categorys: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private medicine_categorysService: PouchService,
        public dialog: MdDialog
    )
    {
        this.medicine_categorysService.onSelectedMedicine_categorysChanged
            .subscribe(selectedMedicine_categorys => {
                this.selectedMedicine_categorys = selectedMedicine_categorys;
                setTimeout(() => {
                    this.hasSelectedMedicine_categorys = selectedMedicine_categorys.length > 0;
                    this.isIndeterminate = (selectedMedicine_categorys.length !== this.medicine_categorysService.medicine_categorys.length && selectedMedicine_categorys.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.medicine_categorysService.selectMedicine_categorys();
    }

    deselectAll()
    {
        this.medicine_categorysService.deselectMedicine_categorys();
    }

    deleteSelectedMedicine_categorys()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected medicine_categoryes?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.medicine_categorysService.deleteSelectedMedicine_categorys();
            }
            this.confirmDialogRef = null;
        });
    }

}
