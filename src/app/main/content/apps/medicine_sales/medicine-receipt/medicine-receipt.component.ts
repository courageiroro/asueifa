import { Component, Inject, OnInit, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Medicine_salesService } from '../medicine_sales.service';
import { StaffsService } from '../../staffs/staffs.service';
import { SettingsService } from '../../settings/settings.service';
import { CurrencysService } from '../../currencys/currencys.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-medicine_sale-receipts-medicine_sale-receipt-form-dialog',
    templateUrl: './medicine-receipt.component.html',
    styleUrls: ['./medicine-receipt.component.scss'],
    encapsulation: ViewEncapsulation.None,
    outputs: ['childEvent']
})

export class FuseMedicine_salereceiptsMedicine_salereceiptFormDialogComponent implements OnInit {

    event: CalendarEvent;
    dialogTitle: string;
    medicine_salereceiptForm: FormGroup;
    childEvent = new EventEmitter<string>();
    action: string;
    public settings;
    public result;
    public final;
    public hospitalName;
    public result2;
    public final2;
    public hospitalAddress;
    public result3;
    public final3;
    public hospitalPhone;
    public result4;
    public final4;
    public hospitalEmail;
    public Pid;
    public medicine_saleDate: any;
    public medicine_saleMonth: any;
    public medicine_saleNumber: any;
    public medicine_saleDueDate: any;
    public staffName: any;
    public basicSalary: any;
    public userType: any;
    public allowancess;
    public deductionss;
    public staffId;
    public staffAddress;
    public staffNumber;
    public staffEmail;
    public subTotal;
    public vat;
    public discount;
    public total;
    public currencys;
    public result5;
    public final5;
    public patientName: any;
    public patientId: any;
    public patientAddress: any;
    public patientNumber: any;
    public pharmName: any;
    public pharmEmail: any;
    public saleDate;
    public medicineId: any;
    public quantityss: any;
    public patientEmail;
    public totalAmount;
    public displayCurrencys;
    localStorageItem;


    constructor(
        public dialogRef: MdDialogRef<FuseMedicine_salereceiptsMedicine_salereceiptFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        public db: PouchService,
        public router: Router,
        public activateRoute: ActivatedRoute
    ) {
        this.Pid = data.pid
        this.dialogTitle = 'Medicine Sale Receipt';
        /* 
                if (this.action === 'edit') {
                    this.dialogTitle = 'Edit medicine_sale-receipt';
                    this.medicine_sale-receipt = data.medicine_sale-receipt;
                }
                else {
                    this.dialogTitle = 'New medicine_sale-receipt';
                    this.medicine_sale-receipt = {
                        id: '',
                        rev: '',
                        name: '',
                        description: '',
                        doctors: []
                    } */


        //this.medicine_sale-receiptForm = this.createmedicine_sale-receiptForm();
    }

    getArray(gets) {
        var mine = [gets.name].join()
        return mine

    }
    getAddress(gets) {
        var mine = [gets.address].join()
        return mine

    }
    getPhone(gets) {
        var mine = [gets.phone].join()
        return mine

    }
    getEmail(gets) {
        var mine = [gets.email].join()
        return mine

    }

    getCurrency(gets) {
        var mine = [gets.currency_symbol].join()
        return mine

    }

    getAmount(gets) {
        var mine = [gets.amount].join()
        return mine

    }

    ngOnInit() {

        this.localStorageItem = JSON.parse(localStorage.getItem('user'))

        this.db.getMedicine_sale(this.Pid).then(res => {
            
            //this.diagnosis_reportDate = new Date(res.creation_timestamp).toDateString()
            this.patientName = res.patient_id;
            this.patientId = res.patient_name;
            this.pharmName = this.localStorageItem.name;
            this.pharmEmail = this.localStorageItem.email;
            this.saleDate = new Date().toDateString();
            this.medicineId = res.medicines;
            this.totalAmount = res.total_amount;
            this.quantityss = res.quantityss;
            
            /*  this.patientEmail = res.pemail;
             this.unitss = res.amountss;
             this.subTotal = res.subtotal;
             this.vat = res.vat_percentage;
             this.discount = res.discount_amount;
             this.total = res.grandtotal; */
            //console.log(this.arrayAmount)

            this.db.getStaff(this.patientId).then(res => {
                
                this.patientAddress = res.address;
                this.patientEmail = res.email;
                this.patientNumber = res.phone;
            })

            this.db.getCurrencys().then(res => {
                this.currencys = res;
                this.result5 = this.currencys.map(this.getCurrency)
                this.final5 = this.result5.length - 1;
                this.displayCurrencys = this.result5[this.final5]
                
                /*  this.arrayAmount = this.unitss.map(this.getAmount)
                 console.log(this.arrayAmount.join())
                 for (this.i = 0; this.i <= this.arrayAmount.length-1; this.i++) {
                     this.formatted = "\n" + (new CurrencyPipe('en-US')).transform(this.arrayAmount[this.i], this.displayCurrencys.trim(), true)+ "<br>";
                     console.log(this.formatted)
                     this.newArray = [];
                     this.newArray.push(this.formatted.toString());
                     console.log(this.newArray);
                 } */
            })


        })


        this.db.getSettings().then(res => {
            this.settings = res;
            this.result = this.settings.map(this.getArray)
            this.final = this.result.length - 1;
            this.hospitalName = this.result[this.final]

            this.result2 = this.settings.map(this.getAddress)
            this.final2 = this.result2.length - 1;
            this.hospitalAddress = this.result2[this.final2]

            this.result3 = this.settings.map(this.getPhone)
            this.final3 = this.result3.length - 1;
            this.hospitalPhone = this.result3[this.final3]

            this.result4 = this.settings.map(this.getEmail)
            this.final4 = this.result4.length - 1;
            this.hospitalEmail = this.result4[this.final4]
        })


    }

    print() {

        window.print();

    }

    createmedicine_salereceiptForm() {
        /*  return this.formBuilder.group({
             id: [this.medicine_sale-receipt.id],
             rev: [this.medicine_sale-receipt.rev],
             name: [this.medicine_sale-receipt.name],
             description: [this.medicine_sale-receipt.description],
 
         });*/
    }
}
