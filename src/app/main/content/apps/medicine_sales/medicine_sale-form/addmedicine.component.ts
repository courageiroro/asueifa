import { Component, Inject, Input, OnInit, EventEmitter } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { Validators, FormGroup, FormArray, FormBuilder } from '@angular/forms';
import 'rxjs/Rx';
import { Medicine_sale } from '../medicine_sale.model';
import { AddMedicine } from './addmedicine.model';
import { FuseMedicine_salesMedicine_saleFormDialogComponent } from './medicine_sale-form.component'
import { MedicinesService } from '../../medicines/medicines.service';
import { Medicine_salesService } from '../medicine_sales.service';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'addmedicine',
    templateUrl: './addmedicine.component.html',
    //styleUrls: ['./invoice-form.component.scss'],
    outputs: ['passInput','passInput2','passInput3','passInput4'],

})

export class AddMedicineComponent implements OnInit {
    public add;
    dialogTitle: string;
    medicine_saleForm: FormGroup;
    action: string;
    medicine_sale: Medicine_sale;
    addMedicine: AddMedicine;
    mediciness: any;
    public medId: string;
    public mednum: string;
    statuss: any;
    passInput = new EventEmitter<number>();
    passInput2 = new EventEmitter<number>();
    passInput3 = new EventEmitter<string>();
    passInput4 = new EventEmitter<string>();
    @Input('group')
    public addMedicine_form: FormGroup;



    constructor(
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        public db: PouchService,
        
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Medicine Sale';
            this.addMedicine = data.addmedicine;
            
        }
        else {
            this.dialogTitle = 'New Medicine Sale';
            this.addMedicine = {
                id: '',
                rev: '',
                quantity: null,
                medicine: '',
                price: null,
                medicine_id:''
            }
            this.addMedicine_form = this.createMedicine_saleForm();
        }

        db.getMedicines().then(res => {
            this.mediciness = res;
        })

    }

    getMId(medicine) {
        this.db.medicinename = medicine.price;
        this.medId = medicine.price;
        
        this.db.medicineId = medicine.id;
        this.mednum = medicine.id;
        
    }

    ngOnInit() {

    }

    onChange(value: number) {
        this.passInput.emit(value);
    }

     onChange2(value: number) {
        this.passInput2.emit(value);
    }

    onChange3(value: string) {
        this.passInput3.emit(value);
    }
     onChange4(value: string) {
        this.passInput4.emit(value);
    }

    createMedicine_saleForm() {
        return this.formBuilder.group({
            id: [this.addMedicine.id],
            rev: [this.addMedicine.rev],
            quantity: [this.addMedicine.quantity],
            medicine: [this.addMedicine.medicine],
            medicine_id: [this.addMedicine.medicine_id],
            price: [this.addMedicine.price]
        });
    }
}