export interface AddMedicine {
    id: string,
    rev: string,
    quantity: number,
    medicine: string,
    price: number,
    medicine_id: string
}
