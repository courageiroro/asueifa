import { Component, Inject, OnInit, ViewEncapsulation, ViewChild, ComponentFactoryResolver, ElementRef, ViewContainerRef } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { BehaviorSubject } from 'rxjs/Rx';
import { Validators, FormGroup, FormArray, FormBuilder, FormControl } from '@angular/forms';
import 'rxjs/Rx';
import { Medicine_sale } from '../medicine_sale.model';
import { AddMedicineComponent } from './addmedicine.component'
import { MedicinesService } from '../../medicines/medicines.service';
import { PatientsService } from '../../patients/patients.service';
import { Medicine } from '../../medicines/medicine.model';
import { AddMedicine } from './addmedicine.model';
import { StaffsService } from '../../staffs/staffs.service';
import { Medicine_salesService } from '../medicine_sales.service';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-medicine_sales-medicine_sale-form-dialog',
    templateUrl: './medicine_sale-form.component.html',
    styleUrls: ['./medicine_sale-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseMedicine_salesMedicine_saleFormDialogComponent implements OnInit {

    @ViewChild('parent', { read: ViewContainerRef }) container: ViewContainerRef;
    @ViewChild('get') input: ElementRef;

    public medicinesss: Array<Medicine> = [];
    medicine: any;
    addmedicine: AddMedicine;
    medicines2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    event: CalendarEvent;
    dialogTitle: string;
    medicine_saleForm: FormGroup;
    action: string;
    public control;
    public childData: number;
    public childData2: string;
    public childData3: string;
    public textData = 0;
    public gets;
    public gets2;
    public trigger: string;
    patientss: any;
    mediciness: any;
    medicine_sale: Medicine_sale;
    public medId: number;
    public result;
    public result2;
    public result3;
    public patientId: string;
    public price2;
    public final;
    public final2;
    public final3;
    public totalPrice;
    public getP;

    constructor(
        public dialogRef: MdDialogRef<FuseMedicine_salesMedicine_saleFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        private factoryResolve: ComponentFactoryResolver,
        public db: PouchService,
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Medicine Sale';
            this.medicine_sale = data.medicine_sale;
            this.addmedicine = data.addmedicine;
            
        }
        else {
            this.dialogTitle = 'New Medicine Sale';
            this.medicine_sale = {
                id: '',
                rev: '',
                patient_id: '',
                medicines: '',
                total_amount: '',
                medicine_number: '',
                date: new Date(),
                patient_name: '',
                quantitys: null,
                quantityss: [],
                insurancename: '',
                insurancepercent: 0
            }
        }

        this.medicine_saleForm = this.createMedicine_saleForm();

        db.getPStaffs().then(res => {
            this.patientss = res;
        })

        db.getMedicines().then(res => {
            this.mediciness = res;
        })
    }

    ngOnInit() {
    }

    ngAfterViewInit() {

    }

    getPId(patient) {
        
        this.db.patientname = patient.id;
        this.patientId = patient.id;
        
        this.medicine_sale.insurancename = patient.insurancename;
        this.medicine_sale.insurancepercent = patient.insurancepercent;
    }


    initQuantity() {
        // initialize our Quantity
        if (this.addmedicine == undefined) {
            return this.formBuilder.group({
                quantity: [''],
                medicine: [''],
                price: [''],
                medicine_id: [''],
            });
        }
        else {
            
            return this.formBuilder.group({
                quantity: [this.addmedicine.quantity],
                medicine: [this.addmedicine.medicine],
                price: [this.addmedicine.price],
                medicine_id: [this.addmedicine.medicine_id],
            });
        }

    }

    getArray(gets) {
        var mine = Number([gets.quantity].join())
        
        return mine

    }

    getArray2(gets2) {
        var mine2 = [gets2.medicine].join()
        
        return mine2

    }

    getArray3(gets3) {
        var mine2 = [gets3.medicine_id].join()
        
        return mine2

    }

    getSum(total, num) {
        return total + num;
    }

    getSum2(total, num) {
        return total + num;
    }

    getMId(medicine) {
        this.db.medicinename = medicine.price;
        this.medId = medicine.price;
        
    }

    addQuantity() {
        // add quantity and entry to the list
        this.control = <FormArray>this.medicine_saleForm.controls['quantityss'];
        this.control.push(this.initQuantity());
        this.trigger = "show";
        
    }

    removeQuantity(i: number) {
        // remove amount and entry from the list
        const control = <FormArray>this.medicine_saleForm.controls['quantityss'];
        control.removeAt(i);
    }


    calculatePrice() {
        this.trigger = "show";
        this.control = <FormArray>this.medicine_saleForm.controls['quantityss'];
        var array = this.control.value;
        var array2 = this.control.value;
        var array3 = this.control.value;
        this.result = array.map(this.getArray);
        this.result2 = array2.map(this.getArray2);
        this.result3 = array3.map(this.getArray3);
        this.final = this.result.reduce(this.getSum);
        this.final2 = this.result2.join();
        this.final3 = this.result3.join();
        
        this.totalPrice = this.final * this.childData * (this.medicine_sale.insurancepercent/100);
        this.totalPrice = this.final * this.childData - this.totalPrice;
    }

    calculatePrice2() {
        this.final = this.textData;
        this.totalPrice = this.final * this.childData * (this.medicine_sale.insurancepercent/100);
        this.totalPrice = this.final * this.childData - this.totalPrice;
        
        this.final2 = this.childData2;
        this.final3 = this.childData3;
       
    }


    createMedicine_saleForm() {
        return this.formBuilder.group({
            id: [this.medicine_sale.id],
            rev: [this.medicine_sale.rev],
            patient_id: [this.medicine_sale.patient_id],
            patient_name: [this.medicine_sale.patient_name],
            medicines: [this.medicine_sale.medicines],
            quantitys: [this.medicine_sale.quantitys],
            date: [this.medicine_sale.date],
            quantityss: this.formBuilder.array([
                this.initQuantity()
            ]),
            insurancename: [{
                value: this.medicine_sale.insurancename,
                disabled: true
            }],
            insurancepercent: [{
                value: this.medicine_sale.insurancepercent,
                disabled: true
            }],
            total_amount: [{
                value: this.medicine_sale.total_amount,
                disabled: true
            }],
            medicine_number: [this.medicine_sale.medicine_number]
        });
    }
}
