import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Medicine_salesService } from '../medicine_sales.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { FuseMedicine_salesMedicine_saleFormDialogComponent } from '../medicine_sale-form/medicine_sale-form.component';
import { AddMedicineComponent } from '../medicine_sale-form/addmedicine.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Medicine_sale } from '../medicine_sale.model';
import { AddMedicine } from '../medicine_sale-form/addmedicine.model';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { MedicinesService } from '../../medicines/medicines.service';
import {PouchService} from '../../../../../provider/pouch-service';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';

@Component({
    selector: 'fuse-medicine_sales-medicine_sale-list',
    templateUrl: './medicine_sale-list.component.html',
    styleUrls: ['./medicine_sale-list.component.scss']
})
export class FuseMedicine_salesMedicine_saleListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public medicine_sales: Array<Medicine_sale> = [];
    public addmedicines: Array<AddMedicine> = [];
    medicine_sales2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'medicine', 'price', 'patient', 'date', 'buttons'];
    selectedMedicine_sales: any[];
    checkboxes: {};
    public ans2;
    public formEntry;
    public i;
    public number;

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;

    constructor(public dialog: MdDialog, public db: PouchService, public router: Router)
    { }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        this._loadMedicine_sales();
    }

    private _loadMedicine_sales(): void {
        this.db.onMedicine_salesChanged.subscribe(data => {
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        this.db.getMedicine_sales()
            .then((medicine_sales: Array<Medicine_sale>) => {
                this.medicine_sales = medicine_sales;
                this.medicine_sales2 = new BehaviorSubject<any>(medicine_sales);

                this.checkboxes = {};
                medicine_sales.map(medicine_sale => {
                    this.checkboxes[medicine_sale.id] = false;
                });
                this.db.onSelectedMedicine_salesChanged.subscribe(selectedMedicine_sales => {
                    for (const id in this.checkboxes) {
                        this.checkboxes[id] = selectedMedicine_sales.includes(id);
                    }

                    this.selectedMedicine_sales = selectedMedicine_sales;
                });
                this.db.onSearchTextChanged.subscribe(data => {
                    this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                })
            });

    }

    newMedicine_sale() {
        this.dialogRef = this.dialog.open(FuseMedicine_salesMedicine_saleFormDialogComponent, {
            panelClass: 'medicine_sale-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
                var res = response.getRawValue();
                res.date = new Date(res.date).toISOString();
                this.db.saveMedicine_sale(res).then(data => {

                    this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

                    let p = res.quantityss.map(async (result) => {
                        var id = result.medicine_id;
                        
                        return this.db.getMedicine2(id).then(data => {
                            
                            data.sold_quantity = data.sold_quantity - (- res.quantitys);
                            data.total_quantity = data.total_quantity - (res.quantitys);
                            
                            return this.db.updateMedicine(data).then(res => {
                                
                                return res;
                            });
                        })
                    });
                    Promise.all(p).then(res => {
                        
                    })
                })
            })

    }

    viewMedicine_sale(medicine_sale) {
        this.router.navigate(['/apps/medicine_salepages', medicine_sale.id]);
    }

    editMedicine_sale(medicine_sale) {
        /*   console.log(medicine_sale)
          for (var i = 0; i <= medicine_sale.quantityss.length - 1; i++) {
              this.db.ids = medicine_sale.quantityss[i];
              console.log(medicine_sale.quantityss.length);
              console.log(this.db.ids);
                console.log(ids.quantity); 
   } */
        //console.log(medicine_sale)
        this.dialogRef = this.dialog.open(FuseMedicine_salesMedicine_saleFormDialogComponent, {
            panelClass: 'medicine_sale-form-dialog',
            data: {
                addmedicine: this.db.ids,
                medicine_sale: medicine_sale,
                action: 'edit'
            }
        });


        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':
                        var res = formData.getRawValue();
                        
                        res.date = new Date(res.date).toISOString();
                        this.db.updateMedicine_sale(res);
                        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

                        /*  console.log(res)
                         var array = [];
                         console.log(res.medicines)
                         if (res.medicines == undefined) {
                             for (var i = 0; i <= medicine_sale.quantityss.length - 1; i++) {
                                 console.log(medicine_sale)
                                 console.log(medicine_sale.quantityss[i].medicine);
                                 var med = medicine_sale.quantityss[i].medicine
 
                                 array.push(med);
 
                                 res.medicines = array.join();
                                 res.total_amount = medicine_sale.total_amount;
                                 console.log(res.medicines)
                                 this.db.updateMedicine_sale(res);
                                 this.dataSource = new FilesDataSource(this.db);
                                   res.quantityss[i].medicine = this.db.ids.medicine;
                                   res.quantityss[i].quantity = this.db.ids.quantity; 
                             }
 
                         }
                         else {
                             this.db.updateMedicine_sale(res);
                             this.dataSource = new FilesDataSource(this.db);
                         }
                         console.log(array);
                         console.log(res.quantityss[0].medicine)
  */
                        if (res.medicines) {
                            for (var i = 0; i <= medicine_sale.quantityss.length; i++) {
                                
                                var items = medicine_sale.quantityss[i];
                                this.db.getMedicine(items.medicine_id).then(data => {
                                    data.sold_quantity = data.sold_quantity - (medicine_sale.quantitys - res.quantitys);
                                    data.total_quantity = data.total_quantity + (medicine_sale.quantitys - res.quantitys);
                                    this.db.updateMedicine(data)
                                
                                })
                                
                            }
                        }

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':
                        this.deleteMedicine_sale(medicine_sale);
                        for (var i = 0; i <= medicine_sale.quantityss.length; i++) {
                            
                            var items = medicine_sale.quantityss[i];
                            this.db.getMedicine(items.medicine_id).then(data => {
                                data.sold_quantity = data.sold_quantity - (medicine_sale.quantitys);
                                data.total_quantity = data.total_quantity - (- medicine_sale.quantitys);
                                this.db.updateMedicine(data)
                                
                            })
                        }


                        break;
                }

            });
    }

    /**
     * Delete medicine_sale
     */
    deleteMedicine_sale(medicine_sale) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deleteMedicine_sale(medicine_sale);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(medicine_saleId) {
        this.db.toggleSelectedMedicine_sale(medicine_saleId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    medicine_sales2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            of(this.db.medicine_sales).delay(5000),
            this.sort.mdSortChange,
            this._paginator.page,
        ];


        var result = Observable.fromPromise(this.db.getMedicine_sales());
        /* result.subscribe(x => console.log(x), e => console.error(e));
        return result; */

        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.medicine_sales]));
        });
    }

    disconnect() {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
          return data;
        }
    
        return data.sort((a, b) => {
          const isAsc = this.sort.direction === 'asc';
          switch (this.sort.active) {
            case 'patient': return compare(a.patient_id, b.patient_id, isAsc);
            case 'amount': return compare(+a.amount, +b.amount, isAsc);
            case 'id': return compare(+a.id, +b.id, isAsc);
            default: return 0;
          }
        });
      }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

