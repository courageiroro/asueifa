import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseMedicine_salesComponent } from './medicine_sales.component';
import { Medicine_salesService } from './medicine_sales.service';
import { FuseMedicine_salesMedicine_saleListComponent } from './medicine_sale-list/medicine_sale-list.component';
import { FuseMedicine_salesSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseMedicine_salesMedicine_saleFormDialogComponent } from './medicine_sale-form/medicine_sale-form.component';
//import {AmountComponent} from './invoice-form/addtext.component'
import {FuseMedicine_salesMedicine_salePageComponent} from './medicine_sale-page/medicine_sale-page.component'
import { StaffsService } from './../staffs/staffs.service';
import { Staff } from './../staffs/staff.model';
import { SettingsService } from './../settings/settings.service';
import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from "@angular/core";
import { CurrencyPipe } from '@angular/common';
import { CurrencysService } from './../currencys/currencys.service';
import {FuseMedicine_salereceiptsMedicine_salereceiptFormDialogComponent} from './medicine-receipt/medicine-receipt.component';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : '**',
        component: FuseMedicine_salesMedicine_salePageComponent,
        children : [],
        resolve  : {
            medicine_sales: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
      FuseMedicine_salesMedicine_salePageComponent,
      FuseMedicine_salereceiptsMedicine_salereceiptFormDialogComponent
        //AmountComponent
    ],
    providers      : [
        Medicine_salesService,
        PouchService,
        StaffsService,
        SettingsService,
        CurrencyPipe,
        CurrencysService,
        DatePipe
    ],
    entryComponents: [FuseMedicine_salesMedicine_salePageComponent,FuseMedicine_salereceiptsMedicine_salereceiptFormDialogComponent]
})
export class FuseMedicine_salePageModule
{
}
