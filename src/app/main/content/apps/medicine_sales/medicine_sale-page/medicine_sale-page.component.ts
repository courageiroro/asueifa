import { Component, Inject, OnInit, ViewEncapsulation, ViewChild, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef, MdDialog } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Medicine_sale } from '../medicine_sale.model';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { Medicine_salesService } from '../medicine_sales.service';
import { StaffsService } from '../../staffs/staffs.service';
import { SettingsService } from '../../settings/settings.service';
import { DataSource } from '@angular/cdk';
import { BehaviorSubject } from 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { CurrencysService } from '../../currencys/currencys.service';
import { Pipe, PipeTransform } from "@angular/core";
import { DatePipe } from '@angular/common';
import { CurrencyPipe } from '@angular/common';
import {FuseMedicine_salereceiptsMedicine_salereceiptFormDialogComponent} from '../medicine-receipt/medicine-receipt.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-medicine_sales-medicine_sale-page',
    templateUrl: './medicine_sale-page.component.html',
    styleUrls: ['./medicine_sale-page.component.scss'],

})

export class FuseMedicine_salesMedicine_salePageComponent implements OnInit {

    event: CalendarEvent;
    dialogTitle: string;
    dialogRef: any;
    action: string;
    dataSource: FilesDataSource | null;
    medicine_sales2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    medicine_sale: Medicine_sale;
    public medicine_sales: Array<Medicine_sale> = [];
    checkboxes: {};
    selectedMedicine_sales: any[];
    statuss: any;
    localStorageItem;
    public medicine_saleDate: any;
    public patientName: any;
    public patientId: any;
    public patientAddress: any;
    public patientNumber: any;
    public pharmName: any;
    public pharmEmail: any;
    public medicine_saleDueDate: any;
    public medicineId: any;
    public quantityss: any;
    public unitss;
    public totalAmount;
    public description;
    public saleDate;
    public laboratoristName;
    public patientEmail;
    public subTotal;
    public vat;
    public discount;
    public total;
    public settings;
    public result;
    public final;
    public hospitalName;
    public result2;
    public final2;
    public hospitalAddress;
    public result3;
    public final3;
    public hospitalPhone;
    public result4;
    public final4;
    public hospitalEmail;
    public id;
    public currencys;
    public result5;
    public final5;
    public displayCurrencys;
    public arrayAmount;
    public finalarrayAmount;
    public i;
    public formatted;
    public newArray;
    public hide;
    pid;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        public dialog: MdDialog,
        public activateRoute: ActivatedRoute,
        public db: PouchService,
        private datePipe: DatePipe,
        private currencyPipe: CurrencyPipe
    ) {


    }

    getArray(gets) {
        var mine = [gets.name].join()
        return mine

    }
    getAddress(gets) {
        var mine = [gets.address].join()
        return mine

    }
    getPhone(gets) {
        var mine = [gets.phone].join()
        return mine

    }
    getEmail(gets) {
        var mine = [gets.email].join()
        return mine

    }

    getCurrency(gets) {
        var mine = [gets.currency_symbol].join()
        return mine

    }

    getAmount(gets) {
        var mine = [gets.amount].join()
        return mine

    }

    ngOnInit() {

        this.localStorageItem = JSON.parse(localStorage.getItem('user'))
        //this.pipe = currency
        //this.ok = 15;
        this.dataSource = new FilesDataSource(this.db);
        this.db.initDB(this.db.sCredentials);

        let id = this.activateRoute.snapshot.params['id'];
        this.pid = id;
        
        this.db.getMedicine_sale(id).then(res => {
            
            //this.diagnosis_reportDate = new Date(res.creation_timestamp).toDateString()
            this.patientName = res.patient_id;
            this.patientId = res.patient_name;
            this.pharmName = this.localStorageItem.name;
            this.pharmEmail = this.localStorageItem.email;
            this.saleDate = new Date().toDateString();
            this.medicineId = res.medicines;
            this.totalAmount = res.total_amount;
            this.quantityss = res.quantityss;
            
            /*  this.patientEmail = res.pemail;
             this.unitss = res.amountss;
             this.subTotal = res.subtotal;
             this.vat = res.vat_percentage;
             this.discount = res.discount_amount;
             this.total = res.grandtotal; */
            //console.log(this.arrayAmount)

            this.db.getStaff(this.patientId).then(res => {
                
                this.patientAddress = res.address;
                this.patientEmail = res.email;
                this.patientNumber = res.phone;
            })

            this.db.getCurrencys().then(res => {
                this.currencys = res;
                this.result5 = this.currencys.map(this.getCurrency)
                this.final5 = this.result5.length - 1;
                this.displayCurrencys = this.result5[this.final5]
                
                /*  this.arrayAmount = this.unitss.map(this.getAmount)
                 console.log(this.arrayAmount.join())
                 for (this.i = 0; this.i <= this.arrayAmount.length-1; this.i++) {
                     this.formatted = "\n" + (new CurrencyPipe('en-US')).transform(this.arrayAmount[this.i], this.displayCurrencys.trim(), true)+ "<br>";
                     console.log(this.formatted)
                     this.newArray = [];
                     this.newArray.push(this.formatted.toString());
                     console.log(this.newArray);
                 } */
            })


        })


        this.db.getSettings().then(res => {
            this.settings = res;
            this.result = this.settings.map(this.getArray)
            this.final = this.result.length - 1;
            this.hospitalName = this.result[this.final]

            this.result2 = this.settings.map(this.getAddress)
            this.final2 = this.result2.length - 1;
            this.hospitalAddress = this.result2[this.final2]

            this.result3 = this.settings.map(this.getPhone)
            this.final3 = this.result3.length - 1;
            this.hospitalPhone = this.result3[this.final3]

            this.result4 = this.settings.map(this.getEmail)
            this.final4 = this.result4.length - 1;
            this.hospitalEmail = this.result4[this.final4]
        })


        //DATE PIPE
        //this.birthday = new Date;
        /* this.datePipe.transform(this.birthday, 'yyyy-MM-dd');
        console.log(this.datePipe.transform(this.birthday, 'yyyy-MM-dd')); */
        //END


    }

    private _loadDiagnosis_reports(): void {
        this.db.onMedicine_salesChanged.subscribe(data => {
            this.dataSource = new FilesDataSource(this.db);
        })
        this.db.getMedicine_sales()
            .then((medicine_sales: Array<Medicine_sale>) => {
                this.medicine_sales = medicine_sales;
                this.medicine_sales2 = new BehaviorSubject<any>(medicine_sales);
                //console.log(this.invoices2);

                this.checkboxes = {};
                medicine_sales.map(medicine_sale => {
                    this.checkboxes[medicine_sale.id] = false;
                });
                this.db.onSelectedMedicine_salesChanged.subscribe(selectedMedicine_sales => {
                    for (const id in this.checkboxes) {
                        this.checkboxes[id] = selectedMedicine_sales.includes(id);
                    }

                    this.selectedMedicine_sales = selectedMedicine_sales;
                });
                this.db.onSearchTextChanged.subscribe(data => {
                    this.dataSource = new FilesDataSource(this.db);
                })
            });

    }

    print() {
        this.dialogRef = this.dialog.open(FuseMedicine_salereceiptsMedicine_salereceiptFormDialogComponent, {
            panelClass: 'medicine_sale-receipt-dialog',
            data: {
                pid: this.pid
            }
        })
        //window.print();
        this.dialogRef.afterClosed()
    }
}




export class FilesDataSource extends DataSource<any>
{
    invoices2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        var result = Observable.fromPromise(this.db.getMedicine_sales());
        result.subscribe(x => console.log(x), e => console.error(e));
        return result;
    }

    disconnect() {
    }
}