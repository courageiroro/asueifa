import { AddMedicine } from './medicine_sale-form/addmedicine.model';
export interface Medicine_sale {
    id: string,
    rev: string,
    patient_id: string,
    medicines: string,
    quantitys: number,
    total_amount: string,
    medicine_number: string,
    date: Date;
    patient_name: string,
    quantityss: AddMedicine[],
    insurancename: string,
    insurancepercent:any
}

