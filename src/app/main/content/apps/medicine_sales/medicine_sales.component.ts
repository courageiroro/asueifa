import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Medicine_salesService } from './medicine_sales.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-medicine_sales',
    templateUrl  : './medicine_sales.component.html',
    styleUrls    : ['./medicine_sales.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FuseMedicine_salesComponent implements OnInit
{
    hasSelectedMedicine_sales: boolean;
    searchInput: FormControl;

    constructor(private medicine_salesService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.medicine_salesService.onSelectedMedicine_salesChanged
            .subscribe(selectedMedicine_sales => {
                this.hasSelectedMedicine_sales = selectedMedicine_sales.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.medicine_salesService.onSearchTextChanged.next(searchText);
            });
    }

}
