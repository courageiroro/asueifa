import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseMedicine_salesComponent } from './medicine_sales.component';
import { Medicine_salesService } from './medicine_sales.service';
import { FuseMedicine_salesMedicine_saleListComponent } from './medicine_sale-list/medicine_sale-list.component';
import { FuseMedicine_salesSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseMedicine_salesMedicine_saleFormDialogComponent } from './medicine_sale-form/medicine_sale-form.component';
import { AddMedicineComponent } from './medicine_sale-form/addmedicine.component'
import { MedicinesService } from './../medicines/medicines.service';
import {StaffsService} from './../staffs/staffs.service'
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path: 'apps/medicine_sales',
        component: FuseMedicine_salesComponent,
        canActivate: [AuthGuard],
        children: [],
        resolve: {
            medicine_sales: PouchService
        }
    }
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        FuseMedicine_salesComponent,
        FuseMedicine_salesMedicine_saleListComponent,
        FuseMedicine_salesSelectedBarComponent,
        FuseMedicine_salesMedicine_saleFormDialogComponent,
        AddMedicineComponent,
    ],
    providers: [
        Medicine_salesService,
        PouchService,
        MedicinesService,
        AuthGuard,
        StaffsService 
    ],
    entryComponents: [FuseMedicine_salesMedicine_saleFormDialogComponent, AddMedicineComponent]
})
export class FuseMedicine_salesModule {
}
