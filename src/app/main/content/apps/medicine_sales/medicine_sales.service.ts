/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import { AddMedicine } from './medicine_sale-form/addmedicine.model';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Medicine_sale } from './medicine_sale.model';
import { Schema } from '../schema';

@Injectable()
export class Medicine_salesService {
    public medicine_salename = "";
    public hide = "";
    public ids: any;
    public addMedicine: AddMedicine
    public sCredentials;
    onMedicine_salesChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedMedicine_salesChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    medicine_sales: Medicine_sale[];
    user: any;
    selectedMedicine_sales: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);

    }

    /**
     * The medicine_sales App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");
        

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getMedicine_sales()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getMedicine_sales();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getMedicine_sales();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('asueifai', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/asueifai';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/asueifai';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }

    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }


    /***********
     * medicine_sale
     **********/
    /**
     * Save a medicine_sale
     * @param {medicine_sale} medicine_sale
     *
     * @return Promise<medicine_sale>
     */
    saveMedicine_sale(medicine_sale: Medicine_sale): Promise<Medicine_sale> {
        medicine_sale.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('medicine_sale', medicine_sale)
            .then((data: any) => {

                if (data && data.medicine_sales && data.medicine_sales
                [0]) {
                    return data.medicine_sales[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a medicine_sale
    * @param {medicine_sale} medicine_sale
    *
    * @return Promise<medicine_sale>
    */
    updateMedicine_sale(medicine_sale: Medicine_sale) {

        return this.db.rel.save('medicine_sale', medicine_sale)
            .then((data: any) => {
                if (data && data.medicine_sales && data.medicine_sales
                [0]) {
                    return data.medicine_sales[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a medicine_sale
     * @param {medicine_sale} medicine_sale
     *
     * @return Promise<boolean>
     */
    removeMedicine_sale(medicine_sale: Medicine_sale): Promise<boolean> {
        return this.db.rel.del('medicine_sale', medicine_sale)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the medicine_sales
     *
     * @return Promise<Array<medicine_sale>>
     */
    getMedicine_sales(): Promise<Array<Medicine_sale>> {
        return this.db.rel.find('medicine_sale')
            .then((data: any) => {
                this.medicine_sales = data.medicine_sales;
                if (this.searchText && this.searchText !== '') {
                    this.medicine_sales = FuseUtils.filterArrayByString(this.medicine_sales, this.searchText);
                }
                //this.onmedicine_salesChanged.next(this.medicine_sales);
                return Promise.resolve(this.medicine_sales);
                //return data.medicine_sales ? data.medicine_sales : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a medicine_sale
     * @param {medicine_sale} medicine_sale
     *
     * @return Promise<medicine_sale>
     */
    getMedicine_sale(id): Promise<Medicine_sale> {
        return this.db.rel.find('medicine_sale', id)
            .then((data: any) => {
                return data && data.medicine_sales ? data.medicine_sales[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected medicine_sale by id
     * @param id
     */
    toggleSelectedMedicine_sale(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedMedicine_sales.length > 0) {
            const index = this.selectedMedicine_sales.indexOf(id);

            if (index !== -1) {
                this.selectedMedicine_sales.splice(index, 1);

                // Trigger the next event
                this.onSelectedMedicine_salesChanged.next(this.selectedMedicine_sales);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedMedicine_sales.push(id);

        // Trigger the next event
        this.onSelectedMedicine_salesChanged.next(this.selectedMedicine_sales);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedMedicine_sales.length > 0) {
            this.deselectMedicine_sales();
        }
        else {
            this.selectMedicine_sales();
        }
    }

    selectMedicine_sales(filterParameter?, filterValue?) {
        this.selectedMedicine_sales = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedMedicine_sales = [];
            this.medicine_sales.map(medicine_sale => {
                this.selectedMedicine_sales.push(medicine_sale.id);
            });
        }
        else {
            /* this.selectedmedicine_sales.push(...
                 this.medicine_sales.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedMedicine_salesChanged.next(this.selectedMedicine_sales);
    }





    deselectMedicine_sales() {
        this.selectedMedicine_sales = [];

        // Trigger the next event
        this.onSelectedMedicine_salesChanged.next(this.selectedMedicine_sales);
    }

    deleteMedicine_sale(medicine_sale) {
        this.db.rel.del('medicine_sale', medicine_sale)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const medicine_saleIndex = this.medicine_sales.indexOf(medicine_sale);
                this.medicine_sales.splice(medicine_saleIndex, 1);
                this.onMedicine_salesChanged.next(this.medicine_sales);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedMedicine_sales() {
        for (const medicine_saleId of this.selectedMedicine_sales) {
            const medicine_sale = this.medicine_sales.find(_medicine_sale => {
                return _medicine_sale.id === medicine_saleId;
            });

            this.db.rel.del('medicine_sale', medicine_sale)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const medicine_saleIndex = this.medicine_sales.indexOf(medicine_sale);
                    this.medicine_sales.splice(medicine_saleIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onMedicine_salesChanged.next(this.medicine_sales);
        this.deselectMedicine_sales();
    }
}
