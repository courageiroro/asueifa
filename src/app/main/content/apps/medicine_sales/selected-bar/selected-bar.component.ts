import { Component, OnInit } from '@angular/core';
import { Medicine_salesService } from '../medicine_sales.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseMedicine_salesSelectedBarComponent implements OnInit
{
    selectedMedicine_sales: string[];
    hasSelectedMedicine_sales: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private medicine_salesService: PouchService,
        public dialog: MdDialog
    )
    {
        this.medicine_salesService.onSelectedMedicine_salesChanged
            .subscribe(selectedMedicine_sales => {
                this.selectedMedicine_sales = selectedMedicine_sales;
                setTimeout(() => {
                    this.hasSelectedMedicine_sales = selectedMedicine_sales.length > 0;
                    this.isIndeterminate = (selectedMedicine_sales.length !== this.medicine_salesService.medicine_sales.length && selectedMedicine_sales.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.medicine_salesService.selectMedicine_sales();
    }

    deselectAll()
    {
        this.medicine_salesService.deselectMedicine_sales();
    }

    deleteSelectedMedicine_sales()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected medicine_salees?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.medicine_salesService.deleteSelectedMedicine_sales();
            }
            this.confirmDialogRef = null;
        });
    }

}
