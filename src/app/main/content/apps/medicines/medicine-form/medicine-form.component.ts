import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Medicine } from '../medicine.model';
import { Medicine_categorysService } from '../../medicine_categorys/medicine_categorys.service';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-medicines-medicine-form-dialog',
    templateUrl: './medicine-form.component.html',
    styleUrls: ['./medicine-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseMedicinesMedicineFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    medicineForm: FormGroup;
    action: string;
    medicine: Medicine;
    medicine_categoryss: any;

    constructor(
        public dialogRef: MdDialogRef<FuseMedicinesMedicineFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        public db: PouchService
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Medicine';
            this.medicine = data.medicine;
        }
        else {
            this.dialogTitle = 'New Medicine';
            this.medicine = {
                id: '',
                rev: '',
                name: '',
                medicine_category_id: '',
                description: '',
                price: '',
                manufacturing_company: '',
                total_quantity: null,
                sold_quantity: null,
                medicine_category:[]

            }
        }
        db.getMedicine_categorys().then(res=>{
        this.medicine_categoryss = res
        })

        this.medicineForm = this.createMedicineForm();
    }

    ngOnInit() {
    }

    createMedicineForm() {
        return this.formBuilder.group({
            id: [this.medicine.id],
            rev: [this.medicine.rev],
            name: [this.medicine.name],
            medicine_category_id: [this.medicine.medicine_category_id],
            description: [this.medicine.description],
            price: [this.medicine.price],
            manufacturing_company: [this.medicine.manufacturing_company],
            total_quantity: [this.medicine.total_quantity],
            sold_quantity: [this.medicine.sold_quantity]

        });
    }
}
