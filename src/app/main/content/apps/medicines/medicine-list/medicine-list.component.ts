import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MedicinesService } from '../medicines.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { FuseMedicinesMedicineFormDialogComponent } from '../medicine-form/medicine-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Medicine } from '../medicine.model';
import {PouchService} from '../../../../../provider/pouch-service';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';

@Component({
    selector: 'fuse-medicines-medicine-list',
    templateUrl: './medicine-list.component.html',
    styleUrls: ['./medicine-list.component.scss']
})
export class FuseMedicinesMedicineListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public medicines: Array<Medicine> = [];
    medicines2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'name', 'category', 'description', 'price', 'total', 'sold', 'company', 'buttons'];
    selectedMedicines: any[];
    checkboxes: {};
    public localStorageItem: any;
    public localStorageType: any;

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;

    constructor(public dialog: MdDialog, public db: PouchService) {
        this.localStorageItem = JSON.parse(localStorage.getItem('user'));
        this.localStorageType = this.localStorageItem.usertype;
    }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        this._loadMedicines();
    }

    private _loadMedicines(): void {
        this.db.onMedicinesChanged.subscribe(data => {
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        this.db.getMedicines()
            .then((medicines: Array<Medicine>) => {
                this.medicines = medicines;
                this.medicines2 = new BehaviorSubject<any>(medicines);
                //console.log(this.medicines2);

                this.checkboxes = {};
                medicines.map(medicine => {
                    this.checkboxes[medicine.id] = false;
                });
                this.db.onSelectedMedicinesChanged.subscribe(selectedMedicines => {
                    for (const id in this.checkboxes) {
                        this.checkboxes[id] = selectedMedicines.includes(id);
                    }

                    this.selectedMedicines = selectedMedicines;
                });
                this.db.onSearchTextChanged.subscribe(data => {
                    this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                })
            });

    }

    newMedicine() {
        this.dialogRef = this.dialog.open(FuseMedicinesMedicineFormDialogComponent, {
            panelClass: 'medicine-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }

                this.db.saveMedicine(response.getRawValue());
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                

            });

    }


    editMedicine(medicine) {
        
        this.dialogRef = this.dialog.open(FuseMedicinesMedicineFormDialogComponent, {
            panelClass: 'medicine-form-dialog',
            data: {
                medicine: medicine,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':

                        this.db.updateMedicine(formData.getRawValue());
                        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteMedicine(medicine);

                        break;
                }
            });
    }

    /**
     * Delete medicine
     */
    deleteMedicine(medicine) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deleteMedicine(medicine);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(medicineId) {
        this.db.toggleSelectedMedicine(medicineId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    medicines2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            of(this.db.medicines).delay(5000),
            this.sort.mdSortChange,
            this._paginator.page,
        ];

        var result = Observable.fromPromise(this.db.getMedicines());
       /*  result.subscribe(x => console.log(x), e => console.error(e));
        return result; */

        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.medicines]));
        });
    }

    disconnect() {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
          return data;
        }
    
        return data.sort((a, b) => {
          const isAsc = this.sort.direction === 'asc';
          switch (this.sort.active) {
            case 'name': return compare(a.name, b.name, isAsc);
            case 'amount': return compare(+a.amount, +b.amount, isAsc);
            case 'id': return compare(+a.id, +b.id, isAsc);
            default: return 0;
          }
        });
      }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }