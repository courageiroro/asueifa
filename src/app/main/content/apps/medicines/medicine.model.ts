export interface Medicine {
	id: string,
	rev: string,
	name: string,
	medicine_category_id: string,
	description: string,
	price: string,
	manufacturing_company: string,
	total_quantity: number,
	sold_quantity: number,
    medicine_category: Array<string>
}
