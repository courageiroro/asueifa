import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MedicinesService } from './medicines.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-medicines',
    templateUrl  : './medicines.component.html',
    styleUrls    : ['./medicines.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FuseMedicinesComponent implements OnInit
{
    hasSelectedMedicines: boolean;
    searchInput: FormControl;

    constructor(private medicinesService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.medicinesService.onSelectedMedicinesChanged
            .subscribe(selectedMedicines => {
                this.hasSelectedMedicines = selectedMedicines.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.medicinesService.onSearchTextChanged.next(searchText);
            });
    }

}
