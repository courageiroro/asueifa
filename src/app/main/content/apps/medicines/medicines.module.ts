import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseMedicinesComponent } from './medicines.component';
import { MedicinesService } from './medicines.service';
import { FuseMedicinesMedicineListComponent } from './medicine-list/medicine-list.component';
import { FuseMedicinesSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseMedicinesMedicineFormDialogComponent } from './medicine-form/medicine-form.component';
import { Medicine_categorysService } from './../medicine_categorys/medicine_categorys.service';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/medicines',
        component: FuseMedicinesComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            medicines: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseMedicinesComponent,
        FuseMedicinesMedicineListComponent,
        FuseMedicinesSelectedBarComponent,
        FuseMedicinesMedicineFormDialogComponent
    ],
    providers      : [
        MedicinesService,
        PouchService,
        AuthGuard,
        Medicine_categorysService
    ],
    entryComponents: [FuseMedicinesMedicineFormDialogComponent]
})
export class FuseMedicinesModule
{
}
