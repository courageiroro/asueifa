/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Medicine } from './medicine.model';
import { Schema } from '../schema';

@Injectable()
export class MedicinesService {
    public medicinename = "";
    public medicineId = "";
    public sCredentials;
    onMedicinesChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedMedicinesChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    medicines: Medicine[];
    user: any;
    selectedMedicines: string[] = [];
    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The medicines App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getMedicines()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getMedicines();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getMedicines();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('asueifai', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/asueifai';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/asueifai';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }


    /***********
     * medicine
     **********/
    /**
     * Save a medicine
     * @param {medicine} medicine
     *
     * @return Promise<medicine>
     */
    saveMedicine(medicine: Medicine): Promise<Medicine> {
        medicine.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('medicine', medicine)
            .then((data: any) => {

                if (data && data.medicines && data.medicines
                [0]) {
                    return data.medicines[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a medicine
    * @param {medicine} medicine
    *
    * @return Promise<medicine>
    */
    updateMedicine(medicine: Medicine) {
        return this.db.rel.save('medicine', medicine)
            .then((data: any) => {
                if (data && data.medicines && data.medicines
                [0]) {
                    return data.medicines[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a medicine
     * @param {medicine} medicine
     *
     * @return Promise<boolean>
     */
    removeMedicine(medicine: Medicine): Promise<boolean> {
        return this.db.rel.del('medicine', medicine)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the medicines
     *
     * @return Promise<Array<medicine>>
     */
    getMedicines(): Promise<Array<Medicine>> {
        return this.db.rel.find('medicine')
            .then((data: any) => {
                this.medicines = data.medicines;
                if (this.searchText && this.searchText !== '') {
                    this.medicines = FuseUtils.filterArrayByString(this.medicines, this.searchText);
                }
                //this.onmedicinesChanged.next(this.medicines);
                return Promise.resolve(this.medicines);
                //return data.medicines ? data.medicines : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a medicine
     * @param {medicine} medicine
     *
     * @return Promise<medicine>
     */
    getMedicine(medicine: Medicine): Promise<Medicine> {
        return this.db.rel.find('medicine', medicine)
            .then((data: any) => {
                return data && data.medicines ? data.medicines[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getMedicine2(id): Promise<Medicine> {
        return this.db.rel.find('medicine', id)
            .then((data: any) => {
                return data && data.medicines ? data.medicines[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Toggle selected medicine by id
     * @param id
     */
    toggleSelectedMedicine(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedMedicines.length > 0) {
            const index = this.selectedMedicines.indexOf(id);

            if (index !== -1) {
                this.selectedMedicines.splice(index, 1);

                // Trigger the next event
                this.onSelectedMedicinesChanged.next(this.selectedMedicines);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedMedicines.push(id);

        // Trigger the next event
        this.onSelectedMedicinesChanged.next(this.selectedMedicines);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedMedicines.length > 0) {
            this.deselectMedicines();
        }
        else {
            this.selectMedicines();
        }
    }

    selectMedicines(filterParameter?, filterValue?) {
        this.selectedMedicines = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedMedicines = [];
            this.medicines.map(medicine => {
                this.selectedMedicines.push(medicine.id);
            });
        }
        else {
            /* this.selectedmedicines.push(...
                 this.medicines.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedMedicinesChanged.next(this.selectedMedicines);
    }





    deselectMedicines() {
        this.selectedMedicines = [];

        // Trigger the next event
        this.onSelectedMedicinesChanged.next(this.selectedMedicines);
    }

    deleteMedicine(medicine) {
        this.db.rel.del('medicine', medicine)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const medicineIndex = this.medicines.indexOf(medicine);
                this.medicines.splice(medicineIndex, 1);
                this.onMedicinesChanged.next(this.medicines);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedMedicines() {
        for (const medicineId of this.selectedMedicines) {
            const medicine = this.medicines.find(_medicine => {
                
                return _medicine.id === medicineId;
            });

            this.db.rel.del('medicine', medicine)
                .then((data: any) => {
                    
                    //return data && data.deleted ? data.deleted: false;
                    const medicineIndex = this.medicines.indexOf(medicine);
                    this.medicines.splice(medicineIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onMedicinesChanged.next(this.medicines);
        this.deselectMedicines();
    }
}
