import { Component, OnInit } from '@angular/core';
import { MedicinesService } from '../medicines.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseMedicinesSelectedBarComponent implements OnInit
{
    selectedMedicines: string[];
    hasSelectedMedicines: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private medicinesService: PouchService,
        public dialog: MdDialog
    )
    {
        this.medicinesService.onSelectedMedicinesChanged
            .subscribe(selectedMedicines => {
                this.selectedMedicines = selectedMedicines;
                setTimeout(() => {
                    this.hasSelectedMedicines = selectedMedicines.length > 0;
                    this.isIndeterminate = (selectedMedicines.length !== this.medicinesService.medicines.length && selectedMedicines.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.medicinesService.selectMedicines();
    }

    deselectAll()
    {
        this.medicinesService.deselectMedicines();
    }

    deleteSelectedMedicines()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected medicinees?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.medicinesService.deleteSelectedMedicines();
            }
            this.confirmDialogRef = null;
        });
    }

}
