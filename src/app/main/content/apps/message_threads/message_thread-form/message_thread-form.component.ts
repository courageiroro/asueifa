import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Message_thread } from '../message_thread.model';

@Component({
    selector: 'fuse-message_threads-message_thread-form-dialog',
    templateUrl: './message_thread-form.component.html',
    styleUrls: ['./message_thread-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseMessage_threadsMessage_threadFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    message_threadForm: FormGroup;
    action: string;
    message_thread: Message_thread;

    constructor(
        public dialogRef: MdDialogRef<FuseMessage_threadsMessage_threadFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Bus Type';
            this.message_thread = data.message_thread;
        }
        else {
            this.dialogTitle = 'New Bus Type';
            this.message_thread = {
                id: '',
                rev: '',
                message_thread_code: '',
                sender: '',
                reciever: '',
                last_message_timestamp: '',
            }
        }

        this.message_threadForm = this.createMessage_threadForm();
    }

    ngOnInit() {
    }

    createMessage_threadForm() {
        return this.formBuilder.group({
            id: [this.message_thread.id],
            rev: [this.message_thread.rev],
            message_thread_code: [this.message_thread.message_thread_code],
            sender: [this.message_thread.sender],
            reciever: [this.message_thread.reciever],
            last_message_timestamp: [this.message_thread.last_message_timestamp],

        });
    }
}
