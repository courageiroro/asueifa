import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Message_threadsService } from '../message_threads.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject} from 'rxjs/Rx';
import { FuseMessage_threadsMessage_threadFormDialogComponent } from '../message_thread-form/message_thread-form.component';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Message_thread } from '../message_thread.model';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-message_threads-message_thread-list',
    templateUrl: './message_thread-list.component.html',
    styleUrls  : ['./message_thread-list.component.scss']
})
export class FuseMessage_threadsMessage_threadListComponent implements OnInit
{
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public message_threads: Array<Message_thread> = [];
    message_threads2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'message_thread', 'buttons'];
    selectedMessage_threads: any[];
    checkboxes: {};

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(public dialog: MdDialog, public db:PouchService)
    { }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db);
        this._loadMessage_threads();
    }

    private _loadMessage_threads(): void {
         this.db.onMessage_threadsChanged.subscribe(data =>{
            this.dataSource = new FilesDataSource(this.db);
        })
        this.db.getMessage_threads()
        .then((message_threads: Array<Message_thread>) => {
            this.message_threads = message_threads;
            this.message_threads2= new BehaviorSubject<any>(message_threads);

            this.checkboxes = {};
            message_threads.map(message_thread => {
                this.checkboxes[message_thread.id] = false;
            });
             this.db.onSelectedMessage_threadsChanged.subscribe(selectedMessage_threads => {
                for ( const id in this.checkboxes )
                {
                    this.checkboxes[id] = selectedMessage_threads.includes(id);
                }

                this.selectedMessage_threads = selectedMessage_threads;
            });
        this.db.onSearchTextChanged.subscribe(data =>{
            this.dataSource = new FilesDataSource(this.db);
        })
        });
        
    }

    newMessage_thread()
    {
        this.dialogRef = this.dialog.open(FuseMessage_threadsMessage_threadFormDialogComponent, {
            panelClass: 'message_thread-form-dialog',
            data      : {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if ( !response )
                {
                    return;
                }

                this.db.saveMessage_thread(response.getRawValue());
                this.dataSource = new FilesDataSource(this.db);

            });

    }

    editMessage_thread(message_thread)
    {
        this.dialogRef = this.dialog.open(FuseMessage_threadsMessage_threadFormDialogComponent, {
            panelClass: 'message_thread-form-dialog',
            data      : {
                message_thread: message_thread,
                action : 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch ( actionType )
                {
                    /**
                     * Save
                     */
                    case 'save':

                        this.db.updateMessage_thread(formData.getRawValue());
                        this.dataSource = new FilesDataSource(this.db);

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteMessage_thread(message_thread);

                        break;
                }
            });
    }

    /**
     * Delete message_thread
     */
    deleteMessage_thread(message_thread)
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.db.deleteMessage_thread(message_thread);
                this.dataSource = new FilesDataSource(this.db);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(message_threadId)
    {
        this.db.toggleSelectedMessage_thread(message_threadId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    message_threads2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService)
    {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]>
    {
        var result = Observable.fromPromise(this.db.getMessage_threads());
        result.subscribe(x => console.log(x), e => console.error(e));
        return result;    
    }

    disconnect()
    {
    }
}
