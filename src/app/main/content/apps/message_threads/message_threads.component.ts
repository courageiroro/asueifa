import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Message_threadsService } from './message_threads.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-message_threads',
    templateUrl  : './message_threads.component.html',
    styleUrls    : ['./message_threads.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FuseMessage_threadsComponent implements OnInit
{
    hasSelectedMessage_threads: boolean;
    searchInput: FormControl;

    constructor(private message_threadsService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.message_threadsService.onSelectedMessage_threadsChanged
            .subscribe(selectedMessage_threads => {
                this.hasSelectedMessage_threads = selectedMessage_threads.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.message_threadsService.onSearchTextChanged.next(searchText);
            });
    }

}
