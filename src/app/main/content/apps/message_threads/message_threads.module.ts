import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseMessage_threadsComponent } from './message_threads.component';
import { Message_threadsService } from './message_threads.service';
import { FuseMessage_threadsMessage_threadListComponent } from './message_thread-list/message_thread-list.component';
import { FuseMessage_threadsSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseMessage_threadsMessage_threadFormDialogComponent } from './message_thread-form/message_thread-form.component';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/message_threads',
        component: FuseMessage_threadsComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            message_threads: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseMessage_threadsComponent,
        FuseMessage_threadsMessage_threadListComponent,
        FuseMessage_threadsSelectedBarComponent,
        FuseMessage_threadsMessage_threadFormDialogComponent
    ],
    providers      : [
        AuthGuard,
        PouchService,
        Message_threadsService
    ],
    entryComponents: [FuseMessage_threadsMessage_threadFormDialogComponent]
})
export class FuseMessage_threadsModule
{
}
