/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Message_thread } from './message_thread.model';
import { Schema } from '../schema';

@Injectable()
export class Message_threadsService {
    public message_threadname = "";
    public sCredentials;
    onMessage_threadsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedMessage_threadsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    message_threads: Message_thread[];
    user: any;
    selectedMessage_threads: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The message_threads App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getMessage_threads()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getMessage_threads();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getMessage_threads();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('asueifaibayelsa', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/asueifaibayelsa';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/asueifaibayelsa';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }


    /***********
     * message_thread
     **********/
    /**
     * Save a message_thread
     * @param {message_thread} message_thread
     *
     * @return Promise<message_thread>
     */
    saveMessage_thread(message_thread: Message_thread): Promise<Message_thread> {
        message_thread.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('message_thread', message_thread)
            .then((data: any) => {

                if (data && data.message_threads && data.message_threads
                [0]) {
                    return data.message_threads[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a message_thread
    * @param {message_thread} message_thread
    *
    * @return Promise<message_thread>
    */
    updateMessage_thread(message_thread: Message_thread) {

        return this.db.rel.save('message_thread', message_thread)
            .then((data: any) => {
                if (data && data.message_threads && data.message_threads
                [0]) {
                    return data.message_threads[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a message_thread
     * @param {message_thread} message_thread
     *
     * @return Promise<boolean>
     */
    removeMessage_thread(message_thread: Message_thread): Promise<boolean> {
        return this.db.rel.del('message_thread', message_thread)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the message_threads
     *
     * @return Promise<Array<message_thread>>
     */
    getMessage_threads(): Promise<Array<Message_thread>> {
        return this.db.rel.find('message_thread')
            .then((data: any) => {
                this.message_threads = data.message_threads;
                if (this.searchText && this.searchText !== '') {
                    this.message_threads = FuseUtils.filterArrayByString(this.message_threads, this.searchText);
                }
                //this.onmessage_threadsChanged.next(this.message_threads);
                return Promise.resolve(this.message_threads);
                //return data.message_threads ? data.message_threads : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a message_thread
     * @param {message_thread} message_thread
     *
     * @return Promise<message_thread>
     */
    getMessage_thread(message_thread: Message_thread): Promise<Message_thread> {
        return this.db.rel.find('message_thread', message_thread.id)
            .then((data: any) => {
                return data && data.message_threads ? data.message_threads[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected message_thread by id
     * @param id
     */
    toggleSelectedMessage_thread(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedMessage_threads.length > 0) {
            const index = this.selectedMessage_threads.indexOf(id);

            if (index !== -1) {
                this.selectedMessage_threads.splice(index, 1);

                // Trigger the next event
                this.onSelectedMessage_threadsChanged.next(this.selectedMessage_threads);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedMessage_threads.push(id);

        // Trigger the next event
        this.onSelectedMessage_threadsChanged.next(this.selectedMessage_threads);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedMessage_threads.length > 0) {
            this.deselectMessage_threads();
        }
        else {
            this.selectMessage_threads();
        }
    }

    selectMessage_threads(filterParameter?, filterValue?) {
        this.selectedMessage_threads = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedMessage_threads = [];
            this.message_threads.map(message_thread => {
                this.selectedMessage_threads.push(message_thread.id);
            });
        }
        else {
            /* this.selectedmessage_threads.push(...
                 this.message_threads.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedMessage_threadsChanged.next(this.selectedMessage_threads);
    }





    deselectMessage_threads() {
        this.selectedMessage_threads = [];

        // Trigger the next event
        this.onSelectedMessage_threadsChanged.next(this.selectedMessage_threads);
    }

    deleteMessage_thread(message_thread) {
        this.db.rel.del('message_thread', message_thread)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const message_threadIndex = this.message_threads.indexOf(message_thread);
                this.message_threads.splice(message_threadIndex, 1);
                this.onMessage_threadsChanged.next(this.message_threads);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedMessage_threads() {
        for (const message_threadId of this.selectedMessage_threads) {
            const message_thread = this.message_threads.find(_message_thread => {
                return _message_thread.id === message_threadId;
            });

            this.db.rel.del('message_thread', message_thread)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const message_threadIndex = this.message_threads.indexOf(message_thread);
                    this.message_threads.splice(message_threadIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onMessage_threadsChanged.next(this.message_threads);
        this.deselectMessage_threads();
    }
}
