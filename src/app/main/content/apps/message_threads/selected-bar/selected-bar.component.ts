import { Component, OnInit } from '@angular/core';
import { Message_threadsService } from '../message_threads.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseMessage_threadsSelectedBarComponent implements OnInit
{
    selectedMessage_threads: string[];
    hasSelectedMessage_threads: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private message_threadsService: PouchService,
        public dialog: MdDialog
    )
    {
        this.message_threadsService.onSelectedMessage_threadsChanged
            .subscribe(selectedMessage_threads => {
                this.selectedMessage_threads = selectedMessage_threads;
                setTimeout(() => {
                    this.hasSelectedMessage_threads = selectedMessage_threads.length > 0;
                    this.isIndeterminate = (selectedMessage_threads.length !== this.message_threadsService.message_threads.length && selectedMessage_threads.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.message_threadsService.selectMessage_threads();
    }

    deselectAll()
    {
        this.message_threadsService.deselectMessage_threads();
    }

    deleteSelectedMessage_threads()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected message threades?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.message_threadsService.deleteSelectedMessage_threads();
            }
            this.confirmDialogRef = null;
        });
    }

}
