import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { FormControl, FormGroup } from '@angular/forms';
import { Message } from '../../message.model';

@Component({
    selector     : 'fuse-message-compose',
    templateUrl  : './compose.component.html',
    styleUrls    : ['./compose.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class FuseMessageComposeDialogComponent implements OnInit
{
    message: Message
    composeForm: FormGroup;

    constructor(
        public dialogRef: MdDialogRef<FuseMessageComposeDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any
    )
    {
        this.composeForm = this.createComposeForm();
    }

    ngOnInit()
    {
    }

    createComposeForm()
    {
        return new FormGroup({
           /*  id: new FormControl(this.message.id),
            rev: new FormControl(this.message.rev), */
            from   : new FormControl({
                value   : 'johndoe@creapond.com',
                disabled: true
            }),
            to     : new FormControl(''),
            cc     : new FormControl(''),
            bcc    : new FormControl(''),
            subject: new FormControl(''),
            message: new FormControl('')
        });
    }

}
