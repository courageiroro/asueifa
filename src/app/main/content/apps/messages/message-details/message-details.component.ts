import { Component, OnDestroy, OnInit } from '@angular/core';
import { MessagesService } from '../messages.service';
import { Message } from '../message.model';
import { Subscription } from 'rxjs/Subscription';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-message-details',
    templateUrl: './message-details.component.html',
    styleUrls: ['./message-details.component.scss']
})
export class FuseMessageDetailsComponent implements OnInit, OnDestroy {
    message: Message;
    labels: any[];
    showDetails = false;

    onCurrentMessageChanged: Subscription;
    onLabelsChanged: Subscription;

    constructor(
        private messageService: MessagesService
    ) {
    }

    ngOnInit() {
           // Subscribe to update the current mail
          this.onCurrentMessageChanged =
              this.messageService.onCurrentMessageChanged
                  .subscribe(currentMessage => {
                      this.message = currentMessage;
                  });
  
          // Subscribe to update on label change
          this.onLabelsChanged =
              this.messageService.onLabelsChanged
                  .subscribe(labels => {
                      this.labels = labels;
                  }); 
    }

    ngOnDestroy() {
         this.onCurrentMessageChanged.unsubscribe(); 
    }

    toggleStar(event) {
        event.stopPropagation();

        this.message.toggleStar();

        this.messageService.updateMessage(this.message); 
    }

    toggleImportant(event) {
        event.stopPropagation();

        this.message.toggleImportant();

        this.messageService.updateMessage(this.message);
    }

}
