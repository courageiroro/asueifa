import { Component, HostBinding, Input, OnDestroy, OnInit } from '@angular/core';
import { Message } from '../../message.model';
import { MessagesService } from '../../messages.service';
import { Subscription } from 'rxjs/Subscription';
import { Mail } from '../../../mail/mail.model';
import { MailService } from '../../../mail/mail.service';

@Component({
    selector: 'fuse-message-list-item',
    templateUrl: './message-list-item.component.html',
    styleUrls: ['./message-list-item.component.scss']
})
export class FuseMessageListItemComponent implements OnInit, OnDestroy {
    @Input() message: Message;
    labels: any[];
    @HostBinding('class.selected') selected: boolean;

    img;
    onSelectedMessagesChanged: Subscription;
    onLabelsChanged: Subscription;

    constructor(
        private messageService: MessagesService,
        //private mailService: MailService
    ) {
    }

    ngOnInit() {
        this.img = 'assets/EP2.png';
        // Set the initial values
        this.message = new Message(this.message);
        //this.messageService.onMessagesChanged.next(this.messageService.messages);
        // Subscribe to update on selected mail change
        this.onSelectedMessagesChanged =
            this.messageService.onSelectedMessagesChanged
                .subscribe(selectedMessages => {
                    this.selected = false;

                    if (selectedMessages.length > 0) {
                        for (const message of selectedMessages) {
                            if (message.id === this.message.id) {
                                this.selected = true;
                                break;
                            }
                        }
                    }
                });

        // Subscribe to update on label change
        this.onLabelsChanged =
            this.messageService.onLabelsChanged
                .subscribe(labels => {
                    this.labels = labels;
                });
    }

    ngOnDestroy() {
        this.onSelectedMessagesChanged.unsubscribe();
    }

    onSelectedChange() {
        this.messageService.toggleSelectedMessage(this.message.id);
    }

    /**
     * Toggle star
     * @param event
     */
    toggleStar(event) {
        event.stopPropagation();

        this.message.toggleStar();

        //this.messageService.updateMessage(this.message);
        this.messageService.updateMessage(this.message);
    }

    /**
     * Toggle Important
     * @param event
     */
    toggleImportant(event) {
        event.stopPropagation();

        this.message.toggleImportant();

        //this.messageService.updateMessage(this.message);
        this.messageService.updateMessage(this.message);
    }
}
