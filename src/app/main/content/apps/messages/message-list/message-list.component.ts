import { Component, OnDestroy, OnInit } from '@angular/core';
import { Message } from '../message.model';
import { ActivatedRoute } from '@angular/router';
import { MessagesService } from '../messages.service';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs/Subscription';
import { MailService } from './../../mail/mail.service';
import { Mail } from './../../mail/mail.model';
import { Subject } from 'rxjs/Subject';

@Component({
    selector: 'fuse-message-list',
    templateUrl: './message-list.component.html',
    styleUrls: ['./message-list.component.scss']
})
export class FuseMessageListComponent implements OnInit, OnDestroy {
    messages: Message[];
    currentMessage: Message;
    onMailsChanged: Subscription;
    onMessagesChanged: Subscription;
    onCurrentMessageChanged: Subscription;
    mails: Mail[];
    refresh: Subject<any> = new Subject();

    constructor(
        private route: ActivatedRoute,
        private messageService: MessagesService,
        private location: Location,
        private mailService: MailService
    ) {
    }

    ngOnInit() {
        // Subscribe to update mails on changes
        
        this.onMessagesChanged =
            this.messageService.onMessagesChanged
                .subscribe(messages => {
                    this.messages = messages;
                    
                    //this.refresh.next();
                });
        //this.messageService.onMessagesChanged.next(this.messageService.messages);

        // Subscribe to update current mail on changes
          this.onCurrentMessageChanged =
             this.messageService.onCurrentMessageChanged
                 .subscribe(currentMessage => {
                     if (!currentMessage) {
                         // Set the current mail id to null to deselect the current mail
                         this.currentMessage = null;
 
                         // Handle the location changes
                         const labelHandle = this.route.snapshot.params.labelHandle,
                             filterHandle = this.route.snapshot.params.filterHandle,
                             folderHandle = this.route.snapshot.params.folderHandle;
                            
                         if (labelHandle) {
                             this.location.go('apps/messages/label/' + labelHandle);
                         }
                         else if (filterHandle) {
                             this.location.go('apps/messages/filter/' + filterHandle);
                         }
                         else {
                             this.location.go('apps/messages/' + folderHandle);
                         }
                     }
                     else {
                         this.currentMessage = currentMessage;
                     }
                 }); 
    }

    ngOnDestroy() {
        this.onMessagesChanged.unsubscribe();
        this.onCurrentMessageChanged.unsubscribe();
    }

    click() {
            var id = this.route.snapshot.params['inbox'];
            
    }


    /**
     * Read mail
     * @param mailId
     */
    readMail(mailId) {
        const labelHandle = this.route.snapshot.params.labelHandle,
            filterHandle = this.route.snapshot.params.filterHandle,
            folderHandle = this.route.snapshot.params.folderHandle;

        if (labelHandle) {
            this.location.go('apps/message/label/' + labelHandle + '/' + mailId);
        }
        else if (filterHandle) {
            this.location.go('apps/message/filter/' + filterHandle + '/' + mailId);
        }
        else {
            this.location.go('apps/message/' + folderHandle + '/' + mailId);
        }

        // Set current mail
        this.messageService.setCurrentMessage(mailId);
    }

}
