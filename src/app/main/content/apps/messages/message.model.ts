export class Message {
    id: string;
    rev: string;
    from: {
        name: string,
        avatar: string,
        email: string
    };
    to: {
        name: string,
        email: string
    }[];
     subject: string;
    message: string;
    time: string;
    read: boolean;
    starred: boolean;
    important: boolean;
    hasAttachments: boolean;
    attachments: {
        type: string,
        fileName: string,
        preview: string,
        url: string,
        size: string
    }[];
    labels: string[];
    folder: string;

    constructor(message)
    {
        this.id = message.id;
        this.from = message.from;
        this.to = message.to;
        this.subject = message.subject;
        this.message = message.message;
        this.time = message.time;
        this.read = message.read;
        this.starred = message.starred;
        this.important = message.important;
        this.hasAttachments = message.hasAttachments;
        this.attachments = message.attachments;
        this.labels = message.labels;
        this.folder = message.folder;
    }

    toggleStar()
    {
        this.starred = !this.starred;
    }

    toggleImportant()
    {
        this.important = !this.important;
    }
}

    /* message_thread_code: string,
    message: string,
    sender: string,
    timestamp: string,
    read_status: string, */



