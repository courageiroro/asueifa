import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MessagesService } from './messages.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import { Message } from './message.model';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'fuse-messages',
    templateUrl: './messages.component.html',
    styleUrls: ['./messages.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [Animations.slideInTop]
})
export class FuseMessagesComponent implements OnInit {
    hasSelectedMessages: boolean;
    searchInput: FormControl;
    isIndeterminate: boolean;
    folders: any[];
    filters: any[];
    labels: any[];
    currentMessage: Message;

    onSelectedMessagesChanged: Subscription;
    onFoldersChanged: Subscription;
    onFiltersChanged: Subscription;
    onLabelsChanged: Subscription;
    onCurrentMessageChanged: Subscription;

    constructor(private messagesService: MessagesService) {
        this.searchInput = new FormControl('');
    }

    ngOnInit() {
        this.onSelectedMessagesChanged =
            this.messagesService.onSelectedMessagesChanged
                .subscribe(selectedMessages => {
                    setTimeout(() => {
                        this.hasSelectedMessages = selectedMessages.length > 0;
                        this.isIndeterminate = (selectedMessages.length !== this.messagesService.messages.length && selectedMessages.length > 0);
                    }, 0);
                });

        this.onFoldersChanged =
            this.messagesService.onFoldersChanged
                .subscribe(folders => {
                    this.folders = this.messagesService.folders;
                });

        this.onFiltersChanged =
            this.messagesService.onFiltersChanged
                .subscribe(folders => {
                    this.filters = this.messagesService.filters;
                });

        this.onLabelsChanged =
            this.messagesService.onLabelsChanged
                .subscribe(labels => {
                    this.labels = this.messagesService.labels;
                });

        this.onCurrentMessageChanged =
            this.messagesService.onCurrentMessageChanged
                .subscribe(currentMessage => {
                    if (!currentMessage) {
                        this.currentMessage = null;
                    }
                    else {
                        this.currentMessage = currentMessage;
                    }
                });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.messagesService.onSearchTextChanged.next(searchText);
            });
    }
   
    ngOnDestroy()
    {
        this.onSelectedMessagesChanged.unsubscribe();
        this.onFoldersChanged.unsubscribe();
        this.onFiltersChanged.unsubscribe();
        this.onLabelsChanged.unsubscribe();
        this.onCurrentMessageChanged.unsubscribe();
    }
   
    toggleSelectAll()
    {
        this.messagesService.toggleSelectAll();
    }

    selectMessages(filterParameter?, filterValue?)
    {
        this.messagesService.selectMessages(filterParameter, filterValue);
    }

    deselectMessages()
    {
        this.messagesService.deselectMessages();
    }

    deSelectCurrentMessage()
    {
        this.messagesService.onCurrentMessageChanged.next(null);
    }

    toggleLabelOnSelectedMessages(labelId)
    {
        this.messagesService.toggleLabelOnSelectedMessages(labelId);
    }

    setFolderOnSelectedMessages(folderId)
    {
        this.messagesService.setFolderOnSelectedMessages(folderId);
    }

}
