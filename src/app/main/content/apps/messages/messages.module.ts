import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseMessagesComponent } from './messages.component';
import { MessagesService } from './messages.service';
import {MailService} from '../mail/mail.service'
//import { FuseMessagesMessageListComponent } from './message-list/message-list.component';
import { FuseMessagesSelectedBarComponent } from './selected-bar/selected-bar.component';
//import { FuseMessagesMessageFormDialogComponent } from './message-form/message-form.component';
import { FuseMessageMainSidenavComponent } from './sidenavs/main/main-sidenav.component';
import { FuseMessageListItemComponent } from './message-list/message-list-item/message-list-item.component';
import { FuseMessageListComponent } from './message-list/message-list.component';
import { FuseMessageDetailsComponent } from './message-details/message-details.component';
import { FuseMessageComposeDialogComponent } from './dialogs/compose/compose.component';
import { AuthGuard } from 'app/auth.guard';

const routes: Routes = [
    {
        path     : 'apps/messages',
        component: FuseMessagesComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            messages: MessagesService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseMessagesComponent,
        FuseMessageListComponent,
        FuseMessageListItemComponent,
        FuseMessageDetailsComponent,
        FuseMessageMainSidenavComponent,
        FuseMessageComposeDialogComponent,
        //FuseMessagesMessageListComponent,
        FuseMessagesSelectedBarComponent,
        //FuseMessagesMessageFormDialogComponent
    ],
    providers      : [
        MessagesService,
        AuthGuard,
        MailService
    ],
    entryComponents: [FuseMessageComposeDialogComponent]
})
export class FuseMessagesModule
{
}
