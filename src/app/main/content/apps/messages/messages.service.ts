/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Message } from './message.model';
import { Schema } from '../schema';

@Injectable()
export class MessagesService {
    public messagename = "";
    public sCredentials;
    folders: any[];
    filters: any[];
    labels: any[];
    routeParams: any;
    folderHandle;
    folderId;
    folderId2;
    onMessagesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    //onEventsUpdated = new Subject<any>();
    onCurrentMessageChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    onSelectedMessagesChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    onFoldersChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    onFiltersChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    onLabelsChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();
    currentMessage: Message;
    selectedMessages2: Message[];
    messages: Message[];
    user: any;
    selectedMessages: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http, private route: ActivatedRoute) {
        this.initDB(this.sCredentials);
    }

    /**
     * The messages App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getFolders(),
                this.getFilters(),
                this.getLabels(),
                this.getMessages()
            ]).then(
                ([files]) => {
                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getMessages();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getMessages();
                    });

                    /*  if (this.routeParams.mailId) {
                         this.setCurrentMessage(this.routeParams.mailId);
                     }
                     else {
                         this.setCurrentMessage(null);
                     }
 
                     this.onSearchTextChanged.subscribe(searchText => {
                         if (searchText !== '') {
                             this.searchText = searchText;
                             this.getMessages();
                         }
                         else {
                             this.searchText = searchText;
                             this.getMessages();
                         }
                     }); */

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('asueifaibayelsa', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'admin',
                password: 'realpeople'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://157.230.219.86:5984/asueifaibayelsa';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'admin',
                password: 'realpeople'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://157.230.219.86:5984/asueifaibayelsa';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('http://157.230.219.86:5984/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }

    //get Title and Icon of Folders
    /**
   * Get all folders
   * @returns {Promise<any>}
   */
    getFolders(): Promise<any> {
        /*  return new Promise((resolve, reject) => {
             var jsonData = [
                 { id: 0, handle: "inbox", title: "Inbox", icon: "inbox" },
                 { id: 1, handle: "sent", title: "Sent", icon: "send" },
                 { id: 2, handle: "drafts", title: "Drafts", icon: "email_open" },
                 { id: 3, handle: "spam", title: "Spam", icon: "error" },
                 { id: 4, handle: "trash", title: "Trash", icon: "delete" }]
 
             this.folders = jsonData;
             console.log(this.folders);
             this.onFoldersChanged.next(this.folders);
             resolve(this.folders);
 
         }); */
        return new Promise((resolve, reject) => {
            this.http.get('api/mail-folders')
                .subscribe(response => {
                    this.folders = response.json().data;
                    this.onFoldersChanged.next(this.folders);
                    resolve(this.folders);
                }, reject);
        });
    }

    /**
    * Get all filters
    * @returns {Promise<any>}
    */
    getFilters(): Promise<any> {
        /*  return new Promise((resolve, reject) => {
             var jsonData = [
                 { id: 0, handle: "starred", title: "Starred", icon: "star" },
                 { id: 1, handle: "important", title: "Important", icon: "label" }]
 
             this.filters = jsonData;
             console.log(this.filters);
             this.onFiltersChanged.next(this.filters);
             resolve(this.filters);
 
         });*/

        return new Promise((resolve, reject) => {
            this.http.get('api/mail-filters')
                .subscribe(response => {
                    this.filters = response.json().data;
                    console.log(this.filters);
                    this.onFiltersChanged.next(this.filters);
                    resolve(this.filters);
                }, reject);
        });
    }


    /**
    * Get all labels
    * @returns {Promise<any>}
    */
    getLabels(): Promise<any> {
        /*   return new Promise((resolve, reject) => {
              var jsonData = [
                  { id: 0, handle: "note", title: "Note", color: "#7cb342" },
                  { id: 1, handle: "paypal", title: "Paypal", color: "#d84315" },
                  { id: 2, handle: "invoice", title: "Invoice", color: "#607d8b" },
                  { id: 3, handle: "amazon", title: "Amazon", color: "#03a9f4" }]
  
              this.labels = jsonData;
              console.log(this.labels);
              this.onLabelsChanged.next(this.labels);
              resolve(this.labels);
  
          }); */
        return new Promise((resolve, reject) => {
            this.http.get('api/mail-labels')
                .subscribe(response => {
                    this.labels = response.json().data;
                    console.log(this.labels);
                    this.onLabelsChanged.next(this.labels);
                    resolve(this.labels);
                }, reject);
        });
    }

    /***********
     * message
     **********/
    /**
     * Save a message
     * @param {message} message
     *
     * @return Promise<message>
     */
    saveMessage(message: Message): Promise<Message> {
        message.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('message', message)
            .then((data: any) => {

                if (data && data.messages && data.messages
                [0]) {
                    console.log(data.messages[0])
                    return data.messages[0]
                }
                //this.onMessagesChanged.next(this.messages);
                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a message
    * @param {message} message
    *
    * @return Promise<message>
    */
    updateMessage(message: Message) {

        return this.db.rel.save('message', message)
            .then((data: any) => {
                if (data && data.messages && data.messages
                [0]) {
                    return data.messages[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a message
     * @param {message} message
     *
     * @return Promise<boolean>
     */
    removeMessage(message: Message): Promise<boolean> {
        return this.db.rel.del('message', message)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Get all mails
     * @returns {Promise<Mail[]>}
     */
    getMessages(): Promise<Message[]> {
        /*      if (this.routeParams.labelHandle) { 
               return this.getMessagesByLabel(this.routeParams.labelHandle);
           }
   
           if (this.routeParams.filterHandle) {
               return this.getMessagesByFilter(this.routeParams.filterHandle);
           }   */
        console.log(this.route.snapshot.params['inbox']);
        return this.getMessagesByFolder();
    }


    getMessagesByFolder(): Promise<Array<Message>> {
        return this.db.rel.find('message')
            .then((data: any) => {
                console.log(this.folders[1].handle)
                this.messages = data.messages;
                this.folderId = this.folders[0].id
                console.log(this.folders[0].id);
                //this.onEventsUpdated.next(this.messages);
                //this.onMessagesChanged.next(this.messages);
                if (this.searchText && this.searchText !== '') {
                    this.messages = FuseUtils.filterArrayByString(this.messages, this.searchText);

                }

                console.log(this.messages);
                this.onMessagesChanged.next(this.messages);
                return Promise.resolve(this.messages);
                //return data.messages ? data.messages : [];
            }).catch((err: any) => {
                console.error(err);
            });
        /*   return new Promise((resolve, reject) => {

        this.http.get('api/mail-folders?handle=' + handle)
            .subscribe(folders => {
                console.log(handle);
                this.folderId = folders.json().data[0].id;
                console.log(this.folderId)
                this.http.get('api/mail-mails?folder=' + this.folderId)
                    .subscribe(mails => {

                        this.messages = mails.json().data.map(mail => {
                            console.log(mails.json().data)
                            return new Message(mail);

                        });

                        this.messages = FuseUtils.filterArrayByString(this.messages, this.searchText);

                        this.onMessagesChanged.next(this.messages);

                        resolve(this.messages);
                        //console.log(this.mails)
                    }, reject);
            });
    }); */
    }

    /**
    * Get mails by filter
    * @param handle
    * @returns {Promise<Mail[]>}
    */
    getMessagesByFilter(handle): Promise<Message[]> {
        return this.db.rel.find('message')
            .then((data: any) => {
                this.folderId = 0;
                this.messages = data.messages + this.folderId;
                //this.onEventsUpdated.next(this.messages);
                //this.onMessagesChanged.next(this.messages);
                if (this.searchText && this.searchText !== '') {
                    this.messages = FuseUtils.filterArrayByString(this.messages, this.searchText);

                }

                console.log(this.messages);
                this.onMessagesChanged.next(this.messages);
                return Promise.resolve(this.messages);
                //return data.messages ? data.messages : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Get mails by label
    * @param handle
    * @returns {Promise<Mail[]>}
    */
    getMessagesByLabel(handle): Promise<Message[]> {
        return this.db.rel.find('message')
            .then((data: any) => {
                this.folderId = 0;
                this.messages = data.messages + this.folderId;
                //this.onEventsUpdated.next(this.messages);
                //this.onMessagesChanged.next(this.messages);
                if (this.searchText && this.searchText !== '') {
                    this.messages = FuseUtils.filterArrayByString(this.messages, this.searchText);

                }

                console.log(this.messages);
                this.onMessagesChanged.next(this.messages);
                return Promise.resolve(this.messages);
                //return data.messages ? data.messages : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the messages
     *
     * @return Promise<Array<message>>
     */
    /*  getMessages(): Promise<Array<Message>> {
        return this.db.rel.find('message')
            .then((data: any) => {
                this.messages = data.messages;
                this.onEventsUpdated.next(this.messages);
                //this.onMessagesChanged.next(this.messages);
                if (this.searchText && this.searchText !== '') {
                    this.messages = FuseUtils.filterArrayByString(this.messages, this.searchText);

                }
                
                console.log(this.messages);
                //this.onmessagesChanged.next(this.messages);
                return Promise.resolve(this.messages);
                //return data.messages ? data.messages : [];
            }).catch((err: any) => {
                console.error(err);
            });
    } 
 */
    /**
     * Read a message
     * @param {message} message
     *
     * @return Promise<message>
     */
    getMessage(message: Message): Promise<Message> {
        return this.db.rel.find('message', message.id)
            .then((data: any) => {
                return data && data.messages ? data.messages[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected message by id
     * @param id
     */
    toggleSelectedMessage(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedMessages.length > 0) {
            const index = this.selectedMessages.indexOf(id);

            if (index !== -1) {
                this.selectedMessages.splice(index, 1);

                // Trigger the next event
                this.onSelectedMessagesChanged.next(this.selectedMessages);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedMessages.push(id);

        // Trigger the next event
        this.onSelectedMessagesChanged.next(this.selectedMessages);
    }

    setCurrentMessage(id) {
        this.currentMessage = this.messages.find(mail => {
            console.log(mail.id)
            return mail.id === id;
        });

        this.onCurrentMessageChanged.next(this.currentMessage);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedMessages.length > 0) {
            this.deselectMessages();
        }
        else {
            this.selectMessages();
        }
    }

    selectMessages(filterParameter?, filterValue?) {
        this.selectedMessages = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedMessages = [];
            this.messages.map(message => {
                this.selectedMessages.push(message.id);
            });
        }
        else {
            /* this.selectedmessages.push(...
                 this.messages.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedMessagesChanged.next(this.selectedMessages);
    }

    toggleLabelOnSelectedMessages(labelId) {
        this.selectedMessages2.map(message => {

            const index = message.labels.indexOf(labelId);

            if (index !== -1) {
                message.labels.splice(index, 1);
            }
            else {
                message.labels.push(labelId);
            }

            this.updateMessage(message);
        });
    }

    /**
     * Set folder on selected mails
     * @param folderId
     */
    setFolderOnSelectedMessages(folderId) {
        this.selectedMessages2.map(message => {
            message.folder = folderId;

            this.updateMessage(message);
        });

        this.deselectMessages();
    }



    deselectMessages() {
        this.selectedMessages = [];

        // Trigger the next event
        this.onSelectedMessagesChanged.next(this.selectedMessages);
    }

    deleteMessage(message) {
        this.db.rel.del('message', message)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const messageIndex = this.messages.indexOf(message);
                this.messages.splice(messageIndex, 1);
                this.onMessagesChanged.next(this.messages);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedMessages() {
        for (const messageId of this.selectedMessages) {
            const message = this.messages.find(_message => {
                return _message.id === messageId;
            });

            this.db.rel.del('message', message)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const messageIndex = this.messages.indexOf(message);
                    this.messages.splice(messageIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onMessagesChanged.next(this.messages);
        this.deselectMessages();
    }
}
