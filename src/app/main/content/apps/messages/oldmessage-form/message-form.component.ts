/* import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Message } from '../message.model';

@Component({
    selector: 'fuse-messages-message-form-dialog',
    templateUrl: './message-form.component.html',
    styleUrls: ['./message-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseMessagesMessageFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    messageForm: FormGroup;
    action: string;
    message: Message;

    constructor(
        public dialogRef: MdDialogRef<FuseMessagesMessageFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Message';
            this.message = data.message;
        }
        else {
            this.dialogTitle = 'New Message';
            this.message = {
                id: '',
                rev: '',
                message_thread_code: '',
                message: '',
                sender: '',
                timestamp: '',
                read_status: '',
            }
        }

        this.messageForm = this.createmessageForm();
    }

    ngOnInit() {
    }

    createmessageForm() {
        return this.formBuilder.group({
            id: [this.message.id],
            rev: [this.message.rev],
            message_thread_code: [this.message.message_thread_code],
            message: [this.message.message],
            sender: [this.message.sender],
            timestamp: [this.message.timestamp],
            read_status: [this.message.read_status],

        });
    }
}
 */