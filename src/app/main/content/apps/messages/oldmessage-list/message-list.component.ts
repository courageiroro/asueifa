/* import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MessagesService } from '../messages.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject} from 'rxjs/Rx';
import { FuseMessagesMessageFormDialogComponent } from '../oldmessage-form/message-form.component';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Message } from '../message.model';

@Component({
    selector   : 'fuse-messages-message-list',
    templateUrl: './message-list.component.html',
    styleUrls  : ['./message-list.component.scss']
})
export class FuseMessagesMessageListComponent implements OnInit
{
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public messages: Array<Message> = [];
    messages2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'message', 'buttons'];
    selectedMessages: any[];
    checkboxes: {};

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(public dialog: MdDialog, public db:MessagesService)
    { }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db);
        this.db.initDB();
        this._loadMessages();
    }

    private _loadMessages(): void {
         this.db.onMessagesChanged.subscribe(data =>{
            this.dataSource = new FilesDataSource(this.db);
        })
        this.db.getMessages()
        .then((messages: Array<Message>) => {
            this.messages = messages;
            this.messages2= new BehaviorSubject<any>(messages);

            this.checkboxes = {};
            messages.map(message => {
                this.checkboxes[message.id] = false;
            });
             this.db.onSelectedMessagesChanged.subscribe(selectedMessages => {
                for ( const id in this.checkboxes )
                {
                    this.checkboxes[id] = selectedMessages.includes(id);
                }

                this.selectedMessages = selectedMessages;
            });
        this.db.onSearchTextChanged.subscribe(data =>{
            this.dataSource = new FilesDataSource(this.db);
        })
        });
        
    }

    newMessage()
    {
        this.dialogRef = this.dialog.open(FuseMessagesMessageFormDialogComponent, {
            panelClass: 'message-form-dialog',
            data      : {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if ( !response )
                {
                    return;
                }

                this.db.saveMessage(response.getRawValue());
                this.dataSource = new FilesDataSource(this.db);

            });

    }

    editMessage(message)
    {
        this.dialogRef = this.dialog.open(FuseMessagesMessageFormDialogComponent, {
            panelClass: 'message-form-dialog',
            data      : {
                message: message,
                action : 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch ( actionType )
                {*/
                    /**
                     * Save
                     */
                   /*  case 'save':

                        this.db.updateMessage(formData.getRawValue());
                        this.dataSource = new FilesDataSource(this.db);

                        break; */
                    /**
                     * Delete
                     */
                  /*   case 'delete':

                        this.deleteMessage(message);

                        break;
                }
            });
    } */

    /**
     * Delete message
     */
   /*  deleteMessage(message)
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.db.deleteMessage(message);
                this.dataSource = new FilesDataSource(this.db);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(messageId)
    {
        this.db.toggleSelectedMessage(messageId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    messages2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: MessagesService)
    {
        super();
    }*/

    /** Connect function called by the table to retrieve one stream containing the data to render. */
  /*   connect(): Observable<any[]>
    {
        var result = Observable.fromPromise(this.db.getMessages());
        result.subscribe(x => console.log(x), e => console.error(e));
        return result;    
    }

    disconnect()
    {
    }
}
  */