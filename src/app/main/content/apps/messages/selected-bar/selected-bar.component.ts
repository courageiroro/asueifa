import { Component, OnInit } from '@angular/core';
import { MessagesService } from '../messages.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseMessagesSelectedBarComponent implements OnInit
{
    selectedMessages: string[];
    hasSelectedMessages: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private messagesService: MessagesService,
        public dialog: MdDialog
    )
    {
        this.messagesService.onSelectedMessagesChanged
            .subscribe(selectedMessages => {
                this.selectedMessages = selectedMessages;
                setTimeout(() => {
                    this.hasSelectedMessages = selectedMessages.length > 0;
                    this.isIndeterminate = (selectedMessages.length !== this.messagesService.messages.length && selectedMessages.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.messagesService.selectMessages();
    }

    deselectAll()
    {
        this.messagesService.deselectMessages();
    }

    deleteSelectedMessages()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected messagees?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.messagesService.deleteSelectedMessages();
            }
            this.confirmDialogRef = null;
        });
    }

}
