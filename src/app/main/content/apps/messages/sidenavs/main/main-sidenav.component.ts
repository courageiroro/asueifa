import { Component, OnDestroy, OnInit } from '@angular/core';
import { MessagesService } from '../../messages.service';
import { Subscription } from 'rxjs/Subscription';
import { FuseMessageComposeDialogComponent } from '../../dialogs/compose/compose.component';
import { MdDialog } from '@angular/material';
import { FormGroup } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import { Message } from '../../message.model';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'fuse-message-main-sidenav',
    templateUrl: './main-sidenav.component.html',
    styleUrls: ['./main-sidenav.component.scss']
})
export class FuseMessageMainSidenavComponent implements OnInit, OnDestroy {
    mail: any[];
    messages: Message[];
    folders: any[];
    filters: any[];
    labels: any[];
    accounts: object;
    selectedAccount: string;
    dialogRef: any;
    refresh: Subject<any> = new Subject();
    onFoldersChanged: Subscription;
    onFiltersChanged: Subscription;
    onLabelsChanged: Subscription;

    constructor(
        private messageService: MessagesService,
        public dialog: MdDialog,
        private route: ActivatedRoute
    ) {
        // Data
        this.accounts = {
            'creapond': 'johndoe@creapond.com',
            'withinpixels': 'johndoe@withinpixels.com'
        };

        this.selectedAccount = 'creapond';
    }

    ngOnInit() {
        this.onFoldersChanged =
            this.messageService.onFoldersChanged
                .subscribe(folders => {
                    this.folders = folders;
                });

        this.onFiltersChanged =
            this.messageService.onFiltersChanged
                .subscribe(filters => {
                    this.filters = filters;
                });

        this.onLabelsChanged =
            this.messageService.onLabelsChanged
                .subscribe(labels => {
                    this.labels = labels;
                });

    }

    composeDialog() {
        this.dialogRef = this.dialog.open(FuseMessageComposeDialogComponent, {
            panelClass: 'mail-compose-dialog'
        });
        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Send
                     */
                    case 'send':
                        const messageData = formData.getRawValue()
                        this.messageService.messages.push(messageData);
                        this.messageService.saveMessage(messageData);
                        this.refresh.next(true);
                        this.messageService.onMessagesChanged.next(this.messageService.messages);
                        console.log(this.messageService.messages)
                        console.log('new Mail', formData.getRawValue());
                        break;
                    /**
                     * Delete
                     */
                    case 'delete':
                        console.log('delete Mail');
                        break;
                }
            });
    }

    send() {
        var folderHandle2 = this.route.snapshot.params.folderHandle;
        console.log(folderHandle2);
        /*  var array2= [];
         var array = this.folders.map(function (obj) {
             console.log(obj.id);
             array2.push(obj.id);
             console.log(array2)
             console.log(array2.length)
             return obj.id
         }); */
        //this.mailService.folderId2 = 
    }

    ngOnDestroy() {
        this.onFoldersChanged.unsubscribe();
        this.onFiltersChanged.unsubscribe();
        this.onLabelsChanged.unsubscribe();
    }
}
