import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Note } from '../note.model';

@Component({
    selector: 'fuse-notes-note-form-dialog',
    templateUrl: './note-form.component.html',
    styleUrls: ['./note-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseNotesNoteFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    noteForm: FormGroup;
    action: string;
    note: Note;

    constructor(
        public dialogRef: MdDialogRef<FuseNotesNoteFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Bus Type';
            this.note = data.note;
        }
        else {
            this.dialogTitle = 'New Bus Type';
            this.note = {
                id: '',
                rev: '',
                note: '',
                user_type: '',
                user_id: '',
                color: '',
                timestamp_create: '',
                timestamp_last_update: ''
            }
        }

        this.noteForm = this.createNoteForm();
    }

    ngOnInit() {
    }

    createNoteForm() {
        return this.formBuilder.group({
            id: [this.note.id],
            rev: [this.note.rev],
            note: [this.note.note],
            user_type: [this.note.user_type],
            user_id: [this.note.user_id],
            color: [this.note.color],
            timestamp_create: [this.note.timestamp_create],
            timestamp_last_update: [this.note.timestamp_last_update]

        });
    }
}
