import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NotesService } from '../notes.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject} from 'rxjs/Rx';
import { FuseNotesNoteFormDialogComponent } from '../note-form/note-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Note } from '../note.model';
import {PouchService} from '../../../../../provider/pouch-service';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';

@Component({
    selector   : 'fuse-notes-note-list',
    templateUrl: './note-list.component.html',
    styleUrls  : ['./note-list.component.scss']
})
export class FuseNotesNoteListComponent implements OnInit
{
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public notes: Array<Note> = [];
    notes2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'note', 'buttons'];
    selectedNotes: any[];
    checkboxes: {};

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;

    constructor(public dialog: MdDialog, public db:PouchService)
    { }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        this._loadNotes();
    }

    private _loadNotes(): void {
         this.db.onNotesChanged.subscribe(data =>{
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        this.db.getNotes()
        .then((notes: Array<Note>) => {
            this.notes = notes;
            this.notes2= new BehaviorSubject<any>(notes);
            //console.log(this.notes2);

            this.checkboxes = {};
            notes.map(note => {
                this.checkboxes[note.id] = false;
            });
             this.db.onSelectedNotesChanged.subscribe(selectedNotes => {
                for ( const id in this.checkboxes )
                {
                    this.checkboxes[id] = selectedNotes.includes(id);
                }

                this.selectedNotes = selectedNotes;
            });
        this.db.onSearchTextChanged.subscribe(data =>{
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        });
        
    }

    newNote()
    {
        this.dialogRef = this.dialog.open(FuseNotesNoteFormDialogComponent, {
            panelClass: 'note-form-dialog',
            data      : {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if ( !response )
                {
                    return;
                }

                this.db.saveNote(response.getRawValue());
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

            });

    }

    editNote(note)
    {
        this.dialogRef = this.dialog.open(FuseNotesNoteFormDialogComponent, {
            panelClass: 'note-form-dialog',
            data      : {
                note: note,
                action : 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch ( actionType )
                {
                    /**
                     * Save
                     */
                    case 'save':

                        this.db.updateNote(formData.getRawValue());
                        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteNote(note);

                        break;
                }
            });
    }

    /**
     * Delete note
     */
    deleteNote(note)
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.db.deleteNote(note);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(noteId)
    {
        this.db.toggleSelectedNote(noteId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    notes2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort)
    {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]>
    {
        const displayDataChanges = [
            of(this.db.notes).delay(5000),
            this.sort.mdSortChange,
            this._paginator.page,
        ];

        var result = Observable.fromPromise(this.db.getNotes());
       /*  result.subscribe(x => console.log(x), e => console.error(e));
        return result;  */   

        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.notes]));
        });
    }

    disconnect()
    {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
          return data;
        }
    
        return data.sort((a, b) => {
          const isAsc = this.sort.direction === 'asc';
          switch (this.sort.active) {
            case 'name': return compare(a.name, b.name, isAsc);
            case 'amount': return compare(+a.amount, +b.amount, isAsc);
            case 'id': return compare(+a.id, +b.id, isAsc);
            default: return 0;
          }
        });
      }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }