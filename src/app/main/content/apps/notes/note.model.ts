export interface Note {
    id: string,
    rev: string,
    note: string,
    user_type: string,
    user_id: string,
    color: string,
    timestamp_create: string,
    timestamp_last_update: string
}

