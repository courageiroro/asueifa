import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NotesService } from './notes.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-notes',
    templateUrl  : './notes.component.html',
    styleUrls    : ['./notes.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FuseNotesComponent implements OnInit
{
    hasSelectedNotes: boolean;
    searchInput: FormControl;

    constructor(private notesService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.notesService.onSelectedNotesChanged
            .subscribe(selectedNotes => {
                this.hasSelectedNotes = selectedNotes.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.notesService.onSearchTextChanged.next(searchText);
            });
    }

}
