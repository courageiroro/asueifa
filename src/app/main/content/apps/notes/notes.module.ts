import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseNotesComponent } from './notes.component';
import { NotesService } from './notes.service';
import { FuseNotesNoteListComponent } from './note-list/note-list.component';
import { FuseNotesSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseNotesNoteFormDialogComponent } from './note-form/note-form.component';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/notes',
        component: FuseNotesComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            notes: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseNotesComponent,
        FuseNotesNoteListComponent,
        FuseNotesSelectedBarComponent,
        FuseNotesNoteFormDialogComponent
    ],
    providers      : [
        AuthGuard,
        PouchService,
        NotesService
    ],
    entryComponents: [FuseNotesNoteFormDialogComponent]
})
export class FuseNotesModule
{
}
