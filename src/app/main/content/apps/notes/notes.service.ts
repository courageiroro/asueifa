/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Note } from './note.model';
import { Schema } from '../schema';

@Injectable()
export class NotesService {
    public notename = "";
    public sCredentials;

    onNotesChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedNotesChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    notes: Note[];
    user: any;
    selectedNotes: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The notes App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getNotes()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getNotes();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getNotes();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('asueifai', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/asueifai';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/asueifai';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }

    /***********
     * note
     **********/
    /**
     * Save a note
     * @param {note} note
     *
     * @return Promise<note>
     */
    saveNote(note: Note): Promise<Note> {
        note.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('note', note)
            .then((data: any) => {

                if (data && data.notes && data.notes
                [0]) {
                    return data.notes[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a note
    * @param {note} note
    *
    * @return Promise<note>
    */
    updateNote(note: Note) {

        return this.db.rel.save('note', note)
            .then((data: any) => {
                if (data && data.notes && data.notes
                [0]) {
                    return data.notes[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a note
     * @param {note} note
     *
     * @return Promise<boolean>
     */
    removeNote(note: Note): Promise<boolean> {
        return this.db.rel.del('note', note)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the notes
     *
     * @return Promise<Array<note>>
     */
    getNotes(): Promise<Array<Note>> {
        return this.db.rel.find('note')
            .then((data: any) => {
                this.notes = data.notes;
                if (this.searchText && this.searchText !== '') {
                    this.notes = FuseUtils.filterArrayByString(this.notes, this.searchText);
                }
                //this.onnotesChanged.next(this.notes);
                return Promise.resolve(this.notes);
                //return data.notes ? data.notes : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a note
     * @param {note} note
     *
     * @return Promise<note>
     */
    getNote(note: Note): Promise<Note> {
        return this.db.rel.find('note', note.id)
            .then((data: any) => {
                return data && data.notes ? data.notes[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected note by id
     * @param id
     */
    toggleSelectedNote(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedNotes.length > 0) {
            const index = this.selectedNotes.indexOf(id);

            if (index !== -1) {
                this.selectedNotes.splice(index, 1);

                // Trigger the next event
                this.onSelectedNotesChanged.next(this.selectedNotes);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedNotes.push(id);

        // Trigger the next event
        this.onSelectedNotesChanged.next(this.selectedNotes);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedNotes.length > 0) {
            this.deselectNotes();
        }
        else {
            this.selectNotes();
        }
    }

    selectNotes(filterParameter?, filterValue?) {
        this.selectedNotes = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedNotes = [];
            this.notes.map(note => {
                this.selectedNotes.push(note.id);
            });
        }
        else {
            /* this.selectednotes.push(...
                 this.notes.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedNotesChanged.next(this.selectedNotes);
    }





    deselectNotes() {
        this.selectedNotes = [];

        // Trigger the next event
        this.onSelectedNotesChanged.next(this.selectedNotes);
    }

    deleteNote(note) {
        this.db.rel.del('note', note)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const noteIndex = this.notes.indexOf(note);
                this.notes.splice(noteIndex, 1);
                this.onNotesChanged.next(this.notes);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedNotes() {
        for (const noteId of this.selectedNotes) {
            const note = this.notes.find(_note => {
                return _note.id === noteId;
            });

            this.db.rel.del('note', note)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const noteIndex = this.notes.indexOf(note);
                    this.notes.splice(noteIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onNotesChanged.next(this.notes);
        this.deselectNotes();
    }
}
