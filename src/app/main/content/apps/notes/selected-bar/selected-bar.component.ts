import { Component, OnInit } from '@angular/core';
import { NotesService } from '../notes.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseNotesSelectedBarComponent implements OnInit
{
    selectedNotes: string[];
    hasSelectedNotes: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private notesService: PouchService,
        public dialog: MdDialog
    )
    {
        this.notesService.onSelectedNotesChanged
            .subscribe(selectedNotes => {
                this.selectedNotes = selectedNotes;
                setTimeout(() => {
                    this.hasSelectedNotes = selectedNotes.length > 0;
                    this.isIndeterminate = (selectedNotes.length !== this.notesService.notes.length && selectedNotes.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.notesService.selectNotes();
    }

    deselectAll()
    {
        this.notesService.deselectNotes();
    }

    deleteSelectedNotes()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected notees?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.notesService.deleteSelectedNotes();
            }
            this.confirmDialogRef = null;
        });
    }

}
