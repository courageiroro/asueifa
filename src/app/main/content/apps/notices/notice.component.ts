import { startOfDay, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth, addHours } from 'date-fns';
import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseNoticesNoticeFormDialogComponent } from './notice-form/notice-form.component';
import { FormGroup } from '@angular/forms';
import { Notice } from './notice.model';
import { NoticesService } from './notices.service';
import {PouchService} from './../../../../provider/pouch-service';
import {
    CalendarEvent,
    CalendarEventAction,
    CalendarEventTimesChangedEvent,
    CalendarMonthViewDay
} from 'angular-calendar';
import { FuseConfirmDialogComponent } from '../../../../core/components/confirm-dialog/confirm-dialog.component';

@Component({
    selector: 'fuse-notice',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './notice.component.html',
    styleUrls: ['./notice.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class FuseNoticeComponent implements OnInit {
    localStorageItem;
    localStorageName;
    title;

    view: string;

    viewDate: Date;
    getNotices: any;
    notice: Notice;

    noticess: CalendarEvent[];

    public actions: CalendarEventAction[];

    activeDayIsOpen: boolean;

    refresh: Subject<any> = new Subject();

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    selectedDay: any;

    constructor(
        public dialog: MdDialog,
        public noticeService: PouchService
    ) {
        /*  this.noticeService.getNotices().then(res=>{
            this.getNotices = res;
        }) */

        this.view = 'month';
        this.viewDate = new Date();
        this.activeDayIsOpen = true;
        this.selectedDay = { date: startOfDay(new Date()) };

        this.actions = [
            {
                label: '<i class="material-icons s-16">edit</i>',
                onClick: ({ event }: { event: CalendarEvent }): void => {
                    this.editNotice('edit', event);
                }
            },
            {
                label: '<i class="material-icons s-16">delete</i>',
                onClick: ({ event }: { event: CalendarEvent }): void => {
                    this.deleteNotice(event);
                }
            }
        ];

        /**
         * Get events from service/server
         */
        this.setNotices();
    }

    ngOnInit() {
        /**
         * Watch re-render-refresh for updating db
         */
        this.refresh.subscribe(updateDB => {
            //console.warn('REFRESH');
            if (updateDB) {
                //console.warn('UPDATE DB');
                this.noticeService.updateNotice(this.notice);
            }
        });

        this.noticeService.onEventsUpdated.subscribe(events => {
            this.setNotices();
            this.refresh.next();

        });

        this.localStorageItem = JSON.parse(localStorage.getItem('user'));
        this.localStorageName = this.localStorageItem.usertype;
        if (this.localStorageName === 'Doctor') {
            this.title = "Appointments";
        }
        else{
            this.title = "Calendar"
        }
    }


    setNotices() {
        
        this.noticess = this.noticeService.notices.map(item => {
            item.actions = this.actions;
            
            return new Notice(item);
        });
    }

    /**
     * Before View Renderer
     * @param {any} header
     * @param {any} body
     */
    beforeMonthViewRender({ header, body }) {
        // console.info('beforeMonthViewRender');
        /**
         * Get the selected day
         */
        const _selectedDay = body.find((_day) => {
            return _day.date.getTime() === this.selectedDay.date.getTime();
        });

        if (_selectedDay) {
            /**
             * Set selectedday style
             * @type {string}
             */
            _selectedDay.cssClass = 'mat-elevation-z3';
        }

    }

    /**
     * Day clicked
     * @param {MonthViewDay} day
     */
    dayClicked(day: CalendarMonthViewDay): void {
        const date: Date = day.date;
        const events: CalendarEvent[] = day.events;

        if (isSameMonth(date, this.viewDate)) {
            if ((isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) || events.length === 0) {
                this.activeDayIsOpen = false;
            }
            else {
                this.activeDayIsOpen = true;
                this.viewDate = date;
            }
        }
        this.selectedDay = day;
        this.refresh.next();
    }

    /**
     * Event times changed
     * Event dropped or resized
     * @param {CalendarEvent} event
     * @param {Date} newStart
     * @param {Date} newEnd
     */
    eventTimesChanged({ event, newStart, newEnd }: CalendarEventTimesChangedEvent): void {
        event.start = newStart;
        event.end = newEnd;
        // console.warn('Dropped or resized', event);
        this.refresh.next(true);
    }

    /**
     * Delete Event
     * @param event
     */
    deleteNotice(notice) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.noticeService.deleteNotice(notice);
                const noticeIndex = this.noticess.indexOf(notice);
                this.noticess.splice(noticeIndex, 1);
                this.refresh.next(true);
            }
            this.confirmDialogRef = null;
        });

    }

    /**
     * Edit Event
     * @param {string} action
     * @param {CalendarEvent} event
     */
    editNotice(action: string, notices: CalendarEvent) {
        const eventIndex = this.noticess.indexOf(notices);
        
        this.dialogRef = this.dialog.open(FuseNoticesNoticeFormDialogComponent, {
            panelClass: 'event-form-dialog',
            data: {
                notices: notices,
                action: action

            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':
                        this.noticess[eventIndex] = Object.assign(this.noticess[eventIndex], formData.getRawValue());
                        this.noticeService.updateNotices(formData.getRawValue());
                        this.refresh.next(true);
                       

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteNotice(event);

                        break;
                }
            });
    }

    /**
     * Add Event
     */
    addNotice(): void {
        this.dialogRef = this.dialog.open(FuseNoticesNoticeFormDialogComponent, {
            panelClass: 'event-form-dialog',
            data: {
                action: 'new',
                date: this.selectedDay.date
            }
        });
        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
                const newNotice = response.getRawValue();
                /*  newNotice.actions = this.actions; */
                this.noticess.push(newNotice);
                this.noticeService.saveNotice(newNotice);
                
                this.refresh.next(true);
            });
    }
}


