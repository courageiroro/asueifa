import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseNoticeComponent } from './notice.component';
import { NoticesService } from './notices.service';
import { FuseNoticesNoticeFormDialogComponent } from './notice-form/notice-form.component';
//import { FuseNoticesNoticeListComponent } from './notice-list/notice-list.component';
import { FuseNoticesSelectedBarComponent } from './selected-bar/selected-bar.component';
//import { FuseNoticesNoticeFormDialogComponent } from './notice-form/notice-form.component';
import { CalendarModule } from 'angular-calendar';
import {ColorPickerModule} from 'angular4-color-picker';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/notices',
        component: FuseNoticeComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            notices: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes),
        CalendarModule.forRoot(),
        ColorPickerModule
    ],
    declarations   : [
        FuseNoticesNoticeFormDialogComponent,
        FuseNoticeComponent,
        FuseNoticesSelectedBarComponent,
        FuseNoticesNoticeFormDialogComponent
    ],
    providers      : [
        AuthGuard,
        PouchService,
        NoticesService
    ],
    entryComponents: [FuseNoticesNoticeFormDialogComponent]
})
export class FuseNoticesModule
{
}
