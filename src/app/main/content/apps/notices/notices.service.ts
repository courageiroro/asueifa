/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Notice } from './notice.model';
import { Schema } from '../schema';

@Injectable()
export class NoticesService {
    public noticename = "";
    noticess: any;
    onNoticesChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedNoticesChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();
    public sCredentials;
    notices: Notice[];
    user: any;
    selectedNotices: string[] = [];
    onEventsUpdated = new Subject<any>();
    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The notices App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getNotices()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getNotices();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getNotices();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('asueifai', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/asueifai';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/asueifai';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }


    /***********
     * notice
     **********/
    /**
     * Save a notice
     * @param {notice} notice
     *
     * @return Promise<notice>
     */
    saveNotice(notice: Notice): Promise<Notice> {
        notice.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('notice', notice)
            .then((data: any) => {

                if (data && data.notices && data.notices
                [0]) {
                    
                    return data.notices[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a notice
    * @param {notice} notice
    *
    * @return Promise<notice>
    */
    updateNotice(notice: Notice) {

        return this.db.rel.save('notice', notice)
            .then((data: any) => {
                if (data && data.notices && data.notices
                [0]) {
                    
                    return data.notices[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    updateNotices(notice: Promise<Array<Notice>>) {

        return this.db.rel.save('notice', notice)
            .then((data: any) => {
                if (data && data.notices && data.notices
                [0]) {
                    return data.notices[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a notice
     * @param {notice} notice
     *
     * @return Promise<boolean>
     */
    removeNotice(notice: Notice): Promise<boolean> {
        return this.db.rel.del('notice', notice)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the notices
     *
     * @return Promise<Array<notice>>
     */
    getNotices(): Promise<Array<Notice>> {
        return this.db.rel.find('notice')
            .then((data: any) => {
                this.notices = data.notices;
                
                this.onEventsUpdated.next(this.notices);
                if (this.searchText && this.searchText !== '') {
                    this.notices = FuseUtils.filterArrayByString(this.notices, this.searchText);
                }
                //this.onnoticesChanged.next(this.notices);
                return Promise.resolve(this.notices);
                //return data.notices ? data.notices : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Read a notice
     * @param {notice} notice
     *
     * @return Promise<notice>
     */
    getNotice(notice: Notice): Promise<Notice> {
        return this.db.rel.find('notice', notice.id)
            .then((data: any) => {
                //this.onEventsUpdated.next(this.notices);
                return data && data.notices ? data.notices[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected notice by id
     * @param id
     */
    toggleSelectedNotice(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedNotices.length > 0) {
            const index = this.selectedNotices.indexOf(id);

            if (index !== -1) {
                this.selectedNotices.splice(index, 1);

                // Trigger the next event
                this.onSelectedNoticesChanged.next(this.selectedNotices);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedNotices.push(id);

        // Trigger the next event
        this.onSelectedNoticesChanged.next(this.selectedNotices);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedNotices.length > 0) {
            this.deselectNotices();
        }
        else {
            this.selectNotices();
        }
    }

    selectNotices(filterParameter?, filterValue?) {
        this.selectedNotices = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedNotices = [];
            this.notices.map(notice => {
                this.selectedNotices.push(notice.id);
            });
        }
        else {
            /* this.selectednotices.push(...
                 this.notices.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedNoticesChanged.next(this.selectedNotices);
    }





    deselectNotices() {
        this.selectedNotices = [];

        // Trigger the next event
        this.onSelectedNoticesChanged.next(this.selectedNotices);
    }

    deleteNotice(notice) {
        this.db.rel.del('notice', notice)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const noticeIndex = this.notices.indexOf(notice);
                this.notices.splice(noticeIndex, 1);
                this.onNoticesChanged.next(this.notices);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedNotices() {
        for (const noticeId of this.selectedNotices) {
            const notice = this.notices.find(_notice => {
                return _notice.id === noticeId;
            });

            this.db.rel.del('notice', notice)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const noticeIndex = this.notices.indexOf(notice);
                    this.notices.splice(noticeIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onNoticesChanged.next(this.notices);
        this.deselectNotices();
    }
}
