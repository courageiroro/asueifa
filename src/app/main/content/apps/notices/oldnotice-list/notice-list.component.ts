/* import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NoticesService } from '../notices.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject} from 'rxjs/Rx';
import { FuseNoticesNoticeFormDialogComponent } from '../oldnotice-form/notice-form.component';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Notice } from '../notice.model';

@Component({
    selector   : 'fuse-notices-notice-list',
    templateUrl: './notice-list.component.html',
    styleUrls  : ['./notice-list.component.scss']
})
export class FuseNoticesNoticeListComponent implements OnInit
{
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public notices: Array<Notice> = [];
    notices2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'title','description','start','end', 'buttons'];
    selectedNotices: any[];
    checkboxes: {};

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(public dialog: MdDialog, public db:NoticesService)
    { }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db);
        this.db.initDB();
        this._loadNotices();
    }

    private _loadNotices(): void {
         this.db.onNoticesChanged.subscribe(data =>{
            this.dataSource = new FilesDataSource(this.db);
        })
        this.db.getNotices()
        .then((notices: Array<Notice>) => {
            this.notices = notices;
            this.notices2= new BehaviorSubject<any>(notices);

            this.checkboxes = {};
            notices.map(notice => {
                this.checkboxes[notice.id] = false;
            });
             this.db.onSelectedNoticesChanged.subscribe(selectedNotices => {
                for ( const id in this.checkboxes )
                {
                    this.checkboxes[id] = selectedNotices.includes(id);
                }

                this.selectedNotices = selectedNotices;
            });
        this.db.onSearchTextChanged.subscribe(data =>{
            this.dataSource = new FilesDataSource(this.db);
        })
        });
        
    } */

    /* newNotice()
    {
        this.dialogRef = this.dialog.open(FuseNoticesNoticeFormDialogComponent, {
            panelClass: 'notice-form-dialog',
            data      : {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if ( !response )
                {
                    return;
                }

                this.db.saveNotice(response.getRawValue());
                this.dataSource = new FilesDataSource(this.db);

            });

    }

    editNotice(notice)
    {
        this.dialogRef = this.dialog.open(FuseNoticesNoticeFormDialogComponent, {
            panelClass: 'notice-form-dialog',
            data      : {
                notice: notice,
                action : 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch ( actionType )
                { */
                    /**
                     * Save
                     */
                   /*  case 'save':

                        this.db.updateNotice(formData.getRawValue());
                        this.dataSource = new FilesDataSource(this.db);

                        break; */
                    /**
                     * Delete
                     */
                /*     case 'delete':

                        this.deleteNotice(notice);

                        break;
                }
            });
    } */

    /**
     * Delete notice
     */
   /*  deleteNotice(notice)
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.db.deleteNotice(notice);
                this.dataSource = new FilesDataSource(this.db);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(noticeId)
    {
        this.db.toggleSelectedNotice(noticeId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    notices2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: NoticesService)
    {
        super();
    }
 */
    /** Connect function called by the table to retrieve one stream containing the data to render. */
   /*  connect(): Observable<any[]>
    {
        var result = Observable.fromPromise(this.db.getNotices());
        result.subscribe(x => console.log(x), e => console.error(e));
        return result;    
    }

    disconnect()
    {
    }
}
 */