import { Component, OnInit } from '@angular/core';
import { NoticesService } from '../notices.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseNoticesSelectedBarComponent implements OnInit
{
    selectedNotices: string[];
    hasSelectedNotices: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private noticesService: PouchService,
        public dialog: MdDialog
    )
    {
        this.noticesService.onSelectedNoticesChanged
            .subscribe(selectedNotices => {
                this.selectedNotices = selectedNotices;
                setTimeout(() => {
                    this.hasSelectedNotices = selectedNotices.length > 0;
                    this.isIndeterminate = (selectedNotices.length !== this.noticesService.notices.length && selectedNotices.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.noticesService.selectNotices();
    }

    deselectAll()
    {
        this.noticesService.deselectNotices();
    }

    deleteSelectedNotices()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected noticees?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.noticesService.deleteSelectedNotices();
            }
            this.confirmDialogRef = null;
        });
    }

}
