import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Nurse } from '../nurse.model';

@Component({
    selector: 'fuse-nurses-nurse-form-dialog',
    templateUrl: './nurse-form.component.html',
    styleUrls: ['./nurse-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseNursesNurseFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    nurseForm: FormGroup;
    action: string;
    nurse: Nurse;

    constructor(
        public dialogRef: MdDialogRef<FuseNursesNurseFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Nurse';
            this.nurse = data.nurse;
        }
        else {
            this.dialogTitle = 'New Nurse';
            this.nurse = {
                id: '',
                rev: '',
                name: '',
                email: '',
                password: '',
                address: '',
                phone: '',
            }
        }

        this.nurseForm = this.createnurseForm();
    }

    ngOnInit() {
    }

    createnurseForm() {
        return this.formBuilder.group({
            nurse_id: [this.nurse.id],
            rev: [this.nurse.rev],
            name: [this.nurse.name],
            email: [this.nurse.email],
            password: [this.nurse.password],
            address: [this.nurse.address],
            phone: [this.nurse.phone],

        });
    }
}
