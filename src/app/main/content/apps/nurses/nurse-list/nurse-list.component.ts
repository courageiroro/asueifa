import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NursesService } from '../nurses.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { FuseNursesNurseFormDialogComponent } from '../nurse-form/nurse-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Nurse } from '../nurse.model';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {PouchService} from '../../../../../provider/pouch-service';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';


@Component({
    selector: 'fuse-nurses-nurse-list',
    templateUrl: './nurse-list.component.html',
    styleUrls: ['./nurse-list.component.scss']
})
export class FuseNursesNurseListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public nurses: Array<Nurse> = [];
    nurses2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'name', 'email', 'password', 'phone', 'address', 'buttons'];
    selectedNurses: any[];
    checkboxes: {};
    //public x;

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;

    constructor(public dialog: MdDialog, private route: ActivatedRoute, public db: PouchService) {

    }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        this._loadNurses();
        //this.x = this.route.snapshot.params['x'];
        //console.log(this.x)

    }

    private _loadNurses(): void {
        this.db.onNursesChanged.subscribe(data => {
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        this.db.getNurses()
            .then((nurses: Array<Nurse>) => {
                this.nurses = nurses;
                this.nurses2 = new BehaviorSubject<any>(nurses);
                //console.log(this.nurses2);

                this.checkboxes = {};
                nurses.map(nurse => {
                    this.checkboxes[nurse.id] = false;
                });
                this.db.onSelectedNursesChanged.subscribe(selectedNurses => {
                    for (const id in this.checkboxes) {
                        this.checkboxes[id] = selectedNurses.includes(id);
                    }

                    this.selectedNurses = selectedNurses;
                });
                this.db.onSearchTextChanged.subscribe(data => {
                    this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                })
            });

    }

    newNurse() {
        this.dialogRef = this.dialog.open(FuseNursesNurseFormDialogComponent, {
            panelClass: 'nurse-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }

                this.db.saveNurse(response.getRawValue());
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

            });
       //console.log(this.x);
    }

    editNurse(nurse) {
        this.dialogRef = this.dialog.open(FuseNursesNurseFormDialogComponent, {
            panelClass: 'nurse-form-dialog',
            data: {
                nurse: nurse,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':

                        this.db.updateNurse(formData.getRawValue());
                        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteNurse(nurse);

                        break;
                }
            });
    }

    /**
     * Delete nurse
     */
    deleteNurse(nurse) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deleteNurse(nurse);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(nurseId) {
        this.db.toggleSelectedNurse(nurseId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    nurses2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            of(this.db.nurses).delay(5000),
            this.sort.mdSortChange,
            this._paginator.page,
        ];

        var result = Observable.fromPromise(this.db.getNurses());
       /*  result.subscribe(x => console.log(x), e => console.error(e));
        return result; */

        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.nurses]));
        });
    }

    disconnect() {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
          return data;
        }
    
        return data.sort((a, b) => {
          const isAsc = this.sort.direction === 'asc';
          switch (this.sort.active) {
            case 'name': return compare(a.name, b.name, isAsc);
            case 'amount': return compare(+a.amount, +b.amount, isAsc);
            case 'id': return compare(+a.id, +b.id, isAsc);
            default: return 0;
          }
        });
      }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
