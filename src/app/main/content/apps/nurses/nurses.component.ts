import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NursesService } from './nurses.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import { InvoicesService } from './../invoices/invoices.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-nurses',
    templateUrl  : './nurses.component.html',
    styleUrls    : ['./nurses.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FuseNursesComponent implements OnInit
{
    hasSelectedNurses: boolean;
    searchInput: FormControl;
    public myName;

    constructor(private nursesService: PouchService)
    {   this.myName = nursesService.getData();
        
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {
     
        this.nursesService.onSelectedNursesChanged
            .subscribe(selectedNurses => {
                this.hasSelectedNurses = selectedNurses.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.nursesService.onSearchTextChanged.next(searchText);
            });
    }

}
