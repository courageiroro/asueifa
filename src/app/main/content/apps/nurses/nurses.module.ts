import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseNursesComponent } from './nurses.component';
import { NursesService } from './nurses.service';
import { FuseNursesNurseListComponent } from './nurse-list/nurse-list.component';
import { FuseNursesSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseNursesNurseFormDialogComponent } from './nurse-form/nurse-form.component';
import { InvoicesService } from './../invoices/invoices.service';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/nurses',
        component: FuseNursesComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            nurses: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseNursesComponent,
        FuseNursesNurseListComponent,
        FuseNursesSelectedBarComponent,
        FuseNursesNurseFormDialogComponent
    ],
    providers      : [
        NursesService,
        PouchService,
        AuthGuard,
        InvoicesService
    ],
    entryComponents: [FuseNursesNurseFormDialogComponent]
})
export class FuseNursesModule
{
}
