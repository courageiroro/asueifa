/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Nurse } from './nurse.model';
import { Schema } from '../schema';

@Injectable()
export class NursesService {
    public name = "";
    public sCredentials;
    onNursesChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedNursesChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    nurses: Nurse[];
    user: any;
    selectedNurses: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The nurses App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getNurses()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getNurses();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getNurses();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('asueifai', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/asueifai';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/asueifai';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }

    /***********
     * nurse
     **********/
    /**
     * Save a nurse
     * @param {nurse} nurse
     *
     * @return Promise<nurse>
     */
    saveNurse(nurse: Nurse): Promise<Nurse> {
        nurse.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('nurse', nurse)
            .then((data: any) => {

                if (data && data.nurses && data.nurses
                [0]) {
                    return data.nurses[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a nurse
    * @param {nurse} nurse
    *
    * @return Promise<nurse>
    */
    updateNurse(nurse: Nurse) {

        return this.db.rel.save('nurse', nurse)
            .then((data: any) => {
                if (data && data.nurses && data.nurses
                [0]) {
                    return data.nurses[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a nurse
     * @param {nurse} nurse
     *
     * @return Promise<boolean>
     */
    removeNurse(nurse: Nurse): Promise<boolean> {
        return this.db.rel.del('nurse', nurse)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the nurses
     *
     * @return Promise<Array<nurse>>
     */
    getNurses(): Promise<Array<Nurse>> {
        return this.db.rel.find('nurse')
            .then((data: any) => {
                this.nurses = data.nurses;
                if (this.searchText && this.searchText !== '') {
                    this.nurses = FuseUtils.filterArrayByString(this.nurses, this.searchText);
                }
                //this.onnursesChanged.next(this.nurses);
                return Promise.resolve(this.nurses);
                //return data.nurses ? data.nurses : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a nurse
     * @param {nurse} nurse
     *
     * @return Promise<nurse>
     */
    getNurse(nurse: Nurse): Promise<Nurse> {
        return this.db.rel.find('nurse', nurse.id)
            .then((data: any) => {
                return data && data.nurses ? data.nurses[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected nurse by id
     * @param id
     */
    toggleSelectedNurse(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedNurses.length > 0) {
            const index = this.selectedNurses.indexOf(id);

            if (index !== -1) {
                this.selectedNurses.splice(index, 1);

                // Trigger the next event
                this.onSelectedNursesChanged.next(this.selectedNurses);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedNurses.push(id);

        // Trigger the next event
        this.onSelectedNursesChanged.next(this.selectedNurses);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedNurses.length > 0) {
            this.deselectNurses();
        }
        else {
            this.selectNurses();
        }
    }

    selectNurses(filterParameter?, filterValue?) {
        this.selectedNurses = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedNurses = [];
            this.nurses.map(nurse => {
                this.selectedNurses.push(nurse.id);
            });
        }
        else {
            /* this.selectednurses.push(...
                 this.nurses.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedNursesChanged.next(this.selectedNurses);
    }





    deselectNurses() {
        this.selectedNurses = [];

        // Trigger the next event
        this.onSelectedNursesChanged.next(this.selectedNurses);
    }

    deleteNurse(nurse) {
        this.db.rel.del('nurse', nurse)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const nurseIndex = this.nurses.indexOf(nurse);
                this.nurses.splice(nurseIndex, 1);
                this.onNursesChanged.next(this.nurses);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedNurses() {
        for (const nurseId of this.selectedNurses) {
            const nurse = this.nurses.find(_nurse => {
                return _nurse.id === nurseId;
            });

            this.db.rel.del('nurse', nurse)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const nurseIndex = this.nurses.indexOf(nurse);
                    this.nurses.splice(nurseIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onNursesChanged.next(this.nurses);
        this.deselectNurses();
    }
}
