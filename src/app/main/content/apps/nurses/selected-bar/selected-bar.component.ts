import { Component, OnInit } from '@angular/core';
import { NursesService } from '../nurses.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseNursesSelectedBarComponent implements OnInit
{
    selectedNurses: string[];
    hasSelectedNurses: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private nursesService: PouchService,
        public dialog: MdDialog
    )
    {
        this.nursesService.onSelectedNursesChanged
            .subscribe(selectedNurses => {
                this.selectedNurses = selectedNurses;
                setTimeout(() => {
                    this.hasSelectedNurses = selectedNurses.length > 0;
                    this.isIndeterminate = (selectedNurses.length !== this.nursesService.nurses.length && selectedNurses.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.nursesService.selectNurses();
    }

    deselectAll()
    {
        this.nursesService.deselectNurses();
    }

    deleteSelectedNurses()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected nursees?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.nursesService.deleteSelectedNurses();
            }
            this.confirmDialogRef = null;
        });
    }

}
