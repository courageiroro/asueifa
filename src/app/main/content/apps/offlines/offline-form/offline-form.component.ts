import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Offline } from '../offline.model';
import { OfflinesService } from '../offlines.service';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-offlines-offline-form-dialog',
    templateUrl: './offline-form.component.html',
    styleUrls: ['./offline-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseOfflinesOfflineFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    offlineForm: FormGroup;
    action: string;
    offline: Offline;
    public categorys;
    public types;
    category;
   

    constructor(
        public dialogRef: MdDialogRef<FuseOfflinesOfflineFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        private db: PouchService,
        
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit offline';
            this.offline = data.offline;
        }
        else {
            this.dialogTitle = 'New offline';
            this.offline = {
                id: '',
                rev: '',               
                amount: null,
                product:'',
                type:'',
                method:'',
                date:new Date,
                expire: null,
                reference:'',
                status: false
            }
        }


        this.offlineForm = this.createofflineForm();
    }

    ngOnInit() {
        this.categorys = ['General', 'Medical History', 'Social History', 'Medical Conditions', 'Current Medications', 'Family History', 'Notes']
        this.types = ['input', 'text', 'checkbox']
    }


    createofflineForm() {
        return this.formBuilder.group({
            id: [this.offline.id],
            rev: [this.offline.rev],
            amount: [this.offline.amount],
            product: [this.offline.product],
            type: [this.offline.type],
            method: [this.offline.method],
            date: [this.offline.date],
            expire: [this.offline.expire],
            reference: [this.offline.reference],
            status: [this.offline.status]
        });
    }
}
