import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { OfflinesService } from '../offlines.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
//import { ConfigureofflinesService } from '../../configure_offlines/configure_offlines.service';
import { FuseOfflinesOfflineFormDialogComponent } from '../offline-form/offline-form.component';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Offline } from '../offline.model';
import { Subject } from 'rxjs/Subject';
import { Staff } from '../../staffs/staff.model';
//import { Configureoffline } from '../../configure_offlines/configure_offline.model';
import { Http, Headers, RequestOptions } from '@angular/http';
import { ExtendlicensesService } from '../../extendlicenses/extendlicenses.service';
import { DomSanitizer } from '@angular/platform-browser';
//import { Angular4paystack } from './angular4-paystack';
import { SettingsService } from '../../settings/settings.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Extendlicense } from '../../extendlicenses/extendlicense.model';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-offlines-offline-list',
    templateUrl: './offline-list.component.html',
    styleUrls: ['./offline-list.component.scss']
})
export class FuseOfflinesOfflineListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public offlines: Array<Offline> = [];
    //public configureofflines: Array<Configureoffline> = [];
    public staff: Staff;
    patients;
    patientName;
    patientAddress;
    patientPhone;
    patientEmail;
    settings;
    extendlicense: Extendlicense;
    settingsEmail;
    dbname: any;
    error = "";
    urloffline;
    public offline: Offline;
    image;
    checkboxstate;
    offlines2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'name', 'description', 'buttons'];
    selectedofflines: any[];
    online = true;
    pin:any;
    checkboxes: {};
    general = [];
    refresh: Subject<any> = new Subject();
    medicalhistory = [];
    socialhistory = [];
    medicalconditions = [];
    currentmedications = [];
    familyhistory = [];
    notes = [];
    offliness: Offline[];
    activate = false;
    reference: any;


    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(public dialog: MdDialog, public _DomSanitizer: DomSanitizer, public db: PouchService, public http: Http, public activateRoute: ActivatedRoute, public router: Router) {
        let id = this.activateRoute.snapshot.params['id'];
        


        this.db.getExtendlicenses().then(payments => {
            let unactivated = payments.filter(payment => payment.status != true);
            if (unactivated[0] != undefined) {
                this.extendlicense = unactivated[unactivated.length - 1];
                this.db.getSettings().then(settings => {
                    this.dbname = settings[0].databasename;
                });
                this.activate = true;
                this.reference = new Date(this.extendlicense.expire).getTime() - parseInt(this.extendlicense.id) + 1519011821;
                
                this.reference = this.encode(this.reference, '123');
                
            }
        });



    }

    encode(s, k) {
        var enc = "";
        var str = "";
        // make sure that input is string

        str = s.toString();
        for (var i = 0; i < str.length; i++) {
            // create block
            var a = str.charCodeAt(i);
            // bitwise XOR
            var b = a ^ k;
            enc = enc + String.fromCharCode(b);
        }
        return enc;
    }


    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db);
        this._loadofflines();


    }

    private _loadofflines(): void {

    }


    /*    onlineCheck() {
           this.online = true;
           this.http.get('https://sarutech.com/couchdblogin/auth/validate-username/xs2da2432252sda').subscribe(data => {
               this.online = true;
               console.log(data)
               console.log(this.online);
           },
               (err) => {
                   this.online = false;
                   console.log(this.online);
               });
       }
   
       click() {
           this.onlineCheck();
       } */

    newOffline() {
        this.dialogRef = this.dialog.open(FuseOfflinesOfflineFormDialogComponent, {
            panelClass: 'offline-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
                

                this.db.saveOffline(response.getRawValue());
                this.refresh.next(true);
                this.dataSource = new FilesDataSource(this.db);
                

            });

    }

    editOffline(offline) {
        
        this.dialogRef = this.dialog.open(FuseOfflinesOfflineFormDialogComponent, {
            panelClass: 'offline-form-dialog',
            data: {
                offline: offline,
                action: 'edit'

            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':

                        this.db.updateOffline(formData.getRawValue());
                        this.dataSource = new FilesDataSource(this.db);
                        
                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteOffline(offline);

                        break;
                }
            });
    }

    /**
     * Delete offline
     */
    deleteOffline(offline) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deleteOffline(offline);
                this.dataSource = new FilesDataSource(this.db);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(offlineId) {
        this.db.toggleSelectedOffline(offlineId);
    }


    submit() {
         if(new Date(this.extendlicense.expire).getTime() - parseInt(this.extendlicense.id) == this.pin - 20050308){
            this.extendlicense.status = true;
            this.extendlicense.reference = this.reference;
            this.db.updateExtendlicense(this.extendlicense).then(res =>{
                this.db.extendLicense(this.extendlicense.expire,this.extendlicense.product);
            
                this.router.navigate(['apps/managepayment']);
            });
        }
        else{
            alert('failed')
        }
    }

     extend(): void {
        this.router.navigate(['apps/extendlicense'])
    }

    }

export class FilesDataSource extends DataSource<any>
{
    offlines2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService) {
        super();
    }

    //Connect function called by the table to retrieve one stream containing the data to render.
    connect(): Observable<any[]> {
        var result = Observable.fromPromise(this.db.getOfflines());
        result.subscribe(x => console.log(x), e => console.error(e));
        return result;
    }

    disconnect() {
    }
}
