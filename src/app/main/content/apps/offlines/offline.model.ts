export interface Offline {
    id: string,
    rev: string,
    amount: number,
    product: string,
    type: string,
    method: string,
    date: any,
    expire: any,
    reference: string,
    status: boolean
}

