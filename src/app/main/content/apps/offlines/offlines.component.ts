import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { OfflinesService } from './offlines.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector: 'fuse-offlines',
    templateUrl: './offlines.component.html',
    styleUrls: ['./offlines.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [Animations.slideInTop]
})
export class FuseOfflinesComponent implements OnInit {
    hasSelectedOfflines: boolean;
    searchInput: FormControl;

    constructor(private offlinesService: PouchService) {
        this.searchInput = new FormControl('');
       
    }

    ngOnInit() {

        this.offlinesService.onSelectedOfflinesChanged
            .subscribe(selectedofflines => {
                this.hasSelectedOfflines = selectedofflines.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.offlinesService.onSearchTextChanged.next(searchText);
            });
    }

}
