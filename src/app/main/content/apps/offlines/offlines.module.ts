import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { Angular4PaystackModule } from 'angular4-paystack';
import { FuseOfflinesComponent } from './offlines.component';
import { OfflinesService } from './offlines.service';
//import { UserService } from '../../../../user.service';
//import { ConfigureofflinesService } from './../configure_offlines/configure_offlines.service';
import { FuseOfflinesOfflineListComponent } from './offline-list/offline-list.component';
import { FuseOfflinesSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseOfflinesOfflineFormDialogComponent } from './offline-form/offline-form.component';
import { StaffsService } from './../staffs/staffs.service';
import { ExtendlicensesService } from './../extendlicenses/extendlicenses.service';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/offline',
        component: FuseOfflinesComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            offlines: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes),
        Angular4PaystackModule
    ],
    declarations   : [
        FuseOfflinesComponent,
        FuseOfflinesOfflineListComponent,
        FuseOfflinesSelectedBarComponent,
        FuseOfflinesOfflineFormDialogComponent
    ],
    providers      : [
        OfflinesService,
        StaffsService,
        AuthGuard,
        PouchService,
        ExtendlicensesService
        //ConfigureofflinesService
       //UserService
    ],
    entryComponents: [FuseOfflinesOfflineFormDialogComponent]
})
export class FuseOfflinesModule
{
}
