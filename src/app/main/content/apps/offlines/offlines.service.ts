/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import { Setting } from 'app/main/content/apps/settings/setting.model';
declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Offline } from './offline.model';
import { Schema } from '../schema';

@Injectable()
export class OfflinesService {
    public offline = "";
    public sCredentials;
    onOfflinesChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedOfflinesChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    offlines: Offline[];
    user: any;
    selectedOfflines: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;
    settings: Setting[];
    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The offlines App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getOfflines()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getOfflines();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getOfflines();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('asueifai', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/asueifai';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/asueifai';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }


    /***********
     * offline
     **********/
    /**
     * Save a offline
     * @param {offline} offline
     *
     * @return Promise<offline>
     */
    saveOffline(offline: Offline): Promise<Offline> {
        offline.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('offline', offline)
            .then((data: any) => {

                if (data && data.offlines && data.offlines
                [0]) {
                    console.log('save');
                    
                    return data.offlines[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    Offline(date,plan){
        
        this.getSettings().then(settings=>{
            settings[0].expiredate = new Date(date).toUTCString();
            settings[0].plans.push(plan);
            
            this.updateSetting(settings[0]);
        });
    }

      /**
     * Return all the settings
     *
     * @return Promise<Array<setting>>
     */
    getSettings(): Promise<Array<Setting>> {
        return this.db.rel.find('settings')
            .then((data: any) => {
                this.settings = data.settings;
                if (this.searchText && this.searchText !== '') {
                    this.settings = FuseUtils.filterArrayByString(this.settings, this.searchText);
                }
                //this.onSettingsChanged.next(this.settings);
                return Promise.resolve(this.settings);
                //return data.settings ? data.settings : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

      /**
    * Update a setting
    * @param {setting} Setting
    *
    * @return Promise<setting>
    */
    updateSetting(setting: Setting) {

        return this.db.rel.save('settings', setting)
            .then((data: any) => {
                if (data && data.settings && data.settings
                [0]) {
                    return data.settings[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a offline
    * @param {offline} offline
    *
    * @return Promise<offline>
    */
    updateOffline(offline: Offline) {
        return this.db.rel.save('offline', offline)
            .then((data: any) => {
                if (data && data.offlines && data.offlines
                [0]) {
                    console.log('Update')
                    
                    return data.offlines[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a offline
     * @param {offline} offline
     *
     * @return Promise<boolean>
     */
    removeOffline(offline: Offline): Promise<boolean> {
        return this.db.rel.del('offline', offline)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the offlines
     *
     * @return Promise<Array<offline>>
     */
    getOfflines(): Promise<Array<Offline>> {
        return this.db.rel.find('offline')
            .then((data: any) => {
                this.offlines = data.offlines;
                if (this.searchText && this.searchText !== '') {
                    this.offlines = FuseUtils.filterArrayByString(this.offlines, this.searchText);
                }
                //this.onofflinesChanged.next(this.offlines);
                return Promise.resolve(this.offlines);
                //return data.offlines ? data.offlines : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getDoctorOfflines(): Promise<Array<Offline>> {
        return this.db.rel.find('offline')
            .then((data: any) => {
                return data.offlines ? data.offlines : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a offline
     * @param {offline} offline
     *
     * @return Promise<offline>
     */
    getoffline(offline: Offline): Promise<Offline> {
        return this.db.rel.find('offline', offline.id)
            .then((data: any) => {
                console.log("Get")
                
                return data && data.offlines ? data.offlines[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected offline by id
     * @param id
     */
    toggleSelectedOffline(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedOfflines.length > 0) {
            const index = this.selectedOfflines.indexOf(id);

            if (index !== -1) {
                this.selectedOfflines.splice(index, 1);

                // Trigger the next event
                this.onSelectedOfflinesChanged.next(this.selectedOfflines);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedOfflines.push(id);

        // Trigger the next event
        this.onSelectedOfflinesChanged.next(this.selectedOfflines);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedOfflines.length > 0) {
            this.deselectOfflines();
        }
        else {
            this.selectOfflines();
        }
    }

    selectOfflines(filterParameter?, filterValue?) {
        this.selectedOfflines = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedOfflines = [];
            this.offlines.map(offline => {
                this.selectedOfflines.push(offline.id);
            });
        }
        else {
            /* this.selectedofflines.push(...
                 this.offlines.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedOfflinesChanged.next(this.selectedOfflines);
    }





    deselectOfflines() {
        this.selectedOfflines = [];

        // Trigger the next event
        this.onSelectedOfflinesChanged.next(this.selectedOfflines);
    }

    deleteOffline(offline) {
        this.db.rel.del('offline', offline)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const offlineIndex = this.offlines.indexOf(offline);
                this.offlines.splice(offlineIndex, 1);
                this.onOfflinesChanged.next(this.offlines);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedOfflines() {
        for (const offlineId of this.selectedOfflines) {
            const offline = this.offlines.find(_offline => {
                return _offline.id === offlineId;
            });

            this.db.rel.del('offline', offline)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const offlineIndex = this.offlines.indexOf(offline);
                    this.offlines.splice(offlineIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onOfflinesChanged.next(this.offlines);
        this.deselectOfflines();
    }
}
