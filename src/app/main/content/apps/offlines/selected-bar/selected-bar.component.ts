import { Component, OnInit } from '@angular/core';
import { OfflinesService } from '../offlines.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseOfflinesSelectedBarComponent implements OnInit
{
    selectedOfflines: string[];
    hasSelectedOfflines: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private offlinesService: PouchService,
        public dialog: MdDialog
    )
    {
        this.offlinesService.onSelectedOfflinesChanged
            .subscribe(selectedOfflines => {
                this.selectedOfflines = selectedOfflines;
                setTimeout(() => {
                    this.hasSelectedOfflines = selectedOfflines.length > 0;
                    this.isIndeterminate = (selectedOfflines.length !== this.offlinesService.offlines.length && selectedOfflines.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.offlinesService.selectOfflines();
    }

    deselectAll()
    {
        this.offlinesService.deselectOfflines();
    }

    deleteSelectedOfflines()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected offlines?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.offlinesService.deleteSelectedOfflines();
            }
            this.confirmDialogRef = null;
        });
    }

}
