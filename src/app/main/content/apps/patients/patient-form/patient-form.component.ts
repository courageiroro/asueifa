import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Staff } from '../../staffs/staff.model';
import { Blood_banksService } from '../../blood_banks/blood_banks.service'
import { StaffsService } from '../../staffs/staffs.service';
import { InsurancetypesService } from '../../insurancetypes/insurancetypes.service';
import { DomSanitizer } from '@angular/platform-browser';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-patients-patient-form-dialog',
    templateUrl: './patient-form.component.html',
    styleUrls: ['./patient-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FusePatientsPatientFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    patientForm: FormGroup;
    action: string;
    staff: Staff;
    sex: any;
    genderss = [];
    public blood_bankss;
    insuranceTypes;
    image;
    public localStorageItem: any;
    public localStorageType: any;

    constructor(
        public dialogRef: MdDialogRef<FusePatientsPatientFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        public db: PouchService,
        public _DomSanitizer: DomSanitizer

    ) {

        this.localStorageItem = JSON.parse(localStorage.getItem('user'));
        this.localStorageType = this.localStorageItem.usertype;
        
        this.db.getInsurancetypes().then(res => {
            this.insuranceTypes = res;
        })

        document.addEventListener('click', function () {
            db.file = document.querySelector('#fileupload');
            var imagestring = document.querySelector('#imagestring');

            var retrieve;

        })

        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Patient';
            this.staff = data.staff;
            this.image = data.staff.image;
        }
        else {
            this.dialogTitle = 'New Patient';
            this.staff = {
                id: '',
                rev: '',
                name: '',
                image: '',
                email: '',
                password: '',
                address: '',
                phone: '',
                sex: '',
                birth_date: new Date(),
                department_id: '',
                profile: '',
                secretquestion: '',
                answer: '',
                _attachments: '',
                usertype: 'Patient',
                age: '',
                blood_group: '',
                patientno: '',
                record: '',
                records: '',
                labrecord: '',
                labrecords: '',
                insurancename: '',
                insuranceid: '',
                imageurl: '',
                insurancepercent: null,
                account_opening_timestamp: '',
                appointments: [],
                bed_allotments: [],
                invoices: [],
                medicine_sales: [],
                prescriptions: [],
                reports: [],
                email_templates: [],
                smss: [],
                appointment: [],
                diagnosis_reports: [],
                blood_sales: []
            }

        }
        this.db.getBlood_banks().then(res => {
            this.blood_bankss = res;

        })

        this.patientForm = this.createPatientForm();
    }

    ngOnInit() {
        this.genderss = ['Male', 'Female'];
    }



    select(sex) {

    }

    createPatientForm() {
        return this.formBuilder.group({
            id: [this.staff.id],
            rev: [this.staff.rev],
            name: [this.staff.name],
            image: [this.staff.image],
            usertype: [this.staff.usertype],
            email: [this.staff.email],
            password: [this.staff.password],
            secretquestion: [this.staff.secretquestion],
            answer: [this.staff.answer],
            address: [this.staff.address],
            _attachments: [this.staff._attachments],
            phone: [this.staff.phone],
            department_id: [this.staff.department_id],
            profile: [this.staff.profile],
            sex: [this.staff.sex],
            patientno: [this.staff.patientno],
            birth_date: [this.staff.birth_date],
            imageurl: [this.staff.imageurl],
            age: [this.staff.age],
            blood_group: [this.staff.blood_group],
            account_opening_timestamp: [this.staff.account_opening_timestamp],
            record: [this.staff.record],
            records: [this.staff.records],
            labrecord: [this.staff.labrecord],
            labrecords: [this.staff.labrecords],
            insurancename: [this.staff.insurancename],
            insuranceid: [this.staff.insuranceid],
            insurancepercent: [this.staff.insurancepercent]
        });
    }
}
