import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { StaffsService } from '../../staffs/staffs.service';
import { PrescriptionsService } from '../../prescriptions/prescriptions.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs/Rx';
import { FusePatientsPatientFormDialogComponent } from '../patient-form/patient-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort  } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import {PouchService} from '../../../../../provider/pouch-service';
import { Staff } from '../../staffs/staff.model';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';

@Component({
    selector: 'fuse-patients-patient-list',
    templateUrl: './patient-list.component.html',
    styleUrls: ['./patient-list.component.scss']
})
export class FusePatientsPatientListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public localStorageItem: any;
    public localStorageType: any;
    public patients: Array<Staff> = [];
    patients2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'name', 'patientno', 'sex', 'age', 'blood_g', 'phone', 'email', 'address', 'birth', 'accounttimestamp', 'password','buttonslab','buttons2','buttons'];
    selectedPatients: any[];
    checkboxes: {};

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;

    constructor(public dialog: MdDialog, public db: PouchService, public router: Router) {
        this.localStorageItem = JSON.parse(localStorage.getItem('user'));
        this.localStorageType = this.localStorageItem.usertype;
    }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        this._loadPatients();
    }

    private _loadPatients(): void {
        this.db.onStaffsChanged.subscribe(data => {
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        this.db.getPStaffs()
            .then((patients: Array<Staff>) => {
                var userType = "Patient"
                this.patients = patients;
                this.patients = patients.filter(data => data.usertype == userType);

                this.patients2 = new BehaviorSubject<any>(patients);

                this.checkboxes = {};
                patients.map(patient => {
                    this.checkboxes[patient.id] = false;
                });
                this.db.onSelectedStaffsChanged.subscribe(selectedPatients => {
                    for (const id in this.checkboxes) {
                        this.checkboxes[id] = selectedPatients.includes(id);
                    }

                    this.selectedPatients = selectedPatients;
                });
                this.db.onSearchTextChanged.subscribe(data => {
                    this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                })
            });

    }

    get() {
        this.db.getPStaffs().then(res => {

            var url = URL.createObjectURL(res[3]._attachments);

            var img = document.createElement('img');
            img.src = url;
            document.getElementById('img2').appendChild(img)
        })
    }

    newPatient() {
        this.dialogRef = this.dialog.open(FusePatientsPatientFormDialogComponent, {
            panelClass: 'patient-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
                var res = response.getRawValue();
                res.birth_date = new Date(res.birth_date).toISOString().substring(0, 10);
                res.profile
                var reader = new FileReader();
                var file = this.db.file;
                var retrieve = (<HTMLInputElement>file).files[0];
                var file2;
                var myThis = this;
                if (retrieve == undefined) {
                    res.profile

                    myThis.db.saveStaff(res);

                    myThis.dataSource = new FilesDataSource(myThis.db, myThis.paginator, myThis.sort);
                }
                else {
                    reader.readAsDataURL(retrieve);
                    reader.onloadend = function () {
                        reader.result;

                        res.image = reader.result;

                        res.profile

                        myThis.db.saveStaff(res);

                        myThis.dataSource = new FilesDataSource(myThis.db, myThis.paginator, myThis.sort);
                    }
                };
                reader.onerror = function (error) {
                    console.log('Error: ', error);
                };
            });

    }

    patientrecords(staff) {
        this.db.records = staff;
        this.router.navigate(['apps/record', staff.id]);

    }

    patientlab(patient){
    this.router.navigate(['apps/labrecord', patient.id]);
    }

    patientpresc(patient){
        this.router.navigate(['apps/prescriptions', patient.id]);
        this.db.id = patient.id;
    }

    editPatient(staff) {
        console.log(staff);
        this.dialogRef = this.dialog.open(FusePatientsPatientFormDialogComponent, {
            panelClass: 'patient-form-dialog',
            data: {
                staff: staff,
                action: 'edit'
            }
        });



        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                var form = formData.getRawValue();
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':
                        form.birth_date = new Date(form.birth_date).toISOString().substring(0, 10);
                        var reader = new FileReader();
                        var file = this.db.file;
                        var retrieve = (<HTMLInputElement>file).files[0];
                        var file2;
                        var myThis = this;
                        if (retrieve == undefined) {
                            form.profile

                            myThis.db.updateStaff(form);

                            myThis.dataSource = new FilesDataSource(myThis.db, myThis.paginator, myThis.sort);
                        }
                        else {
                            reader.readAsDataURL(retrieve);
                            reader.onloadend = function () {
                                reader.result;

                                form.image = reader.result;

                                form.profile


                                myThis.db.updateStaff(form);
                                myThis.dataSource = new FilesDataSource(myThis.db, myThis.paginator, myThis.sort);
                            };
                            reader.onerror = function (error) {
                                console.log('Error: ', error);
                            };
                        }
                        
                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteStaff(staff);

                        break;
                }
            });

    }

    /**
     * Delete patient
     */
    deleteStaff(patient) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deleteStaff(patient);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(patientId) {
        this.db.toggleSelectedStaff(patientId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    patients2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {

        const displayDataChanges = [
            of(this.db.staffs).delay(5000),
            //this.db.onStaffsChanged,
            this.sort.mdSortChange,
            this._paginator.page,
        ];

        this._paginator.length = this.db.staffs.length;
/* 
        var result = Observable.fromPromise(this.db.getPStaffs());
        result.subscribe(x => console.log(x), e => console.error(e));
        return result; */

        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.staffs]));
            }); 
    }

    disconnect() {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
          return data;
        }
    
        return data.sort((a, b) => {
          const isAsc = this.sort.direction === 'asc';
          switch (this.sort.active) {
            case 'name': return compare(a.name, b.name, isAsc);
            case 'amount': return compare(+a.amount, +b.amount, isAsc);
            case 'id': return compare(+a.id, +b.id, isAsc);
            default: return 0;
          }
        });
      }
}
/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }