export interface Patient {
    id: string,
    rev: string,
    name: string,
    email: string,
    password: string,
    address: string,
    phone: string,
    patientno: any,
    sex: string,
    birth_date: Date,
    age: string,
    blood_group: string,
    account_opening_timestamp: string,
    appointments: Array<string>,
    bed_allotments: Array<string>,
    invoices: Array<string>,
    medicine_sales: Array<string>,
    prescriptions: Array<string>,
    reports: Array<string>,
    email_templates: Array<string>,
    diagnosis_reports: Array<string>,
    smss: Array<string>
}

