import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { StaffsService } from '../staffs/staffs.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-patients',
    templateUrl  : './patients.component.html',
    styleUrls    : ['./patients.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FusePatientsComponent implements OnInit
{
    hasSelectedPatients: boolean;
    searchInput: FormControl;

    constructor(private patientsService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.patientsService.onSelectedStaffsChanged
            .subscribe(selectedPatients => {
                this.hasSelectedPatients = selectedPatients.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.patientsService.onSearchTextChanged.next(searchText);
            });
    }

}
