import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FusePatientsComponent } from './patients.component';
import { StaffsService } from '../staffs/staffs.service';
import { FusePatientsPatientListComponent } from './patient-list/patient-list.component';
import { FusePatientsSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FusePatientsPatientFormDialogComponent } from './patient-form/patient-form.component';
import {Blood_banksService} from './../blood_banks/blood_banks.service'
import { RecordsService } from './../records/records.service';
import { PrescriptionsService } from './../prescriptions/prescriptions.service';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/patients',
        component: FusePatientsComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            patients: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FusePatientsComponent,
        FusePatientsPatientListComponent,
        FusePatientsSelectedBarComponent,
        FusePatientsPatientFormDialogComponent
    ],
    providers      : [
        StaffsService,
        PouchService,
        AuthGuard,
        Blood_banksService,
        RecordsService,
        PrescriptionsService
    ],
    entryComponents: [FusePatientsPatientFormDialogComponent]
})
export class FusePatientsModule
{
}
