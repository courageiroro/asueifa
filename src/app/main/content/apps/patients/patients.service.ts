/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Patient } from './patient.model';
import { Schema } from '../schema';

@Injectable()
export class PatientsService {
    public patientname = "";
    public patientnumber = "";
    public sCredentials;
    
    onPatientsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedPatientsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    patients: Patient[];
    user: any;
    selectedPatients: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The patients App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getPatients()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getPatients();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getPatients();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('asueifai', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/asueifai';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/asueifai';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }

    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }


    /***********
     * patient
     **********/
    /**
     * Save a patient
     * @param {patient} patient
     *
     * @return Promise<patient>
     */
    savePatient(patient: Patient): Promise<Patient> {
        patient.id = Math.floor(Date.now()).toString();

        patient.bed_allotments = [];
        patient.appointments = [];
        patient.invoices = [];
        patient.prescriptions = [];
        patient.medicine_sales = [];
        patient.reports = [];
        return this.db.rel.save('patient', patient)
            .then((data: any) => {

                if (data && data.patients && data.patients
                [0]) {
                    return data.patients[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a patient
    * @param {patient} patient
    *
    * @return Promise<patient>
    */
    updatePatient(patient: Patient) {

        return this.db.rel.save('patient', patient)
            .then((data: any) => {
                if (data && data.patients && data.patients
                [0]) {
                    
                    return data.patients[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a patient
     * @param {patient} patient
     *
     * @return Promise<boolean>
     */
    removePatient(patient: Patient): Promise<boolean> {
        return this.db.rel.del('patient', patient)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the patients
     *
     * @return Promise<Array<patient>>
     */
    getPatients(): Promise<Array<Patient>> {
        return this.db.rel.find('patient')
            .then((data: any) => {
                this.patients = data.patients;
                if (this.searchText && this.searchText !== '') {
                    this.patients = FuseUtils.filterArrayByString(this.patients, this.searchText);
                }
                //this.onpatientsChanged.next(this.patients);
                return Promise.resolve(this.patients);
                //return data.patients ? data.patients : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getAppointmentPatients(): Promise<Array<Patient>> {
        return this.db.rel.find('patient')
            .then((data: any) => {
                return data.patients ? data.patients : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a patient
     * @param {patient} patient
     *
     * @return Promise<patient>
     */
    getPatient(patient: Patient): Promise<Patient> {
        return this.db.rel.find('patient', patient.id)
            .then((data: any) => {
                return data && data.patients ? data.patients[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected patient by id
     * @param id
     */
    toggleSelectedPatient(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedPatients.length > 0) {
            const index = this.selectedPatients.indexOf(id);

            if (index !== -1) {
                this.selectedPatients.splice(index, 1);

                // Trigger the next event
                this.onSelectedPatientsChanged.next(this.selectedPatients);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedPatients.push(id);

        // Trigger the next event
        this.onSelectedPatientsChanged.next(this.selectedPatients);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedPatients.length > 0) {
            this.deselectPatients();
        }
        else {
            this.selectPatients();
        }
    }

    selectPatients(filterParameter?, filterValue?) {
        this.selectedPatients = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedPatients = [];
            this.patients.map(patient => {
                this.selectedPatients.push(patient.id);
            });
        }
        else {
            /* this.selectedpatients.push(...
                 this.patients.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedPatientsChanged.next(this.selectedPatients);
    }





    deselectPatients() {
        this.selectedPatients = [];

        // Trigger the next event
        this.onSelectedPatientsChanged.next(this.selectedPatients);
    }

    deletePatient(patient) {
        this.db.rel.del('patient', patient)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const patientIndex = this.patients.indexOf(patient);
                this.patients.splice(patientIndex, 1);
                this.onPatientsChanged.next(this.patients);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedPatients() {
        for (const patientId of this.selectedPatients) {
            const patient = this.patients.find(_patient => {
                return _patient.id === patientId;
            });

            this.db.rel.del('patient', patient)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const patientIndex = this.patients.indexOf(patient);
                    this.patients.splice(patientIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onPatientsChanged.next(this.patients);
        this.deselectPatients();
    }
}
