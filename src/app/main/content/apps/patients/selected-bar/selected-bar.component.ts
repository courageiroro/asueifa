import { Component, OnInit } from '@angular/core';
import { StaffsService } from '../../staffs/staffs.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FusePatientsSelectedBarComponent implements OnInit
{
    selectedPatients: string[];
    hasSelectedPatients: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private patientsService: PouchService,
        public dialog: MdDialog
    )
    {
        this.patientsService.onSelectedStaffsChanged
            .subscribe(selectedPatients => {
                this.selectedPatients = selectedPatients;
                setTimeout(() => {
                    this.hasSelectedPatients = selectedPatients.length > 0;
                    this.isIndeterminate = (selectedPatients.length !== this.patientsService.staffs.length && selectedPatients.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.patientsService.selectStaffs();
    }

    deselectAll()
    {
        this.patientsService.deselectStaffs();
    }

    deleteSelectedPatients()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected patientes?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.patientsService.deleteSelectedStaffs();
            }
            this.confirmDialogRef = null;
        });
    }

}
