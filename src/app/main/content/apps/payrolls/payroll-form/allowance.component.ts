import { Component, Input, EventEmitter, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Payroll } from '../payroll.model';

@Component({
    selector: 'allowance',
    templateUrl: './allowance.component.html',
    outputs: ['passInput'],
})

export class AllowanceComponent {
    public allowance: number;
    passInput = new EventEmitter<number>();

    @Input('group')
    public allowanceForm: FormGroup;

    constructor(
       
    ) {

    }

    onChange(value: number) {
        this.passInput.emit(value);
    }
}


