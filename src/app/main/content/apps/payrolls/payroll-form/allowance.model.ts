export interface Allowance {
    id: string,
    rev: string,
    allowance: number
}