export interface Deduction {
    id: string,
    rev: string,
    deduction: number
}