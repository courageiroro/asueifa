import { Component, Inject, OnInit, ElementRef, ViewEncapsulation, ViewChild, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormArray, Validators, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Payroll } from '../payroll.model';
import { AllowanceComponent } from './allowance.component';
import { DeductionComponent } from './deduction.component';
import { BehaviorSubject } from 'rxjs/Rx';
import { AccountantsService } from '../../accountants/accountants.service';
import { StaffsService } from '../../staffs/staffs.service';
import { LaboratoristsService } from '../../laboratorists/laboratorists.service';
import { NursesService } from '../../nurses/nurses.service';
import { PharmacistsService } from '../../pharmacists/pharmacists.service';
import { ReceptionistsService } from '../../receptionists/receptionists.service';
import { Accountant } from '../../accountants/accountant.model';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-payrolls-payroll-form-dialog',
    templateUrl: './payroll-form.component.html',
    styleUrls: ['./payroll-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FusePayrollsPayrollFormDialogComponent implements OnInit {

    @ViewChild('parent', { read: ViewContainerRef }) container: ViewContainerRef;
    @ViewChild('deduct', { read: ViewContainerRef }) container2: ViewContainerRef;
    @ViewChild('allowance') input: ElementRef;
    @ViewChild('deduction') input2: ElementRef;

    public accountantss: Array<Accountant> = [];
    name: any;
    accountants2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    event: CalendarEvent;
    dialogTitle: string;
    payrollForm: FormGroup;
    action: string;
    payroll: Payroll;
    statuss: any;
    public totalAllowance: string;
    public trigger: string;
    public netSalary;
    public textData: number;
    public textData2: number;
    public final;
    public final2;
    public employeess;
    public basic;
    public accountants: any;
    public namess;
    public control;
    public account;
    public myName;
    public staffs;
    public laboratorists;
    public nurses;
    public result;
    public pharmacists;
    public receptionists;
    public accountantId: string;
    public nameId: string;
    public saddress: string;
    public semail: string;
    public snumber: string

    constructor(
        public dialogRef: MdDialogRef<FusePayrollsPayrollFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        private factoryResolve: ComponentFactoryResolver,
        //public db: AccountantsService,
        public db2: PouchService,
        //public db3: LaboratoristsService,
        //public db4: NursesService,
        //public db5: PharmacistsService,
        //public db6: ReceptionistsService,
    ) {

        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Payroll';
            this.payroll = data.payroll;
        }
        else {
            this.dialogTitle = 'New Payroll';
            this.payroll = {
                id: '',
                rev: '',
                payroll_code: Math.floor((Math.random() * 90000) + 10000),
                user_id: '',
                user_type: '',
                joining_salary: null,
                allowances: [],
                deductions: [],
                date: new Date,
                status: '',
                name_Id: '',
                saddress: '',
                snumber: '',
                semail: '',
                basic_salary: null,
                totalallowance: null,
                totaldeduction: null

            }
        }

        this.payrollForm = this.createPayrollForm();

    }

    ngOnInit() {

        /* this.payroll.joining_salary = 0;
        this.payroll.totalallowance = 0;
        this.payroll.totaldeduction = 0;  */
        this.employeess = ['Accountants', 'Doctors', 'Laboratorists', ' Nurses', 'Pharmacists', 'Receptionists']

        if (this.employeess[0] === this.payroll.user_type) {
            this.name = this.db2.name
            
        }
        else if (this.employeess[1] === this.payroll.user_type) {
            this.name = this.db2.name
            
        }
        else if (this.employeess[2] === this.payroll.user_type) {
            this.name = this.db2.name
        }
        else if (this.employeess[3] === this.payroll.user_type) {
            this.name = this.db2.name
        }
        else if (this.employeess[4] === this.payroll.user_type) {
            this.name = this.db2.name
        }
        else if (this.employeess[5] === this.payroll.user_type) {
            this.name = this.db2.name
        }

        this.db2.getAStaffs().then(res => {
            this.accountants = res;
        })
        this.db2.getDStaffs().then(res => {
            this.staffs = res;
        })
        this.db2.getLStaffs().then(res => {
            this.laboratorists = res;
        })
        this.db2.getNStaffs().then(res => {
            this.nurses = res;
        })
        this.db2.getPHStaffs().then(res => {
            this.pharmacists = res;
        })
        this.db2.getRStaffs().then(res => {
            this.receptionists = res;
        })

        this.statuss = ['Paid', 'Unpaid']
    }


    click() {

        if (this.employeess[0] === this.payroll.user_type) {
            this.namess = this.accountants

        }
        else if (this.employeess[1] === this.payroll.user_type) {
            this.namess = this.staffs
        }
        else if (this.employeess[2] === this.payroll.user_type) {
            this.namess = this.laboratorists
        }
        else if (this.employeess[3] === this.payroll.user_type) {
            this.namess = this.nurses
        }
        else if (this.employeess[4] === this.payroll.user_type) {
            this.namess = this.pharmacists
        }
        else {
            this.namess = this.receptionists

        }
    }

    getUId(name) {
        
        this.payroll.name_Id = name.id;
        this.payroll.snumber = name.phone;
        this.payroll.saddress = name.address;
        this.payroll.semail = name.email;
        /*  if (this.employeess[0] === this.account) {
             this.db2.name = name.accountant_id;
             this.nameId = name.accountant_id
         }
         else if (this.employeess[1] === this.account) {
             this.db2.name = name.doctor_id;
             this.nameId = name.doctor_id
         }
         else if (this.employeess[2] === this.account) {
             this.db2.name = name.laboratorist_id;
             this.nameId = name.laboratorist_id
         }
         else if (this.employeess[3] === this.account) {
             this.db2.name = name.nurse_id;
             this.nameId = name.nurse_id
         }
         else if (this.employeess[4] === this.account) {
             this.db2.name = name.pharmacist_id;
             this.nameId = name.pharmacist_id
         }
         else if (this.employeess[5] === this.account) {
             this.db2.name = name.receptionist_id;
             this.nameId = name.receptionist_id
         } */
     
    }


    initAllowance() {
        // initialize our allowance
        return this.formBuilder.group({
            allowance: [''],
        });
    }

    initDeduction() {
        // initialize our allowance
        return this.formBuilder.group({
            deduction: [''],
        });
    }

    getArray(get) {
        var mine = Number([get.allowance].join())
        
        return mine
    }

    getArray2(get) {
        var mine = Number([get.deduction].join())
        return mine
    }

    getSum(total, num) {
        return total + num;
    }

    getSum2(total, num) {
        return total + num;
    }

    addAllowance() {
        // add allowance and entry to the list
        this.control = <FormArray>this.payrollForm.controls['allowances'];
        this.control.push(this.initAllowance());
        
        this.trigger = "show";
    }

    addDeduction() {
        // add allowance and entry to the list
        this.control = <FormArray>this.payrollForm.controls['deductions'];
        this.control.push(this.initDeduction());
        this.trigger = "show";
        //console.log(this.control.value);
    }

    removeAllowance(i: number) {
        // remove amount and entry from the list
        const control = <FormArray>this.payrollForm.controls['allowances'];
        control.removeAt(i);
    }

    removeDeduction(i: number) {
        // remove amount and entry from the list
        const control = <FormArray>this.payrollForm.controls['deductions'];
        control.removeAt(i);
    }

    calcAllowance() {
        this.trigger = "show";
        this.control = <FormArray>this.payrollForm.controls['allowances'];
        var array = this.control.value;
        this.result = array.map(this.getArray);
        
        this.payroll.totalallowance = this.result.reduce(this.getSum);
        
        this.payroll.joining_salary = this.payroll.totalallowance - this.payroll.totaldeduction

    }

    calcAllowance2() {
        this.payroll.totalallowance = this.textData;
        this.payroll.joining_salary = this.payroll.totalallowance - this.payroll.totaldeduction
        

    }

    calcDeduction() {
        this.trigger = "show";
        this.control = <FormArray>this.payrollForm.controls['deductions'];
        var array = this.control.value;
        this.result = array.map(this.getArray2);
        
        this.payroll.totaldeduction = this.result.reduce(this.getSum2);
        
        this.payroll.joining_salary = - this.payroll.totaldeduction - (-this.payroll.totalallowance)
    }

    calcDeduction2() {
        this.payroll.totaldeduction = this.textData2;
        
        this.payroll.joining_salary = - this.payroll.totaldeduction - (-this.payroll.totalallowance)
    }

    getNetSalary() {
        this.payroll.joining_salary = this.payroll.totalallowance;
    }

    getBasic() {
        this.payroll.joining_salary = this.payroll.basic_salary - (- this.payroll.totalallowance) - this.payroll.totaldeduction
    }


    createPayrollForm() {
        return this.formBuilder.group({
            id: [this.payroll.id],
            rev: [this.payroll.rev],
            payroll_code: [this.payroll.payroll_code],
            user_id: ({
                value: this.payroll.user_id,
                var: [this.myName = this.payroll.user_id]
            }),
            user_type: ({
                value: this.payroll.user_type,
                var: [this.account = this.payroll.user_type]
            }),
            joining_salary: ({
                value: this.payroll.joining_salary,
                disabled: true
            }),
            allowances: this.formBuilder.array([
                this.initAllowance(),
            ]),
            deductions: this.formBuilder.array([
                this.initDeduction(),
            ]),
            date: [this.payroll.date],
            status: [this.payroll.status],
            name_Id: [this.payroll.name_Id],
            saddress: [this.payroll.saddress],
            snumber: [this.payroll.snumber],
            semail: [this.payroll.semail],
            basic_salary: [this.payroll.basic_salary],
            totalallowance: ({
                value: [this.payroll.totalallowance],
                disabled: true
            }),
            totaldeduction: ({
                value: [this.payroll.totaldeduction],
                disabled: true
            }),
             basic: ({
                value: [''],

            })
        });
    }
}
