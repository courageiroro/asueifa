import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { PayrollsService } from '../payrolls.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { FusePayrollsPayrollFormDialogComponent } from '../payroll-form/payroll-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Payroll } from '../payroll.model';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {PouchService} from '../../../../../provider/pouch-service';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';

@Component({
    selector: 'fuse-payrolls-payroll-list',
    templateUrl: './payroll-list.component.html',
    styleUrls: ['./payroll-list.component.scss']
})
export class FusePayrollsPayrollListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public localStorageItem: any;
    public localStorageType: any;
    public payrolls: Array<Payroll> = [];
    payrolls2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'code', 'employee', 'account', 'salary', 'date', 'status', 'allowance', 'deduction', 'buttons'];
    selectedPayrolls: any[];
    checkboxes: {};

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;

    constructor(public dialog: MdDialog, public db: PouchService, public router: Router) {
        this.localStorageItem = JSON.parse(localStorage.getItem('user'));
        this.localStorageType = this.localStorageItem.usertype;
        
    }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        this._loadPayrolls();

        if (this.localStorageType !== "Admin") {
            this.localStorageType = "disable";
        }
        else {
            this.localStorageType = "Admin";
        }
    }

    private _loadPayrolls(): void {
        this.db.onPayrollsChanged.subscribe(data => {
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        this.db.getPayrolls()
            .then((payrolls: Array<Payroll>) => {
                this.payrolls = payrolls;
                this.payrolls2 = new BehaviorSubject<any>(payrolls);

                this.checkboxes = {};
                payrolls.map(payroll => {
                    this.checkboxes[payroll.id] = false;
                });
                this.db.onSelectedPayrollsChanged.subscribe(selectedPayrolls => {
                    for (const id in this.checkboxes) {
                        this.checkboxes[id] = selectedPayrolls.includes(id);
                    }

                    this.selectedPayrolls = selectedPayrolls;
                });
                this.db.onSearchTextChanged.subscribe(data => {
                    this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                })
            });

    }

    viewPayroll(payroll) {
        
        this.router.navigate(['/apps/payrollpages', payroll.id]);
    }

    newPayroll() {
        this.dialogRef = this.dialog.open(FusePayrollsPayrollFormDialogComponent, {
            panelClass: 'payroll-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
                var res = response.getRawValue();
                res.date = new Date(res.date).toISOString().substring(0,10);
                this.db.savePayroll(res);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

            });

    }

    editPayroll(payroll) {
        
        this.dialogRef = this.dialog.open(FusePayrollsPayrollFormDialogComponent, {
            panelClass: 'payroll-form-dialog',
            data: {
                payroll: payroll,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':
                        var res = formData.getRawValue();
                        res.date = res.date = new Date(res.date).toISOString().substring(0,10);
                        this.db.updatePayroll(res);
                        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deletePayroll(payroll);

                        break;
                }
            });
    }

    /**
     * Delete payroll
     */
    deletePayroll(payroll) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deletePayroll(payroll);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(payrollId) {
        this.db.toggleSelectedPayroll(payrollId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    payrolls2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            of(this.db.payrolls).delay(5000),
            this.sort.mdSortChange,
            this._paginator.page,
        ];

        var result = Observable.fromPromise(this.db.getPayrolls());
       /*  result.subscribe(x => console.log(x), e => console.error(e));
        return result; */

        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.payrolls]));
        });

    }

    disconnect() {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
          return data;
        }
    
        return data.sort((a, b) => {
          const isAsc = this.sort.direction === 'asc';
          switch (this.sort.active) {
            case 'employee': return compare(a.user_id, b.user_id, isAsc);
            case 'amount': return compare(+a.amount, +b.amount, isAsc);
            case 'id': return compare(+a.id, +b.id, isAsc);
            default: return 0;
          }
        });
      }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
