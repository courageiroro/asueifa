import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FusePayrollsComponent } from './payrolls.component';
import { PayrollsService } from './payrolls.service';
import { FusePayrollsPayrollListComponent } from './payroll-list/payroll-list.component';
import { FusePayrollsSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FusePayrollsPayrollFormDialogComponent } from './payroll-form/payroll-form.component';
import { AllowanceComponent } from './payroll-form/allowance.component';
import { DeductionComponent } from './payroll-form/deduction.component';
import { FusePayrollsPayrollPageComponent } from './payroll-page/payroll-page.component';
import { StaffsService } from './../staffs/staffs.service';
import { SettingsService } from './../settings/settings.service';
import { CurrencysService } from './../currencys/currencys.service';
import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from "@angular/core";
import { CurrencyPipe } from '@angular/common';
import { FusePayrollreceiptsPayrollreceiptFormDialogComponent } from './payroll-receipt/payroll-receipt.component';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path: '**',
        component: FusePayrollsPayrollPageComponent,
        children: [],
        resolve: {
            payrolls: PouchService
        }
    }
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        FusePayrollsPayrollPageComponent,
        FusePayrollreceiptsPayrollreceiptFormDialogComponent
    ],
    providers: [
        PayrollsService,
        PouchService,
        StaffsService,
        SettingsService,
        CurrencysService,
        DatePipe,
        CurrencyPipe

    ],
    entryComponents: [FusePayrollsPayrollPageComponent, FusePayrollreceiptsPayrollreceiptFormDialogComponent]
})
export class FusePayrollPageModule {
}
