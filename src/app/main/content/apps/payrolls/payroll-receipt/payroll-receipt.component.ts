import { Component, Inject, OnInit, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { PayrollsService } from '../payrolls.service';
import { StaffsService } from '../../staffs/staffs.service';
import { SettingsService } from '../../settings/settings.service';
import { CurrencysService } from '../../currencys/currencys.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-payroll-receipts-payroll-receipt-form-dialog',
    templateUrl: './payroll-receipt.component.html',
    styleUrls: ['./payroll-receipt.component.scss'],
    encapsulation: ViewEncapsulation.None,
    outputs: ['childEvent']
})

export class FusePayrollreceiptsPayrollreceiptFormDialogComponent implements OnInit {
    
    event: CalendarEvent;
    dialogTitle: string;
    payrollreceiptForm: FormGroup;
    childEvent = new EventEmitter<string>();
    action: string;
    public settings;
    public result;
    public final;
    public hospitalName;
    public result2;
    public final2;
    public hospitalAddress;
    public result3;
    public final3;
    public hospitalPhone;
    public result4;
    public final4;
    public hospitalEmail;
    public Pid;
    public payrollDate: any;
    public payrollMonth: any;
    public payrollNumber: any;
    public payrollDueDate: any;
    public staffName: any;
    public basicSalary: any;
    public userType: any;
    public allowancess;
    public deductionss;
    public staffId;
    public staffAddress;
    public staffNumber;
    public staffEmail;
    public subTotal;
    public vat;
    public discount;
    public total;
    public currencys;
    public result5;
    public final5;
    public displayCurrencys;


    constructor(
        public dialogRef: MdDialogRef<FusePayrollreceiptsPayrollreceiptFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        public db: PouchService,
        public router: Router,
        public activateRoute: ActivatedRoute
    ) {
        this.Pid = data.pid
        this.dialogTitle = 'Payroll Receipt';
        /* 
                if (this.action === 'edit') {
                    this.dialogTitle = 'Edit payroll-receipt';
                    this.payroll-receipt = data.payroll-receipt;
                }
                else {
                    this.dialogTitle = 'New payroll-receipt';
                    this.payroll-receipt = {
                        id: '',
                        rev: '',
                        name: '',
                        description: '',
                        doctors: []
                    } */


        //this.payroll-receiptForm = this.createpayroll-receiptForm();
    }

    getArray(gets) {
        var mine = [gets.name].join()
        return mine

    }
    getAddress(gets) {
        var mine = [gets.address].join()
        return mine

    }
    getPhone(gets) {
        var mine = [gets.phone].join()
        return mine

    }
    getEmail(gets) {
        var mine = [gets.email].join()
        return mine

    }

    getCurrency(gets) {
        var mine = [gets.currency_symbol].join()
        return mine

    }

    getAmount(gets) {
        var mine = [gets.amount].join()
        return mine

    }

    ngOnInit() {
        var monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];

        this.db.getPayroll(this.Pid).then(res => {
            
            this.payrollDate = new Date(res.date).toDateString();
            this.payrollMonth = monthNames[new Date(res.date).getMonth()];
            this.payrollNumber = res.payroll_code;
            //this.invoiceDueDate = new Date(res.due_timestamp).toDateString();
            this.staffName = res.user_id;
            this.staffId = res.name_Id;
            this.userType = res.user_type;
            this.staffAddress = res.saddress;
            this.staffNumber = res.snumber;
            this.staffEmail = res.semail;
            this.allowancess = res.allowances;
            this.deductionss = res.deductions;
            this.subTotal = res.joining_salary;
            this.basicSalary = res.basic_salary;
            //console.log(this.arrayAmount)

            this.db.getCurrencys().then(res => {
                this.currencys = res;
                this.result5 = this.currencys.map(this.getCurrency)
                this.final5 = this.result5.length - 1;
                this.displayCurrencys = this.result5[this.final5]
                
                /*  this.arrayAmount = this.unitss.map(this.getAmount)
                 console.log(this.arrayAmount.join())
                 for (this.i = 0; this.i <= this.arrayAmount.length-1; this.i++) {
                     this.formatted = "\n" + (new CurrencyPipe('en-US')).transform(this.arrayAmount[this.i], this.displayCurrencys.trim(), true)+ "<br>";
                     console.log(this.formatted)
                     this.newArray = [];
                     this.newArray.push(this.formatted.toString());
                     console.log(this.newArray);
                 } */
            })


        })

        this.db.getSettings().then(res => {
            this.settings = res;
            this.result = this.settings.map(this.getArray)
            this.final = this.result.length - 1;
            this.hospitalName = this.result[this.final]
            

            this.result2 = this.settings.map(this.getAddress)
            this.final2 = this.result2.length - 1;
            this.hospitalAddress = this.result2[this.final2]

            this.result3 = this.settings.map(this.getPhone)
            this.final3 = this.result3.length - 1;
            this.hospitalPhone = this.result3[this.final3]

            this.result4 = this.settings.map(this.getEmail)
            this.final4 = this.result4.length - 1;
            this.hospitalEmail = this.result4[this.final4]
        })
        //this.print();
    }

    print() {

        window.print();

    }

    createPayrollreceiptForm() {
        /*  return this.formBuilder.group({
             id: [this.payroll-receipt.id],
             rev: [this.payroll-receipt.rev],
             name: [this.payroll-receipt.name],
             description: [this.payroll-receipt.description],
 
         });*/
    }
}
