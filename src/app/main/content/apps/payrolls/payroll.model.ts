import { Allowance } from './payroll-form/allowance.model';
import { Deduction } from './payroll-form/deduction.model';
export interface Payroll {
    id: string,
    rev: string,
    payroll_code: number,
    user_id: string,
    user_type: string,
    joining_salary: number,
    basic_salary: number,
    allowances: Allowance[],
    deductions: Deduction[],
    date: Date,
    status: string,
    name_Id: string,
    saddress: string,
    snumber: string,
    semail: string,
    totaldeduction: number,
    totalallowance: number
}


