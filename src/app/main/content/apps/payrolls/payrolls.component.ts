import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { PayrollsService } from './payrolls.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-payrolls',
    templateUrl  : './payrolls.component.html',
    styleUrls    : ['./payrolls.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FusePayrollsComponent implements OnInit
{
    hasSelectedPayrolls: boolean;
    searchInput: FormControl;

    constructor(private payrollsService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.payrollsService.onSelectedPayrollsChanged
            .subscribe(selectedPayrolls => {
                this.hasSelectedPayrolls = selectedPayrolls.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.payrollsService.onSearchTextChanged.next(searchText);
            });
    }

}
