import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FusePayrollsComponent } from './payrolls.component';
import { PayrollsService } from './payrolls.service';
import { FusePayrollsPayrollListComponent } from './payroll-list/payroll-list.component';
import { FusePayrollsSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FusePayrollsPayrollFormDialogComponent } from './payroll-form/payroll-form.component';
import { AllowanceComponent } from './payroll-form/allowance.component'
import { DeductionComponent } from './payroll-form/deduction.component'
import { AccountantsService } from './../accountants/accountants.service';
import { StaffsService } from './../staffs/staffs.service';
import { LaboratoristsService } from './../laboratorists/laboratorists.service';
import { NursesService } from './../nurses/nurses.service';
import { PharmacistsService } from './../pharmacists/pharmacists.service';
import { ReceptionistsService } from './../receptionists/receptionists.service';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path: 'apps/payrolls',
        component: FusePayrollsComponent,
        canActivate: [AuthGuard],
        children: [],
        resolve: {
            payrolls: PouchService
        }
    }
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        FusePayrollsComponent,
        FusePayrollsPayrollListComponent,
        FusePayrollsSelectedBarComponent,
        FusePayrollsPayrollFormDialogComponent,
        AllowanceComponent,
        DeductionComponent
    ],
    providers: [
        PayrollsService,
        AccountantsService,
        StaffsService,
        PouchService,
        LaboratoristsService,
        NursesService,
        PharmacistsService,
        AuthGuard,
        ReceptionistsService
    ],
    entryComponents: [FusePayrollsPayrollFormDialogComponent, AllowanceComponent, DeductionComponent]
})
export class FusePayrollsModule {
}
