/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Payroll } from './payroll.model';
import { Schema } from '../schema';

@Injectable()
export class PayrollsService {
    public payrollname = "";
    public hide = "";
    public sCredentials;
    public pid = "";
    onPayrollsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedPayrollsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    payrolls: Payroll[];
    user: any;
    selectedPayrolls: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The payrolls App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getPayrolls()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getPayrolls();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getPayrolls();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('asueifai', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/asueifai';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/asueifai';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }


    /***********
     * payroll
     **********/
    /**
     * Save a payroll
     * @param {payroll} payroll
     *
     * @return Promise<payroll>
     */
    savePayroll(payroll: Payroll): Promise<Payroll> {
        payroll.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('payroll', payroll)
            .then((data: any) => {

                if (data && data.payrolls && data.payrolls
                [0]) {
                    return data.payrolls[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a payroll
    * @param {payroll} payroll
    *
    * @return Promise<payroll>
    */
    updatePayroll(payroll: Payroll) {

        return this.db.rel.save('payroll', payroll)
            .then((data: any) => {
                if (data && data.payrolls && data.payrolls
                [0]) {
                    return data.payrolls[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a payroll
     * @param {payroll} payroll
     *
     * @return Promise<boolean>
     */
    removePayroll(payroll: Payroll): Promise<boolean> {
        return this.db.rel.del('payroll', payroll)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the payrolls
     *
     * @return Promise<Array<payroll>>
     */
    getPayrolls(): Promise<Array<Payroll>> {
        var localStorageItem = JSON.parse(localStorage.getItem('user'));
        
        return this.db.rel.find('payroll')
            .then((data: any) => {
                this.payrolls = data.payrolls;
                if (this.searchText && this.searchText !== '') {
                    this.payrolls = FuseUtils.filterArrayByString(this.payrolls, this.searchText);
                }
                //this.onpayrollsChanged.next(this.payrolls);
                var userId = localStorageItem.id
                if (localStorageItem.usertype == "Admin") {
                    this.payrolls
                }
                else {
                    this.payrolls = this.payrolls.filter(data => data.name_Id == userId)
                    
                }
                return Promise.resolve(this.payrolls);
                //return data.payrolls ? data.payrolls : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a payroll
     * @param {payroll} payroll
     *
     * @return Promise<payroll>
     */
    getPayroll(id): Promise<Payroll> {
        return this.db.rel.find('payroll', id)
            .then((data: any) => {
                return data && data.payrolls ? data.payrolls[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected payroll by id
     * @param id
     */
    toggleSelectedPayroll(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedPayrolls.length > 0) {
            const index = this.selectedPayrolls.indexOf(id);

            if (index !== -1) {
                this.selectedPayrolls.splice(index, 1);

                // Trigger the next event
                this.onSelectedPayrollsChanged.next(this.selectedPayrolls);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedPayrolls.push(id);

        // Trigger the next event
        this.onSelectedPayrollsChanged.next(this.selectedPayrolls);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedPayrolls.length > 0) {
            this.deselectPayrolls();
        }
        else {
            this.selectPayrolls();
        }
    }

    selectPayrolls(filterParameter?, filterValue?) {
        this.selectedPayrolls = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedPayrolls = [];
            this.payrolls.map(payroll => {
                this.selectedPayrolls.push(payroll.id);
            });
        }
        else {
            /* this.selectedpayrolls.push(...
                 this.payrolls.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedPayrollsChanged.next(this.selectedPayrolls);
    }





    deselectPayrolls() {
        this.selectedPayrolls = [];

        // Trigger the next event
        this.onSelectedPayrollsChanged.next(this.selectedPayrolls);
    }

    deletePayroll(payroll) {
        this.db.rel.del('payroll', payroll)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const payrollIndex = this.payrolls.indexOf(payroll);
                this.payrolls.splice(payrollIndex, 1);
                this.onPayrollsChanged.next(this.payrolls);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedPayrolls() {
        for (const payrollId of this.selectedPayrolls) {
            const payroll = this.payrolls.find(_payroll => {
                return _payroll.id === payrollId;
            });

            this.db.rel.del('payroll', payroll)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const payrollIndex = this.payrolls.indexOf(payroll);
                    this.payrolls.splice(payrollIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onPayrollsChanged.next(this.payrolls);
        this.deselectPayrolls();
    }
}
