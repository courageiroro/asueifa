import { Component, OnInit } from '@angular/core';
import { PayrollsService } from '../payrolls.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FusePayrollsSelectedBarComponent implements OnInit
{
    selectedPayrolls: string[];
    hasSelectedPayrolls: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private payrollsService: PouchService,
        public dialog: MdDialog
    )
    {
        this.payrollsService.onSelectedPayrollsChanged
            .subscribe(selectedPayrolls => {
                this.selectedPayrolls = selectedPayrolls;
                setTimeout(() => {
                    this.hasSelectedPayrolls = selectedPayrolls.length > 0;
                    this.isIndeterminate = (selectedPayrolls.length !== this.payrollsService.payrolls.length && selectedPayrolls.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.payrollsService.selectPayrolls();
    }

    deselectAll()
    {
        this.payrollsService.deselectPayrolls();
    }

    deleteSelectedPayrolls()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected Payroll?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.payrollsService.deleteSelectedPayrolls();
            }
            this.confirmDialogRef = null;
        });
    }

}
