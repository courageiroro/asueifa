import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Pharmacist } from '../pharmacist.model';

@Component({
    selector: 'fuse-pharmacists-pharmacist-form-dialog',
    templateUrl: './pharmacist-form.component.html',
    styleUrls: ['./pharmacist-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FusePharmacistsPharmacistFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    pharmacistForm: FormGroup;
    action: string;
    pharmacist: Pharmacist;

    constructor(
        public dialogRef: MdDialogRef<FusePharmacistsPharmacistFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Pharmacist';
            this.pharmacist = data.pharmacist;
        }
        else {
            this.dialogTitle = 'New Pharmacist';
            this.pharmacist = {
                id: '',
                rev: '',
                name: '',
                email: '',
                password: '',
                address: '',
                phone: '',
            }
        }

        this.pharmacistForm = this.createPharmacistForm();
    }

    ngOnInit() {
    }

    createPharmacistForm() {
        return this.formBuilder.group({
            id: [this.pharmacist.id],
            rev: [this.pharmacist.rev],
            name: [this.pharmacist.name],
            email: [this.pharmacist.email],
            password: [this.pharmacist.password],
            address: [this.pharmacist.address],
            phone: [this.pharmacist.phone],

        });
    }
}
