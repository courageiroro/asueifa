export interface Pharmacist {
    id: string,
    rev: string,
    name: string,
    email: string,
    password: string,
    address: string,
    phone: string,
}
