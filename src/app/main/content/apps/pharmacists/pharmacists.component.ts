import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { PharmacistsService } from './pharmacists.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-pharmacists',
    templateUrl  : './pharmacists.component.html',
    styleUrls    : ['./pharmacists.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FusePharmacistsComponent implements OnInit
{
    hasSelectedPharmacists: boolean;
    searchInput: FormControl;

    constructor(private pharmacistsService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.pharmacistsService.onSelectedPharmacistsChanged
            .subscribe(selectedPharmacists => {
                this.hasSelectedPharmacists = selectedPharmacists.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.pharmacistsService.onSearchTextChanged.next(searchText);
            });
    }

}
