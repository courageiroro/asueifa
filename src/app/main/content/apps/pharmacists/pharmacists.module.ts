import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FusePharmacistsComponent } from './pharmacists.component';
import { PharmacistsService } from './pharmacists.service';
import { FusePharmacistsPharmacistListComponent } from './pharmacist-list/pharmacist-list.component';
import { FusePharmacistsSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FusePharmacistsPharmacistFormDialogComponent } from './pharmacist-form/pharmacist-form.component';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/pharmacists',
        component: FusePharmacistsComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            pharmacists: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FusePharmacistsComponent,
        FusePharmacistsPharmacistListComponent,
        FusePharmacistsSelectedBarComponent,
        FusePharmacistsPharmacistFormDialogComponent
    ],
    providers      : [
        AuthGuard,
        PouchService,
        PharmacistsService
    ],
    entryComponents: [FusePharmacistsPharmacistFormDialogComponent]
})
export class FusePharmacistsModule
{
}
