/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Pharmacist } from './pharmacist.model';
import { Schema } from '../schema';

@Injectable()
export class PharmacistsService {
    public name = "";
    public sCredentials;

    onPharmacistsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedPharmacistsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    pharmacists: Pharmacist[];
    user: any;
    selectedPharmacists: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The pharmacists App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getPharmacists()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getPharmacists();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getPharmacists();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('asueifai', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/asueifai';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/asueifai';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }

    /***********
     * pharmacist
     **********/
    /**
     * Save a pharmacist
     * @param {pharmacist} pharmacist
     *
     * @return Promise<pharmacist>
     */
    savePharmacist(pharmacist: Pharmacist): Promise<Pharmacist> {
        pharmacist.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('pharmacist', pharmacist)
            .then((data: any) => {

                if (data && data.pharmacists && data.pharmacists
                [0]) {
                    return data.pharmacists[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a pharmacist
    * @param {pharmacist} pharmacist
    *
    * @return Promise<pharmacist>
    */
    updatePharmacist(pharmacist: Pharmacist) {

        return this.db.rel.save('pharmacist', pharmacist)
            .then((data: any) => {
                if (data && data.pharmacists && data.pharmacists
                [0]) {
                    return data.pharmacists[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a pharmacist
     * @param {pharmacist} pharmacist
     *
     * @return Promise<boolean>
     */
    removePharmacist(pharmacist: Pharmacist): Promise<boolean> {
        return this.db.rel.del('pharmacist', pharmacist)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the pharmacists
     *
     * @return Promise<Array<pharmacist>>
     */
    getPharmacists(): Promise<Array<Pharmacist>> {
        return this.db.rel.find('pharmacist')
            .then((data: any) => {
                this.pharmacists = data.pharmacists;
                if (this.searchText && this.searchText !== '') {
                    this.pharmacists = FuseUtils.filterArrayByString(this.pharmacists, this.searchText);
                }
                //this.onpharmacistsChanged.next(this.pharmacists);
                return Promise.resolve(this.pharmacists);
                //return data.pharmacists ? data.pharmacists : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a pharmacist
     * @param {pharmacist} pharmacist
     *
     * @return Promise<pharmacist>
     */
    getPharmacist(pharmacist: Pharmacist): Promise<Pharmacist> {
        return this.db.rel.find('pharmacist', pharmacist.id)
            .then((data: any) => {
                return data && data.pharmacists ? data.pharmacists[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected pharmacist by id
     * @param id
     */
    toggleSelectedPharmacist(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedPharmacists.length > 0) {
            const index = this.selectedPharmacists.indexOf(id);

            if (index !== -1) {
                this.selectedPharmacists.splice(index, 1);

                // Trigger the next event
                this.onSelectedPharmacistsChanged.next(this.selectedPharmacists);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedPharmacists.push(id);

        // Trigger the next event
        this.onSelectedPharmacistsChanged.next(this.selectedPharmacists);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedPharmacists.length > 0) {
            this.deselectPharmacists();
        }
        else {
            this.selectPharmacists();
        }
    }

    selectPharmacists(filterParameter?, filterValue?) {
        this.selectedPharmacists = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedPharmacists = [];
            this.pharmacists.map(pharmacist => {
                this.selectedPharmacists.push(pharmacist.id);
            });
        }
        else {
            /* this.selectedpharmacists.push(...
                 this.pharmacists.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedPharmacistsChanged.next(this.selectedPharmacists);
    }





    deselectPharmacists() {
        this.selectedPharmacists = [];

        // Trigger the next event
        this.onSelectedPharmacistsChanged.next(this.selectedPharmacists);
    }

    deletePharmacist(pharmacist) {
        this.db.rel.del('pharmacist', pharmacist)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const pharmacistIndex = this.pharmacists.indexOf(pharmacist);
                this.pharmacists.splice(pharmacistIndex, 1);
                this.onPharmacistsChanged.next(this.pharmacists);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedPharmacists() {
        for (const pharmacistId of this.selectedPharmacists) {
            const pharmacist = this.pharmacists.find(_pharmacist => {
                return _pharmacist.id === pharmacistId;
            });

            this.db.rel.del('pharmacist', pharmacist)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const pharmacistIndex = this.pharmacists.indexOf(pharmacist);
                    this.pharmacists.splice(pharmacistIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onPharmacistsChanged.next(this.pharmacists);
        this.deselectPharmacists();
    }
}
