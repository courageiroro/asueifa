import { Component, OnInit } from '@angular/core';
import { PharmacistsService } from '../pharmacists.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FusePharmacistsSelectedBarComponent implements OnInit
{
    selectedPharmacists: string[];
    hasSelectedPharmacists: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private pharmacistsService: PouchService,
        public dialog: MdDialog
    )
    {
        this.pharmacistsService.onSelectedPharmacistsChanged
            .subscribe(selectedPharmacists => {
                this.selectedPharmacists = selectedPharmacists;
                setTimeout(() => {
                    this.hasSelectedPharmacists = selectedPharmacists.length > 0;
                    this.isIndeterminate = (selectedPharmacists.length !== this.pharmacistsService.pharmacists.length && selectedPharmacists.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.pharmacistsService.selectPharmacists();
    }

    deselectAll()
    {
        this.pharmacistsService.deselectPharmacists();
    }

    deleteSelectedPharmacists()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected Pharmacists?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.pharmacistsService.deleteSelectedPharmacists();
            }
            this.confirmDialogRef = null;
        });
    }

}
