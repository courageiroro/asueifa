import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg'
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Prescription } from '../prescription.model';
import { StaffsService } from '../../staffs/staffs.service';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-prescriptions-prescription-form-dialog',
    templateUrl: './prescription-form.component.html',
    styleUrls: ['./prescription-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FusePrescriptionsPrescriptionFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    prescriptionForm: FormGroup;
    doctors: any;
    patients: any;
    action: string;
    prescription: Prescription;
    public nameId: string;
    public patientId: string;
    localStorageItem: any;
    disabled = false;

    constructor(
        public dialogRef: MdDialogRef<FusePrescriptionsPrescriptionFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        private staffService: PouchService
    ) {
        this.localStorageItem = JSON.parse(localStorage.getItem('user'));

        if(this.localStorageItem.usertype == 'Doctor'){
            this.disabled = false;
         }
         else {
             this.disabled = true;
         }

        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Prescription';
            this.prescription = data.prescription;
        }
        else {
            this.dialogTitle = 'New Prescription';
            this.prescription = {
                id: '',
                rev: '',
                timestamp: new Date(),
                doctor_id: '',
                patient_id: '',
                case_history: '',
                medication: '',
                note: '',
                nameId: '',
                patientId: ''
            }
        }

        this.prescriptionForm = this.createPrescriptionForm();
    }

    getPId(patient) {
        
        this.prescription.patientId = patient.id;
    }

    getDId(doctor) {
        
        this.prescription.nameId = doctor.id;
    }



    ngOnInit() {
        if (this.localStorageItem.usertype === "Doctor") {
          this.doctors = [{name:this.localStorageItem.name,id:this.localStorageItem.id}]
          
        }
        else{ 
          this.staffService.getDStaffs().then(res => {
                this.doctors = res;
            })
        }
        this.staffService.getPStaffs().then(res => {
            this.patients = res;
        })
    }

    createPrescriptionForm() {
        return this.formBuilder.group({
            id: [this.prescription.id],
            rev: [this.prescription.rev],
            timestamp: new Date(this.prescription.timestamp).toDateString(),
            doctor_id: [this.prescription.doctor_id],
            patient_id: [this.prescription.patient_id],
            nameId: [this.prescription.nameId],
            patientId: [this.prescription.patientId],
            case_history: [this.prescription.case_history],
            medication: [this.prescription.medication],
            note: [this.prescription.note]

        });
    }
}
