import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { PrescriptionsService } from '../prescriptions.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { FusePrescriptionsPrescriptionFormDialogComponent } from '../prescription-form/prescription-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Prescription } from '../prescription.model';
import { Diagnosis_reportsService } from '../../diagnosis_reports/diagnosis_reports.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Diagnosis_report } from '../../diagnosis_reports/diagnosis_report.model';
import {PouchService} from '../../../../../provider/pouch-service';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';

@Component({
    selector: 'fuse-prescriptions-prescription-list',
    templateUrl: './prescription-list.component.html',
    styleUrls: ['./prescription-list.component.scss']
})
export class FusePrescriptionsPrescriptionListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public prescriptions: Array<Prescription> = [];
    prescriptions2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'doctor', 'patient', 'timestamp', 'case', 'medication', 'note', 'buttons'];
    selectedPrescriptions: any[];
    checkboxes: {};
    Ddelete;
    id;
    disabled = false;

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;


    constructor(public dialog: MdDialog, public activateRoute: ActivatedRoute, public db: PouchService, public router: Router)
    { }

    ngOnInit() {
        var localStorageItem = JSON.parse(localStorage.getItem('user'))
        if(localStorageItem.usertype != 'Doctor'){
           this.disabled = true;
        }
        else {
            this.disabled = false;
        }
        this.dataSource = new FilesDataSource(this.db, this.activateRoute, this.paginator, this.sort);
        this._loadPrescriptions();
    }

    private _loadPrescriptions(): void {
        this.id = this.activateRoute.snapshot.params['id'];
        this.db.onPrescriptionsChanged.subscribe(data => {
            this.dataSource = new FilesDataSource(this.db, this.activateRoute, this.paginator, this.sort);
        })
        this.db.getPrescriptions()
            .then((prescriptions: Array<Prescription>) => {
                console.log(prescriptions);
                this.prescriptions = prescriptions;
                this.prescriptions = this.prescriptions.filter(data => data.patientId == this.id);
                this.prescriptions2 = new BehaviorSubject<any>(prescriptions);
                this.prescriptions2 = new BehaviorSubject<any>(this.prescriptions2.filter(data => data.patientId == this.id));
                console.log(this.prescriptions2)
                console.log(this.prescriptions)

                this.checkboxes = {};
                prescriptions.map(prescription => {
                    this.checkboxes[prescription.id] = false;
                });
                this.db.onSelectedPrescriptionsChanged.subscribe(selectedPrescriptions => {
                    for (const id in this.checkboxes) {
                        this.checkboxes[id] = selectedPrescriptions.includes(id);
                    }

                    this.selectedPrescriptions = selectedPrescriptions;
                });
                this.db.onSearchTextChanged.subscribe(data => {
                    this.dataSource = new FilesDataSource(this.db, this.activateRoute, this.paginator, this.sort);
                })
            });

    }

    newPrescription() {
        this.dialogRef = this.dialog.open(FusePrescriptionsPrescriptionFormDialogComponent, {
            panelClass: 'prescription-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
                var res = response.getRawValue()
                res.timestamp = new Date(res.timestamp).toISOString().substring(0, 10);
                this.db.savePrescription(res);
                this.dataSource = new FilesDataSource(this.db, this.activateRoute, this.paginator, this.sort);

            });

    }

    /*   getDiagnosis(prescription) {
          console.log(prescription);
          this.db2.getDiagnosis_report(prescription.id).then(res => {
              this.Ddelete = res
              console.log(res);
  
          })
      }
     
      viewDiagnosis_report(diagnosis_report) {
          this.router.navigate(['/apps/diagnosis_reportpages/:id', diagnosis_report.id]);
      } */

    editPrescription(prescription) {
        
        this.dialogRef = this.dialog.open(FusePrescriptionsPrescriptionFormDialogComponent, {
            panelClass: 'prescription-form-dialog',
            data: {
                prescription: prescription,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                var res = formData.getRawValue();
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':
                        res.timestamp = new Date(res.timestamp).toISOString().substring(0, 10);
                        this.db.updatePrescription(res);
                        
                        this.dataSource = new FilesDataSource(this.db, this.activateRoute, this.paginator, this.sort);

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        //this.deletePrescription(prescription);

                        break;
                }
            });
    }

    /**
     * Delete prescription
     */
    deletePrescription(prescription) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deletePrescription(prescription);
                this.dataSource = new FilesDataSource(this.db, this.activateRoute, this.paginator, this.sort);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(prescriptionId) {
        this.db.toggleSelectedPrescription(prescriptionId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    prescriptions2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    id
    constructor(private db: PouchService, public activateRoute: ActivatedRoute, private _paginator: MdPaginator, private sort: MdSort) {
        super();
        console.log(this.prescriptions2);
    }
   
    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            of(this.db.prescriptions).delay(5000),
            this.sort.mdSortChange,
            this._paginator.page,
        ];

        this.id = this.activateRoute.snapshot.params['id'];
        console.log(this.id);
        if(this.id !=":id"){
        var result = Observable.fromPromise(this.db.getPrescriptions().then(res => res.filter(data => data.patientId == this.id)));
        }
        else{
            var result = Observable.fromPromise(this.db.getPrescriptions());
        }

        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.prescriptions]));
        });
       /*  result.subscribe(x => {
            console.log(this.id);
           x = x.filter(data => data.patientId == this.id);
           console.log(x)
        });
        console.log(result);
        return result; */
    }

    disconnect() {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
          return data;
        }
    
        return data.sort((a, b) => {
          const isAsc = this.sort.direction === 'asc';
          switch (this.sort.active) {
            case 'patient': return compare(a.patient_id, b.patient_id, isAsc);
            case 'amount': return compare(+a.amount, +b.amount, isAsc);
            case 'id': return compare(+a.id, +b.id, isAsc);
            default: return 0;
          }
        });
      }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
