export interface Prescription {
    id: string,
    rev: string,
    timestamp: Date,
    doctor_id: string,
    patient_id: string,
    nameId: string;
    patientId: string;
    case_history: string,
    medication: string,
    note: string,
}

