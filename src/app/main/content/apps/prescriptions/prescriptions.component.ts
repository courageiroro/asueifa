import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { PrescriptionsService } from './prescriptions.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-prescriptions',
    templateUrl  : './prescriptions.component.html',
    styleUrls    : ['./prescriptions.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FusePrescriptionsComponent implements OnInit
{
    hasSelectedPrescriptions: boolean;
    searchInput: FormControl;

    constructor(private prescriptionsService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.prescriptionsService.onSelectedPrescriptionsChanged
            .subscribe(selectedPrescriptions => {
                this.hasSelectedPrescriptions = selectedPrescriptions.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.prescriptionsService.onSearchTextChanged.next(searchText);
            });
    }

}
