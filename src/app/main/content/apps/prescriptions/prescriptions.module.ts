import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FusePrescriptionsComponent } from './prescriptions.component';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg'
import { PrescriptionsService } from './prescriptions.service';
import { FusePrescriptionsPrescriptionListComponent } from './prescription-list/prescription-list.component';
import { FusePrescriptionsSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FusePrescriptionsPrescriptionFormDialogComponent } from './prescription-form/prescription-form.component';
import {StaffsService} from './../staffs/staffs.service';
import { Diagnosis_reportsService } from './../diagnosis_reports/diagnosis_reports.service';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/prescriptions/:id',
        component: FusePrescriptionsComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            prescriptions: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes),
        FroalaEditorModule.forRoot(),
        FroalaViewModule.forRoot()
    ],
    declarations   : [
        FusePrescriptionsComponent,
        FusePrescriptionsPrescriptionListComponent,
        FusePrescriptionsSelectedBarComponent,
        FusePrescriptionsPrescriptionFormDialogComponent
    ],
    providers      : [
        PrescriptionsService,
        StaffsService,
        PouchService,
        AuthGuard,
        Diagnosis_reportsService 
    ],
    entryComponents: [FusePrescriptionsPrescriptionFormDialogComponent]
})
export class FusePrescriptionsModule
{
}
