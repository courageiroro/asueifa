/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Prescription } from './prescription.model';
import { Schema } from '../schema';

@Injectable()
export class PrescriptionsService {
    public prescriptionname = "";
    public sCredentials;
    onPrescriptionsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedPrescriptionsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    prescriptions: Prescription[];
    user: any;
    selectedPrescriptions: string[] = [];

    searchText: string;
    filterBy: string;
    public id;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http, public activateRoute: ActivatedRoute,) {
        this.initDB(this.sCredentials);
    }

    /**
     * The prescriptions App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getPrescriptions()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getPrescriptions();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getPrescriptions();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('asueifai', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/asueifai';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/asueifai';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }


    /***********
     * prescription
     **********/
    /**
     * Save a prescription
     * @param {prescription} prescription
     *
     * @return Promise<prescription>
     */
    savePrescription(prescription: Prescription): Promise<Prescription> {
        prescription.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('prescription', prescription)
            .then((data: any) => {

                if (data && data.prescriptions && data.prescriptions
                [0]) {
                    return data.prescriptions[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a prescription
    * @param {prescription} Prescription
    *
    * @return Promise<prescription>
    */
    updatePrescription(prescription: Prescription) {

        return this.db.rel.save('prescription', prescription)
            .then((data: any) => {
                if (data && data.prescriptions && data.prescriptions
                [0]) {
                    
                    return data.prescriptions[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            }); 
    }

    /**
    * Update a Prescription
    * @param {Prescription} Prescription
    *
    * @return Promise<Prescription>
    */
    updatePrescription2(prescription: Prescription) {
        return this.db.rel.save('prescription', prescription)
            .then((data: any) => {
                if (data && data.prescriptions && data.prescriptions
                [0]) {
                    console.log('Update')
                    
                    return data.prescriptions[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a prescription
     * @param {prescription} prescription
     *
     * @return Promise<boolean>
     */
    removePrescription(prescription: Prescription): Promise<boolean> {
         return this.db.rel.del('prescription', prescription)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            }); 
    }

    /**
     * Return all the prescriptions
     *
     * @return Promise<Array<prescription>>
     */
    getPrescriptions(): Promise<Array<Prescription>> {
        var localStorageItem = JSON.parse(localStorage.getItem('user'));
        var userType = localStorageItem.usertype;
         
        return this.db.rel.find('prescription')
            .then((data: any) => {
                console.log(this.id);
                this.prescriptions = data.prescriptions;
                if (this.searchText && this.searchText !== '') {
                    this.prescriptions = FuseUtils.filterArrayByString(this.prescriptions, this.searchText);
                }
                //this.onprescriptionsChanged.next(this.prescriptions);
                if (userType === "Doctor") {
                    var userId = localStorageItem.id
                    this.prescriptions = this.prescriptions.filter(data => data.nameId == userId)
                }
                return Promise.resolve(this.prescriptions);
                //return data.prescriptions ? data.prescriptions : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a prescription
     * @param {prescription} prescription
     *
     * @return Promise<prescription>
     */
    getPrescription(id): Promise<Prescription> {
        return this.db.rel.find('prescription', id)
            .then((data: any) => {
                return data && data.prescriptions ? data.prescriptions[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected prescription by id
     * @param id
     */
    toggleSelectedPrescription(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedPrescriptions.length > 0) {
            const index = this.selectedPrescriptions.indexOf(id);

            if (index !== -1) {
                this.selectedPrescriptions.splice(index, 1);

                // Trigger the next event
                this.onSelectedPrescriptionsChanged.next(this.selectedPrescriptions);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedPrescriptions.push(id);

        // Trigger the next event
        this.onSelectedPrescriptionsChanged.next(this.selectedPrescriptions);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedPrescriptions.length > 0) {
            this.deselectPrescriptions();
        }
        else {
            this.selectPrescriptions();
        }
    }

    selectPrescriptions(filterParameter?, filterValue?) {
        this.selectedPrescriptions = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedPrescriptions = [];
            this.prescriptions.map(prescription => {
                this.selectedPrescriptions.push(prescription.id);
            });
        }
        else {
            /* this.selectedprescriptions.push(...
                 this.prescriptions.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedPrescriptionsChanged.next(this.selectedPrescriptions);
    }





    deselectPrescriptions() {
        this.selectedPrescriptions = [];

        // Trigger the next event
        this.onSelectedPrescriptionsChanged.next(this.selectedPrescriptions);
    }

    deletePrescription(prescription) {
        this.db.rel.del('prescription', prescription)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const prescriptionIndex = this.prescriptions.indexOf(prescription);
                this.prescriptions.splice(prescriptionIndex, 1);
                this.onPrescriptionsChanged.next(this.prescriptions);
            }).catch((err: any) => {
                console.error(err);
            }); 
    }


    deleteSelectedPrescriptions() {
        for (const prescriptionId of this.selectedPrescriptions) {
            const prescription = this.prescriptions.find(_prescription => {
                return _prescription.id === prescriptionId;
            });

            this.db.rel.del('prescription', prescription)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const prescriptionIndex = this.prescriptions.indexOf(prescription);
                    this.prescriptions.splice(prescriptionIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onPrescriptionsChanged.next(this.prescriptions);
        this.deselectPrescriptions();
    }
}
