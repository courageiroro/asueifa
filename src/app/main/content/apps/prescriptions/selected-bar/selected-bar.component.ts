import { Component, OnInit } from '@angular/core';
import { PrescriptionsService } from '../prescriptions.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FusePrescriptionsSelectedBarComponent implements OnInit
{
    selectedPrescriptions: string[];
    hasSelectedPrescriptions: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private prescriptionsService: PouchService,
        public dialog: MdDialog
    )
    {
        this.prescriptionsService.onSelectedPrescriptionsChanged
            .subscribe(selectedPrescriptions => {
                this.selectedPrescriptions = selectedPrescriptions;
                setTimeout(() => {
                    this.hasSelectedPrescriptions = selectedPrescriptions.length > 0;
                    this.isIndeterminate = (selectedPrescriptions.length !== this.prescriptionsService.prescriptions.length && selectedPrescriptions.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.prescriptionsService.selectPrescriptions();
    }

    deselectAll()
    {
        this.prescriptionsService.deselectPrescriptions();
    }

    deleteSelectedPrescriptions()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected prescriptiones?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.prescriptionsService.deleteSelectedPrescriptions();
            }
            this.confirmDialogRef = null;
        });
    }

}
