import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Receipt } from '../receipt.model';

@Component({
    selector: 'fuse-receipts-receipt-form-dialog',
    templateUrl: './receipt-form.component.html',
    styleUrls: ['./receipt-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseReceiptsReceiptFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    receiptForm: FormGroup;
    action: string;
    receipt: Receipt;

    constructor(
        public dialogRef: MdDialogRef<FuseReceiptsReceiptFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit receipt';
            this.receipt = data.receipt;
            
        }
        else {
            this.dialogTitle = 'New receipt';
            this.receipt = {
                id: '',
                rev: '',
                invoice_number: '',
                patient_id: '',
                title: '',
                invoice_entries: '',
                creation_timestamp: '',
                due_timestamp: '',
                status: '',
                vat_percentage: '',
                discount_amount: ''
            }
        }

        this.receiptForm = this.createReceiptForm();
    }

    ngOnInit() {
    }

    createReceiptForm() {
        return this.formBuilder.group({
            iid: [this.receipt.id],
            rev: [this.receipt.rev],
            invoice_number: [this.receipt.invoice_number],
            patient_id: [this.receipt.patient_id],
            title: [this.receipt.title],
            invoice_entries: [this.receipt.invoice_entries],
            creation_timestamp: [this.receipt.creation_timestamp],
            due_timestamp: [this.receipt.due_timestamp],
            status: [this.receipt.status],
            vat_percentage: [this.receipt.vat_percentage],
            discount_amount: [this.receipt.discount_amount]

        });
    }
}
