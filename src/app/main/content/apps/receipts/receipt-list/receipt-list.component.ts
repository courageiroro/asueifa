import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ReceiptsService } from '../receipts.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { FuseReceiptsReceiptFormDialogComponent } from '../receipt-form/receipt-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Receipt } from '../receipt.model';
import {PouchService} from '../../../../../provider/pouch-service';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';

@Component({
    selector: 'fuse-receipts-receipt-list',
    templateUrl: './receipt-list.component.html',
    styleUrls: ['./receipt-list.component.scss']
})
export class FuseReceiptsReceiptListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public receipts: Array<Receipt> = [];
    receipts2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'receiptname', 'buttons'];
    selectedReceipts: any[];
    checkboxes: {};
    user: any;

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;

    constructor(public dialog: MdDialog, public db: PouchService) {
        /* this.db.onreceiptsChanged.subscribe(receipts => {
 
             this.receipts = receipts;
 
             this.checkboxes = {};
             receipts.map(receipt => {
                 this.checkboxes[receipt.id] = false;
             });
         });
 
         this.db.onSelectedreceiptsChanged.subscribe(selectedreceipts => {
             for (const id in this.checkboxes) {
                 this.checkboxes[id] = selectedreceipts.includes(id);
             }
             this.selectedreceipts = selectedreceipts;
         });
 
         this.db.onUserDataChanged.subscribe(user => {
             this.selectedreceipts = user;
         });*/
    }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        this._loadReceipts();
    }

    private _loadReceipts(): void {
        this.db.onReceiptsChanged.subscribe(data => {
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })

        this.db.getReceipts()
            .then((receipts: Array<Receipt>) => {
                this.receipts = receipts;
                this.receipts2 = new BehaviorSubject<any>(receipts);
                //console.log(this.receipts2);

                this.checkboxes = {};
                receipts.map(receipt => {
                    this.checkboxes[receipt.id] = false;
                });
                this.db.onSelectedReceiptsChanged.subscribe(selectedReceipts => {
                    for (const id in this.checkboxes) {
                        this.checkboxes[id] = selectedReceipts.includes(id);
                    }

                    this.selectedReceipts = selectedReceipts;
                });
                this.db.onSearchTextChanged.subscribe(data => {
                    this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                })
            });

    }

    newReceipt() {

        this.dialogRef = this.dialog.open(FuseReceiptsReceiptFormDialogComponent, {
            panelClass: 'receipt-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }

                this.db.saveReceipt(response.getRawValue());                
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

            });

    }

    editReceipt(receipt) {
        this.dialogRef = this.dialog.open(FuseReceiptsReceiptFormDialogComponent, {
            panelClass: 'receipt-form-dialog',
            data: {
                receipt: receipt,
                action: 'edit'
            }
        });

         this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) { 
                    /**
                     * Save
                     */
                     case 'save':

                        this.db.updateReceipt(formData.getRawValue());
                        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

                        break; 
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteReceipt(receipt);

                        break;
                }
            }); 
    }

    /**
     * Delete receipt
     */
    deleteReceipt(receipt) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deleteReceipt(receipt);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
            }
            this.confirmDialogRef = null;
        });

    }


    onSelectedChange(receiptId) {
        this.db.toggleSelectedReceipt(receiptId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    receipts2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            of(this.db.receipts).delay(5000),
            this.sort.mdSortChange,
            this._paginator.page,
        ];

        var result = Observable.fromPromise(this.db.getReceipts());
       /*  result.subscribe(x => console.log(x), e => console.error(e));
        return result; */

        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.receipts]));
        });
    }

    disconnect() {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
          return data;
        }
    
        return data.sort((a, b) => {
          const isAsc = this.sort.direction === 'asc';
          switch (this.sort.active) {
            case 'receiptname': return compare(a.receiptname, b.receiptname, isAsc);
            case 'amount': return compare(+a.amount, +b.amount, isAsc);
            case 'id': return compare(+a.id, +b.id, isAsc);
            default: return 0;
          }
        });
      }
    
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
