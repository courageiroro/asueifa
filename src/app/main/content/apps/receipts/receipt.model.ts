export interface Receipt {
    id: string,
    rev: string,
    invoice_number: string,
    patient_id: string,
    title: string,
    invoice_entries: string,
    creation_timestamp: string,
    due_timestamp: string,
    status: string,
    vat_percentage: string,
    discount_amount: string,
}