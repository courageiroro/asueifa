import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ReceiptsService } from './receipts.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-receipts',
    templateUrl  : './receipts.component.html',
    styleUrls    : ['./receipts.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FuseReceiptsComponent implements OnInit
{
    hasSelectedReceipts: boolean;
    searchInput: FormControl;

    constructor(private receiptsService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.receiptsService.onSelectedReceiptsChanged
            .subscribe(selectedReceipts => {
                this.hasSelectedReceipts = selectedReceipts.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.receiptsService.onSearchTextChanged.next(searchText);
            });
    }

}
