import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseReceiptsComponent } from './receipts.component';
import { ReceiptsService } from './receipts.service';
import { FuseReceiptsReceiptListComponent } from './receipt-list/receipt-list.component';
import { FuseReceiptsSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseReceiptsReceiptFormDialogComponent } from './receipt-form/receipt-form.component';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/receipts',
        component: FuseReceiptsComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            receipts: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseReceiptsComponent,
        FuseReceiptsReceiptListComponent,
        FuseReceiptsSelectedBarComponent,
        FuseReceiptsReceiptFormDialogComponent
    ],
    providers      : [
        AuthGuard,
        PouchService,
        ReceiptsService
    ],
    entryComponents: [FuseReceiptsReceiptFormDialogComponent]
})
export class FuseReceiptsModule
{
}
