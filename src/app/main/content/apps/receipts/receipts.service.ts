/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Receipt } from './receipt.model';
import { Schema } from '../schema';

@Injectable()
export class ReceiptsService {
    public receipt = "";
    public sCredentials;

    onReceiptsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedReceiptsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    receipts: Receipt[];
    user: any;
    selectedReceipts: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The routes App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getReceipts()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getReceipts();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getReceipts();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('asueifai', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/asueifai';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/asueifai';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }

    /***********
     * receipt
     **********/
    /**
     * Save a receipt
     * @param {receipt} receipt
     *
     * @return Promise<receipt>
     */
    saveReceipt(receipt: Receipt): Promise<Receipt> {
        receipt.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('receipt', receipt)
            .then((data: any) => {

                if (data && data.receipts && data.receipts
                [0]) {
                    return data.receipts[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a receipt
    * @param {receipt} receipt
    *
    * @return Promise<receipt>
    */
    updateReceipt(receipt: Receipt) {
        return this.db.rel.save('receipt', receipt)
            .then((data: any) => {
                if (data && data.receipts && data.receipts
                [0]) {
                    return data.receipts[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a receipt
     * @param {receipt} receipt
     *
     * @return Promise<boolean>
     */
    removeReceipt(receipt: Receipt): Promise<boolean> {
        return this.db.rel.del('receipt', receipt)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the receipts
     *
     * @return Promise<Array<receipt>>
     */
    getReceipts(): Promise<Array<Receipt>> {
        return this.db.rel.find('receipt')
            .then((data: any) => {
                this.receipts = data.receipts;
                //this.onreceiptsChanged.next(this.receipts);
                if (this.searchText && this.searchText !== '') {
                    this.receipts = FuseUtils.filterArrayByString(this.receipts, this.searchText);
                }
                return Promise.resolve(this.receipts);
                //return data.receipts ? data.receipts : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }
    /**
     * Read a receipt
     * @param {receipt} receipt
     *
     * @return Promise<receipt>
     */
    getReceipt(receipt: Receipt): Promise<Receipt> {
        return this.db.rel.find('receipt', receipt.id)
            .then((data: any) => {
                return data && data.receipts ? data.receipts[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected receipt by id
     * @param id
     */
    toggleSelectedReceipt(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedReceipts.length > 0) {
            const index = this.selectedReceipts.indexOf(id);

            if (index !== -1) {
                this.selectedReceipts.splice(index, 1);

                // Trigger the next event
                this.onSelectedReceiptsChanged.next(this.selectedReceipts);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedReceipts.push(id);

        // Trigger the next event
        this.onSelectedReceiptsChanged.next(this.selectedReceipts);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedReceipts.length > 0) {
            this.deselectReceipts();
        }
        else {
            this.selectReceipts();
        }
    }

    selectReceipts(filterParameter?, filterValue?) {
        this.selectedReceipts = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedReceipts = [];
            this.receipts.map(receipt => {
                this.selectedReceipts.push(receipt.id);
            });
        }
        else {
            /* this.selectedroutes.push(...
                 this.routes.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedReceiptsChanged.next(this.selectedReceipts);
    }





    deselectReceipts() {
        this.selectedReceipts = [];

        // Trigger the next event
        this.onSelectedReceiptsChanged.next(this.selectedReceipts);
    }

    deleteReceipt(receipt) {
        this.db.rel.del('receipt', receipt)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const receiptIndex = this.receipts.indexOf(receipt);
                this.receipts.splice(receiptIndex, 1);
                this.onReceiptsChanged.next(this.receipts);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedReceipts() {
        for (const receiptId of this.selectedReceipts) {
            const receipt = this.receipts.find(_receipt => {
                return _receipt.id === receiptId;
            });

            this.db.rel.del('receipt', receipt)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const receiptIndex = this.receipts.indexOf(receipt);
                    this.receipts.splice(receiptIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onReceiptsChanged.next(this.receipts);
        this.deselectReceipts();
    }
}
