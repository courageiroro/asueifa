import { Component, OnInit } from '@angular/core';
import { ReceiptsService } from '../receipts.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls: ['./selected-bar.component.scss'],
})
export class FuseReceiptsSelectedBarComponent implements OnInit {
    
    selectedReceipts: string[];
    hasSelectedReceipts: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private receiptsService: PouchService,
        public dialog: MdDialog,
    ) {
        this.receiptsService.onSelectedReceiptsChanged
            .subscribe(selectedReceipts => {
                this.selectedReceipts = selectedReceipts;
                setTimeout(() => {
                    this.hasSelectedReceipts = selectedReceipts.length > 0;
                    this.isIndeterminate = (selectedReceipts.length !== this.receiptsService.receipts.length && selectedReceipts.length > 0);
                }, 0);
            });

    }

    ngOnInit() {
        
    }

   
    selectAll() {
        this.receiptsService.selectReceipts();
    }

    deselectAll() {
        this.receiptsService.deselectReceipts();
    }

    deleteSelectedreceipts() {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected Receipts?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.receiptsService.deleteSelectedReceipts();
            }
            //this.list.dataSource = new FilesDataSource(this.db);
            this.confirmDialogRef = null;
            
        });
    }

}
