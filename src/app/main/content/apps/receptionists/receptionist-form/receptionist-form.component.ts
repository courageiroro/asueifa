import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Receptionist } from '../receptionist.model';


@Component({
    selector: 'fuse-receptionists-receptionist-form-dialog',
    templateUrl: './receptionist-form.component.html',
    styleUrls: ['./receptionist-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseReceptionistsReceptionistFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    receptionistForm: FormGroup;
    action: string;
    receptionist: Receptionist;

    constructor(
        public dialogRef: MdDialogRef<FuseReceptionistsReceptionistFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Receptionist';
            this.receptionist = data.receptionist;
        }
        else {
            this.dialogTitle = 'New Receptionist';
            this.receptionist = {
                id: '',
                rev: '',
                name: '',
                email: '',
                password: '',
                address: '',
                phone: '',
            }
        }

        this.receptionistForm = this.createReceptionistForm();
    }

    ngOnInit() {
    }

    createReceptionistForm() {
        return this.formBuilder.group({
            id: [this.receptionist.id],
            rev: [this.receptionist.rev],
            name: [this.receptionist.name],
            email: [this.receptionist.email],
            password: [this.receptionist.password],
            address: [this.receptionist.address],
            phone: [this.receptionist.phone],

        });
    }
}
