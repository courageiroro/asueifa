import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ReceptionistsService } from '../receptionists.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject} from 'rxjs/Rx';
import { FuseReceptionistsReceptionistFormDialogComponent } from '../receptionist-form/receptionist-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Receptionist } from '../receptionist.model';
import {PouchService} from '../../../../../provider/pouch-service';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';

@Component({
    selector   : 'fuse-receptionists-receptionist-list',
    templateUrl: './receptionist-list.component.html',
    styleUrls  : ['./receptionist-list.component.scss']
})
export class FuseReceptionistsReceptionistListComponent implements OnInit
{
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public receptionists: Array<Receptionist> = [];
    receptionists2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'name','address','phone','email','password', 'buttons'];
    selectedReceptionists: any[];
    checkboxes: {};

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;


    constructor(public dialog: MdDialog, public db:PouchService)
    { }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        this._loadReceptionists();
    }

    private _loadReceptionists(): void {
         this.db.onReceptionistsChanged.subscribe(data =>{
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        this.db.getReceptionists()
        .then((receptionists: Array<Receptionist>) => {
            this.receptionists = receptionists;
            this.receptionists2= new BehaviorSubject<any>(receptionists);
            //console.log(this.receptionists2);

            this.checkboxes = {};
            receptionists.map(receptionist => {
                this.checkboxes[receptionist.id] = false;
            });
             this.db.onSelectedReceptionistsChanged.subscribe(selectedReceptionists => {
                for ( const id in this.checkboxes )
                {
                    this.checkboxes[id] = selectedReceptionists.includes(id);
                }

                this.selectedReceptionists = selectedReceptionists;
            });
        this.db.onSearchTextChanged.subscribe(data =>{
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        });
        
    }

    newReceptionist()
    {
        this.dialogRef = this.dialog.open(FuseReceptionistsReceptionistFormDialogComponent, {
            panelClass: 'receptionist-form-dialog',
            data      : {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if ( !response )
                {
                    return;
                }

                this.db.saveReceptionist(response.getRawValue());
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

            });

    }

    editReceptionist(receptionist)
    {
        this.dialogRef = this.dialog.open(FuseReceptionistsReceptionistFormDialogComponent, {
            panelClass: 'receptionist-form-dialog',
            data      : {
                receptionist: receptionist,
                action : 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch ( actionType )
                {
                    /**
                     * Save
                     */
                    case 'save':

                        this.db.updateReceptionist(formData.getRawValue());
                        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteReceptionist(receptionist);

                        break;
                }
            });
    }

    /**
     * Delete receptionist
     */
    deleteReceptionist(receptionist)
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.db.deleteReceptionist(receptionist);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(receptionistId)
    {
        this.db.toggleSelectedReceptionist(receptionistId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    receptionists2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort)
    {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]>
    {
        const displayDataChanges = [
            of(this.db.receptionists).delay(5000),
            this.sort.mdSortChange,
            this._paginator.page,
        ];


        var result = Observable.fromPromise(this.db.getReceptionists());
        /* result.subscribe(x => console.log(x), e => console.error(e));
        return result;  */   

        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.receptionists]));
        });
    }

    disconnect()
    {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
          return data;
        }
    
        return data.sort((a, b) => {
          const isAsc = this.sort.direction === 'asc';
          switch (this.sort.active) {
            case 'name': return compare(a.name, b.name, isAsc);
            case 'amount': return compare(+a.amount, +b.amount, isAsc);
            case 'id': return compare(+a.id, +b.id, isAsc);
            default: return 0;
          }
        });
      }
}

 /** Simple sort comparator for example ID/Name columns (for client-side sorting). */
 function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
