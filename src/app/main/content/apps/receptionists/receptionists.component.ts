import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ReceptionistsService } from './receptionists.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-receptionists',
    templateUrl  : './receptionists.component.html',
    styleUrls    : ['./receptionists.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FuseReceptionistsComponent implements OnInit
{
    hasSelectedReceptionists: boolean;
    searchInput: FormControl;

    constructor(private receptionistsService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.receptionistsService.onSelectedReceptionistsChanged
            .subscribe(selectedReceptionists => {
                this.hasSelectedReceptionists = selectedReceptionists.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.receptionistsService.onSearchTextChanged.next(searchText);
            });
    }

}
