import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseReceptionistsComponent } from './receptionists.component';
import { ReceptionistsService } from './receptionists.service';
import { FuseReceptionistsReceptionistListComponent } from './receptionist-list/receptionist-list.component';
import { FuseReceptionistsSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseReceptionistsReceptionistFormDialogComponent } from './receptionist-form/receptionist-form.component';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/receptionists',
        component: FuseReceptionistsComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            receptionists: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseReceptionistsComponent,
        FuseReceptionistsReceptionistListComponent,
        FuseReceptionistsSelectedBarComponent,
        FuseReceptionistsReceptionistFormDialogComponent
    ],
    providers      : [
        AuthGuard,
        PouchService,
        ReceptionistsService
    ],
    entryComponents: [FuseReceptionistsReceptionistFormDialogComponent]
})
export class FuseReceptionistsModule
{
}
