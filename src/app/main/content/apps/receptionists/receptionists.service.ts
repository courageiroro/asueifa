/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
PouchDB.plugin(require('pouchdb-find'));
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('relational-pouch'));

import { Receptionist } from './receptionist.model';
import { Schema } from '../schema';

@Injectable()
export class ReceptionistsService {
    public name = "";
    public sCredentials;
    onReceptionistsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedReceptionistsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    receptionists: Receptionist[];
    user: any;
    selectedReceptionists: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The receptionists App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getReceptionists()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getReceptionists();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getReceptionists();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('asueifai', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/asueifai';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/asueifai';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }


    /***********
     * receptionist
     **********/
    /**
     * Save a receptionist
     * @param {receptionist} receptionist
     *
     * @return Promise<receptionist>
     */
    saveReceptionist(receptionist: Receptionist): Promise<Receptionist> {
        receptionist.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('receptionist', receptionist)
            .then((data: any) => {

                if (data && data.receptionists && data.receptionists
                [0]) {
                    return data.receptionists[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a receptionist
    * @param {receptionist} receptionist
    *
    * @return Promise<receptionist>
    */
    updateReceptionist(receptionist: Receptionist) {

        return this.db.rel.save('receptionist', receptionist)
            .then((data: any) => {
                if (data && data.receptionists && data.receptionists
                [0]) {
                    return data.receptionists[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a receptionist
     * @param {receptionist} receptionist
     *
     * @return Promise<boolean>
     */
    removeReceptionist(receptionist: Receptionist): Promise<boolean> {
        return this.db.rel.del('receptionist', receptionist)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the receptionists
     *
     * @return Promise<Array<receptionist>>
     */
    getReceptionists(): Promise<Array<Receptionist>> {
        return this.db.rel.find('receptionist')
            .then((data: any) => {
                this.receptionists = data.receptionists;
                if (this.searchText && this.searchText !== '') {
                    this.receptionists = FuseUtils.filterArrayByString(this.receptionists, this.searchText);
                }
                //this.onreceptionistsChanged.next(this.receptionists);
                return Promise.resolve(this.receptionists);
                //return data.receptionists ? data.receptionists : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a receptionist
     * @param {receptionist} receptionist
     *
     * @return Promise<receptionist>
     */
    getReceptionist(receptionist: Receptionist): Promise<Receptionist> {
        return this.db.rel.find('receptionist', receptionist.id)
            .then((data: any) => {
                return data && data.receptionists ? data.receptionists[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected receptionist by id
     * @param id
     */
    toggleSelectedReceptionist(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedReceptionists.length > 0) {
            const index = this.selectedReceptionists.indexOf(id);

            if (index !== -1) {
                this.selectedReceptionists.splice(index, 1);

                // Trigger the next event
                this.onSelectedReceptionistsChanged.next(this.selectedReceptionists);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedReceptionists.push(id);

        // Trigger the next event
        this.onSelectedReceptionistsChanged.next(this.selectedReceptionists);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedReceptionists.length > 0) {
            this.deselectReceptionists();
        }
        else {
            this.selectReceptionists();
        }
    }

    selectReceptionists(filterParameter?, filterValue?) {
        this.selectedReceptionists = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedReceptionists = [];
            this.receptionists.map(receptionist => {
                this.selectedReceptionists.push(receptionist.id);
            });
        }
        else {
            /* this.selectedreceptionists.push(...
                 this.receptionists.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedReceptionistsChanged.next(this.selectedReceptionists);
    }





    deselectReceptionists() {
        this.selectedReceptionists = [];

        // Trigger the next event
        this.onSelectedReceptionistsChanged.next(this.selectedReceptionists);
    }

    deleteReceptionist(receptionist) {
        this.db.rel.del('receptionist', receptionist)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const receptionistIndex = this.receptionists.indexOf(receptionist);
                this.receptionists.splice(receptionistIndex, 1);
                this.onReceptionistsChanged.next(this.receptionists);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedReceptionists() {
        for (const receptionistId of this.selectedReceptionists) {
            const receptionist = this.receptionists.find(_receptionist => {
                return _receptionist.id === receptionistId;
            });

            this.db.rel.del('receptionist', receptionist)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const receptionistIndex = this.receptionists.indexOf(receptionist);
                    this.receptionists.splice(receptionistIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onReceptionistsChanged.next(this.receptionists);
        this.deselectReceptionists();
    }
}
