import { Component, OnInit } from '@angular/core';
import { ReceptionistsService } from '../receptionists.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseReceptionistsSelectedBarComponent implements OnInit
{
    selectedReceptionists: string[];
    hasSelectedReceptionists: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private receptionistsService: PouchService,
        public dialog: MdDialog
    )
    {
        this.receptionistsService.onSelectedReceptionistsChanged
            .subscribe(selectedReceptionists => {
                this.selectedReceptionists = selectedReceptionists;
                setTimeout(() => {
                    this.hasSelectedReceptionists = selectedReceptionists.length > 0;
                    this.isIndeterminate = (selectedReceptionists.length !== this.receptionistsService.receptionists.length && selectedReceptionists.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.receptionistsService.selectReceptionists();
    }

    deselectAll()
    {
        this.receptionistsService.deselectReceptionists();
    }

    deleteSelectedReceptionists()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected Receptionists?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.receptionistsService.deleteSelectedReceptionists();
            }
            this.confirmDialogRef = null;
        });
    }

}
