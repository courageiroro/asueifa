import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Record } from '../record.model';
import { RecordsService } from '../records.service';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-records-record-form-dialog',
    templateUrl: './record-form.component.html',
    styleUrls: ['./record-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseRecordsRecordFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    recordForm: FormGroup;
    action: string;
    record: Record;
    public categorys;
    public types;
    category

    constructor(
        public dialogRef: MdDialogRef<FuseRecordsRecordFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        private db: PouchService
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit record';
            this.record = data.record;
        }
        else {
            this.dialogTitle = 'New record';
            this.record = {
                id: '',
                rev: '',
                customer: '',
            }
        }

        this.recordForm = this.createrecordForm();
    }

    ngOnInit() {
        this.categorys = ['General', 'Medical History', 'Social History', 'Medical Conditions', 'Current Medications', 'Family History', 'Notes']
        this.types = ['input', 'text', 'checkbox']
    }

    getCategory(){
       this.db.category = this.category
       
    }

    createrecordForm() {
        return this.formBuilder.group({
            id: [this.record.id],
            rev: [this.record.rev],
            customer: [this.record.customer]
        });
    }
}
