import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { RecordsService } from '../records.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { ConfigurerecordsService } from '../../configure_records/configure_records.service';
import { FuseRecordsRecordFormDialogComponent } from '../record-form/record-form.component';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Record } from '../record.model';
import { Subject } from 'rxjs/Subject';
import { Staff } from '../../staffs/staff.model';
import { Configurerecord } from '../../configure_records/configure_record.model';
import { Http, Headers, RequestOptions } from '@angular/http';
import { StaffsService } from '../../staffs/staffs.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { FuseRecordreceiptsRecordreceiptFormDialogComponent } from '../record-receipt/record-receipt.component';
import { remote, ipcRenderer } from 'electron';
import {PouchService} from '../../../../../provider/pouch-service';
/* declare const window: any;
const { remote } = window.require('electron');
const { BrowserWindow, dialog, shell } = remote;
const fs = window.require('fs'); */

@Component({
    selector: 'fuse-records-record-list',
    templateUrl: './record-list.component.html',
    styleUrls: ['./record-list.component.scss']
})
export class FuseRecordsRecordListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    nativeWindowOpen: true
    public records: Array<Record> = [];
    public configurerecords: Array<Configurerecord> = [];
    public staff: Staff;
    patients;
    patientName;
    patientAddress;
    patientPhone;
    patientEmail;
    window: Window;
    urlRecord;
    image;
    checkboxstate;
    pid;
    records2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    //dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'name', 'description', 'buttons'];
    selectedrecords: any[];
    online = true;
    checkboxes: {};
    record: any = {
        id: Math.round((new Date()).getTime()).toString(),
        rev: '',
        customer: ''
    };
    general = [];
    refresh: Subject<any> = new Subject();
    medicalhistory = [];
    socialhistory = [];
    medicalconditions = [];
    currentmedications = [];
    familyhistory = [];
    notes = [];
    recordss: Record[];
    typeofprint;
    image2;

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(public dialog: MdDialog, public _DomSanitizer: DomSanitizer, public db: PouchService, public http: Http, public activateRoute: ActivatedRoute, public router: Router) {

        let id = this.activateRoute.snapshot.params['id'];
        this.pid = id;
        this.db.getPStaffs().then(res => {
            this.click(event);
            this.checkboxstate = event.isTrusted
            
        })
        this.db.getPStaff2(id).then(res => {
            this.patients = res;
            
            this.patientName = res.name;
            this.patientAddress = res.address; 
            this.patientPhone = res.phone;
            this.patientEmail = res.email;
           
            if (res.image) {
                this.image = true;
                this.image2 = res.image;
                
            }
            else {
                this.image = false
            }
            
            if (this.patients.record == "") {
                this.record = {
                    id: Math.round((new Date()).getTime()).toString(),
                    rev: '',
                    customer: ''
                };
            }
            else {
                
                this.db.getRecord(this.patients.record).then(record => {
                    this.record = record;
                    
                })
            }
        })



    }

    ngOnInit() {
        //this.dataSource = new FilesDataSource(this.db);
        this._loadrecords();
    }

    private _loadrecords(): void {
        this.db.getConfigurerecords()
            .then(configurerecords => {
                this.configurerecords = configurerecords;
                //this.records2 = new BehaviorSubject<any>(records);
                this.general = this.configurerecords.filter(data => data.category == 'General');
                this.refresh.next(this.general);
                this.medicalhistory = this.configurerecords.filter(data => data.category == 'Medical History');
                this.socialhistory = this.configurerecords.filter(data => data.category == 'Social History');
                this.medicalconditions = this.configurerecords.filter(data => data.category == 'Medical Conditions');
                this.currentmedications = this.configurerecords.filter(data => data.category == 'Current Medications');
                this.familyhistory = this.configurerecords.filter(data => data.category == 'Family History');
                this.notes = this.configurerecords.filter(data => data.category == 'Notes');

                //console.log(this.records2);
            });

    }


    /*    onlineCheck() {
           this.online = true;
           this.http.get('https://sarutech.com/couchdblogin/auth/validate-username/xs2da2432252sda').subscribe(data => {
               this.online = true;
               console.log(data)
               console.log(this.online);
           },
               (err) => {
                   this.online = false;
                   console.log(this.online);
               });
       }
   
       click() {
           this.onlineCheck();
       } */

    newRecord() {
        this.dialogRef = this.dialog.open(FuseRecordsRecordFormDialogComponent, {
            panelClass: 'record-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
              
                if (this.db.category == "General") {
                    
                    this.general.push(response.getRawValue());
                }
                else if (this.db.category == "Medical History") {
                    this.medicalhistory.push(response.getRawValue());
                }
                else if (this.db.category == "Social History") {
                    this.socialhistory.push(response.getRawValue());
                }
                else if (this.db.category == "Medical Conditions") {
                    this.medicalconditions.push(response.getRawValue());
                }
                else if (this.db.category == "Current Medications") {
                    this.currentmedications.push(response.getRawValue());
                }
                else if (this.db.category == "Family History") {
                    this.familyhistory.push(response.getRawValue());
                }
                else if (this.db.category == "Notes") {
                    this.notes.push(response.getRawValue());
                }
                //this.db.saveRecord(response.getRawValue());
                this.refresh.next(true);
                //this.dataSource = new FilesDataSource(this.db);
                

            });

    }

    editRecord(record) {
        
        this.dialogRef = this.dialog.open(FuseRecordsRecordFormDialogComponent, {
            panelClass: 'record-form-dialog',
            data: {
                record: record,
                action: 'edit'

            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':

                        //this.db.updateRecord(formData.getRawValue());
                        //this.dataSource = new FilesDataSource(this.db);
                        
                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteRecord(record);

                        break;
                }
            });
    }

    /**
     * Delete record
     */
    deleteRecord(record) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deleteRecord(record);
                //this.dataSource = new FilesDataSource(this.db);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(recordId) {
        this.db.toggleSelectedRecord(recordId);
    }
    navConsultancy() {
        
        this.router.navigate(['apps/consultancy', this.patients.id]);
    }
    click(event) {
        
        this.checkboxstate = event;
    }

    submit() {
      
        this.patients.record = this.record.id;
        //this.customer.record = this.record.id;
        this.db.saveRecord(this.record, this.patients).then();
    }

    print() {
        /*   this.db.content = document.getElementById("printable").innerHTML;
          console.log(this.db.content) */
        /* window.document.write(this.db.content);
        window.document.close();
        window.print();
 */

        this.router.navigate(['pages/print', this.pid]);
    }

    printRecord() {

        /*  const mainWindow = new BrowserWindow({
                width: 800,
                height: 600,
                webPreferences: {
                    nativeWindowOpen: true
                }
            })
    
            mainWindow.loadURL('file://' + __dirname + 'dist/src/app/main/content/apps/records/record-receipt/record-receipt.component.ts');
            console.log(__dirname)
            mainWindow.show();
            setTimeout(function () {
                mainWindow.webContents.print();
            }, 1000); */


        /*  mainWindow.webContents.on('new-window', (event, url, frameName, disposition, options, additionalFeatures) => {
             if (frameName === 'Hospital Manager') {
                 // open window as modal
                 event.preventDefault()
                 Object.assign(options, {
                     modal: true,
                     parent: mainWindow,
                     width: 100,
                     height: 100
                 })
                 event.newGuest = new BrowserWindow(options)
             }
         })
  */
        // renderer process (mainWindow)
        /*   let modal = window.open('', 'Hospital Manager')
          modal.document.write('<h1>Hello</h1>') */
        /* 
                this.dialogRef = this.dialog.open(FuseRecordreceiptsRecordreceiptFormDialogComponent, {
                    panelClass: 'record-receipt-dialog',
                    data: {
                        pid: this.pid
                    }
                })
                this.dialogRef.afterClosed() */

        //this.db.hide = "hidden";
        var win: any;
        win = window;
        var printContent = document.getElementById('printArea').innerHTML;
        var printWindow = win.open('', '_blank', 'top=0,left=0,height=auto,width=auto');
        printWindow.document.open();
        printWindow.document.write(`<html><head><title>Patient Record `, `</title><style>
            .avatar {
                                 font-size: 12px;
                                 padding-bottom: 2px;
                                 border-radius: 50%;
                                 height: 50px;
                                 width: 50px
                              }
                              .username {
                                     padding-left: 6px;
                                 }
                                 .print {
                            display: none;
                         }
                         .print2 {
                            display: none;
                         }
                          .hide {
                                 display: none;
                                 }
                             .invoice-name {
                                     padding-left: 6px;
                                 }
                                 .margin {
                                 font-size: 12px;
                                 padding-bottom: 6px;
                                 margin-bottom: 12px;
                                 }
 
                                  .margin2 {
                                 font-size: 12px;
                                 padding-bottom: 6px;
                                 margin-bottom: 30px;
                                 margin-left: 30px;
                                 }
 
                              .date {
                                     padding-left: 6px;
                                 }
                                 .info {
                                 color: rgba(0, 0, 0, 0.54);
                                 line-height: 22px;
                                 font-size: 12px;
                                 padding-bottom: 12px;
                             }
                             .issuer {
                             margin-right: -58px;
                             padding-right: 66px;
                             }
                         #co-size {
                         visibility: hidden   
                                   }
                     .invoice-table {
                             margin-top: 64px;
                             font-size: 15px;
                             }
                         .text-right{
                             padding-left: 150px;
                         }
                         .invoice-table-footer {
                             margin: 32px 0 72px 0;
                             }
                         td.line{
                            border-bottom: 1px solid rgba(0, 0, 0, 0.12);
                         }
                          td.mytotal {
                                         padding: 24px 8px;
                                         font-size: 35px;
                                         font-weight: 300;
                                         color: rgba(0, 0, 0, 1);
                                     }
                                     .alignright{
                                         padding-left: 100px;
                                     }
                                     #alignright{
                                         padding-left: 100px;
                                     }
           </style></head><body></body></html>`);
        printWindow.document.write(printContent);
        printWindow.document.close();
        printWindow.print();
        // window.print();
    }

}

/* export class FilesDataSource extends DataSource<any>
{
    records2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: recordsService) {
        super();
    }

     //Connect function called by the table to retrieve one stream containing the data to render.
    connect(): Observable<any[]> {
        var result = Observable.fromPromise(this.db.getrecords());
        result.subscribe(x => console.log(x), e => console.error(e));
        return result;
    }

    disconnect() {
    }
}
 */