import { Component, Inject, OnInit, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { RecordsService } from '../records.service';
import { StaffsService } from '../../staffs/staffs.service';
import { SettingsService } from '../../settings/settings.service';
import { CurrencysService } from '../../currencys/currencys.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { ConfigurerecordsService } from '../../configure_records/configure_records.service';
import { Subject } from 'rxjs/Subject';
import { Record } from '../record.model';
import { Configurerecord } from '../../configure_records/configure_record.model';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-record-receipts-record-receipt-form-dialog',
    templateUrl: './record-receipt.component.html',
    styleUrls: ['./record-receipt.component.scss'],
    encapsulation: ViewEncapsulation.None,
})

export class FuseRecordreceiptsRecordreceiptFormDialogComponent implements OnInit {

    event: CalendarEvent;
    dialogTitle: string;
    recordreceiptForm: FormGroup;
    action: string;
    public settings;
    public result;
    public final;
    public hospitalName;
    public result2;
    public final2;
    public hospitalAddress;
    public result3;
    public final3;
    public hospitalPhone;
    public result4;
    public final4;
    public hospitalEmail;
    public Pid;
    public recordDate: any;
    public recordMonth: any;
    public recordNumber: any;
    public recordDueDate: any;
    public staffName: any;
    public basicSalary: any;
    public userType: any;
    public allowancess;
    public deductionss;
    public staffId;
    public staffAddress;
    public staffNumber;
    public configurerecords: Array<Configurerecord> = [];
    public staffEmail;
    public subTotal;
    public vat;
    patients;
    patientName;
    patientAddress;
    patientPhone;
    patientEmail;
    public discount;
    public total;
    checkboxstate;
    public currencys;
    public result5;
    public final5;
    public displayCurrencys;
    urlRecord;
    image;
    general = [];
    refresh: Subject<any> = new Subject();
    medicalhistory = [];
    socialhistory = [];
    medicalconditions = [];
    currentmedications = [];
    familyhistory = [];
    notes = [];
    recordss: Record[];
    record: any = {
        id: Math.round((new Date()).getTime()).toString(),
        rev: '',
        customer: ''
    };


    constructor(
        public dialogRef: MdDialogRef<FuseRecordreceiptsRecordreceiptFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        public db: PouchService,
        public _DomSanitizer: DomSanitizer,
        public router: Router,
        public activateRoute: ActivatedRoute
    ) {
        this.Pid = data.pid
        this.dialogTitle = 'Record Receipt';
        /* 
                if (this.action === 'edit') {
                    this.dialogTitle = 'Edit record-receipt';
                    this.record-receipt = data.record-receipt;
                }
                else {
                    this.dialogTitle = 'New record-receipt';
                    this.record-receipt = {
                        id: '',
                        rev: '',
                        name: '',
                        description: '',
                        doctors: []
                    } */


        //this.record-receiptForm = this.createrecord-receiptForm();
    }


    ngOnInit() {
        this.db.getPStaffs().then(res => {
            this.click(event);
            this.checkboxstate = event.isTrusted
            
        })
        this.db.getPStaff2(this.Pid).then(res => {
            this.patients = res;
            
            this.patientName = res.name;
            this.patientAddress = res.address;
            this.patientPhone = res.phone;
            this.patientEmail = res.email;
            if (res._attachments) {
                this.image = true;
                this.urlRecord = URL.createObjectURL(res._attachments);
                
            }
            else {
                this.image = false
            }
            
            if (this.patients.record == "") {
                this.record = {
                    id: Math.round((new Date()).getTime()).toString(),
                    rev: '',
                    customer: ''
                };
            }
            else {
                this.db.getRecord(this.patients.record).then(record => {
                    this.record = record;
                    
                })
            }
        })
        this._loadrecords();

    }

    private _loadrecords(): void {
        this.db.getConfigurerecords()
            .then(configurerecords => {
                this.configurerecords = configurerecords;
                //this.records2 = new BehaviorSubject<any>(records);
                this.general = this.configurerecords.filter(data => data.category == 'General');
                this.refresh.next(this.general);
                this.medicalhistory = this.configurerecords.filter(data => data.category == 'Medical History');
                this.socialhistory = this.configurerecords.filter(data => data.category == 'Social History');
                this.medicalconditions = this.configurerecords.filter(data => data.category == 'Medical Conditions');
                this.currentmedications = this.configurerecords.filter(data => data.category == 'Current Medications');
                this.familyhistory = this.configurerecords.filter(data => data.category == 'Family History');
                this.notes = this.configurerecords.filter(data => data.category == 'Notes');

                //console.log(this.records2);
            });

    }

    click(event) {
        
        this.checkboxstate = event;
    }

    print() {
      /*   this.db.content = document.getElementById("printable").innerHTML;
        console.log(this.db.content) */
        /* window.document.write(this.db.content);
        window.document.close();
        window.print();
 */
     this.router.navigate(['pages/print']);
    }

    createrecordreceiptForm() {
        /*  return this.formBuilder.group({
             id: [this.record-receipt.id],
             rev: [this.record-receipt.rev],
             name: [this.record-receipt.name],
             description: [this.record-receipt.description],
 
         });*/
    }
}
