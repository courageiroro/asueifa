import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { RecordsService } from './records.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector: 'fuse-records',
    templateUrl: './records.component.html',
    styleUrls: ['./records.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [Animations.slideInTop]
})
export class FuseRecordsComponent implements OnInit {
    hasSelectedRecords: boolean;
    searchInput: FormControl;

    constructor(private recordsService: PouchService) {
        this.searchInput = new FormControl('');
       
    }

    ngOnInit() {

        this.recordsService.onSelectedRecordsChanged
            .subscribe(selectedRecords => {
                this.hasSelectedRecords = selectedRecords.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.recordsService.onSearchTextChanged.next(searchText);
            });
    }

}
