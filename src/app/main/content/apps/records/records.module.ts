import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseRecordsComponent } from './records.component';
import { RecordsService } from './records.service';
//import { UserService } from '../../../../user.service';
import { ConfigurerecordsService } from './../configure_records/configure_records.service';
import { FuseRecordsRecordListComponent } from './record-list/record-list.component';
import { FuseRecordsSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseRecordsRecordFormDialogComponent } from './record-form/record-form.component';
import { StaffsService } from './../staffs/staffs.service';
import {FuseRecordreceiptsRecordreceiptFormDialogComponent} from './record-receipt/record-receipt.component';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/record/:id',
        component: FuseRecordsComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            records: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseRecordsComponent,
        FuseRecordsRecordListComponent,
        FuseRecordsSelectedBarComponent,
        FuseRecordsRecordFormDialogComponent,
        FuseRecordreceiptsRecordreceiptFormDialogComponent
    ],
    providers      : [
        RecordsService,
        StaffsService,
        PouchService,
        AuthGuard,
        ConfigurerecordsService
       //UserService
    ],
    entryComponents: [FuseRecordsRecordFormDialogComponent,FuseRecordreceiptsRecordreceiptFormDialogComponent]
})
export class FuseRecordsModule
{
}
