/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import { Staff } from './../staffs/staff.model';
import { StaffsService } from './../staffs/staffs.service';
declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Record } from './record.model';
import { Schema } from '../schema';

@Injectable()
export class RecordsService {
    public record = "";
    public sCredentials;
    public category;
    public file;
    public retrieve;
    public content: any;
    onRecordsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedRecordsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    records: Record[];
    staffs: Staff[];
    user: any;
    selectedRecords: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    public db2: StaffsService
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http, ) {
        this.initDB(this.sCredentials);
    }

    /**
     * The records App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getRecords()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getRecords();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getRecords();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('asueifai', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/asueifai';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/asueifai';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }

     /**
    * Update a staff
    * @param {staff} staff
    *
    * @return Promise<staff>
    */
    updateStaff(staff: Staff) {

            return this.db.rel.save('staff', staff)
                .then((data: any) => {
                    if (data && data.staffs && data.staffs
                    [0]) {
                        console.log(data.staffs[0])
                        return data.staffs[0]
                    }
                    return null;
                }).catch((err: any) => {
                    console.error(err);
                });

        
      
    }

    /***********
     * record
     **********/
    /**
     * Save a record
     * @param {record} record
     *
     * @return Promise<record>
     */
    saveRecord(record: Record, staff: Staff): Promise<Record> {
        //record.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('record', record)
            .then((data: any) => {
                console.log(staff);
                this.updateStaff(staff);
                /*   if (this.file) {
                      this.file;
                      
                      this.retrieve = (<HTMLInputElement>this.file).files[0];
  
                      if (this.retrieve = (<HTMLInputElement>this.file).files[0]) {
                          staff._attachments = this.retrieve
                      }
  
                      else {
                          staff._attachments;
                          //this.getStaff = staff._attachments;
                      }
  
                      return this.db.rel.save('staff', staff)
                          .then((data: any) => {
                              if (data && data.staffs && data.staffs
                              [0]) {
                                   this.db.rel.putAttachment('staff', { id: staff.id, rev: staff.rev }, 'file', staff._attachments, 'image/png').then(res => {
                                      var reader: FileReader = new FileReader();
  
                                      reader.onloadend = function (e) {
                                          var base64data = reader.result;
                                          staff.image = base64data
                                          //console.log(staff.image);
                                      }
                                      reader.readAsDataURL(this.retrieve);
                                      staff._attachments = res;
                                      staff.image = res
  
                                  }) 
                                  
                                  return data.staffs[0]
                              }
  
                              return null;
                          }).catch((err: any) => {
                              console.error(err);
                          });
  
                  }
                  else {
                      
  
                      return this.db.rel.save('staff', staff)
                          .then((data: any) => {
                              if (data && data.staffs && data.staffs
                              [0]) {
                                   this.db.rel.putAttachment('staff', { id: staff.id, rev: staff.rev }, 'file', staff._attachments, 'image/png').then(res => {
                                      staff._attachments = res
                                  }) 
                                  
                                  return data.staffs[0]
                              }
  
                              return null;
                          }).catch((err: any) => {
                              console.error(err);
                          });
                  } */
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a record
    * @param {record} record
    *
    * @return Promise<record>
    */
    updateRecord(record: Record) {
        return this.db.rel.save('record', record)
            .then((data: any) => {
                if (data && data.records && data.records
                [0]) {
                    console.log('Update')

                    return data.records[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a record
     * @param {record} record
     *
     * @return Promise<boolean>
     */
    removeRecord(record: Record): Promise<boolean> {
        return this.db.rel.del('record', record)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the records
     *
     * @return Promise<Array<record>>
     */
    getRecords(): Promise<Array<Record>> {
        return this.db.rel.find('record')
            .then((data: any) => {
                this.records = data.records;
                if (this.searchText && this.searchText !== '') {
                    this.records = FuseUtils.filterArrayByString(this.records, this.searchText);
                }
                //this.onrecordsChanged.next(this.records);
                return Promise.resolve(this.records);
                //return data.records ? data.records : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getDoctorRecords(): Promise<Array<Record>> {
        return this.db.rel.find('record')
            .then((data: any) => {
                return data.records ? data.records : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a record
     * @param {record} record
     *
     * @return Promise<record>
     */
    getRecord(id): Promise<Record> {
        return this.db.rel.find('record', id)
            .then((data: any) => {
                console.log("Get")

                return data && data.records ? data.records[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected record by id
     * @param id
     */
    toggleSelectedRecord(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedRecords.length > 0) {
            const index = this.selectedRecords.indexOf(id);

            if (index !== -1) {
                this.selectedRecords.splice(index, 1);

                // Trigger the next event
                this.onSelectedRecordsChanged.next(this.selectedRecords);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedRecords.push(id);

        // Trigger the next event
        this.onSelectedRecordsChanged.next(this.selectedRecords);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedRecords.length > 0) {
            this.deselectRecords();
        }
        else {
            this.selectRecords();
        }
    }

    selectRecords(filterParameter?, filterValue?) {
        this.selectedRecords = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedRecords = [];
            this.records.map(record => {
                this.selectedRecords.push(record.id);
            });
        }
        else {
            /* this.selectedrecords.push(...
                 this.records.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedRecordsChanged.next(this.selectedRecords);
    }





    deselectRecords() {
        this.selectedRecords = [];

        // Trigger the next event
        this.onSelectedRecordsChanged.next(this.selectedRecords);
    }

    deleteRecord(record) {
        this.db.rel.del('record', record)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const recordIndex = this.records.indexOf(record);
                this.records.splice(recordIndex, 1);
                this.onRecordsChanged.next(this.records);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedRecords() {
        for (const recordId of this.selectedRecords) {
            const record = this.records.find(_record => {
                return _record.id === recordId;
            });

            this.db.rel.del('record', record)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const recordIndex = this.records.indexOf(record);
                    this.records.splice(recordIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onRecordsChanged.next(this.records);
        this.deselectRecords();
    }
}
