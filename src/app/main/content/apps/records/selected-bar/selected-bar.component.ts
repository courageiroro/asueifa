import { Component, OnInit } from '@angular/core';
import { RecordsService } from '../records.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseRecordsSelectedBarComponent implements OnInit
{
    selectedRecords: string[];
    hasSelectedRecords: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private recordsService: PouchService,
        public dialog: MdDialog
    )
    {
        this.recordsService.onSelectedRecordsChanged
            .subscribe(selectedRecords => {
                this.selectedRecords = selectedRecords;
                setTimeout(() => {
                    this.hasSelectedRecords = selectedRecords.length > 0;
                    this.isIndeterminate = (selectedRecords.length !== this.recordsService.records.length && selectedRecords.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.recordsService.selectRecords();
    }

    deselectAll()
    {
        this.recordsService.deselectRecords();
    }

    deleteSelectedRecords()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected records?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.recordsService.deleteSelectedRecords();
            }
            this.confirmDialogRef = null;
        });
    }

}
