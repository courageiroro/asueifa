import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Report } from '../../reports/report.model';
import { StaffsService } from '../../staffs/staffs.service';
import { ReportsService } from '../../reports/reports.service';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-report_historys-report_history-form-dialog',
    templateUrl: './report_history-form.component.html',
    styleUrls: ['./report_history-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseReport_historysReport_historyFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    reportForm: FormGroup;
    action: string;
    report: Report;
    typess: any;
    public staffss;
    public patientss;
    localStorageItem: any;
    public nameId: string;
    public patientId: string;

    constructor(
        public dialogRef: MdDialogRef<FuseReport_historysReport_historyFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        public db: PouchService,

    ) {
        document.addEventListener('click', function () {
            db.file = document.querySelector('#fileupload')
            var imagestring = document.querySelector('#imagestring');
            
            var retrieve;

        })

        this.localStorageItem = JSON.parse(localStorage.getItem('user'));
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Report';
            this.report = data.report;
        }
        else {
            this.dialogTitle = 'New Report';
            this.report = {
                id: '',
                rev: '',
                type: '',
                description: '',
                timestamp: new Date,
                staff_id: '',
                patient_id: '',
                files: '',
                _attachments: '',
                document_name: '',
                staff_name: '',
                patient_name: ''
            }
        }

        if (this.localStorageItem.usertype === "Doctor") {
            this.staffss = [{ name: this.localStorageItem.name, id: this.localStorageItem.id }]
            
        }
        else {
            db.getDStaffs().then(res => {
                this.staffss = res;
            })
        }


        db.getPStaffs().then(res => {
            this.patientss = res;
        })

        this.reportForm = this.createReportForm();
    }

    ngOnInit() {
        this.typess = ['Operation', 'Birth', 'Death']
    }

    getPId(patient) {
        
        this.patientId = patient.id;
    }

    getDId(staff) {
        
        this.nameId = staff.id;
    }


    createReportForm() {
        return this.formBuilder.group({
            id: [this.report.id],
            rev: [this.report.rev],
            type: [this.report.type],
            description: [this.report.description],
            timestamp: new Date(this.report.timestamp).toDateString(),
            doctor_id: [this.report.staff_id],
            patient_id: [this.report.patient_id],
            files: [this.report.files],
            _attachments: [this.report._attachments],
            document_name: [this.report.document_name],
            staff_name: [this.report.staff_name],
            patient_name: [this.report.patient_name]

        });
    }
}
