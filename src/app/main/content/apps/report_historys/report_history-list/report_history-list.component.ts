import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ReportsService } from '../../reports/reports.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { FuseReport_historysReport_historyFormDialogComponent } from '../report_history-form/report_history-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Report } from '../../reports/report.model';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {PouchService} from '../../../../../provider/pouch-service';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';

@Component({
    selector: 'fuse-report_historys-report_history-list',
    templateUrl: './report_history-list.component.html',
    styleUrls: ['./report_history-list.component.scss']
})
export class FuseReport_historysReport_historyListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public reports: Array<Report> = [];
    public localStorageItem: any;
    public localStorageType: any;
    reports2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['type', 'description', 'date', 'doctor', 'buttons'];
    selectedReports: any[];
    url: any;
    checkboxes: {};
    fileName: any;

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;

    constructor(public dialog: MdDialog, public db: PouchService, public router: Router) {
        this.localStorageItem = JSON.parse(localStorage.getItem('user'));
        this.localStorageType = this.localStorageItem.usertype;
    }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        this._loadReports();
    }

    private _loadReports(): void {
        this.db.onReportsChanged.subscribe(data => {
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        this.db.getReports()
            .then((reports: Array<Report>) => {
                this.reports = reports;
                this.reports2 = new BehaviorSubject<any>(reports);
                //console.log(this.reports2);

                this.checkboxes = {};
                reports.map(report => {
                    this.checkboxes[report.id] = false;
                });
                this.db.onSelectedReportsChanged.subscribe(selectedReports => {
                    for (const id in this.checkboxes) {
                        this.checkboxes[id] = selectedReports.includes(id);
                    }

                    this.selectedReports = selectedReports;
                });
                this.db.onSearchTextChanged.subscribe(data => {
                    this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                })
            });

    }

    newReport() {
        this.dialogRef = this.dialog.open(FuseReport_historysReport_historyFormDialogComponent, {
            panelClass: 'report-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
                var res = response.getRawValue();
                res.timestamp = new Date(res.timestamp).toISOString().substring(0, 10);
                this.db.saveReport(res);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

            });

    }

    editReport(report) {
        this.dialogRef = this.dialog.open(FuseReport_historysReport_historyFormDialogComponent, {
            panelClass: 'report-form-dialog',
            data: {
                report: report,
                action: 'edit'
            }
        });

        
        this.url = URL.createObjectURL(report._attachments);
        

        var label = document.createElement('a');
        label.innerHTML = report.document_name;
        //this.fileName = report.document_name;
        
        label.setAttribute('href', this.url)

        //label.value = report.document_name
        //label = <HTMLInputElement>report.document_name;
        document.getElementById('label1').appendChild(label)
        
        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                var form = formData.getRawValue();
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':
                        form.timestamp = new Date(form.timestamp).toISOString().substring(0, 10);
                        this.db.updateReport(form);
                        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteReport(report);

                        break;
                }
            });
    }

    navFile(report) {
        this.url = URL.createObjectURL(report._attachments);
        
        this.router.navigate([this.url]);
    }

    /**
     * Delete report
     */
    deleteReport(report) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deleteReport(report);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(reportId) {
        this.db.toggleSelectedReport(reportId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    reports2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            of(this.db.reports).delay(5000),
            this.sort.mdSortChange,
            this._paginator.page,
        ];

        var result = Observable.fromPromise(this.db.getReports());
       /*  result.subscribe(x => console.log(x), e => console.error(e));
        return result; */
        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.reports]));
        });
    }

    disconnect() {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
          return data;
        }
    
        return data.sort((a, b) => {
          const isAsc = this.sort.direction === 'asc';
          switch (this.sort.active) {
            case 'doctor': return compare(a.doctor_id, b.doctor_id, isAsc);
            case 'amount': return compare(+a.amount, +b.amount, isAsc);
            case 'id': return compare(+a.id, +b.id, isAsc);
            default: return 0;
          }
        });
      }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

