import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ReportsService } from './../reports/reports.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-report_historys',
    templateUrl  : './report_historys.component.html',
    styleUrls    : ['./report_historys.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FuseReport_historysComponent implements OnInit
{
    hasSelectedReports: boolean;
    searchInput: FormControl;

    constructor(private reportsService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.reportsService.onSelectedReportsChanged
            .subscribe(selectedReports => {
                this.hasSelectedReports = selectedReports.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.reportsService.onSearchTextChanged.next(searchText);
            });
    }

}
