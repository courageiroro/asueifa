import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseReport_historysComponent } from './report_historys.component';
import { ReportsService } from './../reports/reports.service';
import { FuseReport_historysReport_historyListComponent } from './report_history-list/report_history-list.component';
import { FuseReport_historysSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseReport_historysReport_historyFormDialogComponent } from './report_history-form/report_history-form.component';
import { StaffsService } from './../staffs/staffs.service';
import { PatientsService } from './../patients/patients.service';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path: 'apps/reporthistory',
        component: FuseReport_historysComponent,
        canActivate: [AuthGuard],
        children: [],
        resolve: {
            reports: PouchService
        }
    }
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        FuseReport_historysComponent,
        FuseReport_historysReport_historyListComponent ,
        FuseReport_historysSelectedBarComponent,
        FuseReport_historysReport_historyFormDialogComponent
    ],
    providers: [
        ReportsService,
        PouchService,
        StaffsService,
        AuthGuard,
        PatientsService
    ],
    entryComponents: [FuseReport_historysReport_historyFormDialogComponent]
})
export class FuseReport_historysModule {
}
