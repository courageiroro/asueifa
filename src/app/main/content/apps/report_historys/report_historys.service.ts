/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Report } from './../reports/report.model';
import { Schema } from '../schema';

@Injectable()
export class Report_historysService {
    public reportname = "";
    public name = "";
    public retrieve;
    public file;
    public sCredentials;

    onReportsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedReportsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    reports: Report[];
    user: any;
    selectedReports: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The reports App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getReports()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getReports();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getReports();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('asueifai', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/asueifai';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/asueifai';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }
    /***********
     * report
     **********/
    /**
     * Save a report
     * @param {report} report
     *
     * @return Promise<report>
     */
    saveReport(report: Report): Promise<Report> {
        report.id = Math.floor(Date.now()).toString();

        this.file;
        this.retrieve = (<HTMLInputElement>this.file).files[0];
        report._attachments = this.retrieve;
        report.document_name = report._attachments.name;

        return this.db.rel.save('report', report)
            .then((data: any) => {

                if (data && data.reports && data.reports
                [0]) {
                    this.db.rel.putAttachment('report', { id: report.id, rev: report.rev }, 'file', report._attachments, 'application/msword').then(res => {
                        report._attachments = res
                        
                    })
                    return data.reports[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a report
    * @param {report} report
    *
    * @return Promise<report>
    */
    updateReport(report: Report) {


        this.file;
        this.retrieve = (<HTMLInputElement>this.file).files[0];
        report._attachments = this.retrieve;
        report.document_name = report._attachments.name;

        return this.db.rel.save('report', report)
            .then((data: any) => {
                if (data && data.reports && data.reports
                [0]) {
                    this.db.rel.putAttachment('report', { id: report.id, rev: report.rev }, 'file', report._attachments, 'application/msword').then(res => {
                        report._attachments = res
                    })
                    return data.reports[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a report
     * @param {report} report
     *
     * @return Promise<boolean>
     */
    removeReport(report: Report): Promise<boolean> {
        return this.db.rel.del('report', report)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the reports
     *
     * @return Promise<Array<report>>
     */
    getReports(): Promise<Array<Report>> {

        var localStorageItem = JSON.parse(localStorage.getItem('user'));
        var userType = localStorageItem.usertype;

        return this.db.rel.find('report')
            .then((data: any) => {
                this.reports = data.reports;
                if (this.searchText && this.searchText !== '') {
                    this.reports = FuseUtils.filterArrayByString(this.reports, this.searchText);
                }
                //this.onreportsChanged.next(this.reports);
                if (userType === "Doctor") {
                    var userId = localStorageItem.id
                    this.reports = this.reports.filter(data => data.staff_name == userId)
                }
                return Promise.resolve(this.reports);
                //return data.reports ? data.reports : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a report
     * @param {report} report
     *
     * @return Promise<report>
     */
    getReport(report: Report): Promise<Report> {
        return this.db.rel.find('report', report.id)
            .then((data: any) => {
                return data && data.reports ? data.reports[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected report by id
     * @param id
     */
    toggleSelectedReport(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedReports.length > 0) {
            const index = this.selectedReports.indexOf(id);

            if (index !== -1) {
                this.selectedReports.splice(index, 1);

                // Trigger the next event
                this.onSelectedReportsChanged.next(this.selectedReports);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedReports.push(id);

        // Trigger the next event
        this.onSelectedReportsChanged.next(this.selectedReports);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedReports.length > 0) {
            this.deselectReports();
        }
        else {
            this.selectReports();
        }
    }

    selectReports(filterParameter?, filterValue?) {
        this.selectedReports = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedReports = [];
            this.reports.map(report => {
                this.selectedReports.push(report.id);
            });
        }
        else {
            /* this.selectedreports.push(...
                 this.reports.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedReportsChanged.next(this.selectedReports);
    }





    deselectReports() {
        this.selectedReports = [];

        // Trigger the next event
        this.onSelectedReportsChanged.next(this.selectedReports);
    }

    deleteReport(report) {
        this.db.rel.del('report', report)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const reportIndex = this.reports.indexOf(report);
                this.reports.splice(reportIndex, 1);
                this.onReportsChanged.next(this.reports);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedReports() {
        for (const reportId of this.selectedReports) {
            const report = this.reports.find(_report => {
                return _report.id === reportId;
            });

            this.db.rel.del('report', report)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const reportIndex = this.reports.indexOf(report);
                    this.reports.splice(reportIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onReportsChanged.next(this.reports);
        this.deselectReports();
    }
}
