import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Staff } from './contact.model';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

@Injectable()
export class StaffsService2 implements Resolve<any>
{
    onStaffsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedStaffsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    staffs: Staff[];
    user: any;
    selectedStaffs: string[] = [];

    searchText: string;
    filterBy: string;

    constructor(private http: Http)
    {
    }

    /**
     * The Staffs App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getStaffs(),
                this.getUserData()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getStaffs();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getStaffs();
                    });

                    resolve();

                },
                reject
            );
        });
    }

    getStaffs(): Promise<any>
    {
        return new Promise((resolve, reject) => {
                this.http.get('api/staffs-staffs')
                    .subscribe(response => {

                        this.staffs = response.json().data;

                        if ( this.filterBy === 'starred' )
                        {
                            this.staffs = this.staffs.filter(_staff => {
                                return this.user.starred.includes(_staff.id);
                            });
                        }

                        if ( this.filterBy === 'frequent' )
                        {
                            this.staffs = this.staffs.filter(_staff => {
                                return this.user.frequentStaffs.includes(_staff.id);
                            });
                        }

                        if ( this.searchText && this.searchText !== '' )
                        {
                            this.staffs = FuseUtils.filterArrayByString(this.staffs, this.searchText);
                        }

                        this.staffs = this.staffs.map(staff => {
                            return new Staff(staff);
                        });

                        this.onStaffsChanged.next(this.staffs);
                        resolve(this.staffs);
                    }, reject);
            }
        );
    }

    getUserData(): Promise<any>
    {
        return new Promise((resolve, reject) => {
                this.http.get('api/staffs-user/5725a6802d10e277a0f35724')
                    .subscribe(response => {
                        this.user = response.json().data;
                        this.onUserDataChanged.next(this.user);
                        resolve(this.user);
                    }, reject);
            }
        );
    }

    /**
     * Toggle selected staff by id
     * @param id
     */
    toggleSelectedStaff(id)
    {
        // First, check if we already have that todo as selected...
        if ( this.selectedStaffs.length > 0 )
        {
            const index = this.selectedStaffs.indexOf(id);

            if ( index !== -1 )
            {
                this.selectedStaffs.splice(index, 1);

                // Trigger the next event
                this.onSelectedStaffsChanged.next(this.selectedStaffs);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedStaffs.push(id);

        // Trigger the next event
        this.onSelectedStaffsChanged.next(this.selectedStaffs);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll()
    {
        if ( this.selectedStaffs.length > 0 )
        {
            this.deselectStaffs();
        }
        else
        {
            this.selectStaffs();
        }
    }

    selectStaffs(filterParameter?, filterValue?)
    {
        this.selectedStaffs = [];

        // If there is no filter, select all todos
        if ( filterParameter === undefined || filterValue === undefined )
        {
            this.selectedStaffs = [];
            this.staffs.map(staff => {
                this.selectedStaffs.push(staff.id);
            });
        }
        else
        {
            /* this.selectedStaffs.push(...
                 this.staffs.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedStaffsChanged.next(this.selectedStaffs);
    }

    updateStaff(staff)
    {
        return new Promise((resolve, reject) => {

            this.http.post('api/staffs-staffs/' + staff.id, {...staff})
                .subscribe(response => {
                    this.getStaffs();
                    resolve(response);
                });
        });
    }

    updateUserData(userData)
    {
        return new Promise((resolve, reject) => {
            this.http.post('api/staffs-user/' + this.user.id, {...userData})
                .subscribe(response => {
                    this.getUserData();
                    this.getStaffs();
                    resolve(response);
                });
        });
    }

    deselectStaffs()
    {
        this.selectedStaffs = [];

        // Trigger the next event
        this.onSelectedStaffsChanged.next(this.selectedStaffs);
    }

    deleteStaff(staff)
    {
        const staffIndex = this.staffs.indexOf(staff);
        this.staffs.splice(staffIndex, 1);
        this.onStaffsChanged.next(this.staffs);
    }

    deleteSelectedStaffs()
    {
        for ( const staffId of this.selectedStaffs )
        {
            const staff = this.staffs.find(_staff => {
                return _staff.id === staffId;
            });
            const staffIndex = this.staffs.indexOf(staff);
            this.staffs.splice(staffIndex, 1);
        }
        this.onStaffsChanged.next(this.staffs);
        this.deselectStaffs();
    }

}
