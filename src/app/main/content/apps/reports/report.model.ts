export interface Report {
    id: string,
    rev: string,
    type: string,
    description: string,
    timestamp: Date,
    staff_id: string,
    patient_id: string,
     staff_name: string,
    patient_name: string,
    files: string,
    _attachments: any,
    document_name:string
}

