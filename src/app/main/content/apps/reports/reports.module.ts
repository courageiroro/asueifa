import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseReportsComponent } from './reports.component';
import { ReportsService } from './reports.service';
import { FuseReportsReportListComponent } from './report-list/report-list.component';
import { FuseReportsSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseReportsReportFormDialogComponent } from './report-form/report-form.component';
import { StaffsService } from './../staffs/staffs.service';
import { PatientsService } from './../patients/patients.service';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/reports',
        component: FuseReportsComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            reports: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseReportsComponent,
        FuseReportsReportListComponent,
        FuseReportsSelectedBarComponent,
        FuseReportsReportFormDialogComponent
    ],
    providers      : [
        ReportsService,
        PouchService,
        StaffsService,
        AuthGuard,
        PatientsService
    ],
    entryComponents: [FuseReportsReportFormDialogComponent]
})
export class FuseReportsModule
{
}
