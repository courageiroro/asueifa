import { Component, OnInit } from '@angular/core';
import { ReportsService } from '../reports.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseReportsSelectedBarComponent implements OnInit
{
    selectedReports: string[];
    hasSelectedReports: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private reportsService: PouchService,
        public dialog: MdDialog
    )
    {
        this.reportsService.onSelectedReportsChanged
            .subscribe(selectedReports => {
                this.selectedReports = selectedReports;
                setTimeout(() => {
                    this.hasSelectedReports = selectedReports.length > 0;
                    this.isIndeterminate = (selectedReports.length !== this.reportsService.reports.length && selectedReports.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.reportsService.selectReports();
    }

    deselectAll()
    {
        this.reportsService.deselectReports();
    }

    deleteSelectedReports()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected reportes?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.reportsService.deleteSelectedReports();
            }
            this.confirmDialogRef = null;
        });
    }

}
