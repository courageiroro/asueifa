/**
 * Schema defining the db relations
 */
export const Schema = [
    {
        singular: 'accountant',
        plural: 'accountants',
        relations: {
            payrolls: {
                hasMany: {
                    type: 'payroll'
                },
            },
        }
    },

    {
        singular: 'admin',
        plural: 'admins',
        relations: {
            account: {
                belongsTo: 'account'
            },
            notes: {
                hasMany: {
                    type: 'note'
                },
            },
            payrolls: {
                hasMany: {
                    type: 'payroll'
                },
            }
        }


    },

    {
        singular: 'appointment',
        plural: 'appointments',
        relations: {
            staff: {
                belongsTo: 'staff'
            },

            patient: {
                belongsTo: 'patient'
            }
        }
    },
    {
        singular: 'bed',
        plural: 'beds',
        relations: {
            bed_allotments: {
                hasMany: {
                    type: 'bed_allotment'
                }
            }
        }
    },

    {
        singular: 'bed_allotment',
        plural: 'bed_allotments',
        relations: {
            bed: {
                belongsTo: 'bed'
            },
            patient: {
                belongsTo: 'patient'
            },

        }
    },

    {
        singular: 'blood_bank',
        plural: 'blood_banks',
        relations: {
            blood_donors: {
                hasMany: {
                    type: 'blood_donor'
                },
            },
            patients: {
                hasMany: {
                    type: 'patient'
                },
            }
        }
    },

    {
        singular: 'blood_price',
        plural: 'blood_prices',
        relations: {
            blood_sales: {
                hasMany: {
                    type: 'blood_sale'
                },
            },
        }
    },

    {
        singular: 'blood_sale',
        plural: 'blood_sales',
        relations: {
            blood_sale: {
                belongsTo: 'blood_sale'
            },
            staff: {
                belongsTo: 'staffs'
            }
        }
    },

    {
        singular: 'blood_donor',
        plural: 'blood_donors',
        relations: {
            blood_bank: {
                belongsTo: 'blood_bank'
            }
        }
    },

    {
        singular: 'smssetting',
        plural: 'smssettings',
    },

     {
        singular: 'insurancesheme',
        plural: 'insuranceshemes',
    },


    {
        singular: 'currency',
        plural: 'currencys',
    },

    {
        singular: 'account',
        plural: 'accounts',
        relations: {
            admins: {
                hasMany: {
                    type: 'admin'
                },
            }
        }
    },

    {
        singular: 'ci_sessions',
        plural: 'Ci_sessionss',
    },

    {
        singular: 'managepayment',
        plural: 'managepayments',
    },

    {
        singular: 'offline',
        plural: 'offlines',
    },


    {
        singular: 'consultancy',
        plural: 'consultancys',
    },

    {
        singular: 'record',
        plural: 'records',
        relations: {
            staffs: {
                hasMany: {
                    type: 'staff'
                },
            }
        }
    },
    {
        singular: 'anterecord',
        plural: 'anterecords',
        relations: {
            staffs: {
                hasMany: {
                    type: 'antenatal'
                },
            }
        }
    },
    {
        singular: 'labrecord',
        plural: 'labrecords',
        relations: {
            staffs: {
                hasMany: {
                    type: 'staff'
                },
            }
        }
    },

    {
        singular: 'department',
        plural: 'departments',
        relations: {
            staffs: {
                hasMany: {
                    type: 'staff'
                },
            }
        }
    },

    {
        singular: 'diagnosis_report',
        plural: 'diagnosis_reports',
        relations: {
            prescription: {
                belongsTo: 'prescription'
            },
            laboratorist: {
                belongsTo: 'laboratorist'
            },
            staff: {
                belongsTo: 'staff'
            },
            patient: {
                belongsTo: 'patient'
            }
        }
    },

    {
        singular: 'staff',
        plural: 'staffs',
        relations: {
            department: {
                belongsTo: 'department'
            },
            record: {
                belongsTo: 'record'
            },
            labrecord: {
                belongsTo: 'labrecord'
            },
            prescriptions: {
                hasMany: {
                    type: 'prescription'
                },
            },
            reports: {
                hasMany: {
                    type: 'report'
                },
            },
            blood_sales: {
                hasMany: {
                    type: 'blood_sale'
                },
            },
            appointments: {
                hasMany: {
                    type: 'appointment'
                },
            },
            diagnosis_reports: {
                hasMany: {
                    type: 'diagnosis_report'
                },
            },
        }
    },

    {
        singular: 'antenatal',
        plural: 'antenatals',
        relations: {
            anterecord: {
                belongsTo: 'anterecord'
            },
            followups: {
                hasMany: {
                    type: 'followup'
                }
            }
        }
    },

    {
        singular: 'email_template',
        plural: 'email_templates',
        relations: {
            setting: {
                belongsTo: 'setting'
            },
            patient: {
                belongsTo: 'patient'
            }
        }

    },

    {
        singular: 'sms',
        plural: 'smss',
        relations: {
            patient: {
                belongsTo: 'patient'
            }
        }

    },

    {
        singular: 'followup',
        plural: 'followups',
        relations: {
            antenatal: {
                belongsTo: 'antenatal'
            }
        }

    },

    {
        singular: 'form_element',
        plural: 'form_elements',
    },

    {
        singular: 'invoice',
        plural: 'invoices',
        relations: {
            patient: {
                belongsTo: 'patient'
            },
        }
    },
    {
        singular: 'laboratorist',
        plural: 'laboratorists',
        relations: {
            diagnosis_reports: {
                hasMany: {
                    type: 'diagnosis_report'
                },
            },

        }
    },
    {
        singular: 'language',
        plural: 'languages'
    },

    {
        singular: 'medicine',
        plural: 'medicines',
        relations: {
            medicine_category: {
                belongsTo: 'medicine_category'
            },
            medicine_sales: {
                hasMany: {
                    type: 'medicine_sale'
                },
            }
        }
    },

    {
        singular: 'medicine_category',
        plural: 'medicine_categorys',
        relations: {
            medicines: {
                hasMany: {
                    type: 'medicine'
                },
            }
        }
    },

    {
        singular: 'medicine_sale',
        plural: 'medicine_sales',
        relations: {
            patient: {
                belongsTo: 'patient'
            },
            medicine: {
                belongsTo: 'medicine'
            },

        }
    },

    {
        singular: 'message',
        plural: 'messages',

    },

    {
        singular: 'message_thread',
        plural: 'message_threads',

    },

    {
        singular: 'lab_dashboard',
        plural: 'lab_dashboards',

    },

    {
        singular: 'note',
        plural: 'notes',
        relations: {
            admin: {
                belongsTo: 'admin'
            }
        }

    },

    {
        singular: 'notice',
        plural: 'notices',
        relations: {
            noticeboards: {
                hasMany: {
                    type: 'noticeboards'
                },
            }
        }
    },

    {
        singular: 'noticeboard',
        plural: 'noticeboards',
        relations: {
            notice: {
                belongsTo: 'notice'
            }
        }
    },

    {
        singular: 'nurse',
        plural: 'nurses',
    },

    {
        singular: 'patient',
        plural: 'patients',
        relations: {
            blood_bank: {
                belongsTo: 'blood_bank'
            },
            email_templates: {
                hasMany: {
                    type: 'email_template'
                }
            },
            smss: {
                hasMany: {
                    type: 'sms'
                }
            },
            appointments: {
                hasMany: {
                    type: 'appointment'
                },
            },
            bed_allotments: {
                hasMany: {
                    type: 'bed_allotment'
                },
            },
            invoices: {
                hasMany: {
                    type: 'invoice'
                },
            },
            medicine_sales: {
                hasMany: {
                    type: 'medicine_sale'
                },
            },
            prescriptions: {
                hasMany: {
                    type: 'prescription'
                },
            },
            diagnosis_reports: {
                hasMany: {
                    type: 'diagnosis_report'
                },
            },
            reports: {
                hasMany: {
                    type: 'report'
                },
            },

        }

    },

    {
        singular: 'payment',
        plural: 'payments',
    },
    {
        singular: 'insurancetype',
        plural: 'insurancetypes',
    },
    {
        singular: 'extendlicense',
        plural: 'extendlicenses',
    },

    {
        singular: 'configurerecord',
        plural: 'configurerecords',
    },
    {
        singular: 'alerts',
        plural: 'alertss',
    },
    {
        singular: 'configureanterecord',
        plural: 'configureanterecords',
    },
    {
        singular: 'labtest',
        plural: 'labtests',
    },
    {
        singular: 'expense',
        plural: 'expenses',
    },
    {
        singular: 'receipt',
        plural: 'receipts',
    },

    {
        singular: 'payroll',
        plural: 'payrolls',
        relations: {
            admin: {
                belongsTo: 'admin'
            },
            user_type: {
                belongsTo: 'admin'
            },
        },
    },

    {
        singular: 'pharmacist',
        plural: 'pharmacists',
    },

    {
        singular: 'prescription',
        plural: 'prescriptions',
        relations: {
            staff: {
                belongsTo: 'staff'
            },
            patient: {
                belongsTo: 'patient'
            },
        },
    },

    {
        singular: 'receptionist',
        plural: 'receptionists',
    },

    {
        singular: 'report',
        plural: 'reports',
        relations: {
            staff: {
                belongsTo: 'staff'
            },
            patient: {
                belongsTo: 'patient'
            },
        },
    },

    {
        singular: 'setting',
        plural: 'settings',
        relations: {
            email_templates: {
                hasMany: {
                    type: 'email_template'
                }
            }
        }

    },

];