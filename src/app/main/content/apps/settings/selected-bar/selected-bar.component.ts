import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../settings/settings.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseSettingsSelectedBarComponent implements OnInit
{
    selectedSettings: string[];
    hasSelectedSettings: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private settingsService: PouchService,
        public dialog: MdDialog
    )
    {
        this.settingsService.onSelectedSettingsChanged
            .subscribe(selectedSettings => {
                this.selectedSettings = selectedSettings;
                setTimeout(() => {
                    this.hasSelectedSettings = selectedSettings.length > 0;
                    this.isIndeterminate = (selectedSettings.length !== this.settingsService.settings.length && selectedSettings.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.settingsService.selectSettings();
    }

    deselectAll()
    {
        this.settingsService.deselectSettings();
    }

    deleteSelectedSettings()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected Settings?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.settingsService.deleteSelectedSettings();
            }
            this.confirmDialogRef = null;
        });
    }

}
