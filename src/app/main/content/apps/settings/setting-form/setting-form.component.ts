import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Setting } from '../setting.model';

@Component({
    selector: 'fuse-settings-setting-form-dialog',
    templateUrl: './setting-form.component.html',
    styleUrls: ['./setting-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseSettingsSettingFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    settingForm: FormGroup;
    action: string;
    setting: Setting;

    constructor(
        public dialogRef: MdDialogRef<FuseSettingsSettingFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Setting';
            this.setting = data.setting;
        }
        else {
            this.dialogTitle = 'New Setting';
            this.setting = {
                id: '',
                rev: '',
                company: '',
                name: '',
                databasename: '',
                country: '',
                receiptmessage: '',
                startdate: new Date(),
                expiredate: null,
                plans: [],
                online: false,
                password: '',
                address: '',
                phone: '',
                email: '',
                city: '',
                email_templates: []
            }
        }

        this.settingForm = this.createSettingForm();
    }

    ngOnInit() {
    }

    createSettingForm() {
        return this.formBuilder.group({
            id: [this.setting.id],
            rev: [this.setting.rev],
            name: [this.setting.name],
            databasename: [this.setting.databasename],
            country: [this.setting.country],
            receiptmessage: [this.setting.receiptmessage],
            startdate:[this.setting.startdate],
            expiredate: [this.setting.expiredate],
            online:[this.setting.online],
            password:[this.setting.online],
            address: [this.setting.address],
            phone: [this.setting.phone],
            email: [this.setting.email],
            city: [this.setting.city]
        });
    }
}
