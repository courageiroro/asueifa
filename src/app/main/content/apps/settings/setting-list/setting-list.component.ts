import { Component, OnInit, AfterViewInit, TemplateRef, ViewChild } from '@angular/core';
import { SettingsService } from '../settings.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { FuseSettingsSettingFormDialogComponent } from '../setting-form/setting-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Setting } from '../setting.model';
import { Router } from '@angular/router';
import {PouchService} from '../../../../../provider/pouch-service';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';

@Component({
    selector: 'fuse-settings-setting-list',
    templateUrl: './setting-list.component.html',
    styleUrls: ['./setting-list.component.scss']
})
export class FuseSettingsSettingListComponent implements OnInit, AfterViewInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public settings: Array<Setting> = [];
    settings2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'name', 'address', 'phone', 'email', 'buttons'];
    selectedSettings: any[];
    checkboxes: {};
    setting;
    expiredate;
    expiredate2;
    plan;
    databaseId;
    databaseName;
    password;
    regDate;
    edit = false;

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;

    constructor(public dialog: MdDialog, public db: PouchService, public router: Router) {
       
    }

    ngAfterViewInit() {
        this.db.getSettings().then(settings => {

            if (settings[0] != undefined) {
                this.edit = true;
                this.setting = settings[0];
                /*this.expiredate = new Date(this.setting.expiredate).toDateString();
                this.expiredate2 = this.setting.expiredate;
                this.databaseName = this.setting.databasename;
                this.databaseId = this.setting.id;
                this.password = this.setting.password
                this.regDate = this.setting.startdate;
                this.plan = this.setting.plans[0];
                if (new Date(this.setting.expiredate).getTime() < new Date().getTime()) {

                } */
            }
        });
    }
   

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        this._loadSettings();

    }

    private _loadSettings(): void {
        this.db.onSettingsChanged.subscribe(data => {
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        this.db.getSettings()
            .then((settings: Array<Setting>) => {
                this.settings = settings;
                this.settings2 = new BehaviorSubject<any>(settings);
                //console.log(this.settings2);

                this.checkboxes = {};
                settings.map(setting => {
                    this.checkboxes[setting.id] = false;
                });
                this.db.onSelectedSettingsChanged.subscribe(selectedSettings => {
                    for (const id in this.checkboxes) {
                        this.checkboxes[id] = selectedSettings.includes(id);
                    }

                    this.selectedSettings = selectedSettings;
                });
                this.db.onSearchTextChanged.subscribe(data => {
                    this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                })
            });

    }

    newSetting() {
        this.dialogRef = this.dialog.open(FuseSettingsSettingFormDialogComponent, {
            panelClass: 'setting-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }

                this.db.saveSetting(response.getRawValue());

                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

            });

    }

    editSetting(setting) {
        console.log(setting)
        this.dialogRef = this.dialog.open(FuseSettingsSettingFormDialogComponent, {
            panelClass: 'setting-form-dialog',
            data: {
                setting: setting,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':

                        this.db.updateSetting(formData.getRawValue());
                        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteSetting(setting);

                        break;
                }
            });
    }

    /**
     * Delete setting
     */
    deleteSetting(setting) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deleteSetting(setting);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(settingId) {
        this.db.toggleSelectedSetting(settingId);
    }
    checkout() {
        localStorage.removeItem('credentials');
        localStorage.removeItem('user'); //logout user also
        localStorage.removeItem('demo');
        localStorage.removeItem('authguard');
        this.router.navigate(['apps/logindb'])
    }

    configurerecord() {
        this.router.navigate(['apps/configurerecord']);
    }

    labtest(){
        this.router.navigate(['apps/labtest']);
    }

    extendLicense() {
        this.router.navigate(['apps/extendlicense'])
    }

    offlineActivate() {
        this.router.navigate(['apps/offline']);
    }

    managePayment() {
        this.router.navigate(['apps/managepayment'])
    }

}

export class FilesDataSource extends DataSource<any>
{
    settings2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            of(this.db.settings).delay(5000),
            this.sort.mdSortChange,
            this._paginator.page,
        ];

        var result = Observable.fromPromise(this.db.getSettings());
       /*  result.subscribe(x => console.log(x), e => console.error(e));
        return result; */

        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.settings]));
        });
    }

    disconnect() {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
          return data;
        }
    
        return data.sort((a, b) => {
          const isAsc = this.sort.direction === 'asc';
          switch (this.sort.active) {
            case 'name': return compare(a.name, b.name, isAsc);
            case 'amount': return compare(+a.amount, +b.amount, isAsc);
            case 'id': return compare(+a.id, +b.id, isAsc);
            default: return 0;
          }
        });
      }

}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
