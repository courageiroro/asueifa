export interface Setting {
    id: string,
    rev: string,
    name: string,
    address: string,
    phone: string,
    email: string,
    city: string,
    company: string,
    databasename: string,
    password: string,
    country: string,
    receiptmessage: string,
    startdate: any,
    expiredate: any,
    online: boolean,
    plans: Array<string>,
    email_templates: Array<string>
}

