import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { SettingsService } from './settings.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-settings',
    templateUrl  : './settings.component.html',
    styleUrls    : ['./settings.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FuseSettingsComponent implements OnInit
{
    hasSelectedSettings: boolean;
    searchInput: FormControl;

    constructor(private settingsService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.settingsService.onSelectedSettingsChanged
            .subscribe(selectedSettings => {
                this.hasSelectedSettings = selectedSettings.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.settingsService.onSearchTextChanged.next(searchText);
            });
    }

}
