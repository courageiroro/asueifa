import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseSettingsComponent } from './settings.component';
import { SettingsService } from './../settings/settings.service';
import { FuseSettingsSettingListComponent } from './setting-list/setting-list.component';
import { FuseSettingsSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseSettingsSettingFormDialogComponent } from './setting-form/setting-form.component';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/settings',
        component: FuseSettingsComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            settings: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseSettingsComponent,
        FuseSettingsSettingListComponent,
        FuseSettingsSelectedBarComponent,
        FuseSettingsSettingFormDialogComponent
    ],
    providers      : [
        AuthGuard,
        SettingsService,
        PouchService
    ],
    entryComponents: [FuseSettingsSettingFormDialogComponent]
})
export class FuseSettingsModule
{
}
