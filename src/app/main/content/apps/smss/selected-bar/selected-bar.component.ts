import { Component, OnInit } from '@angular/core';
import { SmssService } from '../smss.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseSmssSelectedBarComponent implements OnInit
{
    selectedSmss: string[];
    hasSelectedSmss: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private smssService: PouchService,
        public dialog: MdDialog
    )
    {
        this.smssService.onSelectedSmssChanged
            .subscribe(selectedSmss => {
                this.selectedSmss = selectedSmss;
                setTimeout(() => {
                    this.hasSelectedSmss = selectedSmss.length > 0;
                    this.isIndeterminate = (selectedSmss.length !== this.smssService.smss.length && selectedSmss.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.smssService.selectSmss();
    }

    deselectAll()
    {
        this.smssService.deselectSmss();
    }

    deleteSelectedSmss()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected Sms?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.smssService.deleteSelectedSmss();
            }
            this.confirmDialogRef = null;
        });
    }

}
