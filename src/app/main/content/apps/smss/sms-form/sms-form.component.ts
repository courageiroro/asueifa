import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Sms } from '../sms.model';
import { PatientsService } from '../../patients/patients.service';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-smss-sms-form-dialog',
    templateUrl: './sms-form.component.html',
    styleUrls: ['./sms-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseSmssSmsFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    smsForm: FormGroup;
    action: string;
    sms: Sms;
    public patientss;
    public patientNumber;

    constructor(
        public dialogRef: MdDialogRef<FuseSmssSmsFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        public db: PouchService
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Email Template';
            this.sms = data.sms;
        }
        else {
            this.dialogTitle = 'New Email Template';
            this.sms = {
                id:'',
                rev: '',
                patient:'',
                subject:'',
                body:'',
                phone:''
            }
        }

        db.getPatients().then(res=>{
            this.patientss = res;
        })

        this.smsForm = this.createsmsForm();
    }

    ngOnInit() {
    }

     getPId(patient) {
        this.db.patientnumber = patient.phone;
        this.patientNumber = patient.phone

    }

    createsmsForm() {
        return this.formBuilder.group({
            id: [this.sms.id],
            rev: [this.sms.rev],
            subject: [this.sms.subject],
            body: [this.sms.body],
            patient: [this.sms.patient],
            phone: [this.sms.phone],

        });
    }
}
