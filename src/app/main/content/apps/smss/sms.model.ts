export interface Sms {
    id: string,
    rev: string,
    patient: string,
    phone: string,
    subject: string,
    body: string,
}

