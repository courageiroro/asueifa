import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { SmssService } from './smss.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-smss',
    templateUrl  : './smss.component.html',
    styleUrls    : ['./smss.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FuseSmssComponent implements OnInit
{
    hasSelectedSmss: boolean;
    searchInput: FormControl;

    constructor(private smssService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.smssService.onSelectedSmssChanged
            .subscribe(selectedSmss => {
                this.hasSelectedSmss = selectedSmss.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.smssService.onSearchTextChanged.next(searchText);
            });
    }

}
