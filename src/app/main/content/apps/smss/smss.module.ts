import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseSmssComponent } from './smss.component';
import { SmssService } from './smss.service';
import { FuseSmssSmsListComponent } from './sms-list/sms-list.component';
import { FuseSmssSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseSmssSmsFormDialogComponent } from './sms-form/sms-form.component';
import { PatientsService } from './../patients/patients.service';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : '**',
        component: FuseSmssComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            smss: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseSmssComponent,
        FuseSmssSmsListComponent,
        FuseSmssSelectedBarComponent,
        FuseSmssSmsFormDialogComponent
    ],
    providers      : [
        SmssService,
        AuthGuard,
        PouchService,
        PatientsService
    ],
    entryComponents: [FuseSmssSmsFormDialogComponent]
})
export class FuseSmssModule
{
}
