/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
PouchDB.plugin(require('pouchdb-find'));
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('relational-pouch'));

import { Sms } from './sms.model';
import { Schema } from '../schema';

@Injectable()
export class SmssService {
    public smsname = "";
    public sCredentials;
    onSmssChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedSmssChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    smss: Sms[];
    user: any;
    selectedSmss: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The smss App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getSmss()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getSmss();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getSmss();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('asueifai', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/asueifai';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/asueifai';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }


    /***********
     * sms
     **********/
    /**
     * Save a sms
     * @param {sms} sms
     *
     * @return Promise<sms>
     */
    saveSms(sms: Sms): Promise<Sms> {
        sms.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('sms', sms)
            .then((data: any) => {
                //console.log(data);
                //console.log(sms);
                if (data && data.smss && data.smss
                [0]) {
                    return data.smss[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a sms
    * @param {sms} sms
    *
    * @return Promise<sms>
    */
    updateSms(sms: Sms) {

        return this.db.rel.save('sms', sms)
            .then((data: any) => {
                if (data && data.smss && data.smss
                [0]) {
                    return data.smss[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a sms
     * @param {sms} sms
     *
     * @return Promise<boolean>
     */
    removeSms(sms: Sms): Promise<boolean> {
        return this.db.rel.del('sms', sms)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the smss
     *
     * @return Promise<Array<sms>>
     */
    getSmss(): Promise<Array<Sms>> {
        return this.db.rel.find('sms')
            .then((data: any) => {
                this.smss = data.smss;
                if (this.searchText && this.searchText !== '') {
                    this.smss = FuseUtils.filterArrayByString(this.smss, this.searchText);
                }
                //this.onsmssChanged.next(this.smss);
                return Promise.resolve(this.smss);
                //return data.smss ? data.smss : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a sms
     * @param {sms} sms
     *
     * @return Promise<sms>
     */
    getSms(sms: Sms): Promise<Sms> {
        return this.db.rel.find('sms', sms.id)
            .then((data: any) => {
                return data && data.smss ? data.smss[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected sms by id
     * @param id
     */
    toggleSelectedSms(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedSmss.length > 0) {
            const index = this.selectedSmss.indexOf(id);

            if (index !== -1) {
                this.selectedSmss.splice(index, 1);

                // Trigger the next event
                this.onSelectedSmssChanged.next(this.selectedSmss);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedSmss.push(id);

        // Trigger the next event
        this.onSelectedSmssChanged.next(this.selectedSmss);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedSmss.length > 0) {
            this.deselectSmss();
        }
        else {
            this.selectSmss();
        }
    }

    selectSmss(filterParameter?, filterValue?) {
        this.selectedSmss = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedSmss = [];
            this.smss.map(sms => {
                this.selectedSmss.push(sms.id);
            });
        }
        else {
            /* this.selectedsmss.push(...
                 this.smss.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedSmssChanged.next(this.selectedSmss);
    }





    deselectSmss() {
        this.selectedSmss = [];

        // Trigger the next event
        this.onSelectedSmssChanged.next(this.selectedSmss);
    }

    deleteSms(sms) {
        this.db.rel.del('sms', sms)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const smsIndex = this.smss.indexOf(sms);
                this.smss.splice(smsIndex, 1);
                this.onSmssChanged.next(this.smss);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedSmss() {
        for (const smsId of this.selectedSmss) {
            const sms = this.smss.find(_sms => {
                return _sms.id === smsId;
            });

            this.db.rel.del('sms', sms)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const smsIndex = this.smss.indexOf(sms);
                    this.smss.splice(smsIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onSmssChanged.next(this.smss);
        this.deselectSmss();
    }
}
