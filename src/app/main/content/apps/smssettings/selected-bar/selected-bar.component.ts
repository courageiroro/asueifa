import { Component, OnInit } from '@angular/core';
import { SmssettingsService } from '../smssettings.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseSmssettingsSelectedBarComponent implements OnInit
{
    selectedSmssettings: string[];
    hasSelectedSmssettings: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private smssettingsService: PouchService,
        public dialog: MdDialog
    )
    {
        this.smssettingsService.onSelectedSmssettingsChanged
            .subscribe(selectedSmssettings => {
                this.selectedSmssettings = selectedSmssettings;
                setTimeout(() => {
                    this.hasSelectedSmssettings = selectedSmssettings.length > 0;
                    this.isIndeterminate = (selectedSmssettings.length !== this.smssettingsService.smssettings.length && selectedSmssettings.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.smssettingsService.selectSmssettings();
    }

    deselectAll()
    {
        this.smssettingsService.deselectSmssettings();
    }

    deleteSelectedsmssettings()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected smssettings?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.smssettingsService.deleteSelectedSmssettings();
            }
            this.confirmDialogRef = null;
        });
    }

}
