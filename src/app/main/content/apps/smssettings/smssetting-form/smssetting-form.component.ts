import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { Smssetting } from '../smssetting.model';

@Component({
    selector: 'fuse-smssettings-smssetting-form-dialog',
    templateUrl: './smssetting-form.component.html',
    styleUrls: ['./smssetting-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseSmssettingsSmssettingFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    smssettingForm: FormGroup;
    action: string;
    smssetting: Smssetting;

    constructor(
        public dialogRef: MdDialogRef<FuseSmssettingsSmssettingFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Sms setting';
            this.smssetting = data.smssetting;
        }
        else {
            this.dialogTitle = 'New smssetting';
            this.smssetting = {
                id: '',
                rev: '',
                user: '',
                password: '',
                api: '',
            }
        }

        this.smssettingForm = this.createsmssettingForm();
    }

    ngOnInit() {
    }

    createsmssettingForm() {
        return this.formBuilder.group({
            id: [this.smssetting.id],
            rev: [this.smssetting.rev],
            user: [this.smssetting.user],
            password: [this.smssetting.password],
            api: [this.smssetting.api],

        });
    }
}
