import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { SmssettingsService } from '../smssettings.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject} from 'rxjs/Rx';
import { FuseSmssettingsSmssettingFormDialogComponent } from '../smssetting-form/smssetting-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Smssetting } from '../smssetting.model';
import {PouchService} from '../../../../../provider/pouch-service';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';

@Component({
    selector   : 'fuse-smssettings-smssetting-list',
    templateUrl: './smssetting-list.component.html',
    styleUrls  : ['./smssetting-list.component.scss']
})
export class FuseSmssettingsSmssettingListComponent implements OnInit
{
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public smssettings: Array<Smssetting> = [];
    smssettings2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'user','password','api','buttons'];
    selectedSmssettings: any[];
    checkboxes: {};

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;

    constructor(public dialog: MdDialog, public db:PouchService)
    { }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        this._loadSmssettings();
    }

    private _loadSmssettings(): void {
         this.db.onSmssettingsChanged.subscribe(data =>{
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        this.db.getSmssettings()
        .then((smssettings: Array<Smssetting>) => {
            this.smssettings = smssettings;
            this.smssettings2= new BehaviorSubject<any>(smssettings);

            this.checkboxes = {};
            smssettings.map(smssetting => {
                this.checkboxes[smssetting.id] = false;
            });
             this.db.onSelectedSmssettingsChanged.subscribe(selectedSmssettings => {
                for ( const id in this.checkboxes )
                {
                    this.checkboxes[id] = selectedSmssettings.includes(id);
                }

                this.selectedSmssettings = selectedSmssettings;
            });
        this.db.onSearchTextChanged.subscribe(data =>{
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        });
        
    }

    newSmssetting()
    {
        this.dialogRef = this.dialog.open(FuseSmssettingsSmssettingFormDialogComponent, {
            panelClass: 'smssetting-form-dialog',
            data      : {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if ( !response )
                {
                    return;
                }

                this.db.saveSmssetting(response.getRawValue());
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

            });

    }

    editSmssetting(smssetting)
    {
        this.dialogRef = this.dialog.open(FuseSmssettingsSmssettingFormDialogComponent, {
            panelClass: 'smssetting-form-dialog',
            data      : {
                smssetting: smssetting,
                action : 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch ( actionType )
                {
                    /**
                     * Save
                     */
                    case 'save':

                        this.db.updateSmssetting(formData.getRawValue());
                        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteSmssetting(smssetting);

                        break;
                }
            });
    }

    /**
     * Delete smssetting
     */
    deleteSmssetting(smssetting)
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.db.deleteSmssetting(smssetting);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(smssettingId)
    {
        this.db.toggleSelectedSmssetting(smssettingId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    smssettings2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort)
    {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]>
    {
        const displayDataChanges = [
            of(this.db.smssettings).delay(5000),
            this.sort.mdSortChange,
            this._paginator.page,
        ];

        var result = Observable.fromPromise(this.db.getSmssettings());
        /* result.subscribe(x => console.log(x), e => console.error(e));
        return result;   */  

        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.smssettings]));
        });
    }

    disconnect()
    {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
          return data;
        }
    
        return data.sort((a, b) => {
          const isAsc = this.sort.direction === 'asc';
          switch (this.sort.active) {
            case 'user': return compare(a.user, b.user, isAsc);
            case 'amount': return compare(+a.amount, +b.amount, isAsc);
            case 'id': return compare(+a.id, +b.id, isAsc);
            default: return 0;
          }
        });
      }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
