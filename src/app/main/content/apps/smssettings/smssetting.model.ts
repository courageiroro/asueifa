export interface Smssetting {
    id: string,
    rev: string,
    user: string,
    password: string,
    api: string,
}
