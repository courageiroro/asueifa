import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { SmssettingsService } from './smssettings.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-smssettings',
    templateUrl  : './smssettings.component.html',
    styleUrls    : ['./smssettings.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FuseSmssettingsComponent implements OnInit
{
    hasSelectedSmssettings: boolean;
    searchInput: FormControl;

    constructor(private smssettingsService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.smssettingsService.onSelectedSmssettingsChanged
            .subscribe(selectedSmssettings => {
                this.hasSelectedSmssettings = selectedSmssettings.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.smssettingsService.onSearchTextChanged.next(searchText);
            });
    }

}
