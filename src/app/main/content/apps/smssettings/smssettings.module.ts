import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseSmssettingsComponent } from './smssettings.component';
import { SmssettingsService } from './smssettings.service';
import { FuseSmssettingsSmssettingListComponent } from './smssetting-list/smssetting-list.component';
import { FuseSmssettingsSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseSmssettingsSmssettingFormDialogComponent } from './smssetting-form/smssetting-form.component';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';

const routes: Routes = [
    {
        path     : 'apps/prints',
        component: FuseSmssettingsComponent,
        canActivate: [AuthGuard],
        children : [],
        resolve  : {
            smssettings: PouchService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
        FuseSmssettingsComponent,
        FuseSmssettingsSmssettingListComponent,
        FuseSmssettingsSelectedBarComponent,
        FuseSmssettingsSmssettingFormDialogComponent
    ],
    providers      : [
        AuthGuard,
        PouchService,
        SmssettingsService
    ],
    entryComponents: [FuseSmssettingsSmssettingFormDialogComponent]
})
export class FuseSmssettingsModule
{
}
