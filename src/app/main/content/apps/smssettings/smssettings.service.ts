/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));

import { Smssetting } from './smssetting.model';
import { Schema } from '../schema';

@Injectable()
export class SmssettingsService {
    public name = "";
    public sCredentials;
    onSmssettingsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedSmssettingsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    smssettings: Smssetting[];
    user: any;
    selectedSmssettings: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
    }

    /**
     * The smssettings App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getSmssettings()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getSmssettings();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getSmssettings();
                    });

                    resolve();

                },
                reject
                );
        });
    }

    /**
     * Initialize PouchDB database
     */
    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('asueifai', { adaptater: 'websql'});
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
            this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }

    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
            this.remote = 'http://sarutech.com:5984/asueifai';
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        //}

    }

    loadRemoteDB(credentials) {

        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

            this.remote = 'http://sarutech.com:5984/asueifai';
            return this.db.sync(this.remote, options).on('change', function (change) {

                //return true;
            }).on('complete', function (complete) {
                return true;
            }).on('error', function (err) {
                console.log('offline');
            });
        //}

    }

    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }

    /***********
     * smssetting
     **********/
    /**
     * Save a smssetting
     * @param {smssetting} smssetting
     *
     * @return Promise<smssetting>
     */
    saveSmssetting(smssetting: Smssetting): Promise<Smssetting> {
        smssetting.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('smssetting', smssetting)
            .then((data: any) => {

                if (data && data.smssettings && data.smssettings
                [0]) {
                    return data.smssettings[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a smssetting
    * @param {smssetting} smssetting
    *
    * @return Promise<smssetting>
    */
    updateSmssetting(smssetting: Smssetting) {

        return this.db.rel.save('smssetting', smssetting)
            .then((data: any) => {
                if (data && data.smssettings && data.smssettings
                [0]) {
                    return data.smssettings[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a smssetting
     * @param {smssetting} smssetting
     *
     * @return Promise<boolean>
     */
    removeSmssetting(smssetting: Smssetting): Promise<boolean> {
        return this.db.rel.del('smssetting', smssetting)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the smssettings
     *
     * @return Promise<Array<smssetting>>
     */
    getSmssettings(): Promise<Array<Smssetting>> {
        return this.db.rel.find('smssetting')
            .then((data: any) => {
                this.smssettings = data.smssettings;
                if (this.searchText && this.searchText !== '') {
                    this.smssettings = FuseUtils.filterArrayByString(this.smssettings, this.searchText);
                }
                //this.onsmssettingsChanged.next(this.smssettings);
                return Promise.resolve(this.smssettings);
                //return data.smssettings ? data.smssettings : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a smssetting
     * @param {smssetting} smssetting
     *
     * @return Promise<smssetting>
     */
    getSmssetting(smssetting: Smssetting): Promise<Smssetting> {
        return this.db.rel.find('smssetting', smssetting.id)
            .then((data: any) => {
                return data && data.smssettings ? data.smssettings[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected smssetting by id
     * @param id
     */
    toggleSelectedSmssetting(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedSmssettings.length > 0) {
            const index = this.selectedSmssettings.indexOf(id);

            if (index !== -1) {
                this.selectedSmssettings.splice(index, 1);

                // Trigger the next event
                this.onSelectedSmssettingsChanged.next(this.selectedSmssettings);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedSmssettings.push(id);

        // Trigger the next event
        this.onSelectedSmssettingsChanged.next(this.selectedSmssettings);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedSmssettings.length > 0) {
            this.deselectSmssettings();
        }
        else {
            this.selectSmssettings();
        }
    }

    selectSmssettings(filterParameter?, filterValue?) {
        this.selectedSmssettings = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedSmssettings = [];
            this.smssettings.map(smssetting => {
                this.selectedSmssettings.push(smssetting.id);
            });
        }
        else {
            /* this.selectedsmssettings.push(...
                 this.smssettings.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedSmssettingsChanged.next(this.selectedSmssettings);
    }





    deselectSmssettings() {
        this.selectedSmssettings = [];

        // Trigger the next event
        this.onSelectedSmssettingsChanged.next(this.selectedSmssettings);
    }

    deleteSmssetting(smssetting) {
        this.db.rel.del('smssetting', smssetting)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const smssettingIndex = this.smssettings.indexOf(smssetting);
                this.smssettings.splice(smssettingIndex, 1);
                this.onSmssettingsChanged.next(this.smssettings);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedSmssettings() {
        for (const smssettingId of this.selectedSmssettings) {
            const smssetting = this.smssettings.find(_smssetting => {
                return _smssetting.id === smssettingId;
            });

            this.db.rel.del('smssetting', smssetting)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const smssettingIndex = this.smssettings.indexOf(smssetting);
                    this.smssettings.splice(smssettingIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onSmssettingsChanged.next(this.smssettings);
        this.deselectSmssettings();
    }
}
