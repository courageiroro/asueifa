import { Component, OnInit } from '@angular/core';
import { StaffsService } from '../staffs.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector   : 'fuse-selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseStaffsSelectedBarComponent implements OnInit
{
    selectedStaffs: string[];
    hasSelectedStaffs: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private staffsService: PouchService,
        public dialog: MdDialog
    )
    {
        this.staffsService.onSelectedStaffsChanged
            .subscribe(selectedStaffs => {
                this.selectedStaffs = selectedStaffs;
                setTimeout(() => {
                    this.hasSelectedStaffs = selectedStaffs.length > 0;
                    this.isIndeterminate = (selectedStaffs.length !== this.staffsService.staffs.length && selectedStaffs.length > 0);
                }, 0);
            });

    }

    ngOnInit()
    {
    }

    selectAll()
    {
        this.staffsService.selectStaffs();
    }

    deselectAll()
    {
        this.staffsService.deselectStaffs();
    }

    deleteSelectedStaffs()
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected Staff?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.staffsService.deleteSelectedStaffs();
            }
            this.confirmDialogRef = null;
        });
    }

}
