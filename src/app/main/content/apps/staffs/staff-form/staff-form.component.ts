import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg'
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import { BehaviorSubject } from 'rxjs/Rx';
import 'rxjs/Rx';
import { Staff } from '../staff.model';
declare var jquery: any;
declare var $: any;
import { DepartmentsService } from '../../departments/departments.service';
import { StaffsService } from '../staffs.service';
import { Blood_banksService } from '../../blood_banks/blood_banks.service';
import { DomSanitizer } from '@angular/platform-browser';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-staffs-staff-form-dialog',
    templateUrl: './staff-form.component.html',
    styleUrls: ['./staff-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseStaffsStaffFormDialogComponent implements OnInit {

    event: CalendarEvent;
    dialogTitle: string;
    staffForm: FormGroup;
    action: string;
    staff: Staff;
    public departmentss;
    public imagestring;
    public retrieve;
    public file;
    public blood_bankss;
    public userTypes;
    defaultImage;
    image;


    constructor(
        public dialogRef: MdDialogRef<FuseStaffsStaffFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        public db: PouchService,
        public _DomSanitizer: DomSanitizer
    ) {

        document.addEventListener('click', function () {
            db.file = document.querySelector('#fileupload')
            //var imagestring = document.querySelector('#imagestring');

        })

        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Staff';
            this.staff = data.staff;
            this.image = data.staff.image;
        }
        else {
            this.dialogTitle = 'New Staff';
            this.staff = {
                id: '',
                rev: '',
                name: '',
                image: '',
                email: '',
                password: '',
                address: '',
                phone: '',
                department_id: '',
                secretquestion: '',
                answer: '',
                patientno: '',
                profile: '',
                _attachments: '',
                usertype: '',
                sex: '',
                birth_date: new Date(),
                age: '',
                blood_group: '',
                account_opening_timestamp: '',
                record: '',
                records: '',
                labrecord: '',
                labrecords: '',
                insurancename: '',
                insuranceid: '',
                insurancepercent: null,
                imageurl: '',
                appointments: [],
                bed_allotments: [],
                invoices: [],
                medicine_sales: [],
                prescriptions: [],
                reports: [],
                email_templates: [],
                smss: [],
                appointment: [],
                diagnosis_reports: [],
                blood_sales: []
            }
        }

        db.getDepartments().then(res => {
            this.departmentss = res;
        })

        this.db.getBlood_banks().then(res => {
            this.blood_bankss = res;

        })

        this.staffForm = this.createstaffForm();
    }

    ngOnInit() {
        this.userTypes = ['Admin', 'Doctor', 'Nurse', 'Pharmacist', 'Laboratorist', 'Accountant', 'Receptionist']
    }


    createstaffForm() {
        return this.formBuilder.group({
            id: [this.staff.id],
            rev: [this.staff.rev],
            name: [this.staff.name],
            image: [this.staff.image],
            usertype: [this.staff.usertype],
            email: [this.staff.email],
            password: [this.staff.password],
            address: [this.staff.address],
            secretquestion: [this.staff.secretquestion],
            answer: [this.staff.answer],
            _attachments: [this.staff._attachments],
            phone: [this.staff.phone],
            department_id: [this.staff.department_id],
            profile: [this.staff.profile],
            sex: [this.staff.sex],
            patientno: [this.staff.patientno],
            birth_date: [this.staff.birth_date],
            age: [this.staff.age],
            imageurl: [this.staff.imageurl],
            blood_group: [this.staff.blood_group],
            account_opening_timestamp: [this.staff.account_opening_timestamp],
            record: [this.staff.record],
            records: [this.staff.records],
            labrecord: [this.staff.record],
            labrecords: [this.staff.records],
            insurancename: [this.staff.insurancename],
            insuranceid: [this.staff.insuranceid],
            insurancepercent: [this.staff.insurancepercent]
        });
    }
}
