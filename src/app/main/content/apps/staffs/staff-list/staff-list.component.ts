import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { StaffsService } from '../staffs.service';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg'
import { FuseStaffsStaffFormDialogComponent } from '../staff-form/staff-form.component';
import { MdDialog, MdDialogRef, MdPaginator, MdSort } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Staff } from '../staff.model';
import { PouchService } from '../../../../../provider/pouch-service';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';


@Component({
    selector: 'fuse-staffs-staff-list',
    templateUrl: './staff-list.component.html',
    styleUrls: ['./staff-list.component.scss']
})
export class FuseStaffsStaffListComponent implements OnInit {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;

    public staffs: Array<Staff> = [];
    staffs2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox','name', 'email', 'address', 'phone', 'department_id', 'profile', 'buttons'];
    selectedStaffs: any[];
    checkboxes: {};
    test;
    arrayLength;

    dialogRef: any;

    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MdPaginator) paginator: MdPaginator;
    @ViewChild(MdSort) sort: MdSort;

    constructor(public dialog: MdDialog, public db: PouchService) {
        
    }

    ngOnInit() {
        //this.paginator._intl = new PaginatorTranslated();
        this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        this._loadStaffs();

    }

    private _loadStaffs(): void {
        this.db.onStaffsChanged.subscribe(data => {
            this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
        })
        this.db.getStaffs()
            .then((staffs: Array<Staff>) => {
                this.arrayLength = staffs.length;
                this.staffs = staffs;
                this.staffs2 = new BehaviorSubject<any>(staffs);

                this.checkboxes = {};
                staffs.map(staff => {
                    this.checkboxes[staff.id] = false;
                    console.log(this.checkboxes);
                    console.log(staffs);
                });
                this.db.onSelectedStaffsChanged.subscribe(selectedStaffs => {
                    for (const id in this.checkboxes) {
                        this.checkboxes[id] = selectedStaffs.includes(id);
                    }

                    this.selectedStaffs = selectedStaffs;
                });
                this.db.onSearchTextChanged.subscribe(data => {
                    this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
                })
            });

    }


    get() {
        this.db.getStaffs().then(res => {

            var url = URL.createObjectURL(res[3]._attachments);

            var img = document.createElement('img');
            img.src = url;
            document.getElementById('img2').appendChild(img)
        })
    }

   

    newStaff() {
        this.dialogRef = this.dialog.open(FuseStaffsStaffFormDialogComponent, {
            panelClass: 'staff-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }


                var res = response.getRawValue();
                var reader = new FileReader();
                var file = this.db.file;
                var retrieve = (<HTMLInputElement>file).files[0];
                var file2;
                var myThis = this;

                if (retrieve == undefined) {
                    res.profile

                    myThis.db.saveStaff(res);

                    myThis.dataSource = new FilesDataSource(myThis.db, this.paginator, this.sort);
                }
                else {
                    reader.readAsDataURL(retrieve);
                    reader.onloadend = function () {
                        reader.result;

                        res.image = reader.result;

                        res.profile

                        myThis.db.saveStaff(res);

                        myThis.dataSource = new FilesDataSource(myThis.db, myThis.paginator, myThis.sort);
                    }
                };
                reader.onerror = function (error) {
                    console.log('Error: ', error);
                };
                //res.image = this.db.file2;
            });

    }

    editStaff(staff) {
        this.dialogRef = this.dialog.open(FuseStaffsStaffFormDialogComponent, {
            panelClass: 'staff-form-dialog',
            data: {
                staff: staff,
                action: 'edit'
            }
        });


        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                var form = formData.getRawValue();
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':

                        var reader = new FileReader();
                        var file = this.db.file;
                        var retrieve = (<HTMLInputElement>file).files[0];
                        var file2;
                        var myThis = this;

                        if (retrieve == undefined) {
                            form.profile

                            myThis.db.updateStaff(form);

                            myThis.dataSource = new FilesDataSource(myThis.db, this.paginator, this.sort);
                        }
                        else {
                            reader.readAsDataURL(retrieve);
                            reader.onloadend = function () {
                                reader.result;

                                form.image = reader.result;

                                form.profile


                                myThis.db.updateStaff(form);
                                myThis.dataSource = new FilesDataSource(myThis.db, myThis.paginator, myThis.sort);
                            };
                            reader.onerror = function (error) {
                                console.log('Error: ', error);
                            };
                        }


                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteStaff(staff);

                        break;
                }
            });

        /*     
           var url = URL.createObjectURL(staff.image);
           
           var img = document.createElement('img');
           img.src = url;
           
           document.getElementById('img3').appendChild(img).style.width = '300px';
           document.getElementById('img3').appendChild(img).style.height = '250px'; */

    }

    /**
     * Delete staff
     */
    deleteStaff(staff) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.db.deleteStaff(staff);
                this.dataSource = new FilesDataSource(this.db, this.paginator, this.sort);
            }
            this.confirmDialogRef = null;
        });

    }

    /* name(){
         try {
        var network = new ActiveXObject('WScript.Network');
        // Show a pop up if it works
        alert(network.computerName);
    }
    catch (e) { }
    } */

    onSelectedChange(staffId) {
        this.db.toggleSelectedStaff(staffId);
    }

}


export class FilesDataSource extends DataSource<any>
{
    staffArray: any;
    //staffs2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(private db: PouchService, private _paginator: MdPaginator, private sort: MdSort) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
       const displayDataChanges = [
            of(this.db.staffs).delay(5000),
            //this.db.onStaffsChanged,
            this.sort.mdSortChange,
            this._paginator.page,
        ];

        //this._paginator.length = this.db.staffs.length;

        var result = Observable.fromPromise(this.db.getStaffs());
        /*   result.subscribe(x => console.log(x), e => console.error(e));
         console.log(this.db.staffs); */
        //return result; 
      
        return Observable.merge(...displayDataChanges).map(() => {
            return this.getPagedData(this.getSortedData([...this.db.staffs]));
            /* const data = this.db.staffs;
            
            // Grab the page's slice of data.
            const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
                return data.splice(startIndex, this._paginator.pageSize); */
        });
    }

    disconnect() {
    }

    private getPagedData(data: any[]) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    }

    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
          return data;
        }
    
        return data.sort((a, b) => {
          const isAsc = this.sort.direction === 'asc';
          switch (this.sort.active) {
            case 'name': return compare(a.name, b.name, isAsc);
            case 'amount': return compare(+a.amount, +b.amount, isAsc);
            case 'id': return compare(+a.id, +b.id, isAsc);
            default: return 0;
          }
        });
      }
    
}

 /** Simple sort comparator for example ID/Name columns (for client-side sorting). */
 function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

/* export class PaginatorTranslated {
    constructor() {
        this.simulateTranslationsLoad();
        this.mockTranslate('perPage').subscribe(t => this.itemsPerPageLabel = t);
        this.mockTranslate('nextPage').subscribe(t => this.nextPageLabel = t);
        this.mockTranslate('pervPage').subscribe(t => this.previousPageLabel = t);
    }

    public itemsPerPageLabel: string;
    public nextPageLabel: string;
    public previousPageLabel: string;

    public getRangeLabel(page: number, pageSize: number, length: number): string {
        if (length == 0 || pageSize == 0) {
            return this.mockTranslateInstant('zeroItems', {
                count: length
            });
        }
        length = Math.max(length, 0);
        const startIndex = page * pageSize;
        // If the start index exceeds the list length, do not try and fix the end index to the end.
        const endIndex = startIndex < length ?
            Math.min(startIndex + pageSize, length) :
            startIndex + pageSize;

        return this.mockTranslateInstant('nItems', {
            startItem: (startIndex + 1),
            endItem: endIndex,
            count: length
        });
    }

    private simulateTranslationsLoad() {
        let self = this;
        setTimeout(() => {
            self.loadedTranslatedValues = self.translatedValues;
        }, 250);
    }

    private translatedValues: { [id: string]: string } = {
        'perPage': 'Items per page',
        'nextPage': 'Next page',
        'prevPage': 'Previous page',
        'zeroItems': '0 of {{count}}',
        'nItems': '{{startItem}} - {{endItem}} of {{count}}'
    };

    private loadedTranslatedValues: { [id: string]: string } = {};

    private mockTranslate(key: string, values?: any): Observable<string> {
        return new Observable<string>((subscriber) => {

            setTimeout(() => {
                subscriber.next(this.replaceValues(this.translatedValues[key] || key, values));
            }, 250);

        });
    }

    private mockTranslateInstant(key: string, values?: any): string {
        return this.replaceValues(this.loadedTranslatedValues[key] || key, values);
    }

    private replaceValues(translation: string, values: any): string {
        if (!values) { return translation; }
        for (let key in values) {
            if (values.hasOwnProperty(key)) {
                translation = translation.replace('{{' + key + '}}', values[key]);
            }
        }
        return translation;
    }
}

 */