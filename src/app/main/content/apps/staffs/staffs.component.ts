import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { StaffsService } from './staffs.service';
import { Animations } from '../../../../core/animations';
import { FormControl } from '@angular/forms';
import {PouchService} from './../../../../provider/pouch-service';

@Component({
    selector     : 'fuse-staffs',
    templateUrl  : './staffs.component.html',
    styleUrls    : ['./staffs.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : [Animations.slideInTop]
})
export class FuseStaffsComponent implements OnInit
{
    hasSelectedStaffs: boolean;
    searchInput: FormControl;

    constructor(private staffsService: PouchService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {

        this.staffsService.onSelectedStaffsChanged
            .subscribe(selectedStaffs => {
                this.hasSelectedStaffs = selectedStaffs.length > 0;
            });

        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.staffsService.onSearchTextChanged.next(searchText);
            });
    }

}
