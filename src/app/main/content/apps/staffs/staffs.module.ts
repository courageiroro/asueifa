import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { FuseStaffsComponent } from './staffs.component';
import { StaffsService } from './staffs.service';
import { FuseStaffsStaffListComponent } from './staff-list/staff-list.component';
import { FuseStaffsSelectedBarComponent } from './selected-bar/selected-bar.component';
import { FuseStaffsStaffFormDialogComponent } from './staff-form/staff-form.component';
import { DepartmentsService } from './../departments/departments.service';
import {Blood_banksService} from './../blood_banks/blood_banks.service';
import { RecordsService } from './../records/records.service';
import { AuthGuard } from 'app/auth.guard';
import {PouchService} from './../../../../provider/pouch-service';
import {MdPaginator} from '@angular/material';

const routes: Routes = [
    {
        path: 'apps/staffs',
        component: FuseStaffsComponent,
        canActivate: [AuthGuard],
        children: [],
        resolve: {
            staffs: PouchService
        }
    }
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes),
        FroalaEditorModule.forRoot(),
        FroalaViewModule.forRoot()
    ],
    declarations: [
        FuseStaffsComponent,
        FuseStaffsStaffListComponent,
        FuseStaffsSelectedBarComponent,
        FuseStaffsStaffFormDialogComponent
    ],
    providers: [
        StaffsService,
        DepartmentsService,
        Blood_banksService,
        AuthGuard,
        PouchService,
        RecordsService,
        MdPaginator
    ],
    entryComponents: [FuseStaffsStaffFormDialogComponent]
})
export class FuseStaffsModule {
}
