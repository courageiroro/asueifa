/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http, Headers, RequestOptions } from '@angular/http';
import { RecordsService } from './../records/records.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
declare var require: any;
import PouchDB from 'pouchdb';
//require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));
import { Staff } from './staff.model';
import { Schema } from '../schema';

@Injectable()
export class StaffsService {
    public name = "";
    public retrieve;
    public file;
    reader;
    public file2;
    public records;
    public patientname = "";
    public patientnumber = "";
    public staffId: any;
    public gattachment;
    public hide = "";
    public sCredentials;
    public base64data: any;
    onStaffsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedStaffsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    public plateno = "";

    staffs: Staff[];
    user: any;
    selectedStaffs: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor(private http: Http, public recordservice: RecordsService) {
        this.initDB(this.sCredentials);
    }

    /**
     * The staffs App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getStaffs()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getStaffs();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getStaffs();
                    });

                    resolve();

                },
                reject
            );
        });
    }

    /**
     * Initialize PouchDB database
     * 
     */

    initDB(credentials) {
        console.log(credentials);
        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('asueifai', { adaptater: 'websql' });
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
        this.enableSyncing(credentials);
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }
    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
        };
        //if (credentials.userDBs != undefined) {
        this.remote = 'http://sarutech.com:5984/asueifai';
        this.db.sync(this.remote, options).on('change', function (change) {
            console.log('changed');
        }).on('complete', function (complete) {
            console.log('complete');
        }).on('error', function (err) {
            console.log('offline');
        });;
        //}

    }

    loadRemoteDB(credentials) {
        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

        this.remote = 'http://sarutech.com:5984/asueifai';
        return this.db.sync(this.remote, options).on('change', function (change) {

            //return true;
        }).on('complete', function (complete) {
            return true;
        }).on('error', function (err) {
            console.log('offline');
        });
        //}

    }


    validateUsername(username) {
        return this.http.get('https://sarutech.com/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }

    addLocalStorage() {
        var userType = { "usertype": null }
        return localStorage.setItem('user', JSON.stringify(userType));
    }

    /***********
     *staff
     **********/
    /**
     * Save a staff
     * @param {staff} Staff
     *
     * @return Promise<Staff>
     */

    saveStaff(staff: Staff): Promise<Staff> {

        let record = {
            id: Math.round((new Date()).getTime()).toString(),
            rev: '',
            customer: staff.id
        }

        staff.id = Math.floor(Date.now()).toString();
        console.log(staff._attachments);
        staff.prescriptions = [];
        staff.reports = [];
        var filess;
        filess

        this.file;

        this.retrieve = (<HTMLInputElement>this.file).files[0];

        var file = this.retrieve;

        var file2;

        // if (staff._attachments === "null" || staff._attachments === " ") {

        /*  staff.image = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wAARCADIAMgDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9U6KKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAorivi58W9A+C/g+48Q+IJykKHZDbx8y3Eh6Ig9ffoBXwH43/AOCiPxD1zUJjoEFhoGnkkRx+V50oHYljxn8KAP0xpCwXqQPqa/ILWf2tfi3ru4XHjbUIkP8ADbBIcfiqg/rXC6x8R/FniHd/afifWNQDdRc38sg/ItQB+zus+N/Dnh0E6rr+l6YB1N5eRw4/76YVwms/tU/CXQd32nx5pEhXtaSm5/8ARYavx6JJJJOSe9FAH6laz/wUA+Eml7hBfalqZHT7JZEA/wDfZWuMn/4KWeDlvoooPC+rTW7OFaZpI1KjPXbzn6Zr86K97/Yy+C//AAtz4uWct7B5ug6KVvbzcMrIQf3cZ+rDkegPrQB+rdrcJeW0M8edkqB13DBwRkZHavAPj5+2PofwF8Y23h290G+1W5kt1uHlgkWNEViQMZB3Hg19CV8n/wDBQL4L/wDCa/DyPxjp0G/VfD4LT7B8z2pPzH/gJ+b6Z9KAJ9G/4KMfDO/2rfWetac57m3WRR+IbP6V3mjftn/B7WtoXxjBZyH+C8t5YsfiVx+tfkjRQB+1Oi/GbwF4i2jTfGmgXjN0SLUoS/8A3zuz+ldbDcw3EavFKkqNyGRgQfxr8Kav6Xr+qaI+/TtSu7B+u61naM/+OkUAfuZRX41aN+0Z8TtA2/Y/HOtgL0E920w/8fzXeaH+3Z8XtHZDLrtvqYU8i9tEO7/vnFAH6s0V8z/sy/tnaX8bL6Pw7rVrHovikqWiRGzDd4GTsJ5DYydv5V9MUAFFFFABRRRQAUUUUAFNd1jRnYhVUZJPQCnV8p/t5fHz/hXvgc+D9Iudmva7GUnaNvmgtTwx9i33fpmgD5M/bE+PR+NHxJlg0+cv4a0dmt7IKfllbOHl/Ejj2ArwOiigAooooAKKKKAFVS7BVBLE4AHev1k/Zg+F1n+z78DkuNZ2WN/NA2qavNLx5QCbtrf7ijn3zXxR+w78F/8AhZ/xYg1a/g8zQ/D7LdzbhlZJgcxJ78jcR7e9fRH/AAUO+NH/AAjnhC18B6dPtv8AWMTX2w8pbqeFP+8w/IUAe6fs8fE6f4veALjxPKpjiutRuVtoj1jhVgEB98cn3JrM/Z9+LFr8ZvCniDS9S8u41HSr650y+gcA+bFvZUYjuCnB+hrl/wBgj/k3HTP+vu4/9CFfHPwN+M5+DP7TerXd3MY9C1LVbiy1EE/KqNM22Q/7hwfpmgDg/wBov4RT/Bb4qatoBRv7OL/aLCVv44GOVGe5H3T9K8yr9PP28PgyPiP8LT4m02ETax4eVrnMYy0tr1kHvgfN9Aa/MOgAooooAKKKKAL2ha5e+GtasdW024e1v7KZLiCaM4KOpyD+Yr9hvgD8YbH43fDbTvENsUS82+TfWyn/AFM4HzDHoeo9jX41179+xx8ej8GPiVDbajcGPwzrLLbXu4/LCxOEm/Ann2J9KAP1dopFZXUMpDKRkEHIIpaACiiigAooooA574geONL+G/g7VfEmsTCGw0+BpX55c/wovqzHAHua/G/4ofEXU/it451XxNqzk3N7KWWPOVijHCIvsBgV9Of8FC/jdca/4vT4fWLvHp2klZr0dPNnZcqD7Kp/M18c0AFFFFABRRRQAU+CGS5mjhiRpJZGCIijJYk4AAplfVP7CHwGufHXxDg8YapYv/wj2huJoXlTCT3Q+4BnrtPzH3AoA+xvgJ8PtO/Zq+A6vrDJaTw2zanrE57Pt3FffaPlA7np1r8wfi78SL74tfEPWfE98WDXsxMMROfKiHCIPoMfjmvtL/gor8af7M0Wy+Hmmz4uL7bd6jsPIiB+RD9SM/gK/PygD9T/ANgj/k3HTP8Ar7uP/QhX5ofEP/koHib/ALCd1/6Nav0v/YI/5Nx0z/r7uP8A0IV+aHxD/wCR/wDE3/YTuf8A0a1AH6U/sQ/GOP4rfCRdD1OVbjV9CQWU6SHJlgxiNiO/Hyn6V8JftRfByT4L/FnU9KhiZNHumN3pzkceSx+5n/ZOR+AqP9mL4xSfBb4taVrEsjLpFwwtNRQdDAxALY7lThvwI7199ftm/BM/Gn4UjUdDgF9r2kj7ZZeR8zXMRGXRSOuR8w9SPegD8raKfLE9vK8UqNHIhKsjjBUjqCO1MoAKKKKACiiigD9Mf2D/AI+f8LE8D/8ACI6tc79e0KMLE0jfNPbdFPuV4U+2K+qq/E/4WfEbU/hR470nxPpLkXNlKGePOFmjPDxt7MMj8j2r9l/B3ii08beFdJ1+w3Cz1K2juog4wwDKDg+46fhQBs0UUUAFFFFAHnHjj9nX4cfEjWm1fxF4Vs9R1J1CvclnjdwOm7Ywz9TXO/8ADGvwb/6Ee0/8CJ//AIuvaaKAPFv+GNfg2P8AmSLT/wACJ/8A4umf8Md/Bf8A6Euy/wDAmb/45Xq3i448Ka1/15T/APotq/D3zpP+ejfnQB+tn/DHfwX/AOhLsv8AwJm/+OUf8Md/Bf8A6Euy/wDAmb/45X5J+dJ/z0b86POk/wCejfnQB+uEH7JHwY0+VbkeDNOUxfNmWeVlGPUF8fnWD8Xf2tPh18DPDz6XoM9hq+rQRmO10jSGUwwntvZPlQD0HPtX5WGV2GC7EehNNoA3vHPjXVfiJ4r1HxDrVwbnUb6UySN2X0VR2AGAB7Vg0UUAfqf+wR/ybjpn/X5cf+hCvzQ+If8AyP8A4m/7Cdz/AOjWr9L/ANgj/k3HTf8Ar8uP/QhX5ofEP/kf/E3/AGE7n/0a1AHP19vfsl/tt2PhfRbLwb4/neGytlENjrBBcRp2jlA5wOgYdBXxDRQB+v8Aq/wf+DnxrlGuzaRoXiKSb5jf2M4zJ7s0TDJ+vNZ//DHXwX/6Eyy/8CZv/jlfkirFehI+lL5r/wB9vzoA/W3/AIY6+C//AEJll/4Ezf8Axyj/AIY6+C//AEJll/4Ezf8AxyvyS81/77fnR5r/AN9vzoA/W5f2N/gy4yvgm0b6XM//AMXTv+GNPg3/ANCRa/8AgRP/APF15J/wTSYt8NfFGST/AMTNOv8A1zr7EoA8Yj/Y4+DkUiuPA9oSpyMzzEflvr1/T9PttJsbeysoI7W0t41iihiUKkaAYCgDoAKsUUAFFFFABRRRQAUUUUAZHi//AJFPWv8Aryn/APRbV+HdfuJ4v/5FPWv+vKf/ANFtX4d0AFFFFABRRRQAUUUUAfqd+wP/AMm46b/1+XH8xX5o/EP/AJH/AMTf9hO6/wDRrV+l37A//JuOm/8AX5c/+hCvzR+If/I/+Jv+wnc/+jWoA5+iiigAooooAKKKKAP0W/4Jo/8AJNfFP/YTT/0XX2LXx1/wTR/5Jr4p/wCwmn/ouvsWgAooooAKKKKACiiigAooooAyPF//ACKetf8AXlP/AOi2r8O6/cTxf/yKetf9eU//AKLavw7oAKKKKACiiigAooooA/U79gf/AJNx03/r8uf/AEIV+aPxD/5H/wATf9hO5/8ARrV+l37A/wDybjpv/X5c/wDoQr80fiH/AMj/AOJv+wnc/wDo1qAOfooooAKKKKACiiigD9Fv+CaP/JNfFP8A2E0/9F19i18df8E0f+SaeKf+wmn/AKLr7FoAKKKKACiiigAooooAKKKKAMzxPBJdeGtWhiUvLJaTIijqSUIAr8OCCpIIII4INfuyRkV+fP7Tv7DHiBfFOoeJfh/ZDVNOvpGuJtKjYLLA7HLbASAyk5OByPSgD4rorttX+CHxC0HcdQ8D+ILZB1kbTZin/fQXH61yN1p91YymO5tpreQdUljKkfgaAK9FGMUUAFFFFAH6nfsDf8m46b/1+XP8xX5o/EL/AJH7xL/2E7n/ANGtX6XfsDf8m46b/wBflz/MV+aPxC/5H7xL/wBhO5/9GtQBz9FFFABRRTljZvuqT9BQA2iuh0j4d+K/EG3+y/DOsalu6fZLCWXP/fKmvUPh9+xp8UvHWqwW8vhu68P2LMPNvdWTyVjXudh+Zj7AflQB9Vf8E1LaSL4XeJJmQiOXUxsY9DiMZr7ArjfhH8L9L+D3gLTfDGk5eC1XMk7DDTSnlnPuT+mK7KgAooooAKKKKACiiigAooooAKKKKACq19ptpqcXl3lrBdx/3J4w4/IirNFAHD6v8Dfh5rob7b4J0KZm6uLCNGP/AAJQDXEav+xf8H9X3E+EYrNz1a1nkT9NxH6V7fRQB8tav/wTr+GN9uNnPrGnsf7tyJAPwK1xWq/8EytHck6d4zvY/Rbm2Rsfka+2qKAPN/gB8Im+CXw3tvCzaiNTMM0kv2gR7M7znGM1826p/wAE3Itc8R6lqV34zeOO8upbjy4bQZUO5bGSfevtqigD5F0j/gmz4DtNv2/XdY1DHXaUiz+QNdvpH7CPwg0rbv0O51DH/P3eOc/984r6DooA8y0j9mf4WaHt+y+BdH46efB53/oea7XSvBugaDt/szQ9N07b0+yWkcWP++QK2KKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP//Z';
         var ext = staff.image.substring("data:image/".length, staff.image.indexOf(";base64"));
         var base64String = "";
         start position is 22 or 23
         if (ext.length == 3) {
             base64String = staff.image.substring(22);
         }
         else if (ext.length == 4) {
             base64String = staff.image.substring(23);
         }
 
         staff._attachments = {
             "picture": { // i set all extensions whether jpg or png to jpg to avoid array index error. it does not matter as we already have the content type stored
                 content_type: 'image/' + ext,
                 data: base64String
             },
         }
 
         staff.image = "";
  */
        //}
        if (this.retrieve != undefined) {
            staff._attachments = this.retrieve;

        }
        /*  if (this.file == 'null') {
             return this.db.rel.save('staff', staff)
                 .then((data: any) => {
 
                     if (data && data.staffs && data.staffs
                     [0]) {
                         return data.staffs[0]
                     }
 
                     return null;
                 }).catch((err: any) => {
                     console.error(err);
                 });
         } */


        /*    staff.profile = "";  
              this.db.rel.putAttachment(attachment).then(res => {
            console.log(res)
            console.log("res")
          })   */


        this.db.rel.putAttachment('staff', { id: staff.id, rev: staff.rev }, 'file', staff._attachments, 'image/png').then(res => {
            staff._attachments = res;
            //staff.image = data.staffs[0].image;

        })
        console.log(staff._attachments);
        if (staff._attachments != "") {
            staff.imageurl = URL.createObjectURL(staff._attachments);
        }
        staff.imageurl = "";
        console.log(staff);
        return this.db.rel.save('staff', staff)
            /*  .then((data: any) => {
                 if (data && data.staffs && data.staffs
                 [0]) {
                     console.log(data.staffs[0]);
                     return data.staffs[0]
                 }
 
                 return null;
             }) *//* .catch((err: any) => {
          console.error(err);
          });  */
            .then((data: any) => {

                if (data && data.staffs && data.staffs
                [0]) {


                    /*  this.recordservice.saveRecord(record, data.staffs[0]).then(res => {
                         return data.staffs[0];
                     }); */
                    console.log(data.staffs[0]);
                    return data.staffs[0];

                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });


        // } 
    }


    /**
    * Update a staff
    * @param {staff} staff
    *
    * @return Promise<staff>
    */
    updateStaff(staff: Staff) {

        if (this.file) {
            this.file;

            this.retrieve = (<HTMLInputElement>this.file).files[0];

            if (this.retrieve = (<HTMLInputElement>this.file).files[0]) {
                staff._attachments = this.retrieve
            }

            else {
                staff._attachments;
                //this.getStaff = staff._attachments;
            }

            return this.db.rel.save('staff', staff)
                .then((data: any) => {
                    if (data && data.staffs && data.staffs
                    [0]) {
                        this.db.rel.putAttachment('staff', { id: staff.id, rev: staff.rev }, 'file', staff._attachments, 'image/png').then(res => {
                            var reader: FileReader = new FileReader();

                            reader.onloadend = function (e) {
                                var base64data = reader.result;
                                staff.image = base64data
                                //console.log(staff.image);
                            }
                            reader.readAsDataURL(this.retrieve);
                            staff._attachments = res;
                            staff.image = res

                        })
                        console.log(staff._attachments);
                        staff.imageurl = URL.createObjectURL(staff._attachments);
                        return data.staffs[0]
                    }

                    return null;
                }).catch((err: any) => {
                    console.error(err);
                });

        }
        else {


            return this.db.rel.save('staff', staff)
                .then((data: any) => {
                    if (data && data.staffs && data.staffs
                    [0]) {
                        this.db.rel.putAttachment('staff', { id: staff.id, rev: staff.rev }, 'file', staff._attachments, 'image/png').then(res => {
                            staff._attachments = res
                        })
                        return data.staffs[0]
                    }

                    return null;
                }).catch((err: any) => {
                    console.error(err);
                });
        }
    }

    /**
     * Remove a staff
     * @param {staff} staff
     *
     * @return Promise<boolean>
     */
    removeStaff(staff: Staff): Promise<boolean> {
        return this.db.rel.del('staff', staff)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the staffs
     *
     * @return Promise<Array<staff>>
     */
    getStaffs(): Promise<Array<Staff>> {
        return this.db.rel.find('staff')
            .then((data: any) => {
                this.staffs = data.staffs;

                /*  this.db.rel.getAttachment('staff', 'file').then(attachment => {
                     console.log(attachment)
                     var url = URL.createObjectURL(attachment);
                     console.log(url); */
                /*   $('img').attr('src', URL.createObjectURL(attachment))
                }).then(res => {
                  console.log(res); */
                //var attach = document.getElementById("attachment").innerHTML = attachment

                // })
                if (this.searchText && this.searchText !== '') {
                    this.staffs = FuseUtils.filterArrayByString(this.staffs, this.searchText);
                }
                //this.onstaffsChanged.next(this.staffs);
                var userType = "Patient"
                this.staffs = this.staffs.filter(data => data.usertype != userType);
                return Promise.resolve(this.staffs);
                //return data.staffs ? data.staffs : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getGStaffs(): Promise<Array<Staff>> {
        return this.db.rel.find('staff')
            .then((data: any) => {
                this.staffs = data.staffs;
                console.log(this.staffs);

                /*  this.db.rel.getAttachment('staff', 'file').then(attachment => {
                     console.log(attachment)
                     var url = URL.createObjectURL(attachment);
                     console.log(url); */
                /*   $('img').attr('src', URL.createObjectURL(attachment))
                }).then(res => {
                  console.log(res); */
                //var attach = document.getElementById("attachment").innerHTML = attachment

                // })
                if (this.searchText && this.searchText !== '') {
                    this.staffs = FuseUtils.filterArrayByString(this.staffs, this.searchText);
                }
                //this.onstaffsChanged.next(this.staffs);
                return Promise.resolve(this.staffs);
                //return data.staffs ? data.staffs : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getPStaffs(): Promise<Array<Staff>> {
        return this.db.rel.find('staff')
            .then((data: any) => {
                this.staffs = data.staffs;

                /*  this.db.rel.getAttachment('staff', 'file').then(attachment => {
                     console.log(attachment)
                     var url = URL.createObjectURL(attachment);
                     console.log(url); */
                /*   $('img').attr('src', URL.createObjectURL(attachment))
                }).then(res => {
                  console.log(res); */
                //var attach = document.getElementById("attachment").innerHTML = attachment

                // })
                if (this.searchText && this.searchText !== '') {
                    this.staffs = FuseUtils.filterArrayByString(this.staffs, this.searchText);
                }
                //this.onstaffsChanged.next(this.staffs);
                var userType = "Patient"
                this.staffs = this.staffs.filter(data => data.usertype == userType);
                return Promise.resolve(this.staffs);
                //return data.staffs ? data.staffs : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getDStaffs(): Promise<Array<Staff>> {
        return this.db.rel.find('staff')
            .then((data: any) => {
                this.staffs = data.staffs;

                /*  this.db.rel.getAttachment('staff', 'file').then(attachment => {
                     console.log(attachment)
                     var url = URL.createObjectURL(attachment);
                     console.log(url); */
                /*   $('img').attr('src', URL.createObjectURL(attachment))
                }).then(res => {
                  console.log(res); */
                //var attach = document.getElementById("attachment").innerHTML = attachment

                // })
                if (this.searchText && this.searchText !== '') {
                    this.staffs = FuseUtils.filterArrayByString(this.staffs, this.searchText);
                }
                //this.onstaffsChanged.next(this.staffs);
                var userType = "Doctor"
                this.staffs = this.staffs.filter(data => data.usertype == userType);
                return Promise.resolve(this.staffs);
                //return data.staffs ? data.staffs : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getLStaffs(): Promise<Array<Staff>> {
        return this.db.rel.find('staff')
            .then((data: any) => {
                this.staffs = data.staffs;

                /*  this.db.rel.getAttachment('staff', 'file').then(attachment => {
                     console.log(attachment)
                     var url = URL.createObjectURL(attachment);
                     console.log(url); */
                /*   $('img').attr('src', URL.createObjectURL(attachment))
                }).then(res => {
                  console.log(res); */
                //var attach = document.getElementById("attachment").innerHTML = attachment

                // })
                if (this.searchText && this.searchText !== '') {
                    this.staffs = FuseUtils.filterArrayByString(this.staffs, this.searchText);
                }
                //this.onstaffsChanged.next(this.staffs);
                var userType = "Laboratorist"
                this.staffs = this.staffs.filter(data => data.usertype == userType);
                return Promise.resolve(this.staffs);
                //return data.staffs ? data.staffs : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getAStaffs(): Promise<Array<Staff>> {
        return this.db.rel.find('staff')
            .then((data: any) => {
                this.staffs = data.staffs;

                /*  this.db.rel.getAttachment('staff', 'file').then(attachment => {
                     console.log(attachment)
                     var url = URL.createObjectURL(attachment);
                     console.log(url); */
                /*   $('img').attr('src', URL.createObjectURL(attachment))
                }).then(res => {
                  console.log(res); */
                //var attach = document.getElementById("attachment").innerHTML = attachment

                // })
                if (this.searchText && this.searchText !== '') {
                    this.staffs = FuseUtils.filterArrayByString(this.staffs, this.searchText);
                }
                //this.onstaffsChanged.next(this.staffs);
                var userType = "Accountant"
                this.staffs = this.staffs.filter(data => data.usertype == userType);
                return Promise.resolve(this.staffs);
                //return data.staffs ? data.staffs : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getADStaffs(): Promise<Array<Staff>> {
        return this.db.rel.find('staff')
            .then((data: any) => {
                this.staffs = data.staffs;

                /*  this.db.rel.getAttachment('staff', 'file').then(attachment => {
                     console.log(attachment)
                     var url = URL.createObjectURL(attachment);
                     console.log(url); */
                /*   $('img').attr('src', URL.createObjectURL(attachment))
                }).then(res => {
                  console.log(res); */
                //var attach = document.getElementById("attachment").innerHTML = attachment

                // })
                if (this.searchText && this.searchText !== '') {
                    this.staffs = FuseUtils.filterArrayByString(this.staffs, this.searchText);
                }
                //this.onstaffsChanged.next(this.staffs);
                var userType = "Admin"
                this.staffs = this.staffs.filter(data => data.usertype == userType);
                return Promise.resolve(this.staffs);
                //return data.staffs ? data.staffs : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getNStaffs(): Promise<Array<Staff>> {
        return this.db.rel.find('staff')
            .then((data: any) => {
                this.staffs = data.staffs;

                /*  this.db.rel.getAttachment('staff', 'file').then(attachment => {
                     console.log(attachment)
                     var url = URL.createObjectURL(attachment);
                     console.log(url); */
                /*   $('img').attr('src', URL.createObjectURL(attachment))
                }).then(res => {
                  console.log(res); */
                //var attach = document.getElementById("attachment").innerHTML = attachment

                // })
                if (this.searchText && this.searchText !== '') {
                    this.staffs = FuseUtils.filterArrayByString(this.staffs, this.searchText);
                }
                //this.onstaffsChanged.next(this.staffs);
                var userType = "Nurse"
                this.staffs = this.staffs.filter(data => data.usertype == userType);
                return Promise.resolve(this.staffs);
                //return data.staffs ? data.staffs : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }
    getPHStaffs(): Promise<Array<Staff>> {
        return this.db.rel.find('staff')
            .then((data: any) => {
                this.staffs = data.staffs;

                /*  this.db.rel.getAttachment('staff', 'file').then(attachment => {
                     console.log(attachment)
                     var url = URL.createObjectURL(attachment);
                     console.log(url); */
                /*   $('img').attr('src', URL.createObjectURL(attachment))
                }).then(res => {
                  console.log(res); */
                //var attach = document.getElementById("attachment").innerHTML = attachment

                // })
                if (this.searchText && this.searchText !== '') {
                    this.staffs = FuseUtils.filterArrayByString(this.staffs, this.searchText);
                }
                //this.onstaffsChanged.next(this.staffs);
                var userType = "Pharmacist"
                this.staffs = this.staffs.filter(data => data.usertype == userType);
                return Promise.resolve(this.staffs);
                //return data.staffs ? data.staffs : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getRStaffs(): Promise<Array<Staff>> {
        return this.db.rel.find('staff')
            .then((data: any) => {
                this.staffs = data.staffs;

                /*  this.db.rel.getAttachment('staff', 'file').then(attachment => {
                     console.log(attachment)
                     var url = URL.createObjectURL(attachment);
                     console.log(url); */
                /*   $('img').attr('src', URL.createObjectURL(attachment))
                }).then(res => {
                  console.log(res); */
                //var attach = document.getElementById("attachment").innerHTML = attachment

                // })
                if (this.searchText && this.searchText !== '') {
                    this.staffs = FuseUtils.filterArrayByString(this.staffs, this.searchText);
                }
                //this.onstaffsChanged.next(this.staffs);
                var userType = "Receptionist"
                this.staffs = this.staffs.filter(data => data.usertype == userType);
                return Promise.resolve(this.staffs);
                //return data.staffs ? data.staffs : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a staff
     * @param {staff} staff
     *
     * @return Promise<staff>
     */
    getStaff(staff: Staff): Promise<Staff> {
        /* this.db.rel.getAttachment('staff',staff.id, 'file').then(attachment => {
                    console.log(attachment)
                    var url = URL.createObjectURL(attachment);
                    console.log(url); */
        /*   $('img').attr('src', URL.createObjectURL(attachment))
        }).then(res => {
          console.log(res); */
        //var attach = document.getElementById("attachment").innerHTML = attachment

        //})

        return this.db.rel.find('staff', staff.id)
            .then((data: any) => {

                console.log(data)
                return data && data.staffs ? data.staffs[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getStaff2(id): Promise<Staff> {
        /* this.db.rel.getAttachment('staff',staff.id, 'file').then(attachment => {
                    console.log(attachment)
                    var url = URL.createObjectURL(attachment);
                    console.log(url); */
        /*   $('img').attr('src', URL.createObjectURL(attachment))
        }).then(res => {
          console.log(res); */
        //var attach = document.getElementById("attachment").innerHTML = attachment

        //})

        return this.db.rel.find('staff', id)
            .then((data: any) => {

                console.log(data)
                return data && data.staffs ? data.staffs[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getPStaff(staff: Staff): Promise<Staff> {
        /* this.db.rel.getAttachment('staff',staff.id, 'file').then(attachment => {
                    console.log(attachment)
                    var url = URL.createObjectURL(attachment);
                    console.log(url); */
        /*   $('img').attr('src', URL.createObjectURL(attachment))
        }).then(res => {
          console.log(res); */
        //var attach = document.getElementById("attachment").innerHTML = attachment

        //})

        return this.db.rel.find('staff', staff.id)
            .then((data: any) => {

                console.log("data")
                return data && data.staffs ? data.staffs[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    getPStaff2(id): Promise<Staff> {
        /* this.db.rel.getAttachment('staff',staff.id, 'file').then(attachment => {
                    console.log(attachment)
                    var url = URL.createObjectURL(attachment);
                    console.log(url); */
        /*   $('img').attr('src', URL.createObjectURL(attachment))
        }).then(res => {
          console.log(res); */
        //var attach = document.getElementById("attachment").innerHTML = attachment

        //})

        return this.db.rel.find('staff', id)
            .then((data: any) => {

                console.log(data)
                return data && data.staffs ? data.staffs[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected staff by id
     * @param id
     */
    toggleSelectedStaff(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedStaffs.length > 0) {
            const index = this.selectedStaffs.indexOf(id);

            if (index !== -1) {
                this.selectedStaffs.splice(index, 1);

                // Trigger the next event
                this.onSelectedStaffsChanged.next(this.selectedStaffs);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedStaffs.push(id);

        // Trigger the next event
        this.onSelectedStaffsChanged.next(this.selectedStaffs);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedStaffs.length > 0) {
            this.deselectStaffs();
        }
        else {
            this.selectStaffs();
        }
    }

    selectStaffs(filterParameter?, filterValue?) {
        this.selectedStaffs = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedStaffs = [];
            this.staffs.map(staff => {
                this.selectedStaffs.push(staff.id);
            });
        }
        else {
            /* this.selectedstaffs.push(...
                 this.staffs.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedStaffsChanged.next(this.selectedStaffs);
    }





    deselectStaffs() {
        this.selectedStaffs = [];

        // Trigger the next event
        this.onSelectedStaffsChanged.next(this.selectedStaffs);
    }

    deleteStaff(staff) {
        this.db.rel.del('staff', staff)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const staffIndex = this.staffs.indexOf(staff);
                this.staffs.splice(staffIndex, 1);
                this.onStaffsChanged.next(this.staffs);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedStaffs() {
        for (const staffId of this.selectedStaffs) {
            const staff = this.staffs.find(_staff => {
                return _staff.id === staffId;
            });

            this.db.rel.del('staff', staff)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const staffIndex = this.staffs.indexOf(staff);
                    this.staffs.splice(staffIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onStaffsChanged.next(this.staffs);
        this.deselectStaffs();
    }
}
