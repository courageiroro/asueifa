import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FuseConfigService } from '../../../../../core/services/config.service';
import { StaffsService } from '../../../apps/staffs/staffs.service';
import { Router } from '@angular/router';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg'
import { BehaviorSubject } from 'rxjs/Rx';
import 'rxjs/Rx';
/* import { AppsService } from '../../../../../app.service'; */
import { Staff } from '../../../apps/staffs/staff.model';
import { DepartmentsService } from '../../../apps/departments/departments.service';
import { UserService } from 'app/user.service';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-create-account',
    templateUrl: './create-account.component.html',
    styleUrls: ['./create-account.component.scss']
})
export class FuseCreateAccountComponent implements OnInit {
    createAccountForm: FormGroup;
    createAccountFormErrors: any;
    public email;
    public answer;
    public secretQuestion;
    public data;
    public username: any;
    public password;
    public error;
    public departmentss;
    public imagestring;
    public retrieve;
    public file;
    errorMessageUser: any;
    public userTypes;
    staff: Staff;
    public loading = false;
    disabled = false;

    constructor(
        private fuseConfig: FuseConfigService,
        private formBuilder: FormBuilder,
        private staffservice: PouchService,
        private router: Router,
        private user: UserService,
        /* private appService: AppsService */
    ) {

        document.addEventListener('click', function () {
            staffservice.file = document.querySelector('#fileupload')
            var imagestring = document.querySelector('#imagestring');
            console.log(imagestring)
            var retrieve;
        })

        staffservice.getDepartments().then(res => {
            this.departmentss = res;
        })

        this.fuseConfig.setSettings({
            layout: {
                navigation: 'none',
                toolbar: 'none',
                footer: 'none'
            }
        });

        this.createAccountFormErrors = {
            id: {},
            rev: {},
            department_id: {},
            usertype: {},
            name: {},
            email: {},
            password: {},
            address: {},
            phone: {},
            secretquestion: {},
            answer: {},
            profile: {},
            _attachments: {},
        };

        /*   this.createAccountForm = this.createaccountForm(); */
    }


    emailChange() {
        this.staffservice.getADStaffs().then(res => {
            res = res.filter(data => data.email == this.email.trim());
            this.data = res;
            this.secretQuestion = res[0].secretquestion;
        })
    }

    checkUser(){
        this.disabled = false;
        console.log(this.username);
        this.errorMessageUser = "";
        this.staffservice.getGStaffs().then(res => {
            console.log(res);
            res.forEach(item => {
                if (this.username.toLowerCase() == item.email.toLowerCase()) {
                    this.errorMessageUser = "This email already exists";
                    this.disabled = true;
                }
            })
        })
    }

    navLP() {
        this.router.navigate(['/apps/login']);
    }

    ngOnInit() {

        this.userTypes = 'Admin';

        this.createAccountForm = this.formBuilder.group({
            id: '',
            rev: '',
            name: '',
            email: ['', [Validators.required, Validators.email]],
            password: '',
            department_id: '',
            usertype: { value: 'Admin', disabled: true },
            address: '',
            phone: '',
            secretquestion: '',
            answer: '',
            profile: '',
            _attachments: ''

        });

        this.createAccountForm.valueChanges.subscribe(() => {
            this.onForgotPasswordFormValuesChanged();
        });
    }


    save() {
        console.log("saved")
        this.loading = true;

        const formData: FormGroup = this.createAccountForm;
        var res = formData.getRawValue();
        var reader = new FileReader();
        var file = this.staffservice.file;
        var retrieve = (<HTMLInputElement>file).files[0];
        var file2;
        var myThis = this;
        console.log(retrieve);
        if (retrieve == undefined) {
            res.profile
            console.log(res.profile)
            this.staffservice.saveStaff(res).then(data => {
                console.log(res);
                this.staffservice.getGStaffs().then(data => {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].email == this.username && data[i].password == this.password) {
                            this.user.setUserLoggedIn();

                            localStorage.setItem('user', JSON.stringify(data[i]));

                            if (data[i].usertype == 'Admin') {
                                this.router.navigate(['/apps/login']).then(res => {
                                    //this.loading = false;
                                }, err => {
                                    //this.loading = false;
                                });
                            }
                        }
                    }
                    //Configure Records
                    /* this.appService.initDB(); */
                    this.initSettings();
                });
            })

        }
        else {
            reader.readAsDataURL(retrieve);
            reader.onloadend = function () {
                reader.result;
                console.log(reader.result);
                res.image = reader.result;
                console.log(res.image);
                res.profile
                console.log(res.profile)
                console.log(res);
                myThis.staffservice.saveStaff(res).then(data => {
                    console.log(res);
                    myThis.staffservice.getGStaffs().then(data => {
                        console.log(data);
                        for (var i = 0; i < data.length; i++) {
                            if (data[i].email == myThis.username && data[i].password == myThis.password) {
                                myThis.user.setUserLoggedIn();

                                localStorage.setItem('user', JSON.stringify(data[i]));

                                if (data[i].usertype == 'Admin') {
                                    myThis.router.navigate(['/apps/login']).then(res => {
                                        myThis.loading = false;
                                    }, err => {
                                        myThis.loading = false;
                                    });
                                }
                            }
                        }
                        //Configure Records
                        /* myThis.appService.initDB(); */
                        myThis.initSettings();
                    });
                })

            }

        }



    }

    initSettings() {
      /*   let records = this.getRecords();
        records.map(async (record) => {
           console.log(record);
            this.appService.saveConfigurerecord(record).then(record => {
            });
        });
 */
    }

    getRecords() {
        return [
            {
                id: '',
                rev: '',
                name: 'Date of Birth',
                category: 'General',
                type: 'date'
            },
            {
                id: '',
                rev: '',
                name: 'Marital Status',
                category: 'General',
                type: 'input'
            },
            {
                id: '',
                rev: '',
                name: 'Occupation',
                category: 'General',
                type: 'input'
            },
            {
                id: '',
                rev: '',
                name: 'BMI',
                category: 'General',
                type: 'input'
            },
            {
                id: '',
                rev: '',
                name: 'Doctor',
                category: 'General',
                type: 'input'
            },
            {
                id: '',
                rev: '',
                name: 'Doctors No:',
                category: 'General',
                type: 'input'
            },
            {
                id: '',
                rev: '',
                name: 'Hospital',
                category: 'General',
                type: 'input'
            },
            {
                id: '',
                rev: '',
                name: 'Relationship',
                category: 'General',
                type: 'input'
            },
            {
                id: '',
                rev: '',
                name: 'Religion',
                category: 'General',
                type: 'input'
            },
            {
                id: '',
                rev: '',
                name: 'Allergies',
                category: 'General',
                type: 'text'
            },
            {
                id: '',
                rev: '',
                name: 'Previous Surgeries',
                category: 'Medical History',
                type: 'text'
            },
            {
                id: '',
                rev: '',
                name: 'Previous Ailments',
                category: 'Medical History',
                type: 'text'
            },
            {
                id: '',
                rev: '',
                name: 'Family History',
                category: 'Family History',
                type: 'text'
            },
            {
                id: '',
                rev: '',
                name: 'Notes',
                category: 'Notes',
                type: 'text'
            },
            {
                id: '',
                rev: '',
                name: 'OTC',
                category: 'Current Medications',
                type: 'text'
            },
            {
                id: '',
                rev: '',
                name: 'Herbal',
                category: 'Current Medications',
                type: 'text'
            },
            {
                id: '',
                rev: '',
                name: 'Prescriptions',
                category: 'Current Medications',
                type: 'text'
            },
            {
                id: '',
                rev: '',
                name: 'Diabetes',
                category: 'Medical Conditions',
                type: 'checkbox'
            },
            {
                id: '',
                rev: '',
                name: 'Hypertension',
                category: 'Medical Conditions',
                type: 'checkbox'
            },
            {
                id: '',
                rev: '',
                name: 'Asthma',
                category: 'Medical Conditions',
                type: 'checkbox'
            },
            {
                id: '',
                rev: '',
                name: 'Genotype',
                category: 'Medical Conditions',
                type: 'checkbox'
            },
            {
                id: '',
                rev: '',
                name: 'AKI',
                category: 'Medical Conditions',
                type: 'checkbox'
            },
            {
                id: '',
                rev: '',
                name: 'CKD',
                category: 'Medical Conditions',
                type: 'checkbox'
            },
            {
                id: '',
                rev: '',
                name: 'Liver Disease',
                category: 'Medical Conditions',
                type: 'checkbox'
            },
            {
                id: '',
                rev: '',
                name: 'Arthritis',
                category: 'Medical Conditions',
                type: 'checkbox'
            },
            {
                id: '',
                rev: '',
                name: 'GERD',
                category: 'Medical Conditions',
                type: 'checkbox'
            },
            {
                id: '',
                rev: '',
                name: 'Cancer',
                category: 'Medical Conditions',
                type: 'checkbox'
            },
            {
                id: '',
                rev: '',
                name: 'Gout',
                category: 'Medical Conditions',
                type: 'checkbox'
            },
            {
                id: '',
                rev: '',
                name: 'Thyroid Disorder',
                category: 'Medical Conditions',
                type: 'checkbox'
            },
            {
                id: '',
                rev: '',
                name: 'Cigarette / Tobacco',
                category: 'Social History',
                type: 'text'
            },
            {
                id: '',
                rev: '',
                name: 'Alochol',
                category: 'Social History',
                type: 'text'
            },
            {
                id: '',
                rev: '',
                name: 'Exercise',
                category: 'Social History',
                type: 'text'
            },
            {
                id: '',
                rev: '',
                name: 'Coffee',
                category: 'Social History',
                type: 'text'
            }
        ];
    }



    onForgotPasswordFormValuesChanged() {
        /*   for ( const field in this.forgotPasswordFormErrors )
          {
              if ( !this.forgotPasswordFormErrors.hasOwnProperty(field) )
              {
                  continue;
              }
     
              // Clear previous errors
              this.forgotPasswordFormErrors[field] = {};
     
              // Get the control
              const control = this.forgotPasswordFormErrors.get(field);
     
              if ( control && control.dirty && !control.valid )
              {
                  this.forgotPasswordFormErrors[field] = control.errors;
              }
          } */
    }
}
