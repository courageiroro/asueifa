import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../../core/modules/shared.module';
import { RouterModule } from '@angular/router';
import { StaffsService } from '../../../apps/staffs/staffs.service';
import { DepartmentsService } from '../../../apps/departments/departments.service';
import { FuseCreateAccountComponent } from './create-account.component';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { LoadingModule } from 'ngx-loading';
import { UserService } from 'app/user.service';
import {PouchService} from '../../../../../provider/pouch-service';

const routes = [
    {
        path: 'pages/auth/create-account',
        component: FuseCreateAccountComponent,
        resolve: {
            staffs: PouchService
        }
    }
];

@NgModule({
    declarations: [
        FuseCreateAccountComponent
    ],
    imports: [
        SharedModule,
        RouterModule.forChild(routes),
        FroalaEditorModule.forRoot(),
        FroalaViewModule.forRoot(),
        LoadingModule
    ],
    providers: [
        StaffsService,
        PouchService,
        DepartmentsService,
        UserService
    ]
})

export class CreateAccountModule {

}
