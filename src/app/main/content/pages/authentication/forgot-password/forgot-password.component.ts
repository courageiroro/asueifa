import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FuseConfigService } from '../../../../../core/services/config.service';
import { StaffsService } from '../../../apps/staffs/staffs.service';
import { Router } from '@angular/router';
import {PouchService} from '../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-forgot-password',
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.scss']
})
export class FuseForgotPasswordComponent implements OnInit {
    forgotPasswordForm: FormGroup;
    forgotPasswordFormErrors: any;
    public email;
    public answer;
    public secretQuestion;
    public data;
    public password;
    public error;

    constructor(
        private fuseConfig: FuseConfigService,
        private formBuilder: FormBuilder,
        private staffservice: PouchService,
        private router: Router,
    ) {

        /*  var userType = { usertype: "null" }
         localStorage.setItem('user', JSON.stringify(userType)); */

        this.fuseConfig.setSettings({
            layout: {
                navigation: 'none',
                toolbar: 'none',
                footer: 'none'
            }
        });

        this.forgotPasswordFormErrors = {
            email: {},
            answer: {}
        };
    }


    emailChange() {
        this.staffservice.getADStaffs().then(res => {
            res = res.filter(data => data.email == this.email.trim());
            this.data = res;
            this.secretQuestion = res[0].secretquestion;
            
        })
    }

    revealPassword() {

        if (this.answer.trim() == this.data[0].answer) {
            this.password = this.data[0].password;
            this.error = "";
        }
        else if (this.answer.trim() != this.data[0].answer) {
            this.error = "Wrong answer, please check your answer";
        }
    }

    navLP() {
        this.router.navigate(['/apps/login']);
    }


    ngOnInit() {
        this.forgotPasswordForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            answer: ['',]
        });

        this.forgotPasswordForm.valueChanges.subscribe(() => {
            this.onForgotPasswordFormValuesChanged();
        });
    }

    onForgotPasswordFormValuesChanged() {
        /*   for ( const field in this.forgotPasswordFormErrors )
          {
              if ( !this.forgotPasswordFormErrors.hasOwnProperty(field) )
              {
                  continue;
              }
     
              // Clear previous errors
              this.forgotPasswordFormErrors[field] = {};
     
              // Get the control
              const control = this.forgotPasswordFormErrors.get(field);
     
              if ( control && control.dirty && !control.valid )
              {
                  this.forgotPasswordFormErrors[field] = control.errors;
              }
          } */
    }
}
