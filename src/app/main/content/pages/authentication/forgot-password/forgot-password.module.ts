import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../../core/modules/shared.module';
import { RouterModule } from '@angular/router';
import { StaffsService } from '../../../apps/staffs/staffs.service';
import {PouchService} from '../../../../../provider/pouch-service';
import { FuseForgotPasswordComponent } from './forgot-password.component';

const routes = [
    {
        path     : 'pages/auth/forgot-password',
        component: FuseForgotPasswordComponent,
        resolve: {
            staffs: PouchService
        }
    }
];

@NgModule({
    declarations: [
        FuseForgotPasswordComponent
    ],
    imports     : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    providers: [
        StaffsService,
        PouchService
    ]
})

export class ForgotPasswordModule
{

}
