import { Component, Inject, OnInit, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { RecordsService } from 'app/main/content/apps/records/records.service';
import { StaffsService } from 'app/main/content/apps/staffs/staffs.service';
import { SettingsService } from 'app/main/content/apps/settings/settings.service';
import { CurrencysService } from 'app/main/content/apps/currencys/currencys.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { ConfigurerecordsService } from 'app/main/content/apps/configure_records/configure_records.service';
import { Subject } from 'rxjs/Subject';
import { Record } from 'app/main/content/apps/records/record.model';
import { Configurerecord } from 'app/main/content/apps/configure_records/configure_record.model';
import {PouchService} from './../../../../provider/pouch-service';
import { FuseConfigService } from '../../../../core/services/config.service';

@Component({
    selector: 'fuse-print-page',
    templateUrl: './print-page.component.html',
    styleUrls: ['./print-page.component.scss']
})
export class FusePrintpageComponent implements OnInit {

    event: CalendarEvent;
    dialogTitle: string;
    recordreceiptForm: FormGroup;
    action: string;
    public settings;
    public result;
    public final;
    public hospitalName;
    public result2;
    public final2;
    public hospitalAddress;
    public result3;
    public final3;
    public hospitalPhone;
    public result4;
    public final4;
    public hospitalEmail;
    public Pid;
    public recordDate: any;
    public recordMonth: any;
    public recordNumber: any;
    public recordDueDate: any;
    public staffName: any;
    public basicSalary: any;
    public userType: any;
    public allowancess;
    public deductionss;
    public staffId;
    public staffAddress;
    public staffNumber;
    public configurerecords: Array<Configurerecord> = [];
    public staffEmail;
    public subTotal;
    public vat;
    patients;
    patientName;
    patientAddress;
    patientPhone;
    patientEmail;
    public discount;
    public total;
    checkboxstate;
    public currencys;
    public result5;
    public final5;
    public displayCurrencys;
    urlRecord;
    image;
    general = [];
    refresh: Subject<any> = new Subject();
    medicalhistory = [];
    socialhistory = [];
    medicalconditions = [];
    currentmedications = [];
    familyhistory = [];
    notes = [];
    recordss: Record[];
    record: any = {
        id: Math.round((new Date()).getTime()).toString(),
        rev: '',
        customer: ''
    };
    test;
    img;

    constructor(
        private fuseConfig: FuseConfigService, public db: PouchService,
        public _DomSanitizer: DomSanitizer,
        public router: Router,
        public activateRoute: ActivatedRoute
    ) {

        let id = this.activateRoute.snapshot.params['id'];
        this.Pid = id;
        

        this.fuseConfig.setSettings({
            layout: {
                navigation: 'none',
                toolbar: 'none',
                footer: 'none'
            }
        });

    }

    ngOnInit() {
        this.test = "I did it";
        this.db.getPStaff2(this.Pid).then(res => {
            this.patients = res;
            
            this.patientName = res.name;
            this.patientAddress = res.address;
            this.patientPhone = res.phone;
            this.patientEmail = res.email;
            if (res._attachments) {
                this.image = true;
                this.urlRecord = URL.createObjectURL(res._attachments);
                this.img = document.createElement('img');
                this.img.src = this.urlRecord;
                document.getElementById('img3').appendChild(this.img).style.width = '50px';
                document.getElementById('img3').appendChild(this.img).style.height = '50px';
                document.getElementById('img3').appendChild(this.img).style.borderRadius = '50%';

                
            }
            else {
                this.image = false
            }
            
            if (this.patients.record == "") {
                this.record = {
                    id: Math.round((new Date()).getTime()).toString(),
                    rev: '',
                    customer: ''
                };
            }
            else {
                this.db.getRecord(this.patients.record).then(record => {
                    this.record = record;
                    
                })
            }
        })
        this._loadrecords();

    }

    private _loadrecords(): void {
        this.db.getConfigurerecords()
            .then(configurerecords => {
                this.configurerecords = configurerecords;
                //this.records2 = new BehaviorSubject<any>(records);
                this.general = this.configurerecords.filter(data => data.category == 'General');
                this.refresh.next(this.general);
                this.medicalhistory = this.configurerecords.filter(data => data.category == 'Medical History');
                this.socialhistory = this.configurerecords.filter(data => data.category == 'Social History');
                this.medicalconditions = this.configurerecords.filter(data => data.category == 'Medical Conditions');
                this.currentmedications = this.configurerecords.filter(data => data.category == 'Current Medications');
                this.familyhistory = this.configurerecords.filter(data => data.category == 'Family History');
                this.notes = this.configurerecords.filter(data => data.category == 'Notes');

                //console.log(this.records2);
            });

    }


    back() {
        this.router.navigate(['apps/record/', this.Pid]);
    }
}
