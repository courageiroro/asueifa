import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule } from '@angular/router';
import { AuthGuard } from 'app/auth.guard';
import { RecordsService } from 'app/main/content/apps/records/records.service';
import { StaffsService } from 'app/main/content/apps/staffs/staffs.service';
import { SettingsService } from 'app/main/content/apps/settings/settings.service';
import { CurrencysService } from 'app/main/content/apps/currencys/currencys.service';
import { ConfigurerecordsService } from 'app/main/content/apps/configure_records/configure_records.service';
import {PouchService} from './../../../../provider/pouch-service';
import { FusePrintpageComponent } from './print-page.component';

const routes = [
    {
        path: 'pages/print/:id',
        component: FusePrintpageComponent,
        canActivate: [AuthGuard],
        resolve  : {
            staffs: PouchService
        }

    }
];

@NgModule({
    declarations: [
        FusePrintpageComponent
    ],
    imports: [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    providers: [
        AuthGuard,
        RecordsService,
        StaffsService,
        PouchService,
        SettingsService,
        CurrencysService,
        ConfigurerecordsService
    ],
})

export class PrintpageModule {

}
