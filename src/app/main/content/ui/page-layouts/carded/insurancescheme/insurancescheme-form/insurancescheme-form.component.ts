import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import 'rxjs/Rx';
import { Insurancetype } from 'app/main/content/apps/insurancetypes/insurancetype.model';
import { StaffsService } from 'app/main/content/apps/staffs/staffs.service';

@Component({
    selector: 'fuse-insuranceschemes-insurancescheme-form-dialog',
    templateUrl: './insurancescheme-form.component.html',
    styleUrls: ['./insurancescheme-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseInsuranceschemesInsuranceschemeFormDialogComponent implements OnInit {
    event: CalendarEvent;
    dialogTitle: string;
    insuranceschemeForm: FormGroup;
    action: string;
    insurancescheme: Insurancetype;
    public patientss;
    patient;
    patientObj;

    constructor(
        public dialogRef: MdDialogRef<FuseInsuranceschemesInsuranceschemeFormDialogComponent>,
        @Inject(MD_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        public db: StaffsService
    ) {
        this.db.getPStaffs().then(res => {
            this.patientss = res;
            console.log(this.patientss);
        })

        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Insurance Scheme';
            this.insurancescheme = data.insurancescheme;
            console.log(this.insurancescheme)
        }
        else {
            this.dialogTitle = 'New Insurance Scheme';
            this.insurancescheme = {
                id: '',
                rev: '',
                name: '',
                description: '',
                percentage: null,
                patient: ''
            }
        }

        this.insuranceschemeForm = this.createinsuranceschemeForm();
    }

    ngOnInit() {
    }
    getId(patient) {
        console.log(patient);
        console.log(this.patient)
        this.patientObj = patient;
    }

    save() {
        console.log(this.patientObj)
        this.patientObj.insurancename = this.insurancescheme.name;
        this.patientObj.insuranceid = this.insurancescheme.id;
        this.patientObj.insurancepercent = this.insurancescheme.percentage;
        this.db.updateStaff(this.patientObj).then(res=>{
            console.log(res)
        })
    }

    createinsuranceschemeForm() {
        return this.formBuilder.group({
            id: [this.insurancescheme.id],
            rev: [this.insurancescheme.rev],
            name: [{ value: this.insurancescheme.name, disabled: true }, Validators.required],
            description: [this.insurancescheme.description],
            patient: [this.insurancescheme.patient],
            percentage: [{ value: this.insurancescheme.percentage, disabled: true }, Validators.required],

        });
    }
}
