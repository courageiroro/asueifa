import { Component, OnInit } from '@angular/core';
import { InsurancetypesService } from 'app/main/content/apps/insurancetypes/insurancetypes.service';
import { FuseInsuranceschemesInsuranceschemeFormDialogComponent } from './insurancescheme-form/insurancescheme-form.component'
import { MdDialog, MdDialogRef } from '@angular/material';
import { FormGroup } from '@angular/forms';
import { Staff } from 'app/main/content/apps/staffs/staff.model';
import {PouchService} from '../../../../../../provider/pouch-service';

@Component({
    selector: 'fuse-carded-insurancescheme',
    templateUrl: './insurancescheme.component.html',
    styleUrls: ['./insurancescheme.component.scss']
})
export class FuseCardedInsuranceschemeComponent implements OnInit {
    loopCheckBoxs;
    dialogRef: any;
    cols;
    insuranceData;
    selected = false;
    staff: Staff;

    constructor(public db: PouchService, public dialog: MdDialog) {

        this.db.getInsurancetypes().then(res => {
            this.loopCheckBoxs = res
            console.log(res);
        })
        this.staff = {
            id: '',
            rev: '',
            name: '',
            image: '',
            email: '',
            password: '',
            address: '',
            phone: '',
            department_id: '',
            secretquestion: '',
            answer: '',
            profile: '',
            _attachments: '',
            usertype: '',
            sex: '',
            imageurl:'',
            birth_date: new Date(),
            age: '',
            patientno: '',
            blood_group:'',
            account_opening_timestamp: '',
            record: '',
            records: '',
            labrecord: '',
            labrecords: '',
            insurancename: '',
            insuranceid: '',
            insurancepercent: null,
            appointments: [],
            bed_allotments: [],
            invoices: [],
            medicine_sales: [],
            prescriptions: [],
            reports: [],
            email_templates: [],
            smss: [],
            appointment: [],
            diagnosis_reports: [],
            blood_sales: []
        }
     
    }
    ngOnInit() {
        console.log(screen.height);
        console.log(screen.width);
        this.cols = 4
    }

    click(loopCheckBox) {
        console.log(loopCheckBox);
        this.selected = true;
        this.insuranceData = loopCheckBox;
    }

    newInsurancescheme() {
        console.log(this.insuranceData);
        this.dialogRef = this.dialog.open(FuseInsuranceschemesInsuranceschemeFormDialogComponent, {
            panelClass: 'insurancescheme-form-dialog',
            data: {
                insurancescheme: this.insuranceData,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
            });

    }

    newInsurancescheme2() {
        console.log(this.insuranceData);
        this.dialogRef = this.dialog.open(FuseInsuranceschemesInsuranceschemeFormDialogComponent, {
            panelClass: 'insurancescheme-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
            });

    }


}
