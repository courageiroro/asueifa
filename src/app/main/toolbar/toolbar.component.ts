import { Component, OnInit, } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { PouchService } from '../../provider/pouch-service';
import { FuseSplashScreenService } from '../../core/services/splash-screen.service';
/* import { remote, ipcRenderer } from 'electron';

const { remote } = window.require('electron');

const fs = window.require('fs'); */
/* declare const window: any;
const electron = window.require('electron');
const BrowserWindow = electron.remote.BrowserWindow;
const url = window.require('url')
//const { BrowserWindow, dialog, shell } = window.require('electron');
const { getCurrentWindow, globalShortcut } = window.require('electron').remote; */

@Component({
    selector: 'fuse-toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls: ['./toolbar.component.scss']
})

export class FuseToolbarComponent implements OnInit {
    userStatusOptions: any[];
    languages: any;
    selectedLanguage: any;
    showSpinner: boolean;
    localStorageItem;
    window: Window;
    nativeWindowOpen: true
    name;
    image;
    public loading = false;
    alerts;
    alertsLength;
    url;


    constructor(private fuseSplashScreen: FuseSplashScreenService, private router: Router, public _DomSanitizer: DomSanitizer, private db: PouchService) {

        this.localStorageItem = JSON.parse(localStorage.getItem('user'));

        this.userStatusOptions = [
            {
                'title': 'Online',
                'icon': 'icon-checkbox-marked-circle',
                'color': '#4CAF50'
            },
            {
                'title': 'Away',
                'icon': 'icon-clock',
                'color': '#FFC107'
            },
            {
                'title': 'Do not Disturb',
                'icon': 'icon-minus-circle',
                'color': '#F44336'
            },
            {
                'title': 'Invisible',
                'icon': 'icon-checkbox-blank-circle-outline',
                'color': '#BDBDBD'
            },
            {
                'title': 'Offline',
                'icon': 'icon-checkbox-blank-circle-outline',
                'color': '#616161'
            }
        ];

        this.languages = [
            {
                'id': 'en',
                'title': 'English',
                'flag': 'us'
            },
            {
                'id': 'es',
                'title': 'Spanish',
                'flag': 'es'
            },
            {
                'id': 'tr',
                'title': 'Turkish',
                'flag': 'tr'
            }
        ];

        this.selectedLanguage = this.languages[0];

        router.events.subscribe(
            (event) => {
                if (event instanceof NavigationStart) {
                    this.showSpinner = true;
                }
                if (event instanceof NavigationEnd) {
                    this.showSpinner = false;
                }
            });
    }

    signOut() {
        /*  */
        this.router.navigate(['/apps/login']);
    }

    profileNav() {
        this.router.navigate(['/apps/admins']);
    }

    ngOnInit() {

        this.db.getInvoices().then(res => {
            this.alerts = res;
            this.alerts = this.alerts.filter(data => data.notstatus == false);
            this.alertsLength = this.alerts.length;
            console.log(this.alertsLength)
        })

        this.image = this.localStorageItem.image;
        this.name = this.localStorageItem.name;

    }

    search(value) {
        // Do your search here...

    }

    navAlerts() {
        this.db.getInvoices().then(res => {
            this.alerts = res;
            this.alerts.forEach(item => {
                item.notstatus = true
                this.db.updateInvoice(item).then(res => {

                })
            })
        })
        this.router.navigate(['apps/alerts'])
    }

    sync() {

        /* var remote = window.require('electron').remote;
        //remote.getCurrentWindow().reload();
        remote.getCurrentWindow().loadURL('../../../public/index.html'); */
        this.loading = true;
        let credentials = JSON.parse(localStorage.getItem('credentials'));
        this.db.enableSyncing(credentials);
        this.loading = false;
        /* var url = 'C:/Users/IRORO/Desktop/angularprojects/asueifa-hospital/src/app/app.component';
       win.loadURL(url); */
       /*  console.log(__dirname);
        console.log('file://' + __dirname + '/../public/index.html'); */

        /*  var reload = () => {
             getCurrentWindow().reload()
         }
 
         globalShortcut.register('F5', reload);
         globalShortcut.register('CommandOrControl+R', reload);
         // here is the fix bug #3778, if you know alternative ways, please write them
         window.addEventListener('beforeunload', () => {
             globalShortcut.unregister(reload);
             globalShortcut.unregister(reload);
         })
         console.log("SYNC");
         this.window.location.reload();
         this.loading = true;
         let credentials = JSON.parse(localStorage.getItem('credentials'));
         this.db.enableSyncing(credentials);
         this.loading = false; */
        /*  let win;
  
          win = new BrowserWindow({
              width: 1024,
              height: 600,
              //icon: __dirname + '/../dist/assets/icons/win/icon2.ico',
              title: "Asueifai Hospital Manager",
              backgroundColor: '#c0f4fa',
              webPreferences: {
                  nativeWindowOpen: true
              },
              
              center: true,
              // [NativeImage](https://github.com/atom/electron/blob/master/docs/api/native-image.md) - The window icon, when omitted on Windows the executable's icon would be used as window icon. 
              icon: 'assets/icons/win/icon2.ico',
              // Boolean - When setting `false`, it will disable the same-origin policy (Usually using testing websites by people), and set `allowDisplayingInsecureContent` and `allowRunningInsecureContent` to `true` if these two options are not set by user. Default is `true`. 
              webSecurity: false,
      
              node: {
                  __dirname: false
              },
      
              // Boolean - Allow an https page to display content like images from http URLs. Default is `false`. 
              allowDisplayingInsecureContent: true,
      
              // Boolean - Allow a https page to run JavaScript, CSS or plugins from http URLs. Default is `false`. 
              allowRunningInsecureContent: true,
          });
       
          win.maximize();
         
      
          var url = 'C:/Users/IRORO/Desktop/angularprojects/asueifa-hospital/public/index.html';
          setTimeout(() => {
              win.loadURL(url);
          }, 2000); // 1 second wasn't enough lol */


    }
}
