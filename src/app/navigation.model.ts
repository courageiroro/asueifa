export class FuseNavigation {
    public items: any[];

    constructor() {
        this.items = [
            {
                'title': 'Dashboard',
                'icon': 'dashboard',
                'type': 'nav-item',
                'url': '/apps/dashboards/project'
            },
            {
                'title': 'Department',
                'type': 'nav-item',
                'icon': 'class',
                'url': '/apps/departments'
            },
            {
                'title': 'Appointment',
                'type': 'nav-collapse',
                'icon': 'assignment',
                'children': [
                    {
                        'title': 'Add Appointment',
                        'type': 'nav-item',
                        'icon': 'add',
                        'url': '/apps/addappointments'
                    },
                    {
                        'title': 'View Appointments',
                        'type': 'nav-item',
                        'icon': 'assignment',
                        'url': '/apps/appointments'
                    },
                ]

            },
            {
                'title': 'Hospital Staff',
                'type': 'nav-collapse',
                'icon': 'add',
                'children': [
                    /*  {
                         'title': 'Admin',
                         'type': 'nav-item',
                         'icon': 'person',
                         'url': '/apps/accounts'
                     }, */
                    {
                        'title': 'Add Staff',
                        'type': 'nav-item',
                        'icon': 'account_circle',
                        'url': '/apps/staffs'
                    },
                    {
                        'title': 'Doctor',
                        'type': 'nav-collapse',
                        'icon': 'add',
                        'children': [

                            {
                                'title': 'Doctors Prescription',
                                'type': 'nav-item',
                                'icon': 'note_add',
                                'url': '/apps/prescriptions/:id'
                            },
                        ]
                    },
                    /*   {
                          'title': 'Nurse',
                          'type': 'nav-item',
                          'icon': 'healing',
                          'url': '/apps/nurses'
                      },
                      {
                          'title': 'Pharmacist',
                          'type': 'nav-item',
                          'icon': 'local_pharmacy',
                          'url': '/apps/pharmacists'
                      }, */
                    {
                        'title': 'Laboratorist',
                        'type': 'nav-collapse',
                        'icon': 'person',
                        'children': [
                            /*  {
                                 'title': 'Add Laboratorist',
                                 'icon': 'person',
                                 'type': 'nav-item',
                                 'url': '/apps/laboratorists'
                             }, */
                            {
                                'title': 'Diagnosis Report',
                                'icon': 'colorize',
                                'type': 'nav-item',
                                'url': '/apps/diagnosis_reports'
                            },
                        ]

                    },
                    /*   {
                          'title': 'Accountant',
                          'icon': 'account_balance_wallet',
                          'type': 'nav-item',
                          'url': '/apps/accountants'
                      },
                      {
                          'title': 'Receptionist',
                          'icon': 'contact_phone',
                          'type': 'nav-item',
                          'url': '/apps/receptionists'
                      }, */

                    {
                        'title': 'Accountant',
                        'type': 'nav-collapse',
                        'icon': 'add',
                        'children': [

                            {
                                'title': 'Expense',
                                'type': 'nav-item',
                                'icon': 'monetization_on',
                                'url': '/apps/expense'
                            },
                        ]
                    },

                    {
                        'title': 'View Doctors',
                        'icon': 'group',
                        'type': 'nav-item',
                        'url': 'apps/doctorviews'
                    },


                ]
            },

            {
                'title': 'Patient',
                'type': 'nav-collapse',
                'icon': 'airline_seat_individual_suite',
                'children': [
                    {
                        'title': 'Patients',
                        'type': 'nav-item',
                        'icon': 'airline_seat_individual_suite',
                        'url': '/apps/patients'
                    },
                    {
                        'title': 'Antenatal',
                        'type': 'nav-item',
                        'icon': 'supervisor_account',
                        'url': '/apps/antenatals'
                    },
                   {
                        'title': 'Follow up visits',
                        'type': 'nav-item',
                        'icon': 'supervisor_account',
                        'url': '/apps/followups'
                    } 
                ]
            },
            {
                'title': 'Message',
                'type': 'nav-collapse',
                'icon': 'add',
                'children': [
                    {
                        'title': 'Add Messgae',
                        'icon': 'email',
                        'type': 'nav-item',
                        'url': '/apps/mails/inbox'
                    },
                    /*   {
                          'title': 'Messgae Thread',
                          'icon': 'gesture',
                          'type': 'nav-item',
                          'url': '/apps/message_threads'
                      }, */
                ]
            },


            {
                'title': 'Noticeboard',
                'icon': 'add',
                'type': 'nav-collapse',
                'children': [
                    /*   {
                          'title': 'Notes',
                          'icon': 'assignment',
                          'type': 'nav-item',
                          'url': '/apps/notes'
                      }, */
                    {
                        'title': 'Notices',
                        'icon': 'announcement',
                        'type': 'nav-item',
                        'url': '/apps/notices'
                    },
                ]
            },

            {
                'title': 'Payment',
                'icon': 'add',
                'type': 'nav-collapse',
                'children': [
                    {

                    },
                    {
                        'title': 'Invoice',
                        'icon': 'assessment',
                        'type': 'nav-item',
                        'url': '/apps/invoices'
                    },

                    {
                        'title': 'Payroll',
                        'icon': 'attach_money',
                        'type': 'nav-item',
                        'url': '/apps/payrolls'
                    },

                ]
            },

            {
                'title': 'Account',
                'icon': 'person_pin',
                'type': 'nav-item',
                'url': '/apps/admins'
            },
            {
                'title': 'Manage Hospital',
                'icon': 'desktop_windows',
                'type': 'nav-collapse',
                'children': [
                    {
                        'title': 'Payment History',
                        'icon': 'account_balance',
                        'type': 'nav-item',
                        'url': '/apps/invoices'
                    },
                    {
                        'title': 'Bed',
                        'icon': 'airline_seat_individual_suite',
                        'type': 'nav-item',
                        'url': '/apps/beds'
                    },
                    {
                        'title': 'Bed Allotment',
                        'icon': 'local_hotel',
                        'type': 'nav-item',
                        'url': '/apps/bed_allotments'
                    },
                    {
                        'title': 'Blood Bank',
                        'icon': 'opacity',
                        'type': 'nav-item',
                        'url': '/apps/blood_banks'
                    },
                    {
                        'title': 'Blood Donor',
                        'icon': 'perm_contact_calendar',
                        'type': 'nav-item',
                        'url': '/apps/blood_donors'
                    },
                    {
                        'title': 'Blood Price',
                        'icon': 'attach_money',
                        'type': 'nav-item',
                        'url': '/apps/blood_prices'
                    },
                     {
                        'title': 'Blood Sale',
                        'icon': 'account_balance',
                        'type': 'nav-item',
                        'url': '/apps/blood_sales'
                    },
                    {
                        'title': 'Medicine',
                        'icon': 'add',
                        'type': 'nav-collapse',
                        'children': [
                            {
                                'title': 'Add Medicine',
                                'icon': 'add_circle_outline',
                                'type': 'nav-item',
                                'url': '/apps/medicines'
                            },
                            {
                                'title': 'Medicine Category',
                                'icon': 'library_add',
                                'type': 'nav-item',
                                'url': '/apps/medicine_categorys'
                            },
                            {
                                'title': 'Medicine Sale',
                                'icon': 'shopping_cart',
                                'type': 'nav-item',
                                'url': '/apps/medicine_sales'
                            },

                        ]
                    },
                    {
                        'title': 'Report',
                        'icon': 'description',
                        'type': 'nav-item',
                        'url': '/apps/reports'
                    },
                    {
                        'title': 'Admit History',
                        'icon': 'assignment',
                        'type': 'nav-item',
                        'url': '/apps/admithistory'
                    },
                    {
                        'title': 'Report History',
                        'icon': 'assignment',
                        'type': 'nav-item',
                        'url': '/apps/reporthistory'
                    },
                ]
            },


            {
                'title': 'Setting',
                'icon': 'settings',
                'type': 'nav-collapse',
                'children': [

                    {
                        'title': 'System Setting',
                        'icon': 'build',
                        'type': 'nav-item',
                        'url': '/apps/settings'
                    },
                    {
                        'title': 'Currency',
                        'icon': 'attach_money',
                        'type': 'nav-item',
                        'url': '/apps/currencys'
                    },
                     {
                        'title': 'Add Insurance Type',
                        'icon': 'account_balance',
                        'type': 'nav-item',
                        'url': '/apps/insurancetype'
                    },
                     {
                        'title': 'Insurance Schemes',
                        'icon': 'apps',
                        'type': 'nav-item',
                        'url': '/apps/insurescheme'
                    },
                   /*  {
                        'title': 'Sms Setting',
                        'icon': 'settings',
                        'type': 'nav-item',
                        'url': '/apps/smssettings'
                    },
                    {
                        'title': 'Send Sms',
                        'icon': 'message',
                        'type': 'nav-item',
                        'url': '/apps/smss'
                    },
                    {
                        'title': 'Email Setting',
                        'icon': 'email',
                        'type': 'nav-item',
                        'url': '/apps/email_templates'
                    },
  */               ]

            },
            {
                'title': 'Alert',
                'type': 'nav-item',
                'icon': 'notifications',
                'url': '/apps/alerts'
            },

        ];
    }
}
