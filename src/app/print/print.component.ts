import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FuseConfigService } from '../core/services/config.service';
import { Staff } from '../../app/main/content/apps/staffs/staff.model';
import { Configurerecord } from '../../app/main/content/apps/configure_records/configure_record.model';
import { DomSanitizer } from '@angular/platform-browser';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Record } from '../../app/main/content/apps/records/record.model';
import { Subject } from 'rxjs/Subject';
import { StaffsService } from '../../app/main/content/apps/staffs/staffs.service';
import { ConfigurerecordsService } from '../../app/main/content/apps/configure_records/configure_records.service';
import { RecordsService } from '../../app/main/content/apps/records/records.service';

@Component({
    selector: 'fuse-print',
    templateUrl: './print.component.html',
    styleUrls: ['./print.component.scss']
})
export class FusePrint implements OnInit {
    forgotPasswordForm: FormGroup;
    forgotPasswordFormErrors: any;
    public email;
    public answer;
    public secretQuestion;
    public data;
    public password;
    public error;

    constructor(
        private fuseConfig: FuseConfigService,
        private formBuilder: FormBuilder,
        public db: RecordsService,
        public db3: StaffsService,
        public db2: ConfigurerecordsService,
        public _DomSanitizer: DomSanitizer,
        public router: Router,
        public activateRoute: ActivatedRoute
    ) {

        /*  var userType = { usertype: "null" }
         localStorage.setItem('user', JSON.stringify(userType)); */

        this.fuseConfig.setSettings({
            layout: {
                navigation: 'none',
                toolbar: 'none',
                footer: 'none'
            }
        });

        this.forgotPasswordFormErrors = {
            email: {},
            answer: {}
        };
    }


  
 


    ngOnInit() {
       
    }

    onForgotPasswordFormValuesChanged() {
        /*   for ( const field in this.forgotPasswordFormErrors )
          {
              if ( !this.forgotPasswordFormErrors.hasOwnProperty(field) )
              {
                  continue;
              }
     
              // Clear previous errors
              this.forgotPasswordFormErrors[field] = {};
     
              // Get the control
              const control = this.forgotPasswordFormErrors.get(field);
     
              if ( control && control.dirty && !control.valid )
              {
                  this.forgotPasswordFormErrors[field] = control.errors;
              }
          } */
    }
}
