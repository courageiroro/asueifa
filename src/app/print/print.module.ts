import { NgModule } from '@angular/core';
import { SharedModule } from './../core/modules/shared.module';
import { RouterModule } from '@angular/router';
import { FusePrint } from './print.component';
import { StaffsService } from '../../app/main/content/apps/staffs/staffs.service';
import { ConfigurerecordsService } from '../../app/main/content/apps/configure_records/configure_records.service';
import { RecordsService } from '../../app/main/content/apps/records/records.service';

const routes = [
    {
        path     : 'pages/auth/forgot-password',
        component: FusePrint,
        resolve: {
            staffs: RecordsService
        }
    }
];

@NgModule({
    declarations: [
        FusePrint
    ],
    imports     : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    providers: [
        RecordsService,
        StaffsService,
        ConfigurerecordsService
    ]
})

export class PrintModule
{

}
