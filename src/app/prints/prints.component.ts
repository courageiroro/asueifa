import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Animations } from '../core/animations';
import { FormControl } from '@angular/forms';
import { Staff } from '../../app/main/content/apps/staffs/staff.model';
import { Configurerecord } from '../../app/main/content/apps/configure_records/configure_record.model';
import { DomSanitizer } from '@angular/platform-browser';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Record } from '../../app/main/content/apps/records/record.model';
import { Subject } from 'rxjs/Subject';
import { StaffsService } from '../../app/main/content/apps/staffs/staffs.service';
import { ConfigurerecordsService } from '../../app/main/content/apps/configure_records/configure_records.service';
import { RecordsService } from '../../app/main/content/apps/records/records.service';

@Component({
    selector: 'fuse-prints',
    templateUrl: './prints.component.html',
    styleUrls: ['./prints.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [Animations.slideInTop]
})
export class FusePrintsComponent implements OnInit {
  
    content;

    constructor(public db: RecordsService,
        public db3: StaffsService,
        public db2: ConfigurerecordsService,
        public _DomSanitizer: DomSanitizer,
        public router: Router,
        public activateRoute: ActivatedRoute) {
       
    }

    ngOnInit() {

      this.content =  this.db.content;
    }

}
