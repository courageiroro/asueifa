import { NgModule } from '@angular/core';
import { SharedModule } from './../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FusePrintsComponent } from './prints.component';
import { StaffsService } from '../../app/main/content/apps/staffs/staffs.service';
import { ConfigurerecordsService } from '../../app/main/content/apps/configure_records/configure_records.service';
import { RecordsService } from '../../app/main/content/apps/records/records.service';

const routes: Routes = [
    {
        path     : 'apps/prints',
        component: FusePrintsComponent,
        children : [],
        resolve  : {
            prints: RecordsService
        }
    }
];

@NgModule({
    imports        : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations   : [
       FusePrintsComponent
    ],
    providers      : [
        RecordsService,
        ConfigurerecordsService,
        StaffsService
       //UserService
    ],
    entryComponents: [FusePrintsComponent]
})
export class FusePrintsModule
{
}
