export interface Branch
{
    id: string,		
    name: string,		
    address: string,		
    manager: string,		
    phone: string,	
}

export interface Bus
{
    id: string,		
    bustype: string,		
    model: string,
    plateno: string,		
    bus_id: string,		
    seat: string,
    branch: string,		
    driver: string,		
}

export interface Bus_document
{
    id: string,		
    bus_id: string,		
    detail: string,		
    img: string,		
}

export interface Bustype
{
    id: string,		
    title: string,		
}

export interface Customer
{
    id: string,		
    name: string,
    address: string,
    phone: string,
    email: string,
    nkname: string,
    nkaddress: string,
    nkphone: string,
    nkemail: string,		
}

export interface Drivers_license
{
    id: string,		
    staff_id: string,		
    expirydate: string,		
    img: string,		
}

export interface Expcat
{
    id: string,		
    title: string,		
}

export interface Expense
{
    id: string,		
    expcat: string,		
    description: string,		
    date: string,		
    amount: string,		
}

export interface General_setting
{
    id: string,		
    sitename: string,		
    email: string,		
    mobile: string,		
    currency: string,		
    sms: string,	
}

export interface Inccat
{
    id: string,		
    title: string,		
}

export interface Income
{
    id: string,		
    inccat: string,		
    description: string,		
    date: string,		
    amount: string,		
}

export interface Route
{
    id: string,		
    routename: string,		
}

export interface Schedule
{
    id: string,
    bus: string,		
    route: string,
    date: string,		
    time: string,		
    seat: string,		
    amount: string,		
}

export interface Seat
{
    id: string,		
    bus: string,		
    seatname: string,		
    uid: string,		
    customer: string,				
    trxid: string,		
    status: string,		
    note: string,		
}

export interface Sms
{
    username: string,		
    apikey: string,		
    senderid: string,
}
		
export interface Staff
{
    id: string,
    rev: string,		
    stafftype: string,		
    fullname: string,		
    address: string,		
    mobile: string,		
    salary: string,
    email: string,
    password: string,
    bus: string,
    driverslicenses: Array<string>,		
}

export interface Stafftype
{
    id: string,		
    title: string,		
}