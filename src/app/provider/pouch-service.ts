/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http, Headers, RequestOptions } from '@angular/http';
import { RecordsService } from './../main/content/apps/records/records.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../app/core/fuseUtils';
import { Subject } from 'rxjs/Subject';
declare var require: any;
import PouchDB from 'pouchdb';
require('pouchdb-all-dbs')(PouchDB);
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));
import 'rxjs/add/operator/map';
import { Staff } from './../main/content/apps/staffs/staff.model';
import { antenatal } from './../main/content/apps/antenatal/antenatal.model';
import { Smssetting } from './../main/content/apps/smssettings/smssetting.model';
import { Sms } from './../main/content/apps/smss/sms.model';
import { Setting } from './../main/content/apps/settings/setting.model';
import { Report } from './../main/content/apps/reports/report.model';
import { Record } from './../main/content/apps/records/record.model';
import { Receptionist } from './../main/content/apps/receptionists/receptionist.model';
import { Receipt } from './../main/content/apps/receipts/receipt.model';
import { Prescription } from './../main/content/apps/prescriptions/prescription.model';
import { Pharmacist } from './../main/content/apps/pharmacists/pharmacist.model';
import { Payroll } from './../main/content/apps/payrolls/payroll.model';
import { Patient } from './../main/content/apps/patients/patient.model';
import { Offline } from './../main/content/apps/offlines/offline.model';
import { Nurse } from './../main/content/apps/nurses/nurse.model';
import { Notice } from './../main/content/apps/notices/notice.model';
import { Note } from './../main/content/apps/notes/note.model';
import { Message_thread } from './../main/content/apps/message_threads/message_thread.model';
import { Medicine } from './../main/content/apps/medicines/medicine.model';
import { AddMedicine } from './../main/content/apps/medicine_sales/medicine_sale-form/addmedicine.model';
import { Medicine_sale } from './../main/content/apps/medicine_sales/medicine_sale.model';
import { Medicine_category } from './../main/content/apps/medicine_categorys/medicine_category.model';
import { Managepayment } from './../main/content/apps/managepayments/managepayment.model';
import { Message } from './../main/content/apps/mail2/mail.model';
import { Language } from './../main/content/apps/languages/language.model';
import { labtest } from './../main/content/apps/labtest/labtest.model';
import { labrecord } from './../main/content/apps/labrecords/labrecord.model';
import { Laboratorist } from './../main/content/apps/laboratorists/laboratorist.model';
import { Calendar } from './../main/content/apps/lab_dashboards/lab_dashboard.model';
import { Invoice } from './../main/content/apps/invoices/invoice.model';
import { Insurancetype } from './../main/content/apps/insurancetypes/insurancetype.model';
import { Insurancesheme } from './../main/content/apps/insuranceshemes/insurancesheme.model';
import { Form_element } from './../main/content/apps/form_elements/form_element.model';
import { followup } from './../main/content/apps/followup/followup.model';
import { Extendlicense } from './../main/content/apps/extendlicenses/extendlicense.model';
import { Expense } from './../main/content/apps/expenses/expense.model';
import { Email_template } from './../main/content/apps/email_templates/email_template.model';
import { Payment } from './../main/content/apps/doctorviews/doctorview.model';
import { Diagnosis_report } from './../main/content/apps/diagnosis_reports/diagnosis_report.model';
import { Department } from './../main/content/apps/departments/department.model';
import { Currency } from './../main/content/apps/currencys/currency.model';
import { Consultancy } from './../main/content/apps/consultancys/consultancy.model';
import { Configurerecord } from './../main/content/apps/configure_records/configure_record.model';
import { Blood_sale } from './../main/content/apps/blood_sales/blood_sale.model';
import { Blood_price } from './../main/content/apps/blood_prices/blood_price.model';
import { Blood_donor } from './../main/content/apps/blood_donors/blood_donor.model';
import { Blood_bank } from './../main/content/apps/blood_banks/blood_bank.model';
import { Bed } from './../main/content/apps/beds/bed.model';
import { Bed_allotment } from './../main/content/apps/bed_allotments/bed_allotment.model';
import { anterecord } from './../main/content/apps/anterecords/anterecord.model';
import { Configureanterecord } from './../main/content/apps/ante_records/ante_record.model';
import { alerts } from './../main/content/apps/alerts/alerts.model';
import { Accountant } from './../main/content/apps/accountants/accountant.model';
import { Account } from './../main/content/apps/accounts/account.model';
import { Appointment } from './../main/content/apps/addappointments/addappointment.model';
import { Admin } from './../main/content/apps/admins/admin.model';
import { Schema } from './../main/content/apps/schema';
import { Drivers_license } from './models';

export interface sendNumber {
    number: string;
}

export interface myData {
    name: string;
}

@Injectable()
export class PouchService {

    sharingData: myData = { name: "nyks" };
    saveData(name) {

        this.sharingData.name = name;

    }
    getData() {

        return this.sharingData.name;

    }

    transferredData: sendNumber = { number: "number" };
    saveNumber(num) {
        this.transferredData.number = num;

    }

    getNumber() {

        return this.transferredData.number;
    }

    public name = "";
    public retrieve;
    public file;
    reader;
    public file2;
    public records;
    public patientname = "";
    public patientnumber = "";
    public staffId: any;
    public gattachment;
    public hide = "";
    public sCredentials;
    public base64data: any;
    onStaffsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedStaffsChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    
    /** Stream that emits whenever the data has been modified. */
    dataChange: BehaviorSubject<Staff[]> = new BehaviorSubject<Staff[]>([]);
    get data(): Staff[] { return this.dataChange.value; }

    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject();

    public plateno = "";

    staffs: Staff[];
    user: any;
    selectedStaffs: string[] = [];

    searchText: string;
    filterBy: string;

    private db;
    remote: any;

    //Account

    onAccountantsChanged: BehaviorSubject<any> = new BehaviorSubject({});


    onSelectedAccountantsChanged: BehaviorSubject<any> = new BehaviorSubject([]);


    /*  onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
 
     onSearchTextChanged: Subject<any> = new Subject();
 
     onFilterChanged: Subject<any> = new Subject(); */

    accountants: Accountant[];
    //user: any;
    selectedAccountants: string[] = [];

    //Account

    onAccountsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedAccountsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /*  onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
 
     onSearchTextChanged: Subject<any> = new Subject();
 
     onFilterChanged: Subject<any> = new Subject(); */

    accounts: Account[];
    //user: any;
    selectedAccounts: string[] = [];

    /*  searchText: string;
     filterBy: string; */

    //ADD APPOINTMNETS
    public addappointmentname = "";
    addappointmentss: any;
    onAddappointmentsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedAddappointmentsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /* onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject(); */

    addappointments: Appointment[];
    //user: any;
    selectedAddappointments: string[] = [];
    onEventsUpdated = new Subject<any>();
    /* searchText: string;
    filterBy: string;
    public sCredentials; */

    //Admin
    public admin = "";
    /* public sCredentials; */

    onAdminsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedAdminsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /*  onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
 
     onSearchTextChanged: Subject<any> = new Subject();
 
     onFilterChanged: Subject<any> = new Subject(); */

    admins: Admin[];
    //salarys: Payment[];
    //user: any;
    selectedAdmins: string[] = [];

    checkboxes: {};

    /*  searchText: string;
     filterBy: string; */

    //ALERTS
    public alerts = "";
    public category;
    onalertssChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedalertssChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /* onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject(); */

    alertss: alerts[];
    //user: any;
    selectedalertss: string[] = [];

    /*  searchText: string;
     filterBy: string; */

    //ANTE_RECORDS
    public configureanterecord = "";
    onConfigureanterecordsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedConfigureanterecordsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /*    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
   
       onSearchTextChanged: Subject<any> = new Subject();
   
       onFilterChanged: Subject<any> = new Subject();
    */
    configureanterecords: Configureanterecord[];
    //user: any;
    selectedConfigureanterecords: string[] = [];

    //ANTERECORDS
    public anterecord = "";
    public content: any;
    onanterecordsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedanterecordsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /*  onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
 
     onSearchTextChanged: Subject<any> = new Subject();
 
     onFilterChanged: Subject<any> = new Subject(); */

    anterecords: anterecord[];
    /* staffs: Staff[];
    user: any; */
    selectedanterecords: string[] = [];

    //APPOINTMENT

    onAppointmentsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedAppointmentsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /* onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject(); */

    public inccat = "";

    appointments: Appointment[];
    //user: any;
    selectedAppointments: string[] = [];

    //APPOINTMENTS
    public allotmentName = "";

    onBed_allotmentsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedBed_allotmentsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /* onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject(); */

    bed_allotments: Bed_allotment[];
    //user: any;
    selectedBed_allotments: string[] = [];

    //BEDS
    public bed = "";

    onBedsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedBedsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /*   onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
  
      onSearchTextChanged: Subject<any> = new Subject();
  
      onFilterChanged: Subject<any> = new Subject(); */

    beds: Bed[];
    //user: any;
    selectedBeds: string[] = [];

    //BLOOD_BANK
    public blood_bank = "";

    onBlood_banksChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedBlood_banksChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /*   onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
  
      onSearchTextChanged: Subject<any> = new Subject();
  
      onFilterChanged: Subject<any> = new Subject(); */

    blood_banks: Blood_bank[];
    //user: any;
    selectedBlood_banks: string[] = [];
    public bloodBank = "";

    //BLOOD_DONOR
    onBlood_donorsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedBlood_donorsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /* onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject(); */

    public staff = "";
    public bankId = "";

    blood_donors: Blood_donor[];
    //user: any;
    selectedBlood_donors: string[] = [];

    //BLOODPRICE
    public blood_price = "";

    onBlood_pricesChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedBlood_pricesChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /*  onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
 
     onSearchTextChanged: Subject<any> = new Subject();
 
     onFilterChanged: Subject<any> = new Subject(); */

    blood_prices: Blood_price[];
    //user: any;
    selectedBlood_prices: string[] = [];
    public bloodPrice = "";

    //BLOODSALES
    onBlood_salesChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedBlood_salesChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /* onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject(); */

    //public staff = "";
    //public bankId = "";


    blood_sales: Blood_sale[];
    //user: any;
    selectedBlood_sales: string[] = [];

    //CONFIGURERECORDS
    public configurerecord = "";
    //public category;
    onConfigurerecordsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedConfigurerecordsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /*   onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
  
      onSearchTextChanged: Subject<any> = new Subject();
  
      onFilterChanged: Subject<any> = new Subject(); */

    configurerecords: Configurerecord[];
    //user: any;
    selectedConfigurerecords: string[] = [];

    //CONSULTANCY
    public consultancy = "";

    patientId;
    onConsultancysChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedConsultancysChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /* onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject(); */

    consultancys: Consultancy[];
    //user: any;
    selectedConsultancys: string[] = [];

    //CURRENCY 
    onCurrencysChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedCurrencysChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /* onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject(); */

    currencys: Currency[];
    //user: any;
    selectedCurrencys: string[] = [];

    //DEPARTMENTS
    public department = "";

    onDepartmentsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedDepartmentsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /* onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject(); */

    departments: Department[];
    //user: any;
    selectedDepartments: string[] = [];

    //DIAGNOSIS_REPORTS
    public diagnosis_report = "";
    //public hide = "";
    onDiagnosis_reportsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedDiagnosis_reportsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /*  onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
 
     onSearchTextChanged: Subject<any> = new Subject();
 
     onFilterChanged: Subject<any> = new Subject(); */

    diagnosis_reports: Diagnosis_report[];
    //user: any;
    selectedDiagnosis_reports: string[] = [];

    //DOCTORSVIEW
    public paymentname = "";
    onPaymentsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedPaymentsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /*  onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
 
     onSearchTextChanged: Subject<any> = new Subject();
 
     onFilterChanged: Subject<any> = new Subject(); */

    payments: Payment[];
    //user: any;
    selectedPayments: string[] = [];

    //EMAIL_TEMPLATES
    public email_templatename = "";

    onEmail_templatesChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedEmail_templatesChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /*  onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
 
     onSearchTextChanged: Subject<any> = new Subject();
 
     onFilterChanged: Subject<any> = new Subject(); */

    email_templates: Email_template[];
    //user: any;
    selectedEmail_templates: string[] = [];

    //EXPENSES
    onExpensesChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedExpensesChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /* onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject(); */

    expenses: Expense[];
    //user: any;
    selectedExpenses: string[] = [];

    //EXTENDLICENSE
    public extendlicense = "";

    onExtendlicensesChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedExtendlicensesChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /*  onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
 
     onSearchTextChanged: Subject<any> = new Subject();
 
     onFilterChanged: Subject<any> = new Subject(); */

    extendlicenses: Extendlicense[];
    //user: any;
    selectedExtendlicenses: string[] = [];

    //FOLLOWUPS
    public followup = "";


    onfollowupsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedfollowupsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /*  onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
 
     onSearchTextChanged: Subject<any> = new Subject();
 
     onFilterChanged: Subject<any> = new Subject();
  */
    followups: followup[];
    //user: any;
    selectedfollowups: string[] = [];

    //FORM_ELEMENTS
    onForm_elementsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedForm_elementsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /*  onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
 
     onSearchTextChanged: Subject<any> = new Subject();
 
     onFilterChanged: Subject<any> = new Subject();
  */
    form_elements: Form_element[];
    //user: any;
    selectedForm_elements: string[] = [];

    //INSURANCESHEME
    public insurancesheme = "";

    onInsuranceshemesChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedInsuranceshemesChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /*  onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
 
     onSearchTextChanged: Subject<any> = new Subject();
 
     onFilterChanged: Subject<any> = new Subject(); */

    insuranceshemes: Insurancesheme[];
    //user: any;
    selectedInsuranceshemes: string[] = [];

    //INSURANCETYPES
    public insurancetype = "";

    onInsurancetypesChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedInsurancetypesChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /* onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject(); */

    insurancetypes: Insurancetype[];
    //user: any;
    selectedInsurancetypes: string[] = [];

    //INVOICES
    public invoicename = "";
    //public hide = "";

    onInvoicesChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedInvoicesChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /* onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject(); */

    invoices: Invoice[];
    //user: any;
    selectedInvoices: string[] = [];

    //LABDASHBOARD
    public lab_dashboardname = "";
    lab_dashboardss: any;
    onLab_dashboardsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedLab_dashboardsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /* onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject(); */

    lab_dashboards: Calendar[];
    //user: any;
    selectedLab_dashboards: string[] = [];
    //onEventsUpdated = new Subject<any>();
    //searchText: string;
    //filterBy: string;

    //LABORATORIST
    //public name = "";

    onLaboratoristsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedLaboratoristsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /*  onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
 
     onSearchTextChanged: Subject<any> = new Subject();
 
     onFilterChanged: Subject<any> = new Subject(); */

    laboratorists: Laboratorist[];
    //user: any;
    selectedLaboratorists: string[] = [];

    //LABRECORDS
    public labrecord = "";
    /* public sCredentials;
    public category;
    public file;
    public retrieve;
    public content: any; */
    onlabrecordsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedlabrecordsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /* onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject(); */

    labrecords: labrecord[];
    /* staffs: Staff[];
    user: any; */
    selectedlabrecords: string[] = [];

    //LABTEST
    public labtest = "";

    //public category;
    onlabtestsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedlabtestsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /* onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject(); */

    labtests: labtest[];
    //user: any;
    selectedlabtests: string[] = [];

    //LANGUAGE
    public languagename = "";

    onLanguagesChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedLanguagesChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /* onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject(); */

    languages: Language[];
    //user: any;
    selectedLanguages: string[] = [];

    //MAILS

    public fileName;
    mails: Message[];
    selectedMails: Message[];
    selectedMails2: string[] = [];
    currentMail: Message;
    newMail: Message[];
    //searchText = '';
    public showImage = 0;
    public imageId: any;
    folderId;
    folderId2: any;
    folders: any[];
    filters: any[];
    labels: any[];
    public routerSnapShot: string;
    public message: any;
    routeParams: any;
    //filterBy: string;
    onMailsChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    onSelectedMailsChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    onCurrentMailChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    //onFilterChanged: Subject<any> = new Subject();
    onFoldersChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    onFiltersChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    onLabelsChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    //onEventsUpdated = new Subject<any>();
    public localStorageEmail: string;
    public localStorageItem: any;
    public localStorageType: any;
    public localStorageName: any;
    public localStorageId: any;
    emailTo;

    //onSearchTextChanged: BehaviorSubject<any> = new BehaviorSubject('');

    //MANAGEPAYMENTS
    public managepayment = "";

    onManagepaymentsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedManagepaymentsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /* onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    
    onSearchTextChanged: Subject<any> = new Subject();
    
    onFilterChanged: Subject<any> = new Subject(); */

    managepayments: Managepayment[];
    //user: any;
    selectedManagepayments: string[] = [];

    //MEDICINECATEGORY
    public medicine_categoryname = "";

    onMedicine_categorysChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedMedicine_categorysChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /* onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    
    onSearchTextChanged: Subject<any> = new Subject();
    
    onFilterChanged: Subject<any> = new Subject(); */

    medicine_categorys: Medicine_category[];
    //user: any;
    selectedMedicine_categorys: string[] = [];

    //MEDICINESALES
    public medicine_salename = "";

    public ids: any;
    public addMedicine: AddMedicine

    onMedicine_salesChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedMedicine_salesChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /* onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    
    onSearchTextChanged: Subject<any> = new Subject();
    
    onFilterChanged: Subject<any> = new Subject(); */

    medicine_sales: Medicine_sale[];
    //user: any;
    selectedMedicine_sales: string[] = [];


    //MEDICINES
    public medicinename = "";
    public medicineId = "";

    onMedicinesChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedMedicinesChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /* onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    
    onSearchTextChanged: Subject<any> = new Subject();
    
    onFilterChanged: Subject<any> = new Subject(); */

    medicines: Medicine[];
    //user: any;
    selectedMedicines: string[] = [];

    //MESSAGETHREAD
    public message_threadname = "";

    onMessage_threadsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedMessage_threadsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /* onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    
    onSearchTextChanged: Subject<any> = new Subject();
    
    onFilterChanged: Subject<any> = new Subject(); */

    message_threads: Message_thread[];
    //user: any;
    selectedMessage_threads: string[] = [];

    //MESSAGES
    public messagename = "";

    /* folders: any[];
    filters: any[];
    labels: any[];
    routeParams: any; */
    folderHandle;
    /* folderId;
    folderId2; */
    onMessagesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    //onEventsUpdated = new Subject<any>();
    onCurrentMessageChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    onSelectedMessagesChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    /* onFoldersChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    onFiltersChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    onLabelsChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    
    onSearchTextChanged: Subject<any> = new Subject();
    
    onFilterChanged: Subject<any> = new Subject(); */
    currentMessage: Message;
    selectedMessages2: Message[];
    messages: Message[];
    //user: any;
    selectedMessages: string[] = [];

    //NOTES
    public notename = "";


    onNotesChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedNotesChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /*  onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
 
     onSearchTextChanged: Subject<any> = new Subject();
 
     onFilterChanged: Subject<any> = new Subject(); */

    notes: Note[];
    //user: any;
    selectedNotes: string[] = [];

    //NOTICES
    public noticename = "";
    noticess: any;
    onNoticesChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedNoticesChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /*  onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
 
     onSearchTextChanged: Subject<any> = new Subject();
 
     onFilterChanged: Subject<any> = new Subject(); */

    notices: Notice[];
    //user: any;
    selectedNotices: string[] = [];
    //onEventsUpdated = new Subject<any>();

    //NURSES
    //public name = "";
    onNursesChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedNursesChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /* onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject(); */

    nurses: Nurse[];
    //user: any;
    selectedNurses: string[] = [];

    //OFFLINE
    public offline = "";

    onOfflinesChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedOfflinesChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /*  onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
 
     onSearchTextChanged: Subject<any> = new Subject();
 
     onFilterChanged: Subject<any> = new Subject(); */

    offlines: Offline[];
    //user: any;
    selectedOfflines: string[] = [];

    //PATIENT
    /*  public patientname = "";
     public patientnumber = ""; */

    onPatientsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedPatientsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /* onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject(); */

    patients: Patient[];
    //user: any;
    selectedPatients: string[] = [];

    //PAYROLL
    public payrollname = "";

    public pid = "";
    onPayrollsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedPayrollsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /*   onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
  
      onSearchTextChanged: Subject<any> = new Subject();
  
      onFilterChanged: Subject<any> = new Subject(); */

    payrolls: Payroll[];
    //user: any;
    selectedPayrolls: string[] = [];

    //PHARMACIST
    //public name = "";

    onPharmacistsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedPharmacistsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /* onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject(); */

    pharmacists: Pharmacist[];
    //user: any;
    selectedPharmacists: string[] = [];

    //PRESCRIPTION
    public prescriptionname = "";

    onPrescriptionsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedPrescriptionsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /* onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onSearchTextChanged: Subject<any> = new Subject();

    onFilterChanged: Subject<any> = new Subject(); */

    prescriptions: Prescription[];
    //user: any;
    selectedPrescriptions: string[] = [];

    //RECEIPT
    public receipt = "";

    onReceiptsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedReceiptsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /*  onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
 
     onSearchTextChanged: Subject<any> = new Subject();
 
     onFilterChanged: Subject<any> = new Subject(); */

    receipts: Receipt[];
    //user: any;
    selectedReceipts: string[] = [];

    //RECEPTIONIST
    //public name = "";
    onReceptionistsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedReceptionistsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /*  onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
 
     onSearchTextChanged: Subject<any> = new Subject();
 
     onFilterChanged: Subject<any> = new Subject(); */

    receptionists: Receptionist[];
    //user: any;
    selectedReceptionists: string[] = [];
    //RECORDS
    public record = "";
    /* public sCredentials;
    public category;
    public file;
    public retrieve;
    public content: any; */
    onRecordsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedRecordsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /* onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    
    onSearchTextChanged: Subject<any> = new Subject();
    
    onFilterChanged: Subject<any> = new Subject(); */

    records2: Record[];

    //user: any;
    selectedRecords: string[] = [];

    //REPORT_HISTORY
    public reportname = "";
    /* public name = "";
    public retrieve;
    public file;
    public sCredentials; */

    onReportsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedReportsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /* onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    
    onSearchTextChanged: Subject<any> = new Subject();
    
    onFilterChanged: Subject<any> = new Subject(); */

    reports: Report[];
    //user: any;
    selectedReports: string[] = [];

    //SETTING
    onSettingsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedSettingsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /* onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    
    onSearchTextChanged: Subject<any> = new Subject();
    
    onFilterChanged: Subject<any> = new Subject(); */

    settings: Setting[];
    //user: any;
    selectedSettings: string[] = [];
    public id;

    //SMSS
    public smsname = "";

    onSmssChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedSmssChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /*  onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
 
     onSearchTextChanged: Subject<any> = new Subject();
 
     onFilterChanged: Subject<any> = new Subject(); */

    smss: Sms[];
    //user: any;
    selectedSmss: string[] = [];

    //SMSSETTING
    onSmssettingsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedSmssettingsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    /*   onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
  
      onSearchTextChanged: Subject<any> = new Subject();
  
      onFilterChanged: Subject<any> = new Subject(); */

    smssettings: Smssetting[];
    //user: any;
    selectedSmssettings: string[] = [];

    //ANTENATAL
    public antenatalname = "";
    public antenatalnumber = "";

    onantenatalsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onSelectedantenatalsChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    antenatals: antenatal[];
    //user: any;
    selectedantenatals: string[] = [];

    /**
     * Constructor
     */
    constructor(private http: Http) {
        this.initDB(this.sCredentials);
        this.selectedMails = [];
        this.routerSnapShot = "inbox";
    }

    /**
     * The staffs App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        console.log("resolved");
        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getStaffs(),
                this.getAccountants(),
                this.getAccounts(),
                this.getAddappointments(),
                this.getAdmins(),
                this.getLab_dashboards(),
                this.getalertss(),
                this.getConfigureanterecords(),
                this.getanterecords(),
                this.getAppointments(),
                this.getBed_allotments(),
                this.getFilters(),
                this.getLabels(),
                this.getMailsByFolder(),
                this.getMails(),
                this.getManagepayments(),
                this.getInvoices(),
                this.getMedicine_categorys(),
                this.getMedicine_sales(),
                this.getMedicines(),
                this.getMessages(),
                this.getNotes(),
                this.getNotices(),
                this.getNurses(),
                this.getOfflines(),
                this.getPatients(),
                this.getPayments(),
                this.getPayrolls(),
                this.getPrescriptions(),
                this.getReceipts(),
                this.getReceptionists(),
                this.getRecords(),
                this.getReports(),
                this.getSettings(),
                this.getSmss(),
                this.getantenatals(),
                this.getReceipts(),
                this.getDepartments()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getMails();
                        this.getStaffs();
                        this.getAccountants();
                        this.getAccounts();
                        this.getAddappointments();
                        this.getAdmins();
                        this.getalertss();
                        this.getConfigureanterecords();
                        this.getanterecords();
                        this.getAppointments();
                        this.getBed_allotments();
                        this.getBeds();
                        this.getBlood_banks();
                        this.getBlood_donors();
                        this.getBlood_prices();
                        this.getBlood_sales();
                        this.getConfigureanterecords();
                        this.getConsultancys();
                        this.getCurrencys();
                        this.getDepartments();
                        this.getDiagnosis_reports();
                        this.getPayments();
                        this.getEmail_templates();
                        this.getExpenses();
                        this.getExtendlicenses();
                        this.getfollowups();
                        this.getForm_elements();
                        this.getInsuranceshemes();
                        this.getInsurancetypes();
                        this.getInvoices();
                        this.getLab_dashboards();
                        this.getLaboratorists();
                        this.getlabrecords();
                        this.getlabtests();
                        this.getLanguages();
                        this.getManagepayments();
                        this.getMedicine_categorys();
                        this.getMedicine_sales();
                        this.getMedicines();
                        this.getMessages();
                        this.getNotes();
                        this.getNotices();
                        this.getNurses();
                        this.getOfflines();
                        this.getPatients();
                        this.getPayments();
                        this.getPayrolls();
                        this.getPrescriptions();
                        this.getReceipts();
                        this.getReceptionists();
                        this.getRecords();
                        this.getReports();
                        this.getSettings();
                        this.getSmss();
                        this.getantenatals();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getMails();
                        this.getStaffs();
                        this.getAccountants();
                        this.getAccounts();
                        this.getAddappointments();
                        this.getAdmins();
                        this.getalertss();
                        this.getConfigureanterecords();
                        this.getanterecords();
                        this.getAppointments();
                        this.getBed_allotments();
                        this.getBeds();
                        this.getBlood_banks();
                        this.getBlood_donors();
                        this.getBlood_prices();
                        this.getBlood_sales();
                        this.getConfigureanterecords();
                        this.getConsultancys();
                        this.getCurrencys();
                        this.getDepartments();
                        this.getDiagnosis_reports();
                        this.getPayments();
                        this.getEmail_templates();
                        this.getExpenses();
                        this.getExtendlicenses();
                        this.getfollowups();
                        this.getForm_elements();
                        this.getInsuranceshemes();
                        this.getInsurancetypes();
                        this.getInvoices();
                        this.getLab_dashboards();
                        this.getLaboratorists();
                        this.getlabrecords();
                        this.getlabtests();
                        this.getLanguages();
                        this.getManagepayments();
                        this.getMedicine_categorys();
                        this.getMedicine_sales();
                        this.getMedicines();
                        this.getMessages();
                        this.getNotes();
                        this.getNotices();
                        this.getNurses();
                        this.getOfflines();
                        this.getPatients();
                        this.getPayments();
                        this.getPayrolls();
                        this.getPrescriptions();
                        this.getReceipts();
                        this.getReceptionists();
                        this.getRecords();
                        this.getReports();
                        this.getSettings();
                        this.getSmss();
                        this.getantenatals();
                    });

                    if (this.routeParams.mailId) {
                        this.setCurrentMail(this.routeParams.mailId);
                    }
                    else {
                        this.setCurrentMail(null);
                    }

                    this.onSearchTextChanged.subscribe(searchText => {
                        if (searchText !== '') {
                            this.searchText = searchText;
                            this.getMails();
                        }
                        else {
                            this.searchText = searchText;
                            this.getMails();
                        }
                    });

                    resolve();

                },
                reject
            );
        });
    }

    initDB(credentials) {

        credentials = JSON.parse(localStorage.getItem('credentials'))

        this.sCredentials = credentials;
        this.db = new PouchDB('asueifaibayelsa', { adaptater: 'websql' });
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.db.setSchema(Schema);

        //if (credentials.online) {
        this.enableSyncing(credentials);
        console.log(this.enableSyncing(credentials));
        //}

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }
    databaseExist(db): Promise<boolean> {
        return PouchDB.allDbs().then(dbs => {
            return dbs.indexOf(db) != -1;
        });
    }

    enableSyncing(credentials) {
        let options = {
            Auth: {
                username: 'admin',
                password: 'realpeople'
            },
            live: true,
            retry: true,
            continuous: false
        };
        //if (credentials.userDBs != undefined) {
        this.remote = 'http://157.230.219.86:5984/asueifaibayelsa';
        if (this.remote != undefined) {
            this.db.sync(this.remote, options).on('change', function (change) {
                console.log('changed');
            }).on('complete', function (complete) {
                console.log('complete');
            }).on('error', function (err) {
                console.log('offline');
            });;
        }

    }

    loadRemoteDB(credentials) {
        //this.remote = 'http://sarutech.com:5984/hospital_demo';
        let options = {
            Auth: {
                username: 'admin',
                password: 'realpeople'
            },
            /*    live: true,
               retry: true,
               continuous: true */
        };
        //if (credentials.userDBs != undefined) {

        this.remote = 'http://157.230.219.86:5984/asueifaibayelsa';
        return this.db.sync(this.remote, options).on('change', function (change) {

            //return true;
        }).on('complete', function (complete) {
            return true;
        }).on('error', function (err) {
            console.log('offline');
        });
        //}

    }


    validateUsername(username) {
        return this.http.get('http://157.230.219.86:5984/couchdblogin/' + 'auth/validate-username/' + username).map(res => res.json());
    }

    addLocalStorage() {
        var userType = { "usertype": null }
        return localStorage.setItem('user', JSON.stringify(userType));
    }

    /***********
   * SETTING
   **********/
    /**
     * Save a setting
     * @param {setting} Setting
     *
     * @return Promise<Setting>
     */
    saveSetting(setting: Setting): Promise<Setting> {
        setting.id = Math.floor(Date.now()).toString();
        this.id = setting.id
        setting.email_templates = [];
        return this.db.rel.save('settings', setting)
            .then((data: any) => {

                if (data && data.settings && data.settings
                [0]) {

                    return data.settings[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a setting
    * @param {setting} Setting
    *
    * @return Promise<setting>
    */
    updateSetting(setting: Setting) {

        return this.db.rel.save('settings', setting)
            .then((data: any) => {
                if (data && data.settings && data.settings
                [0]) {
                    return data.settings[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    extendLicense(date, plan) {

        this.getSettings().then(settings => {
            settings[0].expiredate = new Date(date).toUTCString();
            settings[0].plans.push(plan);

            this.updateSetting(settings[0]);
        });
    }

    /**
     * Remove a setting
     * @param {setting} Setting
     *
     * @return Promise<boolean>
     */
    removeSetting(setting: Setting): Promise<boolean> {
        return this.db.rel.del('settings', setting)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the settings
     *
     * @return Promise<Array<setting>>
     */
    getSettings(): Promise<Array<Setting>> {
        return this.db.rel.find('settings')
            .then((data: any) => {
                this.settings = data.settings;
                if (this.searchText && this.searchText !== '') {
                    this.settings = FuseUtils.filterArrayByString(this.settings, this.searchText);
                }
                //this.onSettingsChanged.next(this.settings);
                return Promise.resolve(this.settings);
                //return data.settings ? data.settings : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a setting
     * @param {setting} setting
     *
     * @return Promise<setting>
     */
    getSetting(setting: Setting): Promise<Setting> {
        this.id = setting.id
        return this.db.rel.find('settings', setting.id)
            .then((data: any) => {
                return data && data.settings ? data.settings[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected setting by id
     * @param id
     */
    toggleSelectedSetting(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedSettings.length > 0) {
            const index = this.selectedSettings.indexOf(id);

            if (index !== -1) {
                this.selectedSettings.splice(index, 1);

                // Trigger the next event
                this.onSelectedSettingsChanged.next(this.selectedSettings);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedSettings.push(id);

        // Trigger the next event
        this.onSelectedSettingsChanged.next(this.selectedSettings);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllSetting() {
        if (this.selectedSettings.length > 0) {
            this.deselectSettings();
        }
        else {
            this.selectSettings();
        }
    }

    selectSettings(filterParameter?, filterValue?) {
        this.selectedSettings = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedSettings = [];
            this.settings.map(setting => {
                this.selectedSettings.push(setting.id);
            });
        }
        else {
            /* this.selectedsettings.push(...
                 this.settings.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedSettingsChanged.next(this.selectedSettings);
    }





    deselectSettings() {
        this.selectedSettings = [];

        // Trigger the next event
        this.onSelectedSettingsChanged.next(this.selectedSettings);
    }

    deleteSetting(setting) {
        this.db.rel.del('settings', setting)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const settingIndex = this.settings.indexOf(setting);
                this.settings.splice(settingIndex, 1);
                this.onSettingsChanged.next(this.settings);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedSettings() {
        for (const settingId of this.selectedSettings) {
            const setting = this.settings.find(_setting => {
                return _setting.id === settingId;
            });

            this.db.rel.del('settings', setting)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const settingIndex = this.settings.indexOf(setting);
                    this.settings.splice(settingIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onSettingsChanged.next(this.settings);
        this.deselectSettings();
    }


    /***********
     *staff
     **********/
    /**
     * Save a staff
     * @param {staff} Staff
     *
     * @return Promise<Staff>
     */

    saveStaff(staff: Staff): Promise<Staff> {

        let record = {
            id: Math.round((new Date()).getTime()).toString(),
            rev: '',
            customer: staff.id
        }

        staff.id = Math.floor(Date.now()).toString();
        console.log(staff._attachments);
        staff.prescriptions = [];
        staff.reports = [];
        var filess;
        filess

        this.file;

        this.retrieve = (<HTMLInputElement>this.file).files[0];

        var file = this.retrieve;

        var file2;

        // if (staff._attachments === "null" || staff._attachments === " ") {

        /*  staff.image = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wAARCADIAMgDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9U6KKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAorivi58W9A+C/g+48Q+IJykKHZDbx8y3Eh6Ig9ffoBXwH43/AOCiPxD1zUJjoEFhoGnkkRx+V50oHYljxn8KAP0xpCwXqQPqa/ILWf2tfi3ru4XHjbUIkP8ADbBIcfiqg/rXC6x8R/FniHd/afifWNQDdRc38sg/ItQB+zus+N/Dnh0E6rr+l6YB1N5eRw4/76YVwms/tU/CXQd32nx5pEhXtaSm5/8ARYavx6JJJJOSe9FAH6laz/wUA+Eml7hBfalqZHT7JZEA/wDfZWuMn/4KWeDlvoooPC+rTW7OFaZpI1KjPXbzn6Zr86K97/Yy+C//AAtz4uWct7B5ug6KVvbzcMrIQf3cZ+rDkegPrQB+rdrcJeW0M8edkqB13DBwRkZHavAPj5+2PofwF8Y23h290G+1W5kt1uHlgkWNEViQMZB3Hg19CV8n/wDBQL4L/wDCa/DyPxjp0G/VfD4LT7B8z2pPzH/gJ+b6Z9KAJ9G/4KMfDO/2rfWetac57m3WRR+IbP6V3mjftn/B7WtoXxjBZyH+C8t5YsfiVx+tfkjRQB+1Oi/GbwF4i2jTfGmgXjN0SLUoS/8A3zuz+ldbDcw3EavFKkqNyGRgQfxr8Kav6Xr+qaI+/TtSu7B+u61naM/+OkUAfuZRX41aN+0Z8TtA2/Y/HOtgL0E920w/8fzXeaH+3Z8XtHZDLrtvqYU8i9tEO7/vnFAH6s0V8z/sy/tnaX8bL6Pw7rVrHovikqWiRGzDd4GTsJ5DYydv5V9MUAFFFFABRRRQAUUUUAFNd1jRnYhVUZJPQCnV8p/t5fHz/hXvgc+D9Iudmva7GUnaNvmgtTwx9i33fpmgD5M/bE+PR+NHxJlg0+cv4a0dmt7IKfllbOHl/Ejj2ArwOiigAooooAKKKKAFVS7BVBLE4AHev1k/Zg+F1n+z78DkuNZ2WN/NA2qavNLx5QCbtrf7ijn3zXxR+w78F/8AhZ/xYg1a/g8zQ/D7LdzbhlZJgcxJ78jcR7e9fRH/AAUO+NH/AAjnhC18B6dPtv8AWMTX2w8pbqeFP+8w/IUAe6fs8fE6f4veALjxPKpjiutRuVtoj1jhVgEB98cn3JrM/Z9+LFr8ZvCniDS9S8u41HSr650y+gcA+bFvZUYjuCnB+hrl/wBgj/k3HTP+vu4/9CFfHPwN+M5+DP7TerXd3MY9C1LVbiy1EE/KqNM22Q/7hwfpmgDg/wBov4RT/Bb4qatoBRv7OL/aLCVv44GOVGe5H3T9K8yr9PP28PgyPiP8LT4m02ETax4eVrnMYy0tr1kHvgfN9Aa/MOgAooooAKKKKAL2ha5e+GtasdW024e1v7KZLiCaM4KOpyD+Yr9hvgD8YbH43fDbTvENsUS82+TfWyn/AFM4HzDHoeo9jX41179+xx8ej8GPiVDbajcGPwzrLLbXu4/LCxOEm/Ann2J9KAP1dopFZXUMpDKRkEHIIpaACiiigAooooA574geONL+G/g7VfEmsTCGw0+BpX55c/wovqzHAHua/G/4ofEXU/it451XxNqzk3N7KWWPOVijHCIvsBgV9Of8FC/jdca/4vT4fWLvHp2klZr0dPNnZcqD7Kp/M18c0AFFFFABRRRQAU+CGS5mjhiRpJZGCIijJYk4AAplfVP7CHwGufHXxDg8YapYv/wj2huJoXlTCT3Q+4BnrtPzH3AoA+xvgJ8PtO/Zq+A6vrDJaTw2zanrE57Pt3FffaPlA7np1r8wfi78SL74tfEPWfE98WDXsxMMROfKiHCIPoMfjmvtL/gor8af7M0Wy+Hmmz4uL7bd6jsPIiB+RD9SM/gK/PygD9T/ANgj/k3HTP8Ar7uP/QhX5ofEP/koHib/ALCd1/6Nav0v/YI/5Nx0z/r7uP8A0IV+aHxD/wCR/wDE3/YTuf8A0a1AH6U/sQ/GOP4rfCRdD1OVbjV9CQWU6SHJlgxiNiO/Hyn6V8JftRfByT4L/FnU9KhiZNHumN3pzkceSx+5n/ZOR+AqP9mL4xSfBb4taVrEsjLpFwwtNRQdDAxALY7lThvwI7199ftm/BM/Gn4UjUdDgF9r2kj7ZZeR8zXMRGXRSOuR8w9SPegD8raKfLE9vK8UqNHIhKsjjBUjqCO1MoAKKKKACiiigD9Mf2D/AI+f8LE8D/8ACI6tc79e0KMLE0jfNPbdFPuV4U+2K+qq/E/4WfEbU/hR470nxPpLkXNlKGePOFmjPDxt7MMj8j2r9l/B3ii08beFdJ1+w3Cz1K2juog4wwDKDg+46fhQBs0UUUAFFFFAHnHjj9nX4cfEjWm1fxF4Vs9R1J1CvclnjdwOm7Ywz9TXO/8ADGvwb/6Ee0/8CJ//AIuvaaKAPFv+GNfg2P8AmSLT/wACJ/8A4umf8Md/Bf8A6Euy/wDAmb/45Xq3i448Ka1/15T/APotq/D3zpP+ejfnQB+tn/DHfwX/AOhLsv8AwJm/+OUf8Md/Bf8A6Euy/wDAmb/45X5J+dJ/z0b86POk/wCejfnQB+uEH7JHwY0+VbkeDNOUxfNmWeVlGPUF8fnWD8Xf2tPh18DPDz6XoM9hq+rQRmO10jSGUwwntvZPlQD0HPtX5WGV2GC7EehNNoA3vHPjXVfiJ4r1HxDrVwbnUb6UySN2X0VR2AGAB7Vg0UUAfqf+wR/ybjpn/X5cf+hCvzQ+If8AyP8A4m/7Cdz/AOjWr9L/ANgj/k3HTf8Ar8uP/QhX5ofEP/kf/E3/AGE7n/0a1AHP19vfsl/tt2PhfRbLwb4/neGytlENjrBBcRp2jlA5wOgYdBXxDRQB+v8Aq/wf+DnxrlGuzaRoXiKSb5jf2M4zJ7s0TDJ+vNZ//DHXwX/6Eyy/8CZv/jlfkirFehI+lL5r/wB9vzoA/W3/AIY6+C//AEJll/4Ezf8Axyj/AIY6+C//AEJll/4Ezf8AxyvyS81/77fnR5r/AN9vzoA/W5f2N/gy4yvgm0b6XM//AMXTv+GNPg3/ANCRa/8AgRP/APF15J/wTSYt8NfFGST/AMTNOv8A1zr7EoA8Yj/Y4+DkUiuPA9oSpyMzzEflvr1/T9PttJsbeysoI7W0t41iihiUKkaAYCgDoAKsUUAFFFFABRRRQAUUUUAZHi//AJFPWv8Aryn/APRbV+HdfuJ4v/5FPWv+vKf/ANFtX4d0AFFFFABRRRQAUUUUAfqd+wP/AMm46b/1+XH8xX5o/EP/AJH/AMTf9hO6/wDRrV+l37A//JuOm/8AX5c/+hCvzR+If/I/+Jv+wnc/+jWoA5+iiigAooooAKKKKAP0W/4Jo/8AJNfFP/YTT/0XX2LXx1/wTR/5Jr4p/wCwmn/ouvsWgAooooAKKKKACiiigAooooAyPF//ACKetf8AXlP/AOi2r8O6/cTxf/yKetf9eU//AKLavw7oAKKKKACiiigAooooA/U79gf/AJNx03/r8uf/AEIV+aPxD/5H/wATf9hO5/8ARrV+l37A/wDybjpv/X5c/wDoQr80fiH/AMj/AOJv+wnc/wDo1qAOfooooAKKKKACiiigD9Fv+CaP/JNfFP8A2E0/9F19i18df8E0f+SaeKf+wmn/AKLr7FoAKKKKACiiigAooooAKKKKAMzxPBJdeGtWhiUvLJaTIijqSUIAr8OCCpIIII4INfuyRkV+fP7Tv7DHiBfFOoeJfh/ZDVNOvpGuJtKjYLLA7HLbASAyk5OByPSgD4rorttX+CHxC0HcdQ8D+ILZB1kbTZin/fQXH61yN1p91YymO5tpreQdUljKkfgaAK9FGMUUAFFFFAH6nfsDf8m46b/1+XP8xX5o/EL/AJH7xL/2E7n/ANGtX6XfsDf8m46b/wBflz/MV+aPxC/5H7xL/wBhO5/9GtQBz9FFFABRRTljZvuqT9BQA2iuh0j4d+K/EG3+y/DOsalu6fZLCWXP/fKmvUPh9+xp8UvHWqwW8vhu68P2LMPNvdWTyVjXudh+Zj7AflQB9Vf8E1LaSL4XeJJmQiOXUxsY9DiMZr7ArjfhH8L9L+D3gLTfDGk5eC1XMk7DDTSnlnPuT+mK7KgAooooAKKKKACiiigAooooAKKKKACq19ptpqcXl3lrBdx/3J4w4/IirNFAHD6v8Dfh5rob7b4J0KZm6uLCNGP/AAJQDXEav+xf8H9X3E+EYrNz1a1nkT9NxH6V7fRQB8tav/wTr+GN9uNnPrGnsf7tyJAPwK1xWq/8EytHck6d4zvY/Rbm2Rsfka+2qKAPN/gB8Im+CXw3tvCzaiNTMM0kv2gR7M7znGM1826p/wAE3Itc8R6lqV34zeOO8upbjy4bQZUO5bGSfevtqigD5F0j/gmz4DtNv2/XdY1DHXaUiz+QNdvpH7CPwg0rbv0O51DH/P3eOc/984r6DooA8y0j9mf4WaHt+y+BdH46efB53/oea7XSvBugaDt/szQ9N07b0+yWkcWP++QK2KKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP//Z';
         var ext = staff.image.substring("data:image/".length, staff.image.indexOf(";base64"));
         var base64String = "";
         start position is 22 or 23
         if (ext.length == 3) {
             base64String = staff.image.substring(22);
         }
         else if (ext.length == 4) {
             base64String = staff.image.substring(23);
         }
 
         staff._attachments = {
             "picture": { // i set all extensions whether jpg or png to jpg to avoid array index error. it does not matter as we already have the content type stored
                 content_type: 'image/' + ext,
                 data: base64String
             },
         }
 
         staff.image = "";
  */
        //}
        if (this.retrieve != undefined) {
            staff._attachments = this.retrieve;

        }
        /*  if (this.file == 'null') {
             return this.db.rel.save('staff', staff)
                 .then((data: any) => {
 
                     if (data && data.staffs && data.staffs
                     [0]) {
                         return data.staffs[0]
                     }
 
                     return null;
                 }).catch((err: any) => {
                     console.error(err);
                 });
         } */


        /*    staff.profile = "";  
              this.db.rel.putAttachment(attachment).then(res => {
            console.log(res)
            console.log("res")
          })   */


        this.db.rel.putAttachment('staff', { id: staff.id, rev: staff.rev }, 'file', staff._attachments, 'image/png').then(res => {
            staff._attachments = res;
            //staff.image = data.staffs[0].image;

        })
        console.log(staff._attachments);
        if (staff._attachments != "") {
            staff.imageurl = URL.createObjectURL(staff._attachments);
        }
        staff.imageurl = "";
        console.log(staff);
        return this.db.rel.save('staff', staff)
            /*  .then((data: any) => {
                 if (data && data.staffs && data.staffs
                 [0]) {
                     console.log(data.staffs[0]);
                     return data.staffs[0]
                 }
 
                 return null;
             }) *//* .catch((err: any) => {
      console.error(err);
      });  */
            .then((data: any) => {

                if (data && data.staffs && data.staffs
                [0]) {


                    /*  this.recordservice.saveRecord(record, data.staffs[0]).then(res => {
                         return data.staffs[0];
                     }); */
                    console.log(data.staffs[0]);
                    return data.staffs[0];

                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });


        // } 
    }


    /**
    * Update a staff
    * @param {staff} staff
    *
    * @return Promise<staff>
    */
    updateStaff(staff: Staff) {

        if (this.file) {
            this.file;

            this.retrieve = (<HTMLInputElement>this.file).files[0];

            if (this.retrieve = (<HTMLInputElement>this.file).files[0]) {
                staff._attachments = this.retrieve
            }

            else {
                staff._attachments;
                //this.getStaff = staff._attachments;
            }

            return this.db.rel.save('staff', staff)
                .then((data: any) => {
                    if (data && data.staffs && data.staffs
                    [0]) {
                        this.db.rel.putAttachment('staff', { id: staff.id, rev: staff.rev }, 'file', staff._attachments, 'image/png').then(res => {
                            var reader: FileReader = new FileReader();

                            reader.onloadend = function (e) {
                                var base64data = reader.result;
                                staff.image = base64data
                                //console.log(staff.image);
                            }
                            reader.readAsDataURL(this.retrieve);
                            staff._attachments = res;
                            staff.image = res

                        })
                        console.log(staff._attachments);
                        staff.imageurl = URL.createObjectURL(staff._attachments);
                        return data.staffs[0]
                    }

                    return null;
                }).catch((err: any) => {
                    console.error(err);
                });

        }
        else {


            return this.db.rel.save('staff', staff)
                .then((data: any) => {
                    if (data && data.staffs && data.staffs
                    [0]) {
                        this.db.rel.putAttachment('staff', { id: staff.id, rev: staff.rev }, 'file', staff._attachments, 'image/png').then(res => {
                            staff._attachments = res
                        })
                        return data.staffs[0]
                    }

                    return null;
                }).catch((err: any) => {
                    console.error(err);
                });
        }
    }

    /**
     * Remove a staff
     * @param {staff} staff
     *
     * @return Promise<boolean>
     */
    removeStaff(staff: Staff): Promise<boolean> {
        return this.db.rel.del('staff', staff)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the staffs
     *
     * @return Promise<Array<staff>>
     */
    getStaffs(): Promise<Array<Staff>> {
        return this.db.rel.find('staff')
            .then((data: any) => {
                this.staffs = data.staffs;

                /*  this.db.rel.getAttachment('staff', 'file').then(attachment => {
                     console.log(attachment)
                     var url = URL.createObjectURL(attachment);
                     console.log(url); */
                /*   $('img').attr('src', URL.createObjectURL(attachment))
                }).then(res => {
                  console.log(res); */
                //var attach = document.getElementById("attachment").innerHTML = attachment

                // })
                if (this.searchText && this.searchText !== '') {
                    this.staffs = FuseUtils.filterArrayByString(this.staffs, this.searchText);
                }
                //this.onstaffsChanged.next(this.staffs);
                var userType = "Patient"
                this.staffs = this.staffs.filter(data => data.usertype != userType);
                return Promise.resolve(this.staffs);
                //return data.staffs ? data.staffs : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getGStaffs(): Promise<Array<Staff>> {
        return this.db.rel.find('staff')
            .then((data: any) => {
                this.staffs = data.staffs;


                /*  this.db.rel.getAttachment('staff', 'file').then(attachment => {
                     console.log(attachment)
                     var url = URL.createObjectURL(attachment);
                     console.log(url); */
                /*   $('img').attr('src', URL.createObjectURL(attachment))
                }).then(res => {
                  console.log(res); */
                //var attach = document.getElementById("attachment").innerHTML = attachment

                // })
                if (this.searchText && this.searchText !== '') {
                    this.staffs = FuseUtils.filterArrayByString(this.staffs, this.searchText);
                }
                //this.onstaffsChanged.next(this.staffs);
                return Promise.resolve(this.staffs);
                //return data.staffs ? data.staffs : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getPStaffs(): Promise<Array<Staff>> {
        return this.db.rel.find('staff')
            .then((data: any) => {
                this.staffs = data.staffs;

                /*  this.db.rel.getAttachment('staff', 'file').then(attachment => {
                     console.log(attachment)
                     var url = URL.createObjectURL(attachment);
                     console.log(url); */
                /*   $('img').attr('src', URL.createObjectURL(attachment))
                }).then(res => {
                  console.log(res); */
                //var attach = document.getElementById("attachment").innerHTML = attachment

                // })
                if (this.searchText && this.searchText !== '') {
                    this.staffs = FuseUtils.filterArrayByString(this.staffs, this.searchText);
                }
                //this.onstaffsChanged.next(this.staffs);
                var userType = "Patient"
                this.staffs = this.staffs.filter(data => data.usertype == userType);
                return Promise.resolve(this.staffs);
                //return data.staffs ? data.staffs : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getDStaffs(): Promise<Array<Staff>> {
        return this.db.rel.find('staff')
            .then((data: any) => {
                this.staffs = data.staffs;

                /*  this.db.rel.getAttachment('staff', 'file').then(attachment => {
                     console.log(attachment)
                     var url = URL.createObjectURL(attachment);
                     console.log(url); */
                /*   $('img').attr('src', URL.createObjectURL(attachment))
                }).then(res => {
                  console.log(res); */
                //var attach = document.getElementById("attachment").innerHTML = attachment

                // })
                if (this.searchText && this.searchText !== '') {
                    this.staffs = FuseUtils.filterArrayByString(this.staffs, this.searchText);
                }
                //this.onstaffsChanged.next(this.staffs);
                var userType = "Doctor"
                this.staffs = this.staffs.filter(data => data.usertype == userType);
                return Promise.resolve(this.staffs);
                //return data.staffs ? data.staffs : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getLStaffs(): Promise<Array<Staff>> {
        return this.db.rel.find('staff')
            .then((data: any) => {
                this.staffs = data.staffs;

                /*  this.db.rel.getAttachment('staff', 'file').then(attachment => {
                     console.log(attachment)
                     var url = URL.createObjectURL(attachment);
                     console.log(url); */
                /*   $('img').attr('src', URL.createObjectURL(attachment))
                }).then(res => {
                  console.log(res); */
                //var attach = document.getElementById("attachment").innerHTML = attachment

                // })
                if (this.searchText && this.searchText !== '') {
                    this.staffs = FuseUtils.filterArrayByString(this.staffs, this.searchText);
                }
                //this.onstaffsChanged.next(this.staffs);
                var userType = "Laboratorist"
                this.staffs = this.staffs.filter(data => data.usertype == userType);
                return Promise.resolve(this.staffs);
                //return data.staffs ? data.staffs : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getAStaffs(): Promise<Array<Staff>> {
        return this.db.rel.find('staff')
            .then((data: any) => {
                this.staffs = data.staffs;

                /*  this.db.rel.getAttachment('staff', 'file').then(attachment => {
                     console.log(attachment)
                     var url = URL.createObjectURL(attachment);
                     console.log(url); */
                /*   $('img').attr('src', URL.createObjectURL(attachment))
                }).then(res => {
                  console.log(res); */
                //var attach = document.getElementById("attachment").innerHTML = attachment

                // })
                if (this.searchText && this.searchText !== '') {
                    this.staffs = FuseUtils.filterArrayByString(this.staffs, this.searchText);
                }
                //this.onstaffsChanged.next(this.staffs);
                var userType = "Accountant"
                this.staffs = this.staffs.filter(data => data.usertype == userType);
                return Promise.resolve(this.staffs);
                //return data.staffs ? data.staffs : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getADStaffs(): Promise<Array<Staff>> {
        return this.db.rel.find('staff')
            .then((data: any) => {
                this.staffs = data.staffs;

                /*  this.db.rel.getAttachment('staff', 'file').then(attachment => {
                     console.log(attachment)
                     var url = URL.createObjectURL(attachment);
                     console.log(url); */
                /*   $('img').attr('src', URL.createObjectURL(attachment))
                }).then(res => {
                  console.log(res); */
                //var attach = document.getElementById("attachment").innerHTML = attachment

                // })
                if (this.searchText && this.searchText !== '') {
                    this.staffs = FuseUtils.filterArrayByString(this.staffs, this.searchText);
                }
                //this.onstaffsChanged.next(this.staffs);
                var userType = "Admin"
                this.staffs = this.staffs.filter(data => data.usertype == userType);
                return Promise.resolve(this.staffs);
                //return data.staffs ? data.staffs : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getNStaffs(): Promise<Array<Staff>> {
        return this.db.rel.find('staff')
            .then((data: any) => {
                this.staffs = data.staffs;

                /*  this.db.rel.getAttachment('staff', 'file').then(attachment => {
                     console.log(attachment)
                     var url = URL.createObjectURL(attachment);
                     console.log(url); */
                /*   $('img').attr('src', URL.createObjectURL(attachment))
                }).then(res => {
                  console.log(res); */
                //var attach = document.getElementById("attachment").innerHTML = attachment

                // })
                if (this.searchText && this.searchText !== '') {
                    this.staffs = FuseUtils.filterArrayByString(this.staffs, this.searchText);
                }
                //this.onstaffsChanged.next(this.staffs);
                var userType = "Nurse"
                this.staffs = this.staffs.filter(data => data.usertype == userType);
                return Promise.resolve(this.staffs);
                //return data.staffs ? data.staffs : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }
    getPHStaffs(): Promise<Array<Staff>> {
        return this.db.rel.find('staff')
            .then((data: any) => {
                this.staffs = data.staffs;

                /*  this.db.rel.getAttachment('staff', 'file').then(attachment => {
                     console.log(attachment)
                     var url = URL.createObjectURL(attachment);
                     console.log(url); */
                /*   $('img').attr('src', URL.createObjectURL(attachment))
                }).then(res => {
                  console.log(res); */
                //var attach = document.getElementById("attachment").innerHTML = attachment

                // })
                if (this.searchText && this.searchText !== '') {
                    this.staffs = FuseUtils.filterArrayByString(this.staffs, this.searchText);
                }
                //this.onstaffsChanged.next(this.staffs);
                var userType = "Pharmacist"
                this.staffs = this.staffs.filter(data => data.usertype == userType);
                return Promise.resolve(this.staffs);
                //return data.staffs ? data.staffs : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getRStaffs(): Promise<Array<Staff>> {
        return this.db.rel.find('staff')
            .then((data: any) => {
                this.staffs = data.staffs;

                /*  this.db.rel.getAttachment('staff', 'file').then(attachment => {
                     console.log(attachment)
                     var url = URL.createObjectURL(attachment);
                     console.log(url); */
                /*   $('img').attr('src', URL.createObjectURL(attachment))
                }).then(res => {
                  console.log(res); */
                //var attach = document.getElementById("attachment").innerHTML = attachment

                // })
                if (this.searchText && this.searchText !== '') {
                    this.staffs = FuseUtils.filterArrayByString(this.staffs, this.searchText);
                }
                //this.onstaffsChanged.next(this.staffs);
                var userType = "Receptionist"
                this.staffs = this.staffs.filter(data => data.usertype == userType);
                return Promise.resolve(this.staffs);
                //return data.staffs ? data.staffs : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a staff
     * @param {staff} staff
     *
     * @return Promise<staff>
     */
    getStaff(staff: Staff): Promise<Staff> {
        /* this.db.rel.getAttachment('staff',staff.id, 'file').then(attachment => {
                    console.log(attachment)
                    var url = URL.createObjectURL(attachment);
                    console.log(url); */
        /*   $('img').attr('src', URL.createObjectURL(attachment))
        }).then(res => {
          console.log(res); */
        //var attach = document.getElementById("attachment").innerHTML = attachment

        //})

        return this.db.rel.find('staff', staff.id)
            .then((data: any) => {

                console.log(data)
                return data && data.staffs ? data.staffs[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getStaff2(id): Promise<Staff> {
        /* this.db.rel.getAttachment('staff',staff.id, 'file').then(attachment => {
                    console.log(attachment)
                    var url = URL.createObjectURL(attachment);
                    console.log(url); */
        /*   $('img').attr('src', URL.createObjectURL(attachment))
        }).then(res => {
          console.log(res); */
        //var attach = document.getElementById("attachment").innerHTML = attachment

        //})

        return this.db.rel.find('staff', id)
            .then((data: any) => {

                console.log(data)
                return data && data.staffs ? data.staffs[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getPStaff(staff: Staff): Promise<Staff> {
        /* this.db.rel.getAttachment('staff',staff.id, 'file').then(attachment => {
                    console.log(attachment)
                    var url = URL.createObjectURL(attachment);
                    console.log(url); */
        /*   $('img').attr('src', URL.createObjectURL(attachment))
        }).then(res => {
          console.log(res); */
        //var attach = document.getElementById("attachment").innerHTML = attachment

        //})

        return this.db.rel.find('staff', staff.id)
            .then((data: any) => {

                console.log("data")
                return data && data.staffs ? data.staffs[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    getPStaff2(id): Promise<Staff> {
        /* this.db.rel.getAttachment('staff',staff.id, 'file').then(attachment => {
                    console.log(attachment)
                    var url = URL.createObjectURL(attachment);
                    console.log(url); */
        /*   $('img').attr('src', URL.createObjectURL(attachment))
        }).then(res => {
          console.log(res); */
        //var attach = document.getElementById("attachment").innerHTML = attachment

        //})

        return this.db.rel.find('staff', id)
            .then((data: any) => {

                console.log(data)
                return data && data.staffs ? data.staffs[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected staff by id
     * @param id
     */
    toggleSelectedStaff(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedStaffs.length > 0) {
            const index = this.selectedStaffs.indexOf(id);

            if (index !== -1) {
                this.selectedStaffs.splice(index, 1);

                // Trigger the next event
                this.onSelectedStaffsChanged.next(this.selectedStaffs);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedStaffs.push(id);

        // Trigger the next event
        this.onSelectedStaffsChanged.next(this.selectedStaffs);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedStaffs.length > 0) {
            this.deselectStaffs();
        }
        else {
            this.selectStaffs();
        }
    }

    selectStaffs(filterParameter?, filterValue?) {
        this.selectedStaffs = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedStaffs = [];
            this.staffs.map(staff => {
                this.selectedStaffs.push(staff.id);
            });
        }
        else {
            /* this.selectedstaffs.push(...
                 this.staffs.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedStaffsChanged.next(this.selectedStaffs);
    }





    deselectStaffs() {
        this.selectedStaffs = [];

        // Trigger the next event
        this.onSelectedStaffsChanged.next(this.selectedStaffs);
    }

    deleteStaff(staff) {
        this.db.rel.del('staff', staff)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const staffIndex = this.staffs.indexOf(staff);
                this.staffs.splice(staffIndex, 1);
                this.onStaffsChanged.next(this.staffs);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedStaffs() {
        for (const staffId of this.selectedStaffs) {
            const staff = this.staffs.find(_staff => {
                return _staff.id === staffId;
            });

            this.db.rel.del('staff', staff)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const staffIndex = this.staffs.indexOf(staff);
                    this.staffs.splice(staffIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onStaffsChanged.next(this.staffs);
        this.deselectStaffs();
    }

    /***********
    * accountant
    **********/
    /**
     * Save a accountant
     * @param {accountant} accountant
     *
     * @return Promise<accountant>
     */
    saveAccountant(accountant: Accountant): Promise<Accountant> {
        accountant.id = Math.floor(Date.now()).toString();

        accountant.payrolls = [];
        return this.db.rel.save('accountant', accountant)
            .then((data: any) => {

                if (data && data.accountants && data.accountants
                [0]) {
                    return data.accountants[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a accountant
    * @param {accountant} accountant
    *
    * @return Promise<accountant>
    */
    updateAccountant(accountant: Accountant) {
        return this.db.rel.save('accountant', accountant)
            .then((data: any) => {
                if (data && data.accountants && data.accountants
                [0]) {
                    return data.accountants[0]
                }
                return null;
            }).catch((err: any) => {
                console.error(err);
            });

    }
    /**
     * Remove a accountant
     * @param {accountant} accountant
     *
     * @return Promise<boolean>
     */
    removeAccountant(accountant: Accountant): Promise<boolean> {
        return this.db.rel.del('accountant', accountant)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getAccountants(): Promise<Array<Accountant>> {
        return this.db.rel.find('accountant')
            .then((data: any) => {
                this.accountants = data.accountants;
                if (this.searchText && this.searchText !== '') {
                    this.accountants = FuseUtils.filterArrayByString(this.accountants, this.searchText);
                }
                //this.onaccountantsChanged.next(this.accountants);
                return Promise.resolve(this.accountants);
                //return data.accountants ? data.accountants : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }
    /**
     * Read a accountant
     * @param {accountant} accountant
     *
     * @return Promise<accountant>
     */
    getAccountant(accountant: Accountant): Promise<Accountant> {
        return this.db.rel.find('accountant', accountant.id)
            .then((data: any) => {
                return data && data.accountants ? data.accountants[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Toggle selected accountant by id
     * @param id
     */
    toggleSelectedAccountant(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedAccountants.length > 0) {
            const index = this.selectedAccountants.indexOf(id);

            if (index !== -1) {
                this.selectedAccountants.splice(index, 1);

                // Trigger the next event
                this.onSelectedAccountantsChanged.next(this.selectedAccountants);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedAccountants.push(id);

        // Trigger the next event
        this.onSelectedAccountantsChanged.next(this.selectedAccountants);
    }


    /**
     * Toggle select all
     */
    toggleSelectAllAccountant() {
        if (this.selectedAccountants.length > 0) {
            this.deselectAccountants();
        }
        else {
            this.selectAccountants();
        }
    }

    selectAccountants(filterParameter?, filterValue?) {
        this.selectedAccountants = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedAccountants = [];
            this.accountants.map(accountant => {
                this.selectedAccountants.push(accountant.id);
            });
        }
        else {
            /* this.selectedaccountants.push(...
                 this.accountants.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedAccountantsChanged.next(this.selectedAccountants);
    }

    deselectAccountants() {
        //console.log(this.onSelectedaccountantsChanged);
        this.selectedAccountants = [];
        //return this.selectedaccountants;

        // Trigger the next event
        this.onSelectedAccountantsChanged.next(this.selectedAccountants);
        //console.log(this.onSelectedaccountantsChanged);
    }

    deleteAccountant(accountant) {
        this.db.rel.del('accountant', accountant)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const accountantIndex = this.accountants.indexOf(accountant);
                this.accountants.splice(accountantIndex, 1);
                this.onAccountantsChanged.next(this.accountants);
            }).catch((err: any) => {
                console.error(err);
            });
    }

    deleteSelectedAccountants() {
        for (const accountantId of this.selectedAccountants) {
            const accountant = this.accountants.find(_accountant => {
                return _accountant.id === accountantId;
            });

            this.db.rel.del('accountant', accountant)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const accountantIndex = this.accountants.indexOf(accountant);
                    this.accountants.splice(accountantIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onAccountantsChanged.next(this.accountants);
        this.deselectAccountants();
    }

    /***********
     * account
     **********/
    /**
     * Save a account
     * @param {account} account
     *
     * @return Promise<account>
     */
    saveAccount(account: Account): Promise<Account> {
        account.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('account', account)
            .then((data: any) => {

                if (data && data.accounts && data.accounts
                [0]) {
                    return data.accounts[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a account
    * @param {account} account
    *
    * @return Promise<account>
    */
    updateAccount(account: Account) {

        return this.db.rel.save('account', account)
            .then((data: any) => {
                if (data && data.accounts && data.accounts
                [0]) {
                    return data.accounts[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a account
     * @param {account} account
     *
     * @return Promise<boolean>
     */
    removeAccount(account: Account): Promise<boolean> {
        return this.db.rel.del('account', account)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the accounts
     *
     * @return Promise<Array<account>>
     */
    getAccounts(): Promise<Array<Account>> {
        return this.db.rel.find('account')
            .then((data: any) => {
                this.accounts = data.accounts;
                if (this.searchText && this.searchText !== '') {
                    this.accounts = FuseUtils.filterArrayByString(this.accounts, this.searchText);
                }
                //this.onaccountsChanged.next(this.accounts);
                return Promise.resolve(this.accounts);
                //return data.accounts ? data.accounts : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a account
     * @param {account} account
     *
     * @return Promise<account>
     */
    getAccount(account: Account): Promise<Account> {
        return this.db.rel.find('account', account.id)
            .then((data: any) => {
                return data && data.accounts ? data.accounts[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected account by id
     * @param id
     */
    toggleSelectedAccount(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedAccounts.length > 0) {
            const index = this.selectedAccounts.indexOf(id);

            if (index !== -1) {
                this.selectedAccounts.splice(index, 1);

                // Trigger the next event
                this.onSelectedAccountsChanged.next(this.selectedAccounts);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedAccounts.push(id);

        // Trigger the next event
        this.onSelectedAccountsChanged.next(this.selectedAccounts);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllAccount() {
        if (this.selectedAccounts.length > 0) {
            this.deselectAccounts();
        }
        else {
            this.selectAccounts();
        }
    }

    selectAccounts(filterParameter?, filterValue?) {
        this.selectedAccounts = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedAccounts = [];
            this.accounts.map(account => {
                this.selectedAccounts.push(account.id);
            });
        }
        else {
            /* this.selectedaccounts.push(...
                 this.accounts.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedAccountsChanged.next(this.selectedAccounts);
    }





    deselectAccounts() {
        this.selectedAccounts = [];

        // Trigger the next event
        this.onSelectedAccountsChanged.next(this.selectedAccounts);
    }

    deleteAccount(account) {
        this.db.rel.del('account', account)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const accountIndex = this.accounts.indexOf(account);
                this.accounts.splice(accountIndex, 1);
                this.onAccountsChanged.next(this.accounts);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedAccounts() {
        for (const accountId of this.selectedAccounts) {
            const account = this.accounts.find(_account => {
                return _account.id === accountId;
            });

            this.db.rel.del('account', account)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const accountIndex = this.accounts.indexOf(account);
                    this.accounts.splice(accountIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onAccountsChanged.next(this.accounts);
        this.deselectAccounts();
    }

    /***********
    * addappointment
    **********/
    /**
     * Save a addappointment
     * @param {addappointment} addappointment
     *
     * @return Promise<addappointment>
     */
    saveAddappointment(appointment: Appointment): Promise<Appointment> {
        appointment.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('appointment', appointment)
            .then((data: any) => {

                if (data && data.appointments && data.appointments
                [0]) {

                    return data.appointments[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a appointment
    * @param {appointment} appointment
    *
    * @return Promise<appointment>
    */
    updateAddappointment(appointment: Appointment) {

        return this.db.rel.save('appointment', appointment)
            .then((data: any) => {
                if (data && data.appointments && data.appointments
                [0]) {

                    return data.appointments[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    updateAddappointments(appointment: Promise<Array<Appointment>>) {

        return this.db.rel.save('appointment', appointment)
            .then((data: any) => {
                if (data && data.appointments && data.appointments
                [0]) {
                    return data.appointments[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a addappointment
     * @param {addappointment} addappointment
     *
     * @return Promise<boolean>
     */
    removeAddappointment(appointment: Appointment): Promise<boolean> {
        return this.db.rel.del('appointment', appointment)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the addappointments
     *
     * @return Promise<Array<addappointment>>
     */
    getAddappointments(): Promise<Array<Appointment>> {
        var localStorageItem = JSON.parse(localStorage.getItem('user'));
        var userType = localStorageItem.usertype;
        console.log(userType);
        console.log(localStorageItem);
        return this.db.rel.find('appointment')
            .then((data: any) => {
                this.addappointments = data.appointments;

                this.onEventsUpdated.next(this.addappointments);
                if (this.searchText && this.searchText !== '') {
                    this.addappointments = FuseUtils.filterArrayByString(this.addappointments, this.searchText);
                }
                //this.onnoticesChanged.next(this.addappointments);
                if (userType === "Doctor") {
                    var userId = localStorageItem.id
                    this.addappointments = this.addappointments.filter(data => data.nameId == userId)

                }
                else if (userType === "Patient") {
                    var userId = localStorageItem.id
                    this.addappointments = this.addappointments.filter(data => data.patientId == userId)

                }

                return Promise.resolve(this.addappointments);

                //return data.addappointments ? data.addappointments : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Read a addappointment
     * @param {addappointment} addappointment
     *
     * @return Promise<addappointment>
     */
    getAddappointment(appointment: Appointment): Promise<Appointment> {
        return this.db.rel.find('appointment', appointment.id)
            .then((data: any) => {
                //this.onEventsUpdated.next(this.notices);
                return data && data.appointments ? data.appointments[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected addappointment by id
     * @param id
     */
    toggleSelectedAddappointment(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedAddappointments.length > 0) {
            const index = this.selectedAddappointments.indexOf(id);

            if (index !== -1) {
                this.selectedAddappointments.splice(index, 1);

                // Trigger the next event
                this.onSelectedAddappointmentsChanged.next(this.selectedAddappointments);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedAddappointments.push(id);

        // Trigger the next event
        this.onSelectedAddappointmentsChanged.next(this.selectedAddappointments);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllAppointment() {
        if (this.selectedAddappointments.length > 0) {
            this.deselectAddappointments();
        }
        else {
            this.selectAddappointments();
        }
    }

    selectAddappointments(filterParameter?, filterValue?) {
        this.selectedAddappointments = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedAddappointments = [];
            this.addappointments.map(appointment => {
                this.selectedAddappointments.push(appointment.id);
            });
        }
        else {
            /* this.selectedaddappointments.push(...
                 this.addappointments.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedAddappointmentsChanged.next(this.selectedAddappointments);
    }





    deselectAddappointments() {
        this.selectedAddappointments = [];

        // Trigger the next event
        this.onSelectedAddappointmentsChanged.next(this.selectedAddappointments);
    }

    deleteAddappointment(appointment) {
        this.db.rel.del('appointment', appointment)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const addappointmentIndex = this.addappointments.indexOf(appointment);
                this.addappointments.splice(addappointmentIndex, 1);
                this.onAddappointmentsChanged.next(this.addappointments);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedAddappointments() {
        for (const addappointmentId of this.selectedAddappointments) {
            const addappointment = this.addappointments.find(_addappointment => {
                return _addappointment.id === addappointmentId;
            });

            this.db.rel.del('appointment', addappointment)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const addappointmentIndex = this.addappointments.indexOf(addappointment);
                    this.addappointments.splice(addappointmentIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onAddappointmentsChanged.next(this.addappointments);
        this.deselectAddappointments();
    }

    /***********
    * admin
    **********/
    /**
     * Save a admin
     * @param {admin} admin
     *
     * @return Promise<admin>
     */
    saveAdmin(admin: Admin): Promise<Admin> {
        admin.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('admin', admin)
            .then((data: any) => {
                if (data && data.admins && data.admins
                [0]) {

                    return data.admins[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });

    }

    /**
    * Update a admin
    * @param {admin} admin
    *
    * @return Promise<admin>
    */
    updateAdmin(admin: Admin) {

        return this.db.rel.save('admin', admin)
            .then((data: any) => {
                if (data && data.admins && data.admins
                [0]) {
                    return data.admins[0]
                }
                return null;
            }).catch((err: any) => {
                console.error(err);
            });

    }


    /**
     * Remove a admin
     * @param {admin} Admin
     *
     * @return Promise<boolean>
     */
    removeAdmin(admin: Admin): Promise<boolean> {
        return this.db.rel.del('admin', admin)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }



    getAdmins(): Promise<Array<Admin>> {
        return this.db.rel.find('admin')
            .then((data: any) => {
                this.admins = data.admins;
                if (this.searchText && this.searchText !== '') {
                    this.admins = FuseUtils.filterArrayByString(this.admins, this.searchText);
                }

                return Promise.resolve(this.admins);
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a admin
     * @param {admin} admin
     *
     * @return Promise<admin>
     */
    getAdmin(admin: Admin): Promise<Admin> {
        return this.db.rel.find('admin', admin.id)
            .then((data: any) => {
                return data && data.admins ? data.admins[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Toggle selected admin by id
     * @param id
     */
    toggleSelectedAdmin(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedAdmins.length > 0) {
            const index = this.selectedAdmins.indexOf(id);

            if (index !== -1) {
                this.selectedAdmins.splice(index, 1);

                // Trigger the next event
                this.onSelectedAdminsChanged.next(this.selectedAdmins);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedAdmins.push(id);

        // Trigger the next event
        this.onSelectedAdminsChanged.next(this.selectedAdmins);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllAdmin() {
        if (this.selectedAdmins.length > 0) {
            this.deselectAdmins();
        }
        else {
            this.selectAdmins();
        }
    }


    selectAdmins(filterParameter?, filterValue?) {
        this.selectedAdmins = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedAdmins = [];
            this.admins.map(admin => {
                this.selectedAdmins.push(admin.id);
            });
        }


        // Trigger the next event
        this.onSelectedAdminsChanged.next(this.selectedAdmins);
    }

    deselectAdmins() {
        //console.log(this.onSelectedadmin2sChanged);
        this.selectedAdmins = [];
        //return this.selectedadmin2s;

        // Trigger the next event
        this.onSelectedAdminsChanged.next(this.selectedAdmins);
        //console.log(this.onSelectedadmin2sChanged);
    }

    deleteAdmin(admin) {
        this.db.rel.del('admin', admin)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const adminIndex = this.admins.indexOf(admin);
                this.admins.splice(adminIndex, 1);
                this.onAdminsChanged.next(this.admins);
            }).catch((err: any) => {
                console.error(err);
            });
    }

    deleteSelectedAdmins() {
        for (const adminId of this.selectedAdmins) {
            const admin = this.admins.find(_admin => {
                return _admin.id === adminId;
            });

            this.db.rel.del('admin', admin)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const adminIndex = this.admins.indexOf(admin);
                    this.admins.splice(adminIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onAdminsChanged.next(this.admins);
        this.deselectAdmins();
    }

    /***********
     * alerts
     **********/
    /**
     * Save a alerts
     * @param {alerts} alerts
     *
     * @return Promise<alerts>
     */
    savealerts(alerts: alerts): Promise<alerts> {
        //alerts.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('alerts', alerts)
            .then((data: any) => {

                if (data && data.alertss && data.alertss
                [0]) {
                    console.log('save');

                    return data.alertss[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a alerts
    * @param {alerts} alerts
    *
    * @return Promise<alerts>
    */
    updatealerts(alerts: alerts) {
        return this.db.rel.save('alerts', alerts)
            .then((data: any) => {
                if (data && data.alertss && data.alertss
                [0]) {
                    console.log('Update')

                    return data.alertss[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a alerts
     * @param {alerts} alerts
     *
     * @return Promise<boolean>
     */
    removealerts(alerts: alerts): Promise<boolean> {
        return this.db.rel.del('alerts', alerts)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the alertss
     *
     * @return Promise<Array<alerts>>
     */
    getalertss(): Promise<Array<alerts>> {
        return this.db.rel.find('alerts')
            .then((data: any) => {
                this.alertss = data.alertss;
                if (this.searchText && this.searchText !== '') {
                    this.alertss = FuseUtils.filterArrayByString(this.alertss, this.searchText);
                }
                //this.onalertssChanged.next(this.alertss);
                return Promise.resolve(this.alertss);
                //return data.alertss ? data.alertss : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getDoctoralertss(): Promise<Array<alerts>> {
        return this.db.rel.find('alerts')
            .then((data: any) => {
                return data.alertss ? data.alertss : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a alerts
     * @param {alerts} alerts
     *
     * @return Promise<alerts>
     */
    getalerts(alerts: alerts): Promise<alerts> {
        return this.db.rel.find('alerts', alerts.id)
            .then((data: any) => {
                console.log("Get")

                return data && data.alertss ? data.alertss[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected alerts by id
     * @param id
     */
    toggleSelectedalerts(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedalertss.length > 0) {
            const index = this.selectedalertss.indexOf(id);

            if (index !== -1) {
                this.selectedalertss.splice(index, 1);

                // Trigger the next event
                this.onSelectedalertssChanged.next(this.selectedalertss);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedalertss.push(id);

        // Trigger the next event
        this.onSelectedalertssChanged.next(this.selectedalertss);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllAlerts() {
        if (this.selectedalertss.length > 0) {
            this.deselectalertss();
        }
        else {
            this.selectalertss();
        }
    }

    selectalertss(filterParameter?, filterValue?) {
        this.selectedalertss = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedalertss = [];
            this.alertss.map(alerts => {
                this.selectedalertss.push(alerts.id);
            });
        }
        else {
            /* this.selectedalertss.push(...
                 this.alertss.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedalertssChanged.next(this.selectedalertss);
    }





    deselectalertss() {
        this.selectedalertss = [];

        // Trigger the next event
        this.onSelectedalertssChanged.next(this.selectedalertss);
    }

    deletealerts(alerts) {
        this.db.rel.del('alerts', alerts)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const alertsIndex = this.alertss.indexOf(alerts);
                this.alertss.splice(alertsIndex, 1);
                this.onalertssChanged.next(this.alertss);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedalertss() {
        for (const alertsId of this.selectedalertss) {
            const alerts = this.alertss.find(_alerts => {
                return _alerts.id === alertsId;
            });

            this.db.rel.del('alerts', alerts)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const alertsIndex = this.alertss.indexOf(alerts);
                    this.alertss.splice(alertsIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onalertssChanged.next(this.alertss);
        this.deselectalertss();
    }

    /***********
    * configureanterecord
    **********/
    /**
     * Save a configureanterecord
     * @param {configureanterecord} Configureanterecord
     *
     * @return Promise<configureanterecord>
     */
    saveConfigureanterecord(configureanterecord: Configureanterecord): Promise<Configureanterecord> {
        //configureanterecord.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('configureanterecord', configureanterecord)
            .then((data: any) => {

                if (data && data.configureanterecords && data.configureanterecords
                [0]) {
                    console.log('save');

                    return data.configureanterecords[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a configureanterecord
    * @param {configureanterecord} configureanterecord
    *
    * @return Promise<configureanterecord>
    */
    updateConfigureanterecord(configureanterecord: Configureanterecord) {
        return this.db.rel.save('configureanterecord', configureanterecord)
            .then((data: any) => {
                if (data && data.configureanterecords && data.configureanterecords
                [0]) {
                    console.log('Update')

                    return data.configureanterecords[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a configureanterecord
     * @param {configureanterecord} configureanterecord
     *
     * @return Promise<boolean>
     */
    removeConfigureanterecord(configureanterecord: Configureanterecord): Promise<boolean> {
        return this.db.rel.del('configureanterecord', configureanterecord)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the configureanterecords
     *
     * @return Promise<Array<configureanterecord>>
     */
    getConfigureanterecords(): Promise<Array<Configureanterecord>> {
        return this.db.rel.find('configureanterecord')
            .then((data: any) => {
                this.configureanterecords = data.configureanterecords;
                if (this.searchText && this.searchText !== '') {
                    this.configureanterecords = FuseUtils.filterArrayByString(this.configureanterecords, this.searchText);
                }
                //this.onconfigureanterecordsChanged.next(this.configureanterecords);
                return Promise.resolve(this.configureanterecords);
                //return data.configureanterecords ? data.configureanterecords : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getDoctorConfigureanterecords(): Promise<Array<Configureanterecord>> {
        return this.db.rel.find('configureanterecord')
            .then((data: any) => {
                return data.configureanterecords ? data.configureanterecords : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a configureanterecord
     * @param {configureanterecord} configureanterecord
     *
     * @return Promise<configureanterecord>
     */
    getConfigureanterecord(configureanterecord: Configureanterecord): Promise<Configureanterecord> {
        return this.db.rel.find('configureanterecord', configureanterecord.id)
            .then((data: any) => {
                console.log("Get")

                return data && data.configureanterecords ? data.configureanterecords[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected configureanterecord by id
     * @param id
     */
    toggleSelectedConfigureanterecord(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedConfigureanterecords.length > 0) {
            const index = this.selectedConfigureanterecords.indexOf(id);

            if (index !== -1) {
                this.selectedConfigureanterecords.splice(index, 1);

                // Trigger the next event
                this.onSelectedConfigureanterecordsChanged.next(this.selectedConfigureanterecords);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedConfigureanterecords.push(id);

        // Trigger the next event
        this.onSelectedConfigureanterecordsChanged.next(this.selectedConfigureanterecords);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllAnte_records() {
        if (this.selectedConfigureanterecords.length > 0) {
            this.deselectConfigureanterecords();
        }
        else {
            this.selectConfigureanterecords();
        }
    }

    selectConfigureanterecords(filterParameter?, filterValue?) {
        this.selectedConfigureanterecords = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedConfigureanterecords = [];
            this.configureanterecords.map(configureanterecord => {
                this.selectedConfigureanterecords.push(configureanterecord.id);
            });
        }
        else {
            /* this.selectedconfigureanterecords.push(...
                 this.configureanterecords.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedConfigureanterecordsChanged.next(this.selectedConfigureanterecords);
    }





    deselectConfigureanterecords() {
        this.selectedConfigureanterecords = [];

        // Trigger the next event
        this.onSelectedConfigureanterecordsChanged.next(this.selectedConfigureanterecords);
    }

    deleteConfigureanterecord(configureanterecord) {
        this.db.rel.del('configureanterecord', configureanterecord)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const configureanterecordIndex = this.configureanterecords.indexOf(configureanterecord);
                this.configureanterecords.splice(configureanterecordIndex, 1);
                this.onConfigureanterecordsChanged.next(this.configureanterecords);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedConfigureanterecords() {
        for (const configureanterecordId of this.selectedConfigureanterecords) {
            const configureanterecord = this.configureanterecords.find(_configureanterecord => {
                return _configureanterecord.id === configureanterecordId;
            });

            this.db.rel.del('configureanterecord', configureanterecord)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const configureanterecordIndex = this.configureanterecords.indexOf(configureanterecord);
                    this.configureanterecords.splice(configureanterecordIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onConfigureanterecordsChanged.next(this.configureanterecords);
        this.deselectConfigureanterecords();
    }

    /**
  * Update a staff
  * @param {staff} staff
  *
  * @return Promise<staff>
  */
    updateStaff2(staff: Staff) {

        return this.db.rel.save('staff', staff)
            .then((data: any) => {
                if (data && data.staffs && data.staffs
                [0]) {
                    console.log(data.staffs[0])
                    return data.staffs[0]
                }
                return null;
            }).catch((err: any) => {
                console.error(err);
            });



    }

    /***********
    * anterecord
    **********/
    /**
    * Save a anterecord
    * @param {anterecord} anterecord
    *
    * @return Promise<anterecord>
    */
    saveanterecord(anterecord: anterecord, staff: Staff): Promise<anterecord> {
        //anterecord.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('anterecord', anterecord)
            .then((data: any) => {
                console.log(staff);
                this.updateStaff(staff);
                /*   if (this.file) {
                      this.file;
                      
                      this.retrieve = (<HTMLInputElement>this.file).files[0];
        
                      if (this.retrieve = (<HTMLInputElement>this.file).files[0]) {
                          staff._attachments = this.retrieve
                      }
        
                      else {
                          staff._attachments;
                          //this.getStaff = staff._attachments;
                      }
        
                      return this.db.rel.save('staff', staff)
                          .then((data: any) => {
                              if (data && data.staffs && data.staffs
                              [0]) {
                                   this.db.rel.putAttachment('staff', { id: staff.id, rev: staff.rev }, 'file', staff._attachments, 'image/png').then(res => {
                                      var reader: FileReader = new FileReader();
        
                                      reader.onloadend = function (e) {
                                          var base64data = reader.result;
                                          staff.image = base64data
                                          //console.log(staff.image);
                                      }
                                      reader.readAsDataURL(this.retrieve);
                                      staff._attachments = res;
                                      staff.image = res
        
                                  }) 
                                  
                                  return data.staffs[0]
                              }
        
                              return null;
                          }).catch((err: any) => {
                              console.error(err);
                          });
        
                  }
                  else {
                      
        
                      return this.db.rel.save('staff', staff)
                          .then((data: any) => {
                              if (data && data.staffs && data.staffs
                              [0]) {
                                   this.db.rel.putAttachment('staff', { id: staff.id, rev: staff.rev }, 'file', staff._attachments, 'image/png').then(res => {
                                      staff._attachments = res
                                  }) 
                                  
                                  return data.staffs[0]
                              }
        
                              return null;
                          }).catch((err: any) => {
                              console.error(err);
                          });
                  } */
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a anterecord
    * @param {anterecord} anterecord
    *
    * @return Promise<anterecord>
    */
    updateanterecord(anterecord: anterecord) {
        return this.db.rel.save('anterecord', anterecord)
            .then((data: any) => {
                if (data && data.anterecords && data.anterecords
                [0]) {
                    console.log('Update')

                    return data.anterecords[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Remove a anterecord
    * @param {anterecord} anterecord
    *
    * @return Promise<boolean>
    */
    removeanterecord(anterecord: anterecord): Promise<boolean> {
        return this.db.rel.del('anterecord', anterecord)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Return all the anterecords
    *
    * @return Promise<Array<anterecord>>
    */
    getanterecords(): Promise<Array<anterecord>> {
        return this.db.rel.find('anterecord')
            .then((data: any) => {
                this.anterecords = data.anterecords;
                if (this.searchText && this.searchText !== '') {
                    this.anterecords = FuseUtils.filterArrayByString(this.anterecords, this.searchText);
                }
                //this.onanterecordsChanged.next(this.anterecords);
                return Promise.resolve(this.anterecords);
                //return data.anterecords ? data.anterecords : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getDoctoranterecords(): Promise<Array<anterecord>> {
        return this.db.rel.find('anterecord')
            .then((data: any) => {
                return data.anterecords ? data.anterecords : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Read a anterecord
    * @param {anterecord} anterecord
    *
    * @return Promise<anterecord>
    */
    getanterecord(id): Promise<anterecord> {
        return this.db.rel.find('anterecord', id)
            .then((data: any) => {
                console.log("Get")

                return data && data.anterecords ? data.anterecords[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
    * Toggle selected anterecord by id
    * @param id
    */
    toggleSelectedanterecord(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedanterecords.length > 0) {
            const index = this.selectedanterecords.indexOf(id);

            if (index !== -1) {
                this.selectedanterecords.splice(index, 1);

                // Trigger the next event
                this.onSelectedanterecordsChanged.next(this.selectedanterecords);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedanterecords.push(id);

        // Trigger the next event
        this.onSelectedanterecordsChanged.next(this.selectedanterecords);
    }

    /**
    * Toggle select all
    */
    toggleSelectAllAnterecords() {
        if (this.selectedanterecords.length > 0) {
            this.deselectanterecords();
        }
        else {
            this.selectanterecords();
        }
    }

    selectanterecords(filterParameter?, filterValue?) {
        this.selectedanterecords = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedanterecords = [];
            this.anterecords.map(anterecord => {
                this.selectedanterecords.push(anterecord.id);
            });
        }
        else {
            /* this.selectedanterecords.push(...
                 this.anterecords.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedanterecordsChanged.next(this.selectedanterecords);
    }





    deselectanterecords() {
        this.selectedanterecords = [];

        // Trigger the next event
        this.onSelectedanterecordsChanged.next(this.selectedanterecords);
    }

    deleteanterecord(anterecord) {
        this.db.rel.del('anterecord', anterecord)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const anterecordIndex = this.anterecords.indexOf(anterecord);
                this.anterecords.splice(anterecordIndex, 1);
                this.onanterecordsChanged.next(this.anterecords);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedanterecords() {
        for (const anterecordId of this.selectedanterecords) {
            const anterecord = this.anterecords.find(_anterecord => {
                return _anterecord.id === anterecordId;
            });

            this.db.rel.del('anterecord', anterecord)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const anterecordIndex = this.anterecords.indexOf(anterecord);
                    this.anterecords.splice(anterecordIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onanterecordsChanged.next(this.anterecords);
        this.deselectanterecords();
    }

    /***********
        * appointment
        **********/
    /**
     * Save a appointment
     * @param {appointment} appointment
     *
     * @return Promise<appointment>
     */
    saveAppointment(appointment: Appointment): Promise<Appointment> {
        appointment.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('appointment', appointment)
            .then((data: any) => {
                if (data && data.appointments && data.appointments[0]) {
                    return data.appointments[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a appointment
    * @param {appointment} appointment
    *
    * @return Promise<appointment>
    */
    updateAppointment(appointment: Appointment) {
        return this.db.rel.save('appointment', appointment)
            .then((data: any) => {
                if (data && data.appointments && data.appointments
                [0]) {
                    return data.appointments[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a appointment
     * @param {appointment} Appointment
     *
     * @return Promise<boolean>
     */
    removeAppointment(appointment: Appointment): Promise<boolean> {
        return this.db.rel.del('appointment', appointment)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the appointments
     *
     * @return Promise<Array<appointment>>
     */
    getAppointments(): Promise<Array<Appointment>> {
        return this.db.rel.find('appointment')
            .then((data: any) => {
                this.appointments = data.appointments;
                if (this.searchText && this.searchText !== '') {
                    this.appointments = FuseUtils.filterArrayByString(this.appointments, this.searchText);
                }
                //this.onappointmentsChanged.next(this.appointments);
                return Promise.resolve(this.appointments);
                //return data.appointments ? data.appointments : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a appointment
     * @param {appointment} appointment
     *
     * @return Promise<appointment>
     */
    getAppointment(appointment: Appointment): Promise<Appointment> {
        return this.db.rel.find('appointment', appointment.id)
            .then((data: any) => {
                return data && data.appointments ? data.appointments[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected appointment by id
     * @param id
     */
    toggleSelectedAppointment(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedAppointments.length > 0) {
            const index = this.selectedAppointments.indexOf(id);

            if (index !== -1) {
                this.selectedAppointments.splice(index, 1);

                // Trigger the next event
                this.onSelectedAppointmentsChanged.next(this.selectedAppointments);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedAppointments.push(id);

        // Trigger the next event
        this.onSelectedAppointmentsChanged.next(this.selectedAppointments);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllAppointment2() {
        if (this.selectedAppointments.length > 0) {
            this.deselectAppointments();
        }
        else {
            this.selectAppointments();
        }
    }

    selectAppointments(filterParameter?, filterValue?) {
        this.selectedAppointments = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedAppointments = [];
            this.appointments.map(appointment => {
                this.selectedAppointments.push(appointment.id);
            });
        }
        else {
            /* this.selectedappointments.push(...
                 this.appointments.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedAppointmentsChanged.next(this.selectedAppointments);
    }





    deselectAppointments() {
        this.selectedAppointments = [];

        // Trigger the next event
        this.onSelectedAppointmentsChanged.next(this.selectedAppointments);
    }

    deleteAppointment(appointment) {
        this.db.rel.del('appointment', appointment)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const appointmentIndex = this.appointments.indexOf(appointment);
                this.appointments.splice(appointmentIndex, 1);
                this.onAppointmentsChanged.next(this.appointments);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedAppointments() {
        for (const appointmentId of this.selectedAppointments) {
            const appointment = this.appointments.find(_appointment => {
                return _appointment.id === appointmentId;
            });

            this.db.rel.del('appointment', appointment)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const appointmentIndex = this.appointments.indexOf(appointment);
                    this.appointments.splice(appointmentIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onAppointmentsChanged.next(this.appointments);
        this.deselectAppointments();
    }

    /***********
    * bed_allotment
    **********/
    /**
     * Save a bed_allotment
     * @param {bed_allotment} Bed_allotment
     *
     * @return Promise<bed_allotment>
     */
    saveBed_allotment(bed_allotment: Bed_allotment): Promise<Bed_allotment> {
        bed_allotment.id = Math.floor(Date.now()).toString();
        return this.db.rel.save('bed_allotment', bed_allotment)
            .then((data: any) => {
                if (data && data.bed_allotments && data.bed_allotments
                [0]) {

                    return data.bed_allotments[0]

                };

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
    * Update a bed_allotment
    * @param {bed_allotment} Bed_allotment
    *
    * @return Promise<Bed_allotment>
    */
    updateBed_allotment(bed_allotment: Bed_allotment) {
        return this.db.rel.save('bed_allotment', bed_allotment)
            .then((data: any) => {
                if (data && data.bed_allotments && data.bed_allotments
                [0]) {
                    return data.bed_allotments[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a bed_allotment
     * @param {bed_allotment} Bed_allotment
     *
     * @return Promise<boolean>
     */
    removeBed_allotment(bed_allotment: Bed_allotment): Promise<boolean> {
        return this.db.rel.del('bed_allotment', bed_allotment)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the bed_allotments
     *
     * @return Promise<Array<bed_allotment>>
     */
    getBed_allotments(): Promise<Array<Bed_allotment>> {
        var localStorageItem = JSON.parse(localStorage.getItem('user'));
        return this.db.rel.find('bed_allotment')
            .then((data: any) => {
                this.bed_allotments = data.bed_allotments;
                if (this.searchText && this.searchText !== '') {
                    this.bed_allotments = FuseUtils.filterArrayByString(this.bed_allotments, this.searchText);
                }
                //this.onbed_allotmentsChanged.next(this.bed_allotments);
                var userId = localStorageItem.id
                if (localStorageItem.usertype == "Patient") {
                    this.bed_allotments = this.bed_allotments.filter(data => data.pidentity == userId)
                }
                return Promise.resolve(this.bed_allotments);
                //return data.bed_allotments ? data.bed_allotments : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a bed_allotment
     * @param {bed_allotment} bed_allotment
     *
     * @return Promise<bed_allotment>
     */
    getBed_allotment(bed_allotment: Bed_allotment): Promise<Bed_allotment> {
        return this.db.rel.find('bed_allotment', bed_allotment.id)
            .then((data: any) => {
                return data && data.bed_allotments ? data.bed_allotments[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected bed_allotment by id
     * @param id
     */
    toggleSelectedBed_allotment(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedBed_allotments.length > 0) {
            const index = this.selectedBed_allotments.indexOf(id);

            if (index !== -1) {
                this.selectedBed_allotments.splice(index, 1);

                // Trigger the next event
                this.onSelectedBed_allotmentsChanged.next(this.selectedBed_allotments);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedBed_allotments.push(id);

        // Trigger the next event
        this.onSelectedBed_allotmentsChanged.next(this.selectedBed_allotments);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllBed_allotmnets() {
        if (this.selectedBed_allotments.length > 0) {
            this.deselectBed_allotments();
        }
        else {
            this.selectBed_allotments();
        }
    }

    selectBed_allotments(filterParameter?, filterValue?) {
        this.selectedBed_allotments = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedBed_allotments = [];
            this.bed_allotments.map(bed_allotment => {
                this.selectedBed_allotments.push(bed_allotment.id);
            });
        }
        else {
            /* this.selectedbed_allotments.push(...
                 this.bed_allotments.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedBed_allotmentsChanged.next(this.selectedBed_allotments);
    }





    deselectBed_allotments() {
        this.selectedBed_allotments = [];

        // Trigger the next event
        this.onSelectedBed_allotmentsChanged.next(this.selectedBed_allotments);
    }

    deleteBed_allotment(bed_allotment) {
        this.db.rel.del('bed_allotment', bed_allotment)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const bed_allotmentIndex = this.bed_allotments.indexOf(bed_allotment);
                this.bed_allotments.splice(bed_allotmentIndex, 1);
                this.onBed_allotmentsChanged.next(this.bed_allotments);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedBed_allotments() {
        for (const bed_allotmentId of this.selectedBed_allotments) {
            const bed_allotment = this.bed_allotments.find(_bed_allotment => {
                return _bed_allotment.id === bed_allotmentId;
            });

            this.db.rel.del('bed_allotment', bed_allotment)
                .then((data: any) => {
                    this.getBed2(bed_allotment.bed_number).then(data => {
                        data.status = "Available"
                        this.updateBed(data);
                    })
                    //return data && data.deleted ? data.deleted: false;
                    const bed_allotmentIndex = this.bed_allotments.indexOf(bed_allotment);
                    this.bed_allotments.splice(bed_allotmentIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onBed_allotmentsChanged.next(this.bed_allotments);
        this.deselectBed_allotments();
    }

    /***********
    * bed
    **********/
    /**
     * Save a bed
     * @param {bed} Bed
     *
     * @return Promise<bed>
     */
    saveBed(bed: Bed): Promise<Bed> {
        bed.id = Math.floor(Date.now()).toString();
        bed.bed_allotments = [];
        //bed.driverslicenses = [];
        return this.db.rel.save('bed', bed)
            .then((data: any) => {
                //console.log(data);
                //console.log(bed);
                if (data && data.beds && data.beds
                [0]) {
                    return data.beds[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a bed
    * @param {bed} Bed
    *
    * @return Promise<bed>
    */
    updateBed(bed: Bed) {
        return this.db.rel.save('bed', bed)
            .then((data: any) => {
                if (data && data.beds && data.beds
                [0]) {
                    return data.beds[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a bed
     * @param {bed} bed
     *
     * @return Promise<boolean>
     */
    removeBed(bed: Bed): Promise<boolean> {
        return this.db.rel.del('bed', bed)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the beds
     *
     * @return Promise<Array<bed>>
     */
    getBeds(): Promise<Array<Bed>> {
        return this.db.rel.find('bed')
            .then((data: any) => {
                this.beds = data.beds;
                if (this.searchText && this.searchText !== '') {
                    this.beds = FuseUtils.filterArrayByString(this.beds, this.searchText);
                }
                //this.onbedsChanged.next(this.beds);
                return Promise.resolve(this.beds);
                //return data.beds ? data.beds : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getB_AllotBeds(): Promise<Array<Bed>> {
        return this.db.rel.find('bed')
            .then((data: any) => {
                return data.beds ? data.beds : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a bed
     * @param {bed} bed
     *
     * @return Promise<bed>
     */
    getBed(bed: Bed): Promise<Bed> {
        return this.db.rel.find('bed', bed.id)
            .then((data: any) => {
                return data && data.beds ? data.beds[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getBed2(id): Promise<Bed> {
        return this.db.rel.find('bed', id)
            .then((data: any) => {
                return data && data.beds ? data.beds[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected bed by id
     * @param id
     */
    toggleSelectedBed(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedBeds.length > 0) {
            const index = this.selectedBeds.indexOf(id);

            if (index !== -1) {
                this.selectedBeds.splice(index, 1);

                // Trigger the next event
                this.onSelectedBedsChanged.next(this.selectedBeds);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedBeds.push(id);

        // Trigger the next event
        this.onSelectedBedsChanged.next(this.selectedBeds);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllBeds() {
        if (this.selectedBeds.length > 0) {
            this.deselectBeds();
        }
        else {
            this.selectBeds();
        }
    }

    selectBeds(filterParameter?, filterValue?) {
        this.selectedBeds = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedBeds = [];
            this.beds.map(bed => {
                this.selectedBeds.push(bed.id);
            });
        }
        else {
            /* this.selectedbeds.push(...
                 this.beds.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedBedsChanged.next(this.selectedBeds);
    }





    deselectBeds() {
        this.selectedBeds = [];

        // Trigger the next event
        this.onSelectedBedsChanged.next(this.selectedBeds);
    }

    deleteBed(bed) {
        this.db.rel.del('bed', bed)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const bedIndex = this.beds.indexOf(bed);
                this.beds.splice(bedIndex, 1);
                this.onBedsChanged.next(this.beds);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedBeds() {
        for (const bedId of this.selectedBeds) {
            const bed = this.beds.find(_bed => {
                return _bed.id === bedId;
            });

            this.db.rel.del('bed', bed)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const bedIndex = this.beds.indexOf(bed);
                    this.beds.splice(bedIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onBedsChanged.next(this.beds);
        this.deselectBeds();
    }

    /***********
     * blood_bank
     **********/
    /**
     * Save a blood_bank
     * @param {blood_bank} blood_bank
     *
     * @return Promise<blood_bank>
     */
    saveBlood_bank(blood_bank: Blood_bank): Promise<Blood_bank> {
        blood_bank.id = Math.floor(Date.now()).toString();
        blood_bank.patients = [],
            blood_bank.blood_donors = []

        return this.db.rel.save('blood_bank', blood_bank)
            .then((data: any) => {
                if (data && data.blood_banks && data.blood_banks
                [0]) {
                    return data.blood_banks[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a blood_bank
    * @param {blood_bank} Blood_bank
    *
    * @return Promise<blood_bank>
    */
    updateBlood_bank(blood_bank: Blood_bank) {
        return this.db.rel.save('blood_bank', blood_bank)
            .then((data: any) => {
                if (data && data.blood_banks && data.blood_banks
                [0]) {
                    return data.blood_banks[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a blood_bank
     * @param {blood_bank} Blood_bank
     *
     * @return Promise<boolean>
     */
    removeBlood_bank(blood_bank: Blood_bank): Promise<boolean> {
        return this.db.rel.del('blood_bank', blood_bank)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the blood_banks
     *
     * @return Promise<Array<blood_bank>>
     */
    getBlood_banks(): Promise<Array<Blood_bank>> {
        return this.db.rel.find('blood_bank')
            .then((data: any) => {
                this.blood_banks = data.blood_banks;
                if (this.searchText && this.searchText !== '') {
                    this.blood_banks = FuseUtils.filterArrayByString(this.blood_banks, this.searchText);
                }
                //this.onblood_banksChanged.next(this.blood_banks);
                return Promise.resolve(this.blood_banks);
                //return data.blood_banks ? data.blood_banks : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getPatientBlood_banks(): Promise<Array<Blood_bank>> {
        return this.db.rel.find('blood_bank')
            .then((data: any) => {
                return data.blood_banks ? data.blood_banks : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a blood_bank
     * @param {blood_bank} blood_bank
     *
     * @return Promise<blood_bank>
     */
    getBlood_bank(blood_bank: Blood_bank): Promise<Blood_bank> {
        return this.db.rel.find('blood_bank', blood_bank.id)
            .then((data: any) => {
                return data && data.blood_banks ? data.blood_banks[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getBlood_bank2(id): Promise<Blood_bank> {
        return this.db.rel.find('blood_bank', id)
            .then((data: any) => {
                return data && data.blood_banks ? data.blood_banks[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected blood_bank by id
     * @param id
     */
    toggleSelectedBlood_bank(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedBlood_banks.length > 0) {
            const index = this.selectedBlood_banks.indexOf(id);

            if (index !== -1) {
                this.selectedBlood_banks.splice(index, 1);

                // Trigger the next event
                this.onSelectedBlood_banksChanged.next(this.selectedBlood_banks);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedBlood_banks.push(id);

        // Trigger the next event
        this.onSelectedBlood_banksChanged.next(this.selectedBlood_banks);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllBlood_bank() {
        if (this.selectedBlood_banks.length > 0) {
            this.deselectBlood_banks();
        }
        else {
            this.selectBlood_banks();
        }
    }

    selectBlood_banks(filterParameter?, filterValue?) {
        this.selectedBlood_banks = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedBlood_banks = [];
            this.blood_banks.map(blood_bank => {
                this.selectedBlood_banks.push(blood_bank.id);
            });
        }
        else {
            /* this.selectedblood_banks.push(...
                 this.blood_banks.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedBlood_banksChanged.next(this.selectedBlood_banks);
    }





    deselectBlood_banks() {
        this.selectedBlood_banks = [];

        // Trigger the next event
        this.onSelectedBlood_banksChanged.next(this.selectedBlood_banks);
    }

    deleteBlood_bank(blood_bank) {
        this.db.rel.del('blood_bank', blood_bank)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const blood_bankIndex = this.blood_banks.indexOf(blood_bank);
                this.blood_banks.splice(blood_bankIndex, 1);
                this.onBlood_banksChanged.next(this.blood_banks);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedBlood_banks() {
        for (const blood_bankId of this.selectedBlood_banks) {
            const blood_bank = this.blood_banks.find(_blood_bank => {
                return _blood_bank.id === blood_bankId;
            });

            this.db.rel.del('blood_bank', blood_bank)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const blood_bankIndex = this.blood_banks.indexOf(blood_bank);
                    this.blood_banks.splice(blood_bankIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onBlood_banksChanged.next(this.blood_banks);
        this.deselectBlood_banks();
    }

    //BLOOD_DONOR
    /***********
     *blood_donor
     **********/
    /**
     * Save a blood_donor
     * @param {blood_donor} Blood_donor
     *
     * @return Promise<blood_donor>
     */
    saveBlood_donor(blood_donor: Blood_donor): Promise<Blood_donor> {
        blood_donor.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('blood_donor', blood_donor)
            .then((data: any) => {

                if (data && data.blood_donors && data.blood_donors[0]) {

                    return data.blood_donors[0]

                }


                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
    * Update a blood_donor
    * @param {blood_donor} Blood_donor
    *
    * @return Promise<blood_donor>
    */
    updateBlood_donor(blood_donor: Blood_donor) {

        return this.db.rel.save('blood_donor', blood_donor)
            .then((data: any) => {
                if (data && data.blood_donors && data.blood_donors
                [0]) {
                    return data.blood_donors[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a blood_donor
     * @param {blood_donor} Blood_donor
     *
     * @return Promise<boolean>
     */
    removeBlood_donor(blood_donor: Blood_donor): Promise<boolean> {
        return this.db.rel.del('blood_donor', blood_donor)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the blood_donors
     *
     * @return Promise<Array<blood_donor>>
     */
    getBlood_donors(): Promise<Array<Blood_donor>> {
        return this.db.rel.find('blood_donor')
            .then((data: any) => {
                this.blood_donors = data.blood_donors;
                if (this.searchText && this.searchText !== '') {
                    this.blood_donors = FuseUtils.filterArrayByString(this.blood_donors, this.searchText);
                }
                return Promise.resolve(this.blood_donors);
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a blood_donor
     * @param {blood_donor} blood_donor
     *
     * @return Promise<blood_donor>
     */
    getBlood_donor(blood_donor: Blood_donor): Promise<Blood_donor> {
        return this.db.rel.find('blood_donor', blood_donor.id)
            .then((data: any) => {
                return data && data.blood_donors ? data.blood_donors[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getBlood_donor2(id): Promise<Blood_donor> {
        return this.db.rel.find('blood_donor', id)
            .then((data: any) => {
                return data && data.blood_donors ? data.blood_donors[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Toggle selected blood_donor by id
     * @param id
     */
    toggleSelectedBlood_donor(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedBlood_donors.length > 0) {
            const index = this.selectedBlood_donors.indexOf(id);

            if (index !== -1) {
                this.selectedBlood_donors.splice(index, 1);

                // Trigger the next event
                this.onSelectedBlood_donorsChanged.next(this.selectedBlood_donors);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedBlood_donors.push(id);

        // Trigger the next event
        this.onSelectedBlood_donorsChanged.next(this.selectedBlood_donors);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllBlood_donor() {
        if (this.selectedBlood_donors.length > 0) {
            this.deselectBlood_donors();
        }
        else {
            this.selectBlood_donors();
        }
    }

    selectBlood_donors(filterParameter?, filterValue?) {
        this.selectedBlood_donors = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedBlood_donors = [];
            this.blood_donors.map(blood_donor => {
                this.selectedBlood_donors.push(blood_donor.id);
            });
        }
        else {
            /* this.selecteddriverlicenses.push(...
                 this.driverlicenses.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedBlood_donorsChanged.next(this.selectedBlood_donors);
    }





    deselectBlood_donors() {
        this.selectedBlood_donors = [];

        // Trigger the next event
        this.onSelectedBlood_donorsChanged.next(this.selectedBlood_donors);
    }

    deleteBlood_donor(blood_donor) {
        this.db.rel.del('blood_donor', blood_donor)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const blood_donorIndex = this.blood_donors.indexOf(blood_donor);
                this.blood_donors.splice(blood_donorIndex, 1);
                this.onBlood_donorsChanged.next(this.blood_donors);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedBlood_donors() {
        for (const blood_donorId of this.selectedBlood_donors) {
            const blood_donor = this.blood_donors.find(_blood_donor => {
                return _blood_donor.id === blood_donorId;
            });

            this.db.rel.del('blood_donor', blood_donor)
                .then((data: any) => {
                    this.getBlood_bank2(blood_donor.blood_bank).then(data => {
                        data.status = data.status - blood_donor.blood_donated;
                        this.updateBlood_bank(data);
                    })
                    //return data && data.deleted ? data.deleted: false;
                    const blood_donorIndex = this.blood_donors.indexOf(blood_donor);
                    this.blood_donors.splice(blood_donorIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onBlood_donorsChanged.next(this.blood_donors);
        this.deselectBlood_donors();
    }

    /***********
    * blood_price
    **********/
    /**
     * Save a blood_price
     * @param {blood_price} blood_price
     *
     * @return Promise<blood_price>
     */
    saveBlood_price(blood_price: Blood_price): Promise<Blood_price> {
        blood_price.id = Math.floor(Date.now()).toString();
        blood_price.blood_sales = []

        return this.db.rel.save('blood_price', blood_price)
            .then((data: any) => {
                if (data && data.blood_prices && data.blood_prices
                [0]) {
                    return data.blood_prices[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a blood_price
    * @param {blood_price} Blood_price
    *
    * @return Promise<blood_price>
    */
    updateBlood_price(blood_price: Blood_price) {
        return this.db.rel.save('blood_price', blood_price)
            .then((data: any) => {
                if (data && data.blood_prices && data.blood_prices
                [0]) {
                    return data.blood_prices[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a blood_price
     * @param {blood_price} Blood_price
     *
     * @return Promise<boolean>
     */
    removeBlood_price(blood_price: Blood_price): Promise<boolean> {
        return this.db.rel.del('blood_price', blood_price)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the blood_prices
     *
     * @return Promise<Array<blood_price>>
     */
    getBlood_prices(): Promise<Array<Blood_price>> {
        return this.db.rel.find('blood_price')
            .then((data: any) => {
                this.blood_prices = data.blood_prices;
                if (this.searchText && this.searchText !== '') {
                    this.blood_prices = FuseUtils.filterArrayByString(this.blood_prices, this.searchText);
                }
                //this.onblood_pricesChanged.next(this.blood_prices);
                return Promise.resolve(this.blood_prices);
                //return data.blood_prices ? data.blood_prices : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getPatientBlood_prices(): Promise<Array<Blood_price>> {
        return this.db.rel.find('blood_price')
            .then((data: any) => {
                return data.blood_prices ? data.blood_prices : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a blood_price
     * @param {blood_price} blood_price
     *
     * @return Promise<blood_price>
     */
    getBlood_price(blood_price: Blood_price): Promise<Blood_price> {
        return this.db.rel.find('blood_price', blood_price.id)
            .then((data: any) => {
                return data && data.blood_prices ? data.blood_prices[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getBlood_price2(id): Promise<Blood_price> {
        return this.db.rel.find('blood_price', id)
            .then((data: any) => {
                return data && data.blood_prices ? data.blood_prices[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected blood_price by id
     * @param id
     */
    toggleSelectedBlood_price(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedBlood_prices.length > 0) {
            const index = this.selectedBlood_prices.indexOf(id);

            if (index !== -1) {
                this.selectedBlood_prices.splice(index, 1);

                // Trigger the next event
                this.onSelectedBlood_pricesChanged.next(this.selectedBlood_prices);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedBlood_prices.push(id);

        // Trigger the next event
        this.onSelectedBlood_pricesChanged.next(this.selectedBlood_prices);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllBlood_price() {
        if (this.selectedBlood_prices.length > 0) {
            this.deselectBlood_prices();
        }
        else {
            this.selectBlood_prices();
        }
    }

    selectBlood_prices(filterParameter?, filterValue?) {
        this.selectedBlood_prices = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedBlood_prices = [];
            this.blood_prices.map(blood_price => {
                this.selectedBlood_prices.push(blood_price.id);
            });
        }
        else {
            /* this.selectedblood_prices.push(...
                 this.blood_prices.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedBlood_pricesChanged.next(this.selectedBlood_prices);
    }





    deselectBlood_prices() {
        this.selectedBlood_prices = [];

        // Trigger the next event
        this.onSelectedBlood_pricesChanged.next(this.selectedBlood_prices);
    }

    deleteBlood_price(blood_price) {
        this.db.rel.del('blood_price', blood_price)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const blood_priceIndex = this.blood_prices.indexOf(blood_price);
                this.blood_prices.splice(blood_priceIndex, 1);
                this.onBlood_pricesChanged.next(this.blood_prices);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedblood_prices() {
        for (const blood_priceId of this.selectedBlood_prices) {
            const blood_price = this.blood_prices.find(_blood_price => {
                return _blood_price.id === blood_priceId;
            });

            this.db.rel.del('blood_price', blood_price)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const blood_priceIndex = this.blood_prices.indexOf(blood_price);
                    this.blood_prices.splice(blood_priceIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onBlood_pricesChanged.next(this.blood_prices);
        this.deselectBlood_prices();
    }

    /***********
    *blood_sale
    **********/
    /**
     * Save a blood_sale
     * @param {blood_sale} blood_sale
     *
     * @return Promise<blood_sale>
     */
    saveBlood_sale(blood_sale: Blood_sale): Promise<Blood_sale> {
        blood_sale.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('blood_sale', blood_sale)
            .then((data: any) => {

                if (data && data.blood_sales && data.blood_sales[0]) {

                    return data.blood_sales[0]

                }


                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
    * Update a blood_sale
    * @param {blood_sale} blood_sale
    *
    * @return Promise<blood_sale>
    */
    updateBlood_sale(blood_sale: Blood_sale) {

        return this.db.rel.save('blood_sale', blood_sale)
            .then((data: any) => {
                if (data && data.blood_sales && data.blood_sales
                [0]) {
                    return data.blood_sales[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a blood_sale
     * @param {blood_sale} Blood_sale
     *
     * @return Promise<boolean>
     */
    removeBlood_sale(blood_sale: Blood_sale): Promise<boolean> {
        return this.db.rel.del('blood_sale', blood_sale)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the blood_sales
     *
     * @return Promise<Array<blood_sale>>
     */
    getBlood_sales(): Promise<Array<Blood_sale>> {
        return this.db.rel.find('blood_sale')
            .then((data: any) => {
                this.blood_sales = data.blood_sales;
                if (this.searchText && this.searchText !== '') {
                    this.blood_sales = FuseUtils.filterArrayByString(this.blood_sales, this.searchText);
                }
                return Promise.resolve(this.blood_sales);
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a blood_sale
     * @param {blood_sale} blood_sale
     *
     * @return Promise<blood_sale>
     */
    getBlood_sale(blood_sale: Blood_sale): Promise<Blood_sale> {
        return this.db.rel.find('blood_sale', blood_sale.id)
            .then((data: any) => {
                return data && data.blood_sales ? data.blood_sales[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getBlood_sale2(id): Promise<Blood_sale> {
        return this.db.rel.find('blood_sale', id)
            .then((data: any) => {
                return data && data.blood_sales ? data.blood_sales[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Toggle selected blood_sale by id
     * @param id
     */
    toggleSelectedBlood_sale(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedBlood_sales.length > 0) {
            const index = this.selectedBlood_sales.indexOf(id);

            if (index !== -1) {
                this.selectedBlood_sales.splice(index, 1);

                // Trigger the next event
                this.onSelectedBlood_salesChanged.next(this.selectedBlood_sales);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedBlood_sales.push(id);

        // Trigger the next event
        this.onSelectedBlood_salesChanged.next(this.selectedBlood_sales);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllBlood_sale() {
        if (this.selectedBlood_sales.length > 0) {
            this.deselectBlood_sales();
        }
        else {
            this.selectBlood_sales();
        }
    }

    selectBlood_sales(filterParameter?, filterValue?) {
        this.selectedBlood_sales = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedBlood_sales = [];
            this.blood_sales.map(blood_sale => {
                this.selectedBlood_sales.push(blood_sale.id);
            });
        }
        else {
            /* this.selecteddriverlicenses.push(...
                 this.driverlicenses.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedBlood_salesChanged.next(this.selectedBlood_sales);
    }





    deselectBlood_sales() {
        this.selectedBlood_sales = [];

        // Trigger the next event
        this.onSelectedBlood_salesChanged.next(this.selectedBlood_sales);
    }

    deleteBlood_sale(blood_sale) {
        this.db.rel.del('blood_sale', blood_sale)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const blood_saleIndex = this.blood_sales.indexOf(blood_sale);
                this.blood_sales.splice(blood_saleIndex, 1);
                this.onBlood_salesChanged.next(this.blood_sales);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedBlood_sales() {
        for (const blood_saleId of this.selectedBlood_sales) {
            const blood_sale = this.blood_sales.find(_blood_sale => {
                return _blood_sale.id === blood_saleId;
            });

            this.db.rel.del('blood_sale', blood_sale)
                .then((data: any) => {
                    this.getBlood_bank2(blood_sale.blood_groupId).then(data => {
                        data.status = data.status - (-blood_sale.amount_blood);
                        this.updateBlood_bank(data);
                    })
                    //return data && data.deleted ? data.deleted: false;
                    const blood_saleIndex = this.blood_sales.indexOf(blood_sale);
                    this.blood_sales.splice(blood_saleIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onBlood_salesChanged.next(this.blood_sales);
        this.deselectBlood_sales();
    }

    /***********
   * configurerecord
   **********/
    /**
     * Save a configurerecord
     * @param {configurerecord} Configurerecord
     *
     * @return Promise<configurerecord>
     */
    saveConfigurerecord(configurerecord: Configurerecord): Promise<Configurerecord> {
        //configurerecord.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('configurerecord', configurerecord)
            .then((data: any) => {

                if (data && data.configurerecords && data.configurerecords
                [0]) {
                    console.log('save');

                    return data.configurerecords[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a configurerecord
    * @param {configurerecord} configurerecord
    *
    * @return Promise<configurerecord>
    */
    updateConfigurerecord(configurerecord: Configurerecord) {
        return this.db.rel.save('configurerecord', configurerecord)
            .then((data: any) => {
                if (data && data.configurerecords && data.configurerecords
                [0]) {
                    console.log('Update')

                    return data.configurerecords[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a configurerecord
     * @param {configurerecord} configurerecord
     *
     * @return Promise<boolean>
     */
    removeConfigurerecord(configurerecord: Configurerecord): Promise<boolean> {
        return this.db.rel.del('configurerecord', configurerecord)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the configurerecords
     *
     * @return Promise<Array<configurerecord>>
     */
    getConfigurerecords(): Promise<Array<Configurerecord>> {
        return this.db.rel.find('configurerecord')
            .then((data: any) => {
                this.configurerecords = data.configurerecords;
                if (this.searchText && this.searchText !== '') {
                    this.configurerecords = FuseUtils.filterArrayByString(this.configurerecords, this.searchText);
                }
                //this.onconfigurerecordsChanged.next(this.configurerecords);
                return Promise.resolve(this.configurerecords);
                //return data.configurerecords ? data.configurerecords : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getDoctorConfigurerecords(): Promise<Array<Configurerecord>> {
        return this.db.rel.find('configurerecord')
            .then((data: any) => {
                return data.configurerecords ? data.configurerecords : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a configurerecord
     * @param {configurerecord} configurerecord
     *
     * @return Promise<configurerecord>
     */
    getConfigurerecord(configurerecord: Configurerecord): Promise<Configurerecord> {
        return this.db.rel.find('configurerecord', configurerecord.id)
            .then((data: any) => {
                console.log("Get")

                return data && data.configurerecords ? data.configurerecords[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected configurerecord by id
     * @param id
     */
    toggleSelectedConfigurerecord(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedConfigurerecords.length > 0) {
            const index = this.selectedConfigurerecords.indexOf(id);

            if (index !== -1) {
                this.selectedConfigurerecords.splice(index, 1);

                // Trigger the next event
                this.onSelectedConfigurerecordsChanged.next(this.selectedConfigurerecords);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedConfigurerecords.push(id);

        // Trigger the next event
        this.onSelectedConfigurerecordsChanged.next(this.selectedConfigurerecords);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllConfigure_record() {
        if (this.selectedConfigurerecords.length > 0) {
            this.deselectConfigurerecords();
        }
        else {
            this.selectConfigurerecords();
        }
    }

    selectConfigurerecords(filterParameter?, filterValue?) {
        this.selectedConfigurerecords = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedConfigurerecords = [];
            this.configurerecords.map(configurerecord => {
                this.selectedConfigurerecords.push(configurerecord.id);
            });
        }
        else {
            /* this.selectedconfigurerecords.push(...
                 this.configurerecords.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedConfigurerecordsChanged.next(this.selectedConfigurerecords);
    }





    deselectConfigurerecords() {
        this.selectedConfigurerecords = [];

        // Trigger the next event
        this.onSelectedConfigurerecordsChanged.next(this.selectedConfigurerecords);
    }

    deleteConfigurerecord(configurerecord) {
        this.db.rel.del('configurerecord', configurerecord)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const configurerecordIndex = this.configurerecords.indexOf(configurerecord);
                this.configurerecords.splice(configurerecordIndex, 1);
                this.onConfigurerecordsChanged.next(this.configurerecords);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedConfigurerecords() {
        for (const configurerecordId of this.selectedConfigurerecords) {
            const configurerecord = this.configurerecords.find(_configurerecord => {
                return _configurerecord.id === configurerecordId;
            });

            this.db.rel.del('configurerecord', configurerecord)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const configurerecordIndex = this.configurerecords.indexOf(configurerecord);
                    this.configurerecords.splice(configurerecordIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onConfigurerecordsChanged.next(this.configurerecords);
        this.deselectConfigurerecords();
    }

    /***********
    * consultancy
    **********/
    /**
     * Save a consultancy
     * @param {consultancy} consultancy
     *
     * @return Promise<consultancy>
     */
    saveConsultancy(consultancy: Consultancy): Promise<Consultancy> {
        consultancy.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('consultancy', consultancy)
            .then((data: any) => {

                if (data && data.consultancys && data.consultancys
                [0]) {
                    console.log('save');

                    return data.consultancys[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a consultancy
    * @param {consultancy} Consultancy
    *
    * @return Promise<consultancy>
    */
    updateConsultancy(consultancy: Consultancy) {
        return this.db.rel.save('consultancy', consultancy)
            .then((data: any) => {
                if (data && data.consultancys && data.consultancys
                [0]) {
                    console.log('Update')

                    return data.consultancys[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a consultancy
     * @param {consultancy} consultancy
     *
     * @return Promise<boolean>
     */
    removeConsultancy(consultancy: Consultancy): Promise<boolean> {
        return this.db.rel.del('consultancy', consultancy)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the consultancys
     *
     * @return Promise<Array<consultancy>>
     */
    getConsultancys(): Promise<Array<Consultancy>> {
        return this.db.rel.find('consultancy')
            .then((data: any) => {
                this.consultancys = data.consultancys;
                if (this.searchText && this.searchText !== '') {
                    this.consultancys = FuseUtils.filterArrayByString(this.consultancys, this.searchText);
                }
                //this.onconsultancysChanged.next(this.consultancys);
                return Promise.resolve(this.consultancys);
                //return data.consultancys ? data.consultancys : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getConsultancys2(): Promise<Array<Consultancy>> {
        return this.db.rel.find('consultancy')
            .then((data: any) => {
                this.consultancys = data.consultancys;
                if (this.searchText && this.searchText !== '') {
                    this.consultancys = FuseUtils.filterArrayByString(this.consultancys, this.searchText);
                }
                //this.onconsultancysChanged.next(this.consultancys);
                this.consultancys = this.consultancys.filter(data => data.patientId == this.patientId)
                return Promise.resolve(this.consultancys);
                //return data.consultancys ? data.consultancys : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getDoctorConsultancys(): Promise<Array<Consultancy>> {
        return this.db.rel.find('consultancy')
            .then((data: any) => {
                return data.consultancys ? data.consultancys : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a consultancy
     * @param {consultancy} consultancy
     *
     * @return Promise<consultancy>
     */
    getConsultancy(consultancy: Consultancy): Promise<Consultancy> {
        return this.db.rel.find('consultancy', consultancy.id)
            .then((data: any) => {
                console.log("Get")

                return data && data.consultancys ? data.consultancys[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected consultancy by id
     * @param id
     */
    toggleSelectedConsultancy(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedConsultancys.length > 0) {
            const index = this.selectedConsultancys.indexOf(id);

            if (index !== -1) {
                this.selectedConsultancys.splice(index, 1);

                // Trigger the next event
                this.onSelectedConsultancysChanged.next(this.selectedConsultancys);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedConsultancys.push(id);

        // Trigger the next event
        this.onSelectedConsultancysChanged.next(this.selectedConsultancys);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllConsultancy() {
        if (this.selectedConsultancys.length > 0) {
            this.deselectConsultancys();
        }
        else {
            this.selectConsultancys();
        }
    }

    selectConsultancys(filterParameter?, filterValue?) {
        this.selectedConsultancys = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedConsultancys = [];
            this.consultancys.map(consultancy => {
                this.selectedConsultancys.push(consultancy.id);
            });
        }
        else {
            /* this.selectedconsultancys.push(...
                 this.consultancys.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedConsultancysChanged.next(this.selectedConsultancys);
    }





    deselectConsultancys() {
        this.selectedConsultancys = [];

        // Trigger the next event
        this.onSelectedConsultancysChanged.next(this.selectedConsultancys);
    }

    deleteConsultancy(consultancy) {
        this.db.rel.del('consultancy', consultancy)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const consultancyIndex = this.consultancys.indexOf(consultancy);
                this.consultancys.splice(consultancyIndex, 1);
                this.onConsultancysChanged.next(this.consultancys);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedConsultancys() {
        for (const consultancyId of this.selectedConsultancys) {
            const consultancy = this.consultancys.find(_consultancy => {
                return _consultancy.id === consultancyId;
            });

            this.db.rel.del('consultancy', consultancy)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const consultancyIndex = this.consultancys.indexOf(consultancy);
                    this.consultancys.splice(consultancyIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onConsultancysChanged.next(this.consultancys);
        this.deselectConsultancys();
    }

    /***********
    * currency
    **********/
    /**
     * Save a currency
     * @param {currency} Currency
     *
     * @return Promise<currency>
     */
    saveCurrency(currency: Currency): Promise<Currency> {
        currency.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('currency', currency)
            .then((data: any) => {

                if (data && data.currencys && data.currencys
                [0]) {
                    return data.currencys[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a currency
    * @param {currency} Currency
    *
    * @return Promise<Currency>
    */
    updateCurrency(currency: Currency) {

        return this.db.rel.save('currency', currency)
            .then((data: any) => {
                if (data && data.currencys && data.currencys
                [0]) {
                    return data.routes[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a currency
     * @param {currency} Currency
     *
     * @return Promise<boolean>
     */
    removeCurrency(currency: Currency): Promise<boolean> {
        return this.db.rel.del('currency', currency)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the currencys
     *
     * @return Promise<Array<currency>>
     */
    getCurrencys(): Promise<Array<Currency>> {
        return this.db.rel.find('currency')
            .then((data: any) => {
                this.currencys = data.currencys;
                if (this.searchText && this.searchText !== '') {

                    this.currencys = FuseUtils.filterArrayByString(this.currencys, this.searchText);
                    //this.oncurrencysChanged.next(this.currencys);
                }
                return Promise.resolve(this.currencys);
                //return data.currencys ? data.currencys : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a currency
     * @param {currency} currency
     *
     * @return Promise<currency>
     */
    getCurrency(currency: Currency): Promise<Currency> {
        return this.db.rel.find('currency', currency.id)
            .then((data: any) => {
                return data && data.currencys ? data.currencys[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected route by id
     * @param id
     */
    toggleSelectedCurrency(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedCurrencys.length > 0) {
            const index = this.selectedCurrencys.indexOf(id);

            if (index !== -1) {
                this.selectedCurrencys.splice(index, 1);

                // Trigger the next event
                this.onSelectedCurrencysChanged.next(this.selectedCurrencys);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedCurrencys.push(id);

        // Trigger the next event
        this.onSelectedCurrencysChanged.next(this.selectedCurrencys);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllCurrency() {
        if (this.selectedCurrencys.length > 0) {
            this.deselectCurrencys();
        }
        else {
            this.selectCurrencys();
        }
    }

    selectCurrencys(filterParameter?, filterValue?) {
        this.selectedCurrencys = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedCurrencys = [];
            this.currencys.map(currency => {
                this.selectedCurrencys.push(currency.id);
            });
        }
        else {
            /* this.selectedroutes.push(...
                 this.routes.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedCurrencysChanged.next(this.selectedCurrencys);
    }





    deselectCurrencys() {
        this.selectedCurrencys = [];

        // Trigger the next event
        this.onSelectedCurrencysChanged.next(this.selectedCurrencys);
    }

    deleteCurrency(currency) {
        this.db.rel.del('currency', currency)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const currencyIndex = this.currencys.indexOf(currency);
                this.currencys.splice(currencyIndex, 1);
                this.onCurrencysChanged.next(this.currencys);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedCurrencys() {
        for (const currencyId of this.selectedCurrencys) {
            const currency = this.currencys.find(_currency => {
                return _currency.id === currencyId;
            });

            this.db.rel.del('currency', currency)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const currencyIndex = this.currencys.indexOf(currency);
                    this.currencys.splice(currencyIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onCurrencysChanged.next(this.currencys);
        this.deselectCurrencys();
    }

    /***********
    * department
    **********/
    /**
     * Save a department
     * @param {department} Department
     *
     * @return Promise<department>
     */
    saveDepartment(department: Department): Promise<Department> {
        department.id = Math.floor(Date.now()).toString();

        department.doctors = [];
        return this.db.rel.save('department', department)
            .then((data: any) => {

                if (data && data.departments && data.departments
                [0]) {
                    console.log('save');

                    return data.departments[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a department
    * @param {department} Department
    *
    * @return Promise<department>
    */
    updateDepartment(department: Department) {
        return this.db.rel.save('department', department)
            .then((data: any) => {
                if (data && data.departments && data.departments
                [0]) {
                    console.log('Update')

                    return data.departments[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a department
     * @param {department} department
     *
     * @return Promise<boolean>
     */
    removedepartment(department: Department): Promise<boolean> {
        return this.db.rel.del('department', department)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the departments
     *
     * @return Promise<Array<department>>
     */
    getDepartments(): Promise<Array<Department>> {
        return this.db.rel.find('department')
            .then((data: any) => {
                this.departments = data.departments;
                if (this.searchText && this.searchText !== '') {
                    this.departments = FuseUtils.filterArrayByString(this.departments, this.searchText);
                }
                //this.ondepartmentsChanged.next(this.departments);
                return Promise.resolve(this.departments);
                //return data.departments ? data.departments : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getDoctorDepartments(): Promise<Array<Department>> {
        return this.db.rel.find('department')
            .then((data: any) => {
                return data.departments ? data.departments : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a department
     * @param {department} department
     *
     * @return Promise<department>
     */
    getDepartment(department: Department): Promise<Department> {
        return this.db.rel.find('department', department.id)
            .then((data: any) => {
                console.log("Get")

                return data && data.departments ? data.departments[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected department by id
     * @param id
     */
    toggleSelectedDepartment(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedDepartments.length > 0) {
            const index = this.selectedDepartments.indexOf(id);

            if (index !== -1) {
                this.selectedDepartments.splice(index, 1);

                // Trigger the next event
                this.onSelectedDepartmentsChanged.next(this.selectedDepartments);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedDepartments.push(id);

        // Trigger the next event
        this.onSelectedDepartmentsChanged.next(this.selectedDepartments);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllDepartment() {
        if (this.selectedDepartments.length > 0) {
            this.deselectDepartments();
        }
        else {
            this.selectDepartments();
        }
    }

    selectDepartments(filterParameter?, filterValue?) {
        this.selectedDepartments = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedDepartments = [];
            this.departments.map(department => {
                this.selectedDepartments.push(department.id);
            });
        }
        else {
            /* this.selecteddepartments.push(...
                 this.departments.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedDepartmentsChanged.next(this.selectedDepartments);
    }





    deselectDepartments() {
        this.selectedDepartments = [];

        // Trigger the next event
        this.onSelectedDepartmentsChanged.next(this.selectedDepartments);
    }

    deleteDepartment(department) {
        this.db.rel.del('department', department)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const departmentIndex = this.departments.indexOf(department);
                this.departments.splice(departmentIndex, 1);
                this.onDepartmentsChanged.next(this.departments);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedDepartments() {
        for (const departmentId of this.selectedDepartments) {
            const department = this.departments.find(_department => {
                return _department.id === departmentId;
            });

            this.db.rel.del('department', department)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const departmentIndex = this.departments.indexOf(department);
                    this.departments.splice(departmentIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onDepartmentsChanged.next(this.departments);
        this.deselectDepartments();
    }

    /***********
     * diagnosis_report
     **********/
    /**
     * Save a diagnosis_report
     * @param {diagnosis_report} diagnosis_report
     *
     * @return Promise<diagnosis_report>
     */
    saveDiagnosis_report(diagnosis_report: Diagnosis_report): Promise<Diagnosis_report> {
        diagnosis_report.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('diagnosis_report', diagnosis_report)
            .then((data: any) => {

                if (data && data.diagnosis_reports && data.diagnosis_reports[0]) {

                    return data.diagnosis_reports[0];
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a diagnosis_report
    * @param {diagnosis_report} diagnosis_report
    *
    * @return Promise<diagnosis_report>
    */
    updateDiagnosis_report(diagnosis_report: Diagnosis_report) {

        return this.db.rel.save('diagnosis_report', diagnosis_report)
            .then((data: any) => {
                if (data && data.diagnosis_reports && data.diagnosis_reports
                [0]) {
                    return data.diagnosis_reports[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a diagnosis_report
     * @param {diagnosis_report} diagnosis_report
     *
     * @return Promise<boolean>
     */
    removedDiagnosis_report(diagnosis_report: Diagnosis_report): Promise<boolean> {
        return this.db.rel.del('diagnosis_report', diagnosis_report)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the diagnosis_reports
     *
     * @return Promise<Array<diagnosis_report>>
     */
    getDiagnosis_reports(): Promise<Array<Diagnosis_report>> {
        var localStorageItem = JSON.parse(localStorage.getItem('user'));
        return this.db.rel.find('diagnosis_report')
            .then((data: any) => {
                this.diagnosis_reports = data.diagnosis_reports;
                if (this.searchText && this.searchText !== '') {
                    this.diagnosis_reports = FuseUtils.filterArrayByString(this.diagnosis_reports, this.searchText);
                }
                //this.ondiagnosis_reportsChanged.next(this.diagnosis_reports);
                var userId = localStorageItem.id
                this.diagnosis_reports = this.diagnosis_reports.filter(data => data.name_id == userId)

                return Promise.resolve(this.diagnosis_reports);
                //return data.diagnosis_reports ? data.diagnosis_reports : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getLaboratoristdiagnosis_reports(): Promise<Array<Diagnosis_report>> {
        return this.db.rel.find('diagnosis_report')
            .then((data: any) => {
                return data.diagnosis_reports ? data.diagnosis_reports : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a diagnosis_report
     * @param {diagnosis_report} diagnosis_report
     *
     * @return Promise<diagnosis_report>
     */
    getDiagnosis_report(diagnosis_report: Diagnosis_report): Promise<Diagnosis_report> {
        return this.db.rel.find('diagnosis_report', diagnosis_report.id)
            .then((data: any) => {

                return data && data.diagnosis_reports ? data.diagnosis_reports[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getDiagnosis_report2(id): Promise<Diagnosis_report> {
        return this.db.rel.find('diagnosis_report', id)
            .then((data: any) => {
                return data && data.diagnosis_reports ? data.diagnosis_reports[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected diagnosis_report by id
     * @param id
     */
    toggleSelectedDiagnosis_report(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedDiagnosis_reports.length > 0) {
            const index = this.selectedDiagnosis_reports.indexOf(id);

            if (index !== -1) {
                this.selectedDiagnosis_reports.splice(index, 1);

                // Trigger the next event
                this.onSelectedDiagnosis_reportsChanged.next(this.selectedDiagnosis_reports);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedDiagnosis_reports.push(id);

        // Trigger the next event
        this.onSelectedDiagnosis_reportsChanged.next(this.selectedDiagnosis_reports);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllDiagnosis_report() {
        if (this.selectedDiagnosis_reports.length > 0) {
            this.deselectDiagnosis_reports();
        }
        else {
            this.selectDiagnosis_reports();
        }
    }

    selectDiagnosis_reports(filterParameter?, filterValue?) {
        this.selectedDiagnosis_reports = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedDiagnosis_reports = [];
            this.diagnosis_reports.map(diagnosis_report => {
                this.selectedDiagnosis_reports.push(diagnosis_report.id);
            });
        }
        else {
            /* this.selecteddiagnosis_reports.push(...
                 this.diagnosis_reports.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedDiagnosis_reportsChanged.next(this.selectedDiagnosis_reports);
    }





    deselectDiagnosis_reports() {
        this.selectedDiagnosis_reports = [];

        // Trigger the next event
        this.onSelectedDiagnosis_reportsChanged.next(this.selectedDiagnosis_reports);
    }

    deleteDiagnosis_report(diagnosis_report) {
        this.db.rel.del('diagnosis_report', diagnosis_report)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const diagnosis_reportIndex = this.diagnosis_reports.indexOf(diagnosis_report);
                this.diagnosis_reports.splice(diagnosis_reportIndex, 1);
                this.onDiagnosis_reportsChanged.next(this.diagnosis_reports);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedDiagnosis_reports() {
        for (const diagnosis_reportId of this.selectedDiagnosis_reports) {
            const diagnosis_report = this.diagnosis_reports.find(_diagnosis_report => {
                return _diagnosis_report.id === diagnosis_reportId;
            });

            this.db.rel.del('diagnosis_report', diagnosis_report)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const diagnosis_reportIndex = this.diagnosis_reports.indexOf(diagnosis_report);
                    this.diagnosis_reports.splice(diagnosis_reportIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onDiagnosis_reportsChanged.next(this.diagnosis_reports);
        this.deselectDiagnosis_reports();
    }

    /***********
    * payment
    **********/
    /**
     * Save a payment
     * @param {payment} payment
     *
     * @return Promise<payment>
     */
    savePayment(payment: Payment): Promise<Payment> {
        payment.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('payment', payment)
            .then((data: any) => {

                if (data && data.payments && data.payments
                [0]) {
                    return data.payments[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a payment
    * @param {payment} payment
    *
    * @return Promise<payment>
    */
    updatePayment(payment: Payment) {

        return this.db.rel.save('payment', payment)
            .then((data: any) => {
                if (data && data.payments && data.payments
                [0]) {
                    return data.payments[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a payment
     * @param {payment} payment
     *
     * @return Promise<boolean>
     */
    removePayment(payment: Payment): Promise<boolean> {
        return this.db.rel.del('payment', payment)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the payments
     *
     * @return Promise<Array<payment>>
     */
    getPayments(): Promise<Array<Payment>> {
        return this.db.rel.find('payment')
            .then((data: any) => {
                this.payments = data.payments;
                if (this.searchText && this.searchText !== '') {
                    this.payments = FuseUtils.filterArrayByString(this.payments, this.searchText);
                }
                //this.onpaymentsChanged.next(this.payments);
                return Promise.resolve(this.payments);
                //return data.payments ? data.payments : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a payment
     * @param {payment} payment
     *
     * @return Promise<payment>
     */
    getPayment(payment: Payment): Promise<Payment> {
        return this.db.rel.find('payment', payment.id)
            .then((data: any) => {
                return data && data.payments ? data.payments[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected payment by id
     * @param id
     */
    toggleSelectedPayment(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedPayments.length > 0) {
            const index = this.selectedPayments.indexOf(id);

            if (index !== -1) {
                this.selectedPayments.splice(index, 1);

                // Trigger the next event
                this.onSelectedPaymentsChanged.next(this.selectedPayments);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedPayments.push(id);

        // Trigger the next event
        this.onSelectedPaymentsChanged.next(this.selectedPayments);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllPayment() {
        if (this.selectedPayments.length > 0) {
            this.deselectPayments();
        }
        else {
            this.selectPayments();
        }
    }

    selectPayments(filterParameter?, filterValue?) {
        this.selectedPayments = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedPayments = [];
            this.payments.map(payment => {
                this.selectedPayments.push(payment.id);
            });
        }
        else {
            /* this.selectedpayments.push(...
                 this.payments.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedPaymentsChanged.next(this.selectedPayments);
    }





    deselectPayments() {
        this.selectedPayments = [];

        // Trigger the next event
        this.onSelectedPaymentsChanged.next(this.selectedPayments);
    }

    deletePayment(payment) {
        this.db.rel.del('payment', payment)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const paymentIndex = this.payments.indexOf(payment);
                this.payments.splice(paymentIndex, 1);
                this.onPaymentsChanged.next(this.payments);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedPayments() {
        for (const paymentId of this.selectedPayments) {
            const payment = this.payments.find(_payment => {
                return _payment.id === paymentId;
            });

            this.db.rel.del('payment', payment)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const paymentIndex = this.payments.indexOf(payment);
                    this.payments.splice(paymentIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onPaymentsChanged.next(this.payments);
        this.deselectPayments();
    }

    /***********
    * email_template
    **********/
    /**
     * Save a email_template
     * @param {email_template} Email_template
     *
     * @return Promise<email_template>
     */
    saveEmail_template(email_template: Email_template): Promise<Email_template> {
        email_template.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('email_template', email_template)
            .then((data: any) => {
                //console.log(data);
                //console.log(email_template);
                if (data && data.email_templates && data.email_templates
                [0]) {
                    return data.email_templates[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a email_template
    * @param {email_template} Email_template
    *
    * @return Promise<Email_template>
    */
    updateEmail_template(email_template: Email_template) {

        return this.db.rel.save('email_template', email_template)
            .then((data: any) => {
                if (data && data.email_templates && data.email_templates
                [0]) {
                    return data.email_templates[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a email_template
     * @param {email_template} Email_template
     *
     * @return Promise<boolean>
     */
    removeEmail_template(email_template: Email_template): Promise<boolean> {
        return this.db.rel.del('email_template', email_template)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the email_templates
     *
     * @return Promise<Array<email_template>>
     */
    getEmail_templates(): Promise<Array<Email_template>> {
        return this.db.rel.find('email_template')
            .then((data: any) => {
                this.email_templates = data.email_templates;
                if (this.searchText && this.searchText !== '') {
                    this.email_templates = FuseUtils.filterArrayByString(this.email_templates, this.searchText);
                }
                //this.onemail_templatesChanged.next(this.email_templates);
                return Promise.resolve(this.email_templates);
                //return data.email_templates ? data.email_templates : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a email_template
     * @param {email_template} Email_template
     *
     * @return Promise<email_template>
     */
    getEmail_template(email_template: Email_template): Promise<Email_template> {
        return this.db.rel.find('email_template', email_template.id)
            .then((data: any) => {
                return data && data.email_templates ? data.email_templates[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected email_template by id
     * @param id
     */
    toggleSelectedEmail_template(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedEmail_templates.length > 0) {
            const index = this.selectedEmail_templates.indexOf(id);

            if (index !== -1) {
                this.selectedEmail_templates.splice(index, 1);

                // Trigger the next event
                this.onSelectedEmail_templatesChanged.next(this.selectedEmail_templates);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedEmail_templates.push(id);

        // Trigger the next event
        this.onSelectedEmail_templatesChanged.next(this.selectedEmail_templates);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllEmail_template() {
        if (this.selectedEmail_templates.length > 0) {
            this.deselectEmail_templates();
        }
        else {
            this.selectEmail_templates();
        }
    }

    selectEmail_templates(filterParameter?, filterValue?) {
        this.selectedEmail_templates = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedEmail_templates = [];
            this.email_templates.map(email_template => {
                this.selectedEmail_templates.push(email_template.id);
            });
        }
        else {
            /* this.selectedemail_templates.push(...
                 this.email_templates.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedEmail_templatesChanged.next(this.selectedEmail_templates);
    }





    deselectEmail_templates() {
        this.selectedEmail_templates = [];

        // Trigger the next event
        this.onSelectedEmail_templatesChanged.next(this.selectedEmail_templates);
    }

    deleteEmail_template(email_template) {
        this.db.rel.del('email_template', email_template)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const email_templateIndex = this.email_templates.indexOf(email_template);
                this.email_templates.splice(email_templateIndex, 1);
                this.onEmail_templatesChanged.next(this.email_templates);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedEmail_templates() {
        for (const email_templateId of this.selectedEmail_templates) {
            const email_template = this.email_templates.find(_email_template => {
                return _email_template.id === email_templateId;
            });

            this.db.rel.del('email_template', email_template)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const email_templateIndex = this.email_templates.indexOf(email_template);
                    this.email_templates.splice(email_templateIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onEmail_templatesChanged.next(this.email_templates);
        this.deselectEmail_templates();
    }

    /***********
    * expense
    **********/
    /**
     * Save a expense
     * @param {expense} expense
     *
     * @return Promise<expense>
     */
    saveExpense(expense: Expense): Promise<Expense> {
        expense.id = Math.floor(Date.now()).toString();
        //expense.driverslicenses = [];
        return this.db.rel.save('expense', expense)
            .then((data: any) => {
                //console.log(data);
                //console.log(expense);
                if (data && data.expenses && data.expenses
                [0]) {
                    return data.expenses[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a expense
    * @param {expense} Expense
    *
    * @return Promise<expense>
    */
    updateExpense(expense: Expense) {
        return this.db.rel.save('expense', expense)
            .then((data: any) => {
                if (data && data.expenses && data.expenses
                [0]) {
                    return data.expenses[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a expense
     * @param {expense} Expense
     *
     * @return Promise<boolean>
     */
    removeExpense(expense: Expense): Promise<boolean> {
        return this.db.rel.del('expense', expense)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the expenses
     *
     * @return Promise<Array<expense>>
     */
    getExpenses(): Promise<Array<Expense>> {
        return this.db.rel.find('expense')
            .then((data: any) => {
                this.expenses = data.expenses;
                if (this.searchText && this.searchText !== '') {
                    this.expenses = FuseUtils.filterArrayByString(this.expenses, this.searchText);
                }
                //this.onexpensesChanged.next(this.expenses);
                return Promise.resolve(this.expenses);
                //return data.expenses ? data.expenses : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a expense
     * @param {expense} expense
     *
     * @return Promise<expense>
     */
    getexpense(expense: Expense): Promise<Expense> {
        return this.db.rel.find('expense', expense.id)
            .then((data: any) => {
                return data && data.expenses ? data.expenses[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected expense by id
     * @param id
     */
    toggleSelectedExpense(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedExpenses.length > 0) {
            const index = this.selectedExpenses.indexOf(id);

            if (index !== -1) {
                this.selectedExpenses.splice(index, 1);

                // Trigger the next event
                this.onSelectedExpensesChanged.next(this.selectedExpenses);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedExpenses.push(id);

        // Trigger the next event
        this.onSelectedExpensesChanged.next(this.selectedExpenses);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllExpense() {
        if (this.selectedExpenses.length > 0) {
            this.deselectExpenses();
        }
        else {
            this.selectExpenses();
        }
    }

    selectExpenses(filterParameter?, filterValue?) {
        this.selectedExpenses = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedExpenses = [];
            this.expenses.map(expense => {
                this.selectedExpenses.push(expense.id);
            });
        }
        else {
            /* this.selectedexpenses.push(...
                 this.expenses.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedExpensesChanged.next(this.selectedExpenses);
    }





    deselectExpenses() {
        this.selectedExpenses = [];

        // Trigger the next event
        this.onSelectedExpensesChanged.next(this.selectedExpenses);
    }

    deleteExpense(expense) {
        this.db.rel.del('expense', expense)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const expenseIndex = this.expenses.indexOf(expense);
                this.expenses.splice(expenseIndex, 1);
                this.onExpensesChanged.next(this.expenses);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedExpenses() {
        for (const expenseId of this.selectedExpenses) {
            const expense = this.expenses.find(_expense => {
                return _expense.id === expenseId;
            });

            this.db.rel.del('expense', expense)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const expenseIndex = this.expenses.indexOf(expense);
                    this.expenses.splice(expenseIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onExpensesChanged.next(this.expenses);
        this.deselectExpenses();
    }

    /***********
     * extendlicense
     **********/
    /**
     * Save a extendlicense
     * @param {extendlicense} extendlicense
     *
     * @return Promise<extendlicense>
     */
    saveExtendlicense(extendlicense: Extendlicense): Promise<Extendlicense> {
        extendlicense.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('extendlicense', extendlicense)
            .then((data: any) => {

                if (data && data.extendlicenses && data.extendlicenses
                [0]) {
                    console.log('save');

                    return data.extendlicenses[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /*  extendLicense(date, plan) {
 
         this.getSettings().then(settings => {
             settings[0].expiredate = new Date(date).toUTCString();
             settings[0].plans.push(plan);
 
             this.updateSetting(settings[0]);
         });
     }
  */
    /**
   * Return all the settings
   *
   * @return Promise<Array<setting>>
   */
    /*  getSettings(): Promise<Array<Setting>> {
         return this.db.rel.find('settings')
             .then((data: any) => {
                 this.settings = data.settings;
                 if (this.searchText && this.searchText !== '') {
                     this.settings = FuseUtils.filterArrayByString(this.settings, this.searchText);
                 }
                 //this.onSettingsChanged.next(this.settings);
                 return Promise.resolve(this.settings);
                 //return data.settings ? data.settings : [];
             }).catch((err: any) => {
                 console.error(err);
             });
     } */

    /**
  * Update a setting
  * @param {setting} Setting
  *
  * @return Promise<setting>
  */
    /*  updateSetting(setting: Setting) {
 
         return this.db.rel.save('settings', setting)
             .then((data: any) => {
                 if (data && data.settings && data.settings
                 [0]) {
                     return data.settings[0]
                 }
 
                 return null;
             }).catch((err: any) => {
                 console.error(err);
             });
     }
  */
    /**
    * Update a extendlicense
    * @param {extendlicense} Extendlicense
    *
    * @return Promise<extendlicense>
    */
    updateExtendlicense(extendlicense: Extendlicense) {
        return this.db.rel.save('extendlicense', extendlicense)
            .then((data: any) => {
                if (data && data.extendlicenses && data.extendlicenses
                [0]) {
                    console.log('Update')

                    return data.extendlicenses[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a extendlicense
     * @param {extendlicense} extendlicense
     *
     * @return Promise<boolean>
     */
    removeExtendlicense(extendlicense: Extendlicense): Promise<boolean> {
        return this.db.rel.del('extendlicense', extendlicense)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the extendlicenses
     *
     * @return Promise<Array<extendlicense>>
     */
    getExtendlicenses(): Promise<Array<Extendlicense>> {
        return this.db.rel.find('extendlicense')
            .then((data: any) => {
                this.extendlicenses = data.extendlicenses;
                if (this.searchText && this.searchText !== '') {
                    this.extendlicenses = FuseUtils.filterArrayByString(this.extendlicenses, this.searchText);
                }
                //this.onextendlicensesChanged.next(this.extendlicenses);
                return Promise.resolve(this.extendlicenses);
                //return data.extendlicenses ? data.extendlicenses : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getDoctorExtendlicenses(): Promise<Array<Extendlicense>> {
        return this.db.rel.find('extendlicense')
            .then((data: any) => {
                return data.extendlicenses ? data.extendlicenses : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a extendlicense
     * @param {extendlicense} extendlicense
     *
     * @return Promise<extendlicense>
     */
    getExtendlicense(extendlicense: Extendlicense): Promise<Extendlicense> {
        return this.db.rel.find('extendlicense', extendlicense.id)
            .then((data: any) => {
                console.log("Get")

                return data && data.extendlicenses ? data.extendlicenses[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected extendlicense by id
     * @param id
     */
    toggleSelectedExtendlicense(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedExtendlicenses.length > 0) {
            const index = this.selectedExtendlicenses.indexOf(id);

            if (index !== -1) {
                this.selectedExtendlicenses.splice(index, 1);

                // Trigger the next event
                this.onSelectedExtendlicensesChanged.next(this.selectedExtendlicenses);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedExtendlicenses.push(id);

        // Trigger the next event
        this.onSelectedExtendlicensesChanged.next(this.selectedExtendlicenses);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllExtendlicense() {
        if (this.selectedExtendlicenses.length > 0) {
            this.deselectExtendlicenses();
        }
        else {
            this.selectExtendlicenses();
        }
    }

    selectExtendlicenses(filterParameter?, filterValue?) {
        this.selectedExtendlicenses = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedExtendlicenses = [];
            this.extendlicenses.map(extendlicense => {
                this.selectedExtendlicenses.push(extendlicense.id);
            });
        }
        else {
            /* this.selectedextendlicenses.push(...
                 this.extendlicenses.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedExtendlicensesChanged.next(this.selectedExtendlicenses);
    }





    deselectExtendlicenses() {
        this.selectedExtendlicenses = [];

        // Trigger the next event
        this.onSelectedExtendlicensesChanged.next(this.selectedExtendlicenses);
    }

    deleteExtendlicense(extendlicense) {
        this.db.rel.del('extendlicense', extendlicense)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const extendlicenseIndex = this.extendlicenses.indexOf(extendlicense);
                this.extendlicenses.splice(extendlicenseIndex, 1);
                this.onExtendlicensesChanged.next(this.extendlicenses);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedExtendlicenses() {
        for (const extendlicenseId of this.selectedExtendlicenses) {
            const extendlicense = this.extendlicenses.find(_extendlicense => {
                return _extendlicense.id === extendlicenseId;
            });

            this.db.rel.del('extendlicense', extendlicense)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const extendlicenseIndex = this.extendlicenses.indexOf(extendlicense);
                    this.extendlicenses.splice(extendlicenseIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onExtendlicensesChanged.next(this.extendlicenses);
        this.deselectExtendlicenses();
    }

    /***********
     * followup
     **********/
    /**
     * Save a followup
     * @param {followup} followup
     *
     * @return Promise<followup>
     */
    savefollowup(followup: followup): Promise<followup> {
        followup.id = Math.floor(Date.now()).toString();
        //followup.driverslicenses = [];
        return this.db.rel.save('followup', followup)
            .then((data: any) => {
                //console.log(data);
                //console.log(followup);
                if (data && data.followups && data.followups
                [0]) {
                    return data.followups[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a followup
    * @param {followup} followup
    *
    * @return Promise<followup>
    */
    updatefollowup(followup: followup) {
        return this.db.rel.save('followup', followup)
            .then((data: any) => {
                if (data && data.followups && data.followups
                [0]) {
                    return data.followups[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a followup
     * @param {followup} followup
     *
     * @return Promise<boolean>
     */
    removefollowup(followup: followup): Promise<boolean> {
        return this.db.rel.del('followup', followup)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the followups
     *
     * @return Promise<Array<followup>>
     */
    getfollowups(): Promise<Array<followup>> {
        return this.db.rel.find('followup')
            .then((data: any) => {
                this.followups = data.followups;
                if (this.searchText && this.searchText !== '') {
                    this.followups = FuseUtils.filterArrayByString(this.followups, this.searchText);
                }
                //this.onfollowupsChanged.next(this.followups);
                return Promise.resolve(this.followups);
                //return data.followups ? data.followups : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getB_Allotfollowups(): Promise<Array<followup>> {
        return this.db.rel.find('followup')
            .then((data: any) => {
                return data.followups ? data.followups : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a followup
     * @param {followup} followup
     *
     * @return Promise<followup>
     */
    getfollowup(followup: followup): Promise<followup> {
        return this.db.rel.find('followup', followup.id)
            .then((data: any) => {
                return data && data.followups ? data.followups[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getfollowup2(id): Promise<followup> {
        return this.db.rel.find('followup', id)
            .then((data: any) => {
                return data && data.followups ? data.followups[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected followup by id
     * @param id
     */
    toggleSelectedfollowup(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedfollowups.length > 0) {
            const index = this.selectedfollowups.indexOf(id);

            if (index !== -1) {
                this.selectedfollowups.splice(index, 1);

                // Trigger the next event
                this.onSelectedfollowupsChanged.next(this.selectedfollowups);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedfollowups.push(id);

        // Trigger the next event
        this.onSelectedfollowupsChanged.next(this.selectedfollowups);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllFollow_up() {
        if (this.selectedfollowups.length > 0) {
            this.deselectfollowups();
        }
        else {
            this.selectfollowups();
        }
    }

    selectfollowups(filterParameter?, filterValue?) {
        this.selectedfollowups = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedfollowups = [];
            this.followups.map(followup => {
                this.selectedfollowups.push(followup.id);
            });
        }
        else {
            /* this.selectedfollowups.push(...
                 this.followups.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedfollowupsChanged.next(this.selectedfollowups);
    }





    deselectfollowups() {
        this.selectedfollowups = [];

        // Trigger the next event
        this.onSelectedfollowupsChanged.next(this.selectedfollowups);
    }

    deletefollowup(followup) {
        this.db.rel.del('followup', followup)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const followupIndex = this.followups.indexOf(followup);
                this.followups.splice(followupIndex, 1);
                this.onfollowupsChanged.next(this.followups);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedfollowups() {
        for (const followupId of this.selectedfollowups) {
            const followup = this.followups.find(_followup => {
                return _followup.id === followupId;
            });

            this.db.rel.del('followup', followup)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const followupIndex = this.followups.indexOf(followup);
                    this.followups.splice(followupIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onfollowupsChanged.next(this.followups);
        this.deselectfollowups();
    }

    /***********
  * form_element
  **********/
    /**
     * Save a form_element
     * @param {form_element} Form_element
     *
     * @return Promise<form_element>
     */
    saveForm_element(form_element: Form_element): Promise<Form_element> {
        form_element.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('form_element', form_element)
            .then((data: any) => {

                if (data && data.form_elements && data.form_elements
                [0]) {
                    return data.form_elements[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a form_element
    * @param {form_element} Form_element
    *
    * @return Promise<form_element>
    */
    updateForm_element(form_element: Form_element) {

        return this.db.rel.save('form_element', form_element)
            .then((data: any) => {
                if (data && data.form_elements && data.form_elements
                [0]) {
                    return data.form_elements[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a form_element
     * @param {form_element} form_element
     *
     * @return Promise<boolean>
     */
    removeForm_element(form_element: Form_element): Promise<boolean> {
        return this.db.rel.del('form_element', form_element)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the form_elements
     *
     * @return Promise<Array<form_element>>
     */
    getForm_elements(): Promise<Array<Form_element>> {
        return this.db.rel.find('form_element')
            .then((data: any) => {
                this.form_elements = data.form_elements;
                if (this.searchText && this.searchText !== '') {
                    this.form_elements = FuseUtils.filterArrayByString(this.form_elements, this.searchText);
                }
                //this.onform_elementsChanged.next(this.form_elements);
                return Promise.resolve(this.form_elements);
                //return data.form_elements ? data.form_elements : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a form_element
     * @param {form_element} form_element
     *
     * @return Promise<form_element>
     */
    getForm_element(form_element: Form_element): Promise<Form_element> {
        return this.db.rel.find('form_element', form_element.id)
            .then((data: any) => {
                return data && data.form_elements ? data.form_elements[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected form_element by id
     * @param id
     */
    toggleSelectedForm_element(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedForm_elements.length > 0) {
            const index = this.selectedForm_elements.indexOf(id);

            if (index !== -1) {
                this.selectedForm_elements.splice(index, 1);

                // Trigger the next event
                this.onSelectedForm_elementsChanged.next(this.selectedForm_elements);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedForm_elements.push(id);

        // Trigger the next event
        this.onSelectedForm_elementsChanged.next(this.selectedForm_elements);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllForm_element() {
        if (this.selectedForm_elements.length > 0) {
            this.deselectForm_elements();
        }
        else {
            this.selectForm_elements();
        }
    }

    selectForm_elements(filterParameter?, filterValue?) {
        this.selectedForm_elements = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedForm_elements = [];
            this.form_elements.map(form_element => {
                this.selectedForm_elements.push(form_element.id);
            });
        }
        else {
            /* this.selectedform_elements.push(...
                 this.form_elements.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedForm_elementsChanged.next(this.selectedForm_elements);
    }





    deselectForm_elements() {
        this.selectedForm_elements = [];

        // Trigger the next event
        this.onSelectedForm_elementsChanged.next(this.selectedForm_elements);
    }

    deleteForm_element(form_element) {
        this.db.rel.del('form_element', form_element)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const form_elementIndex = this.form_elements.indexOf(form_element);
                this.form_elements.splice(form_elementIndex, 1);
                this.onForm_elementsChanged.next(this.form_elements);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedForm_elements() {
        for (const form_elementId of this.selectedForm_elements) {
            const form_element = this.form_elements.find(_form_element => {
                return _form_element.id === form_elementId;
            });

            this.db.rel.del('form_element', form_element)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const form_elementIndex = this.form_elements.indexOf(form_element);
                    this.form_elements.splice(form_elementIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onForm_elementsChanged.next(this.form_elements);
        this.deselectForm_elements();
    }


    /***********
     * insurancesheme
     **********/
    /**
     * Save a insurancesheme
     * @param {insurancesheme} insurancesheme
     *
     * @return Promise<insurancesheme>
     */
    saveInsurancesheme(insurancesheme: Insurancesheme): Promise<Insurancesheme> {
        insurancesheme.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('insurancesheme', insurancesheme)
            .then((data: any) => {

                if (data && data.insuranceshemes && data.insuranceshemes
                [0]) {
                    console.log('save');

                    return data.insuranceshemes[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a insurancesheme
    * @param {insurancesheme} insurancesheme
    *
    * @return Promise<insurancesheme>
    */
    updateInsurancesheme(insurancesheme: Insurancesheme) {
        return this.db.rel.save('insurancesheme', insurancesheme)
            .then((data: any) => {
                if (data && data.insuranceshemes && data.insuranceshemes
                [0]) {
                    console.log('Update')

                    return data.insuranceshemes[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a insurancesheme
     * @param {insurancesheme} insurancesheme
     *
     * @return Promise<boolean>
     */
    removeInsurancesheme(insurancesheme: Insurancesheme): Promise<boolean> {
        return this.db.rel.del('insurancesheme', insurancesheme)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the insuranceshemes
     *
     * @return Promise<Array<insurancesheme>>
     */
    getInsuranceshemes(): Promise<Array<Insurancesheme>> {
        return this.db.rel.find('insurancesheme')
            .then((data: any) => {
                this.insuranceshemes = data.insuranceshemes;
                if (this.searchText && this.searchText !== '') {
                    this.insuranceshemes = FuseUtils.filterArrayByString(this.insuranceshemes, this.searchText);
                }
                //this.oninsuranceshemesChanged.next(this.insuranceshemes);
                return Promise.resolve(this.insuranceshemes);
                //return data.insuranceshemes ? data.insuranceshemes : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getDoctorInsuranceshemes(): Promise<Array<Insurancesheme>> {
        return this.db.rel.find('insurancesheme')
            .then((data: any) => {
                return data.insuranceshemes ? data.insuranceshemes : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a insurancesheme
     * @param {insurancesheme} insurancesheme
     *
     * @return Promise<insurancesheme>
     */
    getInsurancesheme(insurancesheme: Insurancesheme): Promise<Insurancesheme> {
        return this.db.rel.find('insurancesheme', insurancesheme.id)
            .then((data: any) => {
                console.log("Get")

                return data && data.insuranceshemes ? data.insuranceshemes[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected insurancesheme by id
     * @param id
     */
    toggleSelectedInsurancesheme(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedInsuranceshemes.length > 0) {
            const index = this.selectedInsuranceshemes.indexOf(id);

            if (index !== -1) {
                this.selectedInsuranceshemes.splice(index, 1);

                // Trigger the next event
                this.onSelectedInsuranceshemesChanged.next(this.selectedInsuranceshemes);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedInsuranceshemes.push(id);

        // Trigger the next event
        this.onSelectedInsuranceshemesChanged.next(this.selectedInsuranceshemes);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllInsurance_scheme() {
        if (this.selectedInsuranceshemes.length > 0) {
            this.deselectInsuranceshemes();
        }
        else {
            this.selectInsuranceshemes();
        }
    }

    selectInsuranceshemes(filterParameter?, filterValue?) {
        this.selectedInsuranceshemes = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedInsuranceshemes = [];
            this.insuranceshemes.map(insurancesheme => {
                this.selectedInsuranceshemes.push(insurancesheme.id);
            });
        }
        else {
            /* this.selectedinsuranceshemes.push(...
                 this.insuranceshemes.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedInsuranceshemesChanged.next(this.selectedInsuranceshemes);
    }





    deselectInsuranceshemes() {
        this.selectedInsuranceshemes = [];

        // Trigger the next event
        this.onSelectedInsuranceshemesChanged.next(this.selectedInsuranceshemes);
    }

    deleteInsurancesheme(insurancesheme) {
        this.db.rel.del('insurancesheme', insurancesheme)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const insuranceshemeIndex = this.insuranceshemes.indexOf(insurancesheme);
                this.insuranceshemes.splice(insuranceshemeIndex, 1);
                this.onInsuranceshemesChanged.next(this.insuranceshemes);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedInsuranceshemes() {
        for (const insuranceshemeId of this.selectedInsuranceshemes) {
            const insurancesheme = this.insuranceshemes.find(_insurancesheme => {
                return _insurancesheme.id === insuranceshemeId;
            });

            this.db.rel.del('insurancesheme', insurancesheme)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const insuranceshemeIndex = this.insuranceshemes.indexOf(insurancesheme);
                    this.insuranceshemes.splice(insuranceshemeIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onInsuranceshemesChanged.next(this.insuranceshemes);
        this.deselectInsuranceshemes();
    }

    /***********
   * insurancetype
   **********/
    /**
     * Save a insurancetype
     * @param {insurancetype} insurancetype
     *
     * @return Promise<insurancetype>
     */
    saveInsurancetype(insurancetype: Insurancetype): Promise<Insurancetype> {
        insurancetype.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('insurancetype', insurancetype)
            .then((data: any) => {

                if (data && data.insurancetypes && data.insurancetypes
                [0]) {
                    console.log('save');

                    return data.insurancetypes[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a insurancetype
    * @param {insurancetype} insurancetype
    *
    * @return Promise<insurancetype>
    */
    updateInsurancetype(insurancetype: Insurancetype) {
        return this.db.rel.save('insurancetype', insurancetype)
            .then((data: any) => {
                if (data && data.insurancetypes && data.insurancetypes
                [0]) {
                    console.log('Update')

                    return data.insurancetypes[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a insurancetype
     * @param {insurancetype} insurancetype
     *
     * @return Promise<boolean>
     */
    removeInsurancetype(insurancetype: Insurancetype): Promise<boolean> {
        return this.db.rel.del('insurancetype', insurancetype)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the insurancetypes
     *
     * @return Promise<Array<insurancetype>>
     */
    getInsurancetypes(): Promise<Array<Insurancetype>> {
        return this.db.rel.find('insurancetype')
            .then((data: any) => {
                this.insurancetypes = data.insurancetypes;
                if (this.searchText && this.searchText !== '') {
                    this.insurancetypes = FuseUtils.filterArrayByString(this.insurancetypes, this.searchText);
                }
                //this.oninsurancetypesChanged.next(this.insurancetypes);
                return Promise.resolve(this.insurancetypes);
                //return data.insurancetypes ? data.insurancetypes : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getDoctorInsurancetypes(): Promise<Array<Insurancetype>> {
        return this.db.rel.find('insurancetype')
            .then((data: any) => {
                return data.insurancetypes ? data.insurancetypes : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a insurancetype
     * @param {insurancetype} insurancetype
     *
     * @return Promise<insurancetype>
     */
    getInsurancetype(insurancetype: Insurancetype): Promise<Insurancetype> {
        return this.db.rel.find('insurancetype', insurancetype.id)
            .then((data: any) => {
                console.log("Get")

                return data && data.insurancetypes ? data.insurancetypes[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected insurancetype by id
     * @param id
     */
    toggleSelectedInsurancetype(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedInsurancetypes.length > 0) {
            const index = this.selectedInsurancetypes.indexOf(id);

            if (index !== -1) {
                this.selectedInsurancetypes.splice(index, 1);

                // Trigger the next event
                this.onSelectedInsurancetypesChanged.next(this.selectedInsurancetypes);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedInsurancetypes.push(id);

        // Trigger the next event
        this.onSelectedInsurancetypesChanged.next(this.selectedInsurancetypes);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllInsurancetype() {
        if (this.selectedInsurancetypes.length > 0) {
            this.deselectInsurancetypes();
        }
        else {
            this.selectInsurancetypes();
        }
    }

    selectInsurancetypes(filterParameter?, filterValue?) {
        this.selectedInsurancetypes = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedInsurancetypes = [];
            this.insurancetypes.map(insurancetype => {
                this.selectedInsurancetypes.push(insurancetype.id);
            });
        }
        else {
            /* this.selectedinsurancetypes.push(...
                 this.insurancetypes.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedInsurancetypesChanged.next(this.selectedInsurancetypes);
    }





    deselectInsurancetypes() {
        this.selectedInsurancetypes = [];

        // Trigger the next event
        this.onSelectedInsurancetypesChanged.next(this.selectedInsurancetypes);
    }

    deleteInsurancetype(insurancetype) {
        this.db.rel.del('insurancetype', insurancetype)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const insurancetypeIndex = this.insurancetypes.indexOf(insurancetype);
                this.insurancetypes.splice(insurancetypeIndex, 1);
                this.onInsurancetypesChanged.next(this.insurancetypes);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedInsurancetypes() {
        for (const insurancetypeId of this.selectedInsurancetypes) {
            const insurancetype = this.insurancetypes.find(_insurancetype => {
                return _insurancetype.id === insurancetypeId;
            });

            this.db.rel.del('insurancetype', insurancetype)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const insurancetypeIndex = this.insurancetypes.indexOf(insurancetype);
                    this.insurancetypes.splice(insurancetypeIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onInsurancetypesChanged.next(this.insurancetypes);
        this.deselectInsurancetypes();
    }

    /***********
    * invoice
    **********/
    /**
     * Save a invoice
     * @param {invoice} invoice
     *
     * @return Promise<invoice>
     */
    saveInvoice(invoice: Invoice): Promise<Invoice> {

        invoice.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('invoice', invoice)
            .then((data: any) => {

                if (data && data.invoices && data.invoices
                [0]) {

                    return data.invoices[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a invoice
    * @param {invoice} invoice
    *
    * @return Promise<invoice>
    */
    updateInvoice(invoice: Invoice) {

        return this.db.rel.save('invoice', invoice)
            .then((data: any) => {
                if (data && data.invoices && data.invoices
                [0]) {
                    return data.invoices[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a invoice
     * @param {invoice} invoice
     *
     * @return Promise<boolean>
     */
    removeInvoice(invoice: Invoice): Promise<boolean> {
        return this.db.rel.del('invoice', invoice)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the invoices
     *
     * @return Promise<Array<invoice>>
     */
    getInvoices(): Promise<Array<Invoice>> {
        var localStorageItem = JSON.parse(localStorage.getItem('user'));
        return this.db.rel.find('invoice')
            .then((data: any) => {
                this.invoices = data.invoices;
                if (this.searchText && this.searchText !== '') {
                    this.invoices = FuseUtils.filterArrayByString(this.invoices, this.searchText);
                }
                //this.oninvoicesChanged.next(this.invoices);
                var userId = localStorageItem.id
                if (localStorageItem.usertype == "Patient") {
                    this.invoices = this.invoices.filter(data => data.patient_id == userId)
                }
                return Promise.resolve(this.invoices);
                //return data.invoices ? data.invoices : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a invoice
     * @param {invoice} invoice
     *
     * @return Promise<invoice>
     */
    getInvoice(id): Promise<Invoice> {
        return this.db.rel.find('invoice', id)
            .then((data: any) => {
                return data && data.invoices ? data.invoices[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected invoice by id
     * @param id
     */
    toggleSelectedInvoice(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedInvoices.length > 0) {
            const index = this.selectedInvoices.indexOf(id);

            if (index !== -1) {
                this.selectedInvoices.splice(index, 1);

                // Trigger the next event
                this.onSelectedInvoicesChanged.next(this.selectedInvoices);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedInvoices.push(id);

        // Trigger the next event
        this.onSelectedInvoicesChanged.next(this.selectedInvoices);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllInvoice() {
        if (this.selectedInvoices.length > 0) {
            this.deselectInvoices();
        }
        else {
            this.selectInvoices();
        }
    }

    selectInvoices(filterParameter?, filterValue?) {
        this.selectedInvoices = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedInvoices = [];
            this.invoices.map(invoice => {
                this.selectedInvoices.push(invoice.id);
            });
        }
        else {
            /* this.selectedinvoices.push(...
                 this.invoices.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedInvoicesChanged.next(this.selectedInvoices);
    }





    deselectInvoices() {
        this.selectedInvoices = [];

        // Trigger the next event
        this.onSelectedInvoicesChanged.next(this.selectedInvoices);
    }

    deleteInvoice(invoice) {
        this.db.rel.del('invoice', invoice)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const invoiceIndex = this.invoices.indexOf(invoice);
                this.invoices.splice(invoiceIndex, 1);
                this.onInvoicesChanged.next(this.invoices);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedInvoices() {
        for (const invoiceId of this.selectedInvoices) {
            const invoice = this.invoices.find(_invoice => {
                return _invoice.id === invoiceId;
            });

            this.db.rel.del('invoice', invoice)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const invoiceIndex = this.invoices.indexOf(invoice);
                    this.invoices.splice(invoiceIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onInvoicesChanged.next(this.invoices);
        this.deselectInvoices();
    }

    /***********
   * lab_dashboard
   **********/
    /**
     * Save a lab_dashboard
     * @param {lab_dashboard} lab_dashboard
     *
     * @return Promise<lab_dashboard>
     */
    saveLab_dashboard(lab_dashboard: Calendar): Promise<Calendar> {
        lab_dashboard.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('lab_dashboard', lab_dashboard)
            .then((data: any) => {

                if (data && data.lab_dashboards && data.lab_dashboards
                [0]) {

                    return data.lab_dashboards[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a lab_dashboard
    * @param {lab_dashboard} lab_dashboard
    *
    * @return Promise<lab_dashboard>
    */
    updateLab_dashboard(lab_dashboard: Calendar) {

        return this.db.rel.save('lab_dashboard', lab_dashboard)
            .then((data: any) => {
                if (data && data.lab_dashboards && data.lab_dashboards
                [0]) {

                    return data.lab_dashboards[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    updateLab_dashboards(lab_dashboard: Promise<Array<Calendar>>) {

        return this.db.rel.save('lab_dashboard', lab_dashboard)
            .then((data: any) => {
                if (data && data.lab_dashboards && data.lab_dashboards
                [0]) {
                    return data.lab_dashboards[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a lab_dashboard
     * @param {lab_dashboard} lab_dashboard
     *
     * @return Promise<boolean>
     */
    removeLab_dashboard(lab_dashboard: Calendar): Promise<boolean> {
        return this.db.rel.del('lab_dashboard', lab_dashboard)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the lab_dashboards
     *
     * @return Promise<Array<lab_dashboard>>
     */
    getLab_dashboards(): Promise<Array<Calendar>> {
        return this.db.rel.find('lab_dashboard')
            .then((data: any) => {
                this.lab_dashboards = data.lab_dashboards;

                this.onEventsUpdated.next(this.lab_dashboards);
                if (this.searchText && this.searchText !== '') {
                    this.lab_dashboards = FuseUtils.filterArrayByString(this.lab_dashboards, this.searchText);
                }
                //this.onnoticesChanged.next(this.lab_dashboards);
                return Promise.resolve(this.lab_dashboards);
                //return data.lab_dashboards ? data.lab_dashboards : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Read a lab_dashboard
     * @param {lab_dashboard} lab_dashboard
     *
     * @return Promise<lab_dashboard>
     */
    getLab_dashboard(lab_dashboard: Calendar): Promise<Calendar> {
        return this.db.rel.find('lab_dashboard', lab_dashboard.id)
            .then((data: any) => {
                //this.onEventsUpdated.next(this.notices);
                return data && data.notices ? data.notices[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected notice by id
     * @param id
     */
    toggleSelectedLab_dashboard(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedLab_dashboards.length > 0) {
            const index = this.selectedLab_dashboards.indexOf(id);

            if (index !== -1) {
                this.selectedLab_dashboards.splice(index, 1);

                // Trigger the next event
                this.onSelectedLab_dashboardsChanged.next(this.selectedLab_dashboards);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedLab_dashboards.push(id);

        // Trigger the next event
        this.onSelectedLab_dashboardsChanged.next(this.selectedLab_dashboards);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllLab_dashboard() {
        if (this.selectedLab_dashboards.length > 0) {
            this.deselectLab_dashboards();
        }
        else {
            this.selectLab_dashboards();
        }
    }

    selectLab_dashboards(filterParameter?, filterValue?) {
        this.selectedLab_dashboards = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedLab_dashboards = [];
            this.lab_dashboards.map(notice => {
                this.selectedLab_dashboards.push(notice.id);
            });
        }
        else {
            /* this.selectednotices.push(...
                 this.notices.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedLab_dashboardsChanged.next(this.selectedLab_dashboards);
    }





    deselectLab_dashboards() {
        this.selectedLab_dashboards = [];

        // Trigger the next event
        this.onSelectedLab_dashboardsChanged.next(this.selectedLab_dashboards);
    }

    deleteLab_dashboard(lab_dashboard) {
        this.db.rel.del('lab_dashboard', lab_dashboard)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const lab_dashboardIndex = this.lab_dashboards.indexOf(lab_dashboard);
                this.lab_dashboards.splice(lab_dashboardIndex, 1);
                this.onLab_dashboardsChanged.next(this.lab_dashboards);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedLab_dashboards() {
        for (const lab_dashboardId of this.selectedLab_dashboards) {
            const lab_dashboard = this.lab_dashboards.find(_lab_dashboard => {
                return _lab_dashboard.id === lab_dashboardId;
            });

            this.db.rel.del('lab_dashboard', lab_dashboard)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const lab_dashboardIndex = this.lab_dashboards.indexOf(lab_dashboard);
                    this.lab_dashboards.splice(lab_dashboardIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onLab_dashboardsChanged.next(this.lab_dashboards);
        this.deselectLab_dashboards();
    }

    /***********
    * laboratorist
    **********/
    /**
     * Save a laboratorist
     * @param {laboratorist} laboratorist
     *
     * @return Promise<laboratorist>
     */
    saveLaboratorist(laboratorist: Laboratorist): Promise<Laboratorist> {
        laboratorist.id = Math.floor(Date.now()).toString();
        laboratorist.diagnosis_reports = [];

        return this.db.rel.save('laboratorist', laboratorist)
            .then((data: any) => {
                //console.log(data);
                //console.log(laboratorist);
                if (data && data.laboratorists && data.laboratorists
                [0]) {
                    return data.laboratorists[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a laboratorist
    * @param {laboratorist} Laboratorist
    *
    * @return Promise<Laboratorist>
    */
    updateLaboratorist(laboratorist: Laboratorist) {

        return this.db.rel.save('laboratorist', laboratorist)
            .then((data: any) => {
                if (data && data.laboratorists && data.laboratorists
                [0]) {
                    return data.laboratorists[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a laboratorist
     * @param {laboratorist} laboratorist
     *
     * @return Promise<boolean>
     */
    removeLaboratorist(laboratorist: Laboratorist): Promise<boolean> {
        return this.db.rel.del('laboratorist', laboratorist)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the laboratorists
     *
     * @return Promise<Array<laboratorist>>
     */
    getLaboratorists(): Promise<Array<Laboratorist>> {
        return this.db.rel.find('laboratorist')
            .then((data: any) => {
                this.laboratorists = data.laboratorists;
                if (this.searchText && this.searchText !== '') {
                    this.laboratorists = FuseUtils.filterArrayByString(this.laboratorists, this.searchText);
                }
                //this.onlaboratoristsChanged.next(this.laboratorists);
                return Promise.resolve(this.laboratorists);
                //return data.laboratorists ? data.laboratorists : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getReportLaboratorists(): Promise<Array<Laboratorist>> {
        return this.db.rel.find('laboratorist')
            .then((data: any) => {
                return data.laboratorists ? data.laboratorists : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a laboratorist
     * @param {laboratorist} Laboratorist
     *
     * @return Promise<Laboratorist>
     */
    getLaboratorist(id): Promise<Laboratorist> {
        return this.db.rel.find('laboratorist', id)
            .then((data: any) => {
                return data && data.laboratorists ? data.laboratorists[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected laboratorist by id
     * @param id
     */
    toggleSelectedLaboratorist(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedLaboratorists.length > 0) {
            const index = this.selectedLaboratorists.indexOf(id);

            if (index !== -1) {
                this.selectedLaboratorists.splice(index, 1);

                // Trigger the next event
                this.onSelectedLaboratoristsChanged.next(this.selectedLaboratorists);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedLaboratorists.push(id);

        // Trigger the next event
        this.onSelectedLaboratoristsChanged.next(this.selectedLaboratorists);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllLaboratorist() {
        if (this.selectedLaboratorists.length > 0) {
            this.deselectLaboratorists();
        }
        else {
            this.selectLaboratorists();
        }
    }

    selectLaboratorists(filterParameter?, filterValue?) {
        this.selectedLaboratorists = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedLaboratorists = [];
            this.laboratorists.map(laboratorist => {
                this.selectedLaboratorists.push(laboratorist.id);
            });
        }
        else {
            /* this.selectedlaboratorists.push(...
                 this.laboratorists.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedLaboratoristsChanged.next(this.selectedLaboratorists);
    }





    deselectLaboratorists() {
        this.selectedLaboratorists = [];

        // Trigger the next event
        this.onSelectedLaboratoristsChanged.next(this.selectedLaboratorists);
    }

    deleteLaboratorist(laboratorist) {
        this.db.rel.del('laboratorist', laboratorist)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const laboratoristIndex = this.laboratorists.indexOf(laboratorist);
                this.laboratorists.splice(laboratoristIndex, 1);
                this.onLaboratoristsChanged.next(this.laboratorists);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedLaboratorists() {
        for (const laboratoristId of this.selectedLaboratorists) {
            const laboratorist = this.laboratorists.find(_laboratorist => {
                return _laboratorist.id === laboratoristId;
            });

            this.db.rel.del('laboratorist', laboratorist)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const laboratoristIndex = this.laboratorists.indexOf(laboratorist);
                    this.laboratorists.splice(laboratoristIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onLaboratoristsChanged.next(this.laboratorists);
        this.deselectLaboratorists();
    }

    /**
   * Update a staff
   * @param {staff} staff
   *
   * @return Promise<staff>
   */
    /* updateStaff(staff: Staff) {
 
     return this.db.rel.save('staff', staff)
         .then((data: any) => {
             if (data && data.staffs && data.staffs
             [0]) {
                 console.log(data.staffs[0])
                 return data.staffs[0]
             }
             return null;
         }).catch((err: any) => {
             console.error(err);
         });
 
 
 
 }
  */
    /***********
    * labrecord
    **********/
    /**
    * Save a labrecord
    * @param {labrecord} labrecord
    *
    * @return Promise<labrecord>
    */
    savelabrecord(labrecord: labrecord, staff: Staff): Promise<labrecord> {
        //labrecord.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('labrecord', labrecord)
            .then((data: any) => {
                console.log(staff);
                this.updateStaff(staff);
                /*   if (this.file) {
                      this.file;
                      
                      this.retrieve = (<HTMLInputElement>this.file).files[0];
        
                      if (this.retrieve = (<HTMLInputElement>this.file).files[0]) {
                          staff._attachments = this.retrieve
                      }
        
                      else {
                          staff._attachments;
                          //this.getStaff = staff._attachments;
                      }
        
                      return this.db.rel.save('staff', staff)
                          .then((data: any) => {
                              if (data && data.staffs && data.staffs
                              [0]) {
                                   this.db.rel.putAttachment('staff', { id: staff.id, rev: staff.rev }, 'file', staff._attachments, 'image/png').then(res => {
                                      var reader: FileReader = new FileReader();
        
                                      reader.onloadend = function (e) {
                                          var base64data = reader.result;
                                          staff.image = base64data
                                          //console.log(staff.image);
                                      }
                                      reader.readAsDataURL(this.retrieve);
                                      staff._attachments = res;
                                      staff.image = res
        
                                  }) 
                                  
                                  return data.staffs[0]
                              }
        
                              return null;
                          }).catch((err: any) => {
                              console.error(err);
                          });
        
                  }
                  else {
                      
        
                      return this.db.rel.save('staff', staff)
                          .then((data: any) => {
                              if (data && data.staffs && data.staffs
                              [0]) {
                                   this.db.rel.putAttachment('staff', { id: staff.id, rev: staff.rev }, 'file', staff._attachments, 'image/png').then(res => {
                                      staff._attachments = res
                                  }) 
                                  
                                  return data.staffs[0]
                              }
        
                              return null;
                          }).catch((err: any) => {
                              console.error(err);
                          });
                  } */
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a labrecord
    * @param {labrecord} labrecord
    *
    * @return Promise<labrecord>
    */
    updatelabrecord(labrecord: labrecord) {
        return this.db.rel.save('labrecord', labrecord)
            .then((data: any) => {
                if (data && data.labrecords && data.labrecords
                [0]) {
                    console.log('Update')

                    return data.labrecords[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Remove a labrecord
    * @param {labrecord} labrecord
    *
    * @return Promise<boolean>
    */
    removelabrecord(labrecord: labrecord): Promise<boolean> {
        return this.db.rel.del('labrecord', labrecord)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Return all the labrecords
    *
    * @return Promise<Array<labrecord>>
    */
    getlabrecords(): Promise<Array<labrecord>> {
        return this.db.rel.find('labrecord')
            .then((data: any) => {
                this.labrecords = data.labrecords;
                if (this.searchText && this.searchText !== '') {
                    this.labrecords = FuseUtils.filterArrayByString(this.labrecords, this.searchText);
                }
                //this.onlabrecordsChanged.next(this.labrecords);
                return Promise.resolve(this.labrecords);
                //return data.labrecords ? data.labrecords : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getDoctorlabrecords(): Promise<Array<labrecord>> {
        return this.db.rel.find('labrecord')
            .then((data: any) => {
                return data.labrecords ? data.labrecords : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Read a labrecord
    * @param {labrecord} labrecord
    *
    * @return Promise<labrecord>
    */
    getlabrecord(id): Promise<labrecord> {
        return this.db.rel.find('labrecord', id)
            .then((data: any) => {
                console.log("Get")

                return data && data.labrecords ? data.labrecords[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
    * Toggle selected labrecord by id
    * @param id
    */
    toggleSelectedlabrecord(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedlabrecords.length > 0) {
            const index = this.selectedlabrecords.indexOf(id);

            if (index !== -1) {
                this.selectedlabrecords.splice(index, 1);

                // Trigger the next event
                this.onSelectedlabrecordsChanged.next(this.selectedlabrecords);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedlabrecords.push(id);

        // Trigger the next event
        this.onSelectedlabrecordsChanged.next(this.selectedlabrecords);
    }

    /**
    * Toggle select all
    */
    toggleSelectAllLabrecords() {
        if (this.selectedlabrecords.length > 0) {
            this.deselectlabrecords();
        }
        else {
            this.selectlabrecords();
        }
    }

    selectlabrecords(filterParameter?, filterValue?) {
        this.selectedlabrecords = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedlabrecords = [];
            this.labrecords.map(labrecord => {
                this.selectedlabrecords.push(labrecord.id);
            });
        }
        else {
            /* this.selectedlabrecords.push(...
                 this.labrecords.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedlabrecordsChanged.next(this.selectedlabrecords);
    }





    deselectlabrecords() {
        this.selectedlabrecords = [];

        // Trigger the next event
        this.onSelectedlabrecordsChanged.next(this.selectedlabrecords);
    }

    deletelabrecord(labrecord) {
        this.db.rel.del('labrecord', labrecord)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const labrecordIndex = this.labrecords.indexOf(labrecord);
                this.labrecords.splice(labrecordIndex, 1);
                this.onlabrecordsChanged.next(this.labrecords);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedlabrecords() {
        for (const labrecordId of this.selectedlabrecords) {
            const labrecord = this.labrecords.find(_labrecord => {
                return _labrecord.id === labrecordId;
            });

            this.db.rel.del('labrecord', labrecord)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const labrecordIndex = this.labrecords.indexOf(labrecord);
                    this.labrecords.splice(labrecordIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onlabrecordsChanged.next(this.labrecords);
        this.deselectlabrecords();
    }

    /***********
         * labtest
         **********/
    /**
     * Save a labtest
     * @param {labtest} labtest
     *
     * @return Promise<labtest>
     */
    savelabtest(labtest: labtest): Promise<labtest> {
        //labtest.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('labtest', labtest)
            .then((data: any) => {

                if (data && data.labtests && data.labtests
                [0]) {
                    console.log('save');

                    return data.labtests[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a labtest
    * @param {labtest} labtest
    *
    * @return Promise<labtest>
    */
    updatelabtest(labtest: labtest) {
        return this.db.rel.save('labtest', labtest)
            .then((data: any) => {
                if (data && data.labtests && data.labtests
                [0]) {
                    console.log('Update')

                    return data.labtests[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a labtest
     * @param {labtest} labtest
     *
     * @return Promise<boolean>
     */
    removelabtest(labtest: labtest): Promise<boolean> {
        return this.db.rel.del('labtest', labtest)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the labtests
     *
     * @return Promise<Array<labtest>>
     */
    getlabtests(): Promise<Array<labtest>> {
        return this.db.rel.find('labtest')
            .then((data: any) => {
                this.labtests = data.labtests;
                if (this.searchText && this.searchText !== '') {
                    this.labtests = FuseUtils.filterArrayByString(this.labtests, this.searchText);
                }
                //this.onlabtestsChanged.next(this.labtests);
                return Promise.resolve(this.labtests);
                //return data.labtests ? data.labtests : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getDoctorlabtests(): Promise<Array<labtest>> {
        return this.db.rel.find('labtest')
            .then((data: any) => {
                return data.labtests ? data.labtests : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a labtest
     * @param {labtest} labtest
     *
     * @return Promise<labtest>
     */
    getlabtest(labtest: labtest): Promise<labtest> {
        return this.db.rel.find('labtest', labtest.id)
            .then((data: any) => {
                console.log("Get")

                return data && data.labtests ? data.labtests[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected labtest by id
     * @param id
     */
    toggleSelectedlabtest(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedlabtests.length > 0) {
            const index = this.selectedlabtests.indexOf(id);

            if (index !== -1) {
                this.selectedlabtests.splice(index, 1);

                // Trigger the next event
                this.onSelectedlabtestsChanged.next(this.selectedlabtests);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedlabtests.push(id);

        // Trigger the next event
        this.onSelectedlabtestsChanged.next(this.selectedlabtests);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllLabtest() {
        if (this.selectedlabtests.length > 0) {
            this.deselectlabtests();
        }
        else {
            this.selectlabtests();
        }
    }

    selectlabtests(filterParameter?, filterValue?) {
        this.selectedlabtests = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedlabtests = [];
            this.labtests.map(labtest => {
                this.selectedlabtests.push(labtest.id);
            });
        }
        else {
            /* this.selectedlabtests.push(...
                 this.labtests.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedlabtestsChanged.next(this.selectedlabtests);
    }





    deselectlabtests() {
        this.selectedlabtests = [];

        // Trigger the next event
        this.onSelectedlabtestsChanged.next(this.selectedlabtests);
    }

    deletelabtest(labtest) {
        this.db.rel.del('labtest', labtest)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const labtestIndex = this.labtests.indexOf(labtest);
                this.labtests.splice(labtestIndex, 1);
                this.onlabtestsChanged.next(this.labtests);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedlabtests() {
        for (const labtestId of this.selectedlabtests) {
            const labtest = this.labtests.find(_labtest => {
                return _labtest.id === labtestId;
            });

            this.db.rel.del('labtest', labtest)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const labtestIndex = this.labtests.indexOf(labtest);
                    this.labtests.splice(labtestIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onlabtestsChanged.next(this.labtests);
        this.deselectlabtests();
    }

    /***********
     * language
     **********/
    /**
     * Save a language
     * @param {language} language
     *
     * @return Promise<language>
     */
    saveLanguage(language: Language): Promise<Language> {
        language.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('language', language)
            .then((data: any) => {
                if (data && data.languages && data.languages
                [0]) {
                    return data.languages[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a language
    * @param {language} language
    *
    * @return Promise<language>
    */
    updateLanguage(language: Language) {

        return this.db.rel.save('language', language)
            .then((data: any) => {
                if (data && data.languages && data.languages
                [0]) {
                    return data.languages[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a language
     * @param {language} language
     *
     * @return Promise<boolean>
     */
    removeLanguage(language: Language): Promise<boolean> {
        return this.db.rel.del('language', language)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the languages
     *
     * @return Promise<Array<language>>
     */
    getLanguages(): Promise<Array<Language>> {
        return this.db.rel.find('language')
            .then((data: any) => {
                this.languages = data.languages;
                if (this.searchText && this.searchText !== '') {
                    this.languages = FuseUtils.filterArrayByString(this.languages, this.searchText);
                }
                //this.onlanguagesChanged.next(this.languages);
                return Promise.resolve(this.languages);
                //return data.languages ? data.languages : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }
    /**
     * Read a language
     * @param {language} language
     *
     * @return Promise<language>
     */
    getLanguage(language: Language): Promise<Language> {
        return this.db.rel.find('language', language.id)
            .then((data: any) => {
                return data && data.languages ? data.languages[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected language by id
     * @param id
     */
    toggleSelectedLanguage(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedLanguages.length > 0) {
            const index = this.selectedLanguages.indexOf(id);

            if (index !== -1) {
                this.selectedLanguages.splice(index, 1);

                // Trigger the next event
                this.onSelectedLanguagesChanged.next(this.selectedLanguages);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedLanguages.push(id);

        // Trigger the next event
        this.onSelectedLanguagesChanged.next(this.selectedLanguages);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllLanguage() {
        if (this.selectedLanguages.length > 0) {
            this.deselectLanguages();
        }
        else {
            this.selectLanguages();
        }
    }

    selectLanguages(filterParameter?, filterValue?) {
        this.selectedLanguages = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedLanguages = [];
            this.languages.map(language => {
                this.selectedLanguages.push(language.id);
            });
        }
        else {
            /* this.selectedlanguages.push(...
                 this.languages.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedLanguagesChanged.next(this.selectedLanguages);
    }





    deselectLanguages() {
        this.selectedLanguages = [];

        // Trigger the next event
        this.onSelectedLanguagesChanged.next(this.selectedLanguages);
    }

    deleteLanguage(language) {
        this.db.rel.del('language', language)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const languageIndex = this.languages.indexOf(language);
                this.languages.splice(languageIndex, 1);
                this.onLanguagesChanged.next(this.languages);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedLanguages() {
        for (const languageId of this.selectedLanguages) {
            const language = this.languages.find(_language => {
                return _language.id === languageId;
            });

            this.db.rel.del('language', language)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const languageIndex = this.languages.indexOf(language);
                    this.languages.splice(languageIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onLanguagesChanged.next(this.languages);
        this.deselectLanguages();
    }

    /**
   * Get all filters
   * @returns {Promise<any>}
   */
    getFilters(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get('api/mail-filters')
                .subscribe(response => {
                    this.filters = response.json().data;

                    this.onFiltersChanged.next(this.filters);
                    resolve(this.filters);
                }, reject);
        });
    }

    /**
     * Get all labels
     * @returns {Promise<any>}
     */
    getLabels(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get('api/mail-labels')
                .subscribe(response => {
                    this.labels = response.json().data;

                    this.onLabelsChanged.next(this.labels);
                    resolve(this.labels);
                }, reject);
        });
    }

    /**
     * Get all mails
     * @returns {Promise<Mail[]>}
     */
    getMails(): Promise<Message[]> {
        if (this.routeParams.labelHandle) {
            return this.getMailsByLabel(this.routeParams.labelHandle);
        }

        if (this.routeParams.filterHandle) {
            return this.getMailsByFilter(this.routeParams.filterHandle);
        }

        return this.getMailsByFolder();

    }

    /***********
    * message
    **********/
    /**
     * Save a message
     * @param {mail} mail
     *
     * @return Promise<message>
     */
    saveMessage(message: Message): Promise<Message> {
        message.id = Math.floor(Date.now()).toString();

        var filess;
        filess

        this.file;

        this.retrieve = (<HTMLInputElement>this.file).files[0];


        message._attachments = this.retrieve;
        if (message._attachments) {
            message.document_name = message._attachments.name;
        }
        message.staff_id = this.localStorageItem.id;


        return this.db.rel.save('message', message)
            .then((data: any) => {

                if (data && data.messages && data.messages
                [0]) {
                    this.db.rel.putAttachment('message', { id: message.id, }, 'file', message._attachments, 'image/png').then(res => {
                        message._attachments = res
                    })

                    return data.messages[0]
                }
                //this.onMessagesChanged.next(this.messages);
                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Get mails by folder
     * @param handle
     * @returns {Promise<Message[]>}
     */
    getMailsByFolder(): Promise<Array<Message>> {
        this.localStorageItem = JSON.parse(localStorage.getItem('user'));
        this.localStorageType = this.localStorageItem.usertype;
        this.localStorageName = this.localStorageItem.name;
        this.localStorageEmail = this.localStorageItem.email;

        //return new Promise((reject,resolve) => {
        return this.db.rel.find('message').then((data: any) => {
            this.mails = data.messages;

            this.onEventsUpdated.next(this.mails)

            this.mails = FuseUtils.filterArrayByString(this.mails, this.searchText)

            /*   this.http.get('api/mail-folders?handle=' + handle)
                 .subscribe(folders => {
                     console.log(handle);
                     this.folderId = folders.json().data[0].id;
                     console.log(this.folderId)
                     this.http.get('api/mail-mails?folder=' + this.folderId)
                         .subscribe(mails => {
 
                             this.mails = mails.json().data.map(mail => {
                                 console.log(mails.json().data)
                                 return new Message(mail);
 
                             });
 
                             this.mails = FuseUtils.filterArrayByString(this.mails, this.searchText);
 
                             this.onMailsChanged.next(this.mails);
 
                            resolve(this.mails);
                             //console.log(this.mails)
                         });  
            //});*/

            if (this.routerSnapShot == "inbox") {
                var email = this.localStorageEmail;

                this.mails = this.mails.filter(data => data.to == email);

                this.onMailsChanged.next(this.mails);

                return Promise.resolve(this.mails);
            }
            else if (this.routerSnapShot == "sent") {
                var email = this.localStorageEmail;
                this.mails = this.mails.filter(data => data.from == email);
                this.onMailsChanged.next(this.mails);
                return Promise.resolve(this.mails);
            }

        }).catch((err: any) => {
            console.error(err);
        })
        // });
    }

    /**
     * Get mails by filter
     * @param handle
     * @returns {Promise<Mail[]>}
     */
    getMailsByFilter(handle): Promise<Message[]> {
        return new Promise((resolve, reject) => {

            this.http.get('api/mail-mails?' + handle + '=true')
                .subscribe(mails => {

                    this.mails = mails.json().data.map(mail => {
                        return new Message(mail);
                    });

                    this.mails = FuseUtils.filterArrayByString(this.mails, this.searchText);

                    this.onMailsChanged.next(this.mails);

                    resolve(this.mails);

                }, reject);
        });
    }

    /**
     * Get mails by label
     * @param handle
     * @returns {Promise<Mail[]>}
     */
    getMailsByLabel(handle): Promise<Message[]> {
        return new Promise((resolve, reject) => {
            this.http.get('api/mail-labels?handle=' + handle)
                .subscribe(labels => {

                    const labelId = labels.json().data[0].id;

                    this.http.get('api/mail-mails?labels=' + labelId)
                        .subscribe(mails => {

                            this.mails = mails.json().data.map(mail => {
                                return new Message(mail);
                            });

                            this.mails = FuseUtils.filterArrayByString(this.mails, this.searchText);

                            this.onMailsChanged.next(this.mails);

                            resolve(this.mails);

                        }, reject);
                });
        });
    }

    /**
     * Toggle selected mail by id
     * @param id
     */
    toggleSelectedMail(id) {
        // First, check if we already have that mail as selected...
        if (this.selectedMails2.length > 0) {
            for (const mail of this.selectedMails) {
                // ...delete the selected mail
                if (mail.id === id) {
                    const index = this.selectedMails2.indexOf(id);

                    if (index !== -1) {
                        this.selectedMails2.splice(index, 1);

                        // Trigger the next event
                        this.onSelectedMailsChanged.next(this.selectedMails2);

                        // Return
                        return;
                    }
                }
            }
        }

        // If we don't have it, push as selected
        this.selectedMails2.push(id);
        /* this.mails.find(mail => {
            return mail.id === id;
        }) */


        // Trigger the next event
        this.onSelectedMailsChanged.next(this.selectedMails2);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllMail2() {
        if (this.selectedMails2.length > 0) {
            this.deselectMails();
        }
        else {
            this.selectMails();
        }

    }


    selectMails(filterParameter?, filterValue?) {
        this.selectedMails = [];

        // If there is no filter, select all mails
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedMails = this.mails;
        }
        else {
            this.selectedMails.push(...
                this.mails.filter(mail => {
                    return mail[filterParameter] === filterValue;
                })
            );
        }

        // Trigger the next event
        this.onSelectedMailsChanged.next(this.selectedMails);
    }

    deselectMails() {
        this.selectedMails2 = [];

        // Trigger the next event
        this.onSelectedMailsChanged.next(this.selectedMails2);
    }

    /**
     * Set current mail by id
     * @param id
     */
    setCurrentMail(id) {

        this.currentMail = this.mails.find(mail => {

            return mail.id === id;
        });

        this.onCurrentMailChanged.next(this.currentMail);

    }

    /**
     * Toggle label on selected mails
     * @param labelId
     */
    toggleLabelOnSelectedMails(labelId) {
        this.selectedMails.map(mail => {

            const index = mail.labels.indexOf(labelId);

            if (index !== -1) {
                mail.labels.splice(index, 1);
            }
            else {
                mail.labels.push(labelId);
            }

            this.updateMail(mail);
        });
    }

    /**
     * Set folder on selected mails
     * @param folderId
     */
    setFolderOnSelectedMails(folderId) {
        this.selectedMails.map(mail => {
            mail.folder = folderId;

            this.updateMail(mail);
        });

        this.deselectMails();
    }

    /**
     * Update the mail
     * @param mail
     * @returns {Promise<any>}
     */
    updateMail(message: Message) {
        return this.db.rel.save('message', message)
            .then((data: any) => {
                if (data && data.messages && data.messages
                [0]) {

                    return data.messages[0]
                }
                //this.onMessagesChanged.next(this.messages);
                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getMail(id): Promise<Message> {
        return this.db.rel.find('message', id)
            .then((data: any) => {
                return data && data.messages ? data.messages[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedMails() {
        for (const mailId of this.selectedMails2) {
            const message = this.mails.find(_message => {

                return _message.id === mailId;
            });

            this.db.rel.del('message', message)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const mailIndex = this.mails.indexOf(message);
                    this.mails.splice(mailIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onMailsChanged.next(this.mails);
        this.deselectMails();
    }

    updateSelectedMails() {
        for (const mailId of this.selectedMails2) {
            const message = this.mails.find(_message => {

                return _message.id === mailId;
            });

            this.db.rel.save('message', message)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const mailIndex = this.mails.indexOf(message);
                    this.mails.splice(mailIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onMailsChanged.next(this.mails);
        this.deselectMails();
    }

    /***********
   * managepayment
   **********/
    /**
     * Save a managepayment
     * @param {managepayment} managepayment
     *
     * @return Promise<managepayment>
     */
    saveManagepayment(managepayment: Managepayment): Promise<Managepayment> {
        managepayment.id = Math.floor(Date.now()).toString();

        //managepayment.doctors = [];
        return this.db.rel.save('managepayment', managepayment)
            .then((data: any) => {

                if (data && data.managepayments && data.managepayments
                [0]) {
                    console.log('save');

                    return data.managepayments[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a managepayment
    * @param {managepayment} managepayment
    *
    * @return Promise<managepayment>
    */
    updateManagepayment(managepayment: Managepayment) {
        return this.db.rel.save('managepayment', managepayment)
            .then((data: any) => {
                if (data && data.managepayments && data.managepayments
                [0]) {
                    console.log('Update')

                    return data.managepayments[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a managepayment
     * @param {managepayment} managepayment
     *
     * @return Promise<boolean>
     */
    removeManagepayment(managepayment: Managepayment): Promise<boolean> {
        return this.db.rel.del('managepayment', managepayment)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the managepayments
     *
     * @return Promise<Array<managepayment>>
     */
    getManagepayments(): Promise<Array<Managepayment>> {
        return this.db.rel.find('managepayment')
            .then((data: any) => {
                this.managepayments = data.managepayments;
                if (this.searchText && this.searchText !== '') {
                    this.managepayments = FuseUtils.filterArrayByString(this.managepayments, this.searchText);
                }
                //this.onmanagepaymentsChanged.next(this.managepayments);
                return Promise.resolve(this.managepayments);
                //return data.managepayments ? data.managepayments : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getDoctorManagepayments(): Promise<Array<Managepayment>> {
        return this.db.rel.find('managepayment')
            .then((data: any) => {
                return data.managepayments ? data.managepayments : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a managepayment
     * @param {managepayment} managepayment
     *
     * @return Promise<managepayment>
     */
    getManagepayment(managepayment: Managepayment): Promise<Managepayment> {
        return this.db.rel.find('managepayment', managepayment.id)
            .then((data: any) => {
                console.log("Get")

                return data && data.managepayments ? data.managepayments[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected managepayment by id
     * @param id
     */
    toggleSelectedManagepayment(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedManagepayments.length > 0) {
            const index = this.selectedManagepayments.indexOf(id);

            if (index !== -1) {
                this.selectedManagepayments.splice(index, 1);

                // Trigger the next event
                this.onSelectedManagepaymentsChanged.next(this.selectedManagepayments);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedManagepayments.push(id);

        // Trigger the next event
        this.onSelectedManagepaymentsChanged.next(this.selectedManagepayments);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllManagepayment() {
        if (this.selectedManagepayments.length > 0) {
            this.deselectManagepayments();
        }
        else {
            this.selectManagepayments();
        }
    }

    selectManagepayments(filterParameter?, filterValue?) {
        this.selectedManagepayments = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedManagepayments = [];
            this.managepayments.map(managepayment => {
                this.selectedManagepayments.push(managepayment.id);
            });
        }
        else {
            /* this.selectedmanagepayments.push(...
                 this.managepayments.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedManagepaymentsChanged.next(this.selectedManagepayments);
    }





    deselectManagepayments() {
        this.selectedManagepayments = [];

        // Trigger the next event
        this.onSelectedManagepaymentsChanged.next(this.selectedManagepayments);
    }

    deleteManagepayment(managepayment) {
        this.db.rel.del('managepayment', managepayment)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const managepaymentIndex = this.managepayments.indexOf(managepayment);
                this.managepayments.splice(managepaymentIndex, 1);
                this.onManagepaymentsChanged.next(this.managepayments);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedManagepayments() {
        for (const managepaymentId of this.selectedManagepayments) {
            const managepayment = this.managepayments.find(_managepayment => {
                return _managepayment.id === managepaymentId;
            });

            this.db.rel.del('managepayment', managepayment)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const managepaymentIndex = this.managepayments.indexOf(managepayment);
                    this.managepayments.splice(managepaymentIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onManagepaymentsChanged.next(this.managepayments);
        this.deselectManagepayments();
    }

    /***********
     * medicine_category
     **********/
    /**
     * Save a medicine_category
     * @param {medicine_category} medicine_category
     *
     * @return Promise<medicine_category>
     */
    saveMedicine_category(medicine_category: Medicine_category): Promise<Medicine_category> {
        medicine_category.id = Math.floor(Date.now()).toString();

        medicine_category.medicines = [];
        return this.db.rel.save('medicine_category', medicine_category)
            .then((data: any) => {

                if (data && data.medicine_categorys && data.medicine_categorys
                [0]) {
                    return data.medicine_categorys[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a medicine_category
    * @param {medicine_category} medicine_category
    *
    * @return Promise<medicine_category>
    */
    updateMedicine_category(medicine_category: Medicine_category) {

        return this.db.rel.save('medicine_category', medicine_category)
            .then((data: any) => {
                if (data && data.medicine_categorys && data.medicine_categorys
                [0]) {
                    return data.medicine_categorys[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a medicine_category
     * @param {medicine_category} medicine_category
     *
     * @return Promise<boolean>
     */
    removeMedicine_category(medicine_category: Medicine_category): Promise<boolean> {
        return this.db.rel.del('medicine_category', medicine_category)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the medicine_categorys
     *
     * @return Promise<Array<medicine_category>>
     */
    getMedicine_categorys(): Promise<Array<Medicine_category>> {
        return this.db.rel.find('medicine_category')
            .then((data: any) => {
                this.medicine_categorys = data.medicine_categorys;
                if (this.searchText && this.searchText !== '') {
                    this.medicine_categorys = FuseUtils.filterArrayByString(this.medicine_categorys, this.searchText);
                }
                //this.onmedicine_categorysChanged.next(this.medicine_categorys);
                return Promise.resolve(this.medicine_categorys);
                //return data.medicine_categorys ? data.medicine_categorys : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getMedMedicine_categorys(): Promise<Array<Medicine_category>> {
        return this.db.rel.find('medicine_category')
            .then((data: any) => {
                return data.medicine_categorys ? data.medicine_categorys : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a medicine_category
     * @param {medicine_category} medicine_category
     *
     * @return Promise<medicine_category>
     */
    getMedicine_category(medicine_category: Medicine_category): Promise<Medicine_category> {
        return this.db.rel.find('medicine_category', medicine_category.id)
            .then((data: any) => {
                return data && data.medicine_categorys ? data.medicine_categorys[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected medicine_category by id
     * @param id
     */
    toggleSelectedMedicine_category(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedMedicine_categorys.length > 0) {
            const index = this.selectedMedicine_categorys.indexOf(id);

            if (index !== -1) {
                this.selectedMedicine_categorys.splice(index, 1);

                // Trigger the next event
                this.onSelectedMedicine_categorysChanged.next(this.selectedMedicine_categorys);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedMedicine_categorys.push(id);

        // Trigger the next event
        this.onSelectedMedicine_categorysChanged.next(this.selectedMedicine_categorys);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllMedicine_category() {
        if (this.selectedMedicine_categorys.length > 0) {
            this.deselectMedicine_categorys();
        }
        else {
            this.selectMedicine_categorys();
        }
    }

    selectMedicine_categorys(filterParameter?, filterValue?) {
        this.selectedMedicine_categorys = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedMedicine_categorys = [];
            this.medicine_categorys.map(medicine_category => {
                this.selectedMedicine_categorys.push(medicine_category.id);
            });
        }
        else {
            /* this.selectedmedicine_categorys.push(...
                 this.medicine_categorys.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedMedicine_categorysChanged.next(this.selectedMedicine_categorys);
    }





    deselectMedicine_categorys() {
        this.selectedMedicine_categorys = [];

        // Trigger the next event
        this.onSelectedMedicine_categorysChanged.next(this.selectedMedicine_categorys);
    }

    deleteMedicine_category(medicine_category) {
        this.db.rel.del('medicine_category', medicine_category)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const medicine_categoryIndex = this.medicine_categorys.indexOf(medicine_category);
                this.medicine_categorys.splice(medicine_categoryIndex, 1);
                this.onMedicine_categorysChanged.next(this.medicine_categorys);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedMedicine_categorys() {
        for (const medicine_categoryId of this.selectedMedicine_categorys) {
            const medicine_category = this.medicine_categorys.find(_medicine_category => {
                return _medicine_category.id === medicine_categoryId;
            });

            this.db.rel.del('medicine_category', medicine_category)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const medicine_categoryIndex = this.medicine_categorys.indexOf(medicine_category);
                    this.medicine_categorys.splice(medicine_categoryIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onMedicine_categorysChanged.next(this.medicine_categorys);
        this.deselectMedicine_categorys();
    }


    /***********
     * medicine_sale
     **********/
    /**
     * Save a medicine_sale
     * @param {medicine_sale} medicine_sale
     *
     * @return Promise<medicine_sale>
     */
    saveMedicine_sale(medicine_sale: Medicine_sale): Promise<Medicine_sale> {
        medicine_sale.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('medicine_sale', medicine_sale)
            .then((data: any) => {

                if (data && data.medicine_sales && data.medicine_sales
                [0]) {
                    return data.medicine_sales[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a medicine_sale
    * @param {medicine_sale} medicine_sale
    *
    * @return Promise<medicine_sale>
    */
    updateMedicine_sale(medicine_sale: Medicine_sale) {

        return this.db.rel.save('medicine_sale', medicine_sale)
            .then((data: any) => {
                if (data && data.medicine_sales && data.medicine_sales
                [0]) {
                    return data.medicine_sales[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a medicine_sale
     * @param {medicine_sale} medicine_sale
     *
     * @return Promise<boolean>
     */
    removeMedicine_sale(medicine_sale: Medicine_sale): Promise<boolean> {
        return this.db.rel.del('medicine_sale', medicine_sale)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the medicine_sales
     *
     * @return Promise<Array<medicine_sale>>
     */
    getMedicine_sales(): Promise<Array<Medicine_sale>> {
        return this.db.rel.find('medicine_sale')
            .then((data: any) => {
                this.medicine_sales = data.medicine_sales;
                if (this.searchText && this.searchText !== '') {
                    this.medicine_sales = FuseUtils.filterArrayByString(this.medicine_sales, this.searchText);
                }
                //this.onmedicine_salesChanged.next(this.medicine_sales);
                return Promise.resolve(this.medicine_sales);
                //return data.medicine_sales ? data.medicine_sales : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a medicine_sale
     * @param {medicine_sale} medicine_sale
     *
     * @return Promise<medicine_sale>
     */
    getMedicine_sale(id): Promise<Medicine_sale> {
        return this.db.rel.find('medicine_sale', id)
            .then((data: any) => {
                return data && data.medicine_sales ? data.medicine_sales[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected medicine_sale by id
     * @param id
     */
    toggleSelectedMedicine_sale(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedMedicine_sales.length > 0) {
            const index = this.selectedMedicine_sales.indexOf(id);

            if (index !== -1) {
                this.selectedMedicine_sales.splice(index, 1);

                // Trigger the next event
                this.onSelectedMedicine_salesChanged.next(this.selectedMedicine_sales);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedMedicine_sales.push(id);

        // Trigger the next event
        this.onSelectedMedicine_salesChanged.next(this.selectedMedicine_sales);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllMedicine_sales() {
        if (this.selectedMedicine_sales.length > 0) {
            this.deselectMedicine_sales();
        }
        else {
            this.selectMedicine_sales();
        }
    }

    selectMedicine_sales(filterParameter?, filterValue?) {
        this.selectedMedicine_sales = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedMedicine_sales = [];
            this.medicine_sales.map(medicine_sale => {
                this.selectedMedicine_sales.push(medicine_sale.id);
            });
        }
        else {
            /* this.selectedmedicine_sales.push(...
                 this.medicine_sales.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedMedicine_salesChanged.next(this.selectedMedicine_sales);
    }





    deselectMedicine_sales() {
        this.selectedMedicine_sales = [];

        // Trigger the next event
        this.onSelectedMedicine_salesChanged.next(this.selectedMedicine_sales);
    }

    deleteMedicine_sale(medicine_sale) {
        this.db.rel.del('medicine_sale', medicine_sale)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const medicine_saleIndex = this.medicine_sales.indexOf(medicine_sale);
                this.medicine_sales.splice(medicine_saleIndex, 1);
                this.onMedicine_salesChanged.next(this.medicine_sales);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedMedicine_sales() {
        for (const medicine_saleId of this.selectedMedicine_sales) {
            const medicine_sale = this.medicine_sales.find(_medicine_sale => {
                return _medicine_sale.id === medicine_saleId;
            });

            this.db.rel.del('medicine_sale', medicine_sale)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const medicine_saleIndex = this.medicine_sales.indexOf(medicine_sale);
                    this.medicine_sales.splice(medicine_saleIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onMedicine_salesChanged.next(this.medicine_sales);
        this.deselectMedicine_sales();
    }


    /***********
     * medicine
     **********/
    /**
     * Save a medicine
     * @param {medicine} medicine
     *
     * @return Promise<medicine>
     */
    saveMedicine(medicine: Medicine): Promise<Medicine> {
        medicine.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('medicine', medicine)
            .then((data: any) => {

                if (data && data.medicines && data.medicines
                [0]) {
                    return data.medicines[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a medicine
    * @param {medicine} medicine
    *
    * @return Promise<medicine>
    */
    updateMedicine(medicine: Medicine) {
        return this.db.rel.save('medicine', medicine)
            .then((data: any) => {
                if (data && data.medicines && data.medicines
                [0]) {
                    return data.medicines[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a medicine
     * @param {medicine} medicine
     *
     * @return Promise<boolean>
     */
    removeMedicine(medicine: Medicine): Promise<boolean> {
        return this.db.rel.del('medicine', medicine)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the medicines
     *
     * @return Promise<Array<medicine>>
     */
    getMedicines(): Promise<Array<Medicine>> {
        return this.db.rel.find('medicine')
            .then((data: any) => {
                this.medicines = data.medicines;
                if (this.searchText && this.searchText !== '') {
                    this.medicines = FuseUtils.filterArrayByString(this.medicines, this.searchText);
                }
                //this.onmedicinesChanged.next(this.medicines);
                return Promise.resolve(this.medicines);
                //return data.medicines ? data.medicines : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a medicine
     * @param {medicine} medicine
     *
     * @return Promise<medicine>
     */
    getMedicine(medicine: Medicine): Promise<Medicine> {
        return this.db.rel.find('medicine', medicine)
            .then((data: any) => {
                return data && data.medicines ? data.medicines[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getMedicine2(id): Promise<Medicine> {
        return this.db.rel.find('medicine', id)
            .then((data: any) => {
                return data && data.medicines ? data.medicines[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Toggle selected medicine by id
     * @param id
     */
    toggleSelectedMedicine(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedMedicines.length > 0) {
            const index = this.selectedMedicines.indexOf(id);

            if (index !== -1) {
                this.selectedMedicines.splice(index, 1);

                // Trigger the next event
                this.onSelectedMedicinesChanged.next(this.selectedMedicines);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedMedicines.push(id);

        // Trigger the next event
        this.onSelectedMedicinesChanged.next(this.selectedMedicines);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllMedicine() {
        if (this.selectedMedicines.length > 0) {
            this.deselectMedicines();
        }
        else {
            this.selectMedicines();
        }
    }

    selectMedicines(filterParameter?, filterValue?) {
        this.selectedMedicines = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedMedicines = [];
            this.medicines.map(medicine => {
                this.selectedMedicines.push(medicine.id);
            });
        }
        else {
            /* this.selectedmedicines.push(...
                 this.medicines.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedMedicinesChanged.next(this.selectedMedicines);
    }





    deselectMedicines() {
        this.selectedMedicines = [];

        // Trigger the next event
        this.onSelectedMedicinesChanged.next(this.selectedMedicines);
    }

    deleteMedicine(medicine) {
        this.db.rel.del('medicine', medicine)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const medicineIndex = this.medicines.indexOf(medicine);
                this.medicines.splice(medicineIndex, 1);
                this.onMedicinesChanged.next(this.medicines);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedMedicines() {
        for (const medicineId of this.selectedMedicines) {
            const medicine = this.medicines.find(_medicine => {

                return _medicine.id === medicineId;
            });

            this.db.rel.del('medicine', medicine)
                .then((data: any) => {

                    //return data && data.deleted ? data.deleted: false;
                    const medicineIndex = this.medicines.indexOf(medicine);
                    this.medicines.splice(medicineIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onMedicinesChanged.next(this.medicines);
        this.deselectMedicines();
    }

    /***********
   * message_thread
   **********/
    /**
     * Save a message_thread
     * @param {message_thread} message_thread
     *
     * @return Promise<message_thread>
     */
    saveMessage_thread(message_thread: Message_thread): Promise<Message_thread> {
        message_thread.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('message_thread', message_thread)
            .then((data: any) => {

                if (data && data.message_threads && data.message_threads
                [0]) {
                    return data.message_threads[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a message_thread
    * @param {message_thread} message_thread
    *
    * @return Promise<message_thread>
    */
    updateMessage_thread(message_thread: Message_thread) {

        return this.db.rel.save('message_thread', message_thread)
            .then((data: any) => {
                if (data && data.message_threads && data.message_threads
                [0]) {
                    return data.message_threads[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a message_thread
     * @param {message_thread} message_thread
     *
     * @return Promise<boolean>
     */
    removeMessage_thread(message_thread: Message_thread): Promise<boolean> {
        return this.db.rel.del('message_thread', message_thread)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the message_threads
     *
     * @return Promise<Array<message_thread>>
     */
    getMessage_threads(): Promise<Array<Message_thread>> {
        return this.db.rel.find('message_thread')
            .then((data: any) => {
                this.message_threads = data.message_threads;
                if (this.searchText && this.searchText !== '') {
                    this.message_threads = FuseUtils.filterArrayByString(this.message_threads, this.searchText);
                }
                //this.onmessage_threadsChanged.next(this.message_threads);
                return Promise.resolve(this.message_threads);
                //return data.message_threads ? data.message_threads : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a message_thread
     * @param {message_thread} message_thread
     *
     * @return Promise<message_thread>
     */
    getMessage_thread(message_thread: Message_thread): Promise<Message_thread> {
        return this.db.rel.find('message_thread', message_thread.id)
            .then((data: any) => {
                return data && data.message_threads ? data.message_threads[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected message_thread by id
     * @param id
     */
    toggleSelectedMessage_thread(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedMessage_threads.length > 0) {
            const index = this.selectedMessage_threads.indexOf(id);

            if (index !== -1) {
                this.selectedMessage_threads.splice(index, 1);

                // Trigger the next event
                this.onSelectedMessage_threadsChanged.next(this.selectedMessage_threads);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedMessage_threads.push(id);

        // Trigger the next event
        this.onSelectedMessage_threadsChanged.next(this.selectedMessage_threads);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllMessagethread() {
        if (this.selectedMessage_threads.length > 0) {
            this.deselectMessage_threads();
        }
        else {
            this.selectMessage_threads();
        }
    }

    selectMessage_threads(filterParameter?, filterValue?) {
        this.selectedMessage_threads = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedMessage_threads = [];
            this.message_threads.map(message_thread => {
                this.selectedMessage_threads.push(message_thread.id);
            });
        }
        else {
            /* this.selectedmessage_threads.push(...
                 this.message_threads.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedMessage_threadsChanged.next(this.selectedMessage_threads);
    }





    deselectMessage_threads() {
        this.selectedMessage_threads = [];

        // Trigger the next event
        this.onSelectedMessage_threadsChanged.next(this.selectedMessage_threads);
    }

    deleteMessage_thread(message_thread) {
        this.db.rel.del('message_thread', message_thread)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const message_threadIndex = this.message_threads.indexOf(message_thread);
                this.message_threads.splice(message_threadIndex, 1);
                this.onMessage_threadsChanged.next(this.message_threads);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedMessage_threads() {
        for (const message_threadId of this.selectedMessage_threads) {
            const message_thread = this.message_threads.find(_message_thread => {
                return _message_thread.id === message_threadId;
            });

            this.db.rel.del('message_thread', message_thread)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const message_threadIndex = this.message_threads.indexOf(message_thread);
                    this.message_threads.splice(message_threadIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onMessage_threadsChanged.next(this.message_threads);
        this.deselectMessage_threads();
    }


    //get Title and Icon of Folders
    /**
   * Get all folders
   * @returns {Promise<any>}
   */
    getFolders(): Promise<any> {
        /*  return new Promise((resolve, reject) => {
             var jsonData = [
                 { id: 0, handle: "inbox", title: "Inbox", icon: "inbox" },
                 { id: 1, handle: "sent", title: "Sent", icon: "send" },
                 { id: 2, handle: "drafts", title: "Drafts", icon: "email_open" },
                 { id: 3, handle: "spam", title: "Spam", icon: "error" },
                 { id: 4, handle: "trash", title: "Trash", icon: "delete" }]
    
             this.folders = jsonData;
             console.log(this.folders);
             this.onFoldersChanged.next(this.folders);
             resolve(this.folders);
    
         }); */
        return new Promise((resolve, reject) => {
            this.http.get('api/mail-folders')
                .subscribe(response => {
                    this.folders = response.json().data;
                    this.onFoldersChanged.next(this.folders);
                    resolve(this.folders);
                }, reject);
        });
    }

    /**
    * Get all filters
    * @returns {Promise<any>}
    */
    getFilters2(): Promise<any> {
        /*  return new Promise((resolve, reject) => {
             var jsonData = [
                 { id: 0, handle: "starred", title: "Starred", icon: "star" },
                 { id: 1, handle: "important", title: "Important", icon: "label" }]
    
             this.filters = jsonData;
             console.log(this.filters);
             this.onFiltersChanged.next(this.filters);
             resolve(this.filters);
    
         });*/

        return new Promise((resolve, reject) => {
            this.http.get('api/mail-filters')
                .subscribe(response => {
                    this.filters = response.json().data;
                    console.log(this.filters);
                    this.onFiltersChanged.next(this.filters);
                    resolve(this.filters);
                }, reject);
        });
    }


    /**
    * Get all labels
    * @returns {Promise<any>}
    */
    getLabels2(): Promise<any> {
        /*   return new Promise((resolve, reject) => {
              var jsonData = [
                  { id: 0, handle: "note", title: "Note", color: "#7cb342" },
                  { id: 1, handle: "paypal", title: "Paypal", color: "#d84315" },
                  { id: 2, handle: "invoice", title: "Invoice", color: "#607d8b" },
                  { id: 3, handle: "amazon", title: "Amazon", color: "#03a9f4" }]
    
              this.labels = jsonData;
              console.log(this.labels);
              this.onLabelsChanged.next(this.labels);
              resolve(this.labels);
    
          }); */
        return new Promise((resolve, reject) => {
            this.http.get('api/mail-labels')
                .subscribe(response => {
                    this.labels = response.json().data;
                    console.log(this.labels);
                    this.onLabelsChanged.next(this.labels);
                    resolve(this.labels);
                }, reject);
        });
    }

    /***********
     * message
     **********/
    /**
     * Save a message
     * @param {message} message
     *
     * @return Promise<message>
     */
    saveMessage2(message: Message): Promise<Message> {
        message.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('message', message)
            .then((data: any) => {

                if (data && data.messages && data.messages
                [0]) {
                    console.log(data.messages[0])
                    return data.messages[0]
                }
                //this.onMessagesChanged.next(this.messages);
                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a message
    * @param {message} message
    *
    * @return Promise<message>
    */
    updateMessage(message: Message) {

        return this.db.rel.save('message', message)
            .then((data: any) => {
                if (data && data.messages && data.messages
                [0]) {
                    return data.messages[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a message
     * @param {message} message
     *
     * @return Promise<boolean>
     */
    removeMessage(message: Message): Promise<boolean> {
        return this.db.rel.del('message', message)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Get all mails
     * @returns {Promise<Mail[]>}
     */
    getMessages(): Promise<Message[]> {
        /*      if (this.routeParams.labelHandle) { 
               return this.getMessagesByLabel(this.routeParams.labelHandle);
           }
    
           if (this.routeParams.filterHandle) {
               return this.getMessagesByFilter(this.routeParams.filterHandle);
           }   */
        //console.log(this.route.snapshot.params['inbox']);
        return this.getMessagesByFolder();
    }


    getMessagesByFolder(): Promise<Array<Message>> {
        return this.db.rel.find('message')
            .then((data: any) => {
                console.log(this.folders[1].handle)
                this.messages = data.messages;
                this.folderId = this.folders[0].id
                console.log(this.folders[0].id);
                //this.onEventsUpdated.next(this.messages);
                //this.onMessagesChanged.next(this.messages);
                if (this.searchText && this.searchText !== '') {
                    this.messages = FuseUtils.filterArrayByString(this.messages, this.searchText);

                }

                console.log(this.messages);
                this.onMessagesChanged.next(this.messages);
                return Promise.resolve(this.messages);
                //return data.messages ? data.messages : [];
            }).catch((err: any) => {
                console.error(err);
            });
        /*   return new Promise((resolve, reject) => {
    
        this.http.get('api/mail-folders?handle=' + handle)
            .subscribe(folders => {
                console.log(handle);
                this.folderId = folders.json().data[0].id;
                console.log(this.folderId)
                this.http.get('api/mail-mails?folder=' + this.folderId)
                    .subscribe(mails => {
    
                        this.messages = mails.json().data.map(mail => {
                            console.log(mails.json().data)
                            return new Message(mail);
    
                        });
    
                        this.messages = FuseUtils.filterArrayByString(this.messages, this.searchText);
    
                        this.onMessagesChanged.next(this.messages);
    
                        resolve(this.messages);
                        //console.log(this.mails)
                    }, reject);
            });
    }); */
    }

    /**
    * Get mails by filter
    * @param handle
    * @returns {Promise<Mail[]>}
    */
    getMessagesByFilter(handle): Promise<Message[]> {
        return this.db.rel.find('message')
            .then((data: any) => {
                this.folderId = 0;
                this.messages = data.messages + this.folderId;
                //this.onEventsUpdated.next(this.messages);
                //this.onMessagesChanged.next(this.messages);
                if (this.searchText && this.searchText !== '') {
                    this.messages = FuseUtils.filterArrayByString(this.messages, this.searchText);

                }

                console.log(this.messages);
                this.onMessagesChanged.next(this.messages);
                return Promise.resolve(this.messages);
                //return data.messages ? data.messages : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Get mails by label
    * @param handle
    * @returns {Promise<Mail[]>}
    */
    getMessagesByLabel(handle): Promise<Message[]> {
        return this.db.rel.find('message')
            .then((data: any) => {
                this.folderId = 0;
                this.messages = data.messages + this.folderId;
                //this.onEventsUpdated.next(this.messages);
                //this.onMessagesChanged.next(this.messages);
                if (this.searchText && this.searchText !== '') {
                    this.messages = FuseUtils.filterArrayByString(this.messages, this.searchText);

                }

                console.log(this.messages);
                this.onMessagesChanged.next(this.messages);
                return Promise.resolve(this.messages);
                //return data.messages ? data.messages : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the messages
     *
     * @return Promise<Array<message>>
     */
    /*  getMessages(): Promise<Array<Message>> {
        return this.db.rel.find('message')
            .then((data: any) => {
                this.messages = data.messages;
                this.onEventsUpdated.next(this.messages);
                //this.onMessagesChanged.next(this.messages);
                if (this.searchText && this.searchText !== '') {
                    this.messages = FuseUtils.filterArrayByString(this.messages, this.searchText);
    
                }
                
                console.log(this.messages);
                //this.onmessagesChanged.next(this.messages);
                return Promise.resolve(this.messages);
                //return data.messages ? data.messages : [];
            }).catch((err: any) => {
                console.error(err);
            });
    } 
    */
    /**
     * Read a message
     * @param {message} message
     *
     * @return Promise<message>
     */
    getMessage(message: Message): Promise<Message> {
        return this.db.rel.find('message', message.id)
            .then((data: any) => {
                return data && data.messages ? data.messages[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected message by id
     * @param id
     */
    toggleSelectedMessage(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedMessages.length > 0) {
            const index = this.selectedMessages.indexOf(id);

            if (index !== -1) {
                this.selectedMessages.splice(index, 1);

                // Trigger the next event
                this.onSelectedMessagesChanged.next(this.selectedMessages);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedMessages.push(id);

        // Trigger the next event
        this.onSelectedMessagesChanged.next(this.selectedMessages);
    }

    setCurrentMessage(id) {
        this.currentMessage = this.messages.find(mail => {
            console.log(mail.id)
            return mail.id === id;
        });

        this.onCurrentMessageChanged.next(this.currentMessage);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllMessage() {
        if (this.selectedMessages.length > 0) {
            this.deselectMessages();
        }
        else {
            this.selectMessages();
        }
    }

    selectMessages(filterParameter?, filterValue?) {
        this.selectedMessages = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedMessages = [];
            this.messages.map(message => {
                this.selectedMessages.push(message.id);
            });
        }
        else {
            /* this.selectedmessages.push(...
                 this.messages.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedMessagesChanged.next(this.selectedMessages);
    }

    toggleLabelOnSelectedMessages(labelId) {
        this.selectedMessages2.map(message => {

            const index = message.labels.indexOf(labelId);

            if (index !== -1) {
                message.labels.splice(index, 1);
            }
            else {
                message.labels.push(labelId);
            }

            this.updateMessage(message);
        });
    }

    /**
     * Set folder on selected mails
     * @param folderId
     */
    setFolderOnSelectedMessages(folderId) {
        this.selectedMessages2.map(message => {
            message.folder = folderId;

            this.updateMessage(message);
        });

        this.deselectMessages();
    }



    deselectMessages() {
        this.selectedMessages = [];

        // Trigger the next event
        this.onSelectedMessagesChanged.next(this.selectedMessages);
    }

    deleteMessage(message) {
        this.db.rel.del('message', message)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const messageIndex = this.messages.indexOf(message);
                this.messages.splice(messageIndex, 1);
                this.onMessagesChanged.next(this.messages);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedMessages() {
        for (const messageId of this.selectedMessages) {
            const message = this.messages.find(_message => {
                return _message.id === messageId;
            });

            this.db.rel.del('message', message)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const messageIndex = this.messages.indexOf(message);
                    this.messages.splice(messageIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onMessagesChanged.next(this.messages);
        this.deselectMessages();
    }

    /***********
        * note
        **********/
    /**
     * Save a note
     * @param {note} note
     *
     * @return Promise<note>
     */
    saveNote(note: Note): Promise<Note> {
        note.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('note', note)
            .then((data: any) => {

                if (data && data.notes && data.notes
                [0]) {
                    return data.notes[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a note
    * @param {note} note
    *
    * @return Promise<note>
    */
    updateNote(note: Note) {

        return this.db.rel.save('note', note)
            .then((data: any) => {
                if (data && data.notes && data.notes
                [0]) {
                    return data.notes[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a note
     * @param {note} note
     *
     * @return Promise<boolean>
     */
    removeNote(note: Note): Promise<boolean> {
        return this.db.rel.del('note', note)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the notes
     *
     * @return Promise<Array<note>>
     */
    getNotes(): Promise<Array<Note>> {
        return this.db.rel.find('note')
            .then((data: any) => {
                this.notes = data.notes;
                if (this.searchText && this.searchText !== '') {
                    this.notes = FuseUtils.filterArrayByString(this.notes, this.searchText);
                }
                //this.onnotesChanged.next(this.notes);
                return Promise.resolve(this.notes);
                //return data.notes ? data.notes : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a note
     * @param {note} note
     *
     * @return Promise<note>
     */
    getNote(note: Note): Promise<Note> {
        return this.db.rel.find('note', note.id)
            .then((data: any) => {
                return data && data.notes ? data.notes[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected note by id
     * @param id
     */
    toggleSelectedNote(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedNotes.length > 0) {
            const index = this.selectedNotes.indexOf(id);

            if (index !== -1) {
                this.selectedNotes.splice(index, 1);

                // Trigger the next event
                this.onSelectedNotesChanged.next(this.selectedNotes);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedNotes.push(id);

        // Trigger the next event
        this.onSelectedNotesChanged.next(this.selectedNotes);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllNote() {
        if (this.selectedNotes.length > 0) {
            this.deselectNotes();
        }
        else {
            this.selectNotes();
        }
    }

    selectNotes(filterParameter?, filterValue?) {
        this.selectedNotes = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedNotes = [];
            this.notes.map(note => {
                this.selectedNotes.push(note.id);
            });
        }
        else {
            /* this.selectednotes.push(...
                 this.notes.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedNotesChanged.next(this.selectedNotes);
    }





    deselectNotes() {
        this.selectedNotes = [];

        // Trigger the next event
        this.onSelectedNotesChanged.next(this.selectedNotes);
    }

    deleteNote(note) {
        this.db.rel.del('note', note)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const noteIndex = this.notes.indexOf(note);
                this.notes.splice(noteIndex, 1);
                this.onNotesChanged.next(this.notes);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedNotes() {
        for (const noteId of this.selectedNotes) {
            const note = this.notes.find(_note => {
                return _note.id === noteId;
            });

            this.db.rel.del('note', note)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const noteIndex = this.notes.indexOf(note);
                    this.notes.splice(noteIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onNotesChanged.next(this.notes);
        this.deselectNotes();
    }

    /***********
   * notice
   **********/
    /**
     * Save a notice
     * @param {notice} notice
     *
     * @return Promise<notice>
     */
    saveNotice(notice: Notice): Promise<Notice> {
        notice.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('notice', notice)
            .then((data: any) => {

                if (data && data.notices && data.notices
                [0]) {

                    return data.notices[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a notice
    * @param {notice} notice
    *
    * @return Promise<notice>
    */
    updateNotice(notice: Notice) {

        return this.db.rel.save('notice', notice)
            .then((data: any) => {
                if (data && data.notices && data.notices
                [0]) {

                    return data.notices[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    updateNotices(notice: Promise<Array<Notice>>) {

        return this.db.rel.save('notice', notice)
            .then((data: any) => {
                if (data && data.notices && data.notices
                [0]) {
                    return data.notices[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a notice
     * @param {notice} notice
     *
     * @return Promise<boolean>
     */
    removeNotice(notice: Notice): Promise<boolean> {
        return this.db.rel.del('notice', notice)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the notices
     *
     * @return Promise<Array<notice>>
     */
    getNotices(): Promise<Array<Notice>> {
        return this.db.rel.find('notice')
            .then((data: any) => {
                this.notices = data.notices;

                this.onEventsUpdated.next(this.notices);
                if (this.searchText && this.searchText !== '') {
                    this.notices = FuseUtils.filterArrayByString(this.notices, this.searchText);
                }
                //this.onnoticesChanged.next(this.notices);
                return Promise.resolve(this.notices);
                //return data.notices ? data.notices : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Read a notice
     * @param {notice} notice
     *
     * @return Promise<notice>
     */
    getNotice(notice: Notice): Promise<Notice> {
        return this.db.rel.find('notice', notice.id)
            .then((data: any) => {
                //this.onEventsUpdated.next(this.notices);
                return data && data.notices ? data.notices[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected notice by id
     * @param id
     */
    toggleSelectedNotice(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedNotices.length > 0) {
            const index = this.selectedNotices.indexOf(id);

            if (index !== -1) {
                this.selectedNotices.splice(index, 1);

                // Trigger the next event
                this.onSelectedNoticesChanged.next(this.selectedNotices);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedNotices.push(id);

        // Trigger the next event
        this.onSelectedNoticesChanged.next(this.selectedNotices);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllNotice() {
        if (this.selectedNotices.length > 0) {
            this.deselectNotices();
        }
        else {
            this.selectNotices();
        }
    }

    selectNotices(filterParameter?, filterValue?) {
        this.selectedNotices = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedNotices = [];
            this.notices.map(notice => {
                this.selectedNotices.push(notice.id);
            });
        }
        else {
            /* this.selectednotices.push(...
                 this.notices.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedNoticesChanged.next(this.selectedNotices);
    }





    deselectNotices() {
        this.selectedNotices = [];

        // Trigger the next event
        this.onSelectedNoticesChanged.next(this.selectedNotices);
    }

    deleteNotice(notice) {
        this.db.rel.del('notice', notice)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const noticeIndex = this.notices.indexOf(notice);
                this.notices.splice(noticeIndex, 1);
                this.onNoticesChanged.next(this.notices);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedNotices() {
        for (const noticeId of this.selectedNotices) {
            const notice = this.notices.find(_notice => {
                return _notice.id === noticeId;
            });

            this.db.rel.del('notice', notice)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const noticeIndex = this.notices.indexOf(notice);
                    this.notices.splice(noticeIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onNoticesChanged.next(this.notices);
        this.deselectNotices();
    }

    /***********
   * nurse
   **********/
    /**
     * Save a nurse
     * @param {nurse} nurse
     *
     * @return Promise<nurse>
     */
    saveNurse(nurse: Nurse): Promise<Nurse> {
        nurse.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('nurse', nurse)
            .then((data: any) => {

                if (data && data.nurses && data.nurses
                [0]) {
                    return data.nurses[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a nurse
    * @param {nurse} nurse
    *
    * @return Promise<nurse>
    */
    updateNurse(nurse: Nurse) {

        return this.db.rel.save('nurse', nurse)
            .then((data: any) => {
                if (data && data.nurses && data.nurses
                [0]) {
                    return data.nurses[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a nurse
     * @param {nurse} nurse
     *
     * @return Promise<boolean>
     */
    removeNurse(nurse: Nurse): Promise<boolean> {
        return this.db.rel.del('nurse', nurse)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the nurses
     *
     * @return Promise<Array<nurse>>
     */
    getNurses(): Promise<Array<Nurse>> {
        return this.db.rel.find('nurse')
            .then((data: any) => {
                this.nurses = data.nurses;
                if (this.searchText && this.searchText !== '') {
                    this.nurses = FuseUtils.filterArrayByString(this.nurses, this.searchText);
                }
                //this.onnursesChanged.next(this.nurses);
                return Promise.resolve(this.nurses);
                //return data.nurses ? data.nurses : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a nurse
     * @param {nurse} nurse
     *
     * @return Promise<nurse>
     */
    getNurse(nurse: Nurse): Promise<Nurse> {
        return this.db.rel.find('nurse', nurse.id)
            .then((data: any) => {
                return data && data.nurses ? data.nurses[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected nurse by id
     * @param id
     */
    toggleSelectedNurse(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedNurses.length > 0) {
            const index = this.selectedNurses.indexOf(id);

            if (index !== -1) {
                this.selectedNurses.splice(index, 1);

                // Trigger the next event
                this.onSelectedNursesChanged.next(this.selectedNurses);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedNurses.push(id);

        // Trigger the next event
        this.onSelectedNursesChanged.next(this.selectedNurses);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllNurse() {
        if (this.selectedNurses.length > 0) {
            this.deselectNurses();
        }
        else {
            this.selectNurses();
        }
    }

    selectNurses(filterParameter?, filterValue?) {
        this.selectedNurses = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedNurses = [];
            this.nurses.map(nurse => {
                this.selectedNurses.push(nurse.id);
            });
        }
        else {
            /* this.selectednurses.push(...
                 this.nurses.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedNursesChanged.next(this.selectedNurses);
    }





    deselectNurses() {
        this.selectedNurses = [];

        // Trigger the next event
        this.onSelectedNursesChanged.next(this.selectedNurses);
    }

    deleteNurse(nurse) {
        this.db.rel.del('nurse', nurse)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const nurseIndex = this.nurses.indexOf(nurse);
                this.nurses.splice(nurseIndex, 1);
                this.onNursesChanged.next(this.nurses);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedNurses() {
        for (const nurseId of this.selectedNurses) {
            const nurse = this.nurses.find(_nurse => {
                return _nurse.id === nurseId;
            });

            this.db.rel.del('nurse', nurse)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const nurseIndex = this.nurses.indexOf(nurse);
                    this.nurses.splice(nurseIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onNursesChanged.next(this.nurses);
        this.deselectNurses();
    }

    /***********
    * offline
    **********/
    /**
     * Save a offline
     * @param {offline} offline
     *
     * @return Promise<offline>
     */
    saveOffline(offline: Offline): Promise<Offline> {
        offline.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('offline', offline)
            .then((data: any) => {

                if (data && data.offlines && data.offlines
                [0]) {
                    console.log('save');

                    return data.offlines[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    Offline(date, plan) {

        this.getSettings().then(settings => {
            settings[0].expiredate = new Date(date).toUTCString();
            settings[0].plans.push(plan);

            this.updateSetting(settings[0]);
        });
    }

    /**
   * Return all the settings
   *
   * @return Promise<Array<setting>>
   */
    /*  getSettings(): Promise<Array<Setting>> {
         return this.db.rel.find('settings')
             .then((data: any) => {
                 this.settings = data.settings;
                 if (this.searchText && this.searchText !== '') {
                     this.settings = FuseUtils.filterArrayByString(this.settings, this.searchText);
                 }
                 //this.onSettingsChanged.next(this.settings);
                 return Promise.resolve(this.settings);
                 //return data.settings ? data.settings : [];
             }).catch((err: any) => {
                 console.error(err);
             });
     } */

    /**
  * Update a setting
  * @param {setting} Setting
  *
  * @return Promise<setting>
  */
    /* updateSetting(setting: Setting) {

        return this.db.rel.save('settings', setting)
            .then((data: any) => {
                if (data && data.settings && data.settings
                [0]) {
                    return data.settings[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }
 */
    /**
    * Update a offline
    * @param {offline} offline
    *
    * @return Promise<offline>
    */
    updateOffline(offline: Offline) {
        return this.db.rel.save('offline', offline)
            .then((data: any) => {
                if (data && data.offlines && data.offlines
                [0]) {
                    console.log('Update')

                    return data.offlines[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a offline
     * @param {offline} offline
     *
     * @return Promise<boolean>
     */
    removeOffline(offline: Offline): Promise<boolean> {
        return this.db.rel.del('offline', offline)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the offlines
     *
     * @return Promise<Array<offline>>
     */
    getOfflines(): Promise<Array<Offline>> {
        return this.db.rel.find('offline')
            .then((data: any) => {
                this.offlines = data.offlines;
                if (this.searchText && this.searchText !== '') {
                    this.offlines = FuseUtils.filterArrayByString(this.offlines, this.searchText);
                }
                //this.onofflinesChanged.next(this.offlines);
                return Promise.resolve(this.offlines);
                //return data.offlines ? data.offlines : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getDoctorOfflines(): Promise<Array<Offline>> {
        return this.db.rel.find('offline')
            .then((data: any) => {
                return data.offlines ? data.offlines : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a offline
     * @param {offline} offline
     *
     * @return Promise<offline>
     */
    getoffline(offline: Offline): Promise<Offline> {
        return this.db.rel.find('offline', offline.id)
            .then((data: any) => {
                console.log("Get")

                return data && data.offlines ? data.offlines[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected offline by id
     * @param id
     */
    toggleSelectedOffline(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedOfflines.length > 0) {
            const index = this.selectedOfflines.indexOf(id);

            if (index !== -1) {
                this.selectedOfflines.splice(index, 1);

                // Trigger the next event
                this.onSelectedOfflinesChanged.next(this.selectedOfflines);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedOfflines.push(id);

        // Trigger the next event
        this.onSelectedOfflinesChanged.next(this.selectedOfflines);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllOffline() {
        if (this.selectedOfflines.length > 0) {
            this.deselectOfflines();
        }
        else {
            this.selectOfflines();
        }
    }

    selectOfflines(filterParameter?, filterValue?) {
        this.selectedOfflines = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedOfflines = [];
            this.offlines.map(offline => {
                this.selectedOfflines.push(offline.id);
            });
        }
        else {
            /* this.selectedofflines.push(...
                 this.offlines.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedOfflinesChanged.next(this.selectedOfflines);
    }





    deselectOfflines() {
        this.selectedOfflines = [];

        // Trigger the next event
        this.onSelectedOfflinesChanged.next(this.selectedOfflines);
    }

    deleteOffline(offline) {
        this.db.rel.del('offline', offline)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const offlineIndex = this.offlines.indexOf(offline);
                this.offlines.splice(offlineIndex, 1);
                this.onOfflinesChanged.next(this.offlines);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedOfflines() {
        for (const offlineId of this.selectedOfflines) {
            const offline = this.offlines.find(_offline => {
                return _offline.id === offlineId;
            });

            this.db.rel.del('offline', offline)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const offlineIndex = this.offlines.indexOf(offline);
                    this.offlines.splice(offlineIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onOfflinesChanged.next(this.offlines);
        this.deselectOfflines();
    }

    /***********
     * patient
     **********/
    /**
     * Save a patient
     * @param {patient} patient
     *
     * @return Promise<patient>
     */
    savePatient(patient: Patient): Promise<Patient> {
        patient.id = Math.floor(Date.now()).toString();

        patient.bed_allotments = [];
        patient.appointments = [];
        patient.invoices = [];
        patient.prescriptions = [];
        patient.medicine_sales = [];
        patient.reports = [];
        return this.db.rel.save('patient', patient)
            .then((data: any) => {

                if (data && data.patients && data.patients
                [0]) {
                    return data.patients[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a patient
    * @param {patient} patient
    *
    * @return Promise<patient>
    */
    updatePatient(patient: Patient) {

        return this.db.rel.save('patient', patient)
            .then((data: any) => {
                if (data && data.patients && data.patients
                [0]) {

                    return data.patients[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a patient
     * @param {patient} patient
     *
     * @return Promise<boolean>
     */
    removePatient(patient: Patient): Promise<boolean> {
        return this.db.rel.del('patient', patient)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the patients
     *
     * @return Promise<Array<patient>>
     */
    getPatients(): Promise<Array<Patient>> {
        return this.db.rel.find('patient')
            .then((data: any) => {
                this.patients = data.patients;
                if (this.searchText && this.searchText !== '') {
                    this.patients = FuseUtils.filterArrayByString(this.patients, this.searchText);
                }
                //this.onpatientsChanged.next(this.patients);
                return Promise.resolve(this.patients);
                //return data.patients ? data.patients : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getAppointmentPatients(): Promise<Array<Patient>> {
        return this.db.rel.find('patient')
            .then((data: any) => {
                return data.patients ? data.patients : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a patient
     * @param {patient} patient
     *
     * @return Promise<patient>
     */
    getPatient(patient: Patient): Promise<Patient> {
        return this.db.rel.find('patient', patient.id)
            .then((data: any) => {
                return data && data.patients ? data.patients[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected patient by id
     * @param id
     */
    toggleSelectedPatient(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedPatients.length > 0) {
            const index = this.selectedPatients.indexOf(id);

            if (index !== -1) {
                this.selectedPatients.splice(index, 1);

                // Trigger the next event
                this.onSelectedPatientsChanged.next(this.selectedPatients);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedPatients.push(id);

        // Trigger the next event
        this.onSelectedPatientsChanged.next(this.selectedPatients);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllPatient() {
        if (this.selectedPatients.length > 0) {
            this.deselectPatients();
        }
        else {
            this.selectPatients();
        }
    }

    selectPatients(filterParameter?, filterValue?) {
        this.selectedPatients = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedPatients = [];
            this.patients.map(patient => {
                this.selectedPatients.push(patient.id);
            });
        }
        else {
            /* this.selectedpatients.push(...
                 this.patients.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedPatientsChanged.next(this.selectedPatients);
    }





    deselectPatients() {
        this.selectedPatients = [];

        // Trigger the next event
        this.onSelectedPatientsChanged.next(this.selectedPatients);
    }

    deletePatient(patient) {
        this.db.rel.del('patient', patient)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const patientIndex = this.patients.indexOf(patient);
                this.patients.splice(patientIndex, 1);
                this.onPatientsChanged.next(this.patients);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedPatients() {
        for (const patientId of this.selectedPatients) {
            const patient = this.patients.find(_patient => {
                return _patient.id === patientId;
            });

            this.db.rel.del('patient', patient)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const patientIndex = this.patients.indexOf(patient);
                    this.patients.splice(patientIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onPatientsChanged.next(this.patients);
        this.deselectPatients();
    }


    /***********
     * payroll
     **********/
    /**
     * Save a payroll
     * @param {payroll} payroll
     *
     * @return Promise<payroll>
     */
    savePayroll(payroll: Payroll): Promise<Payroll> {
        payroll.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('payroll', payroll)
            .then((data: any) => {

                if (data && data.payrolls && data.payrolls
                [0]) {
                    return data.payrolls[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a payroll
    * @param {payroll} payroll
    *
    * @return Promise<payroll>
    */
    updatePayroll(payroll: Payroll) {

        return this.db.rel.save('payroll', payroll)
            .then((data: any) => {
                if (data && data.payrolls && data.payrolls
                [0]) {
                    return data.payrolls[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a payroll
     * @param {payroll} payroll
     *
     * @return Promise<boolean>
     */
    removePayroll(payroll: Payroll): Promise<boolean> {
        return this.db.rel.del('payroll', payroll)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the payrolls
     *
     * @return Promise<Array<payroll>>
     */
    getPayrolls(): Promise<Array<Payroll>> {
        var localStorageItem = JSON.parse(localStorage.getItem('user'));

        return this.db.rel.find('payroll')
            .then((data: any) => {
                this.payrolls = data.payrolls;
                if (this.searchText && this.searchText !== '') {
                    this.payrolls = FuseUtils.filterArrayByString(this.payrolls, this.searchText);
                }
                //this.onpayrollsChanged.next(this.payrolls);
                var userId = localStorageItem.id
                if (localStorageItem.usertype == "Admin") {
                    this.payrolls
                }
                else {
                    this.payrolls = this.payrolls.filter(data => data.name_Id == userId)

                }
                return Promise.resolve(this.payrolls);
                //return data.payrolls ? data.payrolls : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a payroll
     * @param {payroll} payroll
     *
     * @return Promise<payroll>
     */
    getPayroll(id): Promise<Payroll> {
        return this.db.rel.find('payroll', id)
            .then((data: any) => {
                return data && data.payrolls ? data.payrolls[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected payroll by id
     * @param id
     */
    toggleSelectedPayroll(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedPayrolls.length > 0) {
            const index = this.selectedPayrolls.indexOf(id);

            if (index !== -1) {
                this.selectedPayrolls.splice(index, 1);

                // Trigger the next event
                this.onSelectedPayrollsChanged.next(this.selectedPayrolls);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedPayrolls.push(id);

        // Trigger the next event
        this.onSelectedPayrollsChanged.next(this.selectedPayrolls);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllPayroll() {
        if (this.selectedPayrolls.length > 0) {
            this.deselectPayrolls();
        }
        else {
            this.selectPayrolls();
        }
    }

    selectPayrolls(filterParameter?, filterValue?) {
        this.selectedPayrolls = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedPayrolls = [];
            this.payrolls.map(payroll => {
                this.selectedPayrolls.push(payroll.id);
            });
        }
        else {
            /* this.selectedpayrolls.push(...
                 this.payrolls.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedPayrollsChanged.next(this.selectedPayrolls);
    }





    deselectPayrolls() {
        this.selectedPayrolls = [];

        // Trigger the next event
        this.onSelectedPayrollsChanged.next(this.selectedPayrolls);
    }

    deletePayroll(payroll) {
        this.db.rel.del('payroll', payroll)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const payrollIndex = this.payrolls.indexOf(payroll);
                this.payrolls.splice(payrollIndex, 1);
                this.onPayrollsChanged.next(this.payrolls);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedPayrolls() {
        for (const payrollId of this.selectedPayrolls) {
            const payroll = this.payrolls.find(_payroll => {
                return _payroll.id === payrollId;
            });

            this.db.rel.del('payroll', payroll)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const payrollIndex = this.payrolls.indexOf(payroll);
                    this.payrolls.splice(payrollIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onPayrollsChanged.next(this.payrolls);
        this.deselectPayrolls();
    }

    /***********
   * pharmacist
   **********/
    /**
     * Save a pharmacist
     * @param {pharmacist} pharmacist
     *
     * @return Promise<pharmacist>
     */
    savePharmacist(pharmacist: Pharmacist): Promise<Pharmacist> {
        pharmacist.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('pharmacist', pharmacist)
            .then((data: any) => {

                if (data && data.pharmacists && data.pharmacists
                [0]) {
                    return data.pharmacists[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a pharmacist
    * @param {pharmacist} pharmacist
    *
    * @return Promise<pharmacist>
    */
    updatePharmacist(pharmacist: Pharmacist) {

        return this.db.rel.save('pharmacist', pharmacist)
            .then((data: any) => {
                if (data && data.pharmacists && data.pharmacists
                [0]) {
                    return data.pharmacists[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a pharmacist
     * @param {pharmacist} pharmacist
     *
     * @return Promise<boolean>
     */
    removePharmacist(pharmacist: Pharmacist): Promise<boolean> {
        return this.db.rel.del('pharmacist', pharmacist)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the pharmacists
     *
     * @return Promise<Array<pharmacist>>
     */
    getPharmacists(): Promise<Array<Pharmacist>> {
        return this.db.rel.find('pharmacist')
            .then((data: any) => {
                this.pharmacists = data.pharmacists;
                if (this.searchText && this.searchText !== '') {
                    this.pharmacists = FuseUtils.filterArrayByString(this.pharmacists, this.searchText);
                }
                //this.onpharmacistsChanged.next(this.pharmacists);
                return Promise.resolve(this.pharmacists);
                //return data.pharmacists ? data.pharmacists : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a pharmacist
     * @param {pharmacist} pharmacist
     *
     * @return Promise<pharmacist>
     */
    getPharmacist(pharmacist: Pharmacist): Promise<Pharmacist> {
        return this.db.rel.find('pharmacist', pharmacist.id)
            .then((data: any) => {
                return data && data.pharmacists ? data.pharmacists[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected pharmacist by id
     * @param id
     */
    toggleSelectedPharmacist(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedPharmacists.length > 0) {
            const index = this.selectedPharmacists.indexOf(id);

            if (index !== -1) {
                this.selectedPharmacists.splice(index, 1);

                // Trigger the next event
                this.onSelectedPharmacistsChanged.next(this.selectedPharmacists);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedPharmacists.push(id);

        // Trigger the next event
        this.onSelectedPharmacistsChanged.next(this.selectedPharmacists);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllPharmacist() {
        if (this.selectedPharmacists.length > 0) {
            this.deselectPharmacists();
        }
        else {
            this.selectPharmacists();
        }
    }

    selectPharmacists(filterParameter?, filterValue?) {
        this.selectedPharmacists = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedPharmacists = [];
            this.pharmacists.map(pharmacist => {
                this.selectedPharmacists.push(pharmacist.id);
            });
        }
        else {
            /* this.selectedpharmacists.push(...
                 this.pharmacists.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedPharmacistsChanged.next(this.selectedPharmacists);
    }





    deselectPharmacists() {
        this.selectedPharmacists = [];

        // Trigger the next event
        this.onSelectedPharmacistsChanged.next(this.selectedPharmacists);
    }

    deletePharmacist(pharmacist) {
        this.db.rel.del('pharmacist', pharmacist)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const pharmacistIndex = this.pharmacists.indexOf(pharmacist);
                this.pharmacists.splice(pharmacistIndex, 1);
                this.onPharmacistsChanged.next(this.pharmacists);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedPharmacists() {
        for (const pharmacistId of this.selectedPharmacists) {
            const pharmacist = this.pharmacists.find(_pharmacist => {
                return _pharmacist.id === pharmacistId;
            });

            this.db.rel.del('pharmacist', pharmacist)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const pharmacistIndex = this.pharmacists.indexOf(pharmacist);
                    this.pharmacists.splice(pharmacistIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onPharmacistsChanged.next(this.pharmacists);
        this.deselectPharmacists();
    }

    /***********
    * prescription
    **********/
    /**
     * Save a prescription
     * @param {prescription} prescription
     *
     * @return Promise<prescription>
     */
    savePrescription(prescription: Prescription): Promise<Prescription> {
        prescription.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('prescription', prescription)
            .then((data: any) => {

                if (data && data.prescriptions && data.prescriptions
                [0]) {
                    return data.prescriptions[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a prescription
    * @param {prescription} Prescription
    *
    * @return Promise<prescription>
    */
    updatePrescription(prescription: Prescription) {

        return this.db.rel.save('prescription', prescription)
            .then((data: any) => {
                if (data && data.prescriptions && data.prescriptions
                [0]) {

                    return data.prescriptions[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a Prescription
    * @param {Prescription} Prescription
    *
    * @return Promise<Prescription>
    */
    updatePrescription2(prescription: Prescription) {
        return this.db.rel.save('prescription', prescription)
            .then((data: any) => {
                if (data && data.prescriptions && data.prescriptions
                [0]) {
                    console.log('Update')

                    return data.prescriptions[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a prescription
     * @param {prescription} prescription
     *
     * @return Promise<boolean>
     */
    removePrescription(prescription: Prescription): Promise<boolean> {
        return this.db.rel.del('prescription', prescription)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the prescriptions
     *
     * @return Promise<Array<prescription>>
     */
    getPrescriptions(): Promise<Array<Prescription>> {
        var localStorageItem = JSON.parse(localStorage.getItem('user'));
        var userType = localStorageItem.usertype;

        return this.db.rel.find('prescription')
            .then((data: any) => {

                this.prescriptions = data.prescriptions;
                if (this.searchText && this.searchText !== '') {
                    this.prescriptions = FuseUtils.filterArrayByString(this.prescriptions, this.searchText);
                }
                //this.onprescriptionsChanged.next(this.prescriptions);
                if (userType === "Doctor") {
                    var userId = localStorageItem.id
                    this.prescriptions = this.prescriptions.filter(data => data.nameId == userId)
                }
                return Promise.resolve(this.prescriptions);
                //return data.prescriptions ? data.prescriptions : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a prescription
     * @param {prescription} prescription
     *
     * @return Promise<prescription>
     */
    getPrescription(id): Promise<Prescription> {
        return this.db.rel.find('prescription', id)
            .then((data: any) => {
                return data && data.prescriptions ? data.prescriptions[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected prescription by id
     * @param id
     */
    toggleSelectedPrescription(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedPrescriptions.length > 0) {
            const index = this.selectedPrescriptions.indexOf(id);

            if (index !== -1) {
                this.selectedPrescriptions.splice(index, 1);

                // Trigger the next event
                this.onSelectedPrescriptionsChanged.next(this.selectedPrescriptions);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedPrescriptions.push(id);

        // Trigger the next event
        this.onSelectedPrescriptionsChanged.next(this.selectedPrescriptions);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllPrescritpion() {
        if (this.selectedPrescriptions.length > 0) {
            this.deselectPrescriptions();
        }
        else {
            this.selectPrescriptions();
        }
    }

    selectPrescriptions(filterParameter?, filterValue?) {
        this.selectedPrescriptions = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedPrescriptions = [];
            this.prescriptions.map(prescription => {
                this.selectedPrescriptions.push(prescription.id);
            });
        }
        else {
            /* this.selectedprescriptions.push(...
                 this.prescriptions.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedPrescriptionsChanged.next(this.selectedPrescriptions);
    }





    deselectPrescriptions() {
        this.selectedPrescriptions = [];

        // Trigger the next event
        this.onSelectedPrescriptionsChanged.next(this.selectedPrescriptions);
    }

    deletePrescription(prescription) {
        this.db.rel.del('prescription', prescription)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const prescriptionIndex = this.prescriptions.indexOf(prescription);
                this.prescriptions.splice(prescriptionIndex, 1);
                this.onPrescriptionsChanged.next(this.prescriptions);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedPrescriptions() {
        for (const prescriptionId of this.selectedPrescriptions) {
            const prescription = this.prescriptions.find(_prescription => {
                return _prescription.id === prescriptionId;
            });

            this.db.rel.del('prescription', prescription)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const prescriptionIndex = this.prescriptions.indexOf(prescription);
                    this.prescriptions.splice(prescriptionIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onPrescriptionsChanged.next(this.prescriptions);
        this.deselectPrescriptions();
    }

    /***********
    * receipt
    **********/
    /**
     * Save a receipt
     * @param {receipt} receipt
     *
     * @return Promise<receipt>
     */
    saveReceipt(receipt: Receipt): Promise<Receipt> {
        receipt.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('receipt', receipt)
            .then((data: any) => {

                if (data && data.receipts && data.receipts
                [0]) {
                    return data.receipts[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a receipt
    * @param {receipt} receipt
    *
    * @return Promise<receipt>
    */
    updateReceipt(receipt: Receipt) {
        return this.db.rel.save('receipt', receipt)
            .then((data: any) => {
                if (data && data.receipts && data.receipts
                [0]) {
                    return data.receipts[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a receipt
     * @param {receipt} receipt
     *
     * @return Promise<boolean>
     */
    removeReceipt(receipt: Receipt): Promise<boolean> {
        return this.db.rel.del('receipt', receipt)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the receipts
     *
     * @return Promise<Array<receipt>>
     */
    getReceipts(): Promise<Array<Receipt>> {
        return this.db.rel.find('receipt')
            .then((data: any) => {
                this.receipts = data.receipts;
                //this.onreceiptsChanged.next(this.receipts);
                if (this.searchText && this.searchText !== '') {
                    this.receipts = FuseUtils.filterArrayByString(this.receipts, this.searchText);
                }
                return Promise.resolve(this.receipts);
                //return data.receipts ? data.receipts : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }
    /**
     * Read a receipt
     * @param {receipt} receipt
     *
     * @return Promise<receipt>
     */
    getReceipt(receipt: Receipt): Promise<Receipt> {
        return this.db.rel.find('receipt', receipt.id)
            .then((data: any) => {
                return data && data.receipts ? data.receipts[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected receipt by id
     * @param id
     */
    toggleSelectedReceipt(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedReceipts.length > 0) {
            const index = this.selectedReceipts.indexOf(id);

            if (index !== -1) {
                this.selectedReceipts.splice(index, 1);

                // Trigger the next event
                this.onSelectedReceiptsChanged.next(this.selectedReceipts);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedReceipts.push(id);

        // Trigger the next event
        this.onSelectedReceiptsChanged.next(this.selectedReceipts);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllReceipt() {
        if (this.selectedReceipts.length > 0) {
            this.deselectReceipts();
        }
        else {
            this.selectReceipts();
        }
    }

    selectReceipts(filterParameter?, filterValue?) {
        this.selectedReceipts = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedReceipts = [];
            this.receipts.map(receipt => {
                this.selectedReceipts.push(receipt.id);
            });
        }
        else {
            /* this.selectedroutes.push(...
                 this.routes.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedReceiptsChanged.next(this.selectedReceipts);
    }





    deselectReceipts() {
        this.selectedReceipts = [];

        // Trigger the next event
        this.onSelectedReceiptsChanged.next(this.selectedReceipts);
    }

    deleteReceipt(receipt) {
        this.db.rel.del('receipt', receipt)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const receiptIndex = this.receipts.indexOf(receipt);
                this.receipts.splice(receiptIndex, 1);
                this.onReceiptsChanged.next(this.receipts);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedReceipts() {
        for (const receiptId of this.selectedReceipts) {
            const receipt = this.receipts.find(_receipt => {
                return _receipt.id === receiptId;
            });

            this.db.rel.del('receipt', receipt)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const receiptIndex = this.receipts.indexOf(receipt);
                    this.receipts.splice(receiptIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onReceiptsChanged.next(this.receipts);
        this.deselectReceipts();
    }

    /***********
    * receptionist
    **********/
    /**
     * Save a receptionist
     * @param {receptionist} receptionist
     *
     * @return Promise<receptionist>
     */
    saveReceptionist(receptionist: Receptionist): Promise<Receptionist> {
        receptionist.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('receptionist', receptionist)
            .then((data: any) => {

                if (data && data.receptionists && data.receptionists
                [0]) {
                    return data.receptionists[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a receptionist
    * @param {receptionist} receptionist
    *
    * @return Promise<receptionist>
    */
    updateReceptionist(receptionist: Receptionist) {

        return this.db.rel.save('receptionist', receptionist)
            .then((data: any) => {
                if (data && data.receptionists && data.receptionists
                [0]) {
                    return data.receptionists[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a receptionist
     * @param {receptionist} receptionist
     *
     * @return Promise<boolean>
     */
    removeReceptionist(receptionist: Receptionist): Promise<boolean> {
        return this.db.rel.del('receptionist', receptionist)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the receptionists
     *
     * @return Promise<Array<receptionist>>
     */
    getReceptionists(): Promise<Array<Receptionist>> {
        return this.db.rel.find('receptionist')
            .then((data: any) => {
                this.receptionists = data.receptionists;
                if (this.searchText && this.searchText !== '') {
                    this.receptionists = FuseUtils.filterArrayByString(this.receptionists, this.searchText);
                }
                //this.onreceptionistsChanged.next(this.receptionists);
                return Promise.resolve(this.receptionists);
                //return data.receptionists ? data.receptionists : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a receptionist
     * @param {receptionist} receptionist
     *
     * @return Promise<receptionist>
     */
    getReceptionist(receptionist: Receptionist): Promise<Receptionist> {
        return this.db.rel.find('receptionist', receptionist.id)
            .then((data: any) => {
                return data && data.receptionists ? data.receptionists[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected receptionist by id
     * @param id
     */
    toggleSelectedReceptionist(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedReceptionists.length > 0) {
            const index = this.selectedReceptionists.indexOf(id);

            if (index !== -1) {
                this.selectedReceptionists.splice(index, 1);

                // Trigger the next event
                this.onSelectedReceptionistsChanged.next(this.selectedReceptionists);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedReceptionists.push(id);

        // Trigger the next event
        this.onSelectedReceptionistsChanged.next(this.selectedReceptionists);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllReceptionist() {
        if (this.selectedReceptionists.length > 0) {
            this.deselectReceptionists();
        }
        else {
            this.selectReceptionists();
        }
    }

    selectReceptionists(filterParameter?, filterValue?) {
        this.selectedReceptionists = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedReceptionists = [];
            this.receptionists.map(receptionist => {
                this.selectedReceptionists.push(receptionist.id);
            });
        }
        else {
            /* this.selectedreceptionists.push(...
                 this.receptionists.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedReceptionistsChanged.next(this.selectedReceptionists);
    }





    deselectReceptionists() {
        this.selectedReceptionists = [];

        // Trigger the next event
        this.onSelectedReceptionistsChanged.next(this.selectedReceptionists);
    }

    deleteReceptionist(receptionist) {
        this.db.rel.del('receptionist', receptionist)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const receptionistIndex = this.receptionists.indexOf(receptionist);
                this.receptionists.splice(receptionistIndex, 1);
                this.onReceptionistsChanged.next(this.receptionists);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedReceptionists() {
        for (const receptionistId of this.selectedReceptionists) {
            const receptionist = this.receptionists.find(_receptionist => {
                return _receptionist.id === receptionistId;
            });

            this.db.rel.del('receptionist', receptionist)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const receptionistIndex = this.receptionists.indexOf(receptionist);
                    this.receptionists.splice(receptionistIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onReceptionistsChanged.next(this.receptionists);
        this.deselectReceptionists();
    }


    /***********
     * record
     **********/
    /**
     * Save a record
     * @param {record} record
     *
     * @return Promise<record>
     */
    saveRecord(record: Record, staff: Staff): Promise<Record> {
        //record.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('record', record)
            .then((data: any) => {
                console.log(staff);
                this.updateStaff(staff);
                /*   if (this.file) {
                      this.file;
                      
                      this.retrieve = (<HTMLInputElement>this.file).files[0];
  
                      if (this.retrieve = (<HTMLInputElement>this.file).files[0]) {
                          staff._attachments = this.retrieve
                      }
  
                      else {
                          staff._attachments;
                          //this.getStaff = staff._attachments;
                      }
  
                      return this.db.rel.save('staff', staff)
                          .then((data: any) => {
                              if (data && data.staffs && data.staffs
                              [0]) {
                                   this.db.rel.putAttachment('staff', { id: staff.id, rev: staff.rev }, 'file', staff._attachments, 'image/png').then(res => {
                                      var reader: FileReader = new FileReader();
  
                                      reader.onloadend = function (e) {
                                          var base64data = reader.result;
                                          staff.image = base64data
                                          //console.log(staff.image);
                                      }
                                      reader.readAsDataURL(this.retrieve);
                                      staff._attachments = res;
                                      staff.image = res
  
                                  }) 
                                  
                                  return data.staffs[0]
                              }
  
                              return null;
                          }).catch((err: any) => {
                              console.error(err);
                          });
  
                  }
                  else {
                      
  
                      return this.db.rel.save('staff', staff)
                          .then((data: any) => {
                              if (data && data.staffs && data.staffs
                              [0]) {
                                   this.db.rel.putAttachment('staff', { id: staff.id, rev: staff.rev }, 'file', staff._attachments, 'image/png').then(res => {
                                      staff._attachments = res
                                  }) 
                                  
                                  return data.staffs[0]
                              }
  
                              return null;
                          }).catch((err: any) => {
                              console.error(err);
                          });
                  } */
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a record
    * @param {record} record
    *
    * @return Promise<record>
    */
    updateRecord(record: Record) {
        return this.db.rel.save('record', record)
            .then((data: any) => {
                if (data && data.records && data.records
                [0]) {
                    console.log('Update')

                    return data.records[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a record
     * @param {record} record
     *
     * @return Promise<boolean>
     */
    removeRecord(record: Record): Promise<boolean> {
        return this.db.rel.del('record', record)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the records
     *
     * @return Promise<Array<record>>
     */
    getRecords(): Promise<Array<Record>> {
        return this.db.rel.find('record')
            .then((data: any) => {
                this.records = data.records;
                if (this.searchText && this.searchText !== '') {
                    this.records = FuseUtils.filterArrayByString(this.records, this.searchText);
                }
                //this.onrecordsChanged.next(this.records);
                return Promise.resolve(this.records);
                //return data.records ? data.records : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getDoctorRecords(): Promise<Array<Record>> {
        return this.db.rel.find('record')
            .then((data: any) => {
                return data.records ? data.records : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a record
     * @param {record} record
     *
     * @return Promise<record>
     */
    getRecord(id): Promise<Record> {
        return this.db.rel.find('record', id)
            .then((data: any) => {
                console.log("Get")

                return data && data.records ? data.records[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected record by id
     * @param id
     */
    toggleSelectedRecord(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedRecords.length > 0) {
            const index = this.selectedRecords.indexOf(id);

            if (index !== -1) {
                this.selectedRecords.splice(index, 1);

                // Trigger the next event
                this.onSelectedRecordsChanged.next(this.selectedRecords);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedRecords.push(id);

        // Trigger the next event
        this.onSelectedRecordsChanged.next(this.selectedRecords);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllRecord() {
        if (this.selectedRecords.length > 0) {
            this.deselectRecords();
        }
        else {
            this.selectRecords();
        }
    }

    selectRecords(filterParameter?, filterValue?) {
        this.selectedRecords = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedRecords = [];
            this.records2.map(record => {
                this.selectedRecords.push(record.id);
            });
        }
        else {
            /* this.selectedrecords.push(...
                 this.records.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedRecordsChanged.next(this.selectedRecords);
    }





    deselectRecords() {
        this.selectedRecords = [];

        // Trigger the next event
        this.onSelectedRecordsChanged.next(this.selectedRecords);
    }

    deleteRecord(record) {
        this.db.rel.del('record', record)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const recordIndex = this.records2.indexOf(record);
                this.records2.splice(recordIndex, 1);
                this.onRecordsChanged.next(this.records2);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedRecords() {
        for (const recordId of this.selectedRecords) {
            const record = this.records2.find(_record => {
                return _record.id === recordId;
            });

            this.db.rel.del('record', record)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const recordIndex = this.records2.indexOf(record);
                    this.records2.splice(recordIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onRecordsChanged.next(this.records2);
        this.deselectRecords();
    }

    /***********
    * report
    **********/
    /**
     * Save a report
     * @param {report} report
     *
     * @return Promise<report>
     */
    saveReport(report: Report): Promise<Report> {
        report.id = Math.floor(Date.now()).toString();

        this.file;
        this.retrieve = (<HTMLInputElement>this.file).files[0];
        report._attachments = this.retrieve;
        report.document_name = report._attachments.name;

        return this.db.rel.save('report', report)
            .then((data: any) => {

                if (data && data.reports && data.reports
                [0]) {
                    this.db.rel.putAttachment('report', { id: report.id, rev: report.rev }, 'file', report._attachments, 'application/msword').then(res => {
                        report._attachments = res

                    })
                    return data.reports[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a report
    * @param {report} report
    *
    * @return Promise<report>
    */
    updateReport(report: Report) {


        this.file;
        this.retrieve = (<HTMLInputElement>this.file).files[0];
        report._attachments = this.retrieve;
        report.document_name = report._attachments.name;

        return this.db.rel.save('report', report)
            .then((data: any) => {
                if (data && data.reports && data.reports
                [0]) {
                    this.db.rel.putAttachment('report', { id: report.id, rev: report.rev }, 'file', report._attachments, 'application/msword').then(res => {
                        report._attachments = res
                    })
                    return data.reports[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a report
     * @param {report} report
     *
     * @return Promise<boolean>
     */
    removeReport(report: Report): Promise<boolean> {
        return this.db.rel.del('report', report)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the reports
     *
     * @return Promise<Array<report>>
     */
    getReports(): Promise<Array<Report>> {

        var localStorageItem = JSON.parse(localStorage.getItem('user'));
        var userType = localStorageItem.usertype;

        return this.db.rel.find('report')
            .then((data: any) => {
                this.reports = data.reports;
                if (this.searchText && this.searchText !== '') {
                    this.reports = FuseUtils.filterArrayByString(this.reports, this.searchText);
                }
                //this.onreportsChanged.next(this.reports);
                if (userType === "Doctor") {
                    var userId = localStorageItem.id
                    this.reports = this.reports.filter(data => data.staff_name == userId)
                }
                return Promise.resolve(this.reports);
                //return data.reports ? data.reports : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a report
     * @param {report} report
     *
     * @return Promise<report>
     */
    getReport(report: Report): Promise<Report> {
        return this.db.rel.find('report', report.id)
            .then((data: any) => {
                return data && data.reports ? data.reports[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected report by id
     * @param id
     */
    toggleSelectedReport(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedReports.length > 0) {
            const index = this.selectedReports.indexOf(id);

            if (index !== -1) {
                this.selectedReports.splice(index, 1);

                // Trigger the next event
                this.onSelectedReportsChanged.next(this.selectedReports);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedReports.push(id);

        // Trigger the next event
        this.onSelectedReportsChanged.next(this.selectedReports);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllReport() {
        if (this.selectedReports.length > 0) {
            this.deselectReports();
        }
        else {
            this.selectReports();
        }
    }

    selectReports(filterParameter?, filterValue?) {
        this.selectedReports = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedReports = [];
            this.reports.map(report => {
                this.selectedReports.push(report.id);
            });
        }
        else {
            /* this.selectedreports.push(...
                 this.reports.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedReportsChanged.next(this.selectedReports);
    }





    deselectReports() {
        this.selectedReports = [];

        // Trigger the next event
        this.onSelectedReportsChanged.next(this.selectedReports);
    }

    deleteReport(report) {
        this.db.rel.del('report', report)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const reportIndex = this.reports.indexOf(report);
                this.reports.splice(reportIndex, 1);
                this.onReportsChanged.next(this.reports);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedReports() {
        for (const reportId of this.selectedReports) {
            const report = this.reports.find(_report => {
                return _report.id === reportId;
            });

            this.db.rel.del('report', report)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const reportIndex = this.reports.indexOf(report);
                    this.reports.splice(reportIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onReportsChanged.next(this.reports);
        this.deselectReports();
    }

    /***********
    * report
    **********/
    /**
     * Save a report
     * @param {report} report
     *
     * @return Promise<report>
     */
    saveReport2(report: Report): Promise<Report> {
        report.id = Math.floor(Date.now()).toString();

        this.file;
        this.retrieve = (<HTMLInputElement>this.file).files[0];
        report._attachments = this.retrieve;
        report.document_name = report._attachments.name;


        return this.db.rel.save('report', report)
            .then((data: any) => {

                if (data && data.reports && data.reports
                [0]) {
                    this.db.rel.putAttachment('report', { id: report.id, rev: report.rev }, 'file', report._attachments, 'application/msword').then(res => {
                        report._attachments = res

                    })
                    return data.reports[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a report
    * @param {report} report
    *
    * @return Promise<report>
    */
    updateReport2(report: Report) {


        this.file;
        this.retrieve = (<HTMLInputElement>this.file).files[0];
        if (this.retrieve = (<HTMLInputElement>this.file).files[0]) {
            report._attachments = this.retrieve;
            report.document_name = report._attachments.name;
        }
        else {
            report._attachments;
            report.document_name = report._attachments.name;
        }

        return this.db.rel.save('report', report)
            .then((data: any) => {
                if (data && data.reports && data.reports
                [0]) {
                    this.db.rel.putAttachment('report', { id: report.id, rev: report.rev }, 'file', report._attachments, 'application/msword').then(res => {
                        report._attachments = res
                    })
                    return data.reports[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a report
     * @param {report} report
     *
     * @return Promise<boolean>
     */
    removeReport2(report: Report): Promise<boolean> {
        return this.db.rel.del('report', report)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the reports
     *
     * @return Promise<Array<report>>
     */
    getReports2(): Promise<Array<Report>> {

        var localStorageItem = JSON.parse(localStorage.getItem('user'));
        var userType = localStorageItem.usertype;

        return this.db.rel.find('report')
            .then((data: any) => {
                this.reports = data.reports;
                if (this.searchText && this.searchText !== '') {
                    this.reports = FuseUtils.filterArrayByString(this.reports, this.searchText);
                }
                //this.onreportsChanged.next(this.reports);
                if (userType === "Doctor") {
                    var userId = localStorageItem.id
                    this.reports = this.reports.filter(data => data.staff_name == userId)
                }
                else if (userType === "Patient") {
                    var userId = localStorageItem.id
                    this.reports = this.reports.filter(data => data.patient_name == userId)
                }
                return Promise.resolve(this.reports);
                //return data.reports ? data.reports : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a report
     * @param {report} report
     *
     * @return Promise<report>
     */
    getReport2(report: Report): Promise<Report> {
        return this.db.rel.find('report', report.id)
            .then((data: any) => {
                return data && data.reports ? data.reports[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected report by id
     * @param id
     */
    toggleSelectedReport2(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedReports.length > 0) {
            const index = this.selectedReports.indexOf(id);

            if (index !== -1) {
                this.selectedReports.splice(index, 1);

                // Trigger the next event
                this.onSelectedReportsChanged.next(this.selectedReports);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedReports.push(id);

        // Trigger the next event
        this.onSelectedReportsChanged.next(this.selectedReports);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllReport2() {
        if (this.selectedReports.length > 0) {
            this.deselectReports();
        }
        else {
            this.selectReports();
        }
    }

    selectReports2(filterParameter?, filterValue?) {
        this.selectedReports = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedReports = [];
            this.reports.map(report => {
                this.selectedReports.push(report.id);
            });
        }
        else {
            /* this.selectedreports.push(...
                 this.reports.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedReportsChanged.next(this.selectedReports);
    }





    deselectReports2() {
        this.selectedReports = [];

        // Trigger the next event
        this.onSelectedReportsChanged.next(this.selectedReports);
    }

    deleteReport2(report) {
        this.db.rel.del('report', report)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const reportIndex = this.reports.indexOf(report);
                this.reports.splice(reportIndex, 1);
                this.onReportsChanged.next(this.reports);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedReports2() {
        for (const reportId of this.selectedReports) {
            const report = this.reports.find(_report => {
                return _report.id === reportId;
            });

            this.db.rel.del('report', report)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const reportIndex = this.reports.indexOf(report);
                    this.reports.splice(reportIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onReportsChanged.next(this.reports);
        this.deselectReports();
    }

    /***********
    * sms
    **********/
    /**
     * Save a sms
     * @param {sms} sms
     *
     * @return Promise<sms>
     */
    saveSms(sms: Sms): Promise<Sms> {
        sms.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('sms', sms)
            .then((data: any) => {
                //console.log(data);
                //console.log(sms);
                if (data && data.smss && data.smss
                [0]) {
                    return data.smss[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a sms
    * @param {sms} sms
    *
    * @return Promise<sms>
    */
    updateSms(sms: Sms) {

        return this.db.rel.save('sms', sms)
            .then((data: any) => {
                if (data && data.smss && data.smss
                [0]) {
                    return data.smss[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a sms
     * @param {sms} sms
     *
     * @return Promise<boolean>
     */
    removeSms(sms: Sms): Promise<boolean> {
        return this.db.rel.del('sms', sms)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the smss
     *
     * @return Promise<Array<sms>>
     */
    getSmss(): Promise<Array<Sms>> {
        return this.db.rel.find('sms')
            .then((data: any) => {
                this.smss = data.smss;
                if (this.searchText && this.searchText !== '') {
                    this.smss = FuseUtils.filterArrayByString(this.smss, this.searchText);
                }
                //this.onsmssChanged.next(this.smss);
                return Promise.resolve(this.smss);
                //return data.smss ? data.smss : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a sms
     * @param {sms} sms
     *
     * @return Promise<sms>
     */
    getSms(sms: Sms): Promise<Sms> {
        return this.db.rel.find('sms', sms.id)
            .then((data: any) => {
                return data && data.smss ? data.smss[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected sms by id
     * @param id
     */
    toggleSelectedSms(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedSmss.length > 0) {
            const index = this.selectedSmss.indexOf(id);

            if (index !== -1) {
                this.selectedSmss.splice(index, 1);

                // Trigger the next event
                this.onSelectedSmssChanged.next(this.selectedSmss);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedSmss.push(id);

        // Trigger the next event
        this.onSelectedSmssChanged.next(this.selectedSmss);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllSms() {
        if (this.selectedSmss.length > 0) {
            this.deselectSmss();
        }
        else {
            this.selectSmss();
        }
    }

    selectSmss(filterParameter?, filterValue?) {
        this.selectedSmss = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedSmss = [];
            this.smss.map(sms => {
                this.selectedSmss.push(sms.id);
            });
        }
        else {
            /* this.selectedsmss.push(...
                 this.smss.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedSmssChanged.next(this.selectedSmss);
    }





    deselectSmss() {
        this.selectedSmss = [];

        // Trigger the next event
        this.onSelectedSmssChanged.next(this.selectedSmss);
    }

    deleteSms(sms) {
        this.db.rel.del('sms', sms)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const smsIndex = this.smss.indexOf(sms);
                this.smss.splice(smsIndex, 1);
                this.onSmssChanged.next(this.smss);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedSmss() {
        for (const smsId of this.selectedSmss) {
            const sms = this.smss.find(_sms => {
                return _sms.id === smsId;
            });

            this.db.rel.del('sms', sms)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const smsIndex = this.smss.indexOf(sms);
                    this.smss.splice(smsIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onSmssChanged.next(this.smss);
        this.deselectSmss();
    }

    /***********
     * smssetting
     **********/
    /**
     * Save a smssetting
     * @param {smssetting} smssetting
     *
     * @return Promise<smssetting>
     */
    saveSmssetting(smssetting: Smssetting): Promise<Smssetting> {
        smssetting.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('smssetting', smssetting)
            .then((data: any) => {

                if (data && data.smssettings && data.smssettings
                [0]) {
                    return data.smssettings[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a smssetting
    * @param {smssetting} smssetting
    *
    * @return Promise<smssetting>
    */
    updateSmssetting(smssetting: Smssetting) {

        return this.db.rel.save('smssetting', smssetting)
            .then((data: any) => {
                if (data && data.smssettings && data.smssettings
                [0]) {
                    return data.smssettings[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a smssetting
     * @param {smssetting} smssetting
     *
     * @return Promise<boolean>
     */
    removeSmssetting(smssetting: Smssetting): Promise<boolean> {
        return this.db.rel.del('smssetting', smssetting)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the smssettings
     *
     * @return Promise<Array<smssetting>>
     */
    getSmssettings(): Promise<Array<Smssetting>> {
        return this.db.rel.find('smssetting')
            .then((data: any) => {
                this.smssettings = data.smssettings;
                if (this.searchText && this.searchText !== '') {
                    this.smssettings = FuseUtils.filterArrayByString(this.smssettings, this.searchText);
                }
                //this.onsmssettingsChanged.next(this.smssettings);
                return Promise.resolve(this.smssettings);
                //return data.smssettings ? data.smssettings : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a smssetting
     * @param {smssetting} smssetting
     *
     * @return Promise<smssetting>
     */
    getSmssetting(smssetting: Smssetting): Promise<Smssetting> {
        return this.db.rel.find('smssetting', smssetting.id)
            .then((data: any) => {
                return data && data.smssettings ? data.smssettings[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected smssetting by id
     * @param id
     */
    toggleSelectedSmssetting(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedSmssettings.length > 0) {
            const index = this.selectedSmssettings.indexOf(id);

            if (index !== -1) {
                this.selectedSmssettings.splice(index, 1);

                // Trigger the next event
                this.onSelectedSmssettingsChanged.next(this.selectedSmssettings);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedSmssettings.push(id);

        // Trigger the next event
        this.onSelectedSmssettingsChanged.next(this.selectedSmssettings);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllSmssetting() {
        if (this.selectedSmssettings.length > 0) {
            this.deselectSmssettings();
        }
        else {
            this.selectSmssettings();
        }
    }

    selectSmssettings(filterParameter?, filterValue?) {
        this.selectedSmssettings = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedSmssettings = [];
            this.smssettings.map(smssetting => {
                this.selectedSmssettings.push(smssetting.id);
            });
        }
        else {
            /* this.selectedsmssettings.push(...
                 this.smssettings.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedSmssettingsChanged.next(this.selectedSmssettings);
    }





    deselectSmssettings() {
        this.selectedSmssettings = [];

        // Trigger the next event
        this.onSelectedSmssettingsChanged.next(this.selectedSmssettings);
    }

    deleteSmssetting(smssetting) {
        this.db.rel.del('smssetting', smssetting)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const smssettingIndex = this.smssettings.indexOf(smssetting);
                this.smssettings.splice(smssettingIndex, 1);
                this.onSmssettingsChanged.next(this.smssettings);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedSmssettings() {
        for (const smssettingId of this.selectedSmssettings) {
            const smssetting = this.smssettings.find(_smssetting => {
                return _smssetting.id === smssettingId;
            });

            this.db.rel.del('smssetting', smssetting)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const smssettingIndex = this.smssettings.indexOf(smssetting);
                    this.smssettings.splice(smssettingIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onSmssettingsChanged.next(this.smssettings);
        this.deselectSmssettings();
    }

    /***********
    * antenatal
    **********/
    /**
     * Save a antenatal
     * @param {antenatal} antenatal
     *
     * @return Promise<antenatal>
     */
    saveantenatal(antenatal: antenatal): Promise<antenatal> {
        antenatal.id = Math.floor(Date.now()).toString();

        return this.db.rel.save('antenatal', antenatal)
            .then((data: any) => {

                if (data && data.antenatals && data.antenatals
                [0]) {
                    return data.antenatals[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
    * Update a antenatal
    * @param {antenatal} antenatal
    *
    * @return Promise<antenatal>
    */
    updateantenatal(antenatal: antenatal) {

        return this.db.rel.save('antenatal', antenatal)
            .then((data: any) => {
                if (data && data.antenatals && data.antenatals
                [0]) {

                    return data.antenatals[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a antenatal
     * @param {antenatal} antenatal
     *
     * @return Promise<boolean>
     */
    removeantenatal(antenatal: antenatal): Promise<boolean> {
        return this.db.rel.del('antenatal', antenatal)
            .then((data: any) => {
                return data && data.deleted ? data.deleted : false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the antenatals
     *
     * @return Promise<Array<antenatal>>
     */
    getantenatals(): Promise<Array<antenatal>> {
        return this.db.rel.find('antenatal')
            .then((data: any) => {
                this.antenatals = data.antenatals;
                if (this.searchText && this.searchText !== '') {
                    this.antenatals = FuseUtils.filterArrayByString(this.antenatals, this.searchText);
                }
                //this.onantenatalsChanged.next(this.antenatals);
                return Promise.resolve(this.antenatals);
                //return data.antenatals ? data.antenatals : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    getAppointmentantenatals(): Promise<Array<antenatal>> {
        return this.db.rel.find('antenatal')
            .then((data: any) => {
                return data.antenatals ? data.antenatals : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a antenatal
     * @param {antenatal} antenatal
     *
     * @return Promise<antenatal>
     */
    getantenatal(antenatal: antenatal): Promise<antenatal> {
        console.log(antenatal);
        return this.db.rel.find('antenatal', antenatal)
            .then((data: any) => {
                return data && data.antenatals ? data.antenatals[0] : null;
            }).catch((err: any) => {
                console.error(err);
            });
    }


    /**
     * Toggle selected antenatal by id
     * @param id
     */
    toggleSelectedantenatal(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedantenatals.length > 0) {
            const index = this.selectedantenatals.indexOf(id);

            if (index !== -1) {
                this.selectedantenatals.splice(index, 1);

                // Trigger the next event
                this.onSelectedantenatalsChanged.next(this.selectedantenatals);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedantenatals.push(id);

        // Trigger the next event
        this.onSelectedantenatalsChanged.next(this.selectedantenatals);
    }

    /**
     * Toggle select all
     */
    toggleSelectAllAntenatal() {
        if (this.selectedantenatals.length > 0) {
            this.deselectantenatals();
        }
        else {
            this.selectantenatals();
        }
    }

    selectantenatals(filterParameter?, filterValue?) {
        this.selectedantenatals = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedantenatals = [];
            this.antenatals.map(antenatal => {
                this.selectedantenatals.push(antenatal.id);
            });
        }
        else {
            /* this.selectedantenatals.push(...
                 this.antenatals.filter(todo => {
                     return todo[filterParameter] === filterValue;
                 })
             );*/
        }

        // Trigger the next event
        this.onSelectedantenatalsChanged.next(this.selectedantenatals);
    }





    deselectantenatals() {
        this.selectedantenatals = [];

        // Trigger the next event
        this.onSelectedantenatalsChanged.next(this.selectedantenatals);
    }

    deleteantenatal(antenatal) {
        this.db.rel.del('antenatal', antenatal)
            .then((data: any) => {
                //return data && data.deleted ? data.deleted: false;
                const antenatalIndex = this.antenatals.indexOf(antenatal);
                this.antenatals.splice(antenatalIndex, 1);
                this.onantenatalsChanged.next(this.antenatals);
            }).catch((err: any) => {
                console.error(err);
            });
    }


    deleteSelectedantenatals() {
        for (const antenatalId of this.selectedantenatals) {
            const antenatal = this.antenatals.find(_antenatal => {
                return _antenatal.id === antenatalId;
            });

            this.db.rel.del('antenatal', antenatal)
                .then((data: any) => {
                    //return data && data.deleted ? data.deleted: false;
                    const antenatalIndex = this.antenatals.indexOf(antenatal);
                    this.antenatals.splice(antenatalIndex, 1);
                }).catch((err: any) => {
                    console.error(err);
                });
        }
        this.onantenatalsChanged.next(this.antenatals);
        this.deselectantenatals();
    }

}
