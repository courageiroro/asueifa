/**
 * Schema defining the db relations
 */
export const Schema = [
    {
        singular: 'branch',
        plural: 'branchs',
        relations: {
            staffs: {
                hasMany: {
                    type: 'staff'
                },
            }
        }
    },
    {
        singular: 'bus',
        plural: 'buss',
        relations: {
            busdocuments: {
                hasMany: {
                    type: 'busdocument'
                },
            },
            seats: {
                hasMany: {
                    type: 'seat'
                },
            },
            schedules: {
                hasMany: {
                    type: 'schedule'
                },
            },
            bustype: {
                belongsTo: 'bustype'
            }
        }
    },
    {
        singular: 'busdocument',
        plural: 'busdocuments',
        relations: {
            bus: {
                belongsTo: 'bus'
            }
        }
    },
    {
        singular: 'route',
        plural: 'routes',
        relations: {
            schedules: {
                hasMany: {
                    type: 'schedule'
                },
            }
        }
    },
    {
        singular: 'schedule',
        plural: 'schedules',
        relations: {
            seats: {
                hasMany: {
                    type: 'seat'
                },
            },
            bus: {
                belongsTo: 'bus'
            },
            route: {
                belongsTo: 'route'
            }
        }
    },
    {
        singular: 'bustype',
        plural: 'bustypes',
        relations: {
            buss: {
                hasMany: {
                    type: 'bus'
                },
            }
        }
    },
    {
        singular: 'customer',
        plural: 'customers',
        relations: {
            seats: {
                hasMany: {
                    type: 'seat'
                },
            }
        }
    },
    {
        singular: 'driverslicense',
        plural: 'driverslicenses',
        relations: {
            staff: {
                belongsTo: 'staff'
            }
        }
    },
    {
        singular: 'expcat',
        plural: 'expcats',
        relations: {
            expenses: {
                hasMany: {
                    type: 'expense'
                },
            }
        }
    },
    {
        singular: 'expense',
        plural: 'expenses',
        relations: {
            expcat: {
                belongsTo: 'expcat'
            }
        }
    },
    {
        singular: 'setting',
        plural: 'settings'
    },
    {
        singular: 'inccat',
        plural: 'inccats',
        relations: {
            incomes: {
                hasMany: {
                    type: 'income'
                },
            }
        }
    },
    {
        singular: 'income',
        plural: 'incomes',
        relations: {
            inccat: {
                belongsTo: 'inccat'
            }
        }
    },
    {
        singular: 'seat',
        plural: 'seats',
        relations: {
            bus: {
                belongsTo: 'bus'
            },
            schedule: {
                belongsTo: 'schedule'
            },
            customer: {
                belongsTo: 'customer'
            }
        }
    },
    {
        singular: 'sms',
        plural: 'smss'
    },
    {
        singular: 'staff',
        plural: 'staffs',
        relations: {
            driverslicenses: {
                hasMany: {
                    type: 'driverslicense'
                },
            },
            branch: {
                belongsTo: 'branch'
            },
            stafftype: {
                belongsTo: 'stafftype'
            }
        }
    },
    {
        singular: 'stafftype',
        plural: 'stafftypes',
        relations: {
            staffs: {
                hasMany: {
                    type: 'staff'
                },
            }
        }
    },
];