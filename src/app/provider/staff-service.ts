/**
 * Service provider for PouchDB
 * @name pouch-service.ts
 * @author Agbonaye Osaru - osaru@sarutech.com
 */
import { Injectable } from '@angular/core';
declare var require: any;
import PouchDB from 'pouchdb';
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('relational-pouch'));
import 'rxjs/add/operator/map';

import { Staff } from './models';
import { Schema } from './relational-schema';

@Injectable()
export class StaffService {

    private db;
    remote: any;

    /**
     * Constructor
     */
    constructor() {
        
    }

    /**
     * Initialize PouchDB database
     */
    initDB() {
        this.db = new PouchDB('transport_demo', {adaptater: 'websql'});
        this.db.setSchema(Schema);
        this.remote = 'http://sarutech.com:5984/transport_demo';

        let options = {
            Auth: {
                username: 'sarutech',
                password: 'Sarutobi2014'
            },
            live: true,
            retry: true,
            continuous: true
            };

    this.db.sync(this.remote, options);

        this.db.createIndex({
            index: {
                fields: ['_id']
            }
        });
    }


    /***********
     * STAFF
     **********/
    /**
     * Save a staff
     * @param {staff} Staff
     *
     * @return Promise<Staff>
     */
    saveStaff(staff: Staff): Promise<Staff> {
        staff.driverslicenses = [];
        return this.db.rel.save('staff', staff)
            .then((data: any) => {
                //console.log(data);
                //console.log(staff);
                if (data && data.staffs && data.staffs
                [0]) {
                    return data.staffs[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

     /**
     * Update a staff
     * @param {staff} Staff
     *
     * @return Promise<Staff>
     */
    updateStaff(staff: Staff): Promise<Staff> {
        return this.db.rel.save('staff', staff)
            .then((data: any) => {
                if (data && data.staffs && data.staffs
                [0]) {
                    return data.staffs[0]
                }

                return null;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Remove a staff
     * @param {staff} Staff
     *
     * @return Promise<boolean>
     */
    removeStaff(staff: Staff): Promise<boolean> {
        return this.db.rel.del('staff', staff)
            .then((data: any) => {
                return data && data.deleted ? data.deleted: false;
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Return all the staffs
     *
     * @return Promise<Array<Staff>>
     */
    getStaffs(): Promise<Array<Staff>> {
        return this.db.rel.find('staff')
            .then((data: any) => {
                return data.staffs ? data.staffs : [];
            }).catch((err: any) => {
                console.error(err);
            });
    }

    /**
     * Read a staff
     * @param {staff} staff
     *
     * @return Promise<Staff>
     */
    getStaff(staff: Staff): Promise<Staff> {
        return this.db.rel.find('staff', staff.id)
        .then((data: any) => {
            return data && data.staffs ? data.staffs[0]: null;
        }).catch((err: any) => {
            console.error(err);
        });
    }
}
