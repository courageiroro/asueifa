import { Injectable } from '@angular/core';
//import {StaffsService} from './main/content/apps/staffs/staffs.service'

@Injectable()
export class UserService {
 public isUserLoggedIn;

  constructor(/* private staffservice:StaffsService */) {
    this.isUserLoggedIn = false;
   }


  setUserLoggedIn() {

    this.isUserLoggedIn = true;
    console.log(this.isUserLoggedIn);
    //this.username = 'admin';
  }

  getUserLoggedIn() {
    return this.isUserLoggedIn;
  }

}
